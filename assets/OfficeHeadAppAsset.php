<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Подключение JS и Css
 * Class AppAsset
 * @package app\assets
 */
class OfficeHeadAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/bootstrap/css/bootstrap.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        '/dist/css/AdminLTE.min.css',
        '/dist/css/skins/_all-skins.min.css',
        //'/plugins/iCheck/flat/blue.css',
        '/plugins/iCheck/all.css',
        '/plugins/morris/morris.css',
        '/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        '/plugins/datepicker/datepicker3.css',
        '/plugins/daterangepicker/daterangepicker.css',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        '/plugins/ionslider/ion.rangeSlider.css',
        '/plugins/ionslider/ion.rangeSlider.skinNice.css',
        '/plugins/bootstrap-slider/slider.css',
        '/css/office.css?v=57'
    ];
    public $js = [
        '/plugins/jQuery/jquery-2.2.3.min.js',
        'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
        'https://cdn.rawgit.com/zenorocha/clipboard.js/master/dist/clipboard.min.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
    ];

    public function __construct()
    {
    }

}
