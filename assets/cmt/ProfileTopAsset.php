<?php
namespace app\assets\cmt;

use yii\web\AssetBundle;

class ProfileTopAsset extends AssetBundle
{
    public $css = [
        'bootstrap/dist/css/bootstrap.min.css',
        'css/animate.css',
        'css/style.css',
        'css/colors/blue.css',
        'css/colors/default.css',
    ];
    public $js = [
        'https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
