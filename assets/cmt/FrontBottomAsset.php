<?php
namespace app\assets\cmt;

use yii\web\AssetBundle;

class FrontBottomAsset extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'main/js/core.min.js',
        'main/js/script.js',
        'https://widgets.bitcoin.com/widget.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
