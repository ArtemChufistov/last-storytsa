<?php
namespace app\assets\cmt;

use yii\web\AssetBundle;

class FrontTopAsset extends AssetBundle
{
    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans:400,700%7CExo+2:100,300,500,700,100italic,300italic%7CMontserrat:400,700',
        'main/css/fonts.css',
        'main/css/bootstrap.css',
        'main/css/style.css',
    ];
    public $js = [

    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}