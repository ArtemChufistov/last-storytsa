<?php
namespace app\assets\cmt;

use yii\web\AssetBundle;

class OfficeBottomAsset extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'bootstrap/dist/js/bootstrap.min.js',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
        'js/jquery.slimscroll.js',
        'js/waves.js',
        'plugins/bower_components/waypoints/lib/jquery.waypoints.js',
        'plugins/bower_components/counterup/jquery.counterup.min.js',
        'plugins/bower_components/raphael/raphael-min.js',
        'plugins/bower_components/morrisjs/morris.js',
        'plugins/bower_components/chartist-js/dist/chartist.min.js',
        'plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js',
        'plugins/bower_components/moment/moment.js',
        'plugins/bower_components/calendar/dist/fullcalendar.min.js',
        'plugins/bower_components/calendar/dist/cal-init.js',
        'js/jquery-qrcode.min.js',
        'js/custom.min.js',
        'js/cbpFWTabs.js',
        'plugins/bower_components/toast-master/js/jquery.toast.js',
        'plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'plugins/bower_components/custom-select/custom-select.min.js',
        'plugins/bower_components/bootstrap-select/bootstrap-select.min.js',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
