<?php
namespace app\assets\cmt;

use yii\web\AssetBundle;

class OfficeTopAsset extends AssetBundle
{
    public $css = [
        'bootstrap/dist/css/bootstrap.min.css',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
        'plugins/bower_components/toast-master/css/jquery.toast.css',
        'plugins/bower_components/morrisjs/morris.css',
        'plugins/bower_components/chartist-js/dist/chartist.min.css',
        'plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css',
        'plugins/bower_components/custom-select/custom-select.css',
        'plugins/bower_components/bootstrap-select/bootstrap-select.min.css',
        'plugins/bower_components/multiselect/css/multi-select.css',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'plugins/bower_components/calendar/dist/fullcalendar.css',
        'css/animate.css',
        'css/style.css',
        'css/colors/megna-dark.css'
    ];
    public $js = [

    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
