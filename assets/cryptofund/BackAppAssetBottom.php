<?php
namespace app\assets\cryptofund;

use yii\web\AssetBundle;

class BackAppAssetBottom extends AssetBundle
{
    public $css = [];
    public $js = [
        'js/tether.js',
        'bootstrap/js/bootstrap.js',
        'widgets/progressbar/progressbar.js',
        'widgets/superclick/superclick.js',
        'widgets/input-switch/inputswitch-alt.js',
        'widgets/input-switch/inputswitch.js',
        'widgets/slimscroll/slimscroll.js',
        'widgets/textarea/textarea.js',
        'widgets/multi-select/multiselect.js',
        'widgets/slidebars/slidebars.js',
        'widgets/slidebars/slidebars-demo.js',
        'widgets/charts/piegage/piegage.js',
        'widgets/charts/piegage/piegage-demo.js',
        'widgets/screenfull/screenfull.js',
        'widgets/uniform/uniform.js',
        'widgets/uniform/uniform-demo.js',
        'widgets/content-box/contentbox.js',
        'widgets/overlay/overlay.js',
        'widgets/chosen/chosen.js',
        'js-init/widgets-init.js',
        'themes/admin/layout.js',
        'widgets/touchspin/touchspin.js',
        'widgets/theme-switcher/themeswitcher.js',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
