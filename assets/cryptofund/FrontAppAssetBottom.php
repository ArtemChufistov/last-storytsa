<?php
namespace app\assets\cryptofund;

use yii\web\AssetBundle;

class FrontAppAssetBottom extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'js/tether.js',
        'js/bootstrap.js',
        'widgets/skrollr/skrollr.js',
        'widgets/owlcarousel/owlcarousel.js',
        'widgets/owlcarousel/owlcarousel-demo.js',
        'widgets/sticky/sticky.js',
        'widgets/wow/wow.js',
        'widgets/videobg/videobg.js',
        'widgets/videobg/videobg-demo.js',
        'widgets/mixitup/mixitup.js',
        'widgets/mixitup/isotope.js',
        'widgets/superclick/superclick.js',
        'widgets/input-switch/inputswitch-alt.js',
        'widgets/slimscroll/slimscroll.js',
        'widgets/content-box/contentbox.js',
        'widgets/overlay/overlay.js',
        'js-init/widgets-init.js',
        'js-init/frontend-init.js',
        'themes/frontend/layout.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
