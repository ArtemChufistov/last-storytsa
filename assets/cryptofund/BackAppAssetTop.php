<?php
namespace app\assets\cryptofund;

use yii\web\AssetBundle;

class BackAppAssetTop extends AssetBundle
{
    public $css = [
        'bootstrap/css/bootstrap.css',
        'css/helpers/animate.css',
        'css/helpers/backgrounds.css',
        'css/helpers/boilerplate.css',
        'css/helpers/border-radius.css',
        'css/helpers/grid.css',
        'css/helpers/page-transitions.css',
        'css/helpers/spacing.css',
        'css/helpers/typography.css',
        'css/helpers/utils.css',
        'css/helpers/colors.css',
        'css/elements/badges.css',
        'css/elements/buttons.css',
        'css/elements/content-box.css',
        'css/elements/dashboard-box.css',
        'css/elements/forms.css',
        'css/elements/images.css',
        'css/elements/info-box.css',
        'css/elements/invoice.css',
        'css/elements/loading-indicators.css',
        'css/elements/menus.css',
        'css/elements/panel-box.css',
        'css/elements/response-messages.css',
        'css/elements/responsive-tables.css',
        'css/elements/ribbon.css',
        'css/elements/social-box.css',
        'css/elements/tables.css',
        'css/elements/tile-box.css',
        'css/elements/timeline.css',
        'css/icons/fontawesome/fontawesome.css',
        'css/icons/linecons/linecons.css',
        'css/icons/spinnericon/spinnericon.css',
        'widgets/accordion-ui/accordion.css',
        'widgets/calendar/calendar.css',
        'widgets/carousel/carousel.css',
        'widgets/charts/justgage/justgage.css',
        'widgets/charts/morris/morris.css',
        'widgets/charts/piegage/piegage.css',
        'widgets/charts/xcharts/xcharts.css',
        'widgets/chosen/chosen.css',
        'widgets/colorpicker/colorpicker.css',
        'widgets/datatable/datatable.css',
        'widgets/datepicker/datepicker.css',
        'widgets/datepicker-ui/datepicker.css',
        'widgets/daterangepicker/daterangepicker.css',
        'widgets/dialog/dialog.css',
        'widgets/dropdown/dropdown.css',
        'widgets/dropzone/dropzone.css',
        'widgets/file-input/fileinput.css',
        'widgets/input-switch/inputswitch.css',
        'widgets/input-switch/inputswitch-alt.css',
        'widgets/ionrangeslider/ionrangeslider.css',
        'widgets/jcrop/jcrop.css',
        'widgets/jgrowl-notifications/jgrowl.css',
        'widgets/loading-bar/loadingbar.css',
        'widgets/maps/vector-maps/vectormaps.css',
        'widgets/markdown/markdown.css',
        'widgets/modal/modal.css',
        'widgets/multi-select/multiselect.css',
        'widgets/multi-upload/fileupload.css',
        'widgets/nestable/nestable.css',
        'widgets/noty-notifications/noty.css',
        'widgets/popover/popover.css',
        'widgets/pretty-photo/prettyphoto.css',
        'widgets/progressbar/progressbar.css',
        'widgets/range-slider/rangeslider.css',
        'widgets/slidebars/slidebars.css',
        'widgets/slider-ui/slider.css',
        'widgets/summernote-wysiwyg/summernote-wysiwyg.css',
        'widgets/tabs-ui/tabs.css',
        'widgets/theme-switcher/themeswitcher.css',
        'widgets/timepicker/timepicker.css',
        'widgets/tocify/tocify.css',
        'widgets/tooltip/tooltip.css',
        'widgets/touchspin/touchspin.css',
        'widgets/uniform/uniform.css',
        'widgets/wizard/wizard.css',
        'widgets/xeditable/xeditable.css',
        'css/snippets/chat.css',
        'css/snippets/files-box.css',
        'css/snippets/login-box.css',
        'css/snippets/notification-box.css',
        'css/snippets/progress-box.css',
        'css/snippets/todo.css',
        'css/snippets/user-profile.css',
        'css/snippets/mobile-navigation.css',
        'css/applications/mailbox.css',
        'themes/admin/layout.css',
        'themes/admin/color-schemes/default.css',
        'themes/components/default.css',
        'themes/components/border-radius.css',
        'css/helpers/responsive-elements.css',
        'css/helpers/admin-responsive.css',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
    ];
    public $js = [
        'js-core/jquery-core.js',
        'js-core/jquery-ui-core.js',
        'js-core/jquery-ui-widget.js',
        'js-core/jquery-ui-mouse.js',
        'js-core/jquery-ui-position.js',
        'js-core/modernizr.js',
        'js-core/jquery-cookie.js',
        'widgets/wow/wow.js',
        'widgets/interactions-ui/resizable.js',
        'widgets/interactions-ui/draggable.js',
        'widgets/interactions-ui/sortable.js',
        'widgets/interactions-ui/selectable.js',
        'widgets/dialog/dialog.js',
        'widgets/dialog/dialog-demo.js',
        'widgets/charts/sparklines/sparklines.js',
        'widgets/charts/sparklines/sparklines-demo.js',
        'widgets/charts/flot/flot.js',
        'widgets/charts/flot/flot-resize.js',
        'widgets/charts/flot/flot-stack.js',
        'widgets/charts/flot/flot-pie.js',
        'widgets/charts/flot/flot-tooltip.js',
        'widgets/tabs/tabs.js',
        'widgets/wizard/wizard.js',
        'widgets/wizard/wizard-demo.js',
        'widgets/charts/piegage/piegage.js',
        'widgets/charts/piegage/piegage-demo.js',
        'https://cdn.rawgit.com/zenorocha/clipboard.js/master/dist/clipboard.min.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
