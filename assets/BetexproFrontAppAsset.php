<?php
/**
 * @package   yii2-cms
 * @author    Yuri Shekhovtsov <shekhovtsovy@yandex.ru>
 * @copyright Copyright &copy; Yuri Shekhovtsov, lowbase.ru, 2015 - 2016
 * @version   1.0.0
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Подключение JS и Css
 * Class AppAsset
 * @package app\assets
 */
class BetexproFrontAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css',
        '//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,800%7COswald',
    ];
    public $js = [
        '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js',
        'js/core.min.js',
        'js/script.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

    public function __construct()
    {
        //$this->basePath = '@app/themes/' . \Yii::$app->params['theme'];
        //$this->baseUrl = '@app/themes/' . \Yii::$app->params['theme'];
    }

}
