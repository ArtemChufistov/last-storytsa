<?php
namespace app\assets\fontan;

use yii\web\AssetBundle;

class SignUpAppAsset extends AssetBundle
{

    public $css = [
        'css/style.css',
        '//fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:300,300italic,400,700,900%7CYesteryear',

    ];
    public $js = [


    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];


    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}

