<?php
namespace app\assets\fontan;

use yii\web\AssetBundle;

class FrontAppAssetTop extends AssetBundle
{

    public $css = [
        'css/style2.css',
        '//fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,700,700italic'

    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];


    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
