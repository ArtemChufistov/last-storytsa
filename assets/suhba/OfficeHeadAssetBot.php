<?php
namespace app\assets\suhba;

use yii\web\AssetBundle;

class OfficeHeadAssetBot extends AssetBundle
{
    public $css = [
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
    ];
    public $js = [
        '/js/bootstrap-treeview.js',
        'bootstrap/js/bootstrap.js',
        'widgets/progressbar/progressbar.js',
        'widgets/superclick/superclick.js',
        'widgets/input-switch/inputswitch-alt.js',
        'widgets/slimscroll/slimscroll.js',
        'widgets/slidebars/slidebars.js',
        'widgets/charts/piegage/piegage.js',
        'widgets/screenfull/screenfull.js',
        'widgets/content-box/contentbox.js',
        'widgets/overlay/overlay.js',
        'js-init/widgets-init.js',
        'themes/admin/layout.js',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'https://cdn.rawgit.com/zenorocha/clipboard.js/master/dist/clipboard.min.js',
        'widgets/charts/piegage/piegage.js',
        'widgets/charts/piegage/piegage-demo.js',
        'widgets/tabs-ui/tabs.js',
        'widgets/tabs/tabs.js',
        'widgets/tabs/tabs-responsive.js',

//        'widgets/theme-switcher/themeswitcher.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
