<?php
namespace app\assets\suhba;

use yii\web\AssetBundle;

class FrontAssetBottom extends AssetBundle
{
    public $css = [
    ];
    public $js = [

//        'bootstrap/js/bootstrap.js',
//        'widgets/progressbar/progressbar.js',
//        'widgets/superclick/superclick.js',
//        'widgets/input-switch/inputswitch-alt.js',
//        'widgets/slimscroll/slimscroll.js',
//        'widgets/slidebars/slidebars.js',
//        'widgets/slidebars/slidebars-demo.js',
//        'widgets/charts/piegage/piegage.js',
//        'widgets/charts/piegage/piegage-demo.js',
//        'widgets/screenfull/screenfull.js',
//        'widgets/content-box/contentbox.js',
//        'widgets/overlay/overlay.js',
//        'js-init/widgets-init.js',
//        'themes/admin/layout.js',
//        'widgets/theme-switcher/themeswitcher.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}