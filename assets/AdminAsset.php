<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Подключение JS и Css
 * Class AppAsset
 * @package app\assets
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/admin.css?v=3',
    ];
    public $js = [
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

    public function __construct()
    {
        //$this->basePath = '@app/themes/' . \Yii::$app->params['theme'];
        //$this->baseUrl = '@app/themes/' . \Yii::$app->params['theme'];
    }

}
