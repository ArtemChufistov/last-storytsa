<?php
namespace app\assets\bitmoda;

use yii\web\AssetBundle;

class BackAppAssetBottom extends AssetBundle
{
    public $css = [];
    public $js = [

        'js/excanvas.min.js',
        'js/jquery.browser.js',
        'js/jquery.ui.custom.js',
        'js/bootstrap.min.js',
        'js/jquery.flot.min.js',
        'js/jquery.flot.resize.min.js',
        'js/jquery.peity.min.js',
        'js/fullcalendar.min.js',
        'js/jquery.gritter.min.js',
        'js/jquery.uniform.js',
        'js/matrix.interface.js',
        'js/jquery.validate.js',
        'js/jquery.wizard.js',
        'js/select2.min.js',
        'js/matrix.popover.js',
        'js/jquery.dataTables.min.js',
        'js/bootstrap-colorpicker.js',
        'js/bootstrap-datepicker.js',
        'js/jquery.toggle.buttons.js',
        'js/wysihtml5-0.3.0.js',
        'js/bootstrap-wysihtml5.js',
        '//www.youtube.com/iframe_api',
        '/js/bootstrap-treeview.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
