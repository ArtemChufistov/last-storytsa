<?php
namespace app\assets\bitmoda;

use yii\web\AssetBundle;

class BackAppAssetTop extends AssetBundle
{
    public $css = [
        'css/bootstrap.min.css',
        'css/bootstrap-responsive.min.css',
        'css/fullcalendar.css',
        'css/matrix-style.css',
        'css/matrix-media.css',
        'css/font-awesome.css',
        'css/jquery.gritter.css',
        '//fonts.googleapis.com/css?family=Open+Sans:400,700,800',
        'css/colorpicker.css',
        'css/datepicker.css',
        'css/uniform.css',
        'css/select2.css',
        'css/bootstrap-wysihtml5.css',
        'css/bootstrap-toggle-buttons.css'
    ];
    public $js = [
        'https://cdn.rawgit.com/zenorocha/clipboard.js/master/dist/clipboard.min.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
