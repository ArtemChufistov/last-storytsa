<?php
namespace app\assets\biopro;

use yii\web\AssetBundle;
class FrontAppAssetBottom extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'vendor/jquery/jquery.min.js',
        'vendor/tether/tether.min.js',
        'vendor/bootstrap/js/bootstrap.min.js',
        'vendor/jquery-easing/jquery.easing.min.js',
        'js/jqBootstrapValidation.js',
        'js/contact_me.js',
        'js/agency.min.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
