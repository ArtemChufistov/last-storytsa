<?php
namespace app\assets\family;

use yii\web\AssetBundle;

class FrontAssetBottom extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'bootstrap/dist/js/bootstrap.min.js',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
        'js/jquery.slimscroll.js',
        'js/waves.js',
        'js/custom.min.js',
        'plugins/bower_components/styleswitcher/jQuery.style.switcher.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
