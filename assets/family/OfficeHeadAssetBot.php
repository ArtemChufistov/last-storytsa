<?php
namespace app\assets\family;

use yii\web\AssetBundle;

class OfficeHeadAssetBot extends AssetBundle
{
    public $css = [

    ];
    public $js = [
        'bootstrap/dist/js/bootstrap.min.js',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
        'plugins/bower_components/waypoints/lib/jquery.waypoints.js',
        'plugins/bower_components/counterup/jquery.counterup.min.js',
        'js/jquery.slimscroll.js',
        'js/waves.js',
        'plugins/bower_components/chartist-js/dist/chartist.min.js',
        'plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js',
        'plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js',
        'plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js',
        'js/custom.min.js',
        'js/dashboard3.js',
        'plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
