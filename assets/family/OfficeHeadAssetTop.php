<?php
namespace app\assets\family;

use yii\web\AssetBundle;

class OfficeHeadAssetTop extends AssetBundle
{
    public $css = [
        'bootstrap/dist/css/bootstrap.min.css',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
        'plugins/bower_components/chartist-js/dist/chartist.min.css',
        'plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css',
        'plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.css',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'css/animate.css',
        'css/style.css',
        'css/colors/megna-dark.css'
    ];
    public $js = [

    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
