<?php
namespace app\assets\family;

use yii\web\AssetBundle;

class FrontAppAssetTop extends AssetBundle
{
    public $css = [
        'bootstrap/dist/css/bootstrap.min.css',
        'css/animate.css',
        'css/style.css',
        'css/colors/blue.css'
    ];
    public $js = [

    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
