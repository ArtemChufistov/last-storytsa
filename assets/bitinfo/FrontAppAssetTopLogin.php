<?php
namespace app\assets\bitinfo;

use yii\web\AssetBundle;

class FrontAppAssetTopLogin extends AssetBundle
{

    public $css = [
        'css/style2.css',
        '//fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:300,300italic,400,700,900%7CYesteryear',

    ];
    public $js = [

    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}

