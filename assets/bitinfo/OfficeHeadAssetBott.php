<?php

namespace app\assets\bitinfo;

use yii\web\AssetBundle;

class OfficeHeadAssetBott extends AssetBundle
{

    public $css = [
    ];
    public $js = [
        'widgets/charts/sparklines/sparklines.js',
        'widgets/charts/sparklines/sparklines-demo.js',
        'widgets/charts/flot/flot.js',
        'widgets/charts/flot/flot-resize.js',
        'widgets/charts/flot/flot-stack.js',
        'widgets/charts/flot/flot-pie.js',
        'widgets/charts/flot/flot-tooltip.js',
        'widgets/charts/piegage/piegage.js',
        'widgets/charts/piegage/piegage-demo.js',
        'bootstrap/js/bootstrap.js',
        'widgets/progressbar/progressbar.js',
        'widgets/superclick/superclick.js',
        'widgets/slimscroll/slimscroll.js',
        'widgets/slidebars/slidebars.js',
        'widgets/slidebars/slidebars-demo.js',
        'widgets/charts/piegage/piegage.js',
        'widgets/charts/piegage/piegage-demo.js',
        'widgets/screenfull/screenfull.js',
        'widgets/content-box/contentbox.js',
        'widgets/overlay/overlay.js',
        'js-init/widgets-init.js',
        'themes/admin/layout.js',
        'widgets/theme-switcher/themeswitcher.js',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        '/js/bootstrap-treeview.js',

    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
