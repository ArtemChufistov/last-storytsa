<?php
namespace app\assets\bitinfo;

use yii\web\AssetBundle;

class FrontAssetBottom extends AssetBundle
{

    public $css = [
    ];
    public $js = [
        'js/jquery.js',
        'js/core.min.js',
        'js/script.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
