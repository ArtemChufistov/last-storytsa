<?php
namespace app\assets\job;

use yii\web\AssetBundle;

class FrontAppAssetTop extends AssetBundle
{

    public $css = [
        '//fonts.googleapis.com/css?family=Roboto:400,300,500,700%7CPT+Serif:400,700',
        'css/style.css',
    ];
    public $js = [
        'js/html5shiv.min.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
