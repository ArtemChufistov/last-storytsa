<?php
namespace app\assets\stamp;

use yii\web\AssetBundle;

class BackAppAssetBottom extends AssetBundle
{
    public $css = [];
    public $js = [
        'js/bootstrap.min.js',
        'js/material.min.js',
        'js/chartist.min.js',
        'js/bootstrap-notify.js',
        'js/material-dashboard.js',
        'js/moment.js',
        'js/datepicker.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
