<?php
namespace app\assets\ingamo;

use yii\web\AssetBundle;

class OfficeAssetTop extends AssetBundle
{
    public $css = [
        'css/app.css',
        'css/style-lk.css',
    ];
    public $js = [

    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
