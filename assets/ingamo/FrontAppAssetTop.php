<?php
namespace app\assets\ingamo;

use yii\web\AssetBundle;

class FrontAppAssetTop extends AssetBundle
{
    public $css = [
        'css/bootstrap.css',
        'css/fonts.css',
        'css/style.css',
        'css/style-1.css',
    ];
    public $js = [

    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
