<?php
namespace app\assets\ingamo;

use yii\web\AssetBundle;

class OfficeAssetBottom extends AssetBundle
{
    public $css = [

    ];
    public $js = [
        'js/app.js',
        'https://cdn.rawgit.com/zenorocha/clipboard.js/master/dist/clipboard.min.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
