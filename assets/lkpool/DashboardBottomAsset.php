<?php
namespace app\assets\lkpool;

use yii\web\AssetBundle;

class DashboardBottomAsset extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'assets/js/dashboard.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
