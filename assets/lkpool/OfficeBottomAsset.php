<?php
namespace app\assets\lkpool;

use yii\web\AssetBundle;

class OfficeBottomAsset extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'pages/js/pages.min.js',
        'assets/js/scripts.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
