<?php
namespace app\assets\lkpool;

use yii\web\AssetBundle;

class GalleryBottomAsset extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'assets/plugins/imagesloaded/imagesloaded.pkgd.min.js',
        'assets/plugins/jquery-isotope/isotope.pkgd.min.js',
        'assets/plugins/codrops-dialogFx/dialogFx.js',
        'assets/plugins/owl-carousel/owl.carousel.min.js',
        'assets/plugins/jquery-nouislider/jquery.nouislider.min.js',
        'assets/plugins/jquery-nouislider/jquery.liblink.js',
        'assets/js/gallery.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
