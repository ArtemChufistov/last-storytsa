<?php
namespace app\assets\lkpool;

use yii\web\AssetBundle;

class SignupBottomAsset extends AssetBundle
{
    public $css = [
        'assets/plugins/pace/pace-theme-flash.css',

    ];
    public $js = [
        'assets/plugins/pace/pace.min.js',
        'assets/plugins/jquery/jquery-1.11.1.min.js',
        'assets/plugins/modernizr.custom.js',
        'assets/plugins/jquery-ui/jquery-ui.min.js',
        'assets/plugins/tether/js/tether.min.js',
        'assets/plugins/bootstrap/js/bootstrap.min.js',
        'assets/plugins/jquery/jquery-easy.js',
        'assets/plugins/jquery-unveil/jquery.unveil.min.js',
        'assets/plugins/jquery-ios-list/jquery.ioslist.min.js',
        'assets/plugins/jquery-actual/jquery.actual.min.js',
        'assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
        'assets/plugins/select2/js/select2.full.min.js',
        'assets/plugins/classie/classie.js',
        'assets/plugins/switchery/js/switchery.min.js',
        'assets/plugins/jquery-validation/js/jquery.validate.min.js',
        'assets/plugins/summernote/js/summernote.min.js',
        'pages/js/pages.min.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
