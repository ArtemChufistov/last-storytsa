<?php
namespace app\assets\lkpool;

use yii\web\AssetBundle;

class SignupTopAsset extends AssetBundle
{
    public $css = [
        'assets/plugins/pace/pace-theme-flash.css',
        'assets/plugins/bootstrap/css/bootstrap.min.css',
        'assets/plugins/font-awesome/css/font-awesome.css',
        'assets/plugins/jquery-scrollbar/jquery.scrollbar.css',
        'assets/plugins/select2/css/select2.min.css',
        'assets/plugins/switchery/css/switchery.min.css',
        'pages/css/pages-icons.css',
        'pages/css/pages.css'
    ];
    public $js = [
        'https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
