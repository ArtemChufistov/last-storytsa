<?php
namespace app\assets\lkpool;

use yii\web\AssetBundle;

class OfficeTopAsset extends AssetBundle
{
    public $css = [
        'assets/plugins/pace/pace-theme-flash.css',
        'assets/plugins/bootstrap/css/bootstrap.min.css',
        'assets/plugins/font-awesome/css/font-awesome.css',
        'assets/plugins/jquery-scrollbar/jquery.scrollbar.css',
        'assets/plugins/select2/css/select2.min.css',
        'assets/plugins/switchery/css/switchery.min.css',
        'assets/plugins/nvd3/nv.d3.min.css',
        'assets/plugins/mapplic/css/mapplic.css',
        'assets/plugins/rickshaw/rickshaw.min.css',
        'assets/plugins/bootstrap-datepicker/css/datepicker3.css',
        'assets/plugins/jquery-metrojs/MetroJs.css',
        'pages/css/pages-icons.css',
        'pages/css/pages.css'
    ];
    public $js = [
        'assets/plugins/pace/pace.min.js',
        'assets/plugins/jquery/jquery-1.11.1.min.js',
        'assets/plugins/modernizr.custom.js',
        'assets/plugins/jquery-ui/jquery-ui.min.js',
        'assets/plugins/tether/js/tether.min.js',
        'assets/plugins/bootstrap/js/bootstrap.min.js',
        'assets/plugins/jquery/jquery-easy.js',
        'assets/plugins/jquery-unveil/jquery.unveil.min.js',
        'assets/plugins/jquery-ios-list/jquery.ioslist.min.js',
        'assets/plugins/jquery-actual/jquery.actual.min.js',
        'assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
        'assets/plugins/select2/js/select2.full.min.js',
        'assets/plugins/classie/classie.js',
        'assets/plugins/switchery/js/switchery.min.js',
        'assets/plugins/nvd3/lib/d3.v3.js',
        'assets/plugins/nvd3/nv.d3.min.js',
        'assets/plugins/nvd3/src/utils.js',
        'assets/plugins/nvd3/src/tooltip.js',
        'assets/plugins/nvd3/src/interactiveLayer.js',
        'assets/plugins/nvd3/src/models/axis.js',
        'assets/plugins/nvd3/src/models/line.js',
        'assets/plugins/nvd3/src/models/lineWithFocusChart.js',
        'assets/plugins/mapplic/js/hammer.js',
        'assets/plugins/mapplic/js/jquery.mousewheel.js',
        'assets/plugins/mapplic/js/mapplic.js',
        'assets/plugins/rickshaw/rickshaw.min.js',
        'assets/plugins/jquery-metrojs/MetroJs.min.js',
        'assets/plugins/jquery-sparkline/jquery.sparkline.min.js',
        'assets/plugins/skycons/skycons.js',
        'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
