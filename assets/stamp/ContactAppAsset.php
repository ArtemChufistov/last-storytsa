<?php
namespace app\assets\stamp;

use yii\web\AssetBundle;

class ContactAppAsset extends AssetBundle
{

    public $css = [
        'css/google-map.css',
        'css/mailform.css',
    ];
    public $js = [
        
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
