<?php
namespace app\assets\stamp;

use yii\web\AssetBundle;

class FrontAppAssetBottom extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'js/jquery.cookie.js',
        'js/jquery.easing.1.3.js',
        'js/pointer-events.js',
        'js/tmstickup.js',
        'js/jquery.ui.totop.js',
        'js/jquery.equalheights.js',
        'js/rd-smoothscroll.min.js',
        'js/superfish.js',
        'js/jquery.rd-navbar.js',
        'js/jquery.mobile.customized.min.js',
        'js/camera.js',
        'js/jquery.rd-google-map.js',
        'js/wow.js',
        'js/mailform/jquery.form.min.js',
        'js/mailform/jquery.rd-mailform.min.c.js',
        'js/mailform/jquery.rd-mailform.min.js',
        'js/ziehharmonika.js',
        'js/script.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
