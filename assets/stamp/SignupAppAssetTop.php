<?php
namespace app\assets\stamp;

use yii\web\AssetBundle;

class SignupAppAssetTop extends AssetBundle
{
    public $css = [
        '//cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css',
        'http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        'css/signup.css',
        'css/jquery.fancybox.min.css',
    ];
    public $js = [
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
