<?php
namespace app\assets\stamp;

use yii\web\AssetBundle;

class BackAppAssetTop extends AssetBundle
{
    public $css = [
        'css/bootstrap.min.css',
        '//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
        '//fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons',
        'css/material-dashboard.css',
        'css/datepicker.css',
    ];
    public $js = [
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
