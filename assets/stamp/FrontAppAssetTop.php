<?php
namespace app\assets\stamp;

use yii\web\AssetBundle;

class FrontAppAssetTop extends AssetBundle
{

    public $css = [
        'css/grid.css',
        'css/style.css',
        'css/camera.css',
        'css/mailform1.css',
        'css/ziehharmonika.css',
    ];
    public $js = [
        'js/jquery.js',
        'js/jquery-migrate-1.2.1.js',
        'js/device.min.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
