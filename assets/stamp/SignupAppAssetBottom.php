<?php
namespace app\assets\stamp;

use yii\web\AssetBundle;

class SignupAppAssetBottom extends AssetBundle
{
    public $css = [];
    public $js = [
        'js/signup.js',
        'js/jquery.fancybox.min.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
