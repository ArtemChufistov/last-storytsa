<?php
/**
 * @package   yii2-cms
 * @author    Yuri Shekhovtsov <shekhovtsovy@yandex.ru>
 * @copyright Copyright &copy; Yuri Shekhovtsov, lowbase.ru, 2015 - 2016
 * @version   1.0.0
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Подключение JS и Css
 * Class AppAsset
 * @package app\assets
 */
class VulcanSignUpAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/sunrise-style.css',
        '//fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:400,700',
    ];
    public $js = [
        //'//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js',
        //'js/core.min.js',
        //'js/script.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

    public function __construct()
    {
        //$this->basePath = '@app/themes/' . \Yii::$app->params['theme'];
        //$this->baseUrl = '@app/themes/' . \Yii::$app->params['theme'];
    }

}
