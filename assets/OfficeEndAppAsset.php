<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Подключение JS и Css
 * Class AppAsset
 * @package app\assets
 */
class OfficeEndAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [

        '/bootstrap/js/bootstrap.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
        '/plugins/morris/morris.min.js',
        '/plugins/sparkline/jquery.sparkline.min.js',
        '/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        '/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        '/plugins/knob/jquery.knob.js',
        'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js',
        '/plugins/daterangepicker/daterangepicker.js',
        '/plugins/datepicker/bootstrap-datepicker.js',
        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        '/plugins/ionslider/ion.rangeSlider.min.js',
        '/plugins/bootstrap-slider/bootstrap-slider.js',
        '/plugins/slimScroll/jquery.slimscroll.min.js',
        '/plugins/fastclick/fastclick.js',
        '/dist/js/app.min.js',
        //'/dist/js/pages/dashboard.js',
        '/dist/js/demo.js',
        '/js/widget-button.js',
        '/js/bootstrap-treeview.js',
        '/plugins/iCheck/icheck.min.js',
        '/plugins/chartjs/Chart.min.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
    ];

    public function __construct()
    {
    }

}
