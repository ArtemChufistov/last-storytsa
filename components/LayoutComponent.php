<?php

namespace app\components;

use Yii;

class LayoutComponent extends \lowbase\user\controllers\UserController
{

	public static function currentTheme() {
		return '@app/themes/' . \Yii::$app->params['theme'];
	}

	public static function themePath($url) {
		return self::currentTheme() . $url;
	}
}