<?php

namespace app\components;

use Yii;

class UserController extends \lowbase\user\controllers\UserController
{

	public function init()
	{
		return parent::init();
	}

    public function actionShow($alias)
    {
    	return parent::actionShow($alias);
    }

	public function render($view, $params = [])
	{
		return parent::render(Yii::$app->getView()->theme->pathMap['@app/views'] . '/' . $view, $params);
	}
}