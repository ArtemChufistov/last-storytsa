<?php

namespace app\components;

use app\modules\event\models\Event;
use app\modules\profile\models\User;

class RefComponent {
	// если передан ref перезаписать cookie и вернуть ref, если ref записан в куки вернуть ref, иначе пусто
	public static function init($ref = '') {
		if (empty($ref)) {
			$ref = \Yii::$app->getRequest()->getQueryParam('ref');
		}

		if (!empty($ref)) {

			$ref = str_replace('/', '', $ref);

			$user = User::find()->where(['login' => $ref])->one();

			if (!empty($user)) {
				$event = new Event;

				$event->type = Event::TYPE_REF_LINK;
				$event->user_id = $user->id;
				$event->date_add = date('Y-m-d H:i:s');

				$event->save(false);
			}

			$cookies = \Yii::$app->response->cookies;

			$cookies->add(new \yii\web\Cookie([
				'name' => 'ref',
				'value' => $ref,
			]));

		} else {
			$cookies = \Yii::$app->request->cookies;
			if (!empty($cookies->get('ref'))) {
				$ref = $cookies->get('ref')->value;
			}
		}

		return $ref;
	}
}
?>