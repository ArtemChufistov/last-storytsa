<?php

namespace app\components;

use Yii;
use app\controllers\DocumentController;

class FrontendController extends DocumentController
{
	public $title;

	public function init()
	{
		return parent::init();
	}

    public function actionShow($alias = 'index')
    {
    	return parent::actionShow($alias);
    }

	public function render($view, $params = [])
	{
		return parent::render(Yii::$app->getView()->theme->pathMap['@app/views'] . '/' . $view, $params);
	}
}