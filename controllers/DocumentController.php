<?php

namespace app\controllers;

use app\components\RefComponent;
use app\modules\profile\models\User;

class DocumentController extends \lowbase\document\controllers\DocumentController
{
	public function init()
	{
		RefComponent::init();

		return parent::init();
	}

    public function actionShow($alias = 'index')
    {
    	return parent::actionShow($alias);
    }
}
