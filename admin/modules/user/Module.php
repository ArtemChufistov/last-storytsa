<?php
/**
 * @package   yii2-cms
 */

namespace app\admin\modules\user;

use yii\base\BootstrapInterface;

/**
 * Модуль пользователя
 * унаследованный от модуля \lowbase\user\Module
 * Class Module
 * @package app\admin\user
 */
class Module extends \lowbase\user\Module implements BootstrapInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\admin\modules\user\controllers';

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin',
                'route' => 'admin/index',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin',
                'route' => 'admin/index',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/user/<action:(index|update|delete|view|rmv|multidelete|multiactive|multiblock)>',
                'route' => 'admin-user/user/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/country/<action:(index|create|update|delete|view|multidelete)>',
                'route' => 'admin-user/country/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/city/<action:(index|create|update|delete|view|multidelete)>',
                'route' => 'admin-user/city/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/role/<action:(index|create|update|delete|view|multidelete)>',
                'route' => 'admin-user/auth-item/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/rule/<action:(index|create|update|delete|view|multidelete)>',
                'route' => 'admin-user/auth-rule/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/<module:\w+>/<controller:\w+>/<action:[0-9a-zA-Z_\-]+>',
                'route' => '<module>/<controller>/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/<module:\w+>/<controller:\w+>/<action:[0-9a-zA-Z_\-]+>/<id:\d+>',
                'route' => '<module>/<controller>/<action>',
            ]
        ]);
    }
}
