
<?php 
use app\modules\profile\models\AuthAssignment;
use yii\helpers\Url;
?>
<?php foreach($model->getAuthAssignments()->all() as $authAsignment): ?>
	<?php if ($authAsignment->item_name == AuthAssignment::ITEM_NAME_NOT_VERIFIED_USER):?>
		<a class = "notVerified" href = "<?php echo Url::to(['/admin-user/user/verify', 'id' => $model->id]);?>"><?php echo $authAsignment->authTitle();?></a>
	<?php else:?>
		<?php echo $authAsignment->authTitle();?>
	<?php endif;?>
	</br>
<?php endforeach;?>