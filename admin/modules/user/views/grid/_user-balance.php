<?php 
use app\modules\finance\models\UserBalance;
?>

<?php foreach(UserBalance::find()->where(['user_id' => $model->id])->all() as $userBalance): ?>
	<?php if ($userBalance->value > 0):?>
		<span style = "white-space: nowrap;"><?php echo $userBalance->value;?> <?php echo $userBalance->getCurrency()->one()->title;?></span><br/>
	<?php endif;?>
<?php endforeach;?>