<?php
/**
 * @package   yii2-cms
 * @author    Yuri Shekhovtsov <shekhovtsovy@yandex.ru>
 * @copyright Copyright &copy; Yuri Shekhovtsov, lowbase.ru, 2015 - 2016
 * @version   1.0.0
 */

namespace app\admin\modules\document;

use yii\base\BootstrapInterface;
/**
 * Модуль документов
 * унаследованный от модуля \lowbase\document\Module
 * Class Module
 * @package app\admin\document\document
 */
class Module extends \lowbase\user\Module implements BootstrapInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\admin\modules\document\controllers';

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/template/<action:(index|create|update|delete|view|multidelete)>',
                'route' => 'admin-document/template/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/document/<action:(index|create|update|delete|view|multidelete|multiactive|multiblock|move|change|field)>',
                'route' => 'admin-document/document/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'admin/field/<action:(create|update|delete|multidelete)>',
                'route' => 'admin-document/field/<action>'
            ]
        ]);
    }

}
