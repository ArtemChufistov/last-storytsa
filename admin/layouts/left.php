<?php

use dmstr\widgets\Menu;
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <?=Menu::widget(
	[
		'options' => ['class' => 'sidebar-menu'],
		'items' => [
			['label' => 'Панель администрирования', 'options' => ['class' => 'header']],
			['label' => 'Рабочий стол', 'icon' => 'tachometer', 'url' => ['/admin/index']],
			['label' => 'Контент', 'icon' => 'align-justify', 'items' => [
				['label' => 'Документы', 'icon' => 'align-justify', 'url' => ['/admin-document/document/index']],
				['label' => 'Шаблоны', 'icon' => 'align-justify', 'url' => ['/admin-document/template/index']],
				['label' => 'Меню', 'icon' => 'align-justify', 'url' => ['/menu/menu/index'], 'visible' => Yii::$app->user->can('menuIndex')],
				['label' => 'Новости', 'icon' => 'align-justify', 'url' => ['/news/news/index'], 'visible' => Yii::$app->user->can('newsIndex')],
			]],
			['label' => 'Обратная связь', 'icon' => 'align-justify', 'url' => ['/contact/contactmessage/index'], 'visible' => Yii::$app->user->can('contactMessageIndex')],
			['label' => 'Видео', 'icon' => 'align-justify', 'url' => ['/profile/youtubevideo/index'], 'visible' => Yii::$app->user->can('videoIndex')],
			['label' => 'Отзывы', 'icon' => 'align-justify', 'url' => ['/response/response/index'], 'visible' => Yii::$app->user->can('responseIndex')],
			['label' => 'События', 'icon' => 'align-justify', 'url' => ['/event/event/index'], 'visible' => Yii::$app->user->can('eventIndex')],
			['label' => 'Тех. поддержка', 'icon' => 'align-justify', 'url' => ['/support/ticket/index'], 'visible' => Yii::$app->user->can('ticketIndex'), 'items' => [
				['label' => 'Тикеты', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('ticketIndex'), 'url' => ['/support/ticket/index']],
				['label' => 'Отделы', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('ticketIndex'), 'url' => ['/support/ticketdepartment/index']],
				['label' => 'Приоритеты', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('ticketImportanceIndex'), 'url' => ['/support/ticketimportance/index']],
				['label' => 'Статусы', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('ticketStatusIndex'), 'url' => ['/support/ticketstatus/index']],
			]],
			['label' => 'Финансы', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('paymentIndex'), 'items' => [
				['label' => 'Входящие платежи', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('paymentIndex'), 'url' => ['/finance/paymentin/index']],
				['label' => 'Исходящие платежи', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('paymentIndex'), 'url' => ['/finance/paymentout/index']],
				['label' => 'История платежей', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('paymentIndex'), 'url' => ['/finance/paymentlog/index']],
				['label' => 'Платёжные системы', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('paySystemIndex'), 'url' => ['/finance/paysystem/index']],
				['label' => 'Валюты', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('currencyIndex'), 'url' => ['/finance/currency/index']],
				['label' => 'Транзакции', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('transactionIndex'), 'url' => ['/finance/transaction/index']],
				['label' => 'Управ. вводом/выводом', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('currencycourseIndex'), 'url' => ['/finance/currencycourse/index']],
				['label' => 'Крипто кошельки', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('cryptowalletIndex'), 'url' => ['/finance/cryptowallet/index']],
				['label' => 'Залить графики с бирж', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('graphpoloniexIndex'), 'url' => ['/finance/graphhistory/index']],
			]],
			['label' => 'Платёжные реквизиты', 'icon' => 'vcard', 'items' => [
				['label' => 'BlockChain', 'icon' => 'btc', 'visible' => Yii::$app->user->can('userPayWalletBitcoinIndex'), 'url' => ['/finance/userpaywalletbitcoin/index']],
				['label' => 'Coinbase', 'icon' => 'btc', 'visible' => Yii::$app->user->can('userPayWalletCoinbaseIndex'), 'url' => ['/finance/userpaywalletcoinbase/index']],
				['label' => 'Perfect Money', 'icon' => 'usd', 'visible' => Yii::$app->user->can('userPayWalletPerfectMoneyIndex'), 'url' => ['/finance/userpaywalletperfectmoney/index']],
				['label' => 'Advanced cash', 'icon' => 'usd', 'visible' => Yii::$app->user->can('userPayWalletAdvcashIndex'), 'url' => ['/finance/userpaywalletadvcash/index']],
				['label' => 'Наличные', 'icon' => 'rub', 'visible' => Yii::$app->user->can('userPayWalletPerfectMoneyIndex'), 'url' => ['/finance/userpaywalletcash/index']],
			]],
			['label' => 'Платёжные поручения', 'icon' => 'book', 'visible' => Yii::$app->user->can('paymentIndex'), 'items' => [
				['label' => 'BlockChain', 'icon' => 'btc', 'visible' => Yii::$app->user->can('userPayWalletBitcoinIndex'), 'url' => ['/finance/coinbasetransaction/index']],
				['label' => 'Coinbase', 'icon' => 'btc', 'visible' => Yii::$app->user->can('userPayWalletBitcoinIndex'), 'url' => ['/finance/coinbasetransaction/index']],
				['label' => 'Perfect Money', 'icon' => 'usd', 'visible' => Yii::$app->user->can('userPayWalletBitcoinIndex'), 'url' => ['/finance/perfectmoneytransaction/index']],
				['label' => 'Advanced cash', 'icon' => 'usd', 'visible' => Yii::$app->user->can('userPayWalletBitcoinIndex'), 'url' => ['/finance/advcashtransaction/index']],
				['label' => 'F-change', 'icon' => 'usd', 'visible' => Yii::$app->user->can('userPayWalletBitcoinIndex'), 'url' => ['/finance/fchangetransaction/index']],
				['label' => 'Payeer', 'icon' => 'usd', 'visible' => Yii::$app->user->can('userPayWalletBitcoinIndex'), 'url' => ['/finance/payeertransaction/index']],
				['label' => 'Наличные', 'icon' => 'rub', 'visible' => Yii::$app->user->can('userPayWalletBitcoinIndex'), 'url' => ['/finance/cashtransaction/index']],
			]],
			['label' => 'Матрицы', 'icon' => 'th', 'visible' => Yii::$app->user->can('matrixIndex'), 'items' => [
				['label' => 'Управление вершинами', 'icon' => 'crosshairs', 'visible' => Yii::$app->user->can('matrixIndex'), 'url' => ['/matrix/matrix/index']],
				['label' => 'Очередь', 'icon' => 'database', 'visible' => Yii::$app->user->can('matrixQueueIndex'), 'url' => ['/matrix/matrixqueue/index']],
				['label' => 'Категории матриц', 'icon' => 'bars', 'visible' => Yii::$app->user->can('matrixCategoryIndex'), 'url' => ['/matrix/matrixcategory/index']],
				['label' => 'Настройки', 'icon' => 'bars', 'visible' => Yii::$app->user->can('MatrixPrefIndex'), 'url' => ['/matrix/matrixpref/index']],
				['label' => 'Управление местами', 'icon' => 'code-fork', 'visible' => Yii::$app->user->can('matrixChangePlaceIndex'), 'url' => ['/matrix/matrixchangeplace/index']],
				['label' => 'Места', 'icon' => 'code-fork', 'visible' => Yii::$app->user->can('matrixChangePlaceIndex'), 'url' => ['/matrix/matrixplaces/index']],
				['label' => 'Уровневые данные', 'icon' => 'id-card-o', /*'visible' => Yii::$app->user->can('mailTemplateIndex'),*/'url' => ['/matrix/matrixgift/index']],
			]],
			['label' => 'Инвест. пакеты', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('investTypeIndex'), 'items' => [
				['label' => 'Управление пакетами', 'icon' => 'briefcase', 'visible' => Yii::$app->user->can('investTypeIndex'), 'url' => ['/invest/investtype/index']],
				['label' => 'Настройки пакетов', 'icon' => 'file-o', 'visible' => Yii::$app->user->can('investPrefIndex'), 'url' => ['/invest/investpref/index']],
				['label' => 'Приобретённые пакеты', 'icon' => 'cart-plus', 'visible' => Yii::$app->user->can('userInvestIndex'), 'url' => ['/invest/userinvest/index']],
			]],
			['label' => 'Почтовые сообщения', 'icon' => 'envelope-open-o', 'visible' => Yii::$app->user->can('mailIndex'), 'items' => [
				['label' => 'Шаблоны', 'icon' => 'envelope', 'visible' => Yii::$app->user->can('mailTemplateIndex'), 'url' => ['/mail/mailtemplate/index']],
				['label' => 'Очередь', 'icon' => 'tasks', 'visible' => Yii::$app->user->can('mailIndex'), 'url' => ['/mail/mail/index']],
			]],
			['label' => 'Пользователи', 'icon' => 'user-o', 'visible' => Yii::$app->user->can('userManager'), 'items' => [
				['label' => 'Пользователи', 'icon' => 'user', 'visible' => Yii::$app->user->can('userManager'), 'url' => ['/admin-user/user/index']],
				['label' => 'Управление структурами', 'icon' => 'list', 'visible' => Yii::$app->user->can('structUserIndex'), 'url' => ['/profile/structuser/index']],
				['label' => 'Роли и права', 'icon' => 'eye', 'visible' => Yii::$app->user->can('roleManager'), 'url' => ['/admin-user/auth-item/index']],
				['label' => 'Правила допусков', 'icon' => 'lock', 'visible' => Yii::$app->user->can('roleManager'), 'url' => ['/admin-user/auth-rule/index']],
				['label' => 'Страны', 'icon' => 'globe', 'visible' => Yii::$app->user->can('countryManager'), 'url' => ['/admin-user/country/index']],
        		['label' => 'Штаты USA', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('usaStateIndex'), 'url' => ['/profile/usastate/index']],
				['label' => 'Города', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('countryManager'), 'url' => ['/admin-user/city/index']],
				['label' => 'Подписчики', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('subscriberIndex'), 'url' => ['/profile/subscriber/index']],
				['label' => 'Категории работы', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('occupationIndex'), 'url' => ['/profile/occupation/index']],
				['label' => 'Формы работы', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('jobInfoIndex'), 'url' => ['/profile/jobinfo/index']],
			]],
			['label' => 'Статистика', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('statRegIndex'), 'items' => [
				['label' => 'Регистрации', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('statRegIndex'), 'url' => ['/stat/reg/index']],
				['label' => 'Оплаты', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('statPaymentIndex'), 'url' => ['/stat/payment/index']],
				['label' => 'Места в матрице', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('statMatrixIndex'), 'url' => ['/stat/matrix/index']],
				['label' => 'Инвест. пакеты', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('statInvestIndex'), 'url' => ['/stat/invest/index']],
				['label' => 'Внутр. транзакции', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('statUserBalanceIndex'), 'url' => ['/stat/transaction/index']],
				['label' => 'Балансы', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('statUserBalanceIndex'), 'url' => ['/stat/userbalance/index']],
			]],
			['label' => 'Табличные выгрузки', 'icon' => 'file-excel-o', 'visible' => Yii::$app->user->can('statRegIndex'), 'items' => [
				['label' => 'Пользователи', 'icon' => 'user', 'visible' => Yii::$app->user->can('statRegIndex'), 'url' => ['/exportfile/reg/index']],
			]],
			['label' => 'Планировщик задач', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('jobIndex'), 'items' => [
				['label' => 'Задачи', 'icon' => 'crop', 'visible' => Yii::$app->user->can('jobIndex'), 'url' => ['/crontab/job/index']],
				['label' => 'История запуска', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('cronlogIndex'), 'url' => ['/crontab/cronlog/index']],
			]],
			['label' => 'Интернационализация', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('langMessageIndex'), 'items' => [
				['label' => 'Языки сайта', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('langIndex'), 'url' => ['/lang/lang/index']],
				['label' => 'Переводы', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('langMessageIndex'), 'url' => ['/lang/message/index']],
				['label' => 'Машинный перевод', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('administrator'), 'url' => ['/lang/message/index']],
			]],
			['label' => 'Настройки системы', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('preferenceIndex'), 'url' => ['/mainpage/preference/index']],
			['label' => 'Файловый менеджер', 'icon' => 'align-justify', 'template' => '<a href="{url}" target="_blank">{icon}{label}</a>', 'visible' => Yii::$app->user->can('administrator'), 'url' => ['/filerun/']],
			['label' => 'Gii', 'icon' => 'align-justify', 'visible' => Yii::$app->user->can('administrator'), 'url' => ['/gii']],
			['label' => 'Выход', 'icon' => 'align-justify', 'url' => ['/logout']],
		],
	]
)?>

    </section>

</aside>
