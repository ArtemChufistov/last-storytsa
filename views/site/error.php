<?php echo $this->render('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/error', [
    "name" => $name,
    "message" => $message,
    "code" => $code,
    "status" => $status
]);?>