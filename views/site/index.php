<?php
/**
 * @package   yii2-cms
 * @author    Yuri Shekhovtsov <shekhovtsovy@yandex.ru>
 * @copyright Copyright &copy; Yuri Shekhovtsov, lowbase.ru, 2015 - 2016
 * @version   1.0.0
 */

use yii\helpers\Url;

$url = 'i18/language/index';
?>

<a href ="<?php echo Url::to([$url]);?>"><?php echo Url::to([$url]);?></a>