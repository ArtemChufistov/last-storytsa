$(document).ready(function() {
  $("#slider").slider({
      range: "min",
      animate: true,
      value:5000,
      min: 100,
      max: 20000,
      step: 10,
      slide: function(event, ui) {
        update(1,ui.value); //changed
      }
  });

  $("#amount").val(5000);
  $("#amount-label").text(0);

  update();
});


function update(slider,val) {
  var formatNumber = {
    separador: ",",
    sepDecimal: '.',
    formatear:function (num){
    num +='';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
    }
    return this.simbol + splitLeft  +splitRight;
    },
    new:function(num, simbol){
    this.simbol = simbol ||'';
    return this.formatear(num);
    }
  }
  //changed. Now, directly take value from ui.value. if not set (initial, will use current value.)
  var $amount = slider == 1?val:$("#amount").val();

  $percent = (($amount / 100) * 0.9) * 90;
  $total = $percent + $amount;
  $total = formatNumber.new($total,"$");
  $( "#amount" ).val($amount);
  $( "#amount-label" ).text($amount);
  $( "#percent-label").text($percent);
  $( "#total" ).val($total);
  $( "#total-label" ).text($total);

  $('#slider span').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
  $('#slider span').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');


  console.log('percent: '+$percent);
  console.log('amount: '+$amount);
}