$(".calculator .grid .data input[type=range]").on("input", function() {
    $(this).css({
        background: "linear-gradient(to right, #4ba1dd 0%, #4ba1dd " + $(this).val() / $(this).prop("max") * 100 + "%, #d7dde6 " + $(this).val() / $(this).prop("max") * 100 + "%, #d7dde6 100%)"
    }), $(".grid").each(function() {
        var a = new Date;
        a.setDate(a.getDate() + $(this).find('input[name="time"]').val());
        var b = ("0" + a.getDate()).slice(-2) + "." + ("0" + (a.getMonth() + 1)).slice(-2) + "." + a.getFullYear(),
            c = parseInt($(this).find('input[name="sum"]').val(), 10) + Math.round(.009 * $(this).find('input[name="sum"]').val() * $(this).find('input[name="time"]').val());
        $(this).find("span.sum-details").html($(this).find('input[name="sum"]').val() + " руб"), $(this).find("span.time-details").html($(this).find('input[name="time"]').val() + " дней"), $(this).find("h2").html("Всего к оплате: " + c + " pуб. * до " + b + " (включительно)")
    })
}), $(".calculator .grid .data input[type=range]").trigger("input"), $("#cash-payment-button").click(function(a) {
    a.preventDefault(), $("#card-grid").removeClass("active"), $("#cash-grid").addClass("active"), $("#card-payment-button").parent().removeClass("active"), $("#cash-payment-button").parent().addClass("active")
}), $("#card-payment-button").click(function(a) {
    a.preventDefault(), $("#cash-grid").removeClass("active"), $("#card-grid").addClass("active"), $("#cash-payment-button").parent().removeClass("active"), $("#card-payment-button").parent().addClass("active")
}), $(".dots a").click(function(a) {
    a.preventDefault();
    var b = 805,
        c = parseInt($(".slider ul").css("left")) - b * ($(this).index() - $(".dots a.active").index());
    $(".dots a.active").removeClass("active"), $(this).addClass("active"), $(".slider ul").animate({
        left: c
    }, "slow", "swing")
});