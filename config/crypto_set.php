<?php

$active_crypto_wallets = ['bitcoin', 'litecoin', 'bitcoin-cash', 'dogecoin'/*, 'ethereum', /*'monero', 'ripple'/*, 'pay_day_coin'/*, 'ripple', 'zcash'*/];

$crypto_config = [
    'bootstrap' => ['merchant', 'api'], // Модули
    'components' => [
        'response' => [
            'class' => 'yii\web\Response',
            'formatters' => [
                'jsonApi' => 'app\modules\profile\components\merchant\helpers\JsonApiFormatter',
            ],
        ],
        'tool' => [
            'class' => 'app\modules\profile\components\merchant\helpers\Tool',
        ],
        'bitcoin' => [
            'class' => 'app\modules\merchant\components\merchant\Altcoin',
            'currency' => 'bitcoin',
            'username' => 'bitcoin-rpc',
            'password' => 'TsmVwU0nvphrLw-V1gSyLFAubQJo7FysX0+lH13uamY3',
            'host' => '172.100.10.1',
            'port' => '8332',
        ],
        'litecoin' => [
            'class' => 'app\modules\merchant\components\merchant\Altcoin',
            'currency' => 'litecoin',
            'username' => 'litecoin-rpc',
            'password' => 'oi+trXWUdtDPKlMb4MGnxMDiBegov1hXqbTvQ+WDeNnzA',
            'host' => '172.100.10.3',
            'port' => '9332',
        ],
        'bitcoin-cash' => [
            'class' => 'app\modules\merchant\components\merchant\Altcoin',
            'currency' => 'bitcoin-cash',
            'username' => 'bitcoin-cash',
            'password' => 'oi+t3@rXWUdtDPKl5Mb4MGnxMDiBe7go4v1hXqbTvQWDeNnz72A2',
            'host' => '172.100.10.4',
            'port' => '8332',
        ],
        'dogecoin' => [
            'class' => 'app\modules\merchant\components\merchant\Altcoin',
            'currency' => 'dogecoin',
            'username' => 'dogecoin-rpc',
            'password' => 'oi+trXWUdtDPKlMb4MGnxMDiBegov1hXqbTvQ+WDfdeNnzA',
            'host' => '172.100.10.5',
            'port' => '8332',
        ],
        'ethereum' => [
            'class' => 'app\modules\merchant\components\merchant\Ethereum',
            'currency' => 'ethereum',
            'host' => '172.100.10.2',
            'port' => '8545',
        ],
//        'smart_quorum' => [
//            'class' => 'app\modules\merchant\components\merchant\Altcoin',
//            'username' => 'smart_quorum',
//            'password' => 'oi+trXWUdtDPKlMb4MGnxMDiBegov1hXqbTvQFFdsWDeNnzA',
//            'host' => '172.17.0.11',
//            'port' => '18332',
//        ],
//        'pay_day_coin' => [
//            'class' => 'app\modules\merchant\components\merchant\Altcoin',
//            'username' => 'pay_day_coin',
//            'password' => 'oi+trXWUdtDPKlMb4MGnxMDiBegov1hXqbTvQWDeNnzA',
//            'host' => '172.17.0.10',
//            'port' => '19332',
//        ],
        'monero' => [
            'class' => 'app\modules\merchant\components\merchant\Monero',
            'host' => '172.100.10.6',
            'port' => '18081',
        ],
        'ripple' => [
            'class' => 'app\admin\modules\altcoin\components\Ripple',
            'urlNode' => 'url',
            'address' => 'xrp_address',
            'secret' => 'xrp_secret',
        ],
//        'zcash' => [
//            'class' => 'app\admin\modules\altcoin\components\Altcoin',
//            'username' => 'zec_account',
//            'password' => 'zec_password',
//            'host' => 'zec_host',
//            'port' => 'zec_port',
//        ],
//        'bytecoin' => [
//            'class' => 'app\admin\modules\altcoin\components\Bytecoin',
//            'host' => 'bcn_host',
//        ],
    ],

    'modules' => [
        'api' => [
            'class' => 'app\modules\api\Module',
//            'layout' => '@app/views/layouts/admin',
//            'layout' => '@app/admin/layouts/main.php',
            'allowedUsers' => ['admin@example.ru'],
            'wallets' => $active_crypto_wallets,
            //            'mainPageCache' => 60 * 1,
        ],
        'merchant' => [
            'class' => 'app\modules\merchant\Module',
            'allowedUsers' => ['admin@example.ru'],
            'wallets' => $active_crypto_wallets,
            //            'mainPageCache' => 60 * 1,
        ],
    ],
];

return $crypto_config;
