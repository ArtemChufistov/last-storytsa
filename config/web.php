<?php
/**
 * @package   yii2-cms
 */

$params = require __DIR__ . '/params.php';

$config = [
    'id' => 'Suhba',
    'name' => 'Suhba',
    'sourceLanguage' => 'ru-RU',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'profile', 'matrix', 'contact',/* 'admin-user',*/ 'invest', 'finance'],
    'components' => [
        'twoFa' => ['class' => 'app\modules\merchant\components\twofa\TwoFa'],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/' . $params['theme'],
                    '@app/modules' => '@app/themes/' . $params['theme'] . '/modules',
                    '@app/widgets' => '@app/themes/' . $params['theme'] . '/widgets',
                ],

            ],
        ],
        'request' => [
            'cookieValidationKey' => 'ISfNWi2OD58V6WoC8fYVx0q28RaiilRr',
            'enableCsrfValidation' => true,
            'class' => 'app\modules\lang\components\LangRequest',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        //-----------------------
        // Компонент пользователя
        //-----------------------
        'user' => [
            'class' => 'app\modules\merchant\components\twofa\User',
            'identityClass' => 'app\modules\profile\models\User',
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'enableAutoLogin' => true,
            'loginUrl' => ['/login'],
            'on afterLogin' => function ($event) {
                app\modules\profile\models\User::afterLogin($event->identity->id);
            },
        ],
        //--------------------------------------------------------
        // Компонент OAUTH для авторизации через социальные сети,
        // где вмето ? указываем полученные после регистрации
        // клиентский ID и секретный ключ.
        // В комментария указаны ссылки для регистрации приложений
        // в соответствующих социальных сетях.
        //--------------------------------------------------------
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    // https://vk.com/editapp?act=create
                    'class' => 'app\modules\profile\components\oauth\VKontakte',
                    'clientId' => '6125564',
                    'clientSecret' => 'pnt9P9Uw8MO09TnxSr70',
                    'scope' => 'email',
                ],
                'google' => [
                    // https://console.developers.google.com/project
                    'class' => 'app\modules\profile\components\oauth\GoogleOAuth',
                    'clientId' => '670174886465-aqs5annlbns4b0jc7r3jishse86gsj0e.apps.googleusercontent.com',
                    'clientSecret' => 'TptjS1-RnilTeHaK6s35lOJG',
                ],
                /*
                'twitter' => [
                        // https://dev.twitter.com/apps/new
                        'class' => 'app\modules\profile\components\oauth\Twitter',
                        'consumerKey' => 'fEMElM5TfHfafxBbX9UZfSmR4',
                        'consumerSecret' => 'PDQPL5aqzpJVouokb2oho0FaNZXlbSDPznNFhYR8xP2i7EdlSz',
                ],
                */
                'facebook' => [
                    // https://developers.facebook.com/apps
                    'class' => 'app\modules\profile\components\oauth\Facebook',
                    'clientId' => '1110569449077244',
                    'clientSecret' => '449100fafcf2f58cc2dec6281ec58003',
                ],
                'github' => [
                    // https://github.com/settings/applications/new
                    'class' => 'app\modules\profile\components\oauth\GitHub',
                    'clientId' => '?',
                    'clientSecret' => '?',
                    'scope' => 'user:email, user',
                ],


            ],
        ],
        //---------------------------------------------
        // Для реализации разделения прав пользователей
        // с помощью коробочного модуля Yii2 RBAC.
        //---------------------------------------------
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'itemTable' => 'lb_auth_item',
            'itemChildTable' => 'lb_auth_item_child',
            'assignmentTable' => 'lb_auth_assignment',
            'ruleTable' => 'lb_auth_rule',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require __DIR__ . '/db.php',
        'mailer' => require __DIR__ . '/mailer.php',
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            //'class' => 'app\modules\lang\components\LangUrlManager',
            'rules' => [
/*
                //АДМИНИСТРАТИВНАЯ ЧАСТЬ САЙТА
                'api/test/trampampam' => 'api/test/trampampam',
                'admin/user/verify/<id>' => 'admin-user/user/verify',
                //Взаимодействия с пользователем в панели админстрирования
                'admin/user/<action:(index|update|delete|view|rmv|multidelete|multiactive|multiblock)>' => 'admin-user/user/<action>',
                //Взаимодействия со странами в панели админстрирования
                'admin/country/<action:(index|create|update|delete|view|multidelete)>' => 'admin-user/country/<action>',
                //Взаимодействия с городами в панели администрирования
                'admin/city/<action:(index|create|update|delete|view|multidelete)>' => 'admin-user/city/<action>',
                //Работа с ролями и разделением прав доступа
                'admin/role/<action:(index|create|update|delete|view|multidelete)>' => 'admin-user/auth-item/<action>',
                //Работа с правилами контроля доступа
                'admin/rule/<action:(index|create|update|delete|view|multidelete)>' => 'admin-user/auth-rule/<action>',
                //Взаимодействия с шаблонами в панели администрирования
                'admin/template/<action:(index|create|update|delete|view|multidelete)>' => 'admin-document/template/<action>',
                //Взаимодействия с документами в панели администрирования !!! Правила для документов лучше не менять, т.к. на них завязан js скрипт компонента дерево документов
                'admin/document/<action:(index|create|update|delete|view|multidelete|multiactive|multiblock|move|change|field)>' => 'admin-document/document/<action>',
                //Поиск населенного пункта (города)
                'admin' => 'admin/index',
*/
                /**/
                'city/find' => 'lowbase-user/city/find',

                //Просмотр пользователя
                'user/<id:\d+>' => 'user/show',
                // Лайк документа
                'like/<id:\d+>' => 'document/like',

                // toDo убрать отсюда
                'qrcodemain/<login>' => 'mainpage/site/qrcodemain',
                'qrcodereg/<login>' => 'mainpage/site/qrcodereg',

                //Стартовая страница сайта
                '/' => 'document/show',

                '<alias>' => 'document/show',

               // 'admin/lang' => 'i18n/language/index',

            ],
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'sourceLanguage' => 'en-EN',
                    'forceTranslation' => true,
                    /*
                                                'cachingDuration' => 86400,
                                                'enableCaching' => true,
                    */
                    'on missingTranslation' => ['app\modules\lang\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
                'document' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'sourceLanguage' => 'en-EN',
                    'forceTranslation' => true,
                    /*
                                                'cachingDuration' => 86400,
                                                'enableCaching' => true,
                    */
                    'on missingTranslation' => ['app\modules\lang\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
                'user' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'sourceLanguage' => 'en-EN',
                    'forceTranslation' => true,
                    /*
                                                'cachingDuration' => 86400,
                                                'enableCaching' => true,
                    */
                    'on missingTranslation' => ['app\modules\lang\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
            ],
        ],
    ],
    'controllerMap' => [
        'admin' => [
            'class' => 'app\admin\controllers\AdminController',
        ],
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'lowbase-user' => [
            'class' => '\lowbase\user\Module',
        ],
        'lowbase-document' => [
            'class' => '\lowbase\document\Module',
        ],
        /*
        'admin-user' => [
            'class' => 'app\admin\modules\user\Module',
        ],
        'admin-document' => [
            'class' => 'app\admin\modules\document\Module',
        ],
        */
        'i18n' => [
            'class' => 'app\modules\i18n\Module',
        ],
        'menu' => [
            'class' => 'app\modules\menu\Module',
        ],
        'news' => [
            'class' => 'app\modules\news\Module',
        ],
        'event' => [
            'class' => 'app\modules\event\Module',
        ],
        'mail' => [
            'class' => 'app\modules\mail\Module',
        ],
        'crontab' => [
            'class' => 'app\modules\crontab\Module',
        ],
        'contact' => [
            'class' => 'app\modules\contact\Module',
        ],
        'profile' => [
            'class' => 'app\modules\profile\Module',
        ],
        'response' => [
            'class' => 'app\modules\response\Module',
        ],
        'support' => [
            'class' => 'app\modules\support\Module',
        ],
        'finance' => [
            'class' => 'app\modules\finance\Module',
        ],
        'matrix' => [
            'class' => 'app\modules\matrix\Module',
        ],
        'mainpage' => [
            'class' => 'app\modules\mainpage\Module',
        ],
        'invest' => [
            'class' => 'app\modules\invest\Module',
        ],
        'lang' => [
            'class' => 'app\modules\lang\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '178.49.7.118',/*'212.20.46.243'*/],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '178.49.7.118', '212.20.46.243'],
    ];
}


$crypto_set_arr = require __DIR__ . '/crypto_set.php';
$config['bootstrap'] = array_merge($config['bootstrap'], $crypto_set_arr['bootstrap']);
$config['components'] = array_merge($config['components'], $crypto_set_arr['components']);
$config['modules'] = array_merge($config['modules'], $crypto_set_arr['modules']);

return $config;
