<?php
/**
 * @package   yii2-cms
 */

$params = require __DIR__ . '/params.php';

$config = [
    'id' => 'Waka',
    'name' => 'Waka',
    'sourceLanguage' => 'ru',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'profile', 'matrix','contact', 'admin-user'],
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/' . $params['theme'],
                    '@app/modules' => '@app/themes/' . $params['theme'] . '/modules',
                    '@app/widgets' => '@app/themes/' . $params['theme'] . '/widgets',
                ],

            ],
        ],
        'request' => [
            'cookieValidationKey' => 'ISfNWi2OD58V6WoC8fYVx0q28RaiilRr',
            'enableCsrfValidation' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        //-----------------------
        // Компонент пользователя
        //-----------------------
        'user' => [
                'identityClass' => 'app\modules\profile\models\User',
                'enableAutoLogin' => true,
                'loginUrl' => ['/login'],
                'on afterLogin' => function ($event) {
                        app\modules\profile\models\User::afterLogin($event->identity->id);
                },
        ],
        //--------------------------------------------------------
        // Компонент OAUTH для авторизации через социальные сети,
        // где вмето ? указываем полученные после регистрации
        // клиентский ID и секретный ключ.
        // В комментария указаны ссылки для регистрации приложений
        // в соответствующих социальных сетях.
        //--------------------------------------------------------

        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                /*
                    'vkontakte' => [
                        // https://vk.com/editapp?act=create
                        'class' => 'app\modules\profile\components\oauth\VKontakte',
                        'clientId' => '6125564',
                        'clientSecret' => 'pnt9P9Uw8MO09TnxSr70',
                        'scope' => 'email',
                */
                'google' => [
                    // https://console.developers.google.com/project
                    'class' => 'lowbase\user\components\oauth\Google',
                    'clientId' => '670174886465-oc2ivrla2tr74rm2j64631i9ak0870du.apps.googleusercontent.com',
                    'clientSecret' => 'xAuDpNsK8svpW91omQsfu5vI',
                ],
                /*'twitter' => [
                                        // https://dev.twitter.com/apps/new
                                        'class' => 'vendor\lowbase\yii2-user\components\oauth\Twitter',
                                        'consumerKey' => '?',
                                        'consumerSecret' => '?',
                                    ],
                                    'facebook' => [
                                        // https://developers.facebook.com/apps
                                        'class' => 'vendor\lowbase\yii2-user\components\oauth\Facebook',
                                        'clientId' => '?',
                                        'clientSecret' => '?',
                                    ],
                                    'github' => [
                                        // https://github.com/settings/applications/new
                                        'class' => 'vendor\lowbase\yii2-user\components\oauth\GitHub',
                                        'clientId' => '?',
                                        'clientSecret' => '?',
                                        'scope' => 'user:email, user'
                                    ],
                                    'yandex' => [
                                        // https://oauth.yandex.ru/client/new
                                        'class' => 'vendor\lowbase\yii2-user\components\oauth\Yandex',
                                        'clientId' => '?',
                                        'clientSecret' => '?',
                */
            ],
        ],
        //---------------------------------------------
        // Для реализации разделения прав пользователей
        // с помощью коробочного модуля Yii2 RBAC.
        //---------------------------------------------
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'itemTable' => 'lb_auth_item',
            'itemChildTable' => 'lb_auth_item_child',
            'assignmentTable' => 'lb_auth_assignment',
            'ruleTable' => 'lb_auth_rule',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require __DIR__ . '/db.php',
        'mailer' => require __DIR__ . '/mailer.php',
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [

                //Поиск населенного пункта (города)
                'admin' => 'admin/index',
                'city/find' => 'lowbase-user/city/find',

                //Просмотр пользователя
                'user/<id:\d+>' => 'user/show',
                // Лайк документа
                'like/<id:\d+>' => 'document/like',

                // toDo убрать отсюда
                'qrcodemain/<login>' => 'mainpage/site/qrcodemain',
                'qrcodereg/<login>' => 'mainpage/site/qrcodereg',

                '<alias>' => 'document/show',
                //Стартовая страница сайта
                '/' => 'document/show',

                'admin/lang' => 'i18n/language/index',
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'app\modules\i18n\components\DbMessageSource',
                    'sourceLanguage' => 'ru-RU',
                    'sourceMessageTable' => '{{%i18n_source_message}}',
                    'messageTable' => '{{%i18n_message}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => true,
                ],
            ],
        ],
    ],
    'controllerMap' => [
        'admin' => [
            'class' => 'app\admin\controllers\AdminController',
        ],
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'lowbase-user' => [
            'class' => '\lowbase\user\Module',
        ],
        'lowbase-document' => [
            'class' => '\lowbase\document\Module',
        ],
        'admin-user' => [
            'class' => 'app\admin\modules\user\Module',
        ],
        'admin-document' => [
            'class' => 'app\admin\modules\document\Module',
        ],
        'i18n' => [
            'class' => 'app\modules\i18n\Module',
        ],
        'menu' => [
            'class' => 'app\modules\menu\Module',
        ],
        'news' => [
            'class' => 'app\modules\news\Module',
        ],
        'event' => [
            'class' => 'app\modules\event\Module',
        ],
        'mail' => [
            'class' => 'app\modules\mail\Module',
        ],
        'crontab' => [
            'class' => 'app\modules\crontab\Module',
        ],
        'mainpage' => [
            'class' => 'app\modules\mainpage\Module',
        ],
        'contact' => [
            'class' => 'app\modules\contact\Module',
        ],
        'profile' => [
            'class' => 'app\modules\profile\Module',
        ],
        'response' => [
            'class' => 'app\modules\response\Module',
        ],
        'support' => [
            'class' => 'app\modules\support\Module',
        ],
        'finance' => [
            'class' => 'app\modules\finance\Module',
        ],
        'matrix' => [
            'class' => 'app\modules\matrix\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['37.194.194.55'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['37.194.194.55'],
    ];
}

return $config;
