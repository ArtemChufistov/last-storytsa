<?php
/**
 * @package   yii2-cms
 * @author    Yuri Shekhovtsov <shekhovtsovy@yandex.ru>
 * @copyright Copyright &copy; Yuri Shekhovtsov, lowbase.ru, 2015 - 2016
 * @version   1.0.0
 */

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'Suhba',
    'name' => 'Suhba',
    'sourceLanguage' => 'ru',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'profile', 'matrix','contact', 'admin-user', 'invest', 'finance'],
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/' . $params['theme'],
                    '@app/modules' => '@app/themes/' . $params['theme'] . '/modules',
                    '@app/widgets' => '@app/themes/' . $params['theme'] . '/widgets',
                    ],
                //'baseUrl' => '@app/themes/' . $params['theme'],
                //'basePath' => '@app/themes/' . $params['theme']
            ],
        ],
        'request' => [
            'cookieValidationKey' => 'ISfNWi2OD58V6WoC8fYVx0q28RaiilRr',
            'enableCsrfValidation' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        //-----------------------
        // Компонент пользователя
        //-----------------------
        'user' => [
            'identityClass' => 'app\modules\profile\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/login'],
            'on afterLogin' => function($event) {
                app\modules\profile\models\User::afterLogin($event->identity->id);
            }
        ],
        //--------------------------------------------------------
        // Компонент OAUTH для авторизации через социальные сети,
        // где вмето ? указываем полученные после регистрации
        // клиентский ID и секретный ключ.
        // В комментария указаны ссылки для регистрации приложений
        // в соответствующих социальных сетях.
        //--------------------------------------------------------

        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            /*
            'clients' => [
                'vkontakte' => [
                    // https://vk.com/editapp?act=create
                    'class' => 'lowbase\yii2-user\components\oauth\VKontakte',
                    'clientId' => '?',
                    'clientSecret' => '?',
                    'scope' => 'email'
                ],
                'google' => [
                    // https://console.developers.google.com/project
                    'class' => 'vendor\lowbase\yii2-user\components\oauth\Google',
                    'clientId' => '?',
                    'clientSecret' => '?',
                ],
                'twitter' => [
                    // https://dev.twitter.com/apps/new
                    'class' => 'vendor\lowbase\yii2-user\components\oauth\Twitter',
                    'consumerKey' => '?',
                    'consumerSecret' => '?',
                ],
                'facebook' => [
                    // https://developers.facebook.com/apps
                    'class' => 'vendor\lowbase\yii2-user\components\oauth\Facebook',
                    'clientId' => '?',
                    'clientSecret' => '?',
                ],
                'github' => [
                    // https://github.com/settings/applications/new
                    'class' => 'vendor\lowbase\yii2-user\components\oauth\GitHub',
                    'clientId' => '?',
                    'clientSecret' => '?',
                    'scope' => 'user:email, user'
                ],
                'yandex' => [
                    // https://oauth.yandex.ru/client/new
                    'class' => 'vendor\lowbase\yii2-user\components\oauth\Yandex',
                    'clientId' => '?',
                    'clientSecret' => '?',
                ],
            ],
            */
        ],
        //---------------------------------------------
        // Для реализации разделения прав пользователей
        // с помощью коробочного модуля Yii2 RBAC.
        //---------------------------------------------
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'itemTable' => 'lb_auth_item',
            'itemChildTable' => 'lb_auth_item_child',
            'assignmentTable' => 'lb_auth_assignment',
            'ruleTable' => 'lb_auth_rule'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'mailer' => require(__DIR__ . '/mailer.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                //СЛУЖЕБНЫЕ ФУНКЦИИ ДЛЯ КЛИЕНТСКОЙ И АДМИНИСТРАТИВНОЙ ЧАСТИ САЙТА
                //Авторизация через социальные сети
                'auth/<authclient:[\w\-]+>' => 'lowbase-user/auth/index',
                //'captcha' => 'profile/profile/captcha',
                //Поиск населенного пункта (города)
                'city/find' => 'lowbase-user/city/find',

                //АДМИНИСТРАТИВНАЯ ЧАСТЬ САЙТА
                'admin' => 'admin/index',
                'admin/user/verify/<id>' => 'admin-user/user/verify',
                //Взаимодействия с пользователем в панели админстрирования
                'admin/user/<action:(index|update|delete|view|rmv|multidelete|multiactive|multiblock)>' => 'admin-user/user/<action>',
                //Взаимодействия со странами в панели админстрирования
                'admin/country/<action:(index|create|update|delete|view|multidelete)>' => 'admin-user/country/<action>',
                //Взаимодействия с городами в панели администрирования
                'admin/city/<action:(index|create|update|delete|view|multidelete)>' => 'admin-user/city/<action>',
                //Работа с ролями и разделением прав доступа
                'admin/role/<action:(index|create|update|delete|view|multidelete)>' => 'admin-user/auth-item/<action>',
                //Работа с правилами контроля доступа
                'admin/rule/<action:(index|create|update|delete|view|multidelete)>' => 'admin-user/auth-rule/<action>',
                //Взаимодействия с шаблонами в панели администрирования
                'admin/template/<action:(index|create|update|delete|view|multidelete)>' => 'admin-document/template/<action>',
                //Взаимодействия с документами в панели администрирования !!! Правила для документов лучше не менять, т.к. на них завязан js скрипт компонента дерево документов
                'admin/document/<action:(index|create|update|delete|view|multidelete|multiactive|multiblock|move|change|field)>' => 'admin-document/document/<action>',
                //Взаимодействия с файловым менеджеромч
                'elfinder/<action(connect|manager)>' => 'admin-document/path/<action>',
                //Взаимодействия с дополнительными полями шаблонов
                'admin/field/<action:(create|update|delete|multidelete)>' => 'admin-document/field/<action>',

                'admin/<module:\w+>/<controller:\w+>/<action:[0-9a-zA-Z_\-]+>' => '<module>/<controller>/<action>',
                'admin/<module:\w+>/<controller:\w+>/<action:[0-9a-zA-Z_\-]+>/<id:\d+>' => '<module>/<controller>/<action>',


                'profile/captcha' => 'profile/profile/captcha',

                //КЛИЕНТСКАЯ ЧАСТЬ САЙТА
                //Взаимодействия с пользователем на сайте
                '<action:(login|logout|signup|confirm|reset|profile|remove|online)>' => 'profile/profile/<action>',
                //Просмотр пользователя
                'user/<id:\d+>' => 'user/show',
                // Лайк документа
                'like/<id:\d+>' => 'document/like',
                //Отображение документов
                'contact' => 'contact/contact/index',

                'profile/office/<action>' => 'profile/office/<action>',
                'profile/office/deleteresponse/<id>' => 'profile/office/deleteresponse',
                'profile/office/support/<ticketId>' => 'profile/office/support',
                'profile/office/matrix/<slug>' => 'profile/office/matrix',
                'profile/office/matrix/<slug>/<childSlug>' => 'profile/office/matrix',
                'profile/office/qrcode/<address>/<amount>' => 'profile/office/qrcode',
                'qrcodemain/<login>' => 'mainpage/site/qrcodemain',
                'qrcodereg/<login>' => 'mainpage/site/qrcodereg',

                '<alias>' => 'document/show',
                //Стартовая страница сайта
                '/' => 'document/show',


                'admin/lang' => 'i18n/language/index',
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'app\modules\i18n\components\DbMessageSource',
                    'sourceLanguage' => 'ru-RU',
                    'sourceMessageTable' => '{{%i18n_source_message}}',
                    'messageTable' => '{{%i18n_message}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => true,
                ],
            ],
        ],
    ],
    'controllerMap' => [
        'admin' => [
            'class' => 'app\admin\controllers\AdminController',
        ],
    ],
    'modules' => [
        'gridview' => [
                'class' => '\kartik\grid\Module',
        ],
        'lowbase-user' => [
                'class' => '\lowbase\user\Module',
        ],
        'lowbase-document' => [
                'class' => '\lowbase\document\Module',
        ],
        'admin-user' => [
                'class' => 'app\admin\modules\user\Module',
        ],
        'admin-document' => [
                'class' => 'app\admin\modules\document\Module',
        ],
        'i18n' => [
                'class' => 'app\modules\i18n\Module',
        ],
        'menu' => [
                'class' => 'app\modules\menu\Module',
        ],
        'news' => [
                'class' => 'app\modules\news\Module',
        ],
        'event' => [
                'class' => 'app\modules\event\Module',
        ],
        'mail' => [
                'class' => 'app\modules\mail\Module',
        ],
        'crontab' => [
                'class' => 'app\modules\crontab\Module',
        ],
        'contact' => [
                'class' => 'app\modules\contact\Module',
        ],
        'profile' => [
                'class' => 'app\modules\profile\Module',
        ],
        'response' => [
                'class' => 'app\modules\response\Module',
        ],
        'support' => [
                'class' => 'app\modules\support\Module',
        ],
        'finance' => [
                'class' => 'app\modules\finance\Module',
        ],
        'matrix' => [
                'class' => 'app\modules\matrix\Module',
        ],
        'mainpage' => [
                'class' => 'app\modules\mainpage\Module',
        ],
        'invest' => [
                'class' => 'app\modules\invest\Module',
        ],
        'lang' => [
                'class' => 'app\modules\lang\Module',
        ],
        'stat' => [
                'class' => 'app\modules\stat\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['212.20.46.243'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['212.20.46.243'],
    ];
}

return $config;
