<?php
/* Smarty version 3.1.30, created on 2017-06-09 12:11:54
  from "/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/metadata/sections/default/html/pages/quick_view.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a665a781094_42908477',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '32179c4ecba63b269aa2bb5b50df4f583cdf4a7e' => 
    array (
      0 => '/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/metadata/sections/default/html/pages/quick_view.html',
      1 => 1489777134,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a665a781094_42908477 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['app']->value['rating']) {?>
<div>
	<table cellspacing="1" width="100%">
		<tr>
			<td class="fieldName"><?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['t'][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['t'][0] : null;
if (!is_callable($_block_plugin1)) {
throw new SmartyException('block tag \'t\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('t', array('s'=>"Metadata: Custom"));
$_block_repeat1=true;
echo $_block_plugin1(array('s'=>"Metadata: Custom"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>
Rating<?php $_block_repeat1=false;
echo $_block_plugin1(array('s'=>"Metadata: Custom"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
</td>
			<td class="fieldValue">
				<ul class="rating-star">
					<?php
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['v']->step = 1;$_smarty_tpl->tpl_vars['v']->total = (int) ceil(($_smarty_tpl->tpl_vars['v']->step > 0 ? $_smarty_tpl->tpl_vars['app']->value['rating']-1+1 - (0) : 0-($_smarty_tpl->tpl_vars['app']->value['rating']-1)+1)/abs($_smarty_tpl->tpl_vars['v']->step));
if ($_smarty_tpl->tpl_vars['v']->total > 0) {
for ($_smarty_tpl->tpl_vars['v']->value = 0, $_smarty_tpl->tpl_vars['v']->iteration = 1;$_smarty_tpl->tpl_vars['v']->iteration <= $_smarty_tpl->tpl_vars['v']->total;$_smarty_tpl->tpl_vars['v']->value += $_smarty_tpl->tpl_vars['v']->step, $_smarty_tpl->tpl_vars['v']->iteration++) {
$_smarty_tpl->tpl_vars['v']->first = $_smarty_tpl->tpl_vars['v']->iteration == 1;$_smarty_tpl->tpl_vars['v']->last = $_smarty_tpl->tpl_vars['v']->iteration == $_smarty_tpl->tpl_vars['v']->total;?>
					<li class="fa fa-fw fa-star"></li>
					<?php }
}
?>

				</ul>
			</td>
		</tr>
	</table>
</div>
<?php }
if ($_smarty_tpl->tpl_vars['app']->value['metadata']['fieldsets']) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['app']->value['metadata']['fieldsets'], 'fieldset');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['fieldset']->value) {
?>
<div>
	<div class="fieldsetname">
		<?php echo \S::safeHTML(\S::forHTML($_smarty_tpl->tpl_vars['fieldset']->value['name']));?>

		<a href="javascript:;" onclick="FR.UI.infoPanel.tabs.detailsPanel.editMeta()"><li class="fa fa-edit"></li></a>
	</div>
	<table cellspacing="1" width="100%">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['fieldset']->value['fields'], 'field', false, NULL, 'fields', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
?>
		<tr>
			<td class="fieldName"><?php echo \S::safeHTML(\S::forHTML($_smarty_tpl->tpl_vars['field']->value['name']));?>
</td>
			<td class="fieldValue">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['field']->value['values'], 'value', false, NULL, 'values', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
?>
					<div>
					<?php if ($_smarty_tpl->tpl_vars['field']->value['type'] == 'stars' && $_smarty_tpl->tpl_vars['value']->value) {?>
						<?php
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['v']->step = 1;$_smarty_tpl->tpl_vars['v']->total = (int) ceil(($_smarty_tpl->tpl_vars['v']->step > 0 ? $_smarty_tpl->tpl_vars['value']->value-1+1 - (0) : 0-($_smarty_tpl->tpl_vars['value']->value-1)+1)/abs($_smarty_tpl->tpl_vars['v']->step));
if ($_smarty_tpl->tpl_vars['v']->total > 0) {
for ($_smarty_tpl->tpl_vars['v']->value = 0, $_smarty_tpl->tpl_vars['v']->iteration = 1;$_smarty_tpl->tpl_vars['v']->iteration <= $_smarty_tpl->tpl_vars['v']->total;$_smarty_tpl->tpl_vars['v']->value += $_smarty_tpl->tpl_vars['v']->step, $_smarty_tpl->tpl_vars['v']->iteration++) {
$_smarty_tpl->tpl_vars['v']->first = $_smarty_tpl->tpl_vars['v']->iteration == 1;$_smarty_tpl->tpl_vars['v']->last = $_smarty_tpl->tpl_vars['v']->iteration == $_smarty_tpl->tpl_vars['v']->total;?>
							<li class="fa fa-fw fa-star rating-star"></li>
						<?php }
}
?>

					<?php } else { ?>
						<?php if ($_smarty_tpl->tpl_vars['field']->value['type'] != 'large') {?><a href="javascript:;" onclick="FR.actions.filterMeta('<?php echo \S::safeJS(\S::forHTML($_smarty_tpl->tpl_vars['field']->value['id']));?>
', '<?php echo \S::safeJS(\S::forHTML($_smarty_tpl->tpl_vars['value']->value));?>
')"><?php }
echo \S::safeHTML(\S::forHTML($_smarty_tpl->tpl_vars['value']->value));
if ($_smarty_tpl->tpl_vars['field']->value['type'] != 'large') {?></a><?php }?>
					<?php }?>
					</div>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</td>
		</tr>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	</table>
</div>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

<?php }?>
<div>
	<div class="fieldsetname">
		<?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['t'][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['t'][0] : null;
if (!is_callable($_block_plugin1)) {
throw new SmartyException('block tag \'t\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('t', array('s'=>"Metadata: Custom"));
$_block_repeat1=true;
echo $_block_plugin1(array('s'=>"Metadata: Custom"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>
Tags<?php $_block_repeat1=false;
echo $_block_plugin1(array('s'=>"Metadata: Custom"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

		<a href="javascript:;" onclick="FR.UI.infoPanel.tabs.detailsPanel.editMeta()"><li class="fa fa-edit"></li></a>
	</div>
	<div>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['app']->value['tags'], 'tag', false, NULL, 'tags', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->value) {
?>
		<a href="javascript:;" onclick="FR.actions.filterMeta('<?php echo \S::safeJS(\S::forHTML($_smarty_tpl->tpl_vars['app']->value['tagFieldId']));?>
', '<?php echo \S::safeJS(\S::forHTML($_smarty_tpl->tpl_vars['tag']->value['val']));?>
', 'exact')" class="tag"><?php echo \S::forHTML($_smarty_tpl->tpl_vars['tag']->value['val']);?>
</a>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	</div>
	<div style="clear:both;<?php if (!$_smarty_tpl->tpl_vars['app']->value['metadata']['gps']) {?>height:20px;<?php }?>"></div>
</div>
<?php if ($_smarty_tpl->tpl_vars['app']->value['metadata']['gps'] && $_smarty_tpl->tpl_vars['settings']->value['google_static_maps_api_key']) {?>
<div>
    <div class="fieldsetname"><?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['t'][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['t'][0] : null;
if (!is_callable($_block_plugin1)) {
throw new SmartyException('block tag \'t\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('t', array('s'=>"Metadata: Custom"));
$_block_repeat1=true;
echo $_block_plugin1(array('s'=>"Metadata: Custom"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>
Location<?php $_block_repeat1=false;
echo $_block_plugin1(array('s'=>"Metadata: Custom"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
</div>
    <div style="text-align:center;margin-bottom:20px;">
        <a href="https://www.google.com/maps/place/<?php echo $_smarty_tpl->tpl_vars['app']->value['metadata']['gps']['x'];?>
,<?php echo $_smarty_tpl->tpl_vars['app']->value['metadata']['gps']['y'];?>
" target="_blank"><img src="https://maps.googleapis.com/maps/api/staticmap?size=300x300&zoom=11&scale=2&&markers=color:red|<?php echo $_smarty_tpl->tpl_vars['app']->value['metadata']['gps']['x'];?>
,<?php echo $_smarty_tpl->tpl_vars['app']->value['metadata']['gps']['y'];?>
&key=<?php echo \S::forURL($_smarty_tpl->tpl_vars['settings']->value['google_static_maps_api_key']);?>
" width="97%" border="0" /></a>
    </div>
</div>
<?php }
}
}
