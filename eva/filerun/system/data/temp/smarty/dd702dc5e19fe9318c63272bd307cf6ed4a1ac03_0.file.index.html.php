<?php
/* Smarty version 3.1.30, created on 2017-06-09 12:08:16
  from "/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/install/sections/default/html/pages/index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a65804cdcb6_77903837',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dd702dc5e19fe9318c63272bd307cf6ed4a1ac03' => 
    array (
      0 => '/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/install/sections/default/html/pages/index.html',
      1 => 1485189780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a65804cdcb6_77903837 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>
	<title><?php echo $_smarty_tpl->tpl_vars['app']->value['config']['install']['branding']['name'];?>
 :: Installation</title>
	<link rel="stylesheet" type="text/css" href="css/style.css?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['currentVersion']);?>
" />
	<link rel="stylesheet" type="text/css" href="css/ext.php?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['currentVersion']);?>
" />
	<?php echo '<script'; ?>
 type="text/javascript" src="js/min.php?extjs=1&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['currentVersion']);?>
"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="js/install.js?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['currentVersion']);?>
"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript">
	var URLRoot = '<?php echo $_smarty_tpl->tpl_vars['app']->value['url']['root'];?>
';
	var appName = '<?php echo $_smarty_tpl->tpl_vars['app']->value['config']['install']['branding']['name'];?>
';
	var licenseAgreementURL = '<?php echo $_smarty_tpl->tpl_vars['app']->value['config']['install']['branding']['license_agreement_url'];?>
';
	<?php echo '</script'; ?>
>
    <style>.ext-el-mask { background-color: white; }</style>
</head>
<body id="theBODY">
</body>
</html><?php }
}
