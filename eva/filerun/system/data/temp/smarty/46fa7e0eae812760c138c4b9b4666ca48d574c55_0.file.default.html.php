<?php
/* Smarty version 3.1.30, created on 2017-06-09 13:11:58
  from "/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/software_update/sections/cpanel/html/default.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a746e487062_41720751',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '46fa7e0eae812760c138c4b9b4666ca48d574c55' => 
    array (
      0 => '/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/software_update/sections/cpanel/html/default.html',
      1 => 1485189782,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a746e487062_41720751 (Smarty_Internal_Template $_smarty_tpl) {
echo smarty_function_lang(array('section'=>"Admin: Settings"),$_smarty_tpl);?>

<?php echo '<script'; ?>
>
FR.currentVersion = '<?php echo $_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion'];?>
';
ScriptMgr.load({ scripts:[
	'?module=fileman&section=utils&sec=Admin%3A%20Software update&lang=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['language']);?>
&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
&page=translation.js',
	'js/cpanel/software_update.js?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
'
]});
<?php echo '</script'; ?>
><?php }
}
