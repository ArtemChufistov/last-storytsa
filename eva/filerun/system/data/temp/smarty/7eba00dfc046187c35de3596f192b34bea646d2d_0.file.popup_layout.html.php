<?php
/* Smarty version 3.1.30, created on 2017-06-09 12:42:45
  from "/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/@html/popup_layout.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a6d953ada63_52644336',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7eba00dfc046187c35de3596f192b34bea646d2d' => 
    array (
      0 => '/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/@html/popup_layout.html',
      1 => 1485189762,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a6d953ada63_52644336 (Smarty_Internal_Template $_smarty_tpl) {
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title><?php echo \S::forHTML($_smarty_tpl->tpl_vars['app']->value['name']);?>
</title>
	<link rel="stylesheet" type="text/css" href="css/style.css?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
" />
	<link rel="stylesheet" type="text/css" href="css/ext.php?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
" />
	<style>.ext-el-mask { background-color: white; }</style>
	<?php echo '<script'; ?>
 type="text/javascript" src="js/min.php?extjs=1<?php if ($_smarty_tpl->tpl_vars['app']->value['config']['misc']['developmentMode']) {?>&debug=1<?php }?>&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
>
	var URLRoot = '<?php echo $_smarty_tpl->tpl_vars['app']->value['url']['root'];?>
';
	<?php echo '</script'; ?>
>
	<?php if ($_smarty_tpl->tpl_vars['app']->value['headerContent']) {
$_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['contentTemplateFile']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
}?>
</head>

<body id="theBODY">
<?php if (!$_smarty_tpl->tpl_vars['app']->value['headerContent']) {
$_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['contentTemplateFile']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
}?>
</body>
</html><?php }
}
