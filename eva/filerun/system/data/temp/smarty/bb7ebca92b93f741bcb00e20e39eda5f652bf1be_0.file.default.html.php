<?php
/* Smarty version 3.1.30, created on 2017-06-09 12:43:05
  from "/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/weblinks/sections/default/html/pages/default.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a6da96a7460_32407030',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bb7ebca92b93f741bcb00e20e39eda5f652bf1be' => 
    array (
      0 => '/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/weblinks/sections/default/html/pages/default.html',
      1 => 1489777134,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a6da96a7460_32407030 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
FR = {
    UI: {},
    language: '<?php echo $_smarty_tpl->tpl_vars['app']->value['language'];?>
',
    userCanUpload: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['perms']['upload']) {?>true<?php } else { ?>false<?php }?>,
	userCanEmail: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['perms']['email']) {?>true<?php } else { ?>false<?php }?>,
    disableShortURL: <?php if ($_smarty_tpl->tpl_vars['app']->value['config']['app']['weblinks']['disableShortURL']) {?>true<?php } else { ?>false<?php }?>,
    useClientEmail: <?php if ($_smarty_tpl->tpl_vars['app']->value['config']['app']['weblinks']['useClientEmail']) {?>true<?php } else { ?>false<?php }?>,
    showQR: <?php if ($_smarty_tpl->tpl_vars['app']->value['config']['app']['weblinks']['showQRCode']) {?>true<?php } else { ?>false<?php }?>
};
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="?module=fileman&section=utils&sec=Web%20Links&calendar=1&lang=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['language']);?>
&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
&page=translation.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/fileman/weblink.js?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/genpass.js?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
"><?php echo '</script'; ?>
><?php }
}
