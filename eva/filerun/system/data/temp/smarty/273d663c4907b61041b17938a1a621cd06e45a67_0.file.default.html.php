<?php
/* Smarty version 3.1.30, created on 2017-06-09 12:10:34
  from "/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/cpanel/sections/default/html/default.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a660a7cb854_18426435',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '273d663c4907b61041b17938a1a621cd06e45a67' => 
    array (
      0 => '/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/cpanel/sections/default/html/default.html',
      1 => 1489777134,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a660a7cb854_18426435 (Smarty_Internal_Template $_smarty_tpl) {
?>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <title><?php echo $_smarty_tpl->tpl_vars['app']->value['settings']['app_title'];?>
 :: <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['t'][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['t'][0] : null;
if (!is_callable($_block_plugin1)) {
throw new SmartyException('block tag \'t\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('t', array('s'=>"Main Interface"));
$_block_repeat1=true;
echo $_block_plugin1(array('s'=>"Main Interface"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>
Control Panel<?php $_block_repeat1=false;
echo $_block_plugin1(array('s'=>"Main Interface"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
</title>
	<link rel="stylesheet" type="text/css" href="css/ext.php?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
" />
	<link rel="stylesheet" type="text/css" href="css/cpanel.css?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
" />
	<?php if ($_smarty_tpl->tpl_vars['app']->value['settings']['pushercom_enable']) {?>
	<?php echo '<script'; ?>
 src="https://js.pusher.com/3.0/pusher.min.js"><?php echo '</script'; ?>
>
	<?php }?>
	<?php echo '<script'; ?>
 src="js/min.php?extjs=1<?php if ($_smarty_tpl->tpl_vars['app']->value['config']['misc']['developmentMode']) {?>&debug=1<?php }?>&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="js/min.php?cpanel=1&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="?module=fileman&section=utils&sec=Admin&lang=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['language']);?>
&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
&page=translation.js"><?php echo '</script'; ?>
>
	<?php if ($_smarty_tpl->tpl_vars['app']->value['user']['isSuperUser']) {?>
	<?php echo '<script'; ?>
 src="https://www.filerun.com/version_check/<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);
if ($_smarty_tpl->tpl_vars['app']->value['isFree']) {?>/f<?php }?>" defer><?php echo '</script'; ?>
>
	<?php }?>
	<?php echo '<script'; ?>
>
	FR.URLRoot = '<?php echo $_smarty_tpl->tpl_vars['app']->value['url']['root'];?>
';
	FR.iconURL = '<?php echo $_smarty_tpl->tpl_vars['app']->value['url']['root'];?>
/images/icons/';
	FR.system = <?php echo $_smarty_tpl->tpl_vars['app']->value['system'];?>
;
	FR.user = {
		isAdmin: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['isSuperUser'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_type'] == 'simple') {?>true<?php } else { ?>false<?php }?>,
		isIndep: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_type'] == 'indep') {?>true<?php } else { ?>false<?php }?>,
		isSuperuser: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['isSuperUser']) {?>true<?php } else { ?>false<?php }?>,
		perms: {
			adminUsers: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['isSuperUser'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_users'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_type'] == 'indep') {?>true<?php } else { ?>false<?php }?>,
			adminRoles: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['isSuperUser'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_roles'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_type'] == 'indep') {?>true<?php } else { ?>false<?php }?>,
			adminLogs: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['isSuperUser'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_logs'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_type'] == 'indep') {?>true<?php } else { ?>false<?php }?>,
			adminNotif: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['isSuperUser'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_notifications'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_type'] == 'indep') {?>true<?php } else { ?>false<?php }?>,
			adminMetadata: <?php if ($_smarty_tpl->tpl_vars['app']->value['user']['isSuperUser'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_metadata'] || $_smarty_tpl->tpl_vars['app']->value['user']['perms']['admin_type'] == 'indep') {?>true<?php } else { ?>false<?php }?>
		}
	}
	<?php echo '</script'; ?>
>
    <style>.ext-el-mask { background-color: white; }</style>
</head>
<body></body>
</html><?php }
}
