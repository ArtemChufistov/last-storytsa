<?php
/* Smarty version 3.1.30, created on 2017-06-09 12:09:42
  from "/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/fileman/sections/default/html/pages/index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a65d6a57a70_76259788',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '15bbf778c7583a999e7bb25ed9028cd8fa32056f' => 
    array (
      0 => '/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/fileman/sections/default/html/pages/index.html',
      1 => 1489777134,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:customizables/include.html' => 1,
  ),
),false)) {
function content_593a65d6a57a70_76259788 (Smarty_Internal_Template $_smarty_tpl) {
echo smarty_function_lang(array('section'=>"Main Interface"),$_smarty_tpl);?>

<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<title><?php echo \S::forHTML($_smarty_tpl->tpl_vars['app']->value['settings']['app_title']);?>
</title>
<link rel="icon" type="image/png" href="data:image/png;base64,iVBORw0KGgo=" />
<link rel="stylesheet" href="css/style.css?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
" />
<link rel="stylesheet" href="css/ext.php?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);
if ($_smarty_tpl->tpl_vars['app']->value['config']['misc']['developmentMode']) {?>&debug=1<?php }?>" />
<?php if ($_smarty_tpl->tpl_vars['app']->value['config']['app']['ui']['custom_css_url']) {?><link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['app']->value['config']['app']['ui']['custom_css_url'];?>
?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
" /><?php }?>
<style>
	.tmbItem { height: <?php echo $_smarty_tpl->tpl_vars['app']->value['settings']['thumbnails_size'];?>
px;width: <?php echo $_smarty_tpl->tpl_vars['app']->value['settings']['thumbnails_size'];?>
px; }
	.tmbInner { height: <?php echo $_smarty_tpl->tpl_vars['app']->value['settings']['thumbnails_size']-30;?>
px;width: <?php echo $_smarty_tpl->tpl_vars['app']->value['settings']['thumbnails_size'];?>
px; }
	.tmbItem .thumbFolder { width: <?php echo $_smarty_tpl->tpl_vars['app']->value['settings']['thumbnails_size'];?>
px; }
	.tmbItem.largeItem, .tmbItem.largeItem .tmbInner { height: <?php echo round(($_smarty_tpl->tpl_vars['app']->value['settings']['thumbnails_size']*1.5));?>
px;width: <?php echo round(($_smarty_tpl->tpl_vars['app']->value['settings']['thumbnails_size']*1.5));?>
px; }
</style>
</head>
<body id="theBODY">
<div id="loadMsg"><div><?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['t'][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['t'][0] : null;
if (!is_callable($_block_plugin1)) {
throw new SmartyException('block tag \'t\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('t', array());
$_block_repeat1=true;
echo $_block_plugin1(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>
Loading...<?php $_block_repeat1=false;
echo $_block_plugin1(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
</div></div>

<?php echo '<script'; ?>
 src="js/min.php?extjs=1<?php if ($_smarty_tpl->tpl_vars['app']->value['config']['misc']['developmentMode']) {?>&debug=1<?php }?>&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/min.php?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);
if ($_smarty_tpl->tpl_vars['app']->value['config']['misc']['developmentMode']) {?>&debug=1<?php }?>"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="?module=fileman&section=utils&page=custom_actions.js&language=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['language']);?>
&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="?module=fileman&section=utils&page=translation.js&sec=Main%20Interface&lang=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['language']);?>
&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['app']->value['settings']['pushercom_enable'] && !$_smarty_tpl->tpl_vars['app']->value['settings']['disable_file_history']) {
echo '<script'; ?>
 src="https://js.pusher.com/3.0/pusher.min.js"><?php echo '</script'; ?>
>
<?php }
echo '<script'; ?>
>
	var URLRoot = '<?php echo $_smarty_tpl->tpl_vars['app']->value['url']['root'];?>
';
	var Settings = <?php echo $_smarty_tpl->tpl_vars['app']->value['UISettings'];?>
;
	var User = <?php echo $_smarty_tpl->tpl_vars['app']->value['UIUser'];?>
;
	var Sharing = <?php echo $_smarty_tpl->tpl_vars['app']->value['usersWithShares'];?>
;
	var AnonShares = <?php echo $_smarty_tpl->tpl_vars['app']->value['anonShares'];?>
;
	FR.UI.grid.customColumns = <?php echo $_smarty_tpl->tpl_vars['app']->value['customColumns'];?>
;
	FR.searchMetaColumns = <?php echo $_smarty_tpl->tpl_vars['app']->value['searchMetaColumns'];?>
;
	FR.specialMetaFields = <?php echo $_smarty_tpl->tpl_vars['app']->value['specialMetaFields'];?>
;
	FR.homeFolderCfg = <?php echo $_smarty_tpl->tpl_vars['app']->value['homeFolderCfg'];?>
;
	FR.language = '<?php echo $_smarty_tpl->tpl_vars['app']->value['language'];?>
';
	<?php if ($_smarty_tpl->tpl_vars['app']->value['labels']) {?>FR.labels.addAll(<?php echo $_smarty_tpl->tpl_vars['app']->value['labels'];?>
);<?php }
echo '</script'; ?>
>

	<?php $_smarty_tpl->_subTemplateRender("file:customizables/include.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('page'=>"index"), 0, false);
?>

	<?php if ($_smarty_tpl->tpl_vars['app']->value['settings']['tracker_codes']) {
echo $_smarty_tpl->tpl_vars['app']->value['settings']['tracker_codes'];
}?>
</body>
</html><?php }
}
