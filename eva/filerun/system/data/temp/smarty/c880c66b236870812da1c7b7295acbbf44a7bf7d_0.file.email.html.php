<?php
/* Smarty version 3.1.30, created on 2017-06-09 12:19:04
  from "/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/cpanel/sections/settings/html/pages/email.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a6808c87cf5_40734429',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c880c66b236870812da1c7b7295acbbf44a7bf7d' => 
    array (
      0 => '/var/www/clients/client0/web9/web/cryptofund/web/filerun/system/modules/cpanel/sections/settings/html/pages/email.html',
      1 => 1485189778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a6808c87cf5_40734429 (Smarty_Internal_Template $_smarty_tpl) {
echo smarty_function_lang(array('section'=>"Admin: Settings"),$_smarty_tpl);?>

<?php echo '<script'; ?>
>
FR.settings = <?php echo $_smarty_tpl->tpl_vars['app']->value['AllSettings'];?>
;
ScriptMgr.load({ scripts:[
	'?module=fileman&section=utils&sec=Admin%3A%20Setup&lang=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['language']);?>
&v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
&page=translation.js',
	'js/cpanel/forms/settings_email.js?v=<?php echo \S::forURL($_smarty_tpl->tpl_vars['app']->value['settings']['currentVersion']);?>
'
]});
<?php echo '</script'; ?>
><?php }
}
