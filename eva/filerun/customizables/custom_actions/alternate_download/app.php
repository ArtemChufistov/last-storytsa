<?php

class custom_alternate_download {

	static $localeSection = "Custom Actions: Alternate Download";

	function init() {
		$this->settings = [
			[
				'key' => 'config',
				'title' => self::t('Configuration JSON'),
				'large' => true,
				'comment' => self::t('See <a href="%1" target="_blank">this page</a> for more information.', ['http://docs.filerun.com/alternate_downloads'])
			]
		];
		global $config;
		$postURL = $config['url']['root'].'/?module=custom_actions&action=alternate_download&method=run';
		$this->JSconfig = [
			"title" => self::t("Alternate Download"),
			'iconCls' => 'fa fa-fw fa-download',
			'useWith' => ['img', 'wvideo', 'mp3'],
			"fn" => "FR.UI.backgroundPost(false, '".\S::safeJS($postURL)."')",
			"requires" => ["download"]
		];
	}

	function isDisabled() {
		return (strlen(self::getSetting('config')) == 0);
	}

	static function getSetting($k) {
		global $settings;
		$key = 'plugins_alternate_download_'.$k;
		return $settings->{$key};
	}

	function run() {
		\FileRun::checkPerms("download");
		$cfg = self::getSetting('config');
		$this->config = json_decode($cfg, true);
		if (!$this->config) {
			$this->reportError('Failed to decode JSON config!');
		}
		if (!is_array($this->config['paths'])) {
			$this->reportError('The plugin is configured with an invalid JSON set!');
		}
		$parentPath = \FM::dirname($this->data['filePath']);
		$newParentPath = false;
		$newExt = false;
		foreach($this->config['paths'] as $path) {
			if (\FM::inPath($parentPath, $path['normal'])) {
				$newParentPath = $path['alternate'];
				$newExt = $path['extension'];
				$subPath = substr($parentPath, strlen($path['normal']));
				continue;
			}
		}
		if (!$newParentPath) {
			$this->reportNotFound();
		}
		if ($newExt) {
			$fileName = \FM::replaceExtension($this->data['fileName'], $newExt);
		} else {
			$fileName = $this->data['fileName'];
		}
		$newPath = gluePath($newParentPath, $subPath, $fileName);
		if (!is_file($newPath)) {
			$this->reportNotFound();
		}
		$fileSize = \FM::getFileSize($newPath);
		\FileRun\Utils\Downloads::sendHTTPFile($newPath);
		$bytesSentToBrowser = \FileRun\Utils\Downloads::$bytesSentToBrowser;
		$logData = [
			"relative_path" => $this->data['relativePath'],
			"full_path" => $this->data['filePath'],
			"actual_path" => $newPath,
			"file_size" => $fileSize,
			"interface" => "alternate_download",
			'original_path' => $this->data['filePath'],
			"bytes_sent" => $bytesSentToBrowser
		];
		\FileRun\Log::add(false, "download", $logData);
	}

	static function t($text, $vars = false) {
		return \FileRun\Lang::t($text, self::$localeSection, $vars);
	}

	function reportNotFound() {
		$this->reportError('No alternate download found for the selected file.');
	}

	function reportError($msg) {
		echo '<script>window.parent.FR.UI.feedback(\''.self::t($msg).'\');</script>';
		exit();
	}

}