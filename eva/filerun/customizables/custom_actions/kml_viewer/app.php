<?php

class custom_kml_viewer {
	var $online = true;
	function init() {
		$this->settings = [
			[
				'key' => 'APIKey',
				'title' => self::t('Google Maps JavaScript API Key'),
				'comment' => \FileRun\Lang::t('Get it from %1', 'Admin', ['<a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">Google Maps APIs</a>'])
			]
		];
		$this->JSconfig = [
			"title" => self::t('Google Maps'),
			'icon' => 'images/icons/gmaps.png',
			"extensions" => ["kml", "kmz"],
			"popup" => true,
			"requires" => ["download"]
		];
	}

	static function t($text, $vars = false) {
		return \FileRun\Lang::t($text, 'Custom Actions: Google Maps', $vars);
	}

	function isDisabled() {
		return (strlen(self::getSetting('APIKey')) == 0);
	}

	static function getSetting($k) {
		global $settings;
		$key = 'plugins_kml_viewer_'.$k;
		return $settings->{$key};
	}

	function run() {
		\FileRun::checkPerms("download");
		$data = $this->weblinks->createForService($this->data['filePath'], 2, $this->data['shareInfo']['id']);
		$url = $this->weblinks->getURL([
				"id_rnd" => $data['id_rnd'],
				"filename" => $this->data['fileName']
			]
		);
		if (!$url) {exit("Failed to setup weblink");}
		\FileRun\Log::add(false, "preview", [
			"relative_path" => $this->data['relativePath'],
			"full_path" => $this->data['filePath'],
			"method" => 'Google Earth'
		]);
		require($this->path."/display.php");
	}
}