<?php
class custom_image_viewer {
	function init() {
		$this->JSconfig = [
			"title" => \FileRun\Lang::t("Image Viewer", 'Image Viewer'),
			'iconCls' => 'fa fa-fw fa-picture-o',
			'useWith' => ['nothing']
		];
	}
}