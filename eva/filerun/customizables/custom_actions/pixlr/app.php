<?php
class custom_pixlr {
	var $online = true;
	var $localeSection = "Custom Actions: Pixlr";
	function init() {
		$this->JSconfig = [
			"title" => \FileRun\Lang::t("Pixlr", $this->localeSection),
			'icon' => 'images/icons/pixlr.png',
			"extensions" => ["jpg", "jpeg", "gif", "png", "psd", "bmp", "pxd"],
			"popup" => true, "external" => true,
			"requires" => ["download", "create"],
			"requiredUserPerms" => ["download", "upload"],
			"createNew" => [
				"title" => \FileRun\Lang::t("Image with Pixlr", $this->localeSection),
				"defaultFileName" => \FileRun\Lang::t("Untitled.png", $this->localeSection)
			]
		];
		$this->outputName = "image";
	}
	function run() {
		global $config;
		\FileRun::checkPerms("download");
		$weblinkInfo = $this->weblinks->createForService($this->data['filePath'], false, $this->data['shareInfo']['id']);
		if (!$weblinkInfo) {
			echo "Failed to setup weblink";
			exit();
		}
		$this->data['fileURL'] = $this->weblinks->getURL(array("id_rnd" => $weblinkInfo['id_rnd']));
		$this->data['saveURL'] = $this->weblinks->getSaveURL($weblinkInfo['id_rnd'], false, "pixlr");

		\FileRun\Log::add(false, "preview", array(
			"relative_path" => $this->data['relativePath'],
			"full_path" => $this->data['filePath'],
			"method" => "Pixlr"
		));

		$url = "https://apps.pixlr.com/editor/";
		//$url .= "?method=POST";
		$url .= "?image=".urlencode($this->data['fileURL']);
		$url .= "&referrer=".urlencode($config['settings']['app_title']);
		$url .= "&target=".urlencode($this->data['saveURL']);
		$url .= "&title=".urlencode($this->data['fileName']);
		$url .= "&redirect=false";
		$url .= "&locktitle=true";
		$url .= "&locktype=true";
		header('Location: '.$url);
	}
	function createBlankFile() {
		global $myfiles;
		\FileRun::checkPerms("upload");
		if (file_exists($this->data['filePath'])) {
			jsonOutput([
				"rs" => false,
				"msg" => \FileRun\Lang::t('A file with the specified name already exists. Please try again.', $this->localeSection)
			]);
		}
		$blankFilePath = gluePath($this->path, "blank.png");
		$rs = $myfiles->newFile(\FM::dirname($this->data['relativePath']), $this->data['fileName'], $blankFilePath);
		if ($rs) {
			jsonOutput([
				"rs" => true,
				'path' => $this->data['relativePath'],
				"filename" => $this->data['fileName'],
				"msg" => \FileRun\Lang::t("Blank image created successfully", $this->localeSection)
			]);
		} else {
			jsonOutput([
				"rs" => false,
				"msg" => $myfiles->error['msg']
			]);
		}
	}
	function getNewFileContents() {
		return file_get_contents(S::fromHTML($_GET['image']));
	}
}