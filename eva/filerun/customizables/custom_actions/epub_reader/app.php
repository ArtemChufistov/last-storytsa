<?php
class custom_epub_reader {
	static $localeSection = "Custom Actions: E-book Reader";
	function init() {
		$this->JSconfig = [
			"title" => self::t('E-book Reader'),
			'iconCls' => 'fa fa-fw fa-book',
			'extensions' => ['epub'],
			'popup' => true,
			"requires" => ["download"]
		];
	}
	static function t($text, $vars = false) {
		return \FileRun\Lang::t($text, self::$localeSection, $vars);
	}
	function run() {
		require($this->path."/display.php");
	}

	function stream() {
		\FileRun::checkPerms("download");
		\FileRun\Utils\Downloads::sendFileToBrowser($this->data['filePath']);
		\FileRun\Log::add(false, "preview", [
			"relative_path" => $this->data['relativePath'],
			"full_path" => $this->data['filePath'],
			"method" => "E-book Reader"
		]);
		exit();
	}
}