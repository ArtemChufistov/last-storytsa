<?php
/*
	Edit image files with Adobe Creative Cloud
*/
class custom_creative_cloud {
	var $online = true;
	static $localeSection = "Custom Actions: Creative Cloud";
	function init() {
		$this->JSconfig = [
			"title" => self::t("Creative Cloud"),
			"iconCls" => 'fa fa-fw fa-cloud', 'icon' => 'images/icons/creative_cloud.png',
			"extensions" => ["jpg", "jpeg", "png"],
			"popup" => true,
			"requires" => ["download"]
		];
		$this->outputName = "imageOutput";
	}
	static function t($text, $vars = false) {
		return \FileRun\Lang::t($text, self::$localeSection, $vars);
	}
	function run() {
		\FileRun::checkPerms("download");
		$weblinkInfo = $this->weblinks->createForService($this->data['filePath'], false, $this->data['shareInfo']['id']);
		if (!$weblinkInfo) {
			echo "Failed to setup weblink";
			exit();
		}
		$this->data['fileURL'] = $this->weblinks->getURL(["id_rnd" => $weblinkInfo['id_rnd']]);
		$this->data['saveURL'] = $this->weblinks->getSaveURL($weblinkInfo['id_rnd'], false, "creative_cloud");
		require($this->path."/display.php");
		\FileRun\Log::add(false, "preview", [
			"relative_path" => $this->data['relativePath'],
			"full_path" => $this->data['filePath'],
			"method" => "Creative Cloud"
		]);
	}
	
	function getNewFileContents() {
		$fromURL = S::fromHTML($_REQUEST['fromURL']);
		if (!$fromURL) {
			echo 'No URL specified';
			exit();
		}
		return file_get_contents($fromURL);
	}
}