<?php
global $config, $settings;
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title></title>
	<link rel="stylesheet" href="css/style.css?v=<?php echo $settings->currentVersion;?>" />
	<link rel="stylesheet" href="css/ext.php?v=<?php echo $settings->currentVersion;?>" />
	<link rel="stylesheet" href="customizables/custom_actions/audio_player/style.css?v=<?php echo $settings->currentVersion;?>" />
</head>
<body id="theBODY">
<div id="loadMsg"><div><?php echo \FileRun\Lang::t("Loading audio player...", $this->localeSection)?></div></div>
<script src="js/min.php?extjs=1&v=<?php echo $settings->currentVersion;?>"></script>
<script src="customizables/custom_actions/audio_player/js/app.js?v=<?php echo $settings->currentVersion;?>"></script>
<script src="?module=fileman&section=utils&page=translation.js&sec=<?php echo S::forURL("Custom Actions: Audio Player")?>&lang=<?php echo S::forURL(\FileRun\Lang::getCurrent())?>"></script>
<script src="customizables/custom_actions/audio_player/js/howler.min.js?v=<?php echo $settings->currentVersion;?>"></script>
<script src="customizables/custom_actions/audio_player/js/aurora.js?v=<?php echo $settings->currentVersion;?>"></script>
<script src="customizables/custom_actions/audio_player/js/alac.js?v=<?php echo $settings->currentVersion;?>"></script>
<script src="customizables/custom_actions/audio_player/js/flac.js?v=<?php echo $settings->currentVersion;?>"></script>
<script src="customizables/custom_actions/audio_player/js/aac.js?v=<?php echo $settings->currentVersion;?>"></script>
<script>
	var URLRoot = '<?php echo S::safeJS($config['url']['root'])?>';
</script>
</body>
</html>