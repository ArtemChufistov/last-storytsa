<?php
class custom_audio_player {
	var $localeSection = "Custom Actions: Audio Player";
	function init() {
		$this->JSconfig = [
			"title" => \FileRun\Lang::t("Audio Player", $this->localeSection),
			'iconCls' => 'fa fa-fw fa-music',
			'useWith' => ['nothing'],
			"requires" => ["download"]
		];
	}
	function run() {
		require($this->path."/display.php");
	}

	function stream() {
		\FileRun::checkPerms("download");
		\FileRun\Utils\Downloads::sendFileToBrowser($this->data['filePath']);
		if (\FileRun\Utils\Downloads::$bytesSentToBrowser > 1610612736) {
			//todo: downloads, especially for FLAC happen in many small chunks and this doesn't get to log the action
			\FileRun\Log::add(false, "preview", [
				"relative_path" => $this->data['relativePath'],
				"full_path" => $this->data['filePath'],
				"method" => "Audio Player"
			]);
		}
		exit();
	}
}