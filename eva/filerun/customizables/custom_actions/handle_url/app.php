<?php
class custom_handle_url {
    var $localeSection = 'Custom Actions: Link Opener';
	function init() {
		$this->JSconfig = [
			'title' => \FileRun\Lang::t('Link Opener', $this->localeSection),
			'iconCls' => 'fa fa-fw fa-share-square-o',
			'extensions' => ['url'], 'popup' => true,
            'requires' => ['download']
		];
	}
	function run() {
		\FileRun::checkPerms("download");
		$c = file($this->data['filePath']);
		foreach ($c as $r) {
			if (stristr($r, 'URL=') !== false) {
				header('Location: '.str_ireplace(['URL=', '\''], [''], $r));
				exit();
			}
		}
	}
}
