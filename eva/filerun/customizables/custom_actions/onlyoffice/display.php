<?php
$mode = 'view';
if (in_array($extension, $this->canEdit)) {
	$mode = 'edit';
}

global $auth;
$author = \FileRun\Users::formatFullName($auth->currentUserInfo);

$fileSize = \FM::getFileSize($this->data['filePath']);
$fileModifTime = filemtime($this->data['filePath']);
$documentKey = substr($fileSize.md5($this->data['filePath']), 0, 12).substr($fileModifTime,2,10);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><?php echo \S::safeHTML(\S::forHTML($this->data['fileName']));?></title>
	<style>
		body {
			border: 0;
			margin: 0;
			padding: 0;
			overflow:hidden;
		}
	</style>
</head>

<body>
<div id="placeholder"></div>
<script type="text/javascript" src="<?php echo self::getSetting('serverURL');?>/web-apps/apps/api/documents/api.js"></script>
<script>
	var innerAlert = function (message) {
		if (console && console.log)
			console.log(message);
	};

	var onReady = function () {
		innerAlert("Document editor ready");
	};

	var onDocumentStateChange = function (event) {
		var title = document.title.replace(/\*$/g, "");
		document.title = title + (event.data ? "*" : "");
	};

	var onError = function (event) {
		if (event) innerAlert(event.data);
	};
	var docEditor = new DocsAPI.DocEditor("placeholder", {
		"documentType": "<?php echo $docType;?>",
		"type": "desktop",
		"document": {
			"fileType": "<?php echo $extension;?>",
			"key": "<?php echo $documentKey;?>",
			"title": "<?php echo \S::safeJS($this->data['fileName']);?>",
			"url": "<?php echo \S::safeJS($url);?>",
			"info": {
				"author": "<?php echo \S::safeJS($author);?>"
			}
		},
		"editorConfig": {
			"mode": '<?php echo $mode;?>',
			"lang": '<?php echo $this->getShortLangName(\FileRun\Lang::getCurrent());?>',
			"callbackUrl": "<?php echo \S::safeJS($saveURL);?>",
			"user": {
				"firstname": "<?php echo \S::safeJS($auth->currentUserInfo['name']);?>",
				"id": "<?php echo \S::safeJS($auth->currentUserInfo['id']);?>",
				"lastname": "<?php echo \S::safeJS($auth->currentUserInfo['name2']);?>"
			}
		},
		"customization": {
			'about': false,
			'comments': false,
			'feedback': false,
			'goback': false
		},
		"events": {
			'onReady': onReady,
			'onDocumentStateChange': onDocumentStateChange,
			'onError': onError
		}
	});
</script>
</body>
</html>