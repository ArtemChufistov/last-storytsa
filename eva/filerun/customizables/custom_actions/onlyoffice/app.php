<?php

class custom_onlyoffice {

	var $online = true;
	var $outputName = 'content';
	var $canEdit = [
		'doc', 'docx', 'odt', 'rtf', 'txt',
		'xls', 'xlsx', 'ods', 'csv',
		'ppt', 'pptx', 'odp'
	];

	function init() {
		$this->settings = [
			[
				'key' => 'serverURL',
				'title' => self::t('ONLYOFFICE server URL'),
				'comment' => self::t('Download and install %1', ['<a href="https://github.com/ONLYOFFICE/DocumentServer" target="_blank">ONLYOFFICE DocumentServer</a>'])
			]
		];
		$this->JSconfig = [
			"title" => self::t("ONLYOFFICE"),
			"popup" => true,
			'icon' => 'images/icons/onlyoffice.png',
			"loadingMsg" => self::t('Loading document in ONLYOFFICE. Please wait...'),
			'useWith' => ['office'],
			"requires" => ["download", "create"],
			"requiredUserPerms" => ["download", "upload"],
			"createNew" => [
				"title" => self::t("Document with ONLYOFFICE"),
				"options" => [
					[
						"fileName" => self::t("New Document.docx"),
						"title" => self::t("Word Document"),
						"iconCls" => 'fa fa-fw fa-file-word-o'
					],
					[
						"fileName" => self::t("New Spreadsheet.xlsx"),
						"title" => self::t("Spreadsheet"),
						"iconCls" => 'fa fa-fw fa-file-excel-o'
					],
					[
						"fileName" => self::t("New Presentation.pptx"),
						"title" =>  self::t("Presentation"),
						"iconCls" => 'fa fa-fw fa-file-powerpoint-o'
					]
				]
			]
		];
	}

	function isDisabled() {
		return (strlen(self::getSetting('serverURL')) == 0);
	}

	static function getSetting($k) {
		global $settings;
		$key = 'plugins_onlyoffice_'.$k;
		return $settings->{$key};
	}

	static function t($text, $vars = false) {
		$section = 'Custom Actions: ONLYOFFICE';
		return \FileRun\Lang::t($text, $section, $vars);
	}

	function getNewFileContents() {
		$body_stream = file_get_contents("php://input");
		if ($body_stream === false) {return false;}
		$this->POST = json_decode($body_stream, true);
		if ($this->POST["status"] != 2) {return false;}
		return file_get_contents($this->POST["url"]);
	}

	function saveFeedback($success, $message) {
		if ($this->POST["status"] != 2) {
			//ONLYOFFICE makes various calls to the save URL
			exit('{"error":0}');
		}
		if ($success) {
			exit('{"error":0}');
		} else {
			exit($message);
		}
	}
	
	function createBlankFile() {
		global $myfiles;
		if (!\FileRun\Perms::check('upload')) {exit();}
		$ext = \FM::getExtension($this->data['fileName']);
		if (!in_array($ext, $this->canEdit)) {
			jsonOutput([
				"rs" => false,
				"msg" => self::t('The file extension needs to be one of the following: %1', [implode(', ', $this->canEdit)])
			]);
		}
		if (file_exists($this->data['filePath'])) {
			jsonOutput([
				"rs" => false,
				"msg" => self::t('A file with the specified name already exists. Please try again.')
			]);
		}
		$src = gluePath($this->path, 'blanks/blank.'.$ext);
		$rs = $myfiles->newFile(\FM::dirname($this->data['relativePath']), $this->data['fileName'], $src);
		if ($rs) {
			jsonOutput([
				"rs" => true,
				'path' => $this->data['relativePath'],
				"filename" => $this->data['fileName'],
				"msg" => self::t("Blank file created successfully")
			]);
		} else {
			jsonOutput([
				"rs" => false,
				"msg" => $myfiles->error['msg']
			]);
		}
	}

	function run() {
		\FileRun::checkPerms("download");
		if (\FileRun\Perms::check('upload') && (!$this->data['shareInfo'] || ($this->data['shareInfo'] && $this->data['shareInfo']['perms_upload']))) {
			$weblinkInfo = $this->weblinks->createForService($this->data['filePath'], false, $this->data['shareInfo']['id']);
			if (!$weblinkInfo) {
				echo "Failed to setup saving weblink";
				exit();
			}
			$saveURL = $this->weblinks->getSaveURL($weblinkInfo['id_rnd'], false, "onlyoffice");
		} else {
			$saveURL = "";
		}

		$extension = \FM::getExtension($this->data['fileName']);

		if (in_array($extension, ['docx', 'doc','odt','txt','rtf','html','htm','mht','epub','pdf','djvu','xps'])) {
			$docType = 'text';
		} else if (in_array($extension, ['xlsx','xls','ods','csv'])) {
			$docType = 'spreadsheet';
		} else {
			$docType = 'presentation';
		}

		$url = $this->weblinks->getURL(['id_rnd' => $weblinkInfo['id_rnd'], 'download' => 1]);
		if (!$url) {
			exit("Failed to setup weblink");
		}

		require($this->path."/display.php");

		\FileRun\Log::add(false, "preview", [
			"relative_path" => $this->data['relativePath'],
			"full_path" => $this->data['filePath'],
			"method" => "ONLYOFFICE"
		]);
	}

	function getShortLangName($langName) {
		$codes = [
			'basque' => 'eu',
			'brazilian portuguese' => 'pt',
			'chinese traditional' => 'zh',
			'chinese' => 'zh',
			'danish' => 'da',
			'dutch' => 'nl',
			'english' => 'en',
			'finnish' => 'fi',
			'french' => 'fr',
			'german' => 'de',
			'italian' => 'it',
			'polish' => 'pl',
			'romanian' => 'ro',
			'russian' => 'ru',
			'spanish' => 'es',
			'swedish' => 'sv',
			'turkish' => 'tr'
		];
		return $codes[$langName];
	}
}