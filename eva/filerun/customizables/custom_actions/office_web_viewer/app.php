<?php
class custom_office_web_viewer {
	var $online = true;
	function init() {
		$this->URL = "https://view.officeapps.live.com/op/view.aspx";
		$this->JSconfig = [
			"title" => \FileRun\Lang::t("Office Web Viewer", 'Custom Actions: Office Web Viewer'),
			"icon" => 'images/icons/office.png',
			"extensions" => [
				"doc", "docx", "docm", "dotm", "dotx",
				"xls", "xlsx", "xlsb", "xls", "xlsm",
				"ppt", "pptx", "ppsx", "pps", "pptm", "potm", "ppam", "potx", "ppsm"
			],
			"popup" => true,
			"requires" => ["download"]
		];
	}
	function run() {
		\FileRun::checkPerms("download");
		$url = $this->weblinks->getOneTimeDownloadLink($this->data['filePath']);
		if (!$url) {
			exit("Failed to setup weblink");
		}
		\FileRun\Log::add(false, "preview", [
			"relative_path" => $this->data['relativePath'],
			"full_path" => $this->data['filePath'],
			"method" => "Office Web Viewer"
		]);
?>
<html>
<head>
	<title><?php echo \S::safeHTML(\S::forHTML($this->data['fileName']));?></title>
	<style>
		body {border:0; margin:0; padding:0; overflow:hidden;}
	</style>
</head>
<body>
<iframe scrolling="no" width="100%" height="100%" border="0" src="<?php echo $this->URL?>?src=<?php echo urlencode($url)?>"></iframe>
</body>
</html>
<?php
	}
}