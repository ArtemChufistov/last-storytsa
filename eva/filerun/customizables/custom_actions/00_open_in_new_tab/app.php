<?php
class custom_00_open_in_new_tab {
	function init() {
		$this->JSconfig = [
			"title" => \FileRun\Lang::t('New tab', 'Custom Actions'),
			'iconCls' => 'fa fa-fw fa-external-link',
			'folder' => true, 'newTab' => true,
			'requires' => ['download', 'not-homefolder']
		];
	}
	function run() {
		global $config;
		\FileRun::checkPerms("download");
		if (is_file($this->data['filePath'])) {
			header('Location: '.$config['url']['root'].'?module=custom_actions&action=open_in_browser&path='.\S::forURL($this->data['relativePath']));
			exit();
		} else {
			$path = $this->data['relativePath'];
			if (mb_substr($path, 0, 6) == '/ROOT/') {
				$path = mb_substr($path, 6);
			}
			$parts = explode('/', $path);
			$encodedParts = [];
			foreach($parts as $part) {
				$encodedParts[] = \S::forURL($part);
			}
			$path = '/'.implode('/', $encodedParts);
			header('Location: '.$config['url']['root'].'#'.$path);
			exit();
		}
	}
}