<?php
class custom_code_editor {
	static $localeSection = "Custom Actions: Text Editor";
	function init() {

		$this->JSconfig = [
			"title" => self::t("Text Editor"),
			'iconCls' => 'fa fa-fw fa-file-text-o',
			'useWith' => ['txt', 'noext'],
			"popup" => true,
			"createNew" => [
				"title" => self::t("Text File"),
				"options" => [
					[
						'fileName' => self::t('New Text File.txt'),
						'title' => self::t('Plain Text'),
						'iconCls' => 'fa fa-fw fa-file-text-o',
					],
					[
						'fileName' => 'index.html',
						'title' => self::t('HTML'),
						'iconCls' => 'fa fa-fw fa-file-code-o',
					],
					[
						'fileName' => 'script.js',
						'title' => self::t('JavaScript'),
						'iconCls' => 'fa fa-fw fa-file-code-o',
					],
					[
						'fileName' => 'style.css',
						'title' => self::t('CSS'),
						'iconCls' => 'fa fa-fw fa-file-code-o',
					],
					[
						'fileName' => 'index.php',
						'title' => self::t('PHP'),
						'iconCls' => 'fa fa-fw fa-file-code-o',
					],
					[
						'fileName' => 'readme.md',
						'title' => self::t('Markdown'),
						'iconCls' => 'fa fa-fw fa-file-code-o',
					],
					[
						'fileName' => '',
						'title' => self::t('Other..'),
						'iconCls' => 'fa fa-fw fa-file-text-o'
					]
				]
			],
			"requiredUserPerms" => ["download", "upload"],
			'requires' => ['download']
		];
	}
	static function t($text, $vars = false) {
		return \FileRun\Lang::t($text, self::$localeSection, $vars);
	}
	function run() {
		\FileRun::checkPerms("download");
		$this->data['fileContents'] = file_get_contents(S::forFS($this->data['filePath']));
		$enc = mb_list_encodings();
		if ($_REQUEST['charset'] && in_array($_REQUEST['charset'], $enc)) {
			$this->data['fileContents'] = S::convert2UTF8($this->data['fileContents'], $_REQUEST['charset']);
		}
		require($this->path."/display.php");
		\FileRun\Log::add(false, "preview", [
			"relative_path" => $this->data['relativePath'],
			"full_path" => $this->data['filePath'],
			"method" => "Code Editor"
		]);
	}
	function saveChanges() {
		global $myfiles;
		\FileRun::checkPerms("upload");
		$textContents = S::fromHTML($_POST['textContents']);
		$charset = S::fromHTML($_POST['charset']);
		if ($charset != 'UTF-8') {
			$textContents = S::convertEncoding($textContents, 'UTF-8', $charset);
		}
		$rs = $myfiles->newFile(\FM::dirname($this->data['relativePath']), $this->data['fileName'], false, $textContents);
		if ($rs) {
			jsonOutput([
				"rs" => true,
				"filename" => $this->data['fileName'],
				"msg" => self::t("File successfully saved")
			]);
		} else {
			jsonOutput([
				"rs" => false,
				"msg" => $myfiles->error['msg']
			]);
		}
	}
	function createBlankFile() {
		global $myfiles;
		 \FileRun::checkPerms("upload");
		if (strlen($this->data['fileName']) == 0) {
			jsonOutput([
				"rs" => false,
				"msg" => self::t('Please type a file name')
			]);
		} else {
			if (is_file($this->data['filePath'])) {
				jsonOutput([
					"rs" => false,
					"msg" => self::t('A file with that name already exists')
				]);
			}
			$rs = $myfiles->newFile(\FM::dirname($this->data['relativePath']), $this->data['fileName'], false, "");
			if ($rs) {
				jsonOutput([
					"rs" => true,
					'path' => $this->data['relativePath'],
					"filename" => $this->data['fileName'],
					"msg" => self::t("File successfully created")
				]);
			} else {
				jsonOutput([
					"rs" => false,
					"msg" => $myfiles->error['msg']
				]);
			}
		}
	}
}