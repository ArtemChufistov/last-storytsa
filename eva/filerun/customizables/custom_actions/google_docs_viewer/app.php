<?php

class custom_google_docs_viewer {
	var $online = true;
	function init() {
		$this->URL = "https://docs.google.com/viewer";
		$this->JSconfig = [
			"title" => \FileRun\Lang::t("Google Docs Viewer", 'Custom Actions: Google Docs Viewer'),
			'icon' => 'images/icons/gdocs.png',
			"extensions" => [
				"pdf", "ppt", "pptx", "doc", "docx", "xls", "xlsx", "dxf", "ps", "eps", "xps",
				"psd", "tif", "tiff", "bmp", "svg",
				"pages", "ai", "dxf", "ttf"
			],
			"popup" => true,
			"requires" => ["download"]
		];
	}

	function run() {
		\FileRun::checkPerms("download");
		$url = $this->weblinks->getOneTimeDownloadLink($this->data['filePath']);
		if (!$url) {
			echo "Failed to setup weblink";
			exit();
		}
		\FileRun\Log::add(false, "preview", [
			"relative_path" => $this->data['relativePath'],
			"full_path" => $this->data['filePath'],
			"method" => "Google Docs Viewer"
		]);
?>
<html>
<head>
<title><?php echo $this->JSconfig['title'];?></title>
<style>body {  border: 0;  margin: 0;  padding: 0;  overflow:hidden;  }</style>
</head>
<body>
<iframe scrolling="no" width="100%" height="100%" border="0" src="<?php echo $this->URL?>?url=<?php echo urlencode($url)?>&embedded=true">
</iframe> 
</body>
</html>
<?php
	}
}