<?php
class custom_contact_sheet {
	static $localeSection = 'Custom Actions: Photo Proof Sheet';
	function init() {
		$this->JSconfig = [
			'title' => self::t('Create photo proof sheet'),
			'iconCls' => 'fa fa-fw fa-th',
			"ajax" => true,
			'width' => 400, 'height' => 400,
			'requires' => ['multiple', 'download', 'upload']
		];
	}
	function isDisabled() {
		global $settings;
		return !$settings->thumbnails_imagemagick;
	}
	static function t($s, $v = false) {
		return \FileRun\Lang::t($s, self::$localeSection, $v);
	}
	function run() {
		global $settings, $fm, $myfiles, $config;
		\FileRun::checkPerms("download");
		\FileRun::checkPerms("upload");
		if (stripos($this->data['relativePath'], '/ROOT/HOME') === false) {
			$this->data['relativePath'] = '/ROOT/HOME';
		}
		if (sizeof($_POST['paths']) < 2) {
			exit('You need to select at least two files');
		}
		$imagemagick_convert = $settings->thumbnails_imagemagick_path;
		$filename = \FM::basename($imagemagick_convert);
		$graphicsMagick = false;
		if (in_array($filename, ['gm', 'gm.exe'])) {
			$graphicsMagick = true;
			$imagemagick_convert = $imagemagick_convert.' convert';
		}

		$cmd = str_replace('convert', 'montage', $imagemagick_convert);
		$cmd .= " -label \"%f\" -font Arial -pointsize 20 -background \"#ffffff\" -fill \"black\" -strip -define jpeg:size=600x500 -geometry 600x500+2+2";

		if (!$graphicsMagick) {
			if ($config['imagemagick_limit_resources']) {
				$cmd .= " -limit area 20mb";
				$cmd .= " -limit disk 500mb";
			}
			if (!$config['imagemagick']['no_auto_orient']) {
				$cmd .= " -auto-orient";
			}
		}

		if (sizeof($_POST['paths']) > 8) {
			$cmd .= ' -tile 2x4';
		} else {
			$cmd .= ' -tile 2x';
		}

		foreach ($_POST['paths'] as $relativePath) {
			$relativePath = S::fromHTML($relativePath);
			if (!\FileRun\Files\Utils::isCleanPath($relativePath)) {
				echo 'Invalid path!';
				exit();
			}
			if (\FileRun\Files\Utils::isSharedPath($relativePath)) {
				$pathInfo = \FileRun\Files\Utils::parsePath($relativePath);
				$shareInfo = \FileRun\Share::getInfoById($pathInfo['share_id']);
				if (!$shareInfo['perms_download']) {
					jsonFeedback(false, self::t('You are not allowed to access the requested file!'));
				}
			}
			$filePath = $myfiles->getUserAbsolutePath($relativePath);
			if (!file_exists($filePath)) {
				jsonFeedback(false, self::t('The file you are trying to process is no longer available!'));
			}
			$filename = \FM::basename($relativePath);
			$ext =  \FM::getExtension($filename);
			if ($this->isSupportedImageFile($ext)) {
				$cmd .= ' "'.$filePath;
				if (in_array($ext, ['tiff', 'tif', 'pdf', 'gif', 'ai', 'eps'])) {
					$cmd .= '[0]';
				}
				$cmd .= '"';
			}
		}
		$outputFilename = 'Contact_sheet_'.time().'.jpg';
		$outputPath = $myfiles->getUserAbsolutePath(gluePath($this->data['relativePath'], $outputFilename));
		$cmd .= ' "'.$outputPath.'"';

		if ($fm->os == "win") {
			$cmd .= "  && exit";
		} else {
			$cmd .= " 2>&1";
		}
		$return_text = [];
		$return_code = 0;
		session_write_close();
		exec($cmd, $return_text, $return_code);
		if ($return_code != 0) {
			jsonFeedback(false, self::t('Action failed: %1 %2', array($return_code, implode(',', $return_text))));
		} else {
			\FileRun\MetaTypes::auto($outputPath);
			jsonFeedback(true, self::t('Photo proof sheet successfully created in your home folder.'));
		}
	}

	function isSupportedImageFile($ext) {
		global $settings;
		$ext = strtolower($ext);
		$typeInfo = \FM::fileTypeInfo(false, $ext);
		if ($typeInfo['type'] == "img") {
			return true;
		}
		if ($settings->thumbnails_imagemagick && in_array($ext, explode(",", strtolower($settings->thumbnails_imagemagick_ext)))) {
			return true;
		}
	}
}