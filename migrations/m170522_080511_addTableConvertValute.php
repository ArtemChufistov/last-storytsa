<?php
use yii\db\Schema;
use yii\db\Migration;

class m170522_080511_addTableConvertValute extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%currency_course}}", [
            "id" => Schema::TYPE_PK,
            "from_pay_system_id" => Schema::TYPE_INTEGER.'(12)',
            "to_pay_system_id" => Schema::TYPE_INTEGER.'(12)',
            "from_currency" => Schema::TYPE_INTEGER.'(12)',
            "to_currency" => Schema::TYPE_INTEGER.'(12)',
            "type" => Schema::TYPE_STRING,
            "course" => Schema::TYPE_STRING,
            "course_function" => Schema::TYPE_STRING,
        ], $tableOptions);
    }

    public function safeDown()
    {
        echo "m170522_080511_addTableConvertValute cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170522_080511_addTableConvertValute cannot be reverted.\n";

        return false;
    }
    */
}
