<?php
use yii\db\Schema;
use yii\db\Migration;

class m170223_070939_addImageForNews extends Migration
{
    public function up()
    {
        $this->addColumn('{{%news}}', 'image', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m170223_070939_addImageForNews cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
