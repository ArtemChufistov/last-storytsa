<?php
use yii\db\Schema;
use yii\db\Migration;

class m170306_153321_addWaucher extends Migration
{
    public function up()
    {
       $this->addColumn('{{%currency}}', 'group', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m170306_153321_addWaucher cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
