<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170215_121237_addstructUserRights extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['structUserIndex', AuthItem::TYPE_PERMISSION, 'Управление структурами - список', NULL, time(), time()],
            ['structUserView', AuthItem::TYPE_PERMISSION, 'Управление структурами - просмотр', NULL, time(), time()],
            ['structUserUpdate', AuthItem::TYPE_PERMISSION, 'Управление структурами - редактирование', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'structUserIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'structUserView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'structUserUpdate']]);
    }

    public function down()
    {
        echo "m170215_121237_addstructUserRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
