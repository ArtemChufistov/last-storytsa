<?php

use yii\db\Migration;

/**
 * Class m190311_145657_ToUserDefaultNullPayment
 */
class m190311_145657_ToUserDefaultNullPayment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%payment}}', 'to_user_id');
        $this->addColumn('{{%payment}}', 'to_user_id', $this->integer()->defaultValue(NULL)->after('from_user_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190311_145657_ToUserDefaultNullPayment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_145657_ToUserDefaultNullPayment cannot be reverted.\n";

        return false;
    }
    */
}
