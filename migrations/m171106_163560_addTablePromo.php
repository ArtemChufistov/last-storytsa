<?php

use yii\db\Migration;
use yii\db\Schema;

class m171106_163560_addTablePromo extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable("{{%promo}}", [
            "id" => Schema::TYPE_PK,
            "key" => Schema::TYPE_STRING,
            "name" => Schema::TYPE_STRING,
        ], $tableOptions);

        $this->createTable("{{%user_promo}}", [
            "id" => Schema::TYPE_PK,
            "promo_id" => Schema::TYPE_INTEGER,
            "user_id" => Schema::TYPE_INTEGER,
            "sum" => Schema::TYPE_FLOAT,
            "accured_sum" => Schema::TYPE_FLOAT,
            "params" => Schema::TYPE_TEXT,
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170922_043739_addTablePromo cannot be reverted.\n";

        return false;
    }
}