<?php

use yii\db\Migration;

class m170106_153717_addBuyLevel extends Migration
{
    public function up()
    {
       $this->addColumn('{{%matrix_gift}}', 'buy_level_1', $this->integer()->defaultValue(NULL));
       $this->addColumn('{{%matrix_gift}}', 'buy_level_2', $this->integer()->defaultValue(NULL));
       $this->addColumn('{{%matrix_gift}}', 'buy_level_3', $this->integer()->defaultValue(NULL));
       $this->addColumn('{{%matrix_gift}}', 'buy_level_4', $this->integer()->defaultValue(NULL));
       $this->addColumn('{{%matrix_gift}}', 'buy_level_5', $this->integer()->defaultValue(NULL));
       $this->addColumn('{{%matrix_gift}}', 'buy_level_6', $this->integer()->defaultValue(NULL));
    }

    public function down()
    {
        $this->dropColumn('{{%matrix_gift}}', 'buy_level_1');
        $this->dropColumn('{{%matrix_gift}}', 'buy_level_2');
        $this->dropColumn('{{%matrix_gift}}', 'buy_level_3');
        $this->dropColumn('{{%matrix_gift}}', 'buy_level_4');
        $this->dropColumn('{{%matrix_gift}}', 'buy_level_5');
        $this->dropColumn('{{%matrix_gift}}', 'buy_level_6');

        return false;
    }
}
