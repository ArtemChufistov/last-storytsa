<?php

use yii\db\Migration;

class m170101_194406_addfiledsMatrix extends Migration
{
    public function up()
    {
       $this->addColumn('{{%matrix}}', 'wallet_from', $this->string(255)->notNull());
       $this->addColumn('{{%matrix}}', 'wallet_to', $this->string(255)->notNull());
       $this->addColumn('{{%matrix}}', 'transaction', $this->string(255)->notNull());
       $this->addColumn('{{%matrix}}', 'count_submit', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%matrix}}', 'wallet_from');
        $this->dropColumn('{{%matrix}}', 'wallet_to');
        $this->dropColumn('{{%matrix}}', 'transaction');
        $this->dropColumn('{{%matrix}}', 'count_submit');

        return false;
    }
}
