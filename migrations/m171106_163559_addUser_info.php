<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m171106_163559_addUser_info
 */
class m171106_163559_addUser_info extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%user_info}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'self_invest' => Schema::TYPE_FLOAT . '',
            'struct_invest' => Schema::TYPE_FLOAT . '',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171106_163559_addUser_info cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171106_163559_addUser_info cannot be reverted.\n";

        return false;
    }
    */
}
