<?php

use yii\db\Migration;

class m161223_110941_addTReeModel extends Migration
{
    public function up()
    {
        $this->addColumn('{{%menu}}', 'tree', $this->string(255)->after('id'));
    }

    public function down()
    {
        $this->dropColumn('{{%menu}}', 'tree');
        return false;
    }
}
