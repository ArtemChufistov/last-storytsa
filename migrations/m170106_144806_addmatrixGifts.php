<?php

use yii\db\Migration;

class m170106_144806_addmatrixGifts extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%matrix_gift}}', [
            'id' => $this->primaryKey(),
            'matrix_id' => $this->integer()->defaultValue(NULL),
            'left_level_1' => $this->integer()->defaultValue(NULL),
            'left_level_2' => $this->integer()->defaultValue(NULL),
            'left_level_3' => $this->integer()->defaultValue(NULL),
            'left_level_4' => $this->integer()->defaultValue(NULL),
            'left_level_5' => $this->integer()->defaultValue(NULL),
            'left_level_6' => $this->integer()->defaultValue(NULL),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%matrix_gift}}');

        return false;
    }
}