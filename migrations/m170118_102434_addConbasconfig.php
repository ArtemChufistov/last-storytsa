<?php

use yii\db\Migration;

class m170118_102434_addConbasconfig extends Migration
{
    public function up()
    {
        $this->insert('{{%preference}}',array(
            'key' =>'coinBaseApikey',
            'value' =>'',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'coinBaseApiSecret',
            'value' =>'',
        ));
    }

    public function down()
    {
        echo "m170118_102434_addConbasconfig cannot be reverted.\n";

        return false;
    }
}
