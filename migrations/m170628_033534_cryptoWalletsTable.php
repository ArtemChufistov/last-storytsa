<?php
use yii\db\Schema;
use yii\db\Migration;

class m170628_033534_cryptoWalletsTable extends Migration
{
    public function safeUp()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%crypto_wallet}}", [
            "id" => Schema::TYPE_PK,
            "wallet" => Schema::TYPE_STRING,
            "currency_id" => Schema::TYPE_INTEGER,
            "balance" => Schema::TYPE_FLOAT,
            "info" => Schema::TYPE_STRING,
        ], $tableOptions);

    }

    public function safeDown()
    {
        echo "m170628_033533_cryptoWalletsTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170628_033533_cryptoWalletsTable cannot be reverted.\n";

        return false;
    }
    */
}
