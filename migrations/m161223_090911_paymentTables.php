<?php

use yii\db\Migration;

class m161223_090911_paymentTables extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->defaultValue(NULL),
            'to_user_id' => $this->integer()->defaultValue(NULL),
            'type' => $this->string(255)->notNull(),
            'sum' => $this->float()->defaultValue(0),
            'currency_id' => $this->integer()->notNull(),
            'payment_id' => $this->integer()->defaultValue(NULL),
            'data' => $this->text(),
            'hash' => $this->string(255)->notNull(),
            'date_add' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%payment}}', [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->defaultValue(NULL),
            'to_user_id' => $this->integer()->notNull(),
            'status' => $this->string(255)->notNull(),
            'sum' => $this->float()->defaultValue(0),
            'currency_id' => $this->integer()->notNull(),
            'hash' => $this->string(255)->notNull(),
            'date_add' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%user_balance}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'value' => $this->float()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
         $this->dropTable('{{%payment}}');
         $this->dropTable('{{%transaction}}');
         $this->dropTable('{{%user_balance}}');
         $this->dropTable('{{%currency}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
