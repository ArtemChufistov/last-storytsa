<?php
use lowbase\user\models\AuthItem;
use yii\db\Migration;

class m170123_072341_addMatrixRight extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['matrixCreatePlaceIndex', AuthItem::TYPE_PERMISSION, 'Матрицы - создание', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCreatePlaceIndex']]);
    }

    public function down()
    {
        echo "m170123_072341_addMatrixRight cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
