<?php
use yii\db\Schema;
use yii\db\Migration;

class m170106_155627_addMatrixIdPayment extends Migration
{
    public function up()
    {
       $this->addColumn('{{%payment}}', 'matrix_id', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m170106_155627_addMatrixIdPayment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
