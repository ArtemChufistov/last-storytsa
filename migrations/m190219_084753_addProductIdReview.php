<?php

use yii\db\Migration;

/**
 * Class m190219_084753_addProductIdReview
 */
class m190219_084753_addProductIdReview extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_review}}', 'product_id', $this->integer()->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190219_084753_addProductIdReview cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190219_084753_addProductIdReview cannot be reverted.\n";

        return false;
    }
    */
}
