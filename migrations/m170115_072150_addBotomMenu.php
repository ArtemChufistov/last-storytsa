<?php

use yii\db\Migration;

class m170115_072150_addBotomMenu extends Migration
{
    public function up()
    {
         $this->insert('{{%menu}}',array(
            'tree'=>'100',
            'title' =>'Нижнее меню',
            'link' => '',
            'lft' => '1',
            'rgt' => '14',
            'depth' => '0',
            'name' => 'bottomMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'100',
            'title' =>'Как это работает',
            'link' => '/howitworks',
            'lft' => '2',
            'rgt' => '3',
            'depth' => '1',
            'name' => 'bottomMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'100',
            'title' =>'О Нас',
            'link' => '/about',
            'lft' => '4',
            'rgt' => '5',
            'depth' => '1',
            'name' => 'bottomMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'100',
            'title' =>'Связаться с нами',
            'link' => '/contact',
            'lft' => '6',
            'rgt' => '7',
            'depth' => '1',
            'name' => 'bottomMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'100',
            'title' =>'FAQ',
            'link' => '/faq',
            'lft' => '8',
            'rgt' => '9',
            'depth' => '1',
            'name' => 'bottomMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'100',
            'title' =>'Регистрация',
            'link' => '/signup',
            'lft' => '10',
            'rgt' => '11',
            'depth' => '1',
            'name' => 'bottomMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'100',
            'title' =>'Личный кабинет',
            'link' => '/login',
            'lft' => '12',
            'rgt' => '13',
            'depth' => '1',
            'name' => 'bottomMenu',
            'lang' => 'ru-RU',
          ));

    }

    public function down()
    {
        echo "m170115_072150_addBotomMenu cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
