<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171226_171952_addAmountForAdvTrans
 */
class m171226_171952_addAmountForAdvTrans extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%advcash_transaction}}', 'amount', Schema::TYPE_FLOAT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171226_171952_addAmountForAdvTrans cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171226_171952_addAmountForAdvTrans cannot be reverted.\n";

        return false;
    }
    */
}
