<?php

use yii\db\Migration;

class m170711_052122_changeColumnTypesForPayment extends Migration {
	public function safeUp() {
		$this->dropColumn('{{payment}}', 'comment');

		$this->addColumn('{{payment}}', 'comment', 'VARCHAR(255) DEFAULT \'\' AFTER `to_pay_system_id`');

		$this->dropColumn('{{payment}}', 'transaction_hash');

		$this->addColumn('{{payment}}', 'transaction_hash', 'VARCHAR(255) DEFAULT \'\' AFTER `type`');

		$this->dropColumn('{{payment}}', 'wallet');

		$this->addColumn('{{payment}}', 'wallet', 'VARCHAR(255) DEFAULT \'\' AFTER `transaction_hash`');
	}

	public function safeDown() {
		echo "m170711_052122_changeColumnTypesForPayment cannot be reverted.\n";

		return false;
	}

	/*
		    // Use up()/down() to run migration code without a transaction.
		    public function up()
		    {

		    }

		    public function down()
		    {
		        echo "m170711_052122_changeColumnTypesForPayment cannot be reverted.\n";

		        return false;
		    }
	*/
}
