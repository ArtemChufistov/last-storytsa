<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170518_060034_addInvestRights extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['investPrefIndex', AuthItem::TYPE_PERMISSION, 'Настройки пакетов - список', NULL, time(), time()],
            ['investPrefView', AuthItem::TYPE_PERMISSION, 'Настройки пакетов - просмотр', NULL, time(), time()],
            ['investPrefCreate', AuthItem::TYPE_PERMISSION, 'Настройки пакетов - добавление', NULL, time(), time()],
            ['investPrefUpdate', AuthItem::TYPE_PERMISSION, 'Настройки пакетов - редактирование', NULL, time(), time()],
            ['investPrefDelete', AuthItem::TYPE_PERMISSION, 'Настройки пакетов - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investPrefIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investPrefView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investPrefCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investPrefUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investPrefDelete']]);
    }

    public function safeDown()
    {
        echo "m170518_060034_addInvestRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170518_060034_addInvestRights cannot be reverted.\n";

        return false;
    }
    */
}
