<?php

use yii\db\Migration;

class m170823_052334_mail_template extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('mail_template', ['type', 'content', 'lang'], [
            ['resetPassword', '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
             <meta name="viewport" content="width=device-width, initial-scale=1" />
              <style type="text/css">

              img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
              a img { border: none; }
              table { border-collapse: collapse !important; }
              #outlook a { padding:0; }
              .ReadMsgBody { width: 100%; }
              .ExternalClass {width:100%;}
              .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
              table td {border-collapse: collapse;}
              .ExternalClass * {line-height: 115%;}

              td {
                font-family: Arial, sans-serif;
              }

              body {
                -webkit-font-smoothing:antialiased;
                -webkit-text-size-adjust:none;
                width: 100%;
                height: 100%;
                color: #6f6f6f;
                font-weight: 400;
                font-size: 18px;
              }


              h1 {
                margin: 10px 0;
              }

              a {
                color: #27aa90;
                text-decoration: none;
              }


              .body-padding {
                padding: 0 75px;
              }


              .force-full-width {
                width: 100% !important;
              }


              </style>

              <style type="text/css" media="screen">
                  @media screen {
                    @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
                    /* Thanks Outlook 2013! */
                    * {
                      font-family: \'Source Sans Pro\', \'Helvetica Neue\', \'Arial\', \'sans-serif\' !important;
                    }
                  }
              </style>

              <style type="text/css" media="only screen and (max-width: 599px)">
                /* Mobile styles */
                @media only screen and (max-width: 599px) {

                  table[class*="w320"] {
                    width: 320px !important;
                  }

                  td[class*="w320"] {
                    width: 280px !important;
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                  }

                  img[class*="w320"] {
                    width: 250px !important;
                    height: 67px !important;
                  }

                  td[class*="mobile-spacing"] {
                    padding-top: 10px !important;
                    padding-bottom: 10px !important;
                  }

                  *[class*="mobile-hide"] {
                    display: none !important;
                    width: 0 !important;
                  }

                  *[class*="mobile-br"] {
                    font-size: 12px !important;
                  }

                  td[class*="mobile-center"] {
                    text-align: center !important;
                  }

                  table[class*="columns"] {
                    width: 100% !important;
                  }

                  td[class*="column-padding"] {
                    padding: 0 50px !important;
                  }

                }
              </style>
            </head>
            <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
            <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
              <tr>
                <td align="center" valign="top" style="background-color:#eeebeb" width="100%">

                <center>

                  <table cellspacing="0" cellpadding="0" width="600" class="w320">
                    <tr>
                      <td align="center" valign="top">


                      <table style="margin:0 auto;" bgcolor="#363636" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                          <td style="text-align: center;">
                            <br>
                            <a href="#"><img class="w320" width="286" height="73" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR4AAABJCAYAAADrNUktAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAGUZJREFUeNrsnXt03VWVxz+5aZLaJm1vkUKptJBKUQoVvSgvRyjeAuL4qJg6MPiAxSTgzDCzGJ1UXCprFEh0dIGi2IAgI4o0vmUpJZGXgogN5VUphcbyqDza5rY0j6Z5nPnj7NOc/Pp75v5um8TzXeuum/zu+Z3fOfuc3z5777PP3mVKKfYjZgNHA0uAxcBCYC4wE5gGTAGUfHYDu4BtwGbgGeBJ+TyPg4PDhEXZfmA8RwFnAmcBJwijKQbdwF+Ae4HfAA8De9xQOjg4xlMNfBi4AHgP8AafMgXgdWAQKBOJp0rKVsm1OHgKaAVuB551Q+rg8PfHeN4IXAT8C/Bmn9+fB/4qatRM4FBRv6YBFSlIQj8Bvgmsc0Pr4DD5GU+VMJvPAAs8v/UCD6HtNHOAY4HDU2A0QdgD/AC4BtjkhtjBYXIyntOBrwLv9FzvA+6Ul/944FSgZj/2rQBcDVwr6pyDg8MkYDxVwJeBy4Fyz293oo2/7wbOkbJJ0CuMa1jqrhJ1rGwM7XwA+DSw3g23g8PEZjxHAbeIFGPjeeAbwCHAJWj7TRwMiWT0EjAAzAAORtuBpgOV6K32YqSfy4Db3JA7OExMxpNH21AO9Vz/CfAj4N+AM2LW9TfgfuA1YD5wHNr+U1Wi/l4NfN4Nu4PDxGI85wPfA6Z6rl+BNh5fizYgR6ET+LGoU2cAp5SQ2XhxK1CP8/1xcJgQjOdC4CYgY13rAz4l0s+1RNtguqXcs2gfn2UHqN8/A85zzMfB4cAgE7PceT5M53Xg/ehjD9fFYDrtaKfCN0hdyw5gvz8C/F+C/js4OOxniSeP3qWyVaEe4Gy0cbkpxnM+D9yDdu575zjq/7fQRmcHB4dxxHiOAv7AaLvNMHqL/E0iuYRhF/BP6EOft6N3qcYbLhMG5ODgMA4Yz1TgPuBEz/UG4Gn5LUxV2Srq1GLgh+OYBnuA9wqDdXBw2A8IYxxX+TCdG4FfAD+NuHcb2nnw2HHOdED7CN0MzHLTwcHhwEo8p6FtMjZzeRp4O/BLdIiLIPQI05kDrJlAtPgucKmbEg4OB4bxTAX+hA7WZTCMNgovQXssh+HDwBPyqZ5AtFDAUrRDo4ODQwnhdwyhwcN0AK4HNqK3xMNwjUhEE43pgHYH+AZwMs6/x8GhpPDaaQ4CVnqubQM+J0wlG1LXI2gP5q+jjz5MRLwD+KSbFg4O+1fVugJtVLZxOfp4w2a0IdYPe+SlnQI8ysR2zPurMM4eNz0cHEov8cxgX+PqK6JmXRHCdABuQIeduI6J7w18JNpT28HBYT8wnuVop0AvQ6kEPh5Sx3bgSrSH82mThC6Xsm+MIQcHhxIwnk95futDbzFfQLjH8Q3ADpGKJgvegT4x7+DgUELG8xafF+136Dg5nwi5vxt93ODYSSTtGDh1y8GhxIznHPa14fwAnQMr7FDnncKcLmLynfQ+G/+0PA4ODikxnrM913cBdwlDCssGcavU8YFJSJsj0UHqHRwcUsYUdC6sd3iuP4qOtxMWM+dldED3t6Fj8kxG/APwxyLuz6GN7qCjLnbItxe18gmC13Ezj44j3RGjDXl5ZqfVFj90SJ32fXHhvdf0KSffpq0dCemX99Rt6NQeg+4F+eQC2hc0Bu0xxiOo31lpc631u7dMjnB/OBv2uBUS0M9bPml/ktK5M+l8mYK2zxzk+eEB+X57yM1/APrRoUtjZX/o6u/n7i0vs3HXLsrLynhzTQ1nzD2Ug6dW8VhXgeNnZ3mxp5cXe3qprphCpgy6BwaZVVnJohk1bNrVzc49exgQ36OqTIbK8nL6BgcZVIoyoDKTYXZVFUdUT+e5XbvY0T9SvjKToaq8nN2DQwyo4b3XspWV1Nb4OlqfOEaGU8dInKJWGZw6YJX8v9IzGeuAxoCJZSZBi3VfTupfFjE5GuVjFpC2kJcwL3WtlDKNMRiBwUqr7aZtOelrp7Tf1NcQY0Jjtdfuo6HrSqA55L4muacZWG3Rjoh7DJMPG4+gfjehQ+q2e67VSjuarT7kQl5gG63S9jb5f2HAwmWjXuZZuzXuSfszFjonmi9TRGLx4mGRhOaHPNScaXpXnBl03foNXP+XDZx66CHkDzuUaVOm8Hx3D5f9aS07+vqYV1PDTaeeyMDwMI9u7+KKtevoGRjg8iWLuaD2CPqHhnjvmt8xLZNh8ewsGcpYv2MHGws7eNchc5g3fRoDw8M8+MqrvDWb5YH35Tmn7T6Gh4dZctBsyiljw+s7eXp7gdycg5lfPZ3B4WEefnUrc6un8+gHzvZr9ltFKkySl6teBqRBJg6eFXEVOqPGQs+AdIRImLXyAq2WMs0yqKvQ+egLAfc0StkOz+Tye/Gz0u7VUqdfW1TI/eYFapOXZYWnXSulPW1Cm5YxMvWC1NMa4yVEnlMfwXhq5WVriDkefgw+7zOmZpxWW/+vDGCw7REveae0sTnGotceIM0sKyGdk80XpVSLGo0BpdThSqlTVDhOVEqhlHo8opy65KFHFKu+r366+YV9fuvq71cn/eq36sw194y6vvjndypu+aHa2rdbKaXUlp5e9dF7fq+6+vv3lvniuicU375R/fqFl/ZeW1/Yoc695wH1t55eVXfv79WrfX17f2t6Yr3i+hvV6s7Ne6899/ou9cH2+1T3wIBf03copQ6Tfsb51CqlupRS+ZAyWaXUJqXUKutao1KqLaLunLSp1qqnSynVFFC+zadOFaNtXUqp+oDfo+7fFNIe86mXZ9TGoKf3eY1Wv9aG3NcmZW261YWUb5Q2JRkP+xNGM1PfppjtDaLDqog6zPwzZduK6M9Y6JxovmR8dL9t6LQzYTrhDuA59EHQuWFs8OZnN/Hdjsf4bO5tfGTB4fsus5WV3Hb6u5leMfq8amUmQ0UmA2Vai+sfHuLiRQvJVo5svg0rBWVlDFnHPo6ZNZPza49g+549XHRULXOmTg0tv7Cmmo8vPJK+wSG/5s9A5wgjwcrXHqFKFGTVqk+40tv6uqmnwVptve3IeVbwuKtcRwIbhFfSy8ZQaVpCVLm4aPCob1F064iwQdT5SKdJkI2QCtpjSg1E1GEks7AxaE3hWWOhcyJkfBjHK+gEe4eF3Pcq0CUvZaBzYffgIFc+9iQV06fRsCjY/rywppoPHj6PnsHBUTKaSGQALJg+nTPn+fM4b2CP5QsO55iZMzhr3mGxyp+7YD4HTfXNrlNGvHQ9SSdwC8mzotZaIrdtA2gVlSvro2J1jvElGgvyCdSnlogXKI7RtdmyJUWh1WKMfuphrgjVzzC3+ojfi01uUIhBt/oE9rNS0DkR45nhtQHL9xsjGI9CR+0LPMP14KtbebGrwNGzsxxZHR4l4xMLj+QN5cGnFDJlZbHf1LKk5ctCucDMBIwhm/LA+0lTnT6rUtZalVbJRG8eI/PIjXH1zyXYdWmXNtcWQY9mqWdVTEZHwEtbx8iO41ixUmi31pI2SwHDeGoDmA5FMtBi6RwbU9jXSW6nfFdFqFpRZVjXVYDBIeZPn0amLJwNRP1+AFGdgPFA9LZt2P2NAdfNjtOKgJWwQQyYZsfrhAgGEcR06ouQlGoT9N3eHi9GLWiwXvbmiOe1ykvb4sN4WkIM5WFSzEqLkZ4g9DObCwVGdrhaipgXfmqjn7G8LoLpJOnPWOmciPF4g4GZcBBhhyT74lS+bfdugFBJZgJg2gF+fiGGCmRULrMF2hmhDuYDJt6KEkpspYCtCnREtL1VmLPN7IzPTWtI3VHjYpdfKZ9aS3o0Lg0rU5JGWqxx9i5ODTFoFbc/Y6VzbMbjNXkMy3fYFvKQ59tfRxFDcN/Q0ERmPMMJXoIkq7h3qzJsYjTLatUUMrmMGB61Iq0sEXPpTKA62Q52aagCOcJdCwzjKXhoVEew4bdQBJ06LSazUsZtFekYmc0CU289oz5G3YUixz0unWPbeLyo8kg+hJR5PYz5HJedBeXlbNrVPWonaYJhV4LJFteDM0+8RIj2pCnWIFtqtCewbRhv6kJKz/baucKkhXqP9Nda5LPrI8bSqMKdKY2f31yoT6EfadI5FuPp91wzxuatMeweXWEv5umHzuGQ7Ew2bu/S9p4QvLZ7N90Dg+PtZRpC53lPshrFmVx1FqNKsopmxzHjCds58ntZ0zSChrkWeNtojnLUWdeKfXZ9zPFLCy2Wmlgqo3IxdI7FeLo912bJ90sh9x1sMZ7XggrNqqzkS8cfh+rfwxcefTyU6dzybCfK0vrKxNgcZnQuj1GmmPLo82hPJpwQuYgVIceIEXcywfgvrY6hYtaW4EWx7VxBsH168pb6VWy/iRhzc4YrLRW3U+qql3r351yKQ+dYjGe759oc9O7y5pD7DgVqgAG0+38gLj36KC7NHc9dz27i3Hse4LldowWkP23dxjVPrOfD899ETYU+CD+kFNt272ZgYICde4ITPmzp7YWBQV7r2x2rs1t6+2BwkFdilhca9CacECtkEjb5rP71jBwpaBnDZIPSbdWmgRXS5zafFdEc+zBnxwoleL5xeIuSeupSUrOMFGDsOEG7km3yrI6UGUCa/UibzpGM5wXPtUNEonkuZHLMYSRMaiQxv3PSCdx85hk83bWDU+5cw9K72jn//ge54P4HuWPzC/znMW/h6Jlaw1u3vYurHnuS6eXlLJo5g+Yn17Nmy8va69iIIb193PTMczzVtYNFcw/h5y+8yB1/fZ7uwcEQiWoTa7dvZ9HcQ/jNS1v4UedmXh8YiGr6g+xrfI+zAp4gA9Mlk65N/q6XQWsYw1gZm8h4ZjwFRg51mjNpbeit2LXy+wkpv4De56+IKZVGGVvzMvZhn7xV5zLrHjPmmyymsyLlvrZYDCiOGpekP2nQORRlSqmvAJ/3XH+3vHSPEBwI7KPoVMZnA7+N8zAFbNixkxd7eqnIZKitqWZB9fRRZXoGBxkcHqa6ooIyoHdwiCGlmFFZsdfJb8/wMD0Dg0yvmMKUTIb+oSF2Dw0xo6JirzplY2B4mG6r/J6hIfqGhqipqGBKuNr1fuA3RU4QEwYhTmiGOMiWSFooBbLS/6T2rImOvLUoF3DwZTx1Pnr5v6OzS1wP/GvAvd8A/ksm1waSHS2YCNgJLAa2uGni4JAuMsDj7Js508Rf/l3IvaeJLaiAzrM+2fAHx3QcHErHeDrZd8v4ZPQZrPsZObvlxRJ0vBqAH05C2tzupoeDQ+kYzyBwn+f6EWjbTleI1FMBfET+bgOemUR02Urxth0HB4cQxgM6W4QXy+X7ByH3/7NIRv1oe9Bkwd04o6CDQ8lgcqdPB/7C6FCnm4Fj0N67TwKLAupYDvwC7c38BDo7w0RGN3pX73E3PRwcSivx9AA/8VG3zkEbnr8TUsdn0UbmbuBLk4Am3xFm63JqOTiUWOIBvXW8jtF5tO4DlqLPbz0FHB5Qz7nAz4QB3U2R5zgOIJ5G5wg7SvoxXGR9cdKEGIfAKKc6b8T+oDQpSX1mik1jk7PaEpbCx9AjG7OvJgxr3HQw7UXS029cxpKGJqpMFOK0P59gfMJon4/RLuOLFeVsmbP62R5lqrAZD6IyfchT5gx0/qxL0HnS/bBBHtwrKtkjxI/cN14wKBLeEPqc2sYU6jRHB8KyKpgyUR69itGpXtrwD8FhJlgr8YJ6tSV4Ke10Ln4pfExM4A5GTmTbMIcLl4VMchMzZ4XUE9RPL5YVSU8v/MJ9RqWhUYSnDzIxe1pD+r9JvsNS2aiQRcCcXvcGH/OjvZJnhIW5yAtN/TxtTawhO0aPOQdnp2Py6cHo6O+nKKWGPZkW7pPfKiTifBCutur5uJp4+JJSaqG0vSxBRP6oiP+rIyL8t0mWgrUJMy6ERfivled2SZaFpG2PyiZhMkU0BmSqWBXw7LBsB1mhwVr5O24mg7ToGbfuxiLqbIwoUy+ZJNYW8Zx8AB0bA7KOdEVkBslLOe9184zagPnn14ZRWSZsPCRSj43TZBUbAC4jOEDYZ6Qs6J2wr00gaefHwE0i3f2Z5Oez/FBvSR12dkk/tMjvTSn1xxxWTT1WrrRzlUg0fqeiTeiE1gTPzlqSVxoHSNOmZ1poJjx0igkXYkKMjAUmkV/cuDkmGV+SWEFN1lh1Bsw/04b6MOOyjS+wb2jTr6LDZTxE8BH8CuAWRtLBNAK3TQCmczdwBTpr6G5RG0mJ8ZiYu1FZCFKLc+KjGuVSrtNETmyN8ezaGC9QDn2AtIMUItuVmJ5poCNgETK2slaLcdYV0X+TQikboz3NjM5UErVINIaqUSNtaEnCeNajz2HZOMLicv/DSBZRL44EfoSOUKiAi0SaGK9oBy5Ge2AvFCaUFtOB0SeIo15Av1Q1aUg+SSIDxkHcMAxm4uUjmI7JotmQ8tiWgp5pSYydIRKyiULQSnERC1ssw3AcqacQU0KtI37c5WZ5r2IxHoCr0T45NhrQJ9L3ABcAzwfce4ZIPhlRzy4ogbifBn4GnA+cJJPhCXTanrQYT4vPJIhiPqmFlvQw13yKL02SFD4dIRPfMJ2OEjCdUtKzGOQCGHeWfcOXGsZTW+TYx2E8RkKsi8HskqQxCsSUgOu9Igk8AEy1rq9Cb6tvEBvC3fjvXp2H3h26UGxCl6Dj+1wT8sz9iSZpy3lo/6NKYE2Kk8ubxdOsYFFJ7+xUNe2Mv4wPSVP4FAjOAdVkMaaoyVxPdHCvlnFCz7qAl93Y+Rp82lFvzRG7T97g9KWW/o3KFbYdHuYeEnubP4wJ/BltMLaPQswWgixFb5l/FPg5/rmnLkB7RF+EzsP1v9KAb6F9hg4EtgCXiq2qUb6XAl9J8Rn1ll3H+3KsJnpruNUqu5DJd3TD2JyMi0Gb9DXMvtMeod5NBHqGpSkKikFtbCT7K7RpszC61Ywt82ljADPax20hSvr4tjCJS61rx8pA/qNUtlz+n+Vz/3L0MYwL0ccu7gVOBT6Hjvmzv3JWKeBmtJf1EcDX5f/zgG8ykqCwWGQtHbgxZEWMmkgmgdoqio9cV0t6Ef+SpvAJevYKi5GskL6GTfbOIqWVNOkZx7bUHrIoedPc5C0VttFnPtkJHdOyJ4UxRzMeQcn7wtIYLQt492LbeGz8B/Brz7XTgTtEomkHzgrpYA5tjL5Y/t+JtoifhN52313iiXAn8C55/qeAL6KN58bw/XTKYjYhkyTuNmkSnTsKedKL/pckhU+QPaDDI72YyZ6jdNvfadKzGLSwb5obs1AFbUt3jLHNtWNcdExG0cYABtORcPwDRIF4jlPV4nzkxT1KqYOlzPyAMjZ+qZRa4ql7sVLqa0qpzhSdAf+mlPquUurt8oxjpK03iZPgL5RSH0jBSdDPqWpVDKfCuphOaU3i4JVN6EDodf7KpuhA2BjDOY+ANoc5ENZL+fqETntJnPzC6Lk/HAi9dWQD+uxHl2zC53jHKciBMB8yl9t8HAizQsO6GPQKdJjMxORc3ejYO14D7FJZ3Rejg8a/T+wlQU6GHwT+CFwHvFmurRcVaIncf51w1dcTcOleMXp/T9q5UAzaW+XaGuBW4Frg+6Jm/boEtotchPHYqAxxVzCzzTmWXZksI6luCymv2rUx2pTUoGvc/JsoXUD7YuhZChijckuE6hY3d5c9F42vTTHqad5HuilYRuhcRBsC25xkh2mX2GxaxHBssERUqU/LZPsCereriZEQqjamoT2gLxSifl+MvN3AXfIBmCcMZCFwmLxI1egzIz1il3kZfbZlI6PzgJ0sz/iQTPxTxLZ0OzqG9AMlmkQdMURb41sSR/82KkLbGIy3xtEvbcOkySRhzlB5z4PZWVKXjWGy52IYm4tVudoOILPptF7YOIkNC5aKHjWWeWvsG4q0ixmVqymAgZu0PX454c2uZUMgkx+jSnFlgIpzg1LqICmTETFxYwzVaJ1S6iql1FLr/iSfaqXUe5RS11jPe1wp9V6l1Gyl1K1KqYdFzaIEn2zI2SW/j102jirRGKBq+aFLzmnVFdGfOKJ8rXUmy4jlm+TTGKDeNUacW7Np2SZ/t8VUrymSnvtL1WoUGhkVJs5Zupyn7iBskjHJxaR9XNVQBfxWJ2NvxsuUbbPa0Ob3DO/p9CT4EHqr/U2e65vQcXlMHOZq4JPCBZfEqPc1kWA2yOrwsnB9Y4SeKtLPXOG6b5WdNuNP9CBwlahXH0P766wRCagfh1LANmS6yI3xVeHJRKt8xMbKKBTDeEDH52lGb0t7ca/8ZuxCZegcXOehvZvnpdjpzWhP5JvFZnS6PPuNwOXAL908d3AYPyiW8RgsB64MkGjuA24EfsVInvYasbmchj6cuQidFrk8xrP2oB0BnxJbTRs6TGkGHZDsv8XYfQPwZdLz0XFwcBhnjMcYjS8UlWZRgFRyp0gffxQDsUEVsEAkqMPQHtLThBENiGF7K/Ci1POKJUWdgg46/zERX28TVesZN7wODpOf8RhUox3CLkbvLvlhC9o78mF0uNWNaFtOmDNhOTrkxtHo1DunoYOyzxCp5laRchzDcXD4O2Q8Nk4W9ecsUX/KQtSn7Whj2w50PKB+kYSmiQR0sHwb9KK37e9A527f6YbTwcExHhsVwHFou85J8vd8ksVl3oLe6XoE+D36EOs2N4QODo7xxMVsRmw6h4s0UyMSThna87lbJKAtjNh2XkaH23BwcJjA+P8BAAZ2dLKBO1SLAAAAAElFTkSuQmCC" alt="company logo" ></a>
                          <br><br>
                          </td>
                        </tr>
                      </table>

                      <table cellspacing="0" cellpadding="0" class="force-full-width" style="background-color:#3bcdb0;">
                        <tr>
                          <td style="background-color:#3bcdb0;">

                            <table cellspacing="0" cellpadding="0" class="force-full-width">
                              <tr>
                                <td style="font-size:40px; font-weight: 600; color: #ffffff; text-align:center;" class="mobile-spacing">
                                <div class="mobile-br">&nbsp;</div>
                                  Аккаунт успешно создан
                                <br>
                                </td>
                              </tr>
                              <tr>
                                <td style="font-size:24px; text-align:center; padding: 0 75px; color:#6f6f6f;" class="w320 mobile-spacing; ">
                                  Теперь необходимо подтвердить e-mail
                                </td>
                              </tr>
                            </table>

                            <table cellspacing="0" cellpadding="0" width="600" class="force-full-width">
                              <tr>
                                <td>
                                  <img src="https://www.filepicker.io/api/file/4BgENLefRVCrgMGTAENj" style="max-width:100%; display:block;">
                                </td>
                              </tr>
                            </table>

                          </td>
                        </tr>
                      </table>

                      <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff" >
                        <tr>
                          <td style="background-color:#ffffff;">
                            <br>
                            <table class="columns" cellspacing="0" cellpadding="0" width="49%" align="left">
                              <tr>

                              <!-- ############# STEP ONE ############### -->
                              <!-- To change number images to step one:
                                  - Replace image below with this url: https://www.filepicker.io/api/file/acgdn9j9T16oHaZ8znhv
                                  - Then replace step two with this url: https://www.filepicker.io/api/file/iqmbVoMtT7ukbPUoo9zH
                                  - Finally replace step three with this url: https://www.filepicker.io/api/file/ni2yEbRCRJKzRm3cYGnn

                                  Finished!
                               -->
                                <td style="padding-left: 60px; padding-top: 10px;">
                                  <img src="https://www.filepicker.io/api/file/zNDJy10QemuMhAcirOwQ" alt="step one" width="60" height="62">
                                </td>


                                <td style="color:#f3a389; text-align:left; padding-top: 10px;">
                                  Создание аккаунта на сайте
                                </td>
                              </tr>
                              <tr>

                              <!-- ############# STEP TWO ############### -->
                              <!-- To change number images to step two:
                                  - Replace image below with this url: https://www.filepicker.io/api/file/23h1I8Ts2PNLx755Dsfg
                                  - Then replace step one with this url: https://www.filepicker.io/api/file/zNDJy10QemuMhAcirOwQ
                                  - Finally replace step three with this url: https://www.filepicker.io/api/file/ni2yEbRCRJKzRm3cYGnn

                                  Finished!
                               -->
                                <td style="padding-left: 60px; padding-top: 10px;">
                                  <img src="https://www.filepicker.io/api/file/23h1I8Ts2PNLx755Dsfg" alt="step two" width="60" height="65">
                                </td>
                                <td style="color:#f5774e; text-align:left; padding-top: 10px;">
                                  Подтверждение электронной почты
                                </td>
                              </tr>
                              <tr>

                              <!-- ############# STEP THREE ############### -->
                              <!-- To change number images to step three:
                                  - Replace image below with this url: https://www.filepicker.io/api/file/OombIcyT92WWTaHB4vlE
                                  - Then replace step one with this url: https://www.filepicker.io/api/file/zNDJy10QemuMhAcirOwQ
                                  - Finally replace step three with this url: https://www.filepicker.io/api/file/iqmbVoMtT7ukbPUoo9zH

                                  Finished!
                               -->
                                <td style="padding-left: 60px; padding-top: 10px;">
                                  <img src="https://www.filepicker.io/api/file/ni2yEbRCRJKzRm3cYGnn" alt="step three" width="60" height="60">
                                </td>
                                <td  style="color:#f3a389; text-align:left; padding-top: 10px;">
                                  Регистрация завершена!
                                </td>
                              </tr>
                            </table>
                            <table class="columns" cellspacing="0" cellpadding="0" width="49%" align="right">
                              <tr>
                                <td class="column-padding" style="text-align:left; vertical-align:top; padding-left: 20px; padding-right:30px; font-size:16px;">
                                  <br>
                                  <span style="color:#3bcdb0; font-size:18px; font-weight:bold;">Здравствуйте, {{login}}</span><br><br>
                                  Для подтверждения электронной почты и активации аккаунта нажмите на кнопку "Подтвердить e-mail".<br><br>
                                  Если Вы не регистрировались на нашем сайте, просто удалите данное письмо.
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>

                      <table style="margin:0 auto; width:100%" cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff">
                        <tr>
                          <td style="text-align:center; margin:0 auto;">
                          <br>
                            <div><!--[if mso]>
                              <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:220px;" stroke="f" fillcolor="#f5774e">
                                <w:anchorlock/>
                                <center>
                              <![endif]-->
                                  <a href="http://citt.fund/submitemail/{{login}}/{{emailConfirmToken}}"
                                style="background-color:#f5774e;color:#ffffff;display:inline-block;font-family:\'Source Sans Pro\', Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:45px;text-align:center;text-decoration:none;width:220px;-webkit-text-size-adjust:none;">Подтвердить e-mail</a>
                                  <!--[if mso]>
                                </center>
                              </v:rect>
                            <![endif]--></div>
                            <br>
                          </td>
                        </tr>
                      </table>



                      <table cellspacing="0" cellpadding="0" bgcolor="#363636" style="width:100%;" class="force-full-width">
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#e0e0e0; font-size: 14px; text-align:center;">
                            © 2017 Crypto Investment and Token Trading<br>
                          </td>
                        </tr>
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                      </table>









                      </td>
                    </tr>
                  </table>

                </center>




                </td>
              </tr>
            </table>
            </body>
            </html>
            ', 'ru'],
            ['confirmEmailCode', '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
             <meta name="viewport" content="width=device-width, initial-scale=1" />
              <style type="text/css">

              img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
              a img { border: none; }
              table { border-collapse: collapse !important; }
              #outlook a { padding:0; }
              .ReadMsgBody { width: 100%; }
              .ExternalClass {width:100%;}
              .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
              table td {border-collapse: collapse;}
              .ExternalClass * {line-height: 115%;}

              td {
                font-family: Arial, sans-serif;
              }

              body {
                -webkit-font-smoothing:antialiased;
                -webkit-text-size-adjust:none;
                width: 100%;
                height: 100%;
                color: #6f6f6f;
                font-weight: 400;
                font-size: 18px;
              }


              h1 {
                margin: 10px 0;
              }

              a {
                color: #27aa90;
                text-decoration: none;
              }


              .body-padding {
                padding: 0 75px;
              }


              .force-full-width {
                width: 100% !important;
              }


              </style>

              <style type="text/css" media="screen">
                  @media screen {
                    @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
                    /* Thanks Outlook 2013! */
                    * {
                      font-family: \'Source Sans Pro\', \'Helvetica Neue\', \'Arial\', \'sans-serif\' !important;
                    }
                  }
              </style>

              <style type="text/css" media="only screen and (max-width: 599px)">
                /* Mobile styles */
                @media only screen and (max-width: 599px) {

                  table[class*="w320"] {
                    width: 320px !important;
                  }

                  td[class*="w320"] {
                    width: 280px !important;
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                  }

                  img[class*="w320"] {
                    width: 250px !important;
                    height: 67px !important;
                  }

                  td[class*="mobile-spacing"] {
                    padding-top: 10px !important;
                    padding-bottom: 10px !important;
                  }

                  *[class*="mobile-hide"] {
                    display: none !important;
                    width: 0 !important;
                  }

                  *[class*="mobile-br"] {
                    font-size: 12px !important;
                  }

                  td[class*="mobile-center"] {
                    text-align: center !important;
                  }

                  table[class*="columns"] {
                    width: 100% !important;
                  }

                  td[class*="column-padding"] {
                    padding: 0 50px !important;
                  }

                }
              </style>
            </head>
            <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
            <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
              <tr>
                <td align="center" valign="top" style="background-color:#eeebeb" width="100%">

                <center>

                  <table cellspacing="0" cellpadding="0" width="600" class="w320">
                    <tr>
                      <td align="center" valign="top">


                      <table style="margin:0 auto;" bgcolor="#363636" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                          <td style="text-align: center;">
                            <br>
                            <a href="#"><img class="w320" width="286" height="73" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR4AAABJCAYAAADrNUktAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAGUZJREFUeNrsnXt03VWVxz+5aZLaJm1vkUKptJBKUQoVvSgvRyjeAuL4qJg6MPiAxSTgzDCzGJ1UXCprFEh0dIGi2IAgI4o0vmUpJZGXgogN5VUphcbyqDza5rY0j6Z5nPnj7NOc/Pp75v5um8TzXeuum/zu+Z3fOfuc3z5777PP3mVKKfYjZgNHA0uAxcBCYC4wE5gGTAGUfHYDu4BtwGbgGeBJ+TyPg4PDhEXZfmA8RwFnAmcBJwijKQbdwF+Ae4HfAA8De9xQOjg4xlMNfBi4AHgP8AafMgXgdWAQKBOJp0rKVsm1OHgKaAVuB551Q+rg8PfHeN4IXAT8C/Bmn9+fB/4qatRM4FBRv6YBFSlIQj8Bvgmsc0Pr4DD5GU+VMJvPAAs8v/UCD6HtNHOAY4HDU2A0QdgD/AC4BtjkhtjBYXIyntOBrwLv9FzvA+6Ul/944FSgZj/2rQBcDVwr6pyDg8MkYDxVwJeBy4Fyz293oo2/7wbOkbJJ0CuMa1jqrhJ1rGwM7XwA+DSw3g23g8PEZjxHAbeIFGPjeeAbwCHAJWj7TRwMiWT0EjAAzAAORtuBpgOV6K32YqSfy4Db3JA7OExMxpNH21AO9Vz/CfAj4N+AM2LW9TfgfuA1YD5wHNr+U1Wi/l4NfN4Nu4PDxGI85wPfA6Z6rl+BNh5fizYgR6ET+LGoU2cAp5SQ2XhxK1CP8/1xcJgQjOdC4CYgY13rAz4l0s+1RNtguqXcs2gfn2UHqN8/A85zzMfB4cAgE7PceT5M53Xg/ehjD9fFYDrtaKfCN0hdyw5gvz8C/F+C/js4OOxniSeP3qWyVaEe4Gy0cbkpxnM+D9yDdu575zjq/7fQRmcHB4dxxHiOAv7AaLvNMHqL/E0iuYRhF/BP6EOft6N3qcYbLhMG5ODgMA4Yz1TgPuBEz/UG4Gn5LUxV2Srq1GLgh+OYBnuA9wqDdXBw2A8IYxxX+TCdG4FfAD+NuHcb2nnw2HHOdED7CN0MzHLTwcHhwEo8p6FtMjZzeRp4O/BLdIiLIPQI05kDrJlAtPgucKmbEg4OB4bxTAX+hA7WZTCMNgovQXssh+HDwBPyqZ5AtFDAUrRDo4ODQwnhdwyhwcN0AK4HNqK3xMNwjUhEE43pgHYH+AZwMs6/x8GhpPDaaQ4CVnqubQM+J0wlG1LXI2gP5q+jjz5MRLwD+KSbFg4O+1fVugJtVLZxOfp4w2a0IdYPe+SlnQI8ysR2zPurMM4eNz0cHEov8cxgX+PqK6JmXRHCdABuQIeduI6J7w18JNpT28HBYT8wnuVop0AvQ6kEPh5Sx3bgSrSH82mThC6Xsm+MIQcHhxIwnk95futDbzFfQLjH8Q3ADpGKJgvegT4x7+DgUELG8xafF+136Dg5nwi5vxt93ODYSSTtGDh1y8GhxIznHPa14fwAnQMr7FDnncKcLmLynfQ+G/+0PA4ODikxnrM913cBdwlDCssGcavU8YFJSJsj0UHqHRwcUsYUdC6sd3iuP4qOtxMWM+dldED3t6Fj8kxG/APwxyLuz6GN7qCjLnbItxe18gmC13Ezj44j3RGjDXl5ZqfVFj90SJ32fXHhvdf0KSffpq0dCemX99Rt6NQeg+4F+eQC2hc0Bu0xxiOo31lpc631u7dMjnB/OBv2uBUS0M9bPml/ktK5M+l8mYK2zxzk+eEB+X57yM1/APrRoUtjZX/o6u/n7i0vs3HXLsrLynhzTQ1nzD2Ug6dW8VhXgeNnZ3mxp5cXe3qprphCpgy6BwaZVVnJohk1bNrVzc49exgQ36OqTIbK8nL6BgcZVIoyoDKTYXZVFUdUT+e5XbvY0T9SvjKToaq8nN2DQwyo4b3XspWV1Nb4OlqfOEaGU8dInKJWGZw6YJX8v9IzGeuAxoCJZSZBi3VfTupfFjE5GuVjFpC2kJcwL3WtlDKNMRiBwUqr7aZtOelrp7Tf1NcQY0Jjtdfuo6HrSqA55L4muacZWG3Rjoh7DJMPG4+gfjehQ+q2e67VSjuarT7kQl5gG63S9jb5f2HAwmWjXuZZuzXuSfszFjonmi9TRGLx4mGRhOaHPNScaXpXnBl03foNXP+XDZx66CHkDzuUaVOm8Hx3D5f9aS07+vqYV1PDTaeeyMDwMI9u7+KKtevoGRjg8iWLuaD2CPqHhnjvmt8xLZNh8ewsGcpYv2MHGws7eNchc5g3fRoDw8M8+MqrvDWb5YH35Tmn7T6Gh4dZctBsyiljw+s7eXp7gdycg5lfPZ3B4WEefnUrc6un8+gHzvZr9ltFKkySl6teBqRBJg6eFXEVOqPGQs+AdIRImLXyAq2WMs0yqKvQ+egLAfc0StkOz+Tye/Gz0u7VUqdfW1TI/eYFapOXZYWnXSulPW1Cm5YxMvWC1NMa4yVEnlMfwXhq5WVriDkefgw+7zOmZpxWW/+vDGCw7REveae0sTnGotceIM0sKyGdk80XpVSLGo0BpdThSqlTVDhOVEqhlHo8opy65KFHFKu+r366+YV9fuvq71cn/eq36sw194y6vvjndypu+aHa2rdbKaXUlp5e9dF7fq+6+vv3lvniuicU375R/fqFl/ZeW1/Yoc695wH1t55eVXfv79WrfX17f2t6Yr3i+hvV6s7Ne6899/ou9cH2+1T3wIBf03copQ6Tfsb51CqlupRS+ZAyWaXUJqXUKutao1KqLaLunLSp1qqnSynVFFC+zadOFaNtXUqp+oDfo+7fFNIe86mXZ9TGoKf3eY1Wv9aG3NcmZW261YWUb5Q2JRkP+xNGM1PfppjtDaLDqog6zPwzZduK6M9Y6JxovmR8dL9t6LQzYTrhDuA59EHQuWFs8OZnN/Hdjsf4bO5tfGTB4fsus5WV3Hb6u5leMfq8amUmQ0UmA2Vai+sfHuLiRQvJVo5svg0rBWVlDFnHPo6ZNZPza49g+549XHRULXOmTg0tv7Cmmo8vPJK+wSG/5s9A5wgjwcrXHqFKFGTVqk+40tv6uqmnwVptve3IeVbwuKtcRwIbhFfSy8ZQaVpCVLm4aPCob1F064iwQdT5SKdJkI2QCtpjSg1E1GEks7AxaE3hWWOhcyJkfBjHK+gEe4eF3Pcq0CUvZaBzYffgIFc+9iQV06fRsCjY/rywppoPHj6PnsHBUTKaSGQALJg+nTPn+fM4b2CP5QsO55iZMzhr3mGxyp+7YD4HTfXNrlNGvHQ9SSdwC8mzotZaIrdtA2gVlSvro2J1jvElGgvyCdSnlogXKI7RtdmyJUWh1WKMfuphrgjVzzC3+ojfi01uUIhBt/oE9rNS0DkR45nhtQHL9xsjGI9CR+0LPMP14KtbebGrwNGzsxxZHR4l4xMLj+QN5cGnFDJlZbHf1LKk5ctCucDMBIwhm/LA+0lTnT6rUtZalVbJRG8eI/PIjXH1zyXYdWmXNtcWQY9mqWdVTEZHwEtbx8iO41ixUmi31pI2SwHDeGoDmA5FMtBi6RwbU9jXSW6nfFdFqFpRZVjXVYDBIeZPn0amLJwNRP1+AFGdgPFA9LZt2P2NAdfNjtOKgJWwQQyYZsfrhAgGEcR06ouQlGoT9N3eHi9GLWiwXvbmiOe1ykvb4sN4WkIM5WFSzEqLkZ4g9DObCwVGdrhaipgXfmqjn7G8LoLpJOnPWOmciPF4g4GZcBBhhyT74lS+bfdugFBJZgJg2gF+fiGGCmRULrMF2hmhDuYDJt6KEkpspYCtCnREtL1VmLPN7IzPTWtI3VHjYpdfKZ9aS3o0Lg0rU5JGWqxx9i5ODTFoFbc/Y6VzbMbjNXkMy3fYFvKQ59tfRxFDcN/Q0ERmPMMJXoIkq7h3qzJsYjTLatUUMrmMGB61Iq0sEXPpTKA62Q52aagCOcJdCwzjKXhoVEew4bdQBJ06LSazUsZtFekYmc0CU289oz5G3YUixz0unWPbeLyo8kg+hJR5PYz5HJedBeXlbNrVPWonaYJhV4LJFteDM0+8RIj2pCnWIFtqtCewbRhv6kJKz/baucKkhXqP9Nda5LPrI8bSqMKdKY2f31yoT6EfadI5FuPp91wzxuatMeweXWEv5umHzuGQ7Ew2bu/S9p4QvLZ7N90Dg+PtZRpC53lPshrFmVx1FqNKsopmxzHjCds58ntZ0zSChrkWeNtojnLUWdeKfXZ9zPFLCy2Wmlgqo3IxdI7FeLo912bJ90sh9x1sMZ7XggrNqqzkS8cfh+rfwxcefTyU6dzybCfK0vrKxNgcZnQuj1GmmPLo82hPJpwQuYgVIceIEXcywfgvrY6hYtaW4EWx7VxBsH168pb6VWy/iRhzc4YrLRW3U+qql3r351yKQ+dYjGe759oc9O7y5pD7DgVqgAG0+38gLj36KC7NHc9dz27i3Hse4LldowWkP23dxjVPrOfD899ETYU+CD+kFNt272ZgYICde4ITPmzp7YWBQV7r2x2rs1t6+2BwkFdilhca9CacECtkEjb5rP71jBwpaBnDZIPSbdWmgRXS5zafFdEc+zBnxwoleL5xeIuSeupSUrOMFGDsOEG7km3yrI6UGUCa/UibzpGM5wXPtUNEonkuZHLMYSRMaiQxv3PSCdx85hk83bWDU+5cw9K72jn//ge54P4HuWPzC/znMW/h6Jlaw1u3vYurHnuS6eXlLJo5g+Yn17Nmy8va69iIIb193PTMczzVtYNFcw/h5y+8yB1/fZ7uwcEQiWoTa7dvZ9HcQ/jNS1v4UedmXh8YiGr6g+xrfI+zAp4gA9Mlk65N/q6XQWsYw1gZm8h4ZjwFRg51mjNpbeit2LXy+wkpv4De56+IKZVGGVvzMvZhn7xV5zLrHjPmmyymsyLlvrZYDCiOGpekP2nQORRlSqmvAJ/3XH+3vHSPEBwI7KPoVMZnA7+N8zAFbNixkxd7eqnIZKitqWZB9fRRZXoGBxkcHqa6ooIyoHdwiCGlmFFZsdfJb8/wMD0Dg0yvmMKUTIb+oSF2Dw0xo6JirzplY2B4mG6r/J6hIfqGhqipqGBKuNr1fuA3RU4QEwYhTmiGOMiWSFooBbLS/6T2rImOvLUoF3DwZTx1Pnr5v6OzS1wP/GvAvd8A/ksm1waSHS2YCNgJLAa2uGni4JAuMsDj7Js508Rf/l3IvaeJLaiAzrM+2fAHx3QcHErHeDrZd8v4ZPQZrPsZObvlxRJ0vBqAH05C2tzupoeDQ+kYzyBwn+f6EWjbTleI1FMBfET+bgOemUR02Urxth0HB4cQxgM6W4QXy+X7ByH3/7NIRv1oe9Bkwd04o6CDQ8lgcqdPB/7C6FCnm4Fj0N67TwKLAupYDvwC7c38BDo7w0RGN3pX73E3PRwcSivx9AA/8VG3zkEbnr8TUsdn0UbmbuBLk4Am3xFm63JqOTiUWOIBvXW8jtF5tO4DlqLPbz0FHB5Qz7nAz4QB3U2R5zgOIJ5G5wg7SvoxXGR9cdKEGIfAKKc6b8T+oDQpSX1mik1jk7PaEpbCx9AjG7OvJgxr3HQw7UXS029cxpKGJqpMFOK0P59gfMJon4/RLuOLFeVsmbP62R5lqrAZD6IyfchT5gx0/qxL0HnS/bBBHtwrKtkjxI/cN14wKBLeEPqc2sYU6jRHB8KyKpgyUR69itGpXtrwD8FhJlgr8YJ6tSV4Ke10Ln4pfExM4A5GTmTbMIcLl4VMchMzZ4XUE9RPL5YVSU8v/MJ9RqWhUYSnDzIxe1pD+r9JvsNS2aiQRcCcXvcGH/OjvZJnhIW5yAtN/TxtTawhO0aPOQdnp2Py6cHo6O+nKKWGPZkW7pPfKiTifBCutur5uJp4+JJSaqG0vSxBRP6oiP+rIyL8t0mWgrUJMy6ERfivled2SZaFpG2PyiZhMkU0BmSqWBXw7LBsB1mhwVr5O24mg7ToGbfuxiLqbIwoUy+ZJNYW8Zx8AB0bA7KOdEVkBslLOe9184zagPnn14ZRWSZsPCRSj43TZBUbAC4jOEDYZ6Qs6J2wr00gaefHwE0i3f2Z5Oez/FBvSR12dkk/tMjvTSn1xxxWTT1WrrRzlUg0fqeiTeiE1gTPzlqSVxoHSNOmZ1poJjx0igkXYkKMjAUmkV/cuDkmGV+SWEFN1lh1Bsw/04b6MOOyjS+wb2jTr6LDZTxE8BH8CuAWRtLBNAK3TQCmczdwBTpr6G5RG0mJ8ZiYu1FZCFKLc+KjGuVSrtNETmyN8ezaGC9QDn2AtIMUItuVmJ5poCNgETK2slaLcdYV0X+TQikboz3NjM5UErVINIaqUSNtaEnCeNajz2HZOMLicv/DSBZRL44EfoSOUKiAi0SaGK9oBy5Ge2AvFCaUFtOB0SeIo15Av1Q1aUg+SSIDxkHcMAxm4uUjmI7JotmQ8tiWgp5pSYydIRKyiULQSnERC1ssw3AcqacQU0KtI37c5WZ5r2IxHoCr0T45NhrQJ9L3ABcAzwfce4ZIPhlRzy4ogbifBn4GnA+cJJPhCXTanrQYT4vPJIhiPqmFlvQw13yKL02SFD4dIRPfMJ2OEjCdUtKzGOQCGHeWfcOXGsZTW+TYx2E8RkKsi8HskqQxCsSUgOu9Igk8AEy1rq9Cb6tvEBvC3fjvXp2H3h26UGxCl6Dj+1wT8sz9iSZpy3lo/6NKYE2Kk8ubxdOsYFFJ7+xUNe2Mv4wPSVP4FAjOAdVkMaaoyVxPdHCvlnFCz7qAl93Y+Rp82lFvzRG7T97g9KWW/o3KFbYdHuYeEnubP4wJ/BltMLaPQswWgixFb5l/FPg5/rmnLkB7RF+EzsP1v9KAb6F9hg4EtgCXiq2qUb6XAl9J8Rn1ll3H+3KsJnpruNUqu5DJd3TD2JyMi0Gb9DXMvtMeod5NBHqGpSkKikFtbCT7K7RpszC61Ywt82ljADPax20hSvr4tjCJS61rx8pA/qNUtlz+n+Vz/3L0MYwL0ccu7gVOBT6Hjvmzv3JWKeBmtJf1EcDX5f/zgG8ykqCwWGQtHbgxZEWMmkgmgdoqio9cV0t6Ef+SpvAJevYKi5GskL6GTfbOIqWVNOkZx7bUHrIoedPc5C0VttFnPtkJHdOyJ4UxRzMeQcn7wtIYLQt492LbeGz8B/Brz7XTgTtEomkHzgrpYA5tjL5Y/t+JtoifhN52313iiXAn8C55/qeAL6KN58bw/XTKYjYhkyTuNmkSnTsKedKL/pckhU+QPaDDI72YyZ6jdNvfadKzGLSwb5obs1AFbUt3jLHNtWNcdExG0cYABtORcPwDRIF4jlPV4nzkxT1KqYOlzPyAMjZ+qZRa4ql7sVLqa0qpzhSdAf+mlPquUurt8oxjpK03iZPgL5RSH0jBSdDPqWpVDKfCuphOaU3i4JVN6EDodf7KpuhA2BjDOY+ANoc5ENZL+fqETntJnPzC6Lk/HAi9dWQD+uxHl2zC53jHKciBMB8yl9t8HAizQsO6GPQKdJjMxORc3ejYO14D7FJZ3Rejg8a/T+wlQU6GHwT+CFwHvFmurRcVaIncf51w1dcTcOleMXp/T9q5UAzaW+XaGuBW4Frg+6Jm/boEtotchPHYqAxxVzCzzTmWXZksI6luCymv2rUx2pTUoGvc/JsoXUD7YuhZChijckuE6hY3d5c9F42vTTHqad5HuilYRuhcRBsC25xkh2mX2GxaxHBssERUqU/LZPsCereriZEQqjamoT2gLxSifl+MvN3AXfIBmCcMZCFwmLxI1egzIz1il3kZfbZlI6PzgJ0sz/iQTPxTxLZ0OzqG9AMlmkQdMURb41sSR/82KkLbGIy3xtEvbcOkySRhzlB5z4PZWVKXjWGy52IYm4tVudoOILPptF7YOIkNC5aKHjWWeWvsG4q0ixmVqymAgZu0PX454c2uZUMgkx+jSnFlgIpzg1LqICmTETFxYwzVaJ1S6iql1FLr/iSfaqXUe5RS11jPe1wp9V6l1Gyl1K1KqYdFzaIEn2zI2SW/j102jirRGKBq+aFLzmnVFdGfOKJ8rXUmy4jlm+TTGKDeNUacW7Np2SZ/t8VUrymSnvtL1WoUGhkVJs5Zupyn7iBskjHJxaR9XNVQBfxWJ2NvxsuUbbPa0Ob3DO/p9CT4EHqr/U2e65vQcXlMHOZq4JPCBZfEqPc1kWA2yOrwsnB9Y4SeKtLPXOG6b5WdNuNP9CBwlahXH0P766wRCagfh1LANmS6yI3xVeHJRKt8xMbKKBTDeEDH52lGb0t7ca/8ZuxCZegcXOehvZvnpdjpzWhP5JvFZnS6PPuNwOXAL908d3AYPyiW8RgsB64MkGjuA24EfsVInvYasbmchj6cuQidFrk8xrP2oB0BnxJbTRs6TGkGHZDsv8XYfQPwZdLz0XFwcBhnjMcYjS8UlWZRgFRyp0gffxQDsUEVsEAkqMPQHtLThBENiGF7K/Ci1POKJUWdgg46/zERX28TVesZN7wODpOf8RhUox3CLkbvLvlhC9o78mF0uNWNaFtOmDNhOTrkxtHo1DunoYOyzxCp5laRchzDcXD4O2Q8Nk4W9ecsUX/KQtSn7Whj2w50PKB+kYSmiQR0sHwb9KK37e9A527f6YbTwcExHhsVwHFou85J8vd8ksVl3oLe6XoE+D36EOs2N4QODo7xxMVsRmw6h4s0UyMSThna87lbJKAtjNh2XkaH23BwcJjA+P8BAAZ2dLKBO1SLAAAAAElFTkSuQmCC" alt="company logo" ></a>
                          <br><br>
                          </td>
                        </tr>
                      </table>

                      <table cellspacing="0" cellpadding="0" class="force-full-width" style="background-color:#3bcdb0;">
                        <tr>
                          <td style="background-color:#3bcdb0;">

                            <table cellspacing="0" cellpadding="0" class="force-full-width">
                              <tr>
                                <td style="font-size:40px; font-weight: 600; color: #ffffff; text-align:center;" class="mobile-spacing">
                                <div class="mobile-br">&nbsp;</div>
                                  Аккаунт успешно создан
                                <br>
                                </td>
                              </tr>
                              <tr>
                                <td style="font-size:24px; text-align:center; padding: 0 75px; color:#6f6f6f;" class="w320 mobile-spacing; ">
                                  Теперь необходимо подтвердить e-mail
                                </td>
                              </tr>
                            </table>

                            <table cellspacing="0" cellpadding="0" width="600" class="force-full-width">
                              <tr>
                                <td>
                                  <img src="https://www.filepicker.io/api/file/4BgENLefRVCrgMGTAENj" style="max-width:100%; display:block;">
                                </td>
                              </tr>
                            </table>

                          </td>
                        </tr>
                      </table>

                      <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff" >
                        <tr>
                          <td style="background-color:#ffffff;">
                            <br>
                            <table class="columns" cellspacing="0" cellpadding="0" width="49%" align="left">
                              <tr>

                              <!-- ############# STEP ONE ############### -->
                              <!-- To change number images to step one:
                                  - Replace image below with this url: https://www.filepicker.io/api/file/acgdn9j9T16oHaZ8znhv
                                  - Then replace step two with this url: https://www.filepicker.io/api/file/iqmbVoMtT7ukbPUoo9zH
                                  - Finally replace step three with this url: https://www.filepicker.io/api/file/ni2yEbRCRJKzRm3cYGnn

                                  Finished!
                               -->
                                <td style="padding-left: 60px; padding-top: 10px;">
                                  <img src="https://www.filepicker.io/api/file/zNDJy10QemuMhAcirOwQ" alt="step one" width="60" height="62">
                                </td>


                                <td style="color:#f3a389; text-align:left; padding-top: 10px;">
                                  Создание аккаунта на сайте
                                </td>
                              </tr>
                              <tr>

                              <!-- ############# STEP TWO ############### -->
                              <!-- To change number images to step two:
                                  - Replace image below with this url: https://www.filepicker.io/api/file/23h1I8Ts2PNLx755Dsfg
                                  - Then replace step one with this url: https://www.filepicker.io/api/file/zNDJy10QemuMhAcirOwQ
                                  - Finally replace step three with this url: https://www.filepicker.io/api/file/ni2yEbRCRJKzRm3cYGnn

                                  Finished!
                               -->
                                <td style="padding-left: 60px; padding-top: 10px;">
                                  <img src="https://www.filepicker.io/api/file/23h1I8Ts2PNLx755Dsfg" alt="step two" width="60" height="65">
                                </td>
                                <td style="color:#f5774e; text-align:left; padding-top: 10px;">
                                  Подтверждение электронной почты
                                </td>
                              </tr>
                              <tr>

                              <!-- ############# STEP THREE ############### -->
                              <!-- To change number images to step three:
                                  - Replace image below with this url: https://www.filepicker.io/api/file/OombIcyT92WWTaHB4vlE
                                  - Then replace step one with this url: https://www.filepicker.io/api/file/zNDJy10QemuMhAcirOwQ
                                  - Finally replace step three with this url: https://www.filepicker.io/api/file/iqmbVoMtT7ukbPUoo9zH

                                  Finished!
                               -->
                                <td style="padding-left: 60px; padding-top: 10px;">
                                  <img src="https://www.filepicker.io/api/file/ni2yEbRCRJKzRm3cYGnn" alt="step three" width="60" height="60">
                                </td>
                                <td  style="color:#f3a389; text-align:left; padding-top: 10px;">
                                  Регистрация завершена!
                                </td>
                              </tr>
                            </table>
                            <table class="columns" cellspacing="0" cellpadding="0" width="49%" align="right">
                              <tr>
                                <td class="column-padding" style="text-align:left; vertical-align:top; padding-left: 20px; padding-right:30px; font-size:16px;">
                                  <br>
                                  <span style="color:#3bcdb0; font-size:18px; font-weight:bold;">Здравствуйте, {{login}}</span><br><br>
                                  Для подтверждения электронной почты и активации аккаунта нажмите на кнопку "Подтвердить e-mail".<br><br>
                                  Если Вы не регистрировались на нашем сайте, просто удалите данное письмо.
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>

                      <table style="margin:0 auto; width:100%" cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff">
                        <tr>
                          <td style="text-align:center; margin:0 auto;">
                          <br>
                            <div><!--[if mso]>
                              <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:220px;" stroke="f" fillcolor="#f5774e">
                                <w:anchorlock/>
                                <center>
                              <![endif]-->
                                  <a href="http://citt.fund/submitemail/{{login}}/{{emailConfirmToken}}"
                                style="background-color:#f5774e;color:#ffffff;display:inline-block;font-family:\'Source Sans Pro\', Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:45px;text-align:center;text-decoration:none;width:220px;-webkit-text-size-adjust:none;">Подтвердить e-mail</a>
                                  <!--[if mso]>
                                </center>
                              </v:rect>
                            <![endif]--></div>
                            <br>
                          </td>
                        </tr>
                      </table>



                      <table cellspacing="0" cellpadding="0" bgcolor="#363636" style="width:100%;" class="force-full-width">
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#e0e0e0; font-size: 14px; text-align:center;">
                            © 2017 Crypto Investment and Token Trading<br>
                          </td>
                        </tr>
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                      </table>









                      </td>
                    </tr>
                  </table>

                </center>




                </td>
              </tr>
            </table>
            </body>
            </html>
            ', 'ru'],
            ['payPassword', '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
             <meta name="viewport" content="width=device-width, initial-scale=1" />
              <style type="text/css">

              img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
              a img { border: none; }
              table { border-collapse: collapse !important; }
              #outlook a { padding:0; }
              .ReadMsgBody { width: 100%; }
              .ExternalClass {width:100%;}
              .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
              table td {border-collapse: collapse;}
              .ExternalClass * {line-height: 115%;}

              td {
                font-family: Arial, sans-serif;
              }

              body {
                -webkit-font-smoothing:antialiased;
                -webkit-text-size-adjust:none;
                width: 100%;
                height: 100%;
                color: #6f6f6f;
                font-weight: 400;
                font-size: 18px;
              }


              h1 {
                margin: 10px 0;
              }

              a {
                color: #27aa90;
                text-decoration: none;
              }


              .body-padding {
                padding: 0 75px;
              }


              .force-full-width {
                width: 100% !important;
              }


              </style>

              <style type="text/css" media="screen">
                  @media screen {
                    @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
                    /* Thanks Outlook 2013! */
                    * {
                      font-family: \'Source Sans Pro\', \'Helvetica Neue\', \'Arial\', \'sans-serif\' !important;
                    }
                  }
              </style>

              <style type="text/css" media="only screen and (max-width: 599px)">
                /* Mobile styles */
                @media only screen and (max-width: 599px) {

                  table[class*="w320"] {
                    width: 320px !important;
                  }

                  td[class*="w320"] {
                    width: 280px !important;
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                  }

                  img[class*="w320"] {
                    width: 250px !important;
                    height: 67px !important;
                  }

                  td[class*="mobile-spacing"] {
                    padding-top: 10px !important;
                    padding-bottom: 10px !important;
                  }

                  *[class*="mobile-hide"] {
                    display: none !important;
                    width: 0 !important;
                  }

                  *[class*="mobile-br"] {
                    font-size: 12px !important;
                  }

                  td[class*="mobile-center"] {
                    text-align: center !important;
                  }

                  table[class*="columns"] {
                    width: 100% !important;
                  }

                  td[class*="column-padding"] {
                    padding: 0 50px !important;
                  }

                }
              </style>
            </head>
            <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
            <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
              <tr>
                <td align="center" valign="top" style="background-color:#eeebeb" width="100%">

                <center>

                  <table cellspacing="0" cellpadding="0" width="600" class="w320">
                    <tr>
                      <td align="center" valign="top">


                      <table style="margin:0 auto;" bgcolor="#363636" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                          <td style="text-align: center;">
                            <br>
                            <a href="#"><img class="w320" width="286" height="73" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR4AAABJCAYAAADrNUktAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAGUZJREFUeNrsnXt03VWVxz+5aZLaJm1vkUKptJBKUQoVvSgvRyjeAuL4qJg6MPiAxSTgzDCzGJ1UXCprFEh0dIGi2IAgI4o0vmUpJZGXgogN5VUphcbyqDza5rY0j6Z5nPnj7NOc/Pp75v5um8TzXeuum/zu+Z3fOfuc3z5777PP3mVKKfYjZgNHA0uAxcBCYC4wE5gGTAGUfHYDu4BtwGbgGeBJ+TyPg4PDhEXZfmA8RwFnAmcBJwijKQbdwF+Ae4HfAA8De9xQOjg4xlMNfBi4AHgP8AafMgXgdWAQKBOJp0rKVsm1OHgKaAVuB551Q+rg8PfHeN4IXAT8C/Bmn9+fB/4qatRM4FBRv6YBFSlIQj8Bvgmsc0Pr4DD5GU+VMJvPAAs8v/UCD6HtNHOAY4HDU2A0QdgD/AC4BtjkhtjBYXIyntOBrwLv9FzvA+6Ul/944FSgZj/2rQBcDVwr6pyDg8MkYDxVwJeBy4Fyz293oo2/7wbOkbJJ0CuMa1jqrhJ1rGwM7XwA+DSw3g23g8PEZjxHAbeIFGPjeeAbwCHAJWj7TRwMiWT0EjAAzAAORtuBpgOV6K32YqSfy4Db3JA7OExMxpNH21AO9Vz/CfAj4N+AM2LW9TfgfuA1YD5wHNr+U1Wi/l4NfN4Nu4PDxGI85wPfA6Z6rl+BNh5fizYgR6ET+LGoU2cAp5SQ2XhxK1CP8/1xcJgQjOdC4CYgY13rAz4l0s+1RNtguqXcs2gfn2UHqN8/A85zzMfB4cAgE7PceT5M53Xg/ehjD9fFYDrtaKfCN0hdyw5gvz8C/F+C/js4OOxniSeP3qWyVaEe4Gy0cbkpxnM+D9yDdu575zjq/7fQRmcHB4dxxHiOAv7AaLvNMHqL/E0iuYRhF/BP6EOft6N3qcYbLhMG5ODgMA4Yz1TgPuBEz/UG4Gn5LUxV2Srq1GLgh+OYBnuA9wqDdXBw2A8IYxxX+TCdG4FfAD+NuHcb2nnw2HHOdED7CN0MzHLTwcHhwEo8p6FtMjZzeRp4O/BLdIiLIPQI05kDrJlAtPgucKmbEg4OB4bxTAX+hA7WZTCMNgovQXssh+HDwBPyqZ5AtFDAUrRDo4ODQwnhdwyhwcN0AK4HNqK3xMNwjUhEE43pgHYH+AZwMs6/x8GhpPDaaQ4CVnqubQM+J0wlG1LXI2gP5q+jjz5MRLwD+KSbFg4O+1fVugJtVLZxOfp4w2a0IdYPe+SlnQI8ysR2zPurMM4eNz0cHEov8cxgX+PqK6JmXRHCdABuQIeduI6J7w18JNpT28HBYT8wnuVop0AvQ6kEPh5Sx3bgSrSH82mThC6Xsm+MIQcHhxIwnk95futDbzFfQLjH8Q3ADpGKJgvegT4x7+DgUELG8xafF+136Dg5nwi5vxt93ODYSSTtGDh1y8GhxIznHPa14fwAnQMr7FDnncKcLmLynfQ+G/+0PA4ODikxnrM913cBdwlDCssGcavU8YFJSJsj0UHqHRwcUsYUdC6sd3iuP4qOtxMWM+dldED3t6Fj8kxG/APwxyLuz6GN7qCjLnbItxe18gmC13Ezj44j3RGjDXl5ZqfVFj90SJ32fXHhvdf0KSffpq0dCemX99Rt6NQeg+4F+eQC2hc0Bu0xxiOo31lpc631u7dMjnB/OBv2uBUS0M9bPml/ktK5M+l8mYK2zxzk+eEB+X57yM1/APrRoUtjZX/o6u/n7i0vs3HXLsrLynhzTQ1nzD2Ug6dW8VhXgeNnZ3mxp5cXe3qprphCpgy6BwaZVVnJohk1bNrVzc49exgQ36OqTIbK8nL6BgcZVIoyoDKTYXZVFUdUT+e5XbvY0T9SvjKToaq8nN2DQwyo4b3XspWV1Nb4OlqfOEaGU8dInKJWGZw6YJX8v9IzGeuAxoCJZSZBi3VfTupfFjE5GuVjFpC2kJcwL3WtlDKNMRiBwUqr7aZtOelrp7Tf1NcQY0Jjtdfuo6HrSqA55L4muacZWG3Rjoh7DJMPG4+gfjehQ+q2e67VSjuarT7kQl5gG63S9jb5f2HAwmWjXuZZuzXuSfszFjonmi9TRGLx4mGRhOaHPNScaXpXnBl03foNXP+XDZx66CHkDzuUaVOm8Hx3D5f9aS07+vqYV1PDTaeeyMDwMI9u7+KKtevoGRjg8iWLuaD2CPqHhnjvmt8xLZNh8ewsGcpYv2MHGws7eNchc5g3fRoDw8M8+MqrvDWb5YH35Tmn7T6Gh4dZctBsyiljw+s7eXp7gdycg5lfPZ3B4WEefnUrc6un8+gHzvZr9ltFKkySl6teBqRBJg6eFXEVOqPGQs+AdIRImLXyAq2WMs0yqKvQ+egLAfc0StkOz+Tye/Gz0u7VUqdfW1TI/eYFapOXZYWnXSulPW1Cm5YxMvWC1NMa4yVEnlMfwXhq5WVriDkefgw+7zOmZpxWW/+vDGCw7REveae0sTnGotceIM0sKyGdk80XpVSLGo0BpdThSqlTVDhOVEqhlHo8opy65KFHFKu+r366+YV9fuvq71cn/eq36sw194y6vvjndypu+aHa2rdbKaXUlp5e9dF7fq+6+vv3lvniuicU375R/fqFl/ZeW1/Yoc695wH1t55eVXfv79WrfX17f2t6Yr3i+hvV6s7Ne6899/ou9cH2+1T3wIBf03copQ6Tfsb51CqlupRS+ZAyWaXUJqXUKutao1KqLaLunLSp1qqnSynVFFC+zadOFaNtXUqp+oDfo+7fFNIe86mXZ9TGoKf3eY1Wv9aG3NcmZW261YWUb5Q2JRkP+xNGM1PfppjtDaLDqog6zPwzZduK6M9Y6JxovmR8dL9t6LQzYTrhDuA59EHQuWFs8OZnN/Hdjsf4bO5tfGTB4fsus5WV3Hb6u5leMfq8amUmQ0UmA2Vai+sfHuLiRQvJVo5svg0rBWVlDFnHPo6ZNZPza49g+549XHRULXOmTg0tv7Cmmo8vPJK+wSG/5s9A5wgjwcrXHqFKFGTVqk+40tv6uqmnwVptve3IeVbwuKtcRwIbhFfSy8ZQaVpCVLm4aPCob1F064iwQdT5SKdJkI2QCtpjSg1E1GEks7AxaE3hWWOhcyJkfBjHK+gEe4eF3Pcq0CUvZaBzYffgIFc+9iQV06fRsCjY/rywppoPHj6PnsHBUTKaSGQALJg+nTPn+fM4b2CP5QsO55iZMzhr3mGxyp+7YD4HTfXNrlNGvHQ9SSdwC8mzotZaIrdtA2gVlSvro2J1jvElGgvyCdSnlogXKI7RtdmyJUWh1WKMfuphrgjVzzC3+ojfi01uUIhBt/oE9rNS0DkR45nhtQHL9xsjGI9CR+0LPMP14KtbebGrwNGzsxxZHR4l4xMLj+QN5cGnFDJlZbHf1LKk5ctCucDMBIwhm/LA+0lTnT6rUtZalVbJRG8eI/PIjXH1zyXYdWmXNtcWQY9mqWdVTEZHwEtbx8iO41ixUmi31pI2SwHDeGoDmA5FMtBi6RwbU9jXSW6nfFdFqFpRZVjXVYDBIeZPn0amLJwNRP1+AFGdgPFA9LZt2P2NAdfNjtOKgJWwQQyYZsfrhAgGEcR06ouQlGoT9N3eHi9GLWiwXvbmiOe1ykvb4sN4WkIM5WFSzEqLkZ4g9DObCwVGdrhaipgXfmqjn7G8LoLpJOnPWOmciPF4g4GZcBBhhyT74lS+bfdugFBJZgJg2gF+fiGGCmRULrMF2hmhDuYDJt6KEkpspYCtCnREtL1VmLPN7IzPTWtI3VHjYpdfKZ9aS3o0Lg0rU5JGWqxx9i5ODTFoFbc/Y6VzbMbjNXkMy3fYFvKQ59tfRxFDcN/Q0ERmPMMJXoIkq7h3qzJsYjTLatUUMrmMGB61Iq0sEXPpTKA62Q52aagCOcJdCwzjKXhoVEew4bdQBJ06LSazUsZtFekYmc0CU289oz5G3YUixz0unWPbeLyo8kg+hJR5PYz5HJedBeXlbNrVPWonaYJhV4LJFteDM0+8RIj2pCnWIFtqtCewbRhv6kJKz/baucKkhXqP9Nda5LPrI8bSqMKdKY2f31yoT6EfadI5FuPp91wzxuatMeweXWEv5umHzuGQ7Ew2bu/S9p4QvLZ7N90Dg+PtZRpC53lPshrFmVx1FqNKsopmxzHjCds58ntZ0zSChrkWeNtojnLUWdeKfXZ9zPFLCy2Wmlgqo3IxdI7FeLo912bJ90sh9x1sMZ7XggrNqqzkS8cfh+rfwxcefTyU6dzybCfK0vrKxNgcZnQuj1GmmPLo82hPJpwQuYgVIceIEXcywfgvrY6hYtaW4EWx7VxBsH168pb6VWy/iRhzc4YrLRW3U+qql3r351yKQ+dYjGe759oc9O7y5pD7DgVqgAG0+38gLj36KC7NHc9dz27i3Hse4LldowWkP23dxjVPrOfD899ETYU+CD+kFNt272ZgYICde4ITPmzp7YWBQV7r2x2rs1t6+2BwkFdilhca9CacECtkEjb5rP71jBwpaBnDZIPSbdWmgRXS5zafFdEc+zBnxwoleL5xeIuSeupSUrOMFGDsOEG7km3yrI6UGUCa/UibzpGM5wXPtUNEonkuZHLMYSRMaiQxv3PSCdx85hk83bWDU+5cw9K72jn//ge54P4HuWPzC/znMW/h6Jlaw1u3vYurHnuS6eXlLJo5g+Yn17Nmy8va69iIIb193PTMczzVtYNFcw/h5y+8yB1/fZ7uwcEQiWoTa7dvZ9HcQ/jNS1v4UedmXh8YiGr6g+xrfI+zAp4gA9Mlk65N/q6XQWsYw1gZm8h4ZjwFRg51mjNpbeit2LXy+wkpv4De56+IKZVGGVvzMvZhn7xV5zLrHjPmmyymsyLlvrZYDCiOGpekP2nQORRlSqmvAJ/3XH+3vHSPEBwI7KPoVMZnA7+N8zAFbNixkxd7eqnIZKitqWZB9fRRZXoGBxkcHqa6ooIyoHdwiCGlmFFZsdfJb8/wMD0Dg0yvmMKUTIb+oSF2Dw0xo6JirzplY2B4mG6r/J6hIfqGhqipqGBKuNr1fuA3RU4QEwYhTmiGOMiWSFooBbLS/6T2rImOvLUoF3DwZTx1Pnr5v6OzS1wP/GvAvd8A/ksm1waSHS2YCNgJLAa2uGni4JAuMsDj7Js508Rf/l3IvaeJLaiAzrM+2fAHx3QcHErHeDrZd8v4ZPQZrPsZObvlxRJ0vBqAH05C2tzupoeDQ+kYzyBwn+f6EWjbTleI1FMBfET+bgOemUR02Urxth0HB4cQxgM6W4QXy+X7ByH3/7NIRv1oe9Bkwd04o6CDQ8lgcqdPB/7C6FCnm4Fj0N67TwKLAupYDvwC7c38BDo7w0RGN3pX73E3PRwcSivx9AA/8VG3zkEbnr8TUsdn0UbmbuBLk4Am3xFm63JqOTiUWOIBvXW8jtF5tO4DlqLPbz0FHB5Qz7nAz4QB3U2R5zgOIJ5G5wg7SvoxXGR9cdKEGIfAKKc6b8T+oDQpSX1mik1jk7PaEpbCx9AjG7OvJgxr3HQw7UXS029cxpKGJqpMFOK0P59gfMJon4/RLuOLFeVsmbP62R5lqrAZD6IyfchT5gx0/qxL0HnS/bBBHtwrKtkjxI/cN14wKBLeEPqc2sYU6jRHB8KyKpgyUR69itGpXtrwD8FhJlgr8YJ6tSV4Ke10Ln4pfExM4A5GTmTbMIcLl4VMchMzZ4XUE9RPL5YVSU8v/MJ9RqWhUYSnDzIxe1pD+r9JvsNS2aiQRcCcXvcGH/OjvZJnhIW5yAtN/TxtTawhO0aPOQdnp2Py6cHo6O+nKKWGPZkW7pPfKiTifBCutur5uJp4+JJSaqG0vSxBRP6oiP+rIyL8t0mWgrUJMy6ERfivled2SZaFpG2PyiZhMkU0BmSqWBXw7LBsB1mhwVr5O24mg7ToGbfuxiLqbIwoUy+ZJNYW8Zx8AB0bA7KOdEVkBslLOe9184zagPnn14ZRWSZsPCRSj43TZBUbAC4jOEDYZ6Qs6J2wr00gaefHwE0i3f2Z5Oez/FBvSR12dkk/tMjvTSn1xxxWTT1WrrRzlUg0fqeiTeiE1gTPzlqSVxoHSNOmZ1poJjx0igkXYkKMjAUmkV/cuDkmGV+SWEFN1lh1Bsw/04b6MOOyjS+wb2jTr6LDZTxE8BH8CuAWRtLBNAK3TQCmczdwBTpr6G5RG0mJ8ZiYu1FZCFKLc+KjGuVSrtNETmyN8ezaGC9QDn2AtIMUItuVmJ5poCNgETK2slaLcdYV0X+TQikboz3NjM5UErVINIaqUSNtaEnCeNajz2HZOMLicv/DSBZRL44EfoSOUKiAi0SaGK9oBy5Ge2AvFCaUFtOB0SeIo15Av1Q1aUg+SSIDxkHcMAxm4uUjmI7JotmQ8tiWgp5pSYydIRKyiULQSnERC1ssw3AcqacQU0KtI37c5WZ5r2IxHoCr0T45NhrQJ9L3ABcAzwfce4ZIPhlRzy4ogbifBn4GnA+cJJPhCXTanrQYT4vPJIhiPqmFlvQw13yKL02SFD4dIRPfMJ2OEjCdUtKzGOQCGHeWfcOXGsZTW+TYx2E8RkKsi8HskqQxCsSUgOu9Igk8AEy1rq9Cb6tvEBvC3fjvXp2H3h26UGxCl6Dj+1wT8sz9iSZpy3lo/6NKYE2Kk8ubxdOsYFFJ7+xUNe2Mv4wPSVP4FAjOAdVkMaaoyVxPdHCvlnFCz7qAl93Y+Rp82lFvzRG7T97g9KWW/o3KFbYdHuYeEnubP4wJ/BltMLaPQswWgixFb5l/FPg5/rmnLkB7RF+EzsP1v9KAb6F9hg4EtgCXiq2qUb6XAl9J8Rn1ll3H+3KsJnpruNUqu5DJd3TD2JyMi0Gb9DXMvtMeod5NBHqGpSkKikFtbCT7K7RpszC61Ywt82ljADPax20hSvr4tjCJS61rx8pA/qNUtlz+n+Vz/3L0MYwL0ccu7gVOBT6Hjvmzv3JWKeBmtJf1EcDX5f/zgG8ykqCwWGQtHbgxZEWMmkgmgdoqio9cV0t6Ef+SpvAJevYKi5GskL6GTfbOIqWVNOkZx7bUHrIoedPc5C0VttFnPtkJHdOyJ4UxRzMeQcn7wtIYLQt492LbeGz8B/Brz7XTgTtEomkHzgrpYA5tjL5Y/t+JtoifhN52313iiXAn8C55/qeAL6KN58bw/XTKYjYhkyTuNmkSnTsKedKL/pckhU+QPaDDI72YyZ6jdNvfadKzGLSwb5obs1AFbUt3jLHNtWNcdExG0cYABtORcPwDRIF4jlPV4nzkxT1KqYOlzPyAMjZ+qZRa4ql7sVLqa0qpzhSdAf+mlPquUurt8oxjpK03iZPgL5RSH0jBSdDPqWpVDKfCuphOaU3i4JVN6EDodf7KpuhA2BjDOY+ANoc5ENZL+fqETntJnPzC6Lk/HAi9dWQD+uxHl2zC53jHKciBMB8yl9t8HAizQsO6GPQKdJjMxORc3ejYO14D7FJZ3Rejg8a/T+wlQU6GHwT+CFwHvFmurRcVaIncf51w1dcTcOleMXp/T9q5UAzaW+XaGuBW4Frg+6Jm/boEtotchPHYqAxxVzCzzTmWXZksI6luCymv2rUx2pTUoGvc/JsoXUD7YuhZChijckuE6hY3d5c9F42vTTHqad5HuilYRuhcRBsC25xkh2mX2GxaxHBssERUqU/LZPsCereriZEQqjamoT2gLxSifl+MvN3AXfIBmCcMZCFwmLxI1egzIz1il3kZfbZlI6PzgJ0sz/iQTPxTxLZ0OzqG9AMlmkQdMURb41sSR/82KkLbGIy3xtEvbcOkySRhzlB5z4PZWVKXjWGy52IYm4tVudoOILPptF7YOIkNC5aKHjWWeWvsG4q0ixmVqymAgZu0PX454c2uZUMgkx+jSnFlgIpzg1LqICmTETFxYwzVaJ1S6iql1FLr/iSfaqXUe5RS11jPe1wp9V6l1Gyl1K1KqYdFzaIEn2zI2SW/j102jirRGKBq+aFLzmnVFdGfOKJ8rXUmy4jlm+TTGKDeNUacW7Np2SZ/t8VUrymSnvtL1WoUGhkVJs5Zupyn7iBskjHJxaR9XNVQBfxWJ2NvxsuUbbPa0Ob3DO/p9CT4EHqr/U2e65vQcXlMHOZq4JPCBZfEqPc1kWA2yOrwsnB9Y4SeKtLPXOG6b5WdNuNP9CBwlahXH0P766wRCagfh1LANmS6yI3xVeHJRKt8xMbKKBTDeEDH52lGb0t7ca/8ZuxCZegcXOehvZvnpdjpzWhP5JvFZnS6PPuNwOXAL908d3AYPyiW8RgsB64MkGjuA24EfsVInvYasbmchj6cuQidFrk8xrP2oB0BnxJbTRs6TGkGHZDsv8XYfQPwZdLz0XFwcBhnjMcYjS8UlWZRgFRyp0gffxQDsUEVsEAkqMPQHtLThBENiGF7K/Ci1POKJUWdgg46/zERX28TVesZN7wODpOf8RhUox3CLkbvLvlhC9o78mF0uNWNaFtOmDNhOTrkxtHo1DunoYOyzxCp5laRchzDcXD4O2Q8Nk4W9ecsUX/KQtSn7Whj2w50PKB+kYSmiQR0sHwb9KK37e9A527f6YbTwcExHhsVwHFou85J8vd8ksVl3oLe6XoE+D36EOs2N4QODo7xxMVsRmw6h4s0UyMSThna87lbJKAtjNh2XkaH23BwcJjA+P8BAAZ2dLKBO1SLAAAAAElFTkSuQmCC" alt="логотип компании" ></a>
                          <br><br>
                          </td>
                        </tr>
                      </table>

                      <table cellspacing="0" cellpadding="0" class="force-full-width" style="background-color:#3bcdb0;">
                        <tr>
                          <td style="background-color:#3bcdb0;">

                            <table cellspacing="0" cellpadding="0" class="force-full-width">
                              <tr>
                                <td style="font-size:40px; font-weight: 600; color: #ffffff; text-align:center;" class="mobile-spacing">
                                <div class="mobile-br">&nbsp;</div>
                                  Ваш платежный пароль
                                <br>
                                </td>
                              </tr>
                              <tr>
                                <td style="font-size:24px; text-align:center; padding: 0 75px; color:#6f6f6f;" class="w320 mobile-spacing; ">

                                </td>
                              </tr>
                            </table>

                            <table cellspacing="0" cellpadding="0" width="600" class="force-full-width">
                              <tr>
                                <td>
                                  <img src="https://www.filepicker.io/api/file/4BgENLefRVCrgMGTAENj" style="max-width:100%; display:block;">
                                </td>
                              </tr>
                            </table>

                          </td>
                        </tr>
                      </table>

                      <table style="margin:0 auto; width:100%" cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff">
                        <tr>
                          <td style="background-color:#ffffff; padding: 30px 50px">
                              <center>
                                <table style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
                                    <tr>
                                      <td style="text-align:left; color: #6f6f6f;">
                                          <p>Здравствуйте, {{login}}.</p>
                                          <p>Вы запросили платёжный пароль</p>
                                          <p>Ваш платёжный пароль: {{payPassword}}</p>
                                          <p>Если вы не запрашивали платёжный пароль то просто удалите это письмо</p>
                                      </td>
                                    </tr>
                               </table>
                              </center>
                          </td>
                        </tr>
                      </table>



                      <table cellspacing="0" cellpadding="0" bgcolor="#363636" style="width:100%;" class="force-full-width">
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#e0e0e0; font-size: 14px; text-align:center;">
                            © 2017 Crypto Investment and Token Trading<br>
                          </td>
                        </tr>
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                      </table>









                      </td>
                    </tr>
                  </table>

                </center>




                </td>
              </tr>
            </table>
            </body>
            </html>
            ', 'ru'],
            ['unConfirmEmailCode', '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
             <meta name="viewport" content="width=device-width, initial-scale=1" />
              <style type="text/css">

              img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
              a img { border: none; }
              table { border-collapse: collapse !important; }
              #outlook a { padding:0; }
              .ReadMsgBody { width: 100%; }
              .ExternalClass {width:100%;}
              .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
              table td {border-collapse: collapse;}
              .ExternalClass * {line-height: 115%;}

              td {
                font-family: Arial, sans-serif;
              }

              body {
                -webkit-font-smoothing:antialiased;
                -webkit-text-size-adjust:none;
                width: 100%;
                height: 100%;
                color: #6f6f6f;
                font-weight: 400;
                font-size: 18px;
              }


              h1 {
                margin: 10px 0;
              }

              a {
                color: #27aa90;
                text-decoration: none;
              }


              .body-padding {
                padding: 0 75px;
              }


              .force-full-width {
                width: 100% !important;
              }


              </style>

              <style type="text/css" media="screen">
                  @media screen {
                    @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
                    /* Thanks Outlook 2013! */
                    * {
                      font-family: \'Source Sans Pro\', \'Helvetica Neue\', \'Arial\', \'sans-serif\' !important;
                    }
                  }
              </style>

              <style type="text/css" media="only screen and (max-width: 599px)">
                /* Mobile styles */
                @media only screen and (max-width: 599px) {

                  table[class*="w320"] {
                    width: 320px !important;
                  }

                  td[class*="w320"] {
                    width: 280px !important;
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                  }

                  img[class*="w320"] {
                    width: 250px !important;
                    height: 67px !important;
                  }

                  td[class*="mobile-spacing"] {
                    padding-top: 10px !important;
                    padding-bottom: 10px !important;
                  }

                  *[class*="mobile-hide"] {
                    display: none !important;
                    width: 0 !important;
                  }

                  *[class*="mobile-br"] {
                    font-size: 12px !important;
                  }

                  td[class*="mobile-center"] {
                    text-align: center !important;
                  }

                  table[class*="columns"] {
                    width: 100% !important;
                  }

                  td[class*="column-padding"] {
                    padding: 0 50px !important;
                  }

                }
              </style>
            </head>
            <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
            <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
              <tr>
                <td align="center" valign="top" style="background-color:#eeebeb" width="100%">

                <center>

                  <table cellspacing="0" cellpadding="0" width="600" class="w320">
                    <tr>
                      <td align="center" valign="top">


                      <table style="margin:0 auto;" bgcolor="#363636" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                          <td style="text-align: center;">
                            <br>
                            <a href="#"><img class="w320" width="286" height="73" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR4AAABJCAYAAADrNUktAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAGUZJREFUeNrsnXt03VWVxz+5aZLaJm1vkUKptJBKUQoVvSgvRyjeAuL4qJg6MPiAxSTgzDCzGJ1UXCprFEh0dIGi2IAgI4o0vmUpJZGXgogN5VUphcbyqDza5rY0j6Z5nPnj7NOc/Pp75v5um8TzXeuum/zu+Z3fOfuc3z5777PP3mVKKfYjZgNHA0uAxcBCYC4wE5gGTAGUfHYDu4BtwGbgGeBJ+TyPg4PDhEXZfmA8RwFnAmcBJwijKQbdwF+Ae4HfAA8De9xQOjg4xlMNfBi4AHgP8AafMgXgdWAQKBOJp0rKVsm1OHgKaAVuB551Q+rg8PfHeN4IXAT8C/Bmn9+fB/4qatRM4FBRv6YBFSlIQj8Bvgmsc0Pr4DD5GU+VMJvPAAs8v/UCD6HtNHOAY4HDU2A0QdgD/AC4BtjkhtjBYXIyntOBrwLv9FzvA+6Ul/944FSgZj/2rQBcDVwr6pyDg8MkYDxVwJeBy4Fyz293oo2/7wbOkbJJ0CuMa1jqrhJ1rGwM7XwA+DSw3g23g8PEZjxHAbeIFGPjeeAbwCHAJWj7TRwMiWT0EjAAzAAORtuBpgOV6K32YqSfy4Db3JA7OExMxpNH21AO9Vz/CfAj4N+AM2LW9TfgfuA1YD5wHNr+U1Wi/l4NfN4Nu4PDxGI85wPfA6Z6rl+BNh5fizYgR6ET+LGoU2cAp5SQ2XhxK1CP8/1xcJgQjOdC4CYgY13rAz4l0s+1RNtguqXcs2gfn2UHqN8/A85zzMfB4cAgE7PceT5M53Xg/ehjD9fFYDrtaKfCN0hdyw5gvz8C/F+C/js4OOxniSeP3qWyVaEe4Gy0cbkpxnM+D9yDdu575zjq/7fQRmcHB4dxxHiOAv7AaLvNMHqL/E0iuYRhF/BP6EOft6N3qcYbLhMG5ODgMA4Yz1TgPuBEz/UG4Gn5LUxV2Srq1GLgh+OYBnuA9wqDdXBw2A8IYxxX+TCdG4FfAD+NuHcb2nnw2HHOdED7CN0MzHLTwcHhwEo8p6FtMjZzeRp4O/BLdIiLIPQI05kDrJlAtPgucKmbEg4OB4bxTAX+hA7WZTCMNgovQXssh+HDwBPyqZ5AtFDAUrRDo4ODQwnhdwyhwcN0AK4HNqK3xMNwjUhEE43pgHYH+AZwMs6/x8GhpPDaaQ4CVnqubQM+J0wlG1LXI2gP5q+jjz5MRLwD+KSbFg4O+1fVugJtVLZxOfp4w2a0IdYPe+SlnQI8ysR2zPurMM4eNz0cHEov8cxgX+PqK6JmXRHCdABuQIeduI6J7w18JNpT28HBYT8wnuVop0AvQ6kEPh5Sx3bgSrSH82mThC6Xsm+MIQcHhxIwnk95futDbzFfQLjH8Q3ADpGKJgvegT4x7+DgUELG8xafF+136Dg5nwi5vxt93ODYSSTtGDh1y8GhxIznHPa14fwAnQMr7FDnncKcLmLynfQ+G/+0PA4ODikxnrM913cBdwlDCssGcavU8YFJSJsj0UHqHRwcUsYUdC6sd3iuP4qOtxMWM+dldED3t6Fj8kxG/APwxyLuz6GN7qCjLnbItxe18gmC13Ezj44j3RGjDXl5ZqfVFj90SJ32fXHhvdf0KSffpq0dCemX99Rt6NQeg+4F+eQC2hc0Bu0xxiOo31lpc631u7dMjnB/OBv2uBUS0M9bPml/ktK5M+l8mYK2zxzk+eEB+X57yM1/APrRoUtjZX/o6u/n7i0vs3HXLsrLynhzTQ1nzD2Ug6dW8VhXgeNnZ3mxp5cXe3qprphCpgy6BwaZVVnJohk1bNrVzc49exgQ36OqTIbK8nL6BgcZVIoyoDKTYXZVFUdUT+e5XbvY0T9SvjKToaq8nN2DQwyo4b3XspWV1Nb4OlqfOEaGU8dInKJWGZw6YJX8v9IzGeuAxoCJZSZBi3VfTupfFjE5GuVjFpC2kJcwL3WtlDKNMRiBwUqr7aZtOelrp7Tf1NcQY0Jjtdfuo6HrSqA55L4muacZWG3Rjoh7DJMPG4+gfjehQ+q2e67VSjuarT7kQl5gG63S9jb5f2HAwmWjXuZZuzXuSfszFjonmi9TRGLx4mGRhOaHPNScaXpXnBl03foNXP+XDZx66CHkDzuUaVOm8Hx3D5f9aS07+vqYV1PDTaeeyMDwMI9u7+KKtevoGRjg8iWLuaD2CPqHhnjvmt8xLZNh8ewsGcpYv2MHGws7eNchc5g3fRoDw8M8+MqrvDWb5YH35Tmn7T6Gh4dZctBsyiljw+s7eXp7gdycg5lfPZ3B4WEefnUrc6un8+gHzvZr9ltFKkySl6teBqRBJg6eFXEVOqPGQs+AdIRImLXyAq2WMs0yqKvQ+egLAfc0StkOz+Tye/Gz0u7VUqdfW1TI/eYFapOXZYWnXSulPW1Cm5YxMvWC1NMa4yVEnlMfwXhq5WVriDkefgw+7zOmZpxWW/+vDGCw7REveae0sTnGotceIM0sKyGdk80XpVSLGo0BpdThSqlTVDhOVEqhlHo8opy65KFHFKu+r366+YV9fuvq71cn/eq36sw194y6vvjndypu+aHa2rdbKaXUlp5e9dF7fq+6+vv3lvniuicU375R/fqFl/ZeW1/Yoc695wH1t55eVXfv79WrfX17f2t6Yr3i+hvV6s7Ne6899/ou9cH2+1T3wIBf03copQ6Tfsb51CqlupRS+ZAyWaXUJqXUKutao1KqLaLunLSp1qqnSynVFFC+zadOFaNtXUqp+oDfo+7fFNIe86mXZ9TGoKf3eY1Wv9aG3NcmZW261YWUb5Q2JRkP+xNGM1PfppjtDaLDqog6zPwzZduK6M9Y6JxovmR8dL9t6LQzYTrhDuA59EHQuWFs8OZnN/Hdjsf4bO5tfGTB4fsus5WV3Hb6u5leMfq8amUmQ0UmA2Vai+sfHuLiRQvJVo5svg0rBWVlDFnHPo6ZNZPza49g+549XHRULXOmTg0tv7Cmmo8vPJK+wSG/5s9A5wgjwcrXHqFKFGTVqk+40tv6uqmnwVptve3IeVbwuKtcRwIbhFfSy8ZQaVpCVLm4aPCob1F064iwQdT5SKdJkI2QCtpjSg1E1GEks7AxaE3hWWOhcyJkfBjHK+gEe4eF3Pcq0CUvZaBzYffgIFc+9iQV06fRsCjY/rywppoPHj6PnsHBUTKaSGQALJg+nTPn+fM4b2CP5QsO55iZMzhr3mGxyp+7YD4HTfXNrlNGvHQ9SSdwC8mzotZaIrdtA2gVlSvro2J1jvElGgvyCdSnlogXKI7RtdmyJUWh1WKMfuphrgjVzzC3+ojfi01uUIhBt/oE9rNS0DkR45nhtQHL9xsjGI9CR+0LPMP14KtbebGrwNGzsxxZHR4l4xMLj+QN5cGnFDJlZbHf1LKk5ctCucDMBIwhm/LA+0lTnT6rUtZalVbJRG8eI/PIjXH1zyXYdWmXNtcWQY9mqWdVTEZHwEtbx8iO41ixUmi31pI2SwHDeGoDmA5FMtBi6RwbU9jXSW6nfFdFqFpRZVjXVYDBIeZPn0amLJwNRP1+AFGdgPFA9LZt2P2NAdfNjtOKgJWwQQyYZsfrhAgGEcR06ouQlGoT9N3eHi9GLWiwXvbmiOe1ykvb4sN4WkIM5WFSzEqLkZ4g9DObCwVGdrhaipgXfmqjn7G8LoLpJOnPWOmciPF4g4GZcBBhhyT74lS+bfdugFBJZgJg2gF+fiGGCmRULrMF2hmhDuYDJt6KEkpspYCtCnREtL1VmLPN7IzPTWtI3VHjYpdfKZ9aS3o0Lg0rU5JGWqxx9i5ODTFoFbc/Y6VzbMbjNXkMy3fYFvKQ59tfRxFDcN/Q0ERmPMMJXoIkq7h3qzJsYjTLatUUMrmMGB61Iq0sEXPpTKA62Q52aagCOcJdCwzjKXhoVEew4bdQBJ06LSazUsZtFekYmc0CU289oz5G3YUixz0unWPbeLyo8kg+hJR5PYz5HJedBeXlbNrVPWonaYJhV4LJFteDM0+8RIj2pCnWIFtqtCewbRhv6kJKz/baucKkhXqP9Nda5LPrI8bSqMKdKY2f31yoT6EfadI5FuPp91wzxuatMeweXWEv5umHzuGQ7Ew2bu/S9p4QvLZ7N90Dg+PtZRpC53lPshrFmVx1FqNKsopmxzHjCds58ntZ0zSChrkWeNtojnLUWdeKfXZ9zPFLCy2Wmlgqo3IxdI7FeLo912bJ90sh9x1sMZ7XggrNqqzkS8cfh+rfwxcefTyU6dzybCfK0vrKxNgcZnQuj1GmmPLo82hPJpwQuYgVIceIEXcywfgvrY6hYtaW4EWx7VxBsH168pb6VWy/iRhzc4YrLRW3U+qql3r351yKQ+dYjGe759oc9O7y5pD7DgVqgAG0+38gLj36KC7NHc9dz27i3Hse4LldowWkP23dxjVPrOfD899ETYU+CD+kFNt272ZgYICde4ITPmzp7YWBQV7r2x2rs1t6+2BwkFdilhca9CacECtkEjb5rP71jBwpaBnDZIPSbdWmgRXS5zafFdEc+zBnxwoleL5xeIuSeupSUrOMFGDsOEG7km3yrI6UGUCa/UibzpGM5wXPtUNEonkuZHLMYSRMaiQxv3PSCdx85hk83bWDU+5cw9K72jn//ge54P4HuWPzC/znMW/h6Jlaw1u3vYurHnuS6eXlLJo5g+Yn17Nmy8va69iIIb193PTMczzVtYNFcw/h5y+8yB1/fZ7uwcEQiWoTa7dvZ9HcQ/jNS1v4UedmXh8YiGr6g+xrfI+zAp4gA9Mlk65N/q6XQWsYw1gZm8h4ZjwFRg51mjNpbeit2LXy+wkpv4De56+IKZVGGVvzMvZhn7xV5zLrHjPmmyymsyLlvrZYDCiOGpekP2nQORRlSqmvAJ/3XH+3vHSPEBwI7KPoVMZnA7+N8zAFbNixkxd7eqnIZKitqWZB9fRRZXoGBxkcHqa6ooIyoHdwiCGlmFFZsdfJb8/wMD0Dg0yvmMKUTIb+oSF2Dw0xo6JirzplY2B4mG6r/J6hIfqGhqipqGBKuNr1fuA3RU4QEwYhTmiGOMiWSFooBbLS/6T2rImOvLUoF3DwZTx1Pnr5v6OzS1wP/GvAvd8A/ksm1waSHS2YCNgJLAa2uGni4JAuMsDj7Js508Rf/l3IvaeJLaiAzrM+2fAHx3QcHErHeDrZd8v4ZPQZrPsZObvlxRJ0vBqAH05C2tzupoeDQ+kYzyBwn+f6EWjbTleI1FMBfET+bgOemUR02Urxth0HB4cQxgM6W4QXy+X7ByH3/7NIRv1oe9Bkwd04o6CDQ8lgcqdPB/7C6FCnm4Fj0N67TwKLAupYDvwC7c38BDo7w0RGN3pX73E3PRwcSivx9AA/8VG3zkEbnr8TUsdn0UbmbuBLk4Am3xFm63JqOTiUWOIBvXW8jtF5tO4DlqLPbz0FHB5Qz7nAz4QB3U2R5zgOIJ5G5wg7SvoxXGR9cdKEGIfAKKc6b8T+oDQpSX1mik1jk7PaEpbCx9AjG7OvJgxr3HQw7UXS029cxpKGJqpMFOK0P59gfMJon4/RLuOLFeVsmbP62R5lqrAZD6IyfchT5gx0/qxL0HnS/bBBHtwrKtkjxI/cN14wKBLeEPqc2sYU6jRHB8KyKpgyUR69itGpXtrwD8FhJlgr8YJ6tSV4Ke10Ln4pfExM4A5GTmTbMIcLl4VMchMzZ4XUE9RPL5YVSU8v/MJ9RqWhUYSnDzIxe1pD+r9JvsNS2aiQRcCcXvcGH/OjvZJnhIW5yAtN/TxtTawhO0aPOQdnp2Py6cHo6O+nKKWGPZkW7pPfKiTifBCutur5uJp4+JJSaqG0vSxBRP6oiP+rIyL8t0mWgrUJMy6ERfivled2SZaFpG2PyiZhMkU0BmSqWBXw7LBsB1mhwVr5O24mg7ToGbfuxiLqbIwoUy+ZJNYW8Zx8AB0bA7KOdEVkBslLOe9184zagPnn14ZRWSZsPCRSj43TZBUbAC4jOEDYZ6Qs6J2wr00gaefHwE0i3f2Z5Oez/FBvSR12dkk/tMjvTSn1xxxWTT1WrrRzlUg0fqeiTeiE1gTPzlqSVxoHSNOmZ1poJjx0igkXYkKMjAUmkV/cuDkmGV+SWEFN1lh1Bsw/04b6MOOyjS+wb2jTr6LDZTxE8BH8CuAWRtLBNAK3TQCmczdwBTpr6G5RG0mJ8ZiYu1FZCFKLc+KjGuVSrtNETmyN8ezaGC9QDn2AtIMUItuVmJ5poCNgETK2slaLcdYV0X+TQikboz3NjM5UErVINIaqUSNtaEnCeNajz2HZOMLicv/DSBZRL44EfoSOUKiAi0SaGK9oBy5Ge2AvFCaUFtOB0SeIo15Av1Q1aUg+SSIDxkHcMAxm4uUjmI7JotmQ8tiWgp5pSYydIRKyiULQSnERC1ssw3AcqacQU0KtI37c5WZ5r2IxHoCr0T45NhrQJ9L3ABcAzwfce4ZIPhlRzy4ogbifBn4GnA+cJJPhCXTanrQYT4vPJIhiPqmFlvQw13yKL02SFD4dIRPfMJ2OEjCdUtKzGOQCGHeWfcOXGsZTW+TYx2E8RkKsi8HskqQxCsSUgOu9Igk8AEy1rq9Cb6tvEBvC3fjvXp2H3h26UGxCl6Dj+1wT8sz9iSZpy3lo/6NKYE2Kk8ubxdOsYFFJ7+xUNe2Mv4wPSVP4FAjOAdVkMaaoyVxPdHCvlnFCz7qAl93Y+Rp82lFvzRG7T97g9KWW/o3KFbYdHuYeEnubP4wJ/BltMLaPQswWgixFb5l/FPg5/rmnLkB7RF+EzsP1v9KAb6F9hg4EtgCXiq2qUb6XAl9J8Rn1ll3H+3KsJnpruNUqu5DJd3TD2JyMi0Gb9DXMvtMeod5NBHqGpSkKikFtbCT7K7RpszC61Ywt82ljADPax20hSvr4tjCJS61rx8pA/qNUtlz+n+Vz/3L0MYwL0ccu7gVOBT6Hjvmzv3JWKeBmtJf1EcDX5f/zgG8ykqCwWGQtHbgxZEWMmkgmgdoqio9cV0t6Ef+SpvAJevYKi5GskL6GTfbOIqWVNOkZx7bUHrIoedPc5C0VttFnPtkJHdOyJ4UxRzMeQcn7wtIYLQt492LbeGz8B/Brz7XTgTtEomkHzgrpYA5tjL5Y/t+JtoifhN52313iiXAn8C55/qeAL6KN58bw/XTKYjYhkyTuNmkSnTsKedKL/pckhU+QPaDDI72YyZ6jdNvfadKzGLSwb5obs1AFbUt3jLHNtWNcdExG0cYABtORcPwDRIF4jlPV4nzkxT1KqYOlzPyAMjZ+qZRa4ql7sVLqa0qpzhSdAf+mlPquUurt8oxjpK03iZPgL5RSH0jBSdDPqWpVDKfCuphOaU3i4JVN6EDodf7KpuhA2BjDOY+ANoc5ENZL+fqETntJnPzC6Lk/HAi9dWQD+uxHl2zC53jHKciBMB8yl9t8HAizQsO6GPQKdJjMxORc3ejYO14D7FJZ3Rejg8a/T+wlQU6GHwT+CFwHvFmurRcVaIncf51w1dcTcOleMXp/T9q5UAzaW+XaGuBW4Frg+6Jm/boEtotchPHYqAxxVzCzzTmWXZksI6luCymv2rUx2pTUoGvc/JsoXUD7YuhZChijckuE6hY3d5c9F42vTTHqad5HuilYRuhcRBsC25xkh2mX2GxaxHBssERUqU/LZPsCereriZEQqjamoT2gLxSifl+MvN3AXfIBmCcMZCFwmLxI1egzIz1il3kZfbZlI6PzgJ0sz/iQTPxTxLZ0OzqG9AMlmkQdMURb41sSR/82KkLbGIy3xtEvbcOkySRhzlB5z4PZWVKXjWGy52IYm4tVudoOILPptF7YOIkNC5aKHjWWeWvsG4q0ixmVqymAgZu0PX454c2uZUMgkx+jSnFlgIpzg1LqICmTETFxYwzVaJ1S6iql1FLr/iSfaqXUe5RS11jPe1wp9V6l1Gyl1K1KqYdFzaIEn2zI2SW/j102jirRGKBq+aFLzmnVFdGfOKJ8rXUmy4jlm+TTGKDeNUacW7Np2SZ/t8VUrymSnvtL1WoUGhkVJs5Zupyn7iBskjHJxaR9XNVQBfxWJ2NvxsuUbbPa0Ob3DO/p9CT4EHqr/U2e65vQcXlMHOZq4JPCBZfEqPc1kWA2yOrwsnB9Y4SeKtLPXOG6b5WdNuNP9CBwlahXH0P766wRCagfh1LANmS6yI3xVeHJRKt8xMbKKBTDeEDH52lGb0t7ca/8ZuxCZegcXOehvZvnpdjpzWhP5JvFZnS6PPuNwOXAL908d3AYPyiW8RgsB64MkGjuA24EfsVInvYasbmchj6cuQidFrk8xrP2oB0BnxJbTRs6TGkGHZDsv8XYfQPwZdLz0XFwcBhnjMcYjS8UlWZRgFRyp0gffxQDsUEVsEAkqMPQHtLThBENiGF7K/Ci1POKJUWdgg46/zERX28TVesZN7wODpOf8RhUox3CLkbvLvlhC9o78mF0uNWNaFtOmDNhOTrkxtHo1DunoYOyzxCp5laRchzDcXD4O2Q8Nk4W9ecsUX/KQtSn7Whj2w50PKB+kYSmiQR0sHwb9KK37e9A527f6YbTwcExHhsVwHFou85J8vd8ksVl3oLe6XoE+D36EOs2N4QODo7xxMVsRmw6h4s0UyMSThna87lbJKAtjNh2XkaH23BwcJjA+P8BAAZ2dLKBO1SLAAAAAElFTkSuQmCC" alt="логотип компании" ></a>
                          <br><br>
                          </td>
                        </tr>
                      </table>

                      <table cellspacing="0" cellpadding="0" class="force-full-width" style="background-color:#3bcdb0;">
                        <tr>
                          <td style="background-color:#3bcdb0;">

                            <table cellspacing="0" cellpadding="0" class="force-full-width">
                              <tr>
                                <td style="font-size:40px; font-weight: 600; color: #ffffff; text-align:center;" class="mobile-spacing">
                                <div class="mobile-br">&nbsp;</div>
                                  Код подтверждения
                                <br>
                                </td>
                              </tr>
                              <tr>
                                <td style="font-size:24px; text-align:center; padding: 0 75px; color:#6f6f6f;" class="w320 mobile-spacing; ">
                                  Необходимо подтвердить отключение e-mail от аккаунта
                                </td>
                              </tr>
                            </table>

                            <table cellspacing="0" cellpadding="0" width="600" class="force-full-width">
                              <tr>
                                <td>
                                  <img src="https://www.filepicker.io/api/file/4BgENLefRVCrgMGTAENj" style="max-width:100%; display:block;">
                                </td>
                              </tr>
                            </table>

                          </td>
                        </tr>
                      </table>

                      <table style="margin:0 auto; width:100%" cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff">
                        <tr>
                          <td style="background-color:#ffffff; padding: 30px 50px">
                              <center>
                                <table style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
                                    <tr>
                                      <td style="text-align:left; color: #6f6f6f;">
                                          <p>Здравствуйте, {{login}}</p>
                                          <p>Код отключения вашего E-mail от аккаунта: {{emailConfirmToken}}</p>
                                          <p>Если Вы не хотите отключать Ваш аккаунт или не совершали это действие, то просто удалите письмо.</p>
                                      </td>
                                    </tr>
                               </table>
                              </center>
                          </td>
                        </tr>
                      </table>



                      <table cellspacing="0" cellpadding="0" bgcolor="#363636" style="width:100%;" class="force-full-width">
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#e0e0e0; font-size: 14px; text-align:center;">
                            © 2017 Crypto Investment and Token Trading<br>
                          </td>
                        </tr>
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                      </table>









                      </td>
                    </tr>
                  </table>

                </center>




                </td>
              </tr>
            </table>
            </body>
            </html>
            ', 'ru'],
            ['partnerRegisterSuccessConfirm', '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
             <meta name="viewport" content="width=device-width, initial-scale=1" />
              <style type="text/css">

              img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
              a img { border: none; }
              table { border-collapse: collapse !important; }
              #outlook a { padding:0; }
              .ReadMsgBody { width: 100%; }
              .ExternalClass {width:100%;}
              .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
              table td {border-collapse: collapse;}
              .ExternalClass * {line-height: 115%;}

              td {
                font-family: Arial, sans-serif;
              }

              body {
                -webkit-font-smoothing:antialiased;
                -webkit-text-size-adjust:none;
                width: 100%;
                height: 100%;
                color: #6f6f6f;
                font-weight: 400;
                font-size: 18px;
              }


              h1 {
                margin: 10px 0;
              }

              a {
                color: #27aa90;
                text-decoration: none;
              }


              .body-padding {
                padding: 0 75px;
              }


              .force-full-width {
                width: 100% !important;
              }


              </style>

              <style type="text/css" media="screen">
                  @media screen {
                    @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900);
                    /* Thanks Outlook 2013! */
                    * {
                      font-family: font-family: \'Source Sans Pro\', \'Helvetica Neue\', \'Arial\', \'sans-serif\' !important;
                    }
                  }
              </style>

              <style type="text/css" media="only screen and (max-width: 599px)">
                /* Mobile styles */
                @media only screen and (max-width: 599px) {

                  table[class*="w320"] {
                    width: 320px !important;
                  }

                  td[class*="w320"] {
                    width: 280px !important;
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                  }

                  img[class*="w320"] {
                    width: 250px !important;
                    height: 67px !important;
                  }

                  td[class*="mobile-spacing"] {
                    padding-top: 10px !important;
                    padding-bottom: 10px !important;
                  }

                  *[class*="mobile-hide"] {
                    display: none !important;
                    width: 0 !important;
                  }

                  *[class*="mobile-br"] {
                    font-size: 12px !important;
                  }

                  td[class*="mobile-center"] {
                    text-align: center !important;
                  }

                  table[class*="columns"] {
                    width: 100% !important;
                  }

                  td[class*="column-padding"] {
                    padding: 0 50px !important;
                  }

                }
              </style>
            </head>
            <body  offset="0" class="body" style="padding:0; margin:0; display:block; background:#eeebeb; -webkit-text-size-adjust:none" bgcolor="#eeebeb">
            <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
              <tr>
                <td align="center" valign="top" style="background-color:#eeebeb" width="100%">

                <center>

                  <table cellspacing="0" cellpadding="0" width="600" class="w320">
                    <tr>
                      <td align="center" valign="top">


                      <table style="margin:0 auto;" bgcolor="#363636" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                          <td style="text-align: center;">
                            <br>
                            <a href="#"><img class="w320" width="286" height="73" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR4AAABJCAYAAADrNUktAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAGUZJREFUeNrsnXt03VWVxz+5aZLaJm1vkUKptJBKUQoVvSgvRyjeAuL4qJg6MPiAxSTgzDCzGJ1UXCprFEh0dIGi2IAgI4o0vmUpJZGXgogN5VUphcbyqDza5rY0j6Z5nPnj7NOc/Pp75v5um8TzXeuum/zu+Z3fOfuc3z5777PP3mVKKfYjZgNHA0uAxcBCYC4wE5gGTAGUfHYDu4BtwGbgGeBJ+TyPg4PDhEXZfmA8RwFnAmcBJwijKQbdwF+Ae4HfAA8De9xQOjg4xlMNfBi4AHgP8AafMgXgdWAQKBOJp0rKVsm1OHgKaAVuB551Q+rg8PfHeN4IXAT8C/Bmn9+fB/4qatRM4FBRv6YBFSlIQj8Bvgmsc0Pr4DD5GU+VMJvPAAs8v/UCD6HtNHOAY4HDU2A0QdgD/AC4BtjkhtjBYXIyntOBrwLv9FzvA+6Ul/944FSgZj/2rQBcDVwr6pyDg8MkYDxVwJeBy4Fyz293oo2/7wbOkbJJ0CuMa1jqrhJ1rGwM7XwA+DSw3g23g8PEZjxHAbeIFGPjeeAbwCHAJWj7TRwMiWT0EjAAzAAORtuBpgOV6K32YqSfy4Db3JA7OExMxpNH21AO9Vz/CfAj4N+AM2LW9TfgfuA1YD5wHNr+U1Wi/l4NfN4Nu4PDxGI85wPfA6Z6rl+BNh5fizYgR6ET+LGoU2cAp5SQ2XhxK1CP8/1xcJgQjOdC4CYgY13rAz4l0s+1RNtguqXcs2gfn2UHqN8/A85zzMfB4cAgE7PceT5M53Xg/ehjD9fFYDrtaKfCN0hdyw5gvz8C/F+C/js4OOxniSeP3qWyVaEe4Gy0cbkpxnM+D9yDdu575zjq/7fQRmcHB4dxxHiOAv7AaLvNMHqL/E0iuYRhF/BP6EOft6N3qcYbLhMG5ODgMA4Yz1TgPuBEz/UG4Gn5LUxV2Srq1GLgh+OYBnuA9wqDdXBw2A8IYxxX+TCdG4FfAD+NuHcb2nnw2HHOdED7CN0MzHLTwcHhwEo8p6FtMjZzeRp4O/BLdIiLIPQI05kDrJlAtPgucKmbEg4OB4bxTAX+hA7WZTCMNgovQXssh+HDwBPyqZ5AtFDAUrRDo4ODQwnhdwyhwcN0AK4HNqK3xMNwjUhEE43pgHYH+AZwMs6/x8GhpPDaaQ4CVnqubQM+J0wlG1LXI2gP5q+jjz5MRLwD+KSbFg4O+1fVugJtVLZxOfp4w2a0IdYPe+SlnQI8ysR2zPurMM4eNz0cHEov8cxgX+PqK6JmXRHCdABuQIeduI6J7w18JNpT28HBYT8wnuVop0AvQ6kEPh5Sx3bgSrSH82mThC6Xsm+MIQcHhxIwnk95futDbzFfQLjH8Q3ADpGKJgvegT4x7+DgUELG8xafF+136Dg5nwi5vxt93ODYSSTtGDh1y8GhxIznHPa14fwAnQMr7FDnncKcLmLynfQ+G/+0PA4ODikxnrM913cBdwlDCssGcavU8YFJSJsj0UHqHRwcUsYUdC6sd3iuP4qOtxMWM+dldED3t6Fj8kxG/APwxyLuz6GN7qCjLnbItxe18gmC13Ezj44j3RGjDXl5ZqfVFj90SJ32fXHhvdf0KSffpq0dCemX99Rt6NQeg+4F+eQC2hc0Bu0xxiOo31lpc631u7dMjnB/OBv2uBUS0M9bPml/ktK5M+l8mYK2zxzk+eEB+X57yM1/APrRoUtjZX/o6u/n7i0vs3HXLsrLynhzTQ1nzD2Ug6dW8VhXgeNnZ3mxp5cXe3qprphCpgy6BwaZVVnJohk1bNrVzc49exgQ36OqTIbK8nL6BgcZVIoyoDKTYXZVFUdUT+e5XbvY0T9SvjKToaq8nN2DQwyo4b3XspWV1Nb4OlqfOEaGU8dInKJWGZw6YJX8v9IzGeuAxoCJZSZBi3VfTupfFjE5GuVjFpC2kJcwL3WtlDKNMRiBwUqr7aZtOelrp7Tf1NcQY0Jjtdfuo6HrSqA55L4muacZWG3Rjoh7DJMPG4+gfjehQ+q2e67VSjuarT7kQl5gG63S9jb5f2HAwmWjXuZZuzXuSfszFjonmi9TRGLx4mGRhOaHPNScaXpXnBl03foNXP+XDZx66CHkDzuUaVOm8Hx3D5f9aS07+vqYV1PDTaeeyMDwMI9u7+KKtevoGRjg8iWLuaD2CPqHhnjvmt8xLZNh8ewsGcpYv2MHGws7eNchc5g3fRoDw8M8+MqrvDWb5YH35Tmn7T6Gh4dZctBsyiljw+s7eXp7gdycg5lfPZ3B4WEefnUrc6un8+gHzvZr9ltFKkySl6teBqRBJg6eFXEVOqPGQs+AdIRImLXyAq2WMs0yqKvQ+egLAfc0StkOz+Tye/Gz0u7VUqdfW1TI/eYFapOXZYWnXSulPW1Cm5YxMvWC1NMa4yVEnlMfwXhq5WVriDkefgw+7zOmZpxWW/+vDGCw7REveae0sTnGotceIM0sKyGdk80XpVSLGo0BpdThSqlTVDhOVEqhlHo8opy65KFHFKu+r366+YV9fuvq71cn/eq36sw194y6vvjndypu+aHa2rdbKaXUlp5e9dF7fq+6+vv3lvniuicU375R/fqFl/ZeW1/Yoc695wH1t55eVXfv79WrfX17f2t6Yr3i+hvV6s7Ne6899/ou9cH2+1T3wIBf03copQ6Tfsb51CqlupRS+ZAyWaXUJqXUKutao1KqLaLunLSp1qqnSynVFFC+zadOFaNtXUqp+oDfo+7fFNIe86mXZ9TGoKf3eY1Wv9aG3NcmZW261YWUb5Q2JRkP+xNGM1PfppjtDaLDqog6zPwzZduK6M9Y6JxovmR8dL9t6LQzYTrhDuA59EHQuWFs8OZnN/Hdjsf4bO5tfGTB4fsus5WV3Hb6u5leMfq8amUmQ0UmA2Vai+sfHuLiRQvJVo5svg0rBWVlDFnHPo6ZNZPza49g+549XHRULXOmTg0tv7Cmmo8vPJK+wSG/5s9A5wgjwcrXHqFKFGTVqk+40tv6uqmnwVptve3IeVbwuKtcRwIbhFfSy8ZQaVpCVLm4aPCob1F064iwQdT5SKdJkI2QCtpjSg1E1GEks7AxaE3hWWOhcyJkfBjHK+gEe4eF3Pcq0CUvZaBzYffgIFc+9iQV06fRsCjY/rywppoPHj6PnsHBUTKaSGQALJg+nTPn+fM4b2CP5QsO55iZMzhr3mGxyp+7YD4HTfXNrlNGvHQ9SSdwC8mzotZaIrdtA2gVlSvro2J1jvElGgvyCdSnlogXKI7RtdmyJUWh1WKMfuphrgjVzzC3+ojfi01uUIhBt/oE9rNS0DkR45nhtQHL9xsjGI9CR+0LPMP14KtbebGrwNGzsxxZHR4l4xMLj+QN5cGnFDJlZbHf1LKk5ctCucDMBIwhm/LA+0lTnT6rUtZalVbJRG8eI/PIjXH1zyXYdWmXNtcWQY9mqWdVTEZHwEtbx8iO41ixUmi31pI2SwHDeGoDmA5FMtBi6RwbU9jXSW6nfFdFqFpRZVjXVYDBIeZPn0amLJwNRP1+AFGdgPFA9LZt2P2NAdfNjtOKgJWwQQyYZsfrhAgGEcR06ouQlGoT9N3eHi9GLWiwXvbmiOe1ykvb4sN4WkIM5WFSzEqLkZ4g9DObCwVGdrhaipgXfmqjn7G8LoLpJOnPWOmciPF4g4GZcBBhhyT74lS+bfdugFBJZgJg2gF+fiGGCmRULrMF2hmhDuYDJt6KEkpspYCtCnREtL1VmLPN7IzPTWtI3VHjYpdfKZ9aS3o0Lg0rU5JGWqxx9i5ODTFoFbc/Y6VzbMbjNXkMy3fYFvKQ59tfRxFDcN/Q0ERmPMMJXoIkq7h3qzJsYjTLatUUMrmMGB61Iq0sEXPpTKA62Q52aagCOcJdCwzjKXhoVEew4bdQBJ06LSazUsZtFekYmc0CU289oz5G3YUixz0unWPbeLyo8kg+hJR5PYz5HJedBeXlbNrVPWonaYJhV4LJFteDM0+8RIj2pCnWIFtqtCewbRhv6kJKz/baucKkhXqP9Nda5LPrI8bSqMKdKY2f31yoT6EfadI5FuPp91wzxuatMeweXWEv5umHzuGQ7Ew2bu/S9p4QvLZ7N90Dg+PtZRpC53lPshrFmVx1FqNKsopmxzHjCds58ntZ0zSChrkWeNtojnLUWdeKfXZ9zPFLCy2Wmlgqo3IxdI7FeLo912bJ90sh9x1sMZ7XggrNqqzkS8cfh+rfwxcefTyU6dzybCfK0vrKxNgcZnQuj1GmmPLo82hPJpwQuYgVIceIEXcywfgvrY6hYtaW4EWx7VxBsH168pb6VWy/iRhzc4YrLRW3U+qql3r351yKQ+dYjGe759oc9O7y5pD7DgVqgAG0+38gLj36KC7NHc9dz27i3Hse4LldowWkP23dxjVPrOfD899ETYU+CD+kFNt272ZgYICde4ITPmzp7YWBQV7r2x2rs1t6+2BwkFdilhca9CacECtkEjb5rP71jBwpaBnDZIPSbdWmgRXS5zafFdEc+zBnxwoleL5xeIuSeupSUrOMFGDsOEG7km3yrI6UGUCa/UibzpGM5wXPtUNEonkuZHLMYSRMaiQxv3PSCdx85hk83bWDU+5cw9K72jn//ge54P4HuWPzC/znMW/h6Jlaw1u3vYurHnuS6eXlLJo5g+Yn17Nmy8va69iIIb193PTMczzVtYNFcw/h5y+8yB1/fZ7uwcEQiWoTa7dvZ9HcQ/jNS1v4UedmXh8YiGr6g+xrfI+zAp4gA9Mlk65N/q6XQWsYw1gZm8h4ZjwFRg51mjNpbeit2LXy+wkpv4De56+IKZVGGVvzMvZhn7xV5zLrHjPmmyymsyLlvrZYDCiOGpekP2nQORRlSqmvAJ/3XH+3vHSPEBwI7KPoVMZnA7+N8zAFbNixkxd7eqnIZKitqWZB9fRRZXoGBxkcHqa6ooIyoHdwiCGlmFFZsdfJb8/wMD0Dg0yvmMKUTIb+oSF2Dw0xo6JirzplY2B4mG6r/J6hIfqGhqipqGBKuNr1fuA3RU4QEwYhTmiGOMiWSFooBbLS/6T2rImOvLUoF3DwZTx1Pnr5v6OzS1wP/GvAvd8A/ksm1waSHS2YCNgJLAa2uGni4JAuMsDj7Js508Rf/l3IvaeJLaiAzrM+2fAHx3QcHErHeDrZd8v4ZPQZrPsZObvlxRJ0vBqAH05C2tzupoeDQ+kYzyBwn+f6EWjbTleI1FMBfET+bgOemUR02Urxth0HB4cQxgM6W4QXy+X7ByH3/7NIRv1oe9Bkwd04o6CDQ8lgcqdPB/7C6FCnm4Fj0N67TwKLAupYDvwC7c38BDo7w0RGN3pX73E3PRwcSivx9AA/8VG3zkEbnr8TUsdn0UbmbuBLk4Am3xFm63JqOTiUWOIBvXW8jtF5tO4DlqLPbz0FHB5Qz7nAz4QB3U2R5zgOIJ5G5wg7SvoxXGR9cdKEGIfAKKc6b8T+oDQpSX1mik1jk7PaEpbCx9AjG7OvJgxr3HQw7UXS029cxpKGJqpMFOK0P59gfMJon4/RLuOLFeVsmbP62R5lqrAZD6IyfchT5gx0/qxL0HnS/bBBHtwrKtkjxI/cN14wKBLeEPqc2sYU6jRHB8KyKpgyUR69itGpXtrwD8FhJlgr8YJ6tSV4Ke10Ln4pfExM4A5GTmTbMIcLl4VMchMzZ4XUE9RPL5YVSU8v/MJ9RqWhUYSnDzIxe1pD+r9JvsNS2aiQRcCcXvcGH/OjvZJnhIW5yAtN/TxtTawhO0aPOQdnp2Py6cHo6O+nKKWGPZkW7pPfKiTifBCutur5uJp4+JJSaqG0vSxBRP6oiP+rIyL8t0mWgrUJMy6ERfivled2SZaFpG2PyiZhMkU0BmSqWBXw7LBsB1mhwVr5O24mg7ToGbfuxiLqbIwoUy+ZJNYW8Zx8AB0bA7KOdEVkBslLOe9184zagPnn14ZRWSZsPCRSj43TZBUbAC4jOEDYZ6Qs6J2wr00gaefHwE0i3f2Z5Oez/FBvSR12dkk/tMjvTSn1xxxWTT1WrrRzlUg0fqeiTeiE1gTPzlqSVxoHSNOmZ1poJjx0igkXYkKMjAUmkV/cuDkmGV+SWEFN1lh1Bsw/04b6MOOyjS+wb2jTr6LDZTxE8BH8CuAWRtLBNAK3TQCmczdwBTpr6G5RG0mJ8ZiYu1FZCFKLc+KjGuVSrtNETmyN8ezaGC9QDn2AtIMUItuVmJ5poCNgETK2slaLcdYV0X+TQikboz3NjM5UErVINIaqUSNtaEnCeNajz2HZOMLicv/DSBZRL44EfoSOUKiAi0SaGK9oBy5Ge2AvFCaUFtOB0SeIo15Av1Q1aUg+SSIDxkHcMAxm4uUjmI7JotmQ8tiWgp5pSYydIRKyiULQSnERC1ssw3AcqacQU0KtI37c5WZ5r2IxHoCr0T45NhrQJ9L3ABcAzwfce4ZIPhlRzy4ogbifBn4GnA+cJJPhCXTanrQYT4vPJIhiPqmFlvQw13yKL02SFD4dIRPfMJ2OEjCdUtKzGOQCGHeWfcOXGsZTW+TYx2E8RkKsi8HskqQxCsSUgOu9Igk8AEy1rq9Cb6tvEBvC3fjvXp2H3h26UGxCl6Dj+1wT8sz9iSZpy3lo/6NKYE2Kk8ubxdOsYFFJ7+xUNe2Mv4wPSVP4FAjOAdVkMaaoyVxPdHCvlnFCz7qAl93Y+Rp82lFvzRG7T97g9KWW/o3KFbYdHuYeEnubP4wJ/BltMLaPQswWgixFb5l/FPg5/rmnLkB7RF+EzsP1v9KAb6F9hg4EtgCXiq2qUb6XAl9J8Rn1ll3H+3KsJnpruNUqu5DJd3TD2JyMi0Gb9DXMvtMeod5NBHqGpSkKikFtbCT7K7RpszC61Ywt82ljADPax20hSvr4tjCJS61rx8pA/qNUtlz+n+Vz/3L0MYwL0ccu7gVOBT6Hjvmzv3JWKeBmtJf1EcDX5f/zgG8ykqCwWGQtHbgxZEWMmkgmgdoqio9cV0t6Ef+SpvAJevYKi5GskL6GTfbOIqWVNOkZx7bUHrIoedPc5C0VttFnPtkJHdOyJ4UxRzMeQcn7wtIYLQt492LbeGz8B/Brz7XTgTtEomkHzgrpYA5tjL5Y/t+JtoifhN52313iiXAn8C55/qeAL6KN58bw/XTKYjYhkyTuNmkSnTsKedKL/pckhU+QPaDDI72YyZ6jdNvfadKzGLSwb5obs1AFbUt3jLHNtWNcdExG0cYABtORcPwDRIF4jlPV4nzkxT1KqYOlzPyAMjZ+qZRa4ql7sVLqa0qpzhSdAf+mlPquUurt8oxjpK03iZPgL5RSH0jBSdDPqWpVDKfCuphOaU3i4JVN6EDodf7KpuhA2BjDOY+ANoc5ENZL+fqETntJnPzC6Lk/HAi9dWQD+uxHl2zC53jHKciBMB8yl9t8HAizQsO6GPQKdJjMxORc3ejYO14D7FJZ3Rejg8a/T+wlQU6GHwT+CFwHvFmurRcVaIncf51w1dcTcOleMXp/T9q5UAzaW+XaGuBW4Frg+6Jm/boEtotchPHYqAxxVzCzzTmWXZksI6luCymv2rUx2pTUoGvc/JsoXUD7YuhZChijckuE6hY3d5c9F42vTTHqad5HuilYRuhcRBsC25xkh2mX2GxaxHBssERUqU/LZPsCereriZEQqjamoT2gLxSifl+MvN3AXfIBmCcMZCFwmLxI1egzIz1il3kZfbZlI6PzgJ0sz/iQTPxTxLZ0OzqG9AMlmkQdMURb41sSR/82KkLbGIy3xtEvbcOkySRhzlB5z4PZWVKXjWGy52IYm4tVudoOILPptF7YOIkNC5aKHjWWeWvsG4q0ixmVqymAgZu0PX454c2uZUMgkx+jSnFlgIpzg1LqICmTETFxYwzVaJ1S6iql1FLr/iSfaqXUe5RS11jPe1wp9V6l1Gyl1K1KqYdFzaIEn2zI2SW/j102jirRGKBq+aFLzmnVFdGfOKJ8rXUmy4jlm+TTGKDeNUacW7Np2SZ/t8VUrymSnvtL1WoUGhkVJs5Zupyn7iBskjHJxaR9XNVQBfxWJ2NvxsuUbbPa0Ob3DO/p9CT4EHqr/U2e65vQcXlMHOZq4JPCBZfEqPc1kWA2yOrwsnB9Y4SeKtLPXOG6b5WdNuNP9CBwlahXH0P766wRCagfh1LANmS6yI3xVeHJRKt8xMbKKBTDeEDH52lGb0t7ca/8ZuxCZegcXOehvZvnpdjpzWhP5JvFZnS6PPuNwOXAL908d3AYPyiW8RgsB64MkGjuA24EfsVInvYasbmchj6cuQidFrk8xrP2oB0BnxJbTRs6TGkGHZDsv8XYfQPwZdLz0XFwcBhnjMcYjS8UlWZRgFRyp0gffxQDsUEVsEAkqMPQHtLThBENiGF7K/Ci1POKJUWdgg46/zERX28TVesZN7wODpOf8RhUox3CLkbvLvlhC9o78mF0uNWNaFtOmDNhOTrkxtHo1DunoYOyzxCp5laRchzDcXD4O2Q8Nk4W9ecsUX/KQtSn7Whj2w50PKB+kYSmiQR0sHwb9KK37e9A527f6YbTwcExHhsVwHFou85J8vd8ksVl3oLe6XoE+D36EOs2N4QODo7xxMVsRmw6h4s0UyMSThna87lbJKAtjNh2XkaH23BwcJjA+P8BAAZ2dLKBO1SLAAAAAElFTkSuQmCC" alt="логотип компании" ></a>
                          <br><br>
                          </td>
                        </tr>
                      </table>

                      <table cellspacing="0" cellpadding="0" class="force-full-width" style="background-color:#3bcdb0;">
                        <tr>
                          <td style="background-color:#3bcdb0;">

                            <table cellspacing="0" cellpadding="0" class="force-full-width">
                              <tr>
                                <td style="font-size:40px; font-weight: 600; color: #ffffff; text-align:center;" class="mobile-spacing">
                                <div class="mobile-br">&nbsp;</div>
                                  Реферальная программа
                                <br>
                                </td>
                              </tr>
                              <tr>
                                <td style="font-size:24px; text-align:center; padding: 0 75px; color:#6f6f6f;" class="w320 mobile-spacing; ">

                                </td>
                              </tr>
                            </table>

                            <table cellspacing="0" cellpadding="0" width="600" class="force-full-width">
                              <tr>
                                <td>
                                  <img src="https://www.filepicker.io/api/file/4BgENLefRVCrgMGTAENj" style="max-width:100%; display:block;">
                                </td>
                              </tr>
                            </table>

                          </td>
                        </tr>
                      </table>

                      <table style="margin:0 auto; width:100%" cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff">
                        <tr>
                          <td style="background-color:#ffffff; padding: 30px 50px">
                              <center>
                                <table style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
                                    <tr>
                                      <td style="text-align:left; color: #6f6f6f;">
                                          <p>Здравствуйте {{login}}.</p>
                                          <p>Спешим Вас обрадовать, что по вашей реферальной ссылке зарегистрировался новый партнер {{partner_login}}</p>
                                          <p>Его контактные данные можете найти в вашем личном кабинете. Постарайтесь ему помочь разобраться в системе.</p>
                                          <p>Если это письмо не имеет к Вам никакого отношения, то просто удалите его</p>
                                      </td>
                                    </tr>
                               </table>
                              </center>
                          </td>
                        </tr>
                      </table>



                      <table cellspacing="0" cellpadding="0" bgcolor="#363636" style="width:100%;" class="force-full-width">
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#e0e0e0; font-size: 14px; text-align:center;">
                            © 2017 Crypto Investment and Token Trading<br>
                          </td>
                        </tr>
                        <tr>
                          <td style="font-size:12px;">
                            &nbsp;
                          </td>
                        </tr>
                      </table>









                      </td>
                    </tr>
                  </table>

                </center>




                </td>
              </tr>
            </table>
            </body>
            </html>
            ', 'ru']
        ]);
    }

    public function safeDown()
    {
        echo "m170823_052334_mail_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170823_052334_mail_template cannot be reverted.\n";

        return false;
    }
    */
}
