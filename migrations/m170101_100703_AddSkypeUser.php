<?php
use yii\db\Schema;
use yii\db\Migration;

class m170101_100703_AddSkypeUser extends Migration
{
    public function up()
    {
        $this->addColumn('{{%lb_user}}', 'skype', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%lb_user}}', 'skype');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
