<?php

use yii\db\Migration;

class m170327_061246_addadminLogins extends Migration
{
    public function up()
    {
        $this->update('lb_user', array(
                      'login' => 'admin'), 
                      'id=1'
        );

        $this->update('lb_user', array(
                      'login' => 'moderator'), 
                      'id=2'
        );
    }

    public function down()
    {
        echo "m170327_061246_addadminLogins cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
