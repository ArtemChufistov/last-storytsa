<?php
use yii\db\Schema;
use yii\db\Migration;

class m170216_192948_addPerfectMoneyWallet extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%user_pay_wallet_perfectmoney}}", [
            "id" => Schema::TYPE_PK,
            "user_id" => Schema::TYPE_INTEGER.'(12)',
            "pay_system_id" => Schema::TYPE_INTEGER.'(12)',
            "currency_id" => Schema::TYPE_INTEGER.'(12)',
            "wallet" => Schema::TYPE_STRING.'(255)',
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170216_192948_addPerfectMoneyWallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
