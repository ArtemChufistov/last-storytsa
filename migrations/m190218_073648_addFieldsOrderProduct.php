<?php

use yii\db\Migration;

/**
 * Class m190218_073648_addFieldsOrderProduct
 */
class m190218_073648_addFieldsOrderProduct extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_product}}', 'category_id', $this->integer()->after('product_id'));
        $this->addColumn('{{%order_product}}', 'campaign_id', $this->integer()->after('category_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190218_073648_addFieldsOrderProduct cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190218_073648_addFieldsOrderProduct cannot be reverted.\n";

        return false;
    }
    */
}
