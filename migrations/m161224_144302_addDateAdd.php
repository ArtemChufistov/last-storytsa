<?php

use yii\db\Migration;

class m161224_144302_addDateAdd extends Migration
{
    public function up()
    {
        $this->addColumn('{{%contact_message}}', 'date_add', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn('{{%contact_message}}', 'date_add');

        return false;
    }
}
