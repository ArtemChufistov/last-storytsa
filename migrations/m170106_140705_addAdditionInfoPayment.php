<?php

use yii\db\Migration;

class m170106_140705_addAdditionInfoPayment extends Migration
{
    public function up()
    {
       $this->addColumn('{{%payment}}', 'additional_info', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%payment}}', 'additional_info');

        return false;
    }
}
