<?php

use yii\db\Migration;
use yii\db\Schema;

class m170828_130946_youtube_stat_subscription extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%youtube_stat}}', 'subscribed', Schema::TYPE_SMALLINT.' NOT NULL DEFAULT 0 AFTER `comented`');
        $this->addColumn('{{%youtube_stat}}', 'date_subscribed', Schema::TYPE_DATETIME. ' AFTER `date_comented`');
    }

    public function safeDown()
    {
        echo "m170828_130946_youtube_stat_subscription cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170828_130946_youtube_stat_subscription cannot be reverted.\n";

        return false;
    }
    */
}
