<?php

use yii\db\Migration;

/**
 * Class m180802_124131_addRefreshOffersTable
 */
class m180802_124131_addRefreshOffersTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%refresh_campaign}}", [
            "id" => $this->primaryKey(),
            "refrersh_date" => $this->dateTime(),
            "start_date" => $this->dateTime(),
            "end_date" => $this->dateTime(),
        ]);

        $this->createTable("{{%refresh_offer}}", [
            "id" => $this->primaryKey(),
            "refrersh_date" => $this->dateTime(),
            "start_date" => $this->dateTime(),
            "end_date" => $this->dateTime(),
            'new_categoryes' => $this->text(),
            'new_products' => $this->text(),
            'change_products' => $this->text(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180802_124131_addRefreshOffersTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180802_124131_addRefreshOffersTable cannot be reverted.\n";

        return false;
    }
    */
}
