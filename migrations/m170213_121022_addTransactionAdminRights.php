<?php

use yii\db\Migration;

class m170213_121022_addTransactionAdminRights extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionDelete']]);
    }

    public function down()
    {
        echo "m170213_121022_addTransactionAdminRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
