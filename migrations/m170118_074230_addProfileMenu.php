<?php

use yii\db\Migration;

class m170118_074230_addProfileMenu extends Migration
{
    public function up()
    {
         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Меню личного кабинета',
            'link' => '',
            'lft' => '1',
            'rgt' => '24',
            'depth' => '0',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Вернуться на сайт',
            'link' => '/',
            'lft' => '2',
            'rgt' => '3',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Профиль',
            'link' => '/profile/office/index',
            'lft' => '4',
            'rgt' => '5',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Платёжные реквизиты',
            'link' => '/profile/office/walletdetail',
            'lft' => '6',
            'rgt' => '7',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Структура',
            'link' => '/profile/office/struct',
            'lft' => '8',
            'rgt' => '9',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Принять участие',
            'link' => '/profile/office/matrix',
            'lft' => '10',
            'rgt' => '11',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Статистика',
            'link' => '/profile/office/stat',
            'lft' => '12',
            'rgt' => '13',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Рекламные материалы',
            'link' => '/profile/office/promo',
            'lft' => '14',
            'rgt' => '15',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Оставить отзыв',
            'link' => '/profile/office/response',
            'lft' => '16',
            'rgt' => '17',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Финансы',
            'link' => '/profile/office/finance',
            'lft' => '18',
            'rgt' => '19',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Поддержка',
            'link' => '/profile/office/support',
            'lft' => '20',
            'rgt' => '21',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'103',
            'title' =>'Выход',
            'link' => '/logout',
            'lft' => '22',
            'rgt' => '23',
            'depth' => '1',
            'name' => 'profileMenu',
            'lang' => 'ru-RU',
          ));
    }

    public function down()
    {
        echo "m170118_074230_addProfileMenu cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
