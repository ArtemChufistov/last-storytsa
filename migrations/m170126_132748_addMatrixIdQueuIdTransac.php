<?php

use yii\db\Migration;

class m170126_132748_addMatrixIdQueuIdTransac extends Migration
{
    public function up()
    {
        $this->addColumn('{{%transaction}}', 'matrix_id', $this->integer()->defaultValue(NULL));
        $this->addColumn('{{%transaction}}', 'matrix_queue_id', $this->integer()->defaultValue(NULL));
    }

    public function down()
    {
        $this->dropColumn('{{%transaction}}', 'matrix_id');
        $this->dropColumn('{{%transaction}}', 'matrix_queue_id');

        return false;
    }
}
