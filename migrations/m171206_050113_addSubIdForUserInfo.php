<?php

use yii\db\Migration;

/**
 * Class m171206_050113_addSubIdForUserInfo
 */
class m171206_050113_addSubIdForUserInfo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_info}}', 'sub_id', $this->integer()->defaultValue(NULL));
        $this->addColumn('{{%user_info}}', 'status', $this->string(255)->defaultValue(NULL));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_info}}', 'sub_id');
        $this->dropColumn('{{%user_info}}', 'status');

        return false;
    }
}
