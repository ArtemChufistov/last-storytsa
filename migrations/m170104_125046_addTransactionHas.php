<?php

use yii\db\Migration;

class m170104_125046_addTransactionHas extends Migration
{
    public function up()
    {
       $this->addColumn('{{%payment}}', 'transaction_hash', $this->string(255)->notNull());
    }

    public function down()
    {
        echo "m170104_125046_addTransactionHas cannot be reverted.\n";

        return false;
    }
}
