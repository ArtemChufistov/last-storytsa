<?php

use yii\db\Migration;

/**
 * Class m180626_090030_addMasterNodeTable
 */
class m180626_090030_addMasterNodeTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable("{{%master_node}}", [
            "id" => $this->primaryKey(),
            "invest_type_id" => $this->integer()->defaultValue(NULL),
            "invest_num" => $this->integer()->defaultValue(NULL),
            "status" => $this->string(255)->defaultValue(NULL),
            "data" => $this->text(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable("{{%master_node}}");

        return false;
    }
}