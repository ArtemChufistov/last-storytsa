<?php
use yii\db\Schema;
use yii\db\Migration;

class m170120_163945_addWalletId extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_pay_wallet_bitcoin}}', 'wallet_id', Schema::TYPE_STRING . "(255) NOT NULL");
    }

    public function down()
    {
        echo "m170120_163945_addWalletId cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
