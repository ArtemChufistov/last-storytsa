<?php

use yii\db\Migration;

class m161226_093002_addactiveforjob extends Migration
{
    public function up()
    {
        $this->addColumn('{{job}}', 'cron_num', $this->string(255)->defaultValue('first'));
    }

    public function down()
    {
        $this->dropColumn('{{%job}}', 'cron_num');

        return false;
    }
}
