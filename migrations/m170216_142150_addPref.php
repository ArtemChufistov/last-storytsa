<?php
use yii\db\Schema;
use yii\db\Migration;

class m170216_142150_addPref extends Migration
{
    public function up()
    {
        $this->addColumn('{{%perfectmoney_transaction}}', 'treated', Schema::TYPE_INTEGER);
        $this->addColumn('{{%perfectmoney_transaction}}', 'date_tread', Schema::TYPE_DATETIME);
    }

    public function down()
    {
        echo "m170216_142150_addPref cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
