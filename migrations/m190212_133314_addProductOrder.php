<?php

use yii\db\Migration;

/**
 * Class m190212_133314_addProductOrder
 */
class m190212_133314_addProductOrder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        $this->createTable("{{%order_product}}", [
            "id" => $this->primaryKey(),
            "product_id" => $this->integer()->defaultValue(NULL),
            "date_add" => $this->dateTime()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190212_133314_addProductOrder cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190212_133314_addProductOrder cannot be reverted.\n";

        return false;
    }
    */
}
