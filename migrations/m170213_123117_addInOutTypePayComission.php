<?php
use yii\db\Schema;
use yii\db\Migration;

class m170213_123117_addInOutTypePayComission extends Migration
{
    public function up()
    {
       $this->addColumn('{{%pay_system}}', 'in_comis_type', Schema::TYPE_STRING . '(255) NOT NULL');
       $this->addColumn('{{%pay_system}}', 'out_comis_type', Schema::TYPE_STRING . '(255) NOT NULL');
    }

    public function down()
    {
        echo "m170213_123117_addInOutTypePayComission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
