<?php

use yii\db\Migration;

class m170101_150932_addPaywalletPassHasg extends Migration
{
    public function up()
    {
        $this->addColumn('{{%lb_user}}', 'pay_password_hash', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%lb_user}}', 'pay_password_hash');

        return false;
    }
}
