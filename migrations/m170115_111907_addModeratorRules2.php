<?php
use lowbase\user\models\AuthItem;
use yii\db\Migration;

class m170115_111907_addModeratorRules2 extends Migration
{
    public function up()
    {

        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // обратная связь
            ['contactMessageView', AuthItem::TYPE_PERMISSION, 'Обратная связь - просмотр', NULL, time(), time()],
            ['contactMessageUpdate', AuthItem::TYPE_PERMISSION, 'Обратная связь - редактирование', NULL, time(), time()],
            ['contactMessageCreate', AuthItem::TYPE_PERMISSION, 'Обратная связь - создание', NULL, time(), time()],
            ['contactMessageDelete', AuthItem::TYPE_PERMISSION, 'Обратная связь - удаление', NULL, time(), time()],
            // cron
            ['cronlogIndex', AuthItem::TYPE_PERMISSION, 'История задач - список', NULL, time(), time()],
            ['cronlogView', AuthItem::TYPE_PERMISSION, 'История задач - просмотр', NULL, time(), time()],
            ['cronlogCreate', AuthItem::TYPE_PERMISSION, 'История задач - создание', NULL, time(), time()],
            ['cronlogUpdate', AuthItem::TYPE_PERMISSION, 'История задач - редактирование', NULL, time(), time()],
            ['cronlogDelete', AuthItem::TYPE_PERMISSION, 'История задач - удаление', NULL, time(), time()],
            // Job
            ['jobIndex', AuthItem::TYPE_PERMISSION, 'Планировщик задач - список', NULL, time(), time()],
            ['jobView', AuthItem::TYPE_PERMISSION, 'Планировщик задач - просмотр', NULL, time(), time()],
            ['jobCreate', AuthItem::TYPE_PERMISSION, 'Планировщик задач - создание', NULL, time(), time()],
            ['jobUpdate', AuthItem::TYPE_PERMISSION, 'Планировщик задач - редактирование', NULL, time(), time()],
            ['jobDelete', AuthItem::TYPE_PERMISSION, 'Планировщик задач - удаление', NULL, time(), time()],
            // Event
            ['eventIndex', AuthItem::TYPE_PERMISSION, 'События - список', NULL, time(), time()],
            ['eventView', AuthItem::TYPE_PERMISSION, 'События - просмотр', NULL, time(), time()],
            ['eventCreate', AuthItem::TYPE_PERMISSION, 'События - создание', NULL, time(), time()],
            ['eventUpdate', AuthItem::TYPE_PERMISSION, 'События - редактирование', NULL, time(), time()],
            ['eventDelete', AuthItem::TYPE_PERMISSION, 'События - удаление', NULL, time(), time()],
            // mail
            ['mailIndex', AuthItem::TYPE_PERMISSION, 'История писем - список', NULL, time(), time()],
            ['mailView', AuthItem::TYPE_PERMISSION, 'История писем - просмотр', NULL, time(), time()],
            ['mailCreate', AuthItem::TYPE_PERMISSION, 'История писем - создание', NULL, time(), time()],
            ['mailUpdate', AuthItem::TYPE_PERMISSION, 'История писем - редактирование', NULL, time(), time()],
            ['mailDelete', AuthItem::TYPE_PERMISSION, 'История писем - удаление', NULL, time(), time()],
            // mailTemplate
            ['mailTemplateIndex', AuthItem::TYPE_PERMISSION, 'Шаблоны писем - список', NULL, time(), time()],
            ['mailTemplateView', AuthItem::TYPE_PERMISSION, 'Шаблоны писем - просмотр', NULL, time(), time()],
            ['mailTemplateCreate', AuthItem::TYPE_PERMISSION, 'Шаблоны писем - создание', NULL, time(), time()],
            ['mailTemplateUpdate', AuthItem::TYPE_PERMISSION, 'Шаблоны писем - редактирование', NULL, time(), time()],
            ['mailTemplateDelete', AuthItem::TYPE_PERMISSION, 'Шаблоны писем - удаление', NULL, time(), time()],
            // preference
            ['preferenceIndex', AuthItem::TYPE_PERMISSION, 'Настройки сайта - список', NULL, time(), time()],
            ['preferenceView', AuthItem::TYPE_PERMISSION, 'Настройки сайта - просмотр', NULL, time(), time()],
            ['preferenceCreate', AuthItem::TYPE_PERMISSION, 'Настройки сайта - создание', NULL, time(), time()],
            ['preferenceUpdate', AuthItem::TYPE_PERMISSION, 'Настройки сайта - редактирование', NULL, time(), time()],
            ['preferenceDelete', AuthItem::TYPE_PERMISSION, 'Настройки сайта - удаление', NULL, time(), time()],
            // menu
            ['menuIndex', AuthItem::TYPE_PERMISSION, 'Меню - список', NULL, time(), time()],
            ['menuView', AuthItem::TYPE_PERMISSION, 'Меню - просмотр', NULL, time(), time()],
            ['menuCreate', AuthItem::TYPE_PERMISSION, 'Меню - создание', NULL, time(), time()],
            ['menuUpdate', AuthItem::TYPE_PERMISSION, 'Меню - редактирование', NULL, time(), time()],
            ['menuDelete', AuthItem::TYPE_PERMISSION, 'Меню - удаление', NULL, time(), time()],
            //news
            ['newsIndex', AuthItem::TYPE_PERMISSION, 'Новости - список', NULL, time(), time()],
            ['newsView', AuthItem::TYPE_PERMISSION, 'Новости - просмотр', NULL, time(), time()],
            ['newsCreate', AuthItem::TYPE_PERMISSION, 'Новости - создание', NULL, time(), time()],
            ['newsUpdate', AuthItem::TYPE_PERMISSION, 'Новости - редактирование', NULL, time(), time()],
            ['newsDelete', AuthItem::TYPE_PERMISSION, 'Новости - удаление', NULL, time(), time()],
            // payment
            ['paymentIndex', AuthItem::TYPE_PERMISSION, 'Оплаты - список', NULL, time(), time()],
            ['paymentView', AuthItem::TYPE_PERMISSION, 'Оплаты - просмотр', NULL, time(), time()],
            ['paymentCreate', AuthItem::TYPE_PERMISSION, 'Оплаты - создание', NULL, time(), time()],
            ['paymentUpdate', AuthItem::TYPE_PERMISSION, 'Оплаты - редактирование', NULL, time(), time()],
            ['paymentDelete', AuthItem::TYPE_PERMISSION, 'Оплаты - удаление', NULL, time(), time()],
            // pay system
            ['paySystemIndex', AuthItem::TYPE_PERMISSION, 'Платёжные системы - список', NULL, time(), time()],
            ['paySystemView', AuthItem::TYPE_PERMISSION, 'Платёжные системы - просмотр', NULL, time(), time()],
            ['paySystemCreate', AuthItem::TYPE_PERMISSION, 'Платёжные системы - создание', NULL, time(), time()],
            ['paySystemUpdate', AuthItem::TYPE_PERMISSION, 'Платёжные системы - редактирование', NULL, time(), time()],
            ['paySystemDelete', AuthItem::TYPE_PERMISSION, 'Платёжные системы - удаление', NULL, time(), time()],
            // paySystemСurrency
            ['paySystemСurrencyIndex', AuthItem::TYPE_PERMISSION, 'Валюты платёжных систем - список', NULL, time(), time()],
            ['paySystemСurrencyView', AuthItem::TYPE_PERMISSION, 'Валюты платёжных систем - просмотр', NULL, time(), time()],
            ['paySystemСurrencyCreate', AuthItem::TYPE_PERMISSION, 'Валюты платёжных систем - создание', NULL, time(), time()],
            ['paySystemСurrencyUpdate', AuthItem::TYPE_PERMISSION, 'Валюты платёжных систем - редактирование', NULL, time(), time()],
            ['paySystemСurrencyDelete', AuthItem::TYPE_PERMISSION, 'Валюты платёжных систем - удаление', NULL, time(), time()],
            // userPayWalletBitcoin
            ['userPayWalletBitcoinIndex', AuthItem::TYPE_PERMISSION, 'Кошелёк платёжной системы BitCoin - список', NULL, time(), time()],
            ['userPayWalletBitcoinView', AuthItem::TYPE_PERMISSION, 'Кошелёк платёжной системы BitCoin - просмотр', NULL, time(), time()],
            ['userPayWalletBitcoinCreate', AuthItem::TYPE_PERMISSION, 'Кошелёк платёжной системы BitCoin - создание', NULL, time(), time()],
            ['userPayWalletBitcoinUpdate', AuthItem::TYPE_PERMISSION, 'Кошелёк платёжной системы BitCoin - редактирование', NULL, time(), time()],
            ['userPayWalletBitcoinDelete', AuthItem::TYPE_PERMISSION, 'Кошелёк платёжной системы BitCoin - удаление', NULL, time(), time()],
            // searhbyloginIndex
            ['searhbyloginIndex', AuthItem::TYPE_PERMISSION, 'Поиск по логину', NULL, time(), time()],
            // subscriber
            ['subscriberIndex', AuthItem::TYPE_PERMISSION, 'Подписчики - список', NULL, time(), time()],
            ['subscriberView', AuthItem::TYPE_PERMISSION, 'Подписчики - просмотр', NULL, time(), time()],
            ['subscriberCreate', AuthItem::TYPE_PERMISSION, 'Подписчики - создание', NULL, time(), time()],
            ['subscriberUpdate', AuthItem::TYPE_PERMISSION, 'Подписчики - редактирование', NULL, time(), time()],
            ['subscriberDelete', AuthItem::TYPE_PERMISSION, 'Подписчики - удаление', NULL, time(), time()],
            // response
            ['responseIndex', AuthItem::TYPE_PERMISSION, 'Отзывы - список', NULL, time(), time()],
            ['responseView', AuthItem::TYPE_PERMISSION, 'Отзывы - просмотр', NULL, time(), time()],
            ['responseCreate', AuthItem::TYPE_PERMISSION, 'Отзывы - создание', NULL, time(), time()],
            ['responseUpdate', AuthItem::TYPE_PERMISSION, 'Отзывы - редактирование', NULL, time(), time()],
            ['responseDelete', AuthItem::TYPE_PERMISSION, 'Отзывы - удаление', NULL, time(), time()],
            // statPaymentIndex
            ['statPaymentIndex', AuthItem::TYPE_PERMISSION, 'Статистика оплат', NULL, time(), time()],
            ['statRegIndex', AuthItem::TYPE_PERMISSION, 'Статистика регистраций', NULL, time(), time()],
            // tickets
            ['ticketIndex', AuthItem::TYPE_PERMISSION, 'Поддержка, тикеты - список', NULL, time(), time()],
            ['ticketView', AuthItem::TYPE_PERMISSION, 'Поддержка, тикеты - просмотр', NULL, time(), time()],
            ['ticketCreate', AuthItem::TYPE_PERMISSION, 'Поддержка, тикеты - создание', NULL, time(), time()],
            ['ticketUpdate', AuthItem::TYPE_PERMISSION, 'Поддержка, тикеты - редактирование', NULL, time(), time()],
            ['ticketDelete', AuthItem::TYPE_PERMISSION, 'Поддержка, тикеты - удаление', NULL, time(), time()],
            // ticketImportance
            ['ticketImportanceIndex', AuthItem::TYPE_PERMISSION, 'Приоритеты тикетов - список', NULL, time(), time()],
            ['ticketImportanceView', AuthItem::TYPE_PERMISSION, 'Приоритеты тикетов - просмотр', NULL, time(), time()],
            ['ticketImportanceCreate', AuthItem::TYPE_PERMISSION, 'Приоритеты тикетов - создание', NULL, time(), time()],
            ['ticketImportanceUpdate', AuthItem::TYPE_PERMISSION, 'Приоритеты тикетов - редактирование', NULL, time(), time()],
            ['ticketImportanceDelete', AuthItem::TYPE_PERMISSION, 'Приоритеты тикетов - удаление', NULL, time(), time()],
            // ticketMessageIndex
            ['ticketMessageIndex', AuthItem::TYPE_PERMISSION, 'Сообщения тикетов - список', NULL, time(), time()],
            ['ticketMessageView', AuthItem::TYPE_PERMISSION, 'Сообщения тикетов - просмотр', NULL, time(), time()],
            ['ticketMessageCreate', AuthItem::TYPE_PERMISSION, 'Сообщения тикетов - создание', NULL, time(), time()],
            ['ticketMessageUpdate', AuthItem::TYPE_PERMISSION, 'Сообщения тикетов - редактирование', NULL, time(), time()],
            ['ticketMessageDelete', AuthItem::TYPE_PERMISSION, 'Сообщения тикетов - удаление', NULL, time(), time()],
            // ticket status
            ['ticketStatusIndex', AuthItem::TYPE_PERMISSION, 'Статусы тикетов - список', NULL, time(), time()],
            ['ticketStatusView', AuthItem::TYPE_PERMISSION, 'Статусы тикетов - просмотр', NULL, time(), time()],
            ['ticketStatusCreate', AuthItem::TYPE_PERMISSION, 'Статусы тикетов - создание', NULL, time(), time()],
            ['ticketStatusUpdate', AuthItem::TYPE_PERMISSION, 'Статусы тикетов - редактирование', NULL, time(), time()],
            ['ticketStatusDelete', AuthItem::TYPE_PERMISSION, 'Статусы тикетов - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [
            ['moderator', 'contactMessageUpdate'],
            ['moderator', 'contactMessageCreate'],
            ['moderator', 'contactMessageDelete'],
            ['moderator', 'eventIndex'],
            ['moderator', 'eventView'],
            ['moderator', 'eventCreate'],
            ['moderator', 'eventUpdate'],
            ['moderator', 'eventDelete'],
            ['moderator', 'menuIndex'],
            ['moderator', 'menuView'],
            ['moderator', 'menuCreate'],
            ['moderator', 'menuUpdate'],
            ['moderator', 'menuDelete'],
            ['moderator', 'newsIndex'],
            ['moderator', 'newsView'],
            ['moderator', 'newsCreate'],
            ['moderator', 'newsUpdate'],
            ['moderator', 'newsDelete'],
            ['moderator', 'searhbyloginIndex'],
            ['moderator', 'subscriberIndex'],
            ['moderator', 'subscriberView'],
            ['moderator', 'subscriberCreate'],
            ['moderator', 'subscriberUpdate'],
            ['moderator', 'subscriberDelete'],
            ['moderator', 'responseIndex'],
            ['moderator', 'responseView'],
            ['moderator', 'responseCreate'],
            ['moderator', 'responseUpdate'],
            ['moderator', 'responseDelete'],
            ['moderator', 'statPaymentIndex'],
            ['moderator', 'statRegIndex'],
            ['moderator', 'ticketIndex'],
            ['moderator', 'ticketView'],
            ['moderator', 'ticketCreate'],
            ['moderator', 'ticketUpdate'],
            ['moderator', 'ticketDelete'],
            ['moderator', 'ticketImportanceIndex'],
            ['moderator', 'ticketImportanceView'],
            ['moderator', 'ticketImportanceCreate'],
            ['moderator', 'ticketImportanceUpdate'],
            ['moderator', 'ticketImportanceDelete'],
            ['moderator', 'ticketMessageIndex'],
            ['moderator', 'ticketMessageView'],
            ['moderator', 'ticketMessageCreate'],
            ['moderator', 'ticketMessageUpdate'],
            ['moderator', 'ticketMessageDelete'],
            ['moderator', 'ticketStatusIndex'],
            ['moderator', 'ticketStatusView'],
            ['moderator', 'ticketStatusCreate'],
            ['moderator', 'ticketStatusUpdate'],
            ['moderator', 'ticketStatusDelete'],
        ]);
    }

    public function down()
    {
        echo "m170115_111907_addModeratorRules2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
