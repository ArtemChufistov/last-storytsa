<?php
use yii\db\Migration;

/**
 * Class m180626_034051_addInvestNum
 */
class m180626_034051_addInvestNum extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%user_invest}}', 'invest_num', $this->integer()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m180626_034051_addInvestNum cannot be reverted.\n";

        return false;
    }
}
