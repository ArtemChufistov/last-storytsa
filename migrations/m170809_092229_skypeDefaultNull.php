<?php

use yii\db\Migration;

class m170809_092229_skypeDefaultNull extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{lb_user}}', 'skype');
        $this->addColumn('{{lb_user}}', 'skype', 'VARCHAR(255) DEFAULT \'\' AFTER `changing_email`');
    }

    public function safeDown()
    {
        echo "m170809_092229_skypeDefaultNull cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170809_092229_skypeDefaultNull cannot be reverted.\n";

        return false;
    }
    */
}
