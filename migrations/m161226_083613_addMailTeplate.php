<?php

use yii\db\Migration;

class m161226_083613_addMailTeplate extends Migration
{
    public function up()
    {

        $this->addColumn('{{%mail_template}}', 'lang', $this->string(255)->defaultValue(NULL));

        $this->insert('{{%mail_template}}',array(
            'type' =>'registrationSuccessConfirm',
            'content' => '<p>Здравствуйте {{firstName}}</p><p>Для подтверждения адреса и первичной авторизации пройдите по ссылке: {{confirmLink}}</p><p>Если Вы не регистрировались у на нашем сайте, то просто удалите это письмо.</p>',
            'lang' => 'ru'
        ));
    }

    public function down()
    {
        $this->dropColumn('{{%mail_template}}', 'lang');

        return false;
    }
}