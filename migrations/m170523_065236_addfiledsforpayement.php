<?php
use yii\db\Schema;
use yii\db\Migration;

class m170523_065236_addfiledsforpayement extends Migration
{
    public function safeUp()
    {

        $this->addColumn('{{%payment}}', 'to_currency_id', 'INTEGER(12) AFTER `currency_id`');
        $this->addColumn('{{%payment}}', 'to_pay_system_id', 'INTEGER(12) AFTER `pay_system_id`');
        $this->addColumn('{{%payment}}', 'to_sum', 'VARCHAR(100) AFTER `sum`');
    }

    public function safeDown()
    {
        echo "m170523_065236_addfiledsforpayement cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170523_065236_addfiledsforpayement cannot be reverted.\n";

        return false;
    }
    */
}
