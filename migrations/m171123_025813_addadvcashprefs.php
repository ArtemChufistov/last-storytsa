<?php

use yii\db\Migration;

/**
 * Class m171123_025813_addadvcashprefs
 */
class m171123_025813_addadvcashprefs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%preference}}',array(
            'key' =>'advCashSciEmail',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'advCashSciName',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'advCashSciSecret',
            'value' =>'1',
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171123_025813_addadvcashprefs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_025813_addadvcashprefs cannot be reverted.\n";

        return false;
    }
    */
}
