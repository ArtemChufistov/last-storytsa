<?php

use yii\db\Migration;

class m161231_123036_addCHangingEmailUser extends Migration
{
    public function up()
    {
        $this->addColumn('{{%lb_user}}', 'changing_email', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%lb_user}}', 'changing_email');

        return false;
    }
}
