<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170603_045328_addRules extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['statInvestIndex', AuthItem::TYPE_PERMISSION, 'Статистика инвестиций', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'statInvestIndex']]);
    }

    public function safeDown()
    {
        echo "m170603_045328_addRules cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170603_045328_addRules cannot be reverted.\n";

        return false;
    }
    */
}
