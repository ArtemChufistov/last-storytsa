<?php

use yii\db\Migration;

class m170519_071451_addInvestTypeIdFOrTransaction extends Migration
{
    public function up()
    {
        $this->addColumn('{{%transaction}}', 'invest_type_id', $this->integer()->defaultValue(NULL));
    }

    public function down()
    {
        $this->dropColumn('{{%transaction}}', 'invest_type_id');

        return false;
    }
}
