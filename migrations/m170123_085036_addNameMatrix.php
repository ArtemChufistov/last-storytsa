<?php
use yii\db\Schema;
use yii\db\Migration;

class m170123_085036_addNameMatrix extends Migration
{
    public function up()
    {
        $this->addColumn('{{%matrix}}', 'name', Schema::TYPE_STRING . "(255) NOT NULL");
    }

    public function down()
    {
        echo "m170123_085036_addNameMatrix cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
