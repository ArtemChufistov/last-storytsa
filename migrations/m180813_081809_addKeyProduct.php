<?php

use yii\db\Migration;

/**
 * Class m180813_081809_addKeyProduct
 */
class m180813_081809_addKeyProduct extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'key', $this->string(255)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180813_081809_addKeyProduct cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180813_081809_addKeyProduct cannot be reverted.\n";

        return false;
    }
    */
}
