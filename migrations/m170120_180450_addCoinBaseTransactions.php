<?php
use yii\db\Schema;
use yii\db\Migration;

class m170120_180450_addCoinBaseTransactions extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%coinbase_transaction}}", [
            "id" => Schema::TYPE_PK,
            "wallet_id" => Schema::TYPE_STRING.'(255) NOT NULL',
            "type" => Schema::TYPE_STRING.'(255)',
            "amount" => Schema::TYPE_FLOAT,
            "description" => Schema::TYPE_STRING.'(255)',
            "created_at" => Schema::TYPE_DATETIME,
            "updated_at" => Schema::TYPE_DATETIME,
            "resource" => Schema::TYPE_STRING.'(255)',
            "resource_path" => Schema::TYPE_STRING.'(255)',
            "instant_exchange" => Schema::TYPE_STRING.'(255)',
            "status" => Schema::TYPE_STRING.'(255)',
            "network_status" => Schema::TYPE_STRING.'(255)',
            "network_hash" => Schema::TYPE_STRING.'(255)',
            "processed" => Schema::TYPE_INTEGER. ' DEFAULT 0',
            "payment_id" => Schema::TYPE_INTEGER
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170120_180450_addCoinBaseTransactions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
