<?php
use yii\db\Schema;
use yii\db\Migration;

class m170122_091933_addErrorForPayment extends Migration
{
    public function up()
    {
        $this->addColumn('{{%payment}}', 'error', Schema::TYPE_TEXT);
    }

    public function down()
    {
        echo "m170122_091933_addErrorForPayment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
