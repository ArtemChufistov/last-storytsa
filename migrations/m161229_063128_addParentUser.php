<?php

use yii\db\Migration;

class m161229_063128_addParentUser extends Migration
{
    public function up()
    {
        $this->addColumn('{{%lb_user}}', 'parent_id', $this->integer()->defaultValue(NULL));
    }

    public function down()
    {
        $this->dropColumn('{{%lb_user}}', 'parent_id');

        return false;
    }
}
