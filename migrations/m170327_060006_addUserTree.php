<?php

use yii\db\Migration;

class m170327_060006_addUserTree extends Migration
{
    public function up()
    {
        $this->batchInsert('user_tree', ['parent', 'child', 'depth'], [[1, 1, 0]]);
        $this->batchInsert('user_tree', ['parent', 'child', 'depth'], [[1, 2, 1]]);
        $this->batchInsert('user_tree', ['parent', 'child', 'depth'], [[2, 2, 0]]);
    }

    public function down()
    {
        echo "m170327_060006_addUserTree cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
