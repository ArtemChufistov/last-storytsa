<?php

use yii\db\Migration;

class m170116_032617_addADminRules3 extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletBitcoinIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletBitcoinView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletBitcoinCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletBitcoinUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletBitcoinDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'searhbyloginIndex']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'subscriberIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'subscriberView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'subscriberCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'subscriberUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'subscriberDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'statPaymentIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'statRegIndex']]);
    }

    public function down()
    {
        echo "m170116_032617_addADminRules3 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
