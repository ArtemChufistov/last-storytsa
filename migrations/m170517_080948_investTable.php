<?php
use yii\db\Schema;
use yii\db\Migration;

class m170517_080948_investTable extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%user_invest}}", [
            "id" => Schema::TYPE_PK,
            "user_id" => Schema::TYPE_INTEGER.'(12)',
            "invest_type_id" => Schema::TYPE_INTEGER.'(12)',
            "currency_id" => Schema::TYPE_INTEGER.'(12)',
            "sum" => Schema::TYPE_INTEGER.'(12)',
            "paid_sum" => Schema::TYPE_FLOAT,
            "closed_percent" => Schema::TYPE_FLOAT,
            "date_add" => Schema::TYPE_DATETIME,
            "date_update" => Schema::TYPE_DATETIME,
        ], $tableOptions);

        $this->createTable("{{%invest_type}}", [
            "id" => Schema::TYPE_PK,
            "key" => Schema::TYPE_STRING.'(255)',
            "name" => Schema::TYPE_STRING.'(255)',
            "percent" => Schema::TYPE_FLOAT, // fix or floating
            "sum" => Schema::TYPE_STRING.'(255)', // fix or floating
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170216_192948_addPerfectMoneyWallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
