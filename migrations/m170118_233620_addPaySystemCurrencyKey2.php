<?php

use yii\db\Migration;

class m170118_233620_addPaySystemCurrencyKey2 extends Migration
{
    public function up()
    {
        $this->addForeignKey('pay_system_currency_fk2', '{{%pay_system_currency}}', 'currency_id', '{{%currency}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m170118_233620_addPaySystemCurrencyKey2 cannot be reverted.\n";

        return false;
    }
}
