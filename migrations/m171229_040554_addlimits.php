<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171229_040554_addlimits
 */
class m171229_040554_addlimits extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%currency_course}}', 'min_sum', Schema::TYPE_FLOAT, ' DEFAULT 0');
        $this->addColumn('{{%currency_course}}', 'one_active', Schema::TYPE_INTEGER . '(11) DEFAULT 0');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171229_040554_addlimits cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171229_040554_addlimits cannot be reverted.\n";

        return false;
    }
    */
}
