<?php

use yii\db\Migration;
use yii\db\Schema;

class m180712_064004_add_table_coinmarketcap_listings extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // таблица курсов валют
        $this->createTable('{{%m_coinmarketcap_listings}}', [
            'id' => Schema::TYPE_STRING . '(100) NOT NULL',
            'name' => Schema::TYPE_STRING . '(100) NOT NULL',
            'symbol' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'website_slug' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
        ], $tableOptions);

        $this->createIndex('m_coinmarketcap_listings_website_slug_index', '{{%m_coinmarketcap_listings}}', 'website_slug', true);
        $this->createIndex('m_coinmarketcap_ticker_id_index', '{{%m_coinmarketcap_ticker}}', 'id', true);

        $this->alterColumn('{{%m_coinmarketcap_listings}}', 'id', Schema::TYPE_INTEGER . '(11) NOT NULL');

        $this->addColumn('{{%m_account}}', 'url', Schema::TYPE_STRING . '(255) NULL DEFAULT NULL');
        $this->addColumn('{{%m_account}}', 'comment', Schema::TYPE_STRING . '(2000) NULL DEFAULT NULL');

    }

    public function safeDown()
    {
        echo "m180712_064004_add_table_coinmarketcap_listings cannot be reverted.\n";

        return false;
    }
}
