<?php

use yii\db\Migration;

/**
 * Class m190213_132028_addProductInfo
 */
class m190213_132028_addProductInfo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'description', $this->text()->after('name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190213_132028_addProductInfo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190213_132028_addProductInfo cannot be reverted.\n";

        return false;
    }
    */
}
