<?php
use yii\db\Schema;
use yii\db\Migration;

class m170125_121709_addPaymentLog extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%payment_log}}', [
            'id' => $this->primaryKey(),
            'payment_id' => $this->integer(),
            'status' => $this->string(255)->notNull(),
            'date_add' => $this->dateTime(),
            'additional_info' => $this->text()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%payment_log}}');

        return false;
    }
}
