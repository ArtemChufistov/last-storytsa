<?php

use yii\db\Migration;

class m170116_145124_addPendingPaymentTime extends Migration
{
    public function up()
    {
        $this->insert('{{%preference}}',array(
            'key' =>'pendingPaymentTime',
            'value' =>'86400',
        ));
    }

    public function down()
    {
        echo "m170116_145124_addPendingPaymentTime cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
