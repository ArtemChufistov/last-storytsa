<?php

use yii\db\Migration;

/**
 * Class m180813_052601_addFiledsProduct
 */
class m180813_052601_addFiledsProduct extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'discount', $this->integer()->defaultValue(0));
        $this->addColumn('{{%product}}', 'new', $this->integer()->defaultValue(0));
        $this->addColumn('{{%product}}', 'special', $this->integer()->defaultValue(0));
        $this->addColumn('{{%product}}', 'bestseller', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180813_052601_addFiledsProduct cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180813_052601_addFiledsProduct cannot be reverted.\n";

        return false;
    }
    */
}
