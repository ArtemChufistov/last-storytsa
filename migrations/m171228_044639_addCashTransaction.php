<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171228_044639_addCashTransaction
 */
class m171228_044639_addCashTransaction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable("{{%cash_transaction}}", [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING,
            "address" => Schema::TYPE_STRING,
            "email" => Schema::TYPE_STRING,
            "phone" => Schema::TYPE_STRING,
            "skype" => Schema::TYPE_STRING,
            "amount" => Schema::TYPE_FLOAT,
            "status" => Schema::TYPE_STRING,
            "payment_id" => Schema::TYPE_INTEGER,
            "date_add" => Schema::TYPE_DATETIME,
            "description" => Schema::TYPE_TEXT,
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171228_044639_addCashTransaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171228_044639_addCashTransaction cannot be reverted.\n";

        return false;
    }
    */
}
