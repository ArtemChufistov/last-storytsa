<?php

use yii\db\Migration;

/**
 * Class m180803_054106_addoriginal_products
 */
class m180803_054106_addoriginal_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%campaign}}', 'original_products', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180803_054106_addoriginal_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_054106_addoriginal_products cannot be reverted.\n";

        return false;
    }
    */
}
