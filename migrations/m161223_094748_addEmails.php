<?php

use yii\db\Migration;

class m161223_094748_addEmails extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%mail}}', [
            'id' => $this->primaryKey(),
            'template_id' => $this->integer()->defaultValue(NULL),
            'title' => $this->string(255)->notNull(),
            'headers' => $this->text(),
            'from' => $this->string(255)->defaultValue(NULL),
            'to' => $this->string(255)->notNull(),
            'content' => $this->text(),
            'date_add' => $this->dateTime(),
            'date_send' => $this->dateTime(),
            'addition_info' => $this->text()->defaultValue(NULL),
        ], $tableOptions);

        $this->createTable('{{%mail_template}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(255)->notNull(),
            'content' => $this->text(),
        ], $tableOptions);
    }

    public function down()
    {
         $this->dropTable('{{%mail}}');
         $this->dropTable('{{%mail_template}}');

        return false;
    }
}
