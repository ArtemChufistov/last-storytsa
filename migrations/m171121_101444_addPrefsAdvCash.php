<?php

use yii\db\Migration;

/**
 * Class m171121_101444_addPrefsAdvCash
 */
class m171121_101444_addPrefsAdvCash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%preference}}',array(
            'key' =>'advCashApiName',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'advCashAccountEmail',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'advCashApiPassword',
            'value' =>'1',
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171121_101444_addPrefsAdvCash cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171121_101444_addPrefsAdvCash cannot be reverted.\n";

        return false;
    }
    */
}
