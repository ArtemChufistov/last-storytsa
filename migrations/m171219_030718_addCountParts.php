<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171219_030718_addCountParts
 */
class m171219_030718_addCountParts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_info}}', 'count_parts', Schema::TYPE_INTEGER);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171219_030718_addCountParts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171219_030718_addCountParts cannot be reverted.\n";

        return false;
    }
    */
}
