<?php

use yii\db\Migration;

class m161226_103906_preferenecsTable extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%preference}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
            'value' => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->insert('{{%preference}}',array(
            'key' =>'sendMailFrequency',
            'value' =>'5',
        ));
    }

    public function down()
    {
        $this->dropTable('{{%preference}}');

        return false;
    }
}
