<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m171002_050132_addUserShowedVideos
 */
class m171002_050132_addUserShowedVideos extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      $this->createTable("{{%user_showed_video}}", [
          "id" => Schema::TYPE_PK,
          "user_id" => Schema::TYPE_INTEGER,
          "count" => Schema::TYPE_INTEGER,
          "date" => Schema::TYPE_DATETIME,
      ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171002_050132_addUserShowedVideos cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_050132_addUserShowedVideos cannot be reverted.\n";

        return false;
    }
    */
}
