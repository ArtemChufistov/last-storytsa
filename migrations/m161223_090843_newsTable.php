<?php

use yii\db\Migration;

class m161223_090843_newsTable extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'lang' => $this->string(255)->notNull(),
            'slug' => $this->string(255)->notNull(),
            'date' => $this->dateTime(),
            'title' => $this->string(255)->notNull(),
            'smallDescription' => $this->text()->notNull(),
            'fullDescription' => $this->text()->notNull(),

        ], $tableOptions);
    }

    public function down()
    {
         $this->dropTable('{{%news}}');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
