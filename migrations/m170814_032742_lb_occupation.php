<?php

use yii\db\Migration;
use yii\db\Schema;

class m170814_032742_lb_occupation extends Migration
{
    public function safeUp()
    {
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      $this->createTable("{{%lb_occupation}}", [
          "id" => Schema::TYPE_PK,
          "title" => Schema::TYPE_STRING.'(100)',
      ], $tableOptions);

      $this->batchInsert('lb_occupation', ['title'], [
          ['Auto business'],
          ['Banks'],
          ['Accounting - Finance'],
          ['Hotels - Restaurants - Cafes'],
          ['Government agencies'],
          ['Consulting - Analytics - Audit'],
          ['Culture - Show business - Entertainment'],
          ['Logistics'],
          ['Marketing - Advertising - PR'],
          ['Science - Education'],
          ['The property'],
          ['Public organizations'],
          ['Security'],
          ['Sales'],
          ['Production'],
          ['Agriculture'],
          ['Sport'],
          ['Insurance'],
          ['Building'],
          ['Telecommunications'],
          ['Trade'],
          ['Tourism - Travel'],
          ['Lawyers'],
          ['IT']
      ]);
    }

    public function safeDown()
    {
        $this->dropTable("{{%lb_occupation}}");

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170814_032742_lb_occupation cannot be reverted.\n";

        return false;
    }
    */
}
