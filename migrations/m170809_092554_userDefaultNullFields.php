<?php

use yii\db\Migration;

class m170809_092554_userDefaultNullFields extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{lb_user}}', 'changing_email');
        $this->addColumn('{{lb_user}}', 'changing_email', 'VARCHAR(255) DEFAULT \'\' AFTER `last_date_email_confirm_token`');

        $this->dropColumn('{{lb_user}}', 'pay_password_hash');
        $this->addColumn('{{lb_user}}', 'pay_password_hash', 'VARCHAR(255) DEFAULT \'\' AFTER `skype`');
    }

    public function safeDown()
    {
        echo "m170809_092554_userDefaultNullFields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170809_092554_userDefaultNullFields cannot be reverted.\n";

        return false;
    }
    */
}
