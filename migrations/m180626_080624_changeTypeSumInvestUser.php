<?php
use yii\db\Migration;

/**
 * Class m180626_080624_changeTypeSumInvestUser
 */
class m180626_080624_changeTypeSumInvestUser extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropColumn('{{%user_invest}}', 'sum');
        $this->addColumn('{{%user_invest}}', 'sum', $this->float()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m180626_080624_changeTypeSumInvestUser cannot be reverted.\n";

        return false;
    }
}
