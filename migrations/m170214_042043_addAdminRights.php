<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170214_042043_addAdminRights extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['MatrixIndex', AuthItem::TYPE_PERMISSION, 'Управление вершинами - список', NULL, time(), time()],
            ['MatrixView', AuthItem::TYPE_PERMISSION, 'Управление вершинами - просмотр', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixView']]);

    }

    public function down()
    {
        echo "m170214_042043_addAdminRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
