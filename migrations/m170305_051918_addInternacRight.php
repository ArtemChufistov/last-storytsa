<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170305_051918_addInternacRight extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // Языки сайта
            ['langIndex', AuthItem::TYPE_PERMISSION, 'Языки сайта - список', NULL, time(), time()],
            ['langView', AuthItem::TYPE_PERMISSION, 'Языки сайта - просмотр', NULL, time(), time()],
            ['langCreate', AuthItem::TYPE_PERMISSION, 'Языки сайта - добавление', NULL, time(), time()],
            ['langUpdate', AuthItem::TYPE_PERMISSION, 'Языки сайта - редактирование', NULL, time(), time()],
            ['langDelete', AuthItem::TYPE_PERMISSION, 'Языки сайта - удаление', NULL, time(), time()],
            // Языки сайта
            ['langMessageIndex', AuthItem::TYPE_PERMISSION, 'Переводы - список', NULL, time(), time()],
            ['langMessageView', AuthItem::TYPE_PERMISSION, 'Переводы - просмотр', NULL, time(), time()],
            ['langMessageCreate', AuthItem::TYPE_PERMISSION, 'Переводы - добавление', NULL, time(), time()],
            ['langMessageUpdate', AuthItem::TYPE_PERMISSION, 'Переводы - редактирование', NULL, time(), time()],
            ['langMessageDelete', AuthItem::TYPE_PERMISSION, 'Переводы - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langMessageIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langMessageView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langMessageCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langMessageUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'langMessageDelete']]);
    }

    public function down()
    {
        echo "m170305_051918_addInternacRight cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
