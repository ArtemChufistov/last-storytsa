<?php

use yii\db\Migration;

class m161231_034511_addTablesForResponseSupport extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%response}}", [
            "id" => $this->primaryKey(),
            "user_id" => $this->integer()->notNull(),
            "name" => $this->string(255)->notNull(),
            "text" => $this->text(),
            "img1" => $this->string(255)->notNull(),
            "img2" => $this->string(255)->notNull(),
            "img3" => $this->string(255)->notNull(),
            "video1" => $this->string(255)->notNull(),
            "video2" => $this->string(255)->notNull(),
            "video3" => $this->string(255)->notNull(),
            'date' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable("{{%ticket}}", [
            "id" => $this->primaryKey(),
            "user_id" => $this->integer()->notNull(),
            "doing_user_id" => $this->integer()->defaultValue(NULL),
            "text" => $this->text(),
            "status_id" => $this->integer()->notNull(),
            "department" => $this->string(255)->notNull(),
            "importance_id" => $this->integer()->defaultValue(NULL),
            "dateAdd" => $this->dateTime(),
        ], $tableOptions);

        $this->createTable("{{%ticket_message}}", [
            "id" => $this->primaryKey(),
            "user_id" => $this->integer()->notNull(),
            "text" => $this->text(),
            "dateAdd" => $this->dateTime(),
        ], $tableOptions);
    
        $this->createTable("{{%ticket_status}}", [
            "id" => $this->primaryKey(),
            "name" => $this->string(255)->notNull(),
            "color" => $this->string(255)->notNull()
        ], $tableOptions);

        $this->createTable("{{%ticket_importance}}", [
            "id" => $this->primaryKey(),
            "name" => $this->string(255)->notNull(),
            "color" => $this->string(255)->notNull()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%reponse}}");
        $this->dropTable("{{%ticket}}");
        $this->dropTable("{{%ticket_message}}");
        $this->dropTable("{{%ticket_status}}");
        $this->dropTable("{{%ticket_importance}}");

        return false;
    }
}
