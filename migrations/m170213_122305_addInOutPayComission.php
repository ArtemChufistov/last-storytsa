<?php
use yii\db\Schema;
use yii\db\Migration;

class m170213_122305_addInOutPayComission extends Migration
{
    public function up()
    {
       $this->addColumn('{{%pay_system}}', 'in_comission', Schema::TYPE_FLOAT);
       $this->addColumn('{{%pay_system}}', 'out_comission', Schema::TYPE_FLOAT);
    }

    public function down()
    {
        echo "m170213_122305_addInOutPayComission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
