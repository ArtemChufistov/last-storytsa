<?php

use yii\db\Migration;

/**
 * Class m180131_130017_addUserInfoStructSum
 */
class m180131_130017_addUserInfoStructSum extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%user_info}}', 'struct_sum', $this->float()->defaultValue(0));
        $this->addColumn('{{%user_info}}', 'struct_data', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%user_info}}', 'struct_sum');
        $this->dropColumn('{{%user_info}}', 'struct_data');

        return false;
    }

}
