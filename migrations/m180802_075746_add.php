<?php

use yii\db\Migration;

/**
 * Class m180802_075746_add
 */
class m180802_075746_add extends Migration
{
    // Closure table name
    public $closureTbl = "category_tree";

    // Name of the table to which you connect the behavior
    public $relativeTbl = "category";

    public function up()
    {
        $this->createTable("{{%{$this->relativeTbl}}}", [
            "id" => $this->primaryKey(),
            "key" => $this->string(255)->notNull(),
            "name" => $this->string(255)->notNull(),
            "campaign_id" => $this->integer()->notNull(),
            "offer_id" => $this->integer()->notNull(),
        ]);

        $this->createTable("{{%{$this->closureTbl}}}", [
            "parent" => $this->integer()->notNull(),
            "child" => $this->integer()->notNull(),
            "depth" => $this->integer()->notNull()->defaultValue(0),
        ]);
        $this->addPrimaryKey("PK_{$this->closureTbl}", "{{%{$this->closureTbl}}}", ["parent", "child"]);
        $this->createIndex("FK_{$this->closureTbl}_child_{$this->relativeTbl}", "{{%{$this->closureTbl}}}", "child");
        $this->addForeignKey("FK_{$this->closureTbl}_child_{$this->relativeTbl}",
            "{{%{$this->closureTbl}}}", "child",
            "{{%{$this->relativeTbl}}}", "id",
            "CASCADE"
        );
        $this->addForeignKey("FK_{$this->closureTbl}_parent_{$this->relativeTbl}",
            "{{%{$this->closureTbl}}}", "parent",
            "{{%{$this->relativeTbl}}}", "id",
            "CASCADE"
        );

        $this->createTable("{{%campaign}}", [
            "id" => $this->primaryKey(),
            "name" => $this->string(255)->notNull(),
            "alias" => $this->string(255)->notNull(),
            "site_url" => $this->string(255),
            "logo" => $this->string(255),
            "date_start" => $this->dateTime(),
            "description" => $this->text(),
            "geotargeting" => $this->string(255),
            "currency_id" => $this->string(255),
            "postclick_cookie" => $this->string(5),
            "rating" => $this->float(),
            "ecpm" => $this->float(),
            "ecpc" => $this->float(),
            "epc" => $this->float(),
            "cr" => $this->float(),
            "avg_hold_time" => $this->float(),
            "avg_money_transfer_time" => $this->float(),
            "actions" => $this->text(),
            "regions" => $this->text(),
            "categories" => $this->text(),
            "admitad_last_update" => $this->dateTime(),
            "advertiser_last_update" => $this->dateTime(),
            "delivery_options" => $this->text(),
        ]);

        $this->createTable("{{%product}}", [
            "id" => $this->primaryKey(),
            "offer_id" => $this->integer(),
            "available" => $this->integer(1)->defaultValue(1),
            "campaign_id" => $this->integer()->notNull(),
            "category_offer_id" => $this->integer()->notNull(),
            "category_id" => $this->integer()->notNull(),
            "cpa" => $this->integer(),
            "currencyId" => $this->string(255),
            "delivery" => $this->integer(),
            "manufacturer_warranty" => $this->integer(),
            "model" => $this->text(),
            "modified_time" => $this->dateTime(),
            "name" => $this->text(),
            "pickup" => $this->integer(),
            "price" => $this->float(),
            "typePrefix" => $this->string(255),
            "url" => $this->text(),
            "pictures" => $this->text(),
            "vendor" => $this->string(255),
            "vendorCode" => $this->string(255),
        ]);

        $this->createTable("{{%param}}", [
            "id" => $this->primaryKey(),
            "campaign_id" => $this->integer()->notNull(),
            "name" => $this->string(255),
            "key" => $this->string(255),
        ]);

        $this->createTable("{{%product_param}}", [
            "id" => $this->primaryKey(),
            "param_id" => $this->integer()->notNull(),
            "product_id" => $this->integer()->notNull(),
            "campaign_id" => $this->integer()->notNull(),
        ]);

        $this->createTable("{{%param_category}}", [
            "id" => $this->primaryKey(),
            "param_id" => $this->integer()->notNull(),
            "category_id" => $this->integer()->notNull(),
            "campaign_id" => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropForeignKey("FK_{$this->closureTbl}_parent_{$this->relativeTbl}", "{{%{$this->closureTbl}}}");
        $this->dropForeignKey("FK_{$this->closureTbl}_child_{$this->relativeTbl}", "{{%{$this->closureTbl}}}");
        $this->dropTable("{{%{$this->closureTbl}}}");
        $this->dropTable("{{%{$this->relativeTbl}}}");
        $this->dropTable("{{%campaign}}");
        $this->dropTable("{{%product}}");
        $this->dropTable("{{%product_param}}");
    }
}