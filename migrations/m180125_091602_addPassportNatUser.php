<?php

use yii\db\Migration;

/**
 * Class m180125_091602_addPassportNatUser
 */
class m180125_091602_addPassportNatUser extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%lb_user}}', 'nationality', $this->string(255)->defaultValue(""));
        $this->addColumn('{{%lb_user}}', 'passport', $this->string(255)->defaultValue(""));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180125_091602_addPassportNatUser cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180125_091602_addPassportNatUser cannot be reverted.\n";

        return false;
    }
    */
}
