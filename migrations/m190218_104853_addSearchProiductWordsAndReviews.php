<?php

use yii\db\Migration;

/**
 * Class m190218_104853_addSearchProiductWordsAndReviews
 */
class m190218_104853_addSearchProiductWordsAndReviews extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable("{{%product_review}}", [
            "id" => $this->primaryKey(),
            "name" => $this->string(255),
            "text" => $this->text(),
            "active" => $this->integer(),
            "date_add" => $this->dateTime()
        ], $tableOptions);

        $this->createTable("{{%product_search}}", [
            "id" => $this->primaryKey(),
            "word" => $this->string(255),
            "date_add" => $this->dateTime()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190218_104853_addSearchProiductWordsAndReviews cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190218_104853_addSearchProiductWordsAndReviews cannot be reverted.\n";

        return false;
    }
    */
}
