<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m190305_133511_renamefchangeTopexchange
 */
class m190305_133511_renamefchangeTopexchange extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->dropTable('{{%fchange_transaction}}');
        $this->dropTable('{{%user_pay_wallet_topexchange}}');

        $this->createTable("{{%topexchange_transaction}}", [
            "id" => Schema::TYPE_PK,
            "payment_id" => Schema::TYPE_INTEGER,
            "merchant_name" => Schema::TYPE_STRING.'(255)',
            "merchant_title" => Schema::TYPE_STRING.'(255)',
            "payed_paysys" => Schema::TYPE_STRING.'(255)',
            "amount" => Schema::TYPE_FLOAT,
            "payment_info" => Schema::TYPE_STRING.'(255)',
            "payment_num" => Schema::TYPE_STRING.'(255)',
            "sucess_url" => Schema::TYPE_STRING.'(255)',
            "error_url" => Schema::TYPE_STRING.'(255)',
            "obmen_order_id" => Schema::TYPE_STRING.'(255)',
            "obmen_recive_valute" => Schema::TYPE_STRING.'(255)',
            "obmen_timestamp" => Schema::TYPE_DATETIME,
        ], $tableOptions);

        $this->createTable("{{%user_pay_wallet_topexchange}}", [
            "id" => Schema::TYPE_PK,
            "user_id" => Schema::TYPE_INTEGER.'(12)',
            "pay_system_id" => Schema::TYPE_INTEGER.'(12)',
            "currency_id" => Schema::TYPE_INTEGER.'(12)',
            "wallet" => Schema::TYPE_STRING.'(255)',
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190305_133511_renamefchangeTopexchange cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_133511_renamefchangeTopexchange cannot be reverted.\n";

        return false;
    }
    */
}
