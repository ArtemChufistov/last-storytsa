<?php
use yii\db\Schema;
use yii\db\Migration;

class m170119_080620_addRealSumPayment extends Migration
{
    public function up()
    {
      $this->addColumn('{{%payment}}', 'realSum', Schema::TYPE_FLOAT);
    }

    public function down()
    {
        echo "m170119_080620_addRealSumPayment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
