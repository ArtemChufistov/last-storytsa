<?php

use yii\db\Migration;
use yii\db\Schema;

class m170802_091532_youtube_stat extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%youtube_stat}}", [
            "id" => Schema::TYPE_PK,
            "user_id" => Schema::TYPE_INTEGER,
            "video_id" => Schema::TYPE_INTEGER,
            "showed" => Schema::TYPE_SMALLINT.' NOT NULL DEFAULT 0',
            "liked" => Schema::TYPE_SMALLINT.' NOT NULL DEFAULT 0',
            "comented" => Schema::TYPE_SMALLINT.' NOT NULL DEFAULT 0',
            "date_add" => Schema::TYPE_DATETIME,
            "date_showed" => Schema::TYPE_DATETIME,
            "date_liked" => Schema::TYPE_DATETIME,
            "date_comented" => Schema::TYPE_DATETIME,
        ], $tableOptions);

        $this->createTable("{{%youtube_video}}", [
            "id" => Schema::TYPE_PK,
            'video_url' => Schema::TYPE_STRING.'(255) NULL DEFAULT NULL'
        ], $tableOptions);
    }

    public function safeDown()
    {
        echo "m170802_091532_youtube_stat cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170802_091532_youtube_stat cannot be reverted.\n";
        return false;
    }
    */
}
