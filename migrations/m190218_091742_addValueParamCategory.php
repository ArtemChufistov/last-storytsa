<?php

use yii\db\Migration;

/**
 * Class m190218_091742_addValueParamCategory
 */
class m190218_091742_addValueParamCategory extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%param_category}}', 'value', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190218_091742_addValueParamCategory cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190218_091742_addValueParamCategory cannot be reverted.\n";

        return false;
    }
    */
}
