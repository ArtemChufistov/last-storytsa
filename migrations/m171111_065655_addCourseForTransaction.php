<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m171111_065655_addCourseForTransaction
 */
class m171111_065655_addCourseForTransaction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%transaction}}', 'course', Schema::TYPE_FLOAT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171111_065655_addCourseForTransaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171111_065655_addCourseForTransaction cannot be reverted.\n";

        return false;
    }
    */
}
