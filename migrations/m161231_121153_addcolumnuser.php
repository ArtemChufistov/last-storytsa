<?php

use yii\db\Migration;

class m161231_121153_addcolumnuser extends Migration
{
    public function up()
    {
        $this->addColumn('{{%lb_user}}', 'last_date_email_confirm_token', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn('{{%lb_user}}', 'last_date_email_confirm_token');

        return false;
    }
}
