<?php

use yii\db\Migration;
use yii\db\Schema;

class m180709_114638_cripchant_add_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // Мерчант аккаунты
        $this->createTable('{{%m_account}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'is_turn_on' => 'TINYINT(1) NULL DEFAULT 0',
            'name' => Schema::TYPE_STRING . '(100) NOT NULL',
            'secret' => Schema::TYPE_STRING . '(255) NOT NULL',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'updated_at' => Schema::TYPE_DATETIME . ' NULL DEFAULT NULL',
            'is_delete' => 'TINYINT(1) NULL DEFAULT 0',
        ], $tableOptions);

        $this->execute("ALTER TABLE `m_account` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10101");

        // Разрешен доступ с этих ip
        $this->createTable('{{%m_account_allow_ip}}', [
            'id' => Schema::TYPE_PK,
            'm_account_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'allow_ip' => Schema::TYPE_STRING . '(50) NOT NULL',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'is_delete' => 'TINYINT(1) NULL DEFAULT 0'
        ], $tableOptions);

        // Кошельки разных валют аккаунта
        $this->createTable('{{%m_account_wallet}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'm_account_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'currency_wallet' => Schema::TYPE_STRING . '(255) NOT NULL',
            'currency_type' => Schema::TYPE_STRING . '(50) NOT NULL',
            'balance' => Schema::TYPE_FLOAT,
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL'
        ], $tableOptions);

        // Логи запросов
        $this->createTable('{{%m_account_api_request_log}}', [
            'id' => Schema::TYPE_PK,
            'm_account_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'request' => Schema::TYPE_STRING . '(255) NOT NULL',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL'
        ], $tableOptions);

        $this->addForeignKey('account_wallet_fk', '{{%m_account_wallet}}', 'm_account_id', '{{%m_account}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('account_allow_ip_fk', '{{%m_account_allow_ip}}', 'm_account_id', '{{%m_account}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('account_api_request_log_fk', '{{%m_account_api_request_log}}', 'm_account_id', '{{%m_account}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('m_account_user_id_fk', '{{%m_account}}', 'user_id', '{{%lb_user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('m_account_wallet_user_id_fk', '{{%m_account_wallet}}', 'user_id', '{{%lb_user}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('m_account_wallet_m_account_id_index', '{{%m_account_wallet}}', 'm_account_id');


        // таблица курсов валют
        $this->createTable('{{%m_coinmarketcap_ticker}}', [
            'id' => Schema::TYPE_STRING . '(100) NOT NULL',
            'name' => Schema::TYPE_STRING . '(100) NOT NULL',
            'symbol' => Schema::TYPE_STRING . '(50) NULL DEFAULT NULL',
            'rank' => Schema::TYPE_STRING . '(50) NULL DEFAULT NULL',
            'price_usd' => Schema::TYPE_FLOAT . ' NOT NULL',
            'price_btc' => Schema::TYPE_FLOAT . ' NOT NULL',
            'h24h_volume_usd' => Schema::TYPE_FLOAT . ' NULL DEFAULT NULL',
            'market_cap_usd' => Schema::TYPE_FLOAT . ' NULL DEFAULT NULL',
            'available_supply' => Schema::TYPE_FLOAT . ' NULL DEFAULT NULL',
            'total_supply' => Schema::TYPE_FLOAT . ' NULL DEFAULT NULL',
            'max_supply' => Schema::TYPE_STRING . '(50) NULL DEFAULT NULL',
            'percent_change_1h' => Schema::TYPE_FLOAT . ' NULL DEFAULT NULL',
            'percent_change_24h' => Schema::TYPE_FLOAT . ' NULL DEFAULT NULL',
            'percent_change_7d' => Schema::TYPE_FLOAT . ' NULL DEFAULT NULL',
            'last_updated' => Schema::TYPE_INTEGER
        ], $tableOptions);
    }

    public function safeDown()
    {
        echo "m180709_114638_cripchant_add_tables cannot be reverted.\n";

        return false;
    }
}
