<?php

use yii\db\Migration;

/**
 * Class m190321_155743_matching_bonus_date
 */
class m190321_155743_matching_bonus_date extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_info}}', 'matching_bonus_date', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190321_155743_matching_bonus_date cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190321_155743_matching_bonus_date cannot be reverted.\n";

        return false;
    }
    */
}
