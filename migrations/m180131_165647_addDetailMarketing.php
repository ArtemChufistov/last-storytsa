<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180131_165647_addDetailMarketing
 */
class m180131_165647_addDetailMarketing extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable("{{%detail_marketing}}", [
            "id" => Schema::TYPE_PK,
            "to_user_id" => Schema::TYPE_INTEGER,
            "from_user_id" => Schema::TYPE_INTEGER,
            "currency_id" => Schema::TYPE_INTEGER,
            "percent" => Schema::TYPE_INTEGER,
            "level" => Schema::TYPE_INTEGER,
            "sum" => Schema::TYPE_FLOAT,
            "user_invest_id" => Schema::TYPE_INTEGER,
            "date" => Schema::TYPE_DATETIME
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180131_165647_addDetailMarketing cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180131_165647_addDetailMarketing cannot be reverted.\n";

        return false;
    }
    */
}
