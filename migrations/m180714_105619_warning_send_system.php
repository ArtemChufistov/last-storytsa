<?php

use yii\db\Migration;
use yii\db\Schema;

class m180714_105619_warning_send_system extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // отправка транзакций
        $this->createTable('{{%m_send_txid}}', [
            'id' => Schema::TYPE_PK,
            'txid' => Schema::TYPE_STRING . '(1000) NOT NULL',
            'm_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'sent_time' => Schema::TYPE_DATETIME . ' NULL DEFAULT NULL',
            'status' => 'TINYINT(1) NULL DEFAULT 0',
        ], $tableOptions);

        // попытки отправки транзакций
        $this->createTable('{{%m_send_txid_afforts_num}}', [
            'id' => Schema::TYPE_PK,
            'm_send_tx_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'external_server_response' => Schema::TYPE_STRING . '(50) NOT NULL',
            'answer_time' => Schema::TYPE_DATETIME . ' NOT NULL',
            //'is_delete' => 'TINYINT(1) NULL DEFAULT 0'
        ], $tableOptions);

        $this->addColumn('{{%m_send_txid}}', 'currency', Schema::TYPE_STRING . '(100) NOT NULL');
        $this->addColumn('{{%m_send_txid}}', 'amount', Schema::TYPE_FLOAT . ' NOT NULL');
        $this->addColumn('{{%m_send_txid}}', 'from_addr', Schema::TYPE_STRING . '(100) NULL DEFAULT NULL');
        $this->addColumn('{{%m_send_txid}}', 'to_addr', Schema::TYPE_STRING . '(100) NULL DEFAULT NULL');
        $this->addColumn('{{%m_send_txid}}', 'category', Schema::TYPE_STRING . '(20) NOT NULL');
        $this->addColumn('{{%m_send_txid}}', 'blockchain_time', Schema::TYPE_DATETIME);
        $this->addColumn('{{%m_send_txid}}', 'confirmations', Schema::TYPE_INTEGER . '(11) NOT NULL');

        $this->alterColumn('{{%m_send_txid}}', 'txid', Schema::TYPE_STRING . '(200) NOT NULL');
        $this->createIndex('m_send_ftocat_index', '{{%m_send_txid}}', ['txid', 'from_addr', 'to_addr', 'category'], true);
        $this->addColumn('{{%m_send_txid}}', 'isShowToUser', 'TINYINT(1) NULL DEFAULT 0');
        $this->addColumn('{{%m_send_txid}}', 'requested_fee', Schema::TYPE_DOUBLE . ' NOT NULL');
        $this->addColumn('{{%m_send_txid}}', 'fee', Schema::TYPE_DOUBLE . ' NOT NULL');

        $this->alterColumn('{{%m_send_txid}}', 'amount', Schema::TYPE_DOUBLE . ' NOT NULL');


        $this->addColumn('{{%m_send_txid}}', 'id_parent', Schema::TYPE_INTEGER . '(11) NOT NULL');
        $this->addColumn('{{%m_send_txid}}', 'feePaySender', 'TINYINT(1) NULL DEFAULT 1');
        $this->addColumn('{{%m_send_txid}}', 'request_type', 'TINYINT(3) NULL DEFAULT 1');
        $this->addColumn('{{%m_send_txid}}', 'send_msg_status', 'TINYINT(3) NULL DEFAULT 1');
        $this->addColumn('{{%m_send_txid}}', 'created_at', Schema::TYPE_DATETIME);
        $this->addColumn('{{%m_send_txid}}', 'transit_addr', Schema::TYPE_STRING . '(100) NULL DEFAULT NULL');


        $this->renameTable('{{%m_send_txid}}', '{{%m_payment_tasks}}');


        // отправка транзакций
        $this->createTable('{{%m_nodes_transactions}}', [
            'id' => Schema::TYPE_PK,
            'txid' => Schema::TYPE_STRING . '(200) NOT NULL',
            'confirmations' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'currency' => Schema::TYPE_STRING . '(100) NOT NULL',
            'category' => Schema::TYPE_STRING . '(20) NOT NULL',
            'send_msg_status' => 'TINYINT(1) NULL DEFAULT 0',
            'amount' => Schema::TYPE_DOUBLE . ' NOT NULL',
            'fee' => Schema::TYPE_DOUBLE . ' NULL DEFAULT NULL',
            'from_addr' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'to_addr' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'created_at' => Schema::TYPE_DATETIME,
            'blockchain_time' => Schema::TYPE_DATETIME,
        ], $tableOptions);

        $this->createIndex('m_nodes_transactions_composite_index', '{{%m_nodes_transactions}}', ['txid', 'currency', 'category', 'from_addr', 'to_addr'], true);


        $this->addColumn('{{%m_nodes_transactions}}', 'user_id', Schema::TYPE_INTEGER . '(11) NOT NULL');
        $this->addColumn('{{%m_nodes_transactions}}', 'm_account_id', Schema::TYPE_INTEGER . '(11) NOT NULL');

        $this->addColumn('{{%m_payment_tasks}}', 'user_id', Schema::TYPE_INTEGER . '(11) NOT NULL');

        $this->renameColumn('{{%m_payment_tasks}}', 'requested_fee', 'fee_per_kByte');
        $this->alterColumn('{{%m_payment_tasks}}', 'fee_per_kByte', Schema::TYPE_DOUBLE . ' NULL DEFAULT 0');




    }

    public function safeDown()
    {
        return false;
    }
}
