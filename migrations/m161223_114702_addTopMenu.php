<?php

use yii\db\Migration;

class m161223_114702_addTopMenu extends Migration
{
    public function up()
    {
         $this->insert('{{%menu}}',array(
            'tree'=>'1',
            'title' =>'Верхнее меню',
            'link' => '',
            'lft' => '1',
            'rgt' => '16',
            'depth' => '0',
            'name' => 'topMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'1',
            'title' =>'Главная',
            'link' => '/index',
            'lft' => '2',
            'rgt' => '3',
            'depth' => '1',
            'name' => 'topMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'1',
            'title' =>'Как это работает',
            'link' => '/howitworks',
            'lft' => '4',
            'rgt' => '5',
            'depth' => '1',
            'name' => 'topMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'1',
            'title' =>'О Нас',
            'link' => '/about',
            'lft' => '6',
            'rgt' => '7',
            'depth' => '1',
            'name' => 'topMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'1',
            'title' =>'Связаться с нами',
            'link' => '/contact',
            'lft' => '8',
            'rgt' => '9',
            'depth' => '1',
            'name' => 'topMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'1',
            'title' =>'FAQ',
            'link' => '/faq',
            'lft' => '10',
            'rgt' => '11',
            'depth' => '1',
            'name' => 'topMenu',
            'lang' => 'ru-RU',
          ));
    
         $this->insert('{{%menu}}',array(
            'tree'=>'1',
            'title' =>'Личный кабинет',
            'link' => '/login',
            'lft' => '12',
            'rgt' => '13',
            'depth' => '1',
            'name' => 'topMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'1',
            'title' =>'Регистрация',
            'link' => '/signup',
            'lft' => '14',
            'rgt' => '15',
            'depth' => '1',
            'name' => 'topMenu',
            'lang' => 'ru-RU',
          ));
    }

    public function down()
    {
        echo "m161223_114702_addTopMenu cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
