<?php

use yii\db\Migration;

class m170104_101415_pay_system_id extends Migration
{
    public function up()
    {
       $this->addColumn('{{%payment}}', 'pay_system_id', $this->integer()->defaultValue(NULL));
    }

    public function down()
    {
        echo "m170104_101415_pay_system_id cannot be reverted.\n";

        return false;
    }
}
