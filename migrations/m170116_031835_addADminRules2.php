<?php

use yii\db\Migration;

class m170116_031835_addADminRules2 extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cronlogIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cronlogView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cronlogCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cronlogDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'jobIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'jobView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'jobCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'jobUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'jobDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'eventIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'eventView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'eventCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'eventUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'eventDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailTemplateIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailTemplateView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailTemplateCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailTemplateUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'mailTemplateDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'preferenceIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'preferenceView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'preferenceCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'preferenceUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'preferenceDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'menuIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'menuView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'menuCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'menuUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'menuDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'newsIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'newsView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'newsCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'newsUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'newsDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paymentIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paymentView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paymentCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paymentUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paymentDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemСurrencyIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemСurrencyView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemСurrencyCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemСurrencyUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'paySystemСurrencyDelete']]);
    }

    public function down()
    {
        echo "m170116_031835_addADminRules2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
