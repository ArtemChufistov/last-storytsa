<?php

use yii\db\Migration;

/**
 * Class m180625_093416_addTextForInvest
 */
class m180625_093416_addTextForInvest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%invest_type}}', 'text', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m180625_093416_addTextForInvest cannot be reverted.\n";

        return false;
    }
}
