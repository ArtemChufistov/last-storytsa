<?php

use yii\db\Migration;

/**
 * Class m171206_053402_addSubIdForUserInfo
 */
class m171206_053402_addSubIdForUserInfo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171206_053402_addSubIdForUserInfo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171206_053402_addSubIdForUserInfo cannot be reverted.\n";

        return false;
    }
    */
}
