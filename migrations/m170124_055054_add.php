<?php
use yii\db\Schema;
use yii\db\Migration;

class m170124_055054_add extends Migration
{
    public function up()
    {
        $this->addColumn('{{%matrix_pref}}', 'value1', Schema::TYPE_TEXT);
        $this->addColumn('{{%matrix_pref}}', 'value2', Schema::TYPE_TEXT);
    }

    public function down()
    {
        echo "m170124_055054_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
