<?php

use yii\db\Migration;

class m170215_131437_addRegiseterParentSetting extends Migration
{
    public function up()
    {
        $this->insert('{{%preference}}',array(
            'key' =>'userRegCanEditParent',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'userRegDefaultParentLogin',
            'value' =>'1',
        ));
    }

    public function down()
    {
        echo "m170215_131437_addRegiseterParentSetting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
