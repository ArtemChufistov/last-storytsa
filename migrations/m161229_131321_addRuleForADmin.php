<?php

use yii\db\Migration;

class m161229_131321_addRuleForADmin extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [
            ['administrator', 'profileOffice'],
            ['administrator', 'walletdetailOffice'],
            ['administrator', 'structOffice'],
            ['administrator', 'matrixOffice'],
            ['administrator', 'financeOffice'],
            ['administrator', 'promoOffice'],
            ['administrator', 'responseOffice'],
            ['administrator', 'supportOffice'],
        ]);
    }

    public function down()
    {
        echo "m161229_131321_addRuleForADmin cannot be reverted.\n";

        return false;
    }
}
