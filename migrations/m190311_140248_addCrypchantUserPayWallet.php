<?php

use yii\db\Migration;

/**
 * Class m190311_140248_addCrypchantUserPayWallet
 */
class m190311_140248_addCrypchantUserPayWallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable("{{%user_pay_wallet_crypchant}}", [
            "id" => $this->primaryKey(),
            "user_id" => $this->integer(),
            "pay_system_id" => $this->integer(),
            "currency_id" => $this->integer(),
            "wallet" => $this->string(255),
            "in_wallet" => $this->string(255),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190311_140248_addCrypchantUserPayWallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_140248_addCrypchantUserPayWallet cannot be reverted.\n";

        return false;
    }
    */
}
