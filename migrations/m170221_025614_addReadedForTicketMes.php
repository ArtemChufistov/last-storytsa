<?php
use yii\db\Schema;
use yii\db\Migration;

class m170221_025614_addReadedForTicketMes extends Migration
{
    public function up()
    {
        $this->addColumn('{{%ticket_message}}', 'readed', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m170221_025614_addReadedForTicketMes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
