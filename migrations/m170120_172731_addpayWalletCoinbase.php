<?php
use yii\db\Schema;
use yii\db\Migration;

class m170120_172731_addpayWalletCoinbase extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%user_pay_wallet_coinbase}}", [
            "id" => Schema::TYPE_PK,
            "user_id" => Schema::TYPE_INTEGER . " NOT NULL",
            "pay_system_id" => Schema::TYPE_INTEGER . " NOT NULL",
            "currency_id" => Schema::TYPE_INTEGER . " NOT NULL",
            'site_url' => Schema::TYPE_STRING.'(255) NOT NULL',
            'wallet' => Schema::TYPE_STRING.'(255) NOT NULL',
            'in_wallet' => Schema::TYPE_STRING.'(255) NOT NULL',
            'wallet_id' => Schema::TYPE_STRING.'(255) NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170120_172731_addpayWalletCoinbase cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
