<?php
use yii\db\Schema;
use yii\db\Migration;

class m170602_091530_add extends Migration
{
    public function safeUp()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%payment_type}}", [
            "id" => Schema::TYPE_PK,
            "key" => Schema::TYPE_STRING,
            "active" => Schema::TYPE_INTEGER.'(12)',
        ], $tableOptions);

    }

    public function safeDown()
    {
        echo "m170602_091530_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170602_091530_add cannot be reverted.\n";

        return false;
    }
    */
}
