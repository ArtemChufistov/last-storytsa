<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;
/**
 * Class m171228_135316_addRights
 */
class m171228_135316_addRights extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['cashtransactionIndex', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Наличные - список', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cashtransactionIndex']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['cashtransactionView', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Наличные - просмотр', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cashtransactionView']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['cashtransactionCreate', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Наличные - добавление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cashtransactionCreate']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['cashtransactionUpdate', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Наличные - редактирование', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cashtransactionUpdate']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['cashtransactionDelete', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Наличные - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cashtransactionDelete']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['advcashtransactionIndex', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Advanced Cash - список', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'advcashtransactionIndex']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['advcashtransactionView', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Advanced Cash - просмотр', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'advcashtransactionView']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['advcashtransactionCreate', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Advanced Cash - добавление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'advcashtransactionCreate']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['advcashtransactionUpdate', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Advanced Cash - редактирование', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'advcashtransactionUpdate']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['advcashtransactionDelete', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Advanced Cash - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'advcashtransactionDelete']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['coinbasetransactionIndex', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Coinbase - список', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'coinbasetransactionIndex']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['coinbasetransactionView', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Coinbase - просмотр', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'coinbasetransactionView']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['coinbasetransactionCreate', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Coinbase - добавление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'coinbasetransactionCreate']]);


        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['coinbasetransactionDelete', AuthItem::TYPE_PERMISSION, 'Платёжные поручения - Coinbase - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'coinbasetransactionDelete']]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171228_135316_addRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171228_135316_addRights cannot be reverted.\n";

        return false;
    }
    */
}
