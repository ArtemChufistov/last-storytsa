<?php
use yii\db\Schema;
use yii\db\Migration;

class m170217_094049_addCourseToUsd extends Migration
{
    public function up()
    {
        $this->addColumn('{{%currency}}', 'course_to_usd', Schema::TYPE_FLOAT);
    }

    public function down()
    {
        echo "m170217_094049_addCourseToUsd cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
