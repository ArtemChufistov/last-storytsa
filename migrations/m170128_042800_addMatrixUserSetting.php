<?php
use yii\db\Schema;
use yii\db\Migration;

class m170128_042800_addMatrixUserSetting extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%matrix_user_setting}}", [
            "id" => Schema::TYPE_PK,
            "user_id" => Schema::TYPE_INTEGER,
            "matrix_id" => Schema::TYPE_INTEGER,
            "matrix_root_id" => Schema::TYPE_INTEGER,
            "active" => Schema::TYPE_INTEGER,
            "sort" => Schema::TYPE_INTEGER,
            "date_add" => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170128_042800_addMatrixUserSetting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
