<?php

use yii\db\Migration;

class m170106_072755_addWalletForPaym extends Migration
{
    public function up()
    {
       $this->addColumn('{{%payment}}', 'wallet', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%payment}}', 'wallet');

        return false;
    }
}
