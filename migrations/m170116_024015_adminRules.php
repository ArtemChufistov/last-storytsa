<?php

use yii\db\Migration;

class m170116_024015_adminRules extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'contactMessageIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'contactMessageUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'contactMessageCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'contactMessageDelete']]);
    }

    public function down()
    {
        echo "m170116_024015_adminRules cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
