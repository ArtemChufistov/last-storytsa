<?php

use yii\db\Migration;

class m170523_035936_addActiveAndCourse extends Migration
{
    public function up()
    {
        $this->addColumn('{{%payment}}', 'course', $this->string(255));
        $this->addColumn('{{%currency_course}}', 'active', $this->integer());
    }

    public function down()
    {
        $this->addColumn('{{%payment}}', 'course');
        $this->addColumn('{{%currency_course}}', 'active');

        return false;
    }
}
