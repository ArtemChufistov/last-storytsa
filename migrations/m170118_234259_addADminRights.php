<?php
use lowbase\user\models\AuthItem;
use yii\db\Migration;

class m170118_234259_addADminRights extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // валюты
            ['currencyIndex', AuthItem::TYPE_PERMISSION, 'Валюты - список', NULL, time(), time()],
            ['currencyView', AuthItem::TYPE_PERMISSION, 'Валюты - просмотр', NULL, time(), time()],
            ['currencyCreate', AuthItem::TYPE_PERMISSION, 'Валюты - создание', NULL, time(), time()],
            ['currencyUpdate', AuthItem::TYPE_PERMISSION, 'Валюты - редактирование', NULL, time(), time()],
            ['currencyDelete', AuthItem::TYPE_PERMISSION, 'Валюты - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencyIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencyView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencyCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencyUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencyDelete']]);
    }

    public function down()
    {
        echo "m170118_234259_addADminRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
