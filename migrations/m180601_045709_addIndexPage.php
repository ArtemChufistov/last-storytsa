<?php

use yii\db\Migration;

/**
 * Class m180601_045709_addIndexPage
 */
class m180601_045709_addIndexPage extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->insert('{{%lb_template}}',array(
            'name'=>'Главная страница',
            'path' =>'@app/themes/evinizi/document/index',
          ));

         $this->insert('{{%lb_document}}',array(
            'name'=>'Главная страница',
            'alias' => 'index',
            'title' => 'Главная страница',
            'status' => 1,
            'position' => 1,
            'template_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => date('Y-m-d H:i:s'),
          ));

         $this->insert('{{%lb_template}}',array(
            'name'=>'Доступ запрещён',
            'path' =>'@app/themes/family/document/accessdenied',
          ));

         $this->insert('{{%lb_document}}',array(
            'name'=>'Доступ запрещён',
            'alias' => 'accessdenied',
            'title' => 'Доступ запрещён',
            'status' => 1,
            'position' => 2,
            'template_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => date('Y-m-d H:i:s'),
          ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180601_045709_addIndexPage cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180601_045709_addIndexPage cannot be reverted.\n";

        return false;
    }
    */
}
