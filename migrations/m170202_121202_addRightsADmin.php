<?php
use lowbase\user\models\AuthItem;
use yii\db\Migration;

class m170202_121202_addRightsADmin extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'roleManager']]);
    }

    public function down()
    {
        echo "m170202_121202_addRightsADmin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
