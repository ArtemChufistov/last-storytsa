<?php

use yii\db\Migration;

/**
 * Class m200308_012730_addSUbPaySystemForTop
 */
class m200308_012730_addSUbPaySystemForTop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_pay_wallet_topexchange}}', 'sub_pay_system', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200308_012730_addSUbPaySystemForTop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200308_012730_addSUbPaySystemForTop cannot be reverted.\n";

        return false;
    }
    */
}
