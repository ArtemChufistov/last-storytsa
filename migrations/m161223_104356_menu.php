<?php

use yii\db\Migration;

class m161223_104356_menu extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%menu}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'link' => $this->string(255)->notNull(),
            'lft' => $this->integer(255)->notNull(),
            'rgt' => $this->integer(255)->notNull(),
            'depth' => $this->integer(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'lang' => $this->string(255)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%menu}}');

        return false;
    }
}
