<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170628_035242_addCryptoWalletRights extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['cryptowalletIndex', AuthItem::TYPE_PERMISSION, 'Крипто кошельки - список', NULL, time(), time()],
            ['cryptowalletView', AuthItem::TYPE_PERMISSION, 'Крипто кошельки - просмотр', NULL, time(), time()],
            ['cryptowalletCreate', AuthItem::TYPE_PERMISSION, 'Крипто кошельки - добавление', NULL, time(), time()],
            ['cryptowalletUpdate', AuthItem::TYPE_PERMISSION, 'Крипто кошельки - редактирование', NULL, time(), time()],
            ['cryptowalletDelete', AuthItem::TYPE_PERMISSION, 'Крипто кошельки - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cryptowalletIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cryptowalletView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cryptowalletCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cryptowalletUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'cryptowalletDelete']]);
    }

    public function safeDown()
    {
        echo "m170628_035242_addCryptoWalletRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170628_035242_addCryptoWalletRights cannot be reverted.\n";

        return false;
    }
    */
}
