<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170220_054503_addPmRights extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['userPayWalletPerfectMoneyIndex', AuthItem::TYPE_PERMISSION, 'Платёжные реквизиты - Perfect Money - список', NULL, time(), time()],
            ['userPayWalletPerfectMoneyView', AuthItem::TYPE_PERMISSION, 'Платёжные реквизиты - Perfect Money - просмотр', NULL, time(), time()],
            ['userPayWalletPerfectMoneyCreate', AuthItem::TYPE_PERMISSION, 'Платёжные реквизиты - Perfect Money - добавление', NULL, time(), time()],
            ['userPayWalletPerfectMoneyUpdate', AuthItem::TYPE_PERMISSION, 'Платёжные реквизиты - Perfect Money - редактирование', NULL, time(), time()],
            ['userPayWalletPerfectMoneyDelete', AuthItem::TYPE_PERMISSION, 'Платёжные реквизиты - Perfect Money - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletPerfectMoneyIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletPerfectMoneyView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletPerfectMoneyCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletPerfectMoneyUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletPerfectMoneyDelete']]);
    }

    public function down()
    {
        echo "m170220_054503_addPmRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
