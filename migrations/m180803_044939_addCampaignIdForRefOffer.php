<?php

use yii\db\Migration;

/**
 * Class m180803_044939_addCampaignIdForRefOffer
 */
class m180803_044939_addCampaignIdForRefOffer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%refresh_offer}}', 'campaign_id', $this->integer()->notNull()->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180803_044939_addCampaignIdForRefOffer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_044939_addCampaignIdForRefOffer cannot be reverted.\n";

        return false;
    }
    */
}
