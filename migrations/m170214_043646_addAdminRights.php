<?php

use yii\db\Migration;

class m170214_043646_addAdminRights extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixPrefIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixPrefView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixPrefCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixPrefUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixPrefDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueDelete']]);
    }

    public function down()
    {
        echo "m170214_043646_addAdminRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
