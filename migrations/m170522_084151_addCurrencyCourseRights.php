<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170522_084151_addCurrencyCourseRights extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['currencycourseIndex', AuthItem::TYPE_PERMISSION, 'Курсы валют - список', NULL, time(), time()],
            ['currencycourseView', AuthItem::TYPE_PERMISSION, 'Курсы валют - просмотр', NULL, time(), time()],
            ['currencycourseCreate', AuthItem::TYPE_PERMISSION, 'Курсы валют - добавление', NULL, time(), time()],
            ['currencycourseUpdate', AuthItem::TYPE_PERMISSION, 'Курсы валют - редактирование', NULL, time(), time()],
            ['currencycourseDelete', AuthItem::TYPE_PERMISSION, 'Курсы валют - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencycourseIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencycourseView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencycourseCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencycourseUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'currencycourseDelete']]);
    }

    public function safeDown()
    {
        echo "m170522_084151_addCurrencyCourseRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170522_084151_addCurrencyCourseRights cannot be reverted.\n";

        return false;
    }
    */
}
