<?php

use yii\db\Migration;

class m161224_143844_contactsUs extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%contact_message}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(255)->defaultValue(NULL),
            'name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'message' => $this->text(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%contact_message}}');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
