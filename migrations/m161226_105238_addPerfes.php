<?php

use yii\db\Migration;

class m161226_105238_addPerfes extends Migration
{
    public function up()
    {
        $this->insert('{{%preference}}',array(
            'key' =>'sendMailHost',
            'value' =>'smtp.yandex.ru',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'sendMailUsername',
            'value' =>'no-reply@re-charge.me',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'sendMailPassword',
            'value' =>'123456789AsD',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'sendMailPort',
            'value' =>'465',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'sendMailEncryption',
            'value' =>'SSL',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'cronRunPeriodInSec',
            'value' =>'60',
        ));
    }

    public function down()
    {
        echo "m161226_105238_addPerfes cannot be reverted.\n";

        return false;
    }
}
