<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171110_152915_addErrorPaymentLog
 */
class m171110_152915_addErrorPaymentLog extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%payment_log}}', 'error', Schema::TYPE_STRING);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171110_152915_addErrorPaymentLog cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171110_152915_addErrorPaymentLog cannot be reverted.\n";

        return false;
    }
    */
}
