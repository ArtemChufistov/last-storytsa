<?php

use yii\db\Migration;

class m170106_060827_addDepartmentTicket extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%ticket_department}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%ticket_department}}');

        return false;
    }
}
