<?php

use yii\db\Migration;

/**
 * Class m180802_141743_addOfferId
 */
class m180802_141743_addOfferId extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%campaign}}', 'offer_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180802_141743_addOfferId cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180802_141743_addOfferId cannot be reverted.\n";

        return false;
    }
    */
}
