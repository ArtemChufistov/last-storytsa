<?php
use yii\db\Schema;
use yii\db\Migration;

class m170216_034232_addPerfectMoneyTransactions extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%perfectmoney_transaction}}", [
            "id" => $this->primaryKey(),
            "payment_id" => Schema::TYPE_STRING.'(255)',
            "payee_account" => Schema::TYPE_STRING.'(255)',
            "amount" => Schema::TYPE_FLOAT,
            "units" => Schema::TYPE_STRING.'(255)',
            "batch_num" => Schema::TYPE_STRING.'(255)',
            "payeer_account" => Schema::TYPE_STRING.'(255)',
            "timestamp" => Schema::TYPE_STRING.'(255)',
        ], $tableOptions);

        $this->insert('{{%preference}}',array(
            'key' =>'perfMonSecret',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'perfMonPayeeAccountUSD',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'perfMonPayeeName',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'perfMonPaymentUrl',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'perfMonStatusUrl',
            'value' =>'1',
        ));

        $this->insert('{{%preference}}',array(
            'key' =>'perfMonNoPaymentUrl',
            'value' =>'1',
        ));
    }

    public function down()
    {
        echo "m170216_034232_addPerfectMoneyTransactions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
