<?php
use lowbase\user\models\AuthItem;
use yii\db\Migration;

class m170202_130843_addRightsADmin2 extends Migration
{
    public function up()
    {

        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // транзакции
            ['transactionIndex', AuthItem::TYPE_PERMISSION, 'Транзакции - список', NULL, time(), time()],
            ['transactionView', AuthItem::TYPE_PERMISSION, 'Транзакции - просмотр', NULL, time(), time()],
            ['transactionCreate', AuthItem::TYPE_PERMISSION, 'Транзакции - создание', NULL, time(), time()],
            ['transactionUpdate', AuthItem::TYPE_PERMISSION, 'Транзакции - редактирование', NULL, time(), time()],
            ['transactionDelete', AuthItem::TYPE_PERMISSION, 'Транзакции - удаление', NULL, time(), time()],
        ]);
/*
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['MatrixIndex', AuthItem::TYPE_PERMISSION, 'Управление вершинами - список', NULL, time(), time()],
            ['MatrixView', AuthItem::TYPE_PERMISSION, 'Управление вершинами - просмотр', NULL, time(), time()],
            ['matrixCreatePlaceIndex', AuthItem::TYPE_PERMISSION, 'Управление вершинами - создание', NULL, time(), time()],
            ['MatrixUpdate', AuthItem::TYPE_PERMISSION, 'Управление вершинами - редактирование', NULL, time(), time()],
            ['MatrixDelete', AuthItem::TYPE_PERMISSION, 'Управление вершинами - удаление', NULL, time(), time()],
        ]);
*/
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // категории матриц
            ['matrixCategoryIndex', AuthItem::TYPE_PERMISSION, 'Категории матриц - список', NULL, time(), time()],
            ['matrixCategoryView', AuthItem::TYPE_PERMISSION, 'Категории матриц - просмотр', NULL, time(), time()],
            ['matrixCategoryCreate', AuthItem::TYPE_PERMISSION, 'Категории матриц - создание', NULL, time(), time()],
            ['matrixCategoryUpdate', AuthItem::TYPE_PERMISSION, 'Категории матриц - редактирование', NULL, time(), time()],
            ['matrixCategoryDelete', AuthItem::TYPE_PERMISSION, 'Категории матриц - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление местам
            ['matrixChangePlaceIndex', AuthItem::TYPE_PERMISSION, 'Управление местами - список', NULL, time(), time()],
            ['matrixChangePlaceView', AuthItem::TYPE_PERMISSION, 'Управление местами - просмотр', NULL, time(), time()],
            ['matrixChangePlaceCreate', AuthItem::TYPE_PERMISSION, 'Управление местами - создание', NULL, time(), time()],
            ['matrixChangePlaceUpdate', AuthItem::TYPE_PERMISSION, 'Управление местами - редактирование', NULL, time(), time()],
            ['matrixChangePlaceDelete', AuthItem::TYPE_PERMISSION, 'Управление местами - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // матричные настройки
            ['MatrixPrefIndex', AuthItem::TYPE_PERMISSION, 'Матричные настройки - список', NULL, time(), time()],
            ['MatrixPrefView', AuthItem::TYPE_PERMISSION, 'Матричные настройки - просмотр', NULL, time(), time()],
            ['matrixPrefCreate', AuthItem::TYPE_PERMISSION, 'Матричные настройки - создание', NULL, time(), time()],
            ['MatrixPrefUpdate', AuthItem::TYPE_PERMISSION, 'Матричные настройки - редактирование', NULL, time(), time()],
            ['MatrixPrefDelete', AuthItem::TYPE_PERMISSION, 'Матричные настройки - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // матричная очередь
            ['matrixQueueIndex', AuthItem::TYPE_PERMISSION, 'Матрицы очередь - список', NULL, time(), time()],
            ['matrixQueueView', AuthItem::TYPE_PERMISSION, 'Матрицы очередь - просмотр', NULL, time(), time()],
            ['matrixQueueCreate', AuthItem::TYPE_PERMISSION, 'Матрицы очередь - создание', NULL, time(), time()],
            ['matrixQueueUpdate', AuthItem::TYPE_PERMISSION, 'Матрицы очередь - редактирование', NULL, time(), time()],
            ['matrixQueueDelete', AuthItem::TYPE_PERMISSION, 'Матрицы очередь - удаление', NULL, time(), time()],
        ]);
/*
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'transactionDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixView']]);
        //$this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCreatePlaceIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixCategoryDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixChangePlaceDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixPrefIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixPrefView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixPrefCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixPrefUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'MatrixPrefDelete']]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'matrixQueueDelete']]);
*/
    }

    public function down()
    {
        echo "m170202_130843_addRightsADmin2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
