<?php

use lowbase\user\models\AuthItem;
use yii\db\Migration;

class m161229_125802_addAuthRules extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['notVerifiedUser', AuthItem::TYPE_ROLE, 'Не верифицированый участник', NULL, time(), time()],
            ['verifiedUser', AuthItem::TYPE_ROLE, 'Верифицированый участник', NULL, time(), time()],
            ['profileOffice', AuthItem::TYPE_PERMISSION, 'Редактирование своей информации', NULL, time(), time()],
            ['walletdetailOffice', AuthItem::TYPE_PERMISSION, 'Управление реквизитами кошелька', NULL, time(), time()],
            ['structOffice', AuthItem::TYPE_PERMISSION, 'Управление структурой', NULL, time(), time()],
            ['matrixOffice', AuthItem::TYPE_PERMISSION, 'Управление матрицей', NULL, time(), time()],
            ['financeOffice', AuthItem::TYPE_PERMISSION, 'Управление финансов', NULL, time(), time()],
            ['promoOffice', AuthItem::TYPE_PERMISSION, 'Управление промо материалами', NULL, time(), time()],
            ['responseOffice', AuthItem::TYPE_PERMISSION, 'Управление отзывами', NULL, time(), time()],
            ['supportOffice', AuthItem::TYPE_PERMISSION, 'Поддержка', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [
            ['notVerifiedUser', 'profileOffice'],
            ['notVerifiedUser', 'matrixOffice'],
            ['verifiedUser', 'profileOffice'],
            ['verifiedUser', 'walletdetailOffice'],
            ['verifiedUser', 'structOffice'],
            ['verifiedUser', 'matrixOffice'],
            ['verifiedUser', 'financeOffice'],
            ['verifiedUser', 'promoOffice'],
            ['verifiedUser', 'responseOffice'],
            ['verifiedUser', 'supportOffice'],
        ]);
    }

    public function down()
    {
        echo "m161229_125802_addAuthRules cannot be reverted.\n";

        return false;
    }
}
