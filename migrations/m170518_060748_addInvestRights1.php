<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170518_060748_addInvestRights1 extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['investTypeIndex', AuthItem::TYPE_PERMISSION, 'Управление пакетами - список', NULL, time(), time()],
            ['investTypeView', AuthItem::TYPE_PERMISSION, 'Управление пакетами - просмотр', NULL, time(), time()],
            ['investTypeCreate', AuthItem::TYPE_PERMISSION, 'Управление пакетами - добавление', NULL, time(), time()],
            ['investTypeUpdate', AuthItem::TYPE_PERMISSION, 'Управление пакетами - редактирование', NULL, time(), time()],
            ['investTypeDelete', AuthItem::TYPE_PERMISSION, 'Управление пакетами - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investTypeIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investTypeView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investTypeCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investTypeUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'investTypeDelete']]);
    }

    public function safeDown()
    {
        echo "m170518_060748_addInvestRights1 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170518_060748_addInvestRights1 cannot be reverted.\n";

        return false;
    }
    */
}
