<?php

use yii\db\Migration;

class m170103_111531_add extends Migration
{
    public function up()
    {
       $this->addColumn('{{%matrix}}', 'slug', $this->string()->notNull());
    }

    public function down()
    {
        echo "m170103_111531_add cannot be reverted.\n";

        return false;
    }
}