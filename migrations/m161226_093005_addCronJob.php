<?php

use yii\db\Migration;

class m161226_093005_addCronJob extends Migration
{
    public function up()
    {
        $this->insert('{{%job}}',array(
            'frequency' =>'30',
            'key' =>'\app\modules\mail\components\SendMailComponent::run',
            'active' => 1,
            'cron_num' => 'first'
        ));

        $this->insert('{{%job}}',array(
            'frequency' =>'30',
            'key' =>'\app\modules\finance\components\cron\PaymentComponent::cancelOld',
            'active' => 1,
            'cron_num' => 'first'
        ));

        $this->insert('{{%job}}',array(
            'frequency' =>'30',
            'key' =>'\app\modules\finance\components\cron\PaymentComponent::inspect',
            'active' => 1,
            'cron_num' => 'first'
        ));

        $this->insert('{{%job}}',array(
            'frequency' =>'25',
            'key' =>'\app\modules\finance\components\cron\PaymentComponent::process',
            'active' => 1,
            'cron_num' => 'first'
        ));

        $this->insert('{{%job}}',array(
            'frequency' =>'80',
            'key' =>'\app\modules\finance\components\cron\SyncComponent::CoinBaseTransactions',
            'active' => 1,
            'cron_num' => 'first'
        ));

        $this->insert('{{%job}}',array(
            'frequency' =>'60',
            'key' =>'\app\modules\finance\components\cron\CourseComponent::updateCurrencyCourseToUsd',
            'active' => 1,
            'cron_num' => 'first'
        ));

        $this->insert('{{%job}}',array(
            'frequency' =>'30',
            'key' =>'\app\modules\invest\components\MarketingComponent::run',
            'active' => 1,
            'cron_num' => 'first'
        ));
    }

    public function down()
    {
        echo "m161226_093005_addCronJob cannot be reverted.\n";

        return false;
    }
}
