<?php
use yii\db\Schema;
use yii\db\Migration;

class m170518_104619_addColumnsForInvestPref extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invest_pref}}', 'value1', Schema::TYPE_TEXT);
        $this->addColumn('{{%invest_pref}}', 'value2', Schema::TYPE_TEXT);
    }

    public function safeDown()
    {
        echo "m170518_104619_addColumnsForInvestPref cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170518_104619_addColumnsForInvestPref cannot be reverted.\n";

        return false;
    }
    */
}
