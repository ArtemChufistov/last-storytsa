<?php

use yii\db\Migration;

/**
 * Class m180803_063842_addParentOfferIdForCategory
 */
class m180803_063842_addParentOfferIdForCategory extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'parent_offer_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180803_063842_addParentOfferIdForCategory cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_063842_addParentOfferIdForCategory cannot be reverted.\n";

        return false;
    }
    */
}
