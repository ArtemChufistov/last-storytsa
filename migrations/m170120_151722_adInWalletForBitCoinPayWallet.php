<?php
use yii\db\Schema;
use yii\db\Migration;

class m170120_151722_adInWalletForBitCoinPayWallet extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_pay_wallet_bitcoin}}', 'in_wallet', $this->string(255)->defaultValue(NULL));
    }

    public function down()
    {
        $this->dropColumn('{{%user_pay_wallet_bitcoin}}', 'in_wallet');

        return false;
    }
}
