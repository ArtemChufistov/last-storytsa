<?php

use yii\db\Migration;

class m170707_112809_addCorretsForPaymentANdCurrencyCourse extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{currency_course}}', 'transfer_type', 'VARCHAR(255)');

        $this->addColumn('{{payment}}', 'wallet_id', 'VARCHAR(255)');

        $this->dropColumn('{{payment}}', 'sum');

        $this->addColumn('{{payment}}', 'from_sum', 'float AFTER `status`');

        $this->dropColumn('{{payment}}', 'to_sum');

        $this->addColumn('{{payment}}', 'to_sum', 'float AFTER `from_sum`');

        $this->dropColumn('{{payment}}', 'pay_system_id');

        $this->addColumn('{{payment}}', 'from_pay_system_id', 'INT(11) AFTER `date_add`');

        $this->dropColumn('{{payment}}', 'currency_id');

        $this->addColumn('{{payment}}', 'from_currency_id', 'INT(11) AFTER `to_sum`');

    }

    public function safeDown()
    {
        echo "m170707_112809_addCorretsForPaymentANdCurrencyCourse cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170707_112809_addCorretsForPaymentANdCurrencyCourse cannot be reverted.\n";

        return false;
    }
    */
}
