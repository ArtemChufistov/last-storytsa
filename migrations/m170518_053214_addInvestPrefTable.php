<?php

use yii\db\Schema;
use yii\db\Migration;

class m170518_053214_addInvestPrefTable extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%invest_pref}}', [
            'id' => Schema::TYPE_PK,
            'key' => Schema::TYPE_STRING.'(255) NOT NULL',
            'value' => Schema::TYPE_TEXT,
        ], $tableOptions);
    }

    public function safeDown()
    {
        echo "m170518_053214_addInvestPrefTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170518_053214_addInvestPrefTable cannot be reverted.\n";

        return false;
    }
    */
}
