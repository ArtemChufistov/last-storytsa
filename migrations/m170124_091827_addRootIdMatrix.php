<?php
use yii\db\Schema;
use yii\db\Migration;

class m170124_091827_addRootIdMatrix extends Migration
{
    public function up()
    {
        $this->addColumn('{{%matrix}}', 'root_id', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m170124_091827_addRootIdMatrix cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
