<?php

use yii\db\Migration;

/**
 * Class m190214_160753_addParamCategoryValue
 */
class m190214_160753_addParamCategoryValue extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'min_product_price', $this->float()->after('name'));
        $this->addColumn('{{%category}}', 'max_product_price', $this->float()->after('min_product_price'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190214_160753_addParamCategoryValue cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_160753_addParamCategoryValue cannot be reverted.\n";

        return false;
    }
    */
}
