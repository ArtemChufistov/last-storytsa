<?php

use yii\db\Migration;

/**
 * Class m190213_145221_addValueProductParam
 */
class m190213_145221_addValueProductParam extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_param}}', 'value', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190213_145221_addValueProductParam cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190213_145221_addValueProductParam cannot be reverted.\n";

        return false;
    }
    */
}
