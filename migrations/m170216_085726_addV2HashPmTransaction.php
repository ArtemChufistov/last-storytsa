<?php
use yii\db\Schema;
use yii\db\Migration;

class m170216_085726_addV2HashPmTransaction extends Migration
{
    public function up()
    {
        $this->addColumn('{{%perfectmoney_transaction}}', 'v2_hash', Schema::TYPE_TEXT);
    }

    public function down()
    {
        echo "m170216_085726_addV2HashPmTransaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
