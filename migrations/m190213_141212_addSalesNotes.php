<?php

use yii\db\Migration;

/**
 * Class m190213_141212_addSalesNotes
 */
class m190213_141212_addSalesNotes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'sales_notes', $this->text()->after('description'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190213_141212_addSalesNotes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190213_141212_addSalesNotes cannot be reverted.\n";

        return false;
    }
    */
}
