<?php

use yii\db\Migration;
use yii\db\Schema;

class m170814_032734_lb_usa_state extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%lb_usa_state}}", [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING.'(100)',
        ], $tableOptions);

        $this->batchInsert('lb_usa_state', ['name'], [
          ['Victoria, VIC'],
          ['New South Wales, NSW'],
          ['South Australia, SA'],
          ['Queensland, QLD'],
          ['Western Australia, WA'],
          ['Tasmania, TAS']
        ]);
    }

    public function safeDown()
    {
        $this->dropTable("{{%lb_usa_state}}");

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170814_032734_lb_usa_state cannot be reverted.\n";

        return false;
    }
    */
}
