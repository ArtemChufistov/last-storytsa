<?php

use yii\db\Migration;

class m161223_095311_addCrontab extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%job}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(255)->notNull(),
            'work' => $this->text(),
            'type' => $this->string(255)->defaultValue(NULL),
        ], $tableOptions);

        $this->createTable('{{%cron_log}}', [
            'id' => $this->primaryKey(),
            'job_id' => $this->integer()->defaultValue(NULL),
            'work' => $this->text(),
            'date_end' => $this->dateTime(),
            'date_start' => $this->dateTime(),
            'success' => $this->integer()->defaultValue(NULL),
            'addition_info' => $this->text(),
        ], $tableOptions);
    }

    public function down()
    {
         $this->dropTable('{{%job}}');
         $this->dropTable('{{%cron_log}}');

        return false;
    }
}
