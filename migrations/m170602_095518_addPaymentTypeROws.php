<?php
use yii\db\Schema;
use yii\db\Migration;

class m170602_095518_addPaymentTypeROws extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%transaction_type}}", [
            "id" => Schema::TYPE_PK,
            "key" => Schema::TYPE_STRING,
            "active" => Schema::TYPE_INTEGER.'(12)',
        ], $tableOptions);

        $this->batchInsert('{{%transaction_type}}', ['key', 'active'], [['partner_buy_place', 1]]);
        $this->batchInsert('{{%transaction_type}}', ['key', 'active'], [['buy_matrix_place', 1]]);
        $this->batchInsert('{{%transaction_type}}', ['key', 'active'], [['buy_invest', 1]]);
        $this->batchInsert('{{%transaction_type}}', ['key', 'active'], [['reinvest_matrix_place', 1]]);
        $this->batchInsert('{{%transaction_type}}', ['key', 'active'], [['withdraw_matrix_place', 1]]);
        $this->batchInsert('{{%transaction_type}}', ['key', 'active'], [['perc_out_office', 1]]);
        $this->batchInsert('{{%transaction_type}}', ['key', 'active'], [['charge_office', 1]]);
        $this->batchInsert('{{%transaction_type}}', ['key', 'active'], [['out_office', 1]]);
        $this->batchInsert('{{%transaction_type}}', ['key', 'active'], [['inner_pay', 1]]);
    }

    public function safeDown()
    {
        echo "m170602_095518_addPaymentTypeROws cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170602_095518_addPaymentTypeROws cannot be reverted.\n";

        return false;
    }
    */
}
