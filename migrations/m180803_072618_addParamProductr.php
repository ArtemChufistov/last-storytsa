<?php

use yii\db\Migration;

/**
 * Class m180803_072618_addParamProductr
 */
class m180803_072618_addParamProductr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'param', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180803_072618_addParamProductr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_072618_addParamProductr cannot be reverted.\n";

        return false;
    }
    */
}
