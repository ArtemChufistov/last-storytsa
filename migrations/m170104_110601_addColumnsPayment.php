<?php

use yii\db\Migration;

class m170104_110601_addColumnsPayment extends Migration
{
    public function up()
    {
       $this->addColumn('{{%payment}}', 'comment', $this->string(255)->notNull());
       $this->addColumn('{{%payment}}', 'type', $this->string(255)->notNull());
    }

    public function down()
    {
        echo "m170104_110601_addColumnsPayment cannot be reverted.\n";

        return false;
    }
}
