<?php
use yii\db\Schema;
use yii\db\Migration;

class m170303_095943_addFromMatrixRootIdToQUeue extends Migration
{
    public function up()
    {
        $this->addColumn('{{%matrix_queue}}', 'from_matrix_root_id', Schema::TYPE_INTEGER . ' AFTER matrix_root_id');
    }

    public function down()
    {
        echo "m170303_095943_addFromMatrixRootIdToQUeue cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
