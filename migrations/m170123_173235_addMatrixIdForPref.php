<?php
use yii\db\Schema;
use yii\db\Migration;

class m170123_173235_addMatrixIdForPref extends Migration
{
    public function up()
    {
        $this->addColumn('{{%matrix_pref}}', 'matrix_id', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m170123_173235_addMatrixIdForPref cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
