<?php

use yii\db\Migration;

class m170124_131545_addMatrixQueue extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%matrix_queue}}", [
            "id" => $this->primaryKey(),
            "matrix_id" => $this->integer()->defaultValue(NULL),
            "currency_id" => $this->integer()->defaultValue(NULL),
            "user_id" => $this->integer()->defaultValue(NULL),
            "matrix_root_id" => $this->integer()->defaultValue(NULL),
            "sum" => $this->float()->defaultValue(0),
            "status" => $this->string(255)->notNull(),
            "type" => $this->string(255)->notNull(),
            "date_add" => $this->dateTime(),
            "date_update" => $this->dateTime(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%matrix_queue}}");

        return false;
    }
}
