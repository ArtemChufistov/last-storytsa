<?php

use yii\db\Migration;

class m180921_094524_2fa extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%lb_user}}', 'totp_secret', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%lb_user}}', 'totp_secret');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }
    public function down()
    {
        echo "m180921_094524_2fa cannot be reverted.\n";

        return false;
    }
    */
}
