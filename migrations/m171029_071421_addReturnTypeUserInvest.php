<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m171029_071421_addReturnTypeUserInvest
 */
class m171029_071421_addReturnTypeUserInvest extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_invest}}', 'returnType', Schema::TYPE_STRING. '(100) AFTER `paid_sum`');
        $this->addColumn('{{%user_invest}}', 'status', Schema::TYPE_STRING. '(100) AFTER `returnType`');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171029_071421_addReturnTypeUserInvest cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171029_071421_addReturnTypeUserInvest cannot be reverted.\n";

        return false;
    }
    */
}
