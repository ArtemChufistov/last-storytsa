<?php

use yii\db\Migration;

class m170118_232752_addPaySystemCurrencyKey extends Migration
{
    public function up()
    {
        $this->addForeignKey('pay_system_currency_fk', '{{%pay_system_currency}}', 'pay_system_id', '{{%pay_system}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m170118_232752_addPaySystemCurrencyKey cannot be reverted.\n";

        return false;
    }
}
