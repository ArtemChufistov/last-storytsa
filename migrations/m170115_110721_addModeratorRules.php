<?php
use lowbase\user\models\AuthItem;
use yii\db\Migration;

class m170115_110721_addModeratorRules extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['contactMessageIndex', AuthItem::TYPE_PERMISSION, 'Обратная связь - список', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [
            ['moderator', 'contactMessageIndex'],
        ]);
    }

    public function down()
    {
        echo "m170115_110721_addModeratorRules cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
