<?php
use yii\db\Schema;
use yii\db\Migration;

class m170707_040818_addComission extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{pay_system}}', 'in_comission');
        $this->dropColumn('{{pay_system}}', 'out_comission');
        $this->dropColumn('{{pay_system}}', 'in_comis_type');
        $this->dropColumn('{{pay_system}}', 'out_comis_type');

        $this->addColumn('{{currency_course}}', 'comission', Schema::TYPE_FLOAT);
        $this->addColumn('{{currency_course}}', 'comission_type', 'VARCHAR(255)');

        $this->addColumn('{{payment}}', 'comission_from_sum', Schema::TYPE_FLOAT);
        $this->addColumn('{{payment}}', 'comission_to_sum', Schema::TYPE_FLOAT);
    }

    public function safeDown()
    {
        echo "m170707_040818_addComission cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170707_040818_addComission cannot be reverted.\n";

        return false;
    }
    */
}
