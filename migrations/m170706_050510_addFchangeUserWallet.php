<?php
use yii\db\Schema;
use yii\db\Migration;

class m170706_050510_addFchangeUserWallet extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%user_pay_wallet_topexchange}}", [
            "id" => Schema::TYPE_PK,
            "user_id" => Schema::TYPE_INTEGER.'(12)',
            "pay_system_id" => Schema::TYPE_INTEGER.'(12)',
            "currency_id" => Schema::TYPE_INTEGER.'(12)',
            "wallet" => Schema::TYPE_STRING.'(255)',
        ], $tableOptions);
    }

    public function safeDown()
    {
        echo "m170706_050510_addFchangeUserWallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170706_050510_addFchangeUserWallet cannot be reverted.\n";

        return false;
    }
    */
}
