<?php
use yii\db\Schema;
use yii\db\Migration;

class m170628_044210_addFunctioncourseTypeUpdate extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%currency}}', 'auto_course_update', 'INTEGER(12)');
        $this->addColumn('{{%currency}}', 'func_course_update', 'VARCHAR(255)');
    }

    public function safeDown()
    {
        echo "m170628_044210_addFunctioncourseTypeUpdate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170628_044210_addFunctioncourseTypeUpdate cannot be reverted.\n";

        return false;
    }
    */
}
