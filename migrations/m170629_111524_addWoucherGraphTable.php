<?php
use yii\db\Schema;
use yii\db\Migration;

class m170629_111524_addWoucherGraphTable extends Migration
{
    public function safeUp()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%currency_graph}}", [
            "id" => Schema::TYPE_PK,
            "value" => Schema::TYPE_FLOAT,
            "currency_id" => Schema::TYPE_INTEGER,
            "date" => Schema::TYPE_DATETIME,
            "type" => Schema::TYPE_STRING,
        ], $tableOptions);

    }

    public function safeDown()
    {
        echo "m170629_111524_addWoucherGraphTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170629_111524_addWoucherGraphTable cannot be reverted.\n";

        return false;
    }
    */
}
