<?php

use yii\db\Migration;

/**
 * Class m200308_021452_addToxFields
 */
class m200308_021452_addToxFields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_pay_wallet_topexchange}}', 'recive_wallet', $this->string());
        $this->addColumn('{{%user_pay_wallet_topexchange}}', 'user_phone', $this->string());
        $this->addColumn('{{%user_pay_wallet_topexchange}}', 'user_email', $this->string());
        $this->addColumn('{{%user_pay_wallet_topexchange}}', 'wallet_recive_fio', $this->string());
        $this->addColumn('{{%user_pay_wallet_topexchange}}', 'rekvizit_payment_id', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200308_021452_addToxFields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200308_021452_addToxFields cannot be reverted.\n";

        return false;
    }
    */
}
