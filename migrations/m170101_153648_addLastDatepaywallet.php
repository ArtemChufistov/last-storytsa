<?php

use yii\db\Migration;

class m170101_153648_addLastDatepaywallet extends Migration
{
    public function up()
    {
        $this->addColumn('{{%lb_user}}', 'last_date_pay_password', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn('{{%lb_user}}', 'last_date_pay_password');

        return false;
    }
}
