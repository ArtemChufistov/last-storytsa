<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m171107_174258_addLeafFOrUSer
 */
class m171107_174258_addLeafFOrUSer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{lb_user}}', 'leaf', Schema::TYPE_INTEGER. '(1)');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171107_174258_addLeafFOrUSer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171107_174258_addLeafFOrUSer cannot be reverted.\n";

        return false;
    }
    */
}
