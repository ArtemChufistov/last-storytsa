<?php

use yii\db\Migration;

class m170101_203455_addSubscrib extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%subscriber}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->defaultValue(NULL),
            'mail' => $this->string(255)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%subscriber}}');

        return false;
    }
}
