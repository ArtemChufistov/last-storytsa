<?php

use yii\db\Migration;

class m161223_094731_addEvents extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(255)->defaultValue(NULL),
            'user_id' => $this->integer()->defaultValue(NULL),
            'to_user_id' => $this->integer()->defaultValue(NULL),
            'sum' => $this->float()->defaultValue(0),
            'currency_id' => $this->integer()->defaultValue(NULL),
            'date_add' => $this->dateTime(),
            'addition_info' => $this->text(),
        ], $tableOptions);
    }

    public function down()
    {
         $this->dropTable('{{%event}}');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
