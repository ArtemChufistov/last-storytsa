<?php

use yii\db\Migration;

class m170101_110310_addUserWalletPaysystem extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%pay_system}}", [
            "id" => $this->primaryKey(),
            "key" => $this->string(255)->notNull(),
            "name" => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->createTable("{{%pay_system_currency}}", [
            "id" => $this->primaryKey(),
            "pay_system_id" => $this->integer()->notNull(),
            "currency_id" => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable("{{%user_pay_wallet_bitcoin}}", [
            "id" => $this->primaryKey(),
            "user_id" => $this->integer()->notNull(),
            "pay_system_id" => $this->integer()->notNull(),
            "currency_id" => $this->integer()->notNull(),
            'site_url' => $this->string(255)->notNull(),
            'wallet' => $this->string(255)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%pay_system}}");
        $this->dropTable("{{%pay_system_currency}}");
        $this->dropTable("{{%user_pay_wallet_bitcoin}}");

        return false;
    }
}
