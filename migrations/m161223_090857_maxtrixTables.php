<?php

use yii\db\Migration;

class m161223_090857_maxtrixTables extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%user_pref}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'parent_user_id' => $this->integer()->defaultValue(NULL),
            'soc_vk' => $this->string(255)->notNull(),
            'soc_google' => $this->string(255)->notNull(),
            'soc_twitter' => $this->string(255)->notNull(),
            'soc_facebook' => $this->string(255)->notNull(),
            '2factor_auth' => $this->string(255)->notNull(),
            '2factor_auth_hash' => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->createTable('{{%matrix}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'parent_id' => $this->integer()->defaultValue(NULL),
            'sum' => $this->float()->defaultValue(0),
            'currency_id' => $this->integer()->defaultValue(NULL),
            'status' => $this->integer()->defaultValue(NULL),
            'sort' => $this->integer()->defaultValue(NULL),
        ], $tableOptions);

        $this->createTable('{{%matrix_pref}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
            'value' => $this->text(),
        ], $tableOptions);
    }

    public function down()
    {
         $this->dropTable('{{%user_pref}}');
         $this->dropTable('{{%user_matrix}}');
         $this->dropTable('{{%matrix_pref}}');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
