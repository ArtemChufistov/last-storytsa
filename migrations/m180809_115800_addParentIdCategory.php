<?php

use yii\db\Migration;

/**
 * Class m180809_115800_addParentIdCategory
 */
class m180809_115800_addParentIdCategory extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'parent_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180809_115800_addParentIdCategory cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180809_115800_addParentIdCategory cannot be reverted.\n";

        return false;
    }
    */
}
