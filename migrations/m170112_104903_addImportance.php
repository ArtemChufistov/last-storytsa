<?php

use yii\db\Migration;

class m170112_104903_addImportance extends Migration
{
    public function up()
    {
        $this->batchInsert('ticket_importance', ['name', 'color'], [
            ['Средний', '#54d86a'],
            ['Важный', '#d11717'],
            ['Не важный', '#4daecc']
        ]);
    }

    public function down()
    {
        echo "m170112_104903_addImportance cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
