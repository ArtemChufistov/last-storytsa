<?php

use yii\db\Migration;

class m170518_090950_addInvestPrefColumn extends Migration
{
    public function up()
    {
        $this->addColumn('{{%invest_pref}}', 'invest_type_id', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%invest_pref}}', 'invest_type_id');

        return false;
    }
}
