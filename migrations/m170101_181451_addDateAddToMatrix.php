<?php

use yii\db\Migration;

class m170101_181451_addDateAddToMatrix extends Migration
{
    public function up()
    {
       $this->addColumn('{{%matrix}}', 'date_add', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn('{{%matrix}}', 'date_add');

        return false;
    }
}
