<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170214_060821_addAdminRights extends Migration
{
    public function up()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['userPayWalletCoinbaseIndex', AuthItem::TYPE_PERMISSION, 'Кошельки Coinbase - список', NULL, time(), time()],
            ['userPayWalletCoinbaseView', AuthItem::TYPE_PERMISSION, 'Кошельки Coinbase - просмотр', NULL, time(), time()],
            ['userPayWalletCoinbaseCreate', AuthItem::TYPE_PERMISSION, 'Кошельки Coinbase - добавление', NULL, time(), time()],
            ['userPayWalletCoinbaseUpdate', AuthItem::TYPE_PERMISSION, 'Кошельки Coinbase - редактирование', NULL, time(), time()],
            ['userPayWalletCoinbaseDelete', AuthItem::TYPE_PERMISSION, 'Кошельки Coinbase - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletCoinbaseIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletCoinbaseView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletCoinbaseCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletCoinbaseUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userPayWalletCoinbaseDelete']]);
    }

    public function down()
    {
        echo "m170214_060821_addAdminRights cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
