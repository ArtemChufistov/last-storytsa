<?php

use yii\db\Migration;
use yii\db\Schema;

class m170818_072943_changeYoutubeStatVideoIdColumn extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->dropColumn('{{%youtube_stat}}', 'user_id');
        $this->addColumn('{{%youtube_stat}}', 'user_id', Schema::TYPE_STRING.'(255) NULL DEFAULT NULL AFTER `id`');

        $this->dropColumn('{{%youtube_stat}}', 'video_id');
        $this->addColumn('{{%youtube_stat}}', 'video_id', Schema::TYPE_STRING.'(255) NULL DEFAULT NULL AFTER `user_id`');

        $this->dropTable('{{%youtube_video}}');
        $this->createTable("{{%youtube_video}}", [
            "id" => Schema::TYPE_PK,
            'video_id' => Schema::TYPE_STRING.'(255) NULL DEFAULT NULL'
        ], $tableOptions);
    }

    public function safeDown()
    {
        echo "m170818_072943_changeYoutubeStatVideoIdColumn cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170818_072943_changeYoutubeStatVideoIdColumn cannot be reverted.\n";

        return false;
    }
    */
}
