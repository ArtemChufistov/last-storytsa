<?php

use yii\db\Migration;

/**
 * Class m180627_044124_addCMUserWallet
 */
class m180627_044124_addCMUserWallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        $this->createTable("{{%user_pay_wallet_cm}}", [
            "id" => $this->primaryKey(),
            "user_id" => $this->integer()->defaultValue(NULL),
            "pay_system_id" => $this->integer()->defaultValue(NULL),
            "currency_id" => $this->integer()->defaultValue(NULL),
            "wallet" => $this->string(255)->defaultValue(NULL),
            "in_wallet" => $this->string(255)->defaultValue(NULL),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{%user_pay_wallet_cm}}");

        return false;
    }
}
