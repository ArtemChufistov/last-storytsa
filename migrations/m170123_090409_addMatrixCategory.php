<?php
use yii\db\Schema;
use yii\db\Migration;

class m170123_090409_addMatrixCategory extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%matrix_category}}", [
            "id" => $this->primaryKey(),
            "name" => Schema::TYPE_STRING.'(255) NOT NULL',
        ], $tableOptions);

        $this->createTable("{{%matrix_category_matrix}}", [
            "id" => $this->primaryKey(),
            "matrix_id" => Schema::TYPE_INTEGER,
            "matrix_category_id" => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->addForeignKey('matrix_category_matrix_fk', '{{%matrix_category_matrix}}', 'matrix_id', '{{%matrix}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('matrix_category_matrix_fk2', '{{%matrix_category_matrix}}', 'matrix_category_id', '{{%matrix_category}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m170123_090409_addMatrixCategory cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
