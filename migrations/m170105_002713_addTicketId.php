<?php

use yii\db\Migration;

class m170105_002713_addTicketId extends Migration
{
    public function up()
    {
       $this->addColumn('{{%ticket_message}}', 'ticket_id', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%ticket_message}}', 'ticket_id');

        return false;
    }
}
