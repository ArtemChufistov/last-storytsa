<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171228_160825_addUserPayWalletAdvCash
 */
class m171228_160825_addUserPayWalletAdvCash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable("{{%user_pay_wallet_cash}}", [
            "id" => Schema::TYPE_PK,
            "user_id" => Schema::TYPE_INTEGER,
            "pay_system_id" => Schema::TYPE_INTEGER,
            "currency_id" => Schema::TYPE_INTEGER,
            "name" => Schema::TYPE_STRING,
            "address" => Schema::TYPE_STRING,
            "email" => Schema::TYPE_STRING,
            "phone" => Schema::TYPE_STRING,
            "description" => Schema::TYPE_TEXT,
        ], $tableOptions);

        $this->createTable("{{%user_pay_wallet_advcash}}", [
            "id" => Schema::TYPE_PK,
            "user_id" => Schema::TYPE_INTEGER,
            "pay_system_id" => Schema::TYPE_INTEGER,
            "currency_id" => Schema::TYPE_INTEGER,
            "name" => Schema::TYPE_STRING,
            "address" => Schema::TYPE_STRING,
            "email" => Schema::TYPE_STRING,
            "phone" => Schema::TYPE_STRING,
            "description" => Schema::TYPE_TEXT,
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171228_160825_addUserPayWalletAdvCash cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171228_160825_addUserPayWalletAdvCash cannot be reverted.\n";

        return false;
    }
    */
}
