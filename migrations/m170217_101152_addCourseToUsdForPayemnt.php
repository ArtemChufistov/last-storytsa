<?php
use yii\db\Schema;
use yii\db\Migration;

class m170217_101152_addCourseToUsdForPayemnt extends Migration
{
    public function up()
    {
        $this->addColumn('{{%payment}}', 'course_to_usd', Schema::TYPE_FLOAT);
        $this->addColumn('{{%payment}}', 'charged_sum', Schema::TYPE_FLOAT);
    }

    public function down()
    {
        echo "m170217_101152_addCourseToUsdForPayemnt cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
