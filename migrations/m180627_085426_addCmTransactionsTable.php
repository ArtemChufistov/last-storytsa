<?php

use yii\db\Migration;

/**
 * Class m180627_085426_addCmTransactionsTable
 */
class m180627_085426_addCmTransactionsTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        $this->createTable("{{%cm_transaction}}", [
            "id" => $this->primaryKey(),
            "payment_id" => $this->integer()->defaultValue(NULL),
            "account" => $this->integer()->defaultValue(NULL),
            "address" => $this->string(255)->defaultValue(NULL),
            "category" => $this->string(255)->defaultValue(NULL),
            "amount" => $this->float(0),
            "label" => $this->string(255)->defaultValue(NULL),
            "vout" => $this->integer()->defaultValue(NULL),
            "confirmations" => $this->integer()->defaultValue(NULL),
            "blockhash" => $this->string(255)->defaultValue(NULL),
            "blockindex" => $this->integer()->defaultValue(NULL),
            "blocktime" => $this->timestamp(),
            "txid" => $this->string(255)->defaultValue(NULL),
            "time" => $this->dateTime(),
            "timereceived" => $this->dateTime()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%cm_transaction}}');

        return false;
    }
}
