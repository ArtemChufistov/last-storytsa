<?php
use yii\db\Schema;
use yii\db\Migration;

class m170330_033805_addCountPlaces extends Migration
{
    public function up()
    {
        $this->addColumn('{{%matrix}}', 'countPlaces', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m170330_033805_addCountPlaces cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
