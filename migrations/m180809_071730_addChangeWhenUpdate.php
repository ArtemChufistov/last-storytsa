<?php

use yii\db\Migration;

/**
 * Class m180809_071730_addChangeWhenUpdate
 */
class m180809_071730_addChangeWhenUpdate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'changeWhenUpdate', $this->integer()->defaultValue(1));
        $this->addColumn('{{%category}}', 'changeWhenUpdate', $this->integer()->defaultValue(1));
        $this->addColumn('{{%campaign}}', 'changeWhenUpdate', $this->integer()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180809_071730_addChangeWhenUpdate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180809_071730_addChangeWhenUpdate cannot be reverted.\n";

        return false;
    }
    */
}
