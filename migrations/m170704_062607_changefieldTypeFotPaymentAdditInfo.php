<?php

use yii\db\Migration;

class m170704_062607_changefieldTypeFotPaymentAdditInfo extends Migration
{
    public function up()
    {
        $this->alterColumn('payment','additional_info', $this->text()); 
    }

    public function down()
    {
        echo "m170704_062607_changefieldTypeFotPaymentAdditInfo cannot be reverted.\n";

        return false;
    }
}
