<?php

use yii\db\Migration;

class m161228_172658_closureTableForUser extends Migration
{
    // Closure table name
    public $closureTbl = "user_tree";

    // Name of the table to which you connect the behavior
    public $relativeTbl = "lb_user";

    public function up()
    {
        $this->createTable("{{%{$this->closureTbl}}}", [
            "parent" => $this->integer()->notNull(),
            "child" => $this->integer()->notNull(),
            "depth" => $this->integer()->notNull()->defaultValue(0)
        ]);
        $this->addPrimaryKey("PK_{$this->closureTbl}", "{{%{$this->closureTbl}}}", ["parent", "child"]);
        $this->createIndex("FK_{$this->closureTbl}_child_{$this->relativeTbl}", "{{%{$this->closureTbl}}}", "child");
        $this->addForeignKey("FK_{$this->closureTbl}_child_{$this->relativeTbl}",
            "{{%{$this->closureTbl}}}", "child",
            "{{%{$this->relativeTbl}}}", "id",
            "CASCADE"
        );
        $this->addForeignKey("FK_{$this->closureTbl}_parent_{$this->relativeTbl}",
            "{{%{$this->closureTbl}}}", "parent",
            "{{%{$this->relativeTbl}}}", "id",
            "CASCADE"
        );
    }

    public function down()
    {
        $this->dropForeignKey("FK_{$this->closureTbl}_parent_{$this->relativeTbl}", "{{%{$this->closureTbl}}}");
        $this->dropForeignKey("FK_{$this->closureTbl}_child_{$this->relativeTbl}", "{{%{$this->closureTbl}}}");
        $this->dropTable("{{%{$this->closureTbl}}}");
    }
}
