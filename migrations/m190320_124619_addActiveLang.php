<?php

use yii\db\Migration;

/**
 * Class m190320_124619_addActiveLang
 */
class m190320_124619_addActiveLang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%lang}}', 'active', $this->integer()->defaultValue(1)->after('default'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190320_124619_addActiveLang cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190320_124619_addActiveLang cannot be reverted.\n";

        return false;
    }
    */
}
