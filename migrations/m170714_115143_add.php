<?php
use yii\db\Migration;
use yii\db\Schema;

class m170714_115143_add extends Migration {
	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable("{{%currency_graph_history}}", [
			"id" => Schema::TYPE_PK,
			"from_currency_id" => Schema::TYPE_INTEGER,
			"to_currency_id" => Schema::TYPE_INTEGER,
			"course" => Schema::TYPE_FLOAT,
			"course_to_usd" => Schema::TYPE_FLOAT,
			"date" => Schema::TYPE_DATETIME,
		], $tableOptions);
	}

	public function safeDown() {
		echo "m170714_115143_add cannot be reverted.\n";

		return false;
	}

	/*
		    // Use up()/down() to run migration code without a transaction.
		    public function up()
		    {

		    }

		    public function down()
		    {
		        echo "m170714_115143_add cannot be reverted.\n";

		        return false;
		    }
	*/
}
