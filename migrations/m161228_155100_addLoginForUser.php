<?php

use yii\db\Migration;

class m161228_155100_addLoginForUser extends Migration
{
    public function up()
    {
        $this->addColumn('{{%lb_user}}', 'login', $this->string(100)->defaultValue(NULL));
    }

    public function down()
    {
        echo "m161228_155100_addLoginForUser cannot be reverted.\n";

        return false;
    }
}
