<?php

use yii\db\Migration;

class m170110_172348_addPublicResponse extends Migration
{
    public function up()
    {
       $this->addColumn('{{%response}}', 'public', $this->integer(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%response}}', 'public');

        return false;
    }
}
