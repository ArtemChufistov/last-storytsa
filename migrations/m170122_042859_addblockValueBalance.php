<?php

use yii\db\Migration;

class m170122_042859_addblockValueBalance extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_balance}}', 'block_value', $this->float()->defaultValue(0));
    }

    public function down()
    {
        echo "m170122_042859_addblockValueBalance cannot be reverted.\n";

        return false;
    }
}
