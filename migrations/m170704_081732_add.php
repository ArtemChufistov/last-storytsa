<?php
use yii\db\Schema;
use yii\db\Migration;

class m170704_081732_add extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%fchange_transaction}}", [
            "id" => Schema::TYPE_PK,
            "payment_id" => Schema::TYPE_INTEGER,
            "merchant_name" => Schema::TYPE_STRING.'(255)',
            "merchant_title" => Schema::TYPE_STRING.'(255)',
            "payed_paysys" => Schema::TYPE_STRING.'(255)',
            "amount" => Schema::TYPE_FLOAT,
            "payment_info" => Schema::TYPE_STRING.'(255)',
            "payment_num" => Schema::TYPE_STRING.'(255)',
            "sucess_url" => Schema::TYPE_STRING.'(255)',
            "error_url" => Schema::TYPE_STRING.'(255)',
            "obmen_order_id" => Schema::TYPE_STRING.'(255)',
            "obmen_recive_valute" => Schema::TYPE_STRING.'(255)',
            "obmen_timestamp" => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function safeDown()
    {
        echo "m170704_081732_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170704_081732_add cannot be reverted.\n";

        return false;
    }
    */
}
