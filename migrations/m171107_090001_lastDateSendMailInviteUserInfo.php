<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171107_090001_lastDateSendMailInviteUserInfo
 */
class m171107_090001_lastDateSendMailInviteUserInfo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_info}}', 'lastDateSendMailInvite', $this->dateTime());
        $this->addColumn('{{%user_info}}', 'lang', $this->string(255)->defaultValue(NULL));
        $this->addColumn('{{%user_info}}', 'online_date', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171107_090001_lastDateSendMailInviteUserInfo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171107_090001_lastDateSendMailInviteUserInfo cannot be reverted.\n";

        return false;
    }
    */
}
