<?php

use yii\db\Migration;

class m161227_121556_addIconForMenu extends Migration
{
    public function up()
    {
        $this->addColumn('{{%menu}}', 'icon', $this->string(255)->defaultValue(NULL));

         $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Офис меню',
            'link' => '/office',
            'lft' => '1',
            'rgt' => '34',
            'depth' => '0',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Профиль',
            'link' => '/office',
            'lft' => '2',
            'rgt' => '9',
            'depth' => '1',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Платёжные реквизиты',
            'link' => '/office',
            'lft' => '10',
            'rgt' => '17',
            'depth' => '1',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

         $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Структура',
            'link' => '/office',
            'lft' => '18',
            'rgt' => '21',
            'depth' => '1',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Матрица',
            'link' => '/office',
            'lft' => '22',
            'rgt' => '23',
            'depth' => '1',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Финансы',
            'link' => '/office',
            'lft' => '24',
            'rgt' => '25',
            'depth' => '1',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Рекламные материалы',
            'link' => '/office',
            'lft' => '26',
            'rgt' => '27',
            'depth' => '1',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Оставить отзыв',
            'link' => '/office',
            'lft' => '28',
            'rgt' => '29',
            'depth' => '1',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Поддержка',
            'link' => '/office',
            'lft' => '30',
            'rgt' => '31',
            'depth' => '1',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Выход',
            'link' => '/logout',
            'lft' => '32',
            'rgt' => '33',
            'depth' => '1',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Персональные данные',
            'link' => '/office',
            'lft' => '3',
            'rgt' => '4',
            'depth' => '2',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Аватар',
            'link' => '/office',
            'lft' => '5',
            'rgt' => '6',
            'depth' => '2',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Подтверждение почты',
            'link' => '/office',
            'lft' => '7',
            'rgt' => '8',
            'depth' => '2',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Добавление кошелька',
            'link' => '/office',
            'lft' => '11',
            'rgt' => '12',
            'depth' => '2',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Проверка пользователя',
            'link' => '/office',
            'lft' => '13',
            'rgt' => '14',
            'depth' => '2',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Реферальная ссылка',
            'link' => '/office',
            'lft' => '15',
            'rgt' => '16',
            'depth' => '2',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));

        $this->insert('{{%menu}}',array(
            'tree'=>'17',
            'title' =>'Статистика',
            'link' => '/office',
            'lft' => '19',
            'rgt' => '20',
            'depth' => '2',
            'name' => 'officeMenu',
            'lang' => 'ru-RU',
          ));
    }

    public function down()
    {
        $this->dropColumn('{{%menu}}', 'icon');

        return false;
    }
}
