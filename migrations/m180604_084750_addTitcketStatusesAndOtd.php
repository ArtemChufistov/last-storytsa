<?php

use yii\db\Migration;

/**
 * Class m180604_084750_addTitcketStatusesAndOtd
 */
class m180604_084750_addTitcketStatusesAndOtd extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->insert('{{%ticket_status}}',array(
            'name'=>'Новый',
            'color' =>'#ffff00',
          ));

         $this->insert('{{%ticket_status}}',array(
            'name'=>'В работе',
            'color' =>'#00ff00',
          ));

         $this->insert('{{%ticket_status}}',array(
            'name'=>'Выполнен',
            'color' =>'#6fa8dc',
          ));

         $this->insert('{{%ticket_department}}',array(
            'name'=>'Техническая поддержка',
          ));

         $this->insert('{{%ticket_department}}',array(
            'name'=>'Администрация',
          ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180604_084750_addTitcketStatusesAndOtd cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180604_084750_addTitcketStatusesAndOtd cannot be reverted.\n";

        return false;
    }
    */
}
