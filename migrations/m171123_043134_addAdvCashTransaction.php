<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171123_043134_addAdvCashTransaction
 */
class m171123_043134_addAdvCashTransaction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable("{{%advcash_transaction}}", [
            "id" => Schema::TYPE_PK,
            "ac_transfer" => Schema::TYPE_STRING,
            "ac_start_date" => Schema::TYPE_DATETIME,
            "ac_sci_name" => Schema::TYPE_STRING,
            "ac_src_wallet" => Schema::TYPE_STRING,
            "ac_dest_wallet" => Schema::TYPE_STRING,
            "ac_order_id" => Schema::TYPE_STRING,
            "ac_amount" => Schema::TYPE_FLOAT,
            "ac_merchant_currency" => Schema::TYPE_STRING,
            "treated" => Schema::TYPE_INTEGER,
            "date_tread" => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171123_043134_addAdvCashTransaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_043134_addAdvCashTransaction cannot be reverted.\n";

        return false;
    }
    */
}
