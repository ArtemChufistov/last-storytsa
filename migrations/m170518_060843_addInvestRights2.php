<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170518_060843_addInvestRights2 extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            // управление першинами
            ['userInvestIndex', AuthItem::TYPE_PERMISSION, 'Приобретённые пакеты - список', NULL, time(), time()],
            ['userInvestView', AuthItem::TYPE_PERMISSION, 'Приобретённые пакеты - просмотр', NULL, time(), time()],
            ['userInvestCreate', AuthItem::TYPE_PERMISSION, 'Приобретённые пакеты - добавление', NULL, time(), time()],
            ['userInvestUpdate', AuthItem::TYPE_PERMISSION, 'Приобретённые пакеты - редактирование', NULL, time(), time()],
            ['userInvestDelete', AuthItem::TYPE_PERMISSION, 'Приобретённые пакеты - удаление', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userInvestIndex']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userInvestView']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userInvestCreate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userInvestUpdate']]);
        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'userInvestDelete']]);
    }

    public function safeDown()
    {
        echo "m170518_060843_addInvestRights2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170518_060843_addInvestRights2 cannot be reverted.\n";

        return false;
    }
    */
}
