<?php

use yii\db\Migration;

class m161226_082631_addcronColumns extends Migration
{
    public function up()
    {
        $this->addColumn('{{%job}}', 'key', $this->string(255)->notNull());
        $this->addColumn('{{%job}}', 'frequency', $this->string(255)->notNull());
        $this->addColumn('{{%job}}', 'last_date', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn('{{%job}}', 'key');
        $this->dropColumn('{{%job}}', 'frequency');
        $this->dropColumn('{{%job}}', 'last_date');

        return false;
    }
}
