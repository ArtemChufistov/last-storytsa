<?php

use yii\db\Migration;

class m161226_093003_addactiveforjob extends Migration
{
    public function up()
    {
       $this->addColumn('{{%job}}', 'active', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%job}}', 'active');

        return false;
    }
}
