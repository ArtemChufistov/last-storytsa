<?php

use yii\db\Migration;
use yii\db\Schema;

class m170814_032737_lb_jobinfo extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable("{{%lb_jobinfo}}", [
            "id" => Schema::TYPE_PK,
            "company_name" => Schema::TYPE_STRING.'(100)',
            "state_id" => Schema::TYPE_INTEGER.'(11)',
            "name" => Schema::TYPE_STRING.'(100)',
            "phone" => Schema::TYPE_STRING.'(100)',
            "email" => Schema::TYPE_STRING.'(100)',
            "website" => Schema::TYPE_STRING.'(100)',
            "occupation_id" => Schema::TYPE_INTEGER.'(11)',
            "attach_file" => Schema::TYPE_STRING.'(100)',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{%lb_jobinfo}}");

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170814_032737_lb_jobinfo cannot be reverted.\n";

        return false;
    }
    */
}
