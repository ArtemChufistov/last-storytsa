<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;
/**
 * Class m180130_092747_addstatUserBalanceIndex
 */
class m180130_092747_addstatUserBalanceIndex extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['statUserBalanceIndex', AuthItem::TYPE_PERMISSION, 'Статистика внутренних балансов', NULL, time(), time()],
        ]);

        $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [['administrator', 'statUserBalanceIndex']]);
    }

    public function safeDown()
    {
        echo "m170603_045328_addRules cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180130_092747_addstatUserBalanceIndex cannot be reverted.\n";

        return false;
    }
    */
}
