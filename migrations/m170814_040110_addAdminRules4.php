<?php

use yii\db\Migration;
use lowbase\user\models\AuthItem;

class m170814_040110_addAdminRules4 extends Migration
{
    public function safeUp()
    {
      $this->batchInsert('lb_auth_item', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
          // штаты usa
          ['usaStateIndex', AuthItem::TYPE_PERMISSION, 'Штаты USA', NULL, time(), time()],
          ['usaStateView', AuthItem::TYPE_PERMISSION, 'Штаты USA - просмотр', NULL, time(), time()],
          ['usaStateUpdate', AuthItem::TYPE_PERMISSION, 'Штаты USA - редактирование', NULL, time(), time()],
          ['usaStateCreate', AuthItem::TYPE_PERMISSION, 'Штаты USA - создание', NULL, time(), time()],
          ['usaStateDelete', AuthItem::TYPE_PERMISSION, 'Штаты USA - удаление', NULL, time(), time()],
          // категории работы
          ['occupationIndex', AuthItem::TYPE_PERMISSION, 'Категории работы', NULL, time(), time()],
          ['occupationView', AuthItem::TYPE_PERMISSION, 'Категории работы - просмотр', NULL, time(), time()],
          ['occupationUpdate', AuthItem::TYPE_PERMISSION, 'Категории работы - редактирование', NULL, time(), time()],
          ['occupationCreate', AuthItem::TYPE_PERMISSION, 'Категории работы - создание', NULL, time(), time()],
          ['occupationDelete', AuthItem::TYPE_PERMISSION, 'Категории работы - удаление', NULL, time(), time()],
          // формы работы
          ['jobInfoIndex', AuthItem::TYPE_PERMISSION, 'Формы работы', NULL, time(), time()],
          ['jobInfoView', AuthItem::TYPE_PERMISSION, 'Формы работы - просмотр', NULL, time(), time()],
          ['jobInfoUpdate', AuthItem::TYPE_PERMISSION, 'Формы работы - редактирование', NULL, time(), time()],
          ['jobInfoCreate', AuthItem::TYPE_PERMISSION, 'Формы работы - создание', NULL, time(), time()],
          ['jobInfoDelete', AuthItem::TYPE_PERMISSION, 'Формы работы - удаление', NULL, time(), time()]
      ]);

      $this->batchInsert('lb_auth_item_child', ['parent', 'child'], [
          ['moderator', 'usaStateIndex'],
          ['moderator', 'usaStateView'],
          ['moderator', 'usaStateCreate'],
          ['moderator', 'usaStateUpdate'],
          ['moderator', 'usaStateDelete'],

          ['moderator', 'occupationIndex'],
          ['moderator', 'occupationView'],
          ['moderator', 'occupationCreate'],
          ['moderator', 'occupationUpdate'],
          ['moderator', 'occupationDelete'],

          ['moderator', 'jobInfoIndex'],
          ['moderator', 'jobInfoView'],
          ['moderator', 'jobInfoCreate'],
          ['moderator', 'jobInfoUpdate'],
          ['moderator', 'jobInfoDelete'],

          ['administrator', 'usaStateIndex'],
          ['administrator', 'usaStateView'],
          ['administrator', 'usaStateCreate'],
          ['administrator', 'usaStateUpdate'],
          ['administrator', 'usaStateDelete'],

          ['administrator', 'occupationIndex'],
          ['administrator', 'occupationView'],
          ['administrator', 'occupationCreate'],
          ['administrator', 'occupationUpdate'],
          ['administrator', 'occupationDelete'],

          ['administrator', 'jobInfoIndex'],
          ['administrator', 'jobInfoView'],
          ['administrator', 'jobInfoCreate'],
          ['administrator', 'jobInfoUpdate'],
          ['administrator', 'jobInfoDelete']
      ]);
    }

    public function safeDown()
    {
        echo "m170814_040110_addAdminRules4 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170814_040110_addAdminRules4 cannot be reverted.\n";

        return false;
    }
    */
}
