<?php if ($model->from_user_id != \Yii::$app->user->identity->id):?>
	<?php echo Yii::t('app', 'Подарок от партнера {level} уровня', ['level' => empty($model->additional_info) ? 1 : $model->additional_info]);?>
<?php else:?>
	<?php echo $model->getTypeTitle();?>
<?php endif;?>
