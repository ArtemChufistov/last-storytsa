<?php
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\payment\models\Payment;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$fromUser = $payment->getFromUser()->one();
?>
  <div class="modalPaymentBuyPlace modal">
  	<?php $form = ActiveForm::begin(); ?>
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">
          	<?php if ($payment->type == Payment::TYPE_BUY_PLACE):?>
          		<?php echo Yii::t('app', 'Вам поступил перевод от участника за приобретение места');?>
          	<?php elseif ($payment->type == Payment::TYPE_BUY_LEVEL):?>
          		<?php echo Yii::t('app', 'Вам поступил перевод от участника за покупку уровня № {level}', ['level' => $payment->additional_info]);?>
          	<?php endif;?>
          </h4>
        </div>
        <div class="modal-body">

          <div class = "row">

	        <div class="col-md-12">
	          <div class="box box-widget widget-user-2">
	            <div class="widget-user-header bg-green">
	            	<?php if ($fromUser->image != ''):?>
		              <div class="widget-user-image">
		                <img class="img-circle" src="/<?php echo $fromUser->image; ?>" alt="<?php echo $fromUser->login; ?>">
		              </div>
		            <?php endif;?>
	              <h3 class="widget-user-username"><?php echo $fromUser->email; ?></h3>
	              <h5 class="widget-user-desc"><?php echo $fromUser->login; ?></h5>
	            </div>
	            <div class="box-footer no-padding">
	              <ul class="nav nav-stacked">
	                <li><a href="#"><?php echo Yii::t('app', 'ФИО');?> <span class="pull-right badge bg-green"><?php echo $fromUser->first_name; ?> <?php echo $fromUser->last_name; ?></span></a></li>
	                <li><a href="#"><?php echo Yii::t('app', 'Телефон');?> <span class="pull-right badge bg-red"><?php echo $fromUser->phone; ?></span></a></li>
	                <li><a href="#"><?php echo Yii::t('app', 'Skype');?> <span class="pull-right badge bg-blue"><?php echo $fromUser->skype; ?></span></a></li>
	              </ul>
	            </div>
	          </div>
	        </div>

          	<div class = "col-sm-12">
	          	<div class="input-group">
					<div class="input-group-btn">
						<button class="btn btn-success payment-sum" type="button" data-clipboard-target = "#payment-sum"><?php echo Yii::t('app', 'Сумма');?></button>
					</div>
					<input class="form-control" id ="payment-sum" readonly="true" type="text" value = "<?php echo $payment->sum;?> BTC">
				</div>
				</br>
	          	<div class="input-group">
					<div class="input-group-btn">
						<button class="btn btn-success payment-sum" type="button" data-clipboard-target = "#payment-sum"><?php echo Yii::t('app', 'Хэш транзакции');?></button>
					</div>
					<input class="form-control" id ="payment-sum" readonly="true" type="text" value = "<?php echo $payment->transaction_hash;?>">
				</div>

				</br>
				<?php if ($payment->status == Payment::STATUS_WAIT):?>
					<p class="margin"><?php echo Yii::t('app', 'Ожидайте подтверждения от участника');?></p>
				<?php elseif ($payment->status == Payment::STATUS_WAIT_SYSTEM):?>
					<?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Я получил перевод'), [
		                'class' => 'btn btn-success pull-left',
		                'value' => '1',
		                'name' => 'gotPayment']) ?>
				<?php elseif ($payment->status == Payment::PROCESS_SYSTEM):?>
					<p class="margin"><?php echo Yii::t('app', 'Происходит поиск места в структуре, ожидайте');?></p>
				<?php elseif ($payment->status == Payment::STATUS_OK):?>
					<p class="margin"><?php echo Yii::t('app', 'Оплата обработана');?></p>
				<?php elseif ($payment->status == Payment::STATUS_CANCEL):?>
					<p class="margin"><?php echo Yii::t('app', 'Оплата отменена');?></p>
				<?php endif;?>
          	</div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
    <?php ActiveForm::end();?>
  </div>

<?php $this->registerJs("
  $('.modalPaymentBuyPlace').modal('show');
", View::POS_END);?>