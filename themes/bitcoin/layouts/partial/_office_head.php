<?php
use yii\web\View;
use yii\helpers\Html;
use lowbase\user\models\User;
use app\modules\profile\widgets\NotifyWidget;
?>

<!-- Logo -->
<a href="/" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>S</b></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><img src="/images/logo-light.png" alt="" width="138" height="31"></span>
</a>

<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only"><?php echo Yii::t('app', 'Навигация')?></span>
  </a>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
 
      <div id="google_translate_element" style = "float: left;
    height: 25px;
    margin-top: 13px;
    overflow: hidden;"></div><script type="text/javascript">
      function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
      }
      </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
 
      <?php /* echo NotifyWidget::widget(['user' => $user]);*/ ?>
      <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

          <?php
          if ($user->identity->image) {
              echo "<img src='/".$user->identity->image."' class='user-image' alt='" . Yii::t('app', 'Фото') . "'>";
          } else {
              echo "<img src='/images/default-avatar.jpg' class='user-image' alt='" . Yii::t('app', 'Фото') . "'>";
          }
          ?>

          <span class="hidden-xs"><?php echo $user->identity->login;?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">

            <?php
            if ($user->identity->image) {
                echo "<img src='/".$user->identity->image."' class='img-circle' alt='" . Yii::t('app', 'Фото') . "'>";
            } else {
                echo "<img src='/images/default-avatar.jpg' class='img-circle' alt='" . Yii::t('app', 'Фото') . "'>";
            }
            ?>

            <p>
              <?= $user->identity->first_name;?> <?= $user->identity->last_name;?>
              <small><?= Yii::t('app', 'Участник проекта');?></small>
            </p>
          </li>
          <!-- Menu Body -->
          <li class="user-body">
            <div class="row">
              <div class="col-xs-4 text-center">
                <a href="/profile/office/struct"><?= Yii::t('app', 'Структура');?></a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="/profile/office/matrix"><?= Yii::t('app', 'Storytsa');?></a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="/profile/office/promo"><?= Yii::t('app', 'Реклама');?></a>
              </div>
            </div>
            <!-- /.row -->
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="/profile/office/index" class="btn btn-default btn-flat"><?= Yii::t('app', 'Профиль');?></a>
            </div>
            <div class="pull-right">
              <a href="/logout" class="btn btn-default btn-flat"><?= Yii::t('app', 'Выйти');?></a>
            </div>
          </li>
        </ul>
      </li>

    </ul>
  </div>
</nav>