<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'О Нас',
    'url' => '/about'
  ],
];
$this->title = $model->title;
?>

</header>
<!-- Classic Breadcrumbs-->
<section class="breadcrumb-classic">
  <div class="shell section-34 section-sm-50">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-account-switch icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h2><span class="big"><?= Yii::t('app', 'О Нас');?></span></h2>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">

          <?= Breadcrumbs::widget([
            'options' => ['class' => 'list-inline list-inline-dashed p'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>

      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
  </svg>
</section>
<!-- Page Content-->
<main class="page-content">
  <!-- Meet Our Team-->
  <section class="section-team-custom-effects section-top-98 section-bottom-66 section-lg-top-110 section-lg-bottom-0">
    <div class="shell-fluid section-relative">
      <h1><?= Yii::t('app', 'О Нас');?></h1>
      <hr class="divider bg-mantis">
      <div class="offset-top-66">
        <div data-items="1" data-nav="false" data-dots="false" data-nav-custom=".owl-custom-navigation" data-animation-out="fadeOut" class="owl-carousel owl-carousel-classic owl-carousel-class-light">
          
          <div>
            <div class="range range-sm-middle range-condensed range-xs-center range-lg-left">
              <div class="cell-xs-10 cell-lg-6 cell-xl-5 text-lg-right text-lg-left inset-lg-right-50"><img width="140" height="140" src="images/users/user-johns-doe-140x140.jpg" alt="" class="img-responsive img-circle reveal-inline-block veil-lg"><img width="470" height="320" alt="" src="images/users/user-john-doe-470x640.png" class="img-responsive veil reveal-lg-inline-block">
                <h1 class="bg-title veil reveal-xl-block">STORYTSA<span>STORYTSA</span></h1>
              </div>
              <div class="cell-xs-10 cell-lg-4 cell-xl-3 text-lg-left offset-top-34 offset-lg-top-0 cell-lg-preffix-1 cell-xl-preffix-2">
                <div class="slide-content-animate">
                  
                  <p><?= Yii::t('app', 'STORYTSA ориентирован на финансовое решение своих идей,планов,проблем. Наша цель состоит в том, чтобы создать сообщество единомышленников, которые имеют желание помочь кому-то в их задачах и решить свои.');?></p>
                  <p><?= Yii::t('app', 'Мы создали платформу , которая поможет людям получить средства на поддержку своего дела, на лечение и реабилитацию,частично или полностью расплатиться с кредитами и ипотекой, получить средства для специального проекта или многих других планов, задач или проблем.');?></p>
                  <p><?= Yii::t('app', 'Наша концепция проста: Помоги человеку и тебе это вернется сторицей. Наша платформа начинается с оказания помощи одному из участников который собирает средства на какое-то свое дело. После того как вы оказали помощь, вам смогут ее оказать другие.');?></p>
                  <p><?= Yii::t('app', 'Наша платформа-это способ реализации закона вселенной: Помогая другим - помогаешь себе. Сделай добро и оно вернется к тебе стократно.');?></p>
                </div>
              </div>
            </div>
          </div>
          
      </div>
    </div>
  </section>
  <!-- Want to Join Us?-->
  <section class="section-66 bg-gray-darkest context-dark">
    <div class="shell">
      <h2>Почему мы??</h2>
      <hr class="divider bg-mantis">
      <div class="range range-xs-center">
        <div class="cell-xs-10">
          <p><?= Yii::t('app', 'Мы строим амбициозные планы создать сообщество единомышленников , которые имеют желание помочь кому то в их задачах и решить свои.');?></p><a href="/signup" class="btn btn-primary btn-icon btn-icon-left offset-top-20"><span class="icon mdi mdi-check-all"></span><?= Yii::t('app', 'Присоединиться');?></a>
        </div>
      </div>
    </div>
  </section>
</main>
<!-- Page Footer-->