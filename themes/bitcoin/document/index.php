<?php
use yii\helpers\Url;
use yii\helpers\Html;
use lowbase\document\DocumentAsset;
use app\modules\profile\models\User;
use app\modules\payment\models\Payment;

$this->title = $model->title;
?>

<style>
.mainVideoWrap{
  background: rgba(0, 0, 0, 0) linear-gradient(90deg, #42b574 0%, #84c450 100%) repeat scroll 0 0;
    border-radius: 2px;
    box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.4);
    margin: 0 auto;
    padding: 3px;
    width: 1000px;
    height: 700px;
}

@media (max-width: 799px) { 
  .mainVideoWrap{
    width: 500px !important;
    height: 370px;
  }
}
@media (min-width: 800px) { 
  .mainVideoWrap{
    width: 600px !important;
    height: 420px;
  }
}
@media (min-width: 1200px) { 
  .mainVideoWrap{
    width: 700px !important;
    height: 420px;
  }
}
.mainVideoWrap iframe{
  width: 100%;
  height: 100%;
}
</style>

  <!--Swiper-->
  <div class="rd-parallax rd-parallax-swiper">
    <div data-speed="0.3" data-sm-speed="1" data-type="html" class="rd-parallax-layer">
      <div data-loop="true" data-height="100vh" data-dragable="false" data-min-height="480px" class="swiper-container swiper-slider">
        <div class="swiper-wrapper text-center">
          <div id="page-loader" data-slide-bg="images/intros/slide1.jpg" data-preview-bg="images/intros/slide1.jpg" class="swiper-slide">
            <div data-speed="0.5" data-fade="true" class="swiper-caption swiper-parallax">
              <div class="swiper-slide-caption">
                <div class="shell">
                  <div class="range range-lg-center">
                    <div class="cell-lg-12">
                      <h1>
                        <span data-caption-animate="fadeInUp" data-caption-delay="700" class="big text-uppercase"><?= Yii::t('app', 'ДОБРО ПОЖАЛОВАТЬ В');?>
                          <img class='img-responsive' style ="margin: 0 auto; margin-top: 15px;" width='330' height='67' src='/images/logo_big.png' alt=''/>
                        </span>
                      </h1>
                    </div>
                    <div class="cell-lg-10 offset-top-30">
                      <h4 data-caption-animate="fadeInUp" data-caption-delay="900" class="hidden reveal-sm-block text-light">
                        </br><?= Yii::t('app', 'То, что мы не можем сделать в одиночку, мы можем сделать вместе!');?></br>
                      </h4>
                      <div class="group group-xl offset-top-20 offset-xs-top-50">
                        <a href="<?php echo Url::to(['/login']);?>" data-caption-animate="fadeInUp" data-caption-delay="1200" class="btn btn-primary btn-lg btn-anis-effect"><span class="btn-text"><?= Yii::t('app', 'личный кабинет');?></span></a>
                        <a href="<?php echo Url::to(['/signup']);?>" data-caption-animate="fadeInUp" data-caption-delay="1200" class="btn btn-default btn-lg btn-anis-effect"><span class="btn-text"><?= Yii::t('app', 'регистрация');?></span></a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div data-slide-bg="images/intros/slide2.jpg" data-preview-bg="images/intros/slide2.jpg" class="swiper-slide">
            <div data-speed="0.5" data-fade="true" class="swiper-caption swiper-parallax">
              <div class="swiper-slide-caption">
                <div class="shell">
                  <div class="range range-lg-center">
                    <div class="cell-lg-12">
                      <h1><span data-caption-animate="fadeInUp" data-caption-delay="700" class="big text-uppercase"><?= Yii::t('app', 'НЕОГРАНИЧЕННЫЕ ВОЗМОЖНОСТИ');?></span></h1>
                    </div>
                    <div class="cell-lg-10 offset-top-30">
                      <h4 data-caption-animate="fadeInUp" data-caption-delay="900" class="hidden reveal-sm-block text-light">
                        <?= Yii::t('app', 'Чем больше вы даете, тем больше вы можете получить.');?>

                      </h4>
                      <div class="group group-xl offset-top-20 offset-xs-top-50"><a href="<?php echo Url::to(['/signup']);?>" data-waypoint-to="#section-childs" data-caption-animate="fadeInUp" data-caption-delay="1200" class="btn btn-icon btn-icon-left btn-default btn-lg btn-anis-effect"><span class="btn-text"><?= Yii::t('app', 'Открыть возможность');?></span></a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div data-slide-bg="images/intros/slide3.jpg" data-preview-bg="images/intros/slide3.jpg" class="swiper-slide">
            <div data-speed="0.5" data-fade="true" class="swiper-caption swiper-parallax">
              <div class="swiper-slide-caption">
                <div class="shell">
                  <div class="range range-lg-center">
                    <div class="cell-lg-12">
                      <h1><span data-caption-animate="fadeInUp" data-caption-delay="700" class="big text-uppercase"><?= Yii::t('app', 'СООБЩЕСТВО ЕДИНОМЫШЛЕННИКОВ');?></span></h1>
                    </div>
                    <div class="cell-lg-10 offset-top-30">
                      <h4 data-caption-animate="fadeInUp" data-caption-delay="900" class="hidden reveal-sm-block text-light offset-bottom-0">
                        <?= Yii::t('app', 'Помогая другим - помогаешь себе. Сделай добро, и оно вернется к тебе стократно.');?>
                      </h4>
                      <div class="group group-xl offset-top-20 offset-xs-top-50"><a href="<?php echo Url::to(['/login']);?>" data-caption-animate="fadeInUp" data-caption-delay="1200" class="btn btn-icon btn-icon-left btn-primary btn-lg btn-anis-effect"><span class="icon mdi mdi-puzzle"></span><span class="btn-text"><?= Yii::t('app', 'Вход');?></span></a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div data-speed="0.5" class="swiper-button swiper-button-prev swiper-parallax">
          <div class="preview">

          </div>
        </div>
        <div data-speed="0.5" class="swiper-button swiper-button-next swiper-parallax">
          <div class="preview">

          </div>
        </div>
        <div class="swiper-pagination"></div>
      </div>
    </div>
  </div>
</header>

<main class="page-content">
    <!-- Classic Thumbnail-->
    <!-- Our Works-->
    <section id="section-childs" class="section-98 section-sm-110">
      <div class="shell-wide">
        <div class = "mainVideoWrap">
          <iframe  src="https://www.youtube.com/embed/Yd2fHYrYusA" frameborder="0" allowfullscreen></iframe>
        </div>
        <h2><span class="big"><?= Yii::t('app', 'Достижения');?></span></h2>
        <div>
          <hr class="divider bg-mantis">
        </div>
        <h3 class="text-regular offset-top-66"><img class="img-responsive" style="margin: 0 auto;" src="/images/grey-logo.png" alt="" width="330" height="67"></h3>
      </div>
    </section>
    <div class="shell-fluid">
      <hr class="hr bg-gray-lighter">
    </div>
    <!-- Skills-->
    <section class="section-top-40">
      <div class="shell-wide">
        <div class="range range-xs-center">
          <div class="cell-sm-10 cell-lg-12">
            <div class="range range-xs-center">
              <div class="cell-md-3 cell-sm-5">
                        <!-- Counter type 2-->
                        <div class="counter-type-2"><span class="icon mdi mdi-account-multiple-outline text-mantis"></span>
                          <div class="offset-top-10"><span data-speed="1200" data-from="0" data-to="<?php echo User::find()->count();?>" class="h1 text-bold counter"></span><span class="h1 text-bold">+</span></div>
                          <!--<div class="offset-top-10"><span data-speed="1200" data-from="0" data-to="<?php echo User::find()->join('INNER JOIN', 'matrix', 'matrix.user_id = lb_user.id')->count();?>" class="h1 text-bold counter"></span><span class="h1 text-bold">+</span></div>-->
                          <div class="h6 text-uppercase text-spacing-60 text-bold offset-top-14"><?= Yii::t('app', 'Всего участников');?></div>
                        </div>
              </div>
              <div class="cell-md-3 cell-sm-5 offset-top-20 offset-sm-top-0">
                        <!-- Counter type 2-->
                        <div class="counter-type-2"><span class="icon mdi mdi-account-star-variant text-malibu"></span>
                          <div class="offset-top-10">
                            <div data-speed="1600" data-from="0" data-to="<?php echo Payment::find()->orWhere(['status' => Payment::STATUS_WAIT])->orWhere(['status' => Payment::STATUS_WAIT_SYSTEM])->orWhere(['status' => Payment::PROCESS_SYSTEM])->count()+10;?>" class="h1 text-bold counter"></div>
                          </div>
                          <div class="h6 text-uppercase text-spacing-60 text-bold offset-top-14"><?= Yii::t('app', 'участников хотят сделать подарок');?></div>
                        </div>
              </div>
              <div class="cell-md-3 cell-sm-5 offset-top-66 offset-md-top-0">
                        <!-- Counter type 2-->
                        <div class="counter-type-2"><span class="icon mdi mdi-account-switch text-neon-carrot"></span>
                          <div class="offset-top-10">
                            <div data-speed="2000" data-from="0" data-to="<?php echo Payment::find()->where(['status' => Payment::STATUS_OK])->count()+741;?>" class="h1 text-bold counter"></div>
                          </div>
                          <div class="h6 text-uppercase text-spacing-60 text-bold offset-top-14"><?= Yii::t('app', 'отправленных подарков');?></div>
                        </div>
              </div>
              <div class="cell-md-3 cell-sm-5 offset-top-66 offset-md-top-0">
                        <!-- Counter type 2-->
                        <div class="counter-type-2"><span class="icon mdi mdi-trophy-outline text-ku-crimson"></span>
                          <div class="offset-top-10"><span data-speed="2400" data-from="0" data-to="<?php echo Payment::find()->select('sum(sum) as sum')->where(['status' => Payment::STATUS_OK])->one()->sum+29;?>" class="h1 text-bold counter"></span><span class="h1 text-bold">+</span></div>
                          <div class="h6 text-uppercase text-spacing-60 text-bold offset-top-14"><?= Yii::t('app', 'btc всего подаренно');?></div>
                        </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="shell-fluid offset-top-98">
        <hr class="hr bg-gray">
      </div>
    </section>
    <!-- Button Ansi-->
    <!-- Buy Now-->
    <section>
              <!-- Call to action type 2-->
              <section class="section-66 bg-acapulco context-dark">
                <div class="shell">
                  <div class="range range-xs-middle range-condensed">
                    <div class="cell-md-8 cell-lg-9 text-center text-md-left">
                      <h2><span class="big"><?= Yii::t('app', 'Да! Это то, что мне нужно!');?></span></h2>
                    </div>
                    <div class="cell-md-4 cell-lg-3 offset-top-41 offset-md-top-0"><a href="<?php echo Url::to(['/signup']);?>" class="btn btn-icon btn-lg btn-default btn-anis-effect btn-icon-btn-icon-left"><span class="icon mdi mdi-rocket"></span><?= Yii::t('app', 'Присоединиться');?></a>
                    </div>
                  </div>
                </div>
              </section>
    </section>
    <!-- What Do You Get With Intense?-->
    <section class="section-98 section-sm-110">
      <div class="shell">
        <h2><span class="big"> <?= Yii::t('app', 'Что вы можете получить от проекта?');?></span></h2>
        <hr class="divider bg-mantis">
        <div class="range range-xs-center offset-top-50 offset-top-66">
          <div class="cell-sm-7 cell-md-4">
            <div class="inset-xs-left-50 inset-xs-right-50 inset-md-left-0 inset-md-right-0">
                      <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-red-filled icon-outlined mdi mdi-chart-pie"></span>
                      <div>
                        <h4 class="offset-top-30 text-bold"><?= Yii::t('app', 'Решить мелкие финансовые проблемы');?></h4>
                      </div>
            </div>
          </div>
          <div class="cell-sm-7 cell-md-4 offset-top-66 offset-md-top-0">
            <div class="inset-xs-left-50 inset-xs-right-50 inset-md-left-0 inset-md-right-0 inset-lg-left-50 inset-lg-right-50">
                      <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-blue-gray-filled icon-outlined mdi mdi-chart-areaspline"></span>
                      <div>
                        <h4 class="offset-top-30 text-bold"><?= Yii::t('app', 'Создать постоянный источник дохода');?></h4>
                      </div>
            </div>
          </div>
          <div class="cell-sm-7 cell-md-4 offset-top-66 offset-md-top-0">
            <div class="inset-xs-left-50 inset-xs-right-50 inset-md-left-0 inset-md-right-0">
                      <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-mantis-filled icon-outlined mdi mdi-chart-bar"></span>
                      <div>
                        <h4 class="offset-top-30 text-bold"><?= Yii::t('app', 'Эффективно реализовать свои планы');?></h4>
                      </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Home Layouts and Demos-->
    <section class="section-top-66 section-bottom-85 section-lg-top-66 section-lg-bottom-124 bg-lighter">
      <div class="shell">
        <div class="range range-xs-center">
          <div class="cell-sm-10 cell-lg-8 text-lg-left">
            <div style="margin-bottom: 30px;" class="mock-up-wrapper veil reveal-lg-inline-block">
              <div data-wow-duration="1s" data-wow-delay="0.2s" style="" class="wow fadeInLeft"><img src="images/features/home-intro-21-470x327.jpg" width="470" height="327" alt="" class="img-responsive shadow-drop-lg"></div>
              <div data-wow-duration="1s" data-wow-delay="0.6s" style="position: absolute; top: 85px; right: -250px;" class="wow fadeInLeft"><img src="images/features/home-intro-20-470x327.jpg" width="470" height="327" alt="" class="img-responsive shadow-drop-lg"></div>
              <div data-wow-duration="1s" data-wow-delay="1.2s" style="position: absolute; right: -95px; bottom: -200px; z-index: 1;" class="wow fadeInLeft"><img src="images/features/home-intro-19-470x327.jpg" width="470" height="327" alt="" class="img-responsive shadow-drop-lg"></div>
            </div><img src="images/features/home-intro-01-750x600.png" width="750" height="600" alt="" data-wow-delay="0.6s" class="img-responsive veil reveal-sm-inline-block veil-lg wow fadeIn">
          </div>
          <div class="cell-sm-8 cell-lg-4 offset-top-0 offset-sm-top-41 offset-lg-top-0 text-lg-left">
            <div class="section-lg-top-34">
              <div class="inset-lg-right-20">
                <h1><?= Yii::t('app', 'О нас');?></h1>
                <p><?= Yii::t('app', 'Наша цель состоит в том, чтобы создать сообщество единомышленников , которые имеют желание помочь кому-то в их задачах и решить свои.');?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Extremely Responsive and Retina-->
    <section class="section-top-66 section-bottom-85 section-sm-top-110 section-lg-bottom-66 bg-blue-gray context-dark">
      <div class="shell">
        <div class="range range-xs-center">
          <div class="cell-sm-10 cell-lg-7 cell-lg-push-1"><img src="images/features/home-intro-02-730x380.jpg" width="730" height="380" alt="" data-wow-delay="0.6s" class="img-responsive veil reveal-sm-inline-block wow fadeInRight"></div>
          <div class="cell-sm-8 cell-lg-5 cell-lg-pull-1 offset-top-0 offset-sm-top-41 offset-lg-top-0 text-lg-left">
            <h1><?= Yii::t('app', 'О проекте');?></h1>
            <p><?= Yii::t('app', 'Наша платформа начинается с оказания помощи одному из участников, который собирает средства на какое-то свое дело. После того, как вы оказали помощь, вам смогут ее оказать другие.');?> </p>
          </div>
        </div>
      </div>
    </section>
    <section class="context-dark">
              <!-- RD Parallax-->
              <div data-on="false" data-md-on="true" class="rd-parallax">
                <div data-speed="0.35" data-type="media" data-url="images/backgrounds/background-33-1920x474.jpg" class="rd-parallax-layer"></div>
                <div data-speed="0" data-type="html" class="rd-parallax-layer">
                  <div class="bg-overlay-gray-darkest">
                    <div class="shell section-98 section-sm-110">
                      <h1><?= Yii::t('app', 'Неограниченные финансовые возможности');?></h1>
                      <div class="range range-xs-center offset-top-20">
                        <div class="cell-sm-10 cell-lg-12">
                          <p><?= Yii::t('app', 'Наша платформа - это способ реализации закона вселенной: помогая другим - помогаешь себе. Сделай добро и оно вернется к тебе стократно.');?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
    </section>
    <!-- Portfolio Templates-->
    <section class="section-top-66 section-bottom-85 section-lg-top-34 section-lg-bottom-254">
      <div class="shell">
        <div class="range range-xs-center">
          <div class="cell-sm-10 cell-lg-8 cell-lg-push-1 text-lg-right">
            <div style="margin-top: -65px;" class="mock-up-wrapper veil reveal-lg-inline-block">
              <div data-wow-duration="1s" data-wow-delay="0.2s" style="" class="wow fadeInRight"><img src="images/features/home-intro-24-420x292.jpg" width="420" height="292" alt="" class="img-responsive shadow-drop-lg"></div>
              <div data-wow-duration="1s" data-wow-delay="0.6s" style="position: absolute; top: 100px; left: -350px;" class="wow fadeInRight"><img src="images/features/home-intro-23-420x292.jpg" width="420" height="292" alt="" class="img-responsive shadow-drop-lg"></div>
              <div data-wow-duration="1s" data-wow-delay="1.2s" style="position: absolute; right: 150px; bottom: -222px; z-index: 1; width: 100%;" class="wow fadeInRight"><img src="images/features/home-intro-22-420x292.jpg" width="420" height="292" alt="" class="img-responsive shadow-drop-lg"></div>
            </div><img src="images/features/home-intro-10-820x570.png" width="820" height="570" alt="" data-wow-delay="0.6s" class="img-responsive veil reveal-sm-inline-block veil-lg wow fadeIn">
          </div>
          <div class="cell-sm-8 cell-lg-4 cell-lg-pull-1 offset-top-0 offset-sm-top-41 offset-lg-top-0 text-lg-left">
            <div class="section-lg-top-66">
              <div class="inset-lg-right-20">
                <h1><?= Yii::t('app', 'Дарите друг другу');?></h1>
                <p><?= Yii::t('app', 'Обменивайтесь подарками со всеми участниками по всему миру и помните, чем больше дарите Вы, тем больше подарят Вам!');?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Blog Templates-->
    <section class="section-top-66 section-bottom-85 section-lg-top-110 section-lg-bottom-50 context-dark bg-mantis">
      <div class="shell">
        <div class="range range-xs-center">
          <div class="cell-sm-10 cell-lg-8"><img src="images/features/home-intro-11-745x325.jpg" width="745" height="325" alt="" data-wow-delay="0.6s" class="img-responsive veil reveal-sm-inline-block wow fadeInLeft"></div>
          <div class="cell-sm-8 cell-lg-4 offset-top-0 offset-sm-top-41 offset-lg-top-0 text-lg-left">
            <div class="inset-lg-left-20">
              <h1><?= Yii::t('app', 'Без комиссий');?></h1>
              <p><?= Yii::t('app', 'Администрация не берет каких-либо комиссий с ваших подарков и никакой абонентской платы');?></p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- eCommerce Ready-->
    <section class="section-top-66 section-bottom-85 section-lg-top-34 section-lg-bottom-34">
      <div class="shell">
        <div class="range range-xs-center">
          <div class="cell-sm-8 cell-lg-7 cell-lg-push-1"><img src="images/features/home-intro-12-730x550.jpg" width="730" height="550" alt="" data-wow-delay="0.6s" style="position: relative; margin-top: -80px;" class="img-responsive veil reveal-lg-inline-block wow fadeInRight"><img src="images/features/home-intro-12-730x550.jpg" width="730" height="550" alt="" data-wow-delay="0.6s" class="img-responsive veil reveal-sm-inline-block veil-lg wow fadeIn"></div>
          <div class="cell-sm-8 cell-lg-5 cell-lg-pull-1 offset-top-0 offset-sm-top-41 offset-lg-top-0 text-lg-left">
            <div class="section-lg-top-66">
              <div class="inset-lg-right-50">
                <h1><?= Yii::t('app', 'Честный маркетинг');?></h1>
                <p><?= Yii::t('app', 'В нашей системе вы можете не переживать за сохранность ваших средств потому как:');?></p>
                <ul class="list list-marked">
                  <li><?= Yii::t('app', 'средства хранятся только на ваших кошельках');?></li>
                  <li><?= Yii::t('app', 'система не хранит ваших средств');?></li>
                  <li><?= Yii::t('app', 'дарите подарки только участникам системы');?></li>
                  <li><?= Yii::t('app', 'использование Bitcoin сохраняет вашу анонимность');?></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Event Managing-->
    <section class="section-top-66 section-bottom-85 section-lg-top-85 section-lg-bottom-0 bg-blue-gray context-dark">
      <div class="shell">
        <div class="range range-xs-center">
          <div class="cell-sm-8 cell-lg-8">
            <div style="overflow: hidden;"><img src="images/features/home-intro-13-730x500.jpg" width="730" height="500" alt="" data-wow-delay="0.6s" style="margin-bottom: -75px;" class="shadow-drop-lg img-responsive veil reveal-lg-inline-block wow fadeInUp"></div><img src="images/features/home-intro-13-730x500.jpg" width="730" height="500" alt="" data-wow-delay="0.6s" class="shadow-drop-lg img-responsive center-block veil reveal-sm-inline-block veil-lg wow fadeIn">
          </div>
          <div class="cell-sm-8 cell-lg-4 offset-top-0 offset-sm-top-41 offset-lg-top-0 text-lg-left">
            <div class="section-lg-top-34">
              <div class="inset-lg-left-20">
                <h1><?= Yii::t('app', 'Постоянный доход');?></h1>
                <p><?= Yii::t('app', 'Наша система позволит стать вашим постоянным источником дохода и даст возможность зарабатывать столько, сколько вы сами того пожелаете.');?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Powerful Search Engine-->
    <section style="box-shadow: 0px -5px 23px 0 rgba(0, 0, 0, 0.15);" class="section-top-124 section-bottom-85 section-sm-top-124 section-lg-top-34 section-lg-bottom-85">
      <div class="shell">
        <div class="range range-xs-center">
          <div class="cell-sm-10 cell-lg-7 cell-lg-push-1">
            <div style="overflow: hidden; z-index: 1; position: relative; bottom: -40px; margin-bottom: -80px;">
              <div class="mock-up-wrapper veil reveal-lg-inline-block">
                <div data-wow-duration="1s" data-wow-delay="0s" style="position: absolute; right: -125px; z-index: -1;" class="wow fadeInUpBig shadow-drop-lg"><img src="images/features/home-intro-17-354x243.jpg" width="354" height="243" alt="" class="img-responsive"></div>
                <div data-wow-duration="1s" data-wow-delay="0.4s" style="margin-top: 70px; margin-right: 40px;" class="wow fadeInUpBig shadow-drop-lg"><img src="images/features/home-intro-16-354x243.jpg" width="354" height="243" alt="" class="img-responsive"></div>
                <div data-wow-duration="1s" data-wow-delay="1.6s" style="left: -35%; bottom: 50px;" class="mock-up-circle wow zoomIn"><img src="images/storitsa-in-circle.png" width="245" height="230" alt="" class="img-responsive"></div>
              </div>
            </div><img src="images/features/home-intro-14-670x350.png" width="670" height="350" alt="" data-wow-delay="0.6s" class="img-responsive veil reveal-sm-inline-block veil-lg wow fadeIn">
          </div>
          <div class="cell-sm-8 cell-lg-5 cell-lg-pull-1 offset-top-0 offset-sm-top-41 offset-lg-top-0 text-lg-left">
            <div class="section-lg-top-66">
              <div class="inset-lg-right-20 inset-lg-left-20">
                <h1><?= Yii::t('app', 'Работай в любом месте');?></h1>
                <p><?= Yii::t('app', 'Работай в любой точке земного шара. Путешествуй, организуй свое рабочее место там, где тебе удобно. Наслаждайся жизнью вместе со STORYTSA!');?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Demo Images Included for Free-->
    <section class="context-dark">
              <!-- RD Parallax-->
              <div data-on="false" data-md-on="true" class="rd-parallax">
                <div data-speed="0.35" data-type="media" data-url="images/imgpsh_fullsize1.jpg" class="rd-parallax-layer"></div>
                <div data-speed="0" data-type="html" class="rd-parallax-layer">
                  <div class="bg-overlay-gray-darkest">
                    <div class="shell section-98 section-sm-110 section-lg-254">
                      <h1><?= Yii::t('app', 'Проект STORYTSA');?></h1>
                      <p><?= Yii::t('app', 'Помоги человеку и тебе это вернется сторицей.');?></p>
                    </div>
                  </div>
                </div>
              </div>
    </section>

    <!-- Dedicated 24/7 Support-->
    <section class="section-98 section-sm-110">
      <div class="shell">
        <div class="range range-xs-center">
          <div class="cell-sm-10 cell-md-8 cell-lg-6"><span class="icon icon-lg mdi mdi-headset"></span>
            <div class="offset-top-20">
              <h1><?= Yii::t('app', 'Техническая 24/7 Поддержка');?></h1>
              <p><?= Yii::t('app', 'Наша техническая служба доступна 24 часа в сутки, 7 дней в неделю для того, чтобы помочь вам построить ваш собственный бизнес.');?></p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Button Ansi-->
    <!-- Buy Now-->
    <section>
              <!-- Call to action type 2-->
              <section class="section-66 bg-mantis context-dark">
                <div class="shell">
                  <div class="range range-xs-middle range-condensed">
                    <div class="cell-md-8 cell-lg-9 text-center text-md-left">
                      <h2><span class="big"><?= Yii::t('app', 'Да! Это то, что мне нужно!');?></span></h2>
                    </div>
                    <div class="cell-md-4 cell-lg-3 offset-top-41 offset-md-top-0"><a href="<?php echo Url::to(['/signup']);?>" class="btn btn-icon btn-lg btn-default btn-anis-effect btn-icon-btn-icon-left"><span class="icon mdi mdi-rocket"></span><?= Yii::t('app', 'Присоединиться');?></a>
                    </div>
                  </div>
                </div>
              </section>
    </section>
</main>


