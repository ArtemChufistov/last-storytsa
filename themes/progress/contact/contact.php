<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\captcha\Captcha;
use yii\web\View;

$this->title = Yii::t('app', 'Связаться с нами');

$this->params['breadcrumbs']= [ [
    'label' => $this->title,
    'url' => '/contact'
  ],
]

?>

<main>
  <section class="map">
    <div id="google-map" class="map_model" data-zoom="17"></div>
    <ul class="map_locations">
      <li data-x="37.589327" data-y="55.733986" data-basic="images/gmap_marker.png" data-active="images/gmap_marker_active.png">
        <p> ул. Тимура Фрунзе, 11, к. 49, Москва, Россия, 119021<br /> <span></span></p>
      </li>
    </ul>
  </section>

  <section class="well2">
    <div class="container text-sm-center">
      <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12 ">
          <h2 class="">
            <?= Yii::t('app', 'Адрес');?>
          </h2>
          <div class="contact-info contact-info__mod">
            <address class="h4"><?= Yii::t('app', '119021, г. Москва,</br> ул. Тимура Фрунзе, 11, к. 49');?><br>
              <?= Yii::t('app', 'БЦ Красная Роза');?></address>
            <dl class="freephone">
              <dt><?= Yii::t('app', 'Телефон');?>: </dt>
              <dd><a href="callto:#">8 800 770 7456</a></dd>
            </dl>
            <dl class="telephone">
              <dt><?= Yii::t('app', 'Абон. отдел.');?>:</dt>
              <dd><a href="callto:#">+7 495 792 43 00</a></dd>
            </dl>

            <dl class="email">
              <dt><?= Yii::t('app', 'E-mail');?>: </dt>
              <dd><a href="mailto:#">Info@investprogress.net</a></dd>
            </dl>
          </div>
        </div>

        <?php if(Yii::$app->session->hasFlash('success')): ?>

            <div class="col-md-8 col-sm-12 col-xs-12 wow fadeInRight mg-add3" data-wow-duration='2s' style = "margin-bottom: 15px;">
              <h4>
                <?= Yii::t('app', 'Ваше сообщение принято');?>
              </h4>
              <p><?= Yii::t('app', 'Спасибо, что оставили нам сообщение, мы обязательно свяжемся с вам в ближайшее время');?></p>
            </div>

        <?php endif;?>

        <div class="col-md-8 col-sm-12 col-xs-12">

          <h2 class="text-xs-center">
            <?= Yii::t('app', 'Отправить сообщение');?>
          </h2>

          <?php $form = ActiveForm::begin(['options' => ['class' => 'rd-mailform']]); ?>
            <fieldset>
              <?= $form->field($model, 'name', ['template' => '<label style = "margin-top:10px;margin-bottom:5px;" data-add-placeholder class="mfInput">{input}</label>{error}'])
              ->input('text', ['placeholder' => Yii::t('app', 'Имя')])->label(false); ?>

              <?= $form->field($model, 'email', ['template' => '<label style = "margin-top:10px;margin-bottom:5px;" data-add-placeholder class="mfInput">{input}</label>{error}'])
              ->input('text', ['placeholder' => Yii::t('app', 'E-mail')])->label(false); ?>

              <?= $form->field($model, 'message', ['template' => '<label style = "margin-top:10px;margin-bottom:5px;" data-add-placeholder class="mfInput">{input}</label>{error}'])
              ->textArea(['placeholder' => Yii::t('app', 'Сообщение'), 'rows' => '6'])->label(false); ?>

              <?php echo $form->field($model, 'captcha', ['template' => '<label style = "margin-top:10px;margin-bottom:5px;" data-add-placeholder class="mfInput">{input}</label>{error}'])->widget(Captcha::className(), [
                  'captchaAction' => '/contact/contact/captcha',
                  'options' => [
                      'class' => 'form-control',
                      'placeholder' => $model->getAttributeLabel('captcha')
                  ],
                  'template' => '<div class="row">
                  <div class="col-lg-8">{input}</div>
                  <div class="col-lg-4">{image}</div>
                  </div>',
              ]);
              ?>

              <div class="mfControls">
                <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary btn-md']) ?>
              </div>
            </fieldset>
          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
  </section>
</main>