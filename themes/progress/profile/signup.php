<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use lowbase\user\components\AuthChoice;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;

$this->title = Yii::t('app', 'Регистрация');
$this->params['breadcrumbs'][] = $this->title;
UserAsset::register($this);

?>

<span class="icon icon-circle icon-bordered icon-lg icon-default mdi mdi-account-multiple-outline"></span>
<div>
  <div class="offset-top-24 text-darker big text-bold"><?= Html::encode($this->title) ?></div>
</div>

<?php echo $this->render('partial/_licenseAgreement');?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}"
    ],
    'options' => [
        'data-form-output' => 'form-output-global',
        'data-form-type' => 'contact',
        'class' => 'text-left offset-top-30'
    ]
]); ?>

    <div class="form-group-sign offset-top-30">
      <div class="input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-star-variant"></span></span>

        <?= $form->field($model, 'ref')->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('ref'),
            'id' => 'ref-user-name',
        ]);?>

      </div>
    </div>
    <div class="form-group-sign offset-top-30">
      <div class="input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-outline"></span></span>

        <?= $form->field($model, 'login')->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('login'),
            'id' => 'login-user-name',
        ]);?>

      </div>
    </div>
    <div class="form-group-sign offset-top-20">
      <div class="input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-email-outline"></span></span>

        <?= $form->field($model, 'email')->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('email')
        ]);?>

      </div>
    </div>
    <div class="form-group-sign offset-top-20">
      <div class="input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-lock-open-outline"></span></span>

        <?= $form->field($model, 'password')->passwordInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('password')
        ]); ?>

      </div>
    </div>
    <div class="form-group-sign offset-top-20">
      <div class="input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-lock-open-outline"></span></span>

        <?= $form->field($model, 'passwordRepeat')->passwordInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('passwordRepeat')
        ]); ?>

      </div>
    </div>
    <div class="form-group-sign offset-top-20">

        <?php
        echo $form->field($model, 'captcha')->widget(Captcha::className(), [
            'imageOptions' => [
                'id' => 'my-captcha-image',
            ],
            'captchaAction' => '/profile/profile/captcha',
            'options' => [
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('captcha')
            ],
            'template' => '<div class="row">
            <div class="col-lg-8">{input}</div>
            <div class="col-lg-4">{image}</div>
            </div>',
        ]);
        ?>

    </div>
    <div class="form-group-sign offset-top-24">

        <?php $labelLicense = '<span class="checkbox-custom-dummy"></span><span class="text-dark text-extra-small">' . Yii::t('app', 'Я согласен с условиями') . ' ' . Html::a(Yii::t('app', 'лицензионного соглашения'), ['#'], [
              'data-toggle' => 'modal',
              'data-target' => '#pass',
              'class' => 'text-picton-blue'
          ]). '</span>';?>

        <?= $form->field($model, 'licenseAgree', ['template' => '<label class="checkbox-inline">{input}{error}</label>', 'options'=> ['tag' => 'div']])->checkBox(['label' => $labelLicense, 'class' => 'checkbox-custom']);?>

    </div>

    <div class="form-group-sign offset-top-26">
        <?= Html::submitButton('<i class="glyphicon glyphicon-user"></i> '.Yii::t('app', 'Зарегистрироваться'), [
            'class' => 'btn btn-xs btn-icon btn-block btn-primary offset-top-20 offset-bottom-10',
            ]) ?>
    </div>

<?php ActiveForm::end(); ?>

<div class="offset-top-28">
    <span class="text-dark text-extra-small"><?= Yii::t('app', 'Зарегистрироваться с помощью социальных сетей')?>:</span>

    <div class="text-center" style="text-align: center">
        <?= AuthChoice::widget([
            'baseAuthUrl' => ['/lowbase-user/auth/index'],
        ]) ?>
    </div>

    <span class="text-dark text-extra-small">
        <?= Yii::t('app', 'Если регистрировались ранее, можете')?> <?=Html::a(Yii::t('app', 'войти на сайт'), ['/login'], ['class' => ['text-picton-blue']])?>,
        <?= Yii::t('app', 'используя Логин или E-mail')?>.
    </span>
</div>