<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

Modal::begin([
    'header' => '<h1 class="text-center">' . Yii::t('app', 'Лицензионное соглашение') . '</h1>',
    'toggleButton' => false,
    'id' => 'pass'
]);
?>

    <p class="hint-block">
        <?= Yii::t('app', 'Настоящие Условия предоставления услуг распространяются на исполняемый код Google Chrome. Исходный код Google Chrome предоставляется бесплатно на условиях лицензионных соглашений на программное обеспечение с открытым исходным кодом по адресу</br>
</br>
1. Взаимоотношения с компанией Google
</br>
1.1. Использование продуктов, программного обеспечения, служб и сайтов, принадлежащих Google (далее по тексту совместно именуемых "Услуги", за исключением услуг, предоставляемых компанией Google в рамках отдельного письменного соглашения), регулируется условиями юридического соглашения между Вами и Google. Под "Google" понимается корпорация Google Inc., главный офис которой находится по адресу 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States. В настоящем документе описываются основные положения соглашения, а также изложены некоторые условия данного соглашения.
</br>
1.2. Если с компанией Google не заключено иного письменного соглашения, то ваше соглашение с компанией Google всегда будет включать в себя по меньшей мере те условия использования, которые изложены в данном документе. Далее они именуются "Универсальные условия". Лицензии на программное обеспечение с открытым исходным кодом для исходного кода Google Chrome составляют отдельные письменные соглашения. В тех случаях, когда лицензии на программное обеспечение с открытым исходным кодом явным образом заменяют собой эти Универсальные условия, Ваше соглашение с Google на использование Google Chrome или отдельных компонентов Google Chrome регулируется лицензиями на программное обеспечение с открытым исходным кодом.
</br>
1.3. В Ваше соглашение с Google, помимо Универсальных условий, также включены условия, указанные в приведенных ниже Дополнительных условиях предоставления услуг Google Chrome, и условия всех юридических уведомлений, действующих в отношении Услуг. которые далее именуются "Дополнительные условия". В тех случаях, когда на Услугу распространяются Дополнительные условия, с ними можно будет ознакомиться в процессе использования данной Услуги либо прочитать в сопутствующей документации.
</br>
1.4. Универсальные условия и Дополнительные условия представляют собой юридически обязательное соглашение между Вами и компанией Google о пользовании Услугами. Эти документы необходимо внимательно прочитать. Совокупность этих юридических соглашений именуется далее "Условиями".
</br>
1.5. При наличии расхождений между Дополнительными условиями и Универсальными условиями, положения Дополнительных условий, относящиеся к соответствующей Услуге, будут иметь преимущественную силу.') ?>.
    </p>

<?php Modal::end();?>