<?php
use app\modules\profile\widgets\UserTreeElementWidget;
use app\modules\matrix\components\StorytsaMarketing;
use yii\widgets\ActiveForm;
use yii\web\View;

$child1 = $currentPlace->children()->orderBy(['sort' => 'asc'])->all()[0];
$child11 = !empty($child1) ? $child1->children()->orderBy(['sort' => 'asc'])->all()[0] : '';
$child12 = !empty($child1) ? $child1->children()->orderBy(['sort' => 'asc'])->all()[1] : '';
$child2 = $currentPlace->children()->orderBy(['sort' => 'asc'])->all()[1];
$child21 = !empty($child2) ? $child2->children()->orderBy(['sort' => 'asc'])->all()[0] : '';
$child22 = !empty($child2) ? $child2->children()->orderBy(['sort' => 'asc'])->all()[1] : '';

?>

<div class="box box-success">

	<?php $form = ActiveForm::begin([
	  'id' => 'form-profile',
	  'options' => [
	      'class'=>'form',
	      'enctype'=>'multipart/form-data'
	  ],
	  ]); ?>

	<div class="box-header with-border">
	  <h3 class="box-title"><i class="fa fa-users" aria-hidden="true"></i><?php echo Yii::t('app', 'Ваша структура');?></h3>
	</div>
	<div class="box-body">

	    <section class="management-hierarchy">

	        <div class="hv-container">

	            <div class="hv-wrapper">

	                <ul class="timeline">

	                	<?php foreach($currentPlace->path()->having('depth <=' . $currentLevel)->all() as $placeItem):?>
						<li>
							<a href = "/profile/office/matrix<?php echo !empty($startPlace) ? '/'. $startPlace->slug : '';?><?php echo '/' . $placeItem->slug;?>">
							    <img class="fa" src = "<?php echo $placeItem->getUser()->one()->getImage();?>">
							</a>
								<div class="timeline-item"></div>
						</li>
						<?php endforeach;?>
					</ul>

	                <div class="hv-item">
	                    <div class="hv-item-parent parent1">
	                    	<?php echo UserTreeElementWidget::widget(['currentPlace' => $currentPlace, 'startPlace' => $startPlace]);?>
	                    </div>

	                    <div class="hv-item-children">

	                        <div class="hv-item-child">
	                            <!-- Key component -->
	                            <div class="hv-item">

	                                <div class="hv-item-parent parent11">
	                                	<?php echo UserTreeElementWidget::widget(['currentPlace' => $child1, 'startPlace' => $startPlace]);?>
	                                </div>

	                                <div class="hv-item-children">
                               			<div class="hv-item-child child1">
	                                        <?php echo UserTreeElementWidget::widget(['currentPlace' => $child11, 'startPlace' => $startPlace]);?>
	                                    </div>

                                		<div class="hv-item-child child2">
		                                    <?php echo UserTreeElementWidget::widget(['currentPlace' => $child12, 'startPlace' => $startPlace]);?>
	                                    </div>

	                                </div>
	                            </div>
	                        </div>

	                        <div class="hv-item-child">
	                            <!-- Key component -->
	                            <div class="hv-item">

	                                <div class="hv-item-parent parent12">
										<?php echo UserTreeElementWidget::widget(['currentPlace' => $child2, 'startPlace' => $startPlace]);?>
	                                </div>

	                                <div class="hv-item-children">

	                                    <div class="hv-item-child child3">
	                                        <?php echo UserTreeElementWidget::widget(['currentPlace' => $child21, 'startPlace' => $startPlace]);?>
	                                    </div>


	                                    <div class="hv-item-child child4">
	                                        <?php echo UserTreeElementWidget::widget(['currentPlace' => $child22, 'startPlace' => $startPlace]);?>
	                                    </div>

	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>
</div>
<?php ActiveForm::end();?>