<?php
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\payment\models\Payment;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
?>
  <div class="modalPaymentBuyPlace modal">
  	<?php $form = ActiveForm::begin(); ?>
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">
          	<?php if ($payment->type == Payment::TYPE_BUY_PLACE):?>
          		<?php echo Yii::t('app', 'Вы подали заявку на приобретение места');?>
          	<?php elseif ($payment->type == Payment::TYPE_BUY_LEVEL):?>
          		<?php echo Yii::t('app', 'Вы подали заявку на покупку уровня № {level}', ['level' => $payment->additional_info]);?>
          	<?php endif;?>
          </h4>
        </div>
        <div class="modal-body">
          <div class = "row">
          	<div class = "col-sm-12 text-trancpancy">
	          <img style = "width: 200px; float: right;" src = "/profile/office/qrcode/<?php echo $payment->getToUserWallet()->wallet;?>/<?php echo $payment->sum;?>">
	          </br>
	          <?php echo Yii::t('app', 'Для подтверждения заявки и принятия участия в программе Вам необходимо перевести <strong>{countBtc}</strong> BTC на адрес: <strong>{wallet}</strong> и указать хэш транзакции ниже', [
	          		'countBtc' => $payment->sum,
	          		'wallet' => $payment->getToUserWallet()->wallet
	          ]);?>
	          
	          </br>
	          </br>
	          <?php echo Yii::t('app', 'Для более удобной оплаты вы можете сканировать QR код.');?>
          	</div>
          </div>

          <div class = "row">
          	<div class = "col-sm-12">
          		<p class="margin"><?php echo Yii::t('app', 'Реквизиты участника');?></p>
	          	<div class="input-group paymentButtonAsTable">
					<div class="input-group-btn">
						<button class="btn btn-success payment-wallet" type="button"><?php echo Yii::t('app', 'ФИО');?></button>
					</div>
					<input class="form-control" readonly="true" type="text" id = "payment-login" value = "<?php echo $payment->getToUser()->one()->first_name . ' ' . $payment->getToUser()->one()->last_name;?>">
				</div>
				</br>
	          	<div class="input-group paymentButtonAsTable">
					<div class="input-group-btn">
						<button class="btn btn-success payment-wallet" type="button"><?php echo Yii::t('app', 'Skype');?></button>
					</div>
					<input class="form-control" readonly="true" type="text" id = "payment-login" value = "<?php echo $payment->getToUser()->one()->skype;?>">
				</div>
				</br>
	          	<div class="input-group paymentButtonAsTable">
					<div class="input-group-btn">
						<button class="btn btn-success payment-wallet" type="button"><?php echo Yii::t('app', 'Телефон');?></button>
					</div>
					<input class="form-control" readonly="true" type="text" id = "payment-login" value = "<?php echo $payment->getToUser()->one()->phone;?>">
				</div>
				</br>
	          	<div class="input-group paymentButtonAsTable">
					<div class="input-group-btn">
						<button class="btn btn-success payment-wallet" type="button"><?php echo Yii::t('app', 'Почта');?></button>
					</div>
					<input class="form-control" readonly="true" type="text" id = "payment-login" value = "<?php echo $payment->getToUser()->one()->email;?>">
				</div>
				</br>
	          	<div class="input-group paymentButtonAsTable">
					<div class="input-group-btn">
						<button class="btn btn-success payment-wallet" type="button"><?php echo Yii::t('app', 'Логин участника');?></button>
					</div>
					<input class="form-control" readonly="true" type="text" id = "payment-login" value = "<?php echo $payment->getToUser()->one()->login;?>">
				</div>
				</br>
	          	<p class="margin"><?php echo Yii::t('app', 'Кошелек BitCoin');?></p>
	          	<div class="input-group">
					<div class="input-group-btn">
						<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-wallet"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
					</div>
					<input class="form-control" readonly="true" type="text" id = "payment-wallet" value = "<?php echo $payment->getToUserWallet()->wallet;?>">
				</div>
				</br>
				<p class="margin"><?php echo Yii::t('app', 'Сумма');?></p>
	          	<div class="input-group">
					<div class="input-group-btn">
						<button class="btn btn-success payment-sum" type="button" data-clipboard-target = "#payment-sum"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
					</div>
					<input class="form-control" id ="payment-sum" readonly="true" type="text" value = "<?php echo $payment->sum;?>">
				</div>
				</br>
				<?php if ($payment->status == Payment::STATUS_WAIT):?>
					<p class="margin"><?php echo Yii::t('app', 'Хэш транзакции');?></p>
						<?= $form->field($payment, 'transaction_hash', 
							['template' => '{error}<div class = "input-group"><div class="input-group-btn"><button class="btn btn-success" type="button">' . Yii::t('app', 'Вставьте сюда хэш транзакции') . '</button></div>{input}</div>'])->textInput([
				            'maxlength' => true,
				            'placeholder' => $payment->getAttributeLabel('transaction_hash')
				        ])->label(''); ?>
				<?php elseif ($payment->status == Payment::STATUS_WAIT_SYSTEM):?>
					<p class="margin"><?php echo Yii::t('app', 'Вы указали хэш транзакции: <strong>{transaction_hash}</strong>. Ожидайте подтверждения транзакции системой', 
					['transaction_hash' => $payment->transaction_hash]);?></p>
				<?php elseif ($payment->status == Payment::PROCESS_SYSTEM):?>
					<p class="margin"><?php echo Yii::t('app', 'Происходит поиск места в структуре, ожидайте');?></p>
				<?php elseif ($payment->status == Payment::STATUS_OK):?>
					<p class="margin"><?php echo Yii::t('app', 'Оплата обработана');?></p>
				<?php endif;?>
          	</div>
          </div>
        </div>
        <div class="modal-footer">

        	<?php if ($payment->status == Payment::STATUS_WAIT):?>
	            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Я совершил перевод'), [
	                'class' => 'btn btn-success pull-left',
	                'name' => 'signup-button']) ?>
            <?php endif;?>

          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
    <?php ActiveForm::end();?>
  </div>

<?php $this->registerJs('new Clipboard(".payment-sum");');?>
<?php $this->registerJs('new Clipboard(".payment-wallet");');?>
<?php $this->registerJs("
  $('.modalPaymentBuyPlace').modal('show');
", View::POS_END);?>