<?php
use yii\helpers\Url;
?>
<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
      <div class="box-header with-border">
      </div>
      <div class="box-body">
      	<?php echo Yii::t('app', 'Поздравляем вы успешно подтвердили свой E-mail');?>
      	</br>
      	</br>
      	<a href = "<?php echo Url::to('/profile/office/index');?>" style = "font-weight:bold; text-decoration: underline;"><?php echo Yii::t('app', 'Перейти в личный кабинет');?></a>
  	  </div>
  	</div>
  </div>
</div>