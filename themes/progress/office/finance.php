
<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\payment\models\Payment;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = Yii::t('user', 'Финансы');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<div class="row">
  <div class="col-md-9">
    <div class="box">

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $paymentProvider,
            'filterModel' => $payment,
            'id' => 'payment-grid',
            'layout' => '
                  <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i>' . Yii::t('app', 'История операций') . '</h3>
                    <div class="box-tools"><div class="pagination page-success pagination-sm no-margin pull-right">{pager}</div></div>
                  </div>
                 </div><div class="box-body no-padding eventTable">{items}</div>',
            'columns' => [['class' => 'yii\grid\SerialColumn'],
                [
                  'attribute' => 'type',
                  'format' => 'raw',
                  'filter' => false,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-type', ['model' => $model]);},
                ],[
                  'attribute' => 'sum',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-sum', ['model' => $model]);},
                ],[
                  'attribute' => 'status',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-status', ['model' => $model]);},
                  'filter' => Select2::widget([
                      'name' => 'PaymentFinanceSearch[status]',
                      'data' => Payment::getStatusArray(),
                      'theme' => Select2::THEME_BOOTSTRAP,
                      'hideSearch' => true,
                      'options' => [
                          'placeholder' => Yii::t('app', 'Выберите статус'),
                          'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                      ]
                  ]),
                ],[
                  'attribute' => 'wallet',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-wallet', ['model' => $model]);},
                ],[
                  'attribute' => 'transaction_hash',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-hash', ['model' => $model]);},
                ],[
                  'attribute' => 'date_add',
                  'format' => 'raw',
                  'filter' => true,
                  'filter' => \yii\jui\DatePicker::widget([
                      'model'=>$payment,
                      'attribute'=>'date_add',
                      'options' => ['class' => 'form-control'],
                      'language' => 'ru',
                      'dateFormat' => 'dd-MM-yyyy',
                  ]),
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-date-add', ['model' => $model]);},
                ]
            ],
        ]); ?>
        <?php Pjax::end(); ?>

    </div>

    <div class="col-md-3">

      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i><?php echo Yii::t('app', 'Сводная информация');?></h3>
        </div>
        <div class="box-body">

        </div>
      </div>
    </div>
</div>
