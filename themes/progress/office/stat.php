<?php
use app\modules\event\models\Event;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Статистика');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>
<div class = "row stat">
  <div class="col-lg-3">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo $user->descendants()->count();?></h3>

        <p><?php echo Yii::t('app', 'Зарегистрированных участников в структуре');?></p>
      </div>
      <div class="icon">
        <i class="ion ion-person"></i>
      </div>
      <a href="/profile/office/struct" class="small-box-footer">
        <?php echo Yii::t('app', 'Посмотреть структуру');?> <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>

    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo $user->getEvents()->where(['type' => Event::TYPE_REF_LINK])->count();?></h3>
        <p><?php echo Yii::t('app', 'Переходов по REF ссылке');?></p>
      </div>
      <div class="icon">
        <i class="ion ion-person-stalker"></i>
      </div>
      <a class="small-box-footer" href="#">
        <?php echo Yii::t('app', 'Информация по событиям');?>
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>

  </div>
  <div class = "col-lg-5">
      <div class="box box-success">

          <?php Pjax::begin(); ?>

          <?= GridView::widget([
              'dataProvider' => $eventProvider,
              'filterModel' => $eventModel,
              'layout' => '
                  <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i>' . Yii::t('app', 'События') . '</h3>
                    <div class="box-tools"><div class="pagination page-success pagination-sm no-margin pull-right">{pager}</div></div>
                  </div>
                 </div><div class="box-body no-padding eventTable">{items}</div>',
              'columns' => [
                  ['class' => 'yii\grid\SerialColumn'],
                  [
                      'attribute' => 'type',
                      'format' => 'html',
                      'filter' => false,
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-type', ['model' => $model]);},
                  ],[
                      'attribute' => 'to_user_id',
                      'format' => 'html',
                      'label' => Yii::t('app', 'Участник'),
                      'filter' => false,
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_to_user_id', ['model' => $model]);},
                  ],[
                      'attribute' => 'date_add',
                      'format' => 'html',
                      'filter' => false,
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-date', ['model' => $model]);},
                  ],
              ],
          ]); ?>
          <?php Pjax::end(); ?>
      </div>
  </div>
</div>