<div class="col-md-2 col-sm-6 col-xs-6">
  <h3><?php echo \Yii::t('app', 'Меню');?></h3>

  <ul class="footer-menu">
    <?php foreach($menu->children($menu->depth + 1)->all() as $num => $child):?>
      <li class="<?php if($requestUrl == $children->link):?>active<?php endif;?>"><a href="<?= $child->link;?>"><?= $child->title;?></a></li>
    <?php endforeach;?>
  </ul>

</div>
