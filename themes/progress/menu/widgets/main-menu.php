<div id="stuck_container" class="stuck_container">
  <div class="container-fluid">
    <div class="brand">
      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_logo_light_1');?>
    </div>

    <nav class="nav">
      <ul class="sf-menu" data-type="navbar">
        <?php foreach($menu->children()->all() as $num => $children):?>
          <li class="<?php if($requestUrl == $children->link):?>active<?php endif;?>" ><a href="<?= $children->link;?>"><span><?= $children->title;?></span></a>
            <?php if (!empty($children->children()->all())):?>
              <ul>
                <?php foreach($children->children()->all() as $numChild => $childChildren):?>
                  <li>
                    <a href="<?= $childChildren->link;?>"><?= $childChildren->title;?></a>
                    <!-- <a href="<?= $childChildren->link;?>" class="sub-menu"><?= $childChildren->title;?></a>
                    <ul>
                      <li><a href="#"><?= $childChildren->title;?></a></li>
                    </ul> -->
                  </li>
                <?php endforeach;?>
              </ul>
            <?php endif;?>
          </li>
        <?php endforeach;?>
      </ul>
    </nav>

  </div>
</div>
