<?php
use yii\helpers\Url;
use yii\helpers\Html;
use lowbase\document\DocumentAsset;
use app\modules\profile\models\User;
use app\modules\payment\models\Payment;

$this->title = "Search Results";
?>

<section id="content" class="content">
    <div class="container ">
        <h4>Search Results</h4>
        <div id="search-results"></div>
    </div>
</section>
