<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'О Нас',
    'url' => '/about'
  ],
];
$this->title = $model->title;
?>
  <main>

    <section class="well2">
      <div class="container">
        <h2>
          О НАС
        </h2>
        <div class="row row_ins1 wow fadeInLeft" data-wow-duration='2s'>

          <div class="col-md-5 col-sm-6 col-xs-6">
            <img src="/progress/about1.png" alt="" class="mg-add2 img-full">
          </div>
          <div class="col-md-7 col-sm-6 col-xs-6">
            <h5 class="clr-primary" style = "font-size: 18px; ">
                Совершенно новый финансовый продукт!
            </h5>
            <p>
              С 2015 года в Российской Федерации вступили поправки в Федеральный Закон о Микро Финансовой Деятельности согласно которым размер прямых инвестиций от частных лиц и организаций в Микрофинансовые Организации не может быть менее <strong>1500000</strong> рублей.
            </p>
            <p>
              Данный закон был пролоббирован так как отток средств с банковских депозитов в направлении микрофинансовых организаций уже принимал угрожающие размеры и мог повлиять на целостность и стабильность банковской системы.
            </p>
          </div>

        </div>

        <div class="row row_ins1 wow fadeInRight" data-wow-duration='2s'>
          <div class="col-md-12 col-sm-12 col-xs-12" style = "text-align: center;">
            <i style = "font-style: italic;">
            Не для кого не является секретом тот факт, что доходность предлагаемая Микро Финансовыми Организациями своим инвесторам в разы превышает доходность традиционных банковских депозитов и нередко доходит до 100% и даже 200% годовых, так как Микро Финансовые организации имеют возможность более свободно распоряжаться имеющимся капиталом и получать гораздо большую прибыль за меньшие временные отрезки.
            </i>
          </div>

        </div>

        <div class="row wow fadeInRight" data-wow-duration='2s'>
          <div class="col-md-7 col-sm-7 col-xs-7">
            <strong>В 2016 году компания «Прогресс» представила совершенно новый финансовый продукт</strong>
            <h5 class="clr-primary" style = "font-size: 18px;  margin-top: 15px;">
                «Программа безубыточного инвестирования»
            </h5>
            <p>
              Продукт призван упростить доступ свободного капитала организаций и физических лиц на рынок Микро Финансовых Организаций.
            </p>
            <p>
              В основе программы лежат принципы высокой стабильности и гарантированной безубыточности абсолютно для каждого инвестора благодаря взвешенной политике диверсификации инвестиционного портфеля среди множества МФО. 
            </p>
          </div>

          <div class="col-md-5 col-md-release col-sm-5 col-sm-clear col-xs-5 col-xs-clear">
            <img src="progress/about2.png" alt="" class="mg-add2 img-full">
          </div>

        </div>

      </div>
    </section>

    <section class="well2 brdr-t">
      <div class="container">
        <h2>
          Наша команда
        </h2>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay=".3s">
            <div class="thumbnail-1">
              <img class="" src="images/page-2_img2.jpg" alt="">
              <div class="caption">
                <h4>
                  <a href="#">
                    Борис Данилов
                  </a>
                </h4>
                <p>
                  Генеральный директор по развитию компании
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay=".6s">
            <div class="thumbnail-1">
              <img class="" src="images/page-2_img3.jpg" alt="">
              <div class="caption">
                <h4>
                  <a href="#">
                    Виталий Соболев
                  </a>
                </h4>
                <p>
                  Главный директор по маркетингу и связям с общественностью
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-md-release col-sm-6 col-sm-clear col-xs-12 wow fadeInUp" data-wow-delay=".9s">
            <div class="thumbnail-1">
              <img class="" src="images/page-2_img4.jpg" alt="">
              <div class="caption">
                <h4>
                  <a href="#">
                    Марина Лобова
                  </a>
                </h4>
                <p>
                  Начальник отделения бухгалтерского учета и аудита
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="1.2s">
            <div class="thumbnail-1">
              <img class="" src="images/page-2_img5.jpg" alt="">
              <div class="caption">
                <h4>
                  <a href="#">
                    Алексей Мельников
                  </a>
                </h4>
                <p>
                  Региональный директор по развитию регионов. Руководитель отдела международных связей с общественностью
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="well3 parallax" data-url="images/parallax2-1.jpg" data-mobile="true">
      <div class="container text-center">
        <div class="h1 clr-white italic">

            Залог успешного руководства — видеть многовариантность будущего, <br>
            умение предусматривать самые мрачные сценарии.

        </div>
      </div>
    </section>
<?php /*
    <section class="well2">
      <div class="container wow fadeInLeft" data-wow-duration='3s'>
        <h2>
          Что мы предлагаем
        </h2>
        <div class="row">
          <ul class="index-list">
            <li class="col-md-4 col-sm-6 col-xs-6">
              <h5>
                <a href="#">
                  Вашу финаносвую независимость
                </a>
              </h5>
              <p>
                Хватит работать на деньги, пусть деньги работают на вас, это ведь так просто - получать деньги и наслаждаться жизнью!
              </p>
            </li>
            <li class="col-md-4 col-sm-6 col-xs-6">
              <h5>
                <a href="#">
                  Вашу финаносвую независимость
                </a>
              </h5>
              <p>
                Хватит работать на деньги, пусть деньги работают на вас, это ведь так просто - получать деньги и наслаждаться жизнью!
              </p>
            </li>
            <li class="col-md-4 col-md-release col-sm-6 col-sm-clear col-xs-6 col-xs-clear">
              <h5>
                <a href="#">
                  Вашу финаносвую независимость
                </a>
              </h5>
              <p>
                Хватит работать на деньги, пусть деньги работают на вас, это ведь так просто - получать деньги и наслаждаться жизнью!
              </p>
            </li>
            <li class="col-md-4 col-md-clear col-sm-6 col-xs-6">
              <h5>
                <a href="#">
                  Вашу финаносвую независимость
                </a>
              </h5>
              <p>
                Хватит работать на деньги, пусть деньги работают на вас, это ведь так просто - получать деньги и наслаждаться жизнью!
              </p>
            </li>
            <li class="col-md-4 col-md-release col-sm-6 col-sm-clear col-xs-6 col-sm-clear">
              <h5>
                <a href="#">
                  Вашу финаносвую независимость
                </a>
              </h5>
              <p>
                Хватит работать на деньги, пусть деньги работают на вас, это ведь так просто - получать деньги и наслаждаться жизнью!
              </p>
            </li>
            <li class="col-md-4 col-sm-6 col-xs-6">
              <h5>
                <a href="#">
                  Вашу финаносвую независимость
                </a>
              </h5>
              <p>
                Хватит работать на деньги, пусть деньги работают на вас, это ведь так просто - получать деньги и наслаждаться жизнью!
              </p>
            </li>
          </ul>
        </div>
      </div>
    </section>
*/?>
<?php /*
    <section class="well2 bg1">
      <div class="container wow fadeInRight" data-wow-duration='3s'>
        <h2>
          Наши преимущества
        </h2>
        <div class="row">
          <ul class="service-list">

            <li class="col-md-3 col-sm-6 col-xs-6">
              <h5>
                <a href="#">
                  График в красном кружочке
                </a>
              </h5>
              <p>
                Наше самое главное преимущество, которое неоспоримо перед всеми, именно поэтому мы являемся лучшими в том, что мы делаем.
                Наши миссия открыть людям новые грани жизни, в которых нет необходимости бесконечно работать.
              </p>
              <p>
                Мы просто хотим показать, что бывает по другому, бывает получаешь многа бабла и кайфуешь от этого
              </p>
            </li>
            <li class="col-md-3 col-sm-6 col-xs-6">
              <h5>
                <a href="#">
                  Кейс в красном кружочке
                </a>
              </h5>
              <p>
                Наше самое главное преимущество, которое неоспоримо перед всеми, именно поэтому мы являемся лучшими в том, что мы делаем.
                Наши миссия открыть людям новые грани жизни, в которых нет необходимости бесконечно работать.
              </p>
              <p>
                Мы просто хотим показать, что бывает по другому, бывает получаешь многа бабла и кайфуешь от этого
              </p>
            </li>
            <li class="col-md-3 col-md-release col-sm-6 col-sm-clear col-xs-6 col-xs-clear">
              <h5>
                <a href="#">
                  Календарь в красном кружочке
                </a>
              </h5>
              <p>
                Наше самое главное преимущество, которое неоспоримо перед всеми, именно поэтому мы являемся лучшими в том, что мы делаем.
                Наши миссия открыть людям новые грани жизни, в которых нет необходимости бесконечно работать.
              </p>
              <p>
                Мы просто хотим показать, что бывает по другому, бывает получаешь многа бабла и кайфуешь от этого
              </p>
            </li>
            <li class="col-md-3 col-sm-6 col-xs-6">
              <h5>
                <a href="#">
                  Письмо в красном кружочке
                </a>
              </h5>
              <p>
                Наше самое главное преимущество, которое неоспоримо перед всеми, именно поэтому мы являемся лучшими в том, что мы делаем.
                Наши миссия открыть людям новые грани жизни, в которых нет необходимости бесконечно работать.
              </p>
              <p>
                Мы просто хотим показать, что бывает по другому, бывает получаешь многа бабла и кайфуешь от этого
              </p>
            </li>

          </ul>
        </div>
      </div>
    </section>
*/?>
<?php /*
    <section class="well3 parallax" data-url="images/parallax2-2.jpg" data-mobile="true">
      <div class="container text-center">
        <div class="h1 clr-white italic">

            Партнёрство ведёт к успеху<br>
            Только так мы можем добиться желаемого!

        </div>
      </div>
    </section>
*/?>
</main>