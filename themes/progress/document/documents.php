<?php
use yii\helpers\Url;
use yii\helpers\Html;
use lowbase\document\DocumentAsset;
use app\modules\profile\models\User;
use app\modules\payment\models\Payment;

$this->title = $model->title;
?>

<section class="well2 text-sm-center">
  <div class="container">
    <h2 style = "text-align: center;">
      Документы
    </h2>

<style>

.fadeInUp a img{
	margin: 10px;
	border: 1px solid #b2b2b2;
	height: 400px;
	overflow: hidden;
-webkit-box-shadow: 2px 2px 13px 0px rgba(0,0,0,0.3);
-moz-box-shadow: 2px 2px 13px 0px rgba(0,0,0,0.3);
box-shadow: 2px 2px 13px 0px rgba(0,0,0,0.3);
}
</style>

    <div class="row">
      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/1.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/1.jpg" alt="" />
		</a>
      </div>

      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/2.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/2.jpg" alt="" />
		</a>
      </div>

      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/3.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/3.jpg" alt="" />
		</a>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/4.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/4.jpg" alt="" />
		</a>
      </div>

      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/5.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/5.jpg" alt="" />
		</a>
      </div>

      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/6.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/6.jpg" alt="" />
		</a>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/7.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/7.jpg" alt="" />
		</a>
      </div>

      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/8.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/8.jpg" alt="" />
		</a>
      </div>

      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/9.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/9.jpg" alt="" />
		</a>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/10.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/10.jpg" alt="" />
		</a>
      </div>

      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/11.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/11.jpg" alt="" />
		</a>
      </div>

      <div class="col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <a href="/progress/documents/12.jpg" data-fancybox="group" data-caption="Документы">
			<img src="/progress/documents/12.jpg" alt="" />
		</a>
      </div>
    </div>

  </div>
</section>