<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'Faq',
    'url' => '/faq'
  ],
];
$this->title = $model->title;
?>

</header>
<!-- Classic Breadcrumbs-->
<section class="breadcrumb-classic">
  <div class="shell section-34 section-sm-50">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-help-circle icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h2><span class="big">Faq</span></h2>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">

          <?= Breadcrumbs::widget([
            'options' => ['class' => 'list-inline list-inline-dashed p'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>

      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
  </svg>
</section>
<!-- Page Content-->
<main class="page-content">
  <!-- Faq variant 4-->
  <section class="section-66 section-sm-top-110 section-lg-bottom-0">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-sm-9 cell-lg-6">
          <h1 class="text-darker text-lg-left"><?= Yii::t('app', 'Основные вопросы');?></h1>
          <hr class="divider bg-mantis hr-lg-left-0">
          <div class="offset-top-41 offset-lg-top-66 text-left">
                    <!-- Bootstrap Accordion-->
                    <div role="tablist" aria-multiselectable="true" id="accordion-1" class="panel-group accordion offset-top-0">
                      <div class="panel panel-default">
                        <div role="tab" id="headingOne" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Как зарегистрироваться в проекте?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingOne" id="collapseOne" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'В разработке');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingTwo" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Как принять участие в проекте?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingTwo" id="collapseTwo" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'В разработке');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingThree" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Сколько я могу получить подарков?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingThree" id="collapseThree" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'В разработке');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Как подтвердить свой E-mail');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseFour" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'В разработке');?>
                          </div>
                        </div>
                      </div>
                    </div>
          </div>
        </div>
        <div class="cell-lg-6 offset-top-0"><img src="images/pages/faq-01-531x699.png" width="531" height="699" alt="" class="veil reveal-lg-inline-block"></div>
      </div>
    </div>
  </section>
</main>
