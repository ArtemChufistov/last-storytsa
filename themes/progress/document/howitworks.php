<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
		'label' => 'Как это работает',
		'url' => '/howitworks'
	],
];

$this->title = $model->title;
?>

<style>
@font-face {
    font-family: Merriweather; /* Имя шрифта */
    src: url(progress/Merriweather-Italic.ttf); /* Путь к файлу со шрифтом */
   }
i{
  font-family: Merriweather;
}
.thumbnail-2 h3{
  font-family: Merriweather;
}
.caption div{
  text-align: center;
  margin-top: 20px;: 
}
.jusrtifyText{
  text-align: justify;
}
.thumbnail-2{
  border: 1px solid #dedede;
}
.thumb-hat1{
  background-color: #f44236;
}
.thumb-hat2{
  background-color: #32c5d2;
}
.thumb-hat3{
  background-color: #4baf4f;
}
.thumb-hat4{
  background-color: #febd00;
}
.thumbnail-2 h3{
  color: white;
  text-align: center;
  padding-top: 20px;
}
.thumb-hat1 img, .thumb-hat2 img, .thumb-hat3 img, .thumb-hat4 img{
  margin: 0 auto;
  margin-top: 17px;
  display: block;
}
.thumb-hat1, .thumb-hat2, .thumb-hat3, .thumb-hat4{
  width: 100%;
  height: 140px;
}
.bigStrong{
  font-size: 28px;
}
.caption div{
  line-height: 0.4;
}
.thumbnail-2 .caption{
  margin-top: 0px;
}
.thumbPtrc1{
  color: #f44236;
}
.backPtrc1::before{
  background-color: #f44236;
}
.backPtrc1{
  border: 1px solid #f44236;
}
.backPtrc1:hover{
  color: #f44236;
}
.thumbPtrc2{
  color: #32c5d2;
}
.backPtrc2::before{
  background-color: #32c5d2;
}
.backPtrc2{
  border: 1px solid #32c5d2;
}
.backPtrc2:hover{
  color: #32c5d2;
}
.thumbPtrc3{
  color: #4baf4f;
}
.backPtrc3::before{
  background-color: #4baf4f;
}
.backPtrc3:hover{
  color: #4baf4f;
}
.backPtrc3{
  border: 1px solid #4baf4f;
}
.thumbPtrc4{
  color: #febd00;
}
.backPtrc4{
  border: 1px solid #febd00;
}
.backPtrc4:hover{
  color: #febd00;
}
.backPtrc4::before{
  background-color: #febd00;
}
.backPtrc1, .backPtrc2, .backPtrc3, .backPtrc4{
  margin-bottom: 40px;
}
.smallLine{
  background-color: #dedede;
  width: 70%;
  height: 1px;
  margin: 0 auto;
  margin-top: 30px !important;
}
.caption{
  padding: 20px;
}
</style>

  <main>
    <section class="well2">
      <div class="container">
        <h2>
          инвестиционные планы
        </h2>
        <div class="row">

          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="thumbnail-2">
              <div class = "thumb-hat1">
                <h3><span style = "font-size: 12px;">«</span>Старт<span style = "font-size: 12px;">»</span></h3>
                <img src = "/progress/money1.png">
              </div>
              <div class="caption">
                <div>
                  <span>%</span>
                  <strong class = "bigStrong">0,38</strong>
                </div>
                <div  style="line-height: 1;">
                  Ежедневная процентная ставка
                </div>
                <div class = "thumbPtrc1" style="margin-top:33px;">
                  <span>%</span>
                  <strong class = "bigStrong">11,4</strong>
                </div>
                <div class = "thumbPtrc1" style="line-height: 1;">
                  Ежемесячная ставка
                </div>
                <div class = "smallLine">
                </div>
                <div style="margin-top:32px; line-height: 1.2;">
                  Период начисления</br>
                  <strong>ежедневно</strong>
                </div>
                <div style="margin-top:32px; line-height: 1.2;">
                  Срок действия депозита</br>
                  <strong>360 дней</strong>
                </div>
                <div>
                  <a href="#" class="btn btn-secondary3 backPtrc1"><span>Оформить</span></a> 
                </div>
              </div>

            </div>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="thumbnail-2">
              <div class = "thumb-hat2">
                <h3><span style = "font-size: 12px;">«</span>Лояльный<span style = "font-size: 12px;">»</span></h3>
                <img src = "/progress/money2.png">
              </div>
              <div class="caption">
                <div>
                  <span>%</span>
                  <strong class = "bigStrong">0,47</strong>
                </div>
                <div  style="line-height: 1;">
                  Ежедневная процентная ставка
                </div>
                <div class = "thumbPtrc2" style="margin-top:33px;">
                  <span>%</span>
                  <strong class = "bigStrong">14,1</strong>
                </div>
                <div class = "thumbPtrc2" style="line-height: 1;">
                  Ежемесячная ставка
                </div>
                <div class = "smallLine">
                </div>
                <div style="margin-top:32px; line-height: 1.2;">
                  Период начисления</br>
                  <strong>каждые 15 дней</strong>
                </div>
                <div style="margin-top:32px; line-height: 1.2;">
                  Срок действия депозита</br>
                  <strong>360 дней</strong>
                </div>
                <div>
                  <a href="#" class="btn btn-secondary3 backPtrc2"><span>Оформить</span></a> 
                </div>
              </div>

            </div>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="thumbnail-2">
              <div class = "thumb-hat3">
                <h3><span style = "font-size: 12px;">«</span>Оптимальный<span style = "font-size: 12px;">»</span></h3>
                <img src = "/progress/money3.png">
              </div>
              <div class="caption">
                <div>
                  <span>%</span>
                  <strong class = "bigStrong">0,65</strong>
                </div>
                <div  style="line-height: 1;">
                  Ежедневная процентная ставка
                </div>
                <div class = "thumbPtrc3" style="margin-top:33px;">
                  <span>%</span>
                  <strong class = "bigStrong">19,5</strong>
                </div>
                <div class = "thumbPtrc3" style="line-height: 1;">
                  Ежемесячная ставка
                </div>
                <div class = "smallLine">
                </div>
                <div style="margin-top:32px; line-height: 1.2;">
                  Период начисления</br>
                  <strong>каждые 30 дней</strong>
                </div>
                <div style="margin-top:32px; line-height: 1.2;">
                  Срок действия депозита</br>
                  <strong>360 дней</strong>
                </div>
                <div>
                  <a href="#" class="btn btn-secondary3 backPtrc3"><span>Оформить</span></a> 
                </div>
              </div>

            </div>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="thumbnail-2">
              <div class = "thumb-hat4">
                <h3><span style = "font-size: 12px;">«</span>Премиум<span style = "font-size: 12px;">»</span></h3>
                <img src = "/progress/money4.png">
              </div>
              <div class="caption">
                <div>
                  <span>%</span>
                  <strong class = "bigStrong">0,95</strong>
                </div>
                <div  style="line-height: 1;">
                  Ежедневная процентная ставка
                </div>
                <div class = "thumbPtrc4" style="margin-top:33px;">
                  <span>%</span>
                  <strong class = "bigStrong">28,5</strong>
                </div>
                <div class = "thumbPtrc4" style="line-height: 1;">
                  Ежемесячная ставка
                </div>
                <div class = "smallLine">
                </div>
                <div style="margin-top:32px; line-height: 1.2;">
                  Период начисления</br>
                  <strong>в конце срока</strong>
                </div>
                <div style="margin-top:32px; line-height: 1.2;">
                  Срок действия депозита</br>
                  <strong>360 дней</strong>
                </div>
                <div>
                  <a href="#" class="btn btn-secondary3 backPtrc4"><span>Оформить</span></a> 
                </div>
              </div>

            </div>
          </div>

        </div>
      </div>
    </section>

    <section class="well5 brdr-t" style="background-color:#f0f0f0;">
      <div class="container">
        <h2 id="sistema_voznnagrazhdenii">
          СИСТЕМА ВОЗНАГРАЖДЕНИЙ
        </h2>
        <div class="row row_ins1 wow fadeInLeft" data-wow-duration='2s'>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <h5 class="clr-primary" style = "font-size: 18px; ">
                Агентская программа
            </h5>
            <p>
              В рамках развития «Программы безубыточного инвестирования Прогресс» компания приглашает к <strong>сотрудничеству</strong> —
            </p>
            <table style = "margin-top: 20px;">
              <tr>
                <td rowspan="2">
                  <img src="/progress/plane-icon1.png" style="margin-right: 15px;">
                </td>
                <td rowspan="2" >
                  <i style="font-size: 16px;font-weight: bold; font-style: italic; ">Специалистов в сфере финансового</br> консультирования и инвестиций</i>
                </td>
                <td>
                  <img src="/progress/plane-logo2.png" style="margin-left: 15px; margin-right: 18px;">
                </td>
                <td >
                  <h5 class="clr-primary" style="font-size: 14px; font-weight: normal; white-space: nowrap;">УДАЛЁННАЯ РАБОТА</h5>
                </td>
              </tr>
              <tr>
                <td>
                  <img src="/progress/plane-logo3.png" style="margin-left: 15px;margin-right: 18px;">
                </td>
                <td>
                  <h5 class="clr-primary" style="font-size: 14px; font-weight: normal;">КОМИССИЯ 4%</h5>
                </td>
              </tr>
            </table>
            <p>
              Возможна <strong>удалённая работа</strong> на основании Агентского Договора.
            </p>
            <p>
              Неограниченный доход формируется за счёт комиссионных в размере 4% от каждого заключенного контракта Вашим клиентом по «Программе безубыточного инвестирования Прогресс».
            </p>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <img src="/progress/progresshow2.png" alt="" class="mg-add2 img-full" style = "margin-top: 37px;">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="smallLine" style = "margin-bottom: 0px !important; width: 100%; margin-top: 0px !important; margin-bottom: 30px;"> </div>
          </div>
        </div>

        <div class="row row_ins1 wow fadeInRight" data-wow-duration='2s'>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <h5 class="clr-primary" style = "font-size: 18px; color: #f44236;">
                Партнёрская программа
            </h5>
            <p>
              <i style = "font-weight: bold; font-style: italic; color: #f44236;">Десятиуровневый маркетинг</i> согласно которому на каждом уровне клиентской сети агенту начисляются <strong>бонусные баллы</strong> от прибыли получаемой клиентами
            </p>
            <table style = "margin-top: 20px;">
              <tr>
                <td rowspan="2">
                  <img src="/progress/handshake.png" style="margin-right: 15px;">
                </td>
                <td rowspan="2">
                  <i style="font-size: 16px;font-weight: bold; font-style: italic; ">Количество баллов </br> вычисляется по формуле</i>
                </td>
                <td>
                  <span  class="btn btn-secondary3 backPtrc1" style = "margin-left: 30px; cursor: inherit;"><span>Х*0,03=В</span></span> 
                </td>
              </tr>
            </table>
            <table>
              <tr>
                <td width="250px;">
                  <strong>X</strong> - прибыль, начисленная клиенту</br>
                  <strong>0,03</strong> - коэффициент расчёта</br>
                  <strong>В</strong> - количество начисляемых баллов</br>
                  <strong>1 балл = 1 руб.</strong></br>
                </td>
                <td>
                  <div style = "margin-left: 10px;">
                    Баллы доступны к выплате или покупке нового инвестиционного пакета в любой момент любым удобным для агента способом. Активация уровней партнерской программы производится в автоматическом режиме по достижении требуемого минимального общего объёма привлеченных средств в уже открытых уровнях.
                  </div>
                </td>
              </tr>
            </table>
          </div>

        <style>
        .table-striped tr:nth-child(odd) {
           background-color: #dedede;
        }
        .table-striped tr td{
          font-size: 10px;
          color: black;
          padding: 3px;
        }
        .centerRedText{
          text-align: center;
          color: #f44236 !important;
          font-size: 13px !important;
          font-weight: bold;
        }
        .redText{
          font-size: 13px !important;
          color: #f44236 !important;
        }
        .voidSpace{
          background-color: #f0f0f0 !important;
          width: 2px !important;
        }
        .table-striped tr:hover{
          background-color: #d0d0d0;
        }
        </style>

          <div class="col-md-6 col-sm-6 col-xs-6">
            <table class="table table-striped" style = "margin-top:37px;">
                <tbody>
                  <tr>
                    <td style="background-color: #f44236; color: white; padding-top: 6px; padding-bottom: 6px; padding-left: 6px; padding-right: 6px; font-size: 14px; ">Уровень
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td style="width: 260px;">Объем привлечённых средств для активации уровня</br>
                    (учитывается вся сумма уже открытых уровней)
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td>
                      Коэффициент расчёта начисления баллов
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td style = "width: 110px;">
                      Баллы за активацию нового уровня
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">1</td>
                    <td class="voidSpace">
                    </td>
                    <td>100 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      1 000
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">2</td>
                    <td class="voidSpace">
                    </td>
                    <td>500 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      5 000
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">3</td>
                    <td class="voidSpace">
                    </td>
                    <td>1 000 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      10 000
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">4</td>
                    <td class="voidSpace">
                    </td>
                    <td>5 000 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      50 000
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">5</td>
                    <td class="voidSpace">
                    </td>
                    <td>10 000 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      100 000
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">6</td>
                    <td class="voidSpace">
                    </td>
                    <td>25 000 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      250 000
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">7</td>
                    <td class="voidSpace">
                    </td>
                    <td>50 000 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      500 000
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">8</td>
                    <td class="voidSpace">
                    </td>
                    <td>100 000 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      1 000 000
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">9</td>
                    <td class="voidSpace">
                    </td>
                    <td>250 000 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      2 500 000
                    </td>
                  </tr>
                  <tr>
                    <td class = "centerRedText">10</td>
                    <td class="voidSpace">
                    </td>
                    <td>500 000 000 руб.</td>
                    <td class="voidSpace">
                    </td>
                    <td>0,03
                    </td>
                    <td class="voidSpace">
                    </td>
                    <td class = "redText">
                      5 000 000
                    </td>
                  </tr>
                </tbody>
              </table>
          </div>
        </div>

      </div>
    </section>

    <section class="well3 parallax wow fadeIn" data-wow-duration='3s' data-url="images/parallax3-1.jpg" data-mobile="true">
      <div class="container text-center">
        <div class="h1 clr-white italic">
            Мы обучаем профессионалов<br>
            Сделайте шаг к успешной карьере! 
        </div>
      </div>
    </section>


    <section class="well2">
      <div class="container">
        <h2 id = "how_it_work">
          как это работает
        </h2>
        <div class="row row_ins1 wow fadeInLeft" data-wow-duration='2s'>

          <div class="col-md-6 col-sm-6 col-xs-6">
            <img src="/progress/howit1.png" alt="" class="mg-add2 img-full">

          </div>

          <div class="col-md-6 col-md-release col-sm-6 col-sm-clear col-xs-6 col-xs-clear">
            <img src="/progress/howit2.png" alt="" class="mg-add2 img-full">
          </div>

        </div>
        <div class="row wow fadeInRight" data-wow-duration='2s'>

          <div class="col-md-6 col-sm-6 col-xs-6">
            <img src="/progress/howit3.png" alt="" class="mg-add2 img-full">
          </div>

          <div class="col-md-6 col-md-release col-sm-6 col-sm-clear col-xs-6 col-xs-clear">
            <img src="/progress/howit4.png" alt="" class="mg-add2 img-full">
          </div>

        </div>

      </div>
    </section>

    <section class="well3 parallax" data-url="images/parallax3-2.jpg" data-mobile="true">
      <div class="container text-center">
        <div class="h1 italic clr-white">
            Мы знаем, сколько стоит <br>
            безопасность Ваших инвестиций!
        </div>
      </div>
    </section>
  </main>