<?php
use yii\helpers\Url;
use yii\helpers\Html;
use lowbase\document\DocumentAsset;
use app\modules\profile\models\User;
use app\modules\payment\models\Payment;
use app\modules\news\widgets\LastNewsWidget;

$this->title = $model->title;
?>

<section class="camera_container">
  <div id="camera" class="camera_wrap">
    <div data-src="images/slide1.jpg">
      <div class="camera_caption fadeIn">
        <div>
          <em>
            Высокий
          </em>
          <p>
            уровень прфессионализма <br>
             нашей команды...
             </br>
          </p>
        </div>
      </div>
    </div>
    <div data-src="images/slide2.jpg">
      <div class="camera_caption fadeIn">
        <div>
          <em>
            Вместе
                      </em>
          <p>
          создадим и реализуем <br>
          стратегию вашего развития!
          </br>
          </p>
        </div>
      </div>
    </div>
    <div data-src="images/slide3.jpg">
      <div class="camera_caption fadeIn">
        <div>
          <em>
            Дайте нам
          </em>
          <p>
            шанс и мы докажем <br>
            нашу эффективность!
            </br>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="well2 text-sm-center">
  <div class="container">
    <h2>
      Добро пожаловать!
    </h2>
    <div class="row">

      <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay=".3s">
        <div class="thumbnail">
          <div class="img-thumbnail">
            <img src="images/page-1_img1.jpg" alt="">
            <div class="bg-secondary">
              <a href="/howitworks#sistema_voznnagrazhdenii"><span>Карьерный рост>></span></a>
            </div>
          </div>
          <div class="caption">
            <h4>
              Выгодная система вознаграждений для каждого
            </h4>
            <p>
              Поиск потенциальных партнеров и  сопровождение их в сотрудничестве с компанией «ПРОГРЕСС», позволит зарабатывать каждому желающему солидныые бонусы. Узнайте подробнее об агентской программе, а так же многоуровневой партнерской программе.
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay=".6s">
        <div class="thumbnail">
          <div class="img-thumbnail">
            <img src="images/page-1_img2.jpg" alt="">
            <div class="bg-primary">
              <a href="/howitworks"><span>Выгодные инвестиции >></span></a>
            </div>
          </div>
          <div class="caption">
            <h4>
              Высокодоходные варианты размещения депозитов
            </h4>
            <p>
              Компания «ПРОГРЕСС» разработала и внедрила выгодные условия сотрудничества для микроинвесторов. Реализованы ряд инвестиционных планов с широким спектром процентных ставок по депозитам. Каждый для себя сможет выбрать удобный план сотрудничества.
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <div class="thumbnail">
          <div class="img-thumbnail">
            <img src="images/page-1_img3.jpg" alt="">
            <div class="bg-secondary3">
              <a href="/howitworks#how_it_work"><span>Минимальные риски >></span></a>
            </div>
          </div>
          <div class="caption">
            <h4>
              Высокая степень защиты ваших инвестиций
            </h4>
            <p>
              Безубыточная программа инвестирования компании «ПРОГРЕСС» выражается в абсолютно прозрачных действиях на всем пути совершения сделки, между участниками (партнерами) компании и МФО. Документооборот во всех процессах полностью соответствует законодательным нормам РФ.
            </p>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

<section class="well well2 brdr-t">
  <div class="container">
    <h2>
      как это работает
    </h2>
    <div class="row">

      <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-duration='2s'>
        <div class="resp-tabs">
          <ul class="resp-tabs-list">
            <li id="resident-button">Для резидентов</li>
            <li id="nonresident-button">Для нерезидентов</li>
          </ul>
          <div class="resp-tabs-container">
            <div>
              <div id="for-resident" class="media">
                <div class="media-body" style="position: relative; height: 300px">
                  <div class="progress-investments-bigicon">
                    <div class="bigicon-container"><img src="progress/logo_only.png" alt=""></div>
                    <img src="images/progress-investments-text.png" alt="">
                  </div>
                  <div class="human-bigicon">
                    <div class="bigicon-container active"><img src="images/resident.png" alt=""></div>
                    <em style="color: #32c5d2; font-size: 16px; margin-top: 10px">Резидент РФ</em>
                  </div>
                  <div class="mfo-bigicon">
                    <div class="bigicon-container"><img src="images/mfo.png" alt=""></div>
                    <em style="color: #32c5d2; font-size: 16px; margin-top: 10px">Рынок МФО</em>
                  </div>

                  <!-- Arrows -->

                  <div class="top-left-arrow active">
                  </div>
                  <div class="top-right-arrow">
                  </div>
                  <div class="bottom-center-arrow">
                  </div>

                 <!-- Comments -->

                  <div style="position: absolute; top: 78px; left: 10px; font-size: 12px; width: 150px;">
                    <img src="images/partnership-icon.png" style="display: block; float: left; width: 30px">
                    <span class="comment-text"><a href="#">Договор партнёрства</a></span>
                  </div>

                  <div id="corporation-comment" style="position: absolute; top: 78px; left: 370px; font-size: 12px; width: 140px;">
                    <img src="images/corporation-icon.png" style="display: block; float: left; width: 24px">
                    <span style="display: block; float: right; width: 110px">Корпоративный договор</span>
                    <button class="popup-open-button"></button>
                  </div>

                  <div id="payment-comment" style="position: absolute; top: 190px; left: 175px; font-size: 12px; width: 150px">
                    <img src="images/payment-icon.png" style="display: block; float: left; width: 30px">
                    <span style="display: block; float: right; width: 110px">Перевод средств в адрес МФО</span>
                    <button class="popup-open-button"></button>
                  </div>

                  <!-- Pop-ups -->

                  <div id="corporation-popup" style="display: none; position: absolute; top: 20px; left: 340px; font-size: 10px; width: 160px;">
                    <button class="popup-close-button"></button>
                    <span style="display: block; float: right; width: 130px">Перевод средств от имени клиента на основании договора партнёрства и договора на уступку права требования платежа по корпоративному договору</span>
                  </div>

                  <div id="payment-popup" style="display: none; position: absolute; top: 180px; left: 180px; font-size: 10px; width: 160px">
                    <button class="popup-close-button"></button>
                    <span style="display: block; float: right; width: 130px">Возврат средств клиенту напрямую в рублях или транзитом через swift прогресс</span>
                  </div>
                </div>
              </div>
            </div>

            <div>
              <div id="for-nonresident" class="media">
                <div class="media-body" style="position: relative; height: 300px">
                  <div class="progress-investments-bigicon">
                    <div class="bigicon-container"><img src="progress/logo_only.png" alt=""></div>
                    <img src="images/progress-investments-text.png" alt="">
                  </div>
                  <div class="human-bigicon">
                    <div class="bigicon-container active"><img src="images/resident.png" alt=""></div>
                    <em style="color: #32c5d2; font-size: 16px; margin-top: 10px">Нерезидент РФ</em>
                  </div>
                  <div class="mfo-bigicon">
                    <div class="bigicon-container"><img src="images/mfo.png" alt=""></div>
                    <em style="color: #32c5d2; font-size: 16px; margin-top: 10px">Рынок МФО</em>
                  </div>

                  <!-- Arrows -->

                  <div class="top-left-arrow active">
                  </div>
                  <div class="top-right-arrow">
                  </div>
                  <div class="bottom-center-arrow">
                  </div>
                  
                  <!-- Comments -->

                  <div id="wallet-comment" style="position: absolute; top: 78px; left: 10px; font-size: 12px; width: 150px;">
                    <img src="images/wallet-icon.png" style="display: block; float: left; width: 30px">
                    <span style="display: block; float: right; width: 110px">Клиент пополняет внутренний счёт</span>
                    <button class="popup-open-button"></button>
                  </div>

                  <div id="transfer-comment" style="position: absolute; top: 78px; left: 370px; font-size: 12px; width: 140px;">
                    <img src="images/transfer-icon.png" style="display: block; float: left; width: 30px">
                    <span style="display: block; float: right; width: 100px">Перевод средств от имени клиента</span>
                    <button class="popup-open-button"></button>
                  </div>

                  <div id="ruble-comment" style="position: absolute; top: 190px; left: 185px; font-size: 12px; width: 140px">
                    <img src="images/ruble-icon.png" style="display: block; float: left; width: 30px">
                    <span style="display: block; float: right; width: 100px">Возврат средств клиенту напрямую</span>
                    <button class="popup-open-button"></button>
                  </div>

                  <!-- Pop-ups -->

                  <div id="wallet-popup" style="display: none; position: absolute; top: 30px; left: 5px; font-size: 10px; width: 160px;">
                    <button class="popup-close-button"></button>
                    <span style="display: block; float: right; width: 130px">Клиент пополняет внутренний счёт Прогресс используя swift или электронный платёж на основании оферты</span>
                  </div>

                  <div id="transfer-popup" style="display: none; position: absolute; top: 20px; left: 340px; font-size: 10px; width: 160px;">
                    <button class="popup-close-button"></button>
                    <span style="display: block; float: right; width: 130px">Перевод средств от имени клиента на основании договора партнёрства и договора на уступку права требования платежа по корпоративному договору</span>
                  </div>

                  <div id="ruble-popup" style="display: none; position: absolute; top: 180px; left: 180px; font-size: 10px; width: 160px">
                    <button class="popup-close-button"></button>
                    <span style="display: block; float: right; width: 130px">Возврат средств клиенту напрямую в рублях или транзитом через swift прогресс</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay=".6s">
        <div id="resident-block" class="accordion">
          <h5 class="accordion_header first">
            Компания “ПРОГРЕСС” заключает Договор Инвестиционного Займа
          </h5>
          <div class="accordion_cnt">
            с абсолютно любой микрофинансовой организацией согласно которому займодавец обязуется внести за весь срок действия договора сумму превышающую минимальный порог установленный действующим законодательством. (Заключение контракта с МФО).
          </div>

          <h5 class="accordion_header second">
            На основании заключенного с МФО Договора Инвестиционного Займа компания “Прогресс” имеет право заключить “Договор о совместной деятельности”
          </h5>
          <div class="accordion_cnt">
            с любым физическим или юридическим лицом, согласно которому производится уступка права получения прибыли и средств полученных в виде дохода по договору инвестиционного займа с МФО. (Заключение договора о совместной деятельности с любым физическим или юридическим лицом).
          </div>

          <h5 class="accordion_header third">
            На основании “Договора о совместной деятельности” с физическим или юридическим лицом заключается “Договор обеспечения обязательств под уступку денежного требования”
          </h5>
          <div class="accordion_cnt">
            согласно которому производится  перевод средств на расчетный счет МФО с которой у компании “ПРОГРЕСС” заключен Договор Инвестиционного Займа.
          </div>
        </div>

        <div id="nonresident-block" class="accordion" style="display: none">
          <h5 class="accordion_header first">
            Компания “ПРОГРЕСС” заключает Договор Инвестиционного Займа 2
          </h5>
          <div class="accordion_cnt">
            с абсолютно любой микрофинансовой организацией согласно которому займодавец обязуется внести за весь срок действия договора сумму превышающую минимальный порог установленный действующим законодательством. (Заключение контракта с МФО).
          </div>

          <h5 class="accordion_header second">
            На основании заключенного с МФО Договора Инвестиционного Займа компания “Прогресс” имеет право заключить “Договор о совместной деятельности” 2
          </h5>
          <div class="accordion_cnt">
            с любым физическим или юридическим лицом, согласно которому производится уступка права получения прибыли и средств полученных в виде дохода по договору инвестиционного займа с МФО. (Заключение договора о совместной деятельности с любым физическим или юридическим лицом).
          </div>

          <h5 class="accordion_header third">
            На основании “Договора о совместной деятельности” с физическим или юридическим лицом заключается “Договор обеспечения обязательств под уступку денежного требования” 2
          </h5>
          <div class="accordion_cnt">
            согласно которому производится  перевод средств на расчетный счет МФО с которой у компании “ПРОГРЕСС” заключен Договор Инвестиционного Займа.
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

<section class="well5 bg1">
  <div class="container">
    <div class="row">

      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="icon-box">
          <div class="icon-box_aside bg-secondary">
            <img src="images/page-1_icn1.png" alt="">
          </div>
          <div class="icon-box_cnt icon-box_cnt__no-flow">
            <h5 class="clr-secondary">
              <span>
                Диверсификация портфеля
              </span>
            </h5>
            <p>
              Наша компания сотрудничает с рядом Микро Финансовых Организаций для исключения финансовых рисков.
            </p>
          </div>
        </div>
        <div class="icon-box">
          <div class="icon-box_aside bg-secondary">
            <img src="images/page-1_icn2.png" alt="">
          </div>
          <div class="icon-box_cnt icon-box_cnt__no-flow">
            <h5 class="clr-secondary">
              <span>
                Увеличение прибыли
              </span>
            </h5>
            <p>
              Возможность увеличить свою прибыль, используя сразу несколько источников дохода.
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="icon-box">
          <div class="icon-box_aside bg-primary">
            <img src="images/page-1_icn3.png" alt="">
          </div>
          <div class="icon-box_cnt icon-box_cnt__no-flow">
            <h5 class="clr-primary">
              <span>
                Отсутствие волатильности
              </span>
            </h5>
            <p>
              Все операции происходят в национальной валюте РФ, что исключает риски, связанные с волатильностью.
            </p>
          </div>
        </div>
        <div class="icon-box">
          <div class="icon-box_aside bg-primary">
            <img src="images/page-1_icn4.png" alt="">
          </div>
          <div class="icon-box_cnt icon-box_cnt__no-flow">
            <h5 class="clr-primary">
              <span>
                Учет и аудит
              </span>
            </h5>
            <p>
              Отдел бухгалтерского учета компании ПРОГРЕСС, ответственно контролирует весь процесс участия в проекте.
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-md-release col-sm-6 col-sm-clear col-xs-6">
        <div class="icon-box">
          <div class="icon-box_aside bg-secondary2">
            <img src="images/page-1_icn5.png" alt="">
          </div>
          <div class="icon-box_cnt icon-box_cnt__no-flow">
            <h5 class="clr-secondary2">
              <span>
                Выход в безубыточность
              </span>
            </h5>
            <p>
              Разнообразие тарифных планов дают возможность выйти в окупаемость уже через 5 месяцев.
            </p>
          </div>
        </div>
        <div class="icon-box">
          <div class="icon-box_aside bg-secondary2">
            <img src="images/page-1_icn6.png" alt="">
          </div>
          <div class="icon-box_cnt icon-box_cnt__no-flow">
            <h5 class="clr-secondary2">
              <span>
                Надежное сотрудничество
              </span>
            </h5>
            <p>
              Наши профессионалы позаботятся обо всех сложных процессах. Вам остается только наслаждаться прибылью.
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="icon-box">
          <div class="icon-box_aside bg-secondary3">
            <img src="images/page-1_icn7.png" alt="">
          </div>
          <div class="icon-box_cnt icon-box_cnt__no-flow">
            <h5 class="clr-secondary3">
              <span>
                Договорные отношения
              </span>
            </h5>
            <p>
              Все партнерские отношения с компанией ПРОГРЕСС закреплены официальными договорами.
            </p>
          </div>
        </div>
        <div class="icon-box">
          <div class="icon-box_aside bg-secondary3">
            <img src="images/page-1_icn8.png" alt="">
          </div>
          <div class="icon-box_cnt icon-box_cnt__no-flow">
            <h5 class="clr-secondary3">
              <span>
                Ваша прибыль
              </span>
            </h5>
            <p>
              Доход в области микро финансирования не ограничен. Все зависит только от вашего желания и решения.
            </p>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

<section class="well well2_out brdr-t">
  <div class="container">
    <div class="row">

      <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-duration='2s'>
        <h2 class="text-default">
          Калькулятор расчета заработка
        </h2>
        <p>
          Используя наш калькулятор, вы можете с легкостью рассчитать свою прибыль в компании ПРОГРЕСС. Применяйте различные настройки калькулятора при выборе тарифного плана, суммы инвестиций, а также учитывайте срок начисления прибыли.
                </p>
        <p>
          Минимальная сумма инвестиций составляет 5000 рублей. Срок инвестиционного договора составляет 360 дней.
        </p>

<style>
.inline-list li span{
  font-size: 12px;
}
.inline-list li a{
  margin-top: 10px;
    padding: 10px 12px;
}
.inline-list{
  display: table;
  width: 92.5%;
}
@media (max-width: 1199px) {
  .inline-list{
    display: table;
    width: 100%;
  }
}
.inline-list li{
  display: table-cell;
}
.inline-list li h6{
  margin-bottom: 5px;
}
</style>

        <div class="progress-wrap" data-speed="2500">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <br>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h6>Выберите тарифный план</h6>
              <ul class="inline-list">
                <li><a id="start-tariff" href="#" class="btn btn-gray"><span>Старт / 0,38%</span></a></li>
                <li><a id="loyal-tariff" href="#" class="btn btn-gray"><span>Лояльный / 0,47%</span></a></li>
                <li><a id="optimal-tariff" href="#" class="btn btn-gray"><span>Оптимальный / 0,65%</span></a></li>
                <li><a id="progress-tariff" href="#" class="btn btn-gray"><span>Прогресс / 0,95%</span></a></li>
              </ul>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <br>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="inline-list">
                <li><h6 >Сумма взноса в руб</h6><input style = "font-size:16px;" name="summ" class = "calcsum" type="text" value="20000"></li>
                <li><h6 >Срок инвестиций</h6><input style = "font-size:16px;" type="text" value="360 дней" disabled="true"></li>
                <li><h6 >Период начисления</h6><input style = "font-size:16px;" name="period" type="text" value="ежедневно" disabled="true"></li>
              </ul>
              <em id="summ-error" style="display: none; color: red">Введите сумму от 5 000 до 1 000 000 рублей</em>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <br>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h6>В день</h6>
              <div class="progress">
                <div id="daily-bar" class="progress-bar progress-bar__secondary" data-progress-percent="38%">
                  <span class="progress-count" data-progress-count="0"></span>
                </div>
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <br>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h6>В месяц</h6>
              <div class="progress">
                <div id="monthly-bar" class="progress-bar progress-bar__secondary2" data-progress-percent="53%">
                  <span class="progress-count" data-progress-count="0"></span>
                </div>
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <br>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h6>За весь срок</h6>
              <div class="progress">
                <div id="annually-bar" class="progress-bar progress-bar__secondary3" data-progress-percent="68%">
                  <span class="progress-count" data-progress-count="0"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay=".6s">
        <img src="images/calculator-bg.png" alt="" style="margin-top: 200px">
      </div>

    </div>
  </div>
</section>

<section class="well3 parallax" data-url="images/parallax1.jpg" data-mobile="true">
  <div class="container text-center">
    <div class="h1 italic clr-white">
        Мы работаем на Ваш успех <br> Мы знаем, что делать!
    </div>
  </div>
</section>

<?php echo LastNewsWidget::widget(['count' => 4, 'view' => 'main-menu']);?>

<?php /*
<section class="well2 bg1 wow fadeIn" data-wow-duration='2s'>
  <div class="container text-es-center">
    <h2>
      О нас
    </h2>
    <div class="row">

      <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".3s">
        <p>
          В 2016 году наша компания представила совершенно новый финансовый продукт, “Программа безубыточного инвестирования ПРОГРЕСС”, призванный упростить доступ свободного капитала организаций и физических лиц на рынок Микро Финансовых Организаций.
        </p>
        <p>
          В основе данного продукта лежат принципы высокой стабильности и гарантированной безубыточности абсолютно для каждого инвестора благодаря взвешанной политике диверсификации инвестиционного портфеля среди множества МФО.
        </p>
        <a href="#" class="btn btn-primary btn_offs"><span>Узнать больше >></span></a>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".6s">
        <ul class="marked-list">
          <li>
            <a href="#">
              Lorem ipsum dolor sit amet
            </a>
          </li>
          <li>
            <a href="#">
              Constetuer adipiscing esent
            </a>
          </li>
          <li>
            <a href="#">
              Vestibulum molestie lacus eno
            </a>
          </li>
          <li>
            <a href="#">
              Nummy hendrerit mauris
            </a>
          </li>
          <li>
            <a href="#">
              Phasellus port usce suscipit
            </a>
          </li>
          <li>
            <a href="#">
              Cum sociis natoque penatibu
            </a>
          </li>
          <li>
            <a href="#">
              Magnis dis parturient
            </a>
          </li>
        </ul>
      </div>

      <div class="col-md-3 col-md-release col-sm-6 col-sm-clear col-xs-6 col-xs-clear wow fadeInUp"
           data-wow-delay=".9s">
        <ul class="marked-list">
          <li>
            <a href="#">
              Lorem ipsum dolor sit amet
            </a>
          </li>
          <li>
            <a href="#">
              Constetuer adipiscing esent
            </a>
          </li>
          <li>
            <a href="#">
              Vestibulum molestie lacus eno
            </a>
          </li>
          <li>
            <a href="#">
              Nummy hendrerit mauris
            </a>
          </li>
          <li>
            <a href="#">
              Phasellus port usce suscipit
            </a>
          </li>
          <li>
            <a href="#">
              Cum sociis natoque penatibu
            </a>
          </li>
          <li>
            <a href="#">
              Magnis dis parturient
            </a>
          </li>
        </ul>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay="1.2s">
        <ul class="marked-list">
          <li>
            <a href="#">
              Lorem ipsum dolor sit amet
            </a>
          </li>
          <li>
            <a href="#">
              Constetuer adipiscing esent
            </a>
          </li>
          <li>
            <a href="#">
              Vestibulum molestie lacus eno
            </a>
          </li>
          <li>
            <a href="#">
              Nummy hendrerit mauris
            </a>
          </li>
          <li>
            <a href="#">
              Phasellus port usce suscipit
            </a>
          </li>
          <li>
            <a href="#">
              Cum sociis natoque penatibu
            </a>
          </li>
          <li>
            <a href="#">
              Magnis dis parturient
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
*/?>
<section class="well4 parallax" data-url="images/parallax2.jpg" data-mobile="true">
  <div class="container count-wrap">
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-6 text-center">
        <div class="counter-box">
          <span class="fa fa-building"></span>
                        <span class="count" data-from="0" data-to="2"
                              data-speed="3000" data-refresh-interval="100">2</span>
          <hr>
          <span class="counter-box_heading">Партнерских МФО</span>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 text-center">
        <div class="counter-box">
          <span class="fa fa-users"></span>
                        <span class="count" data-from="0" data-to="416" data-speed="3000"
                              data-refresh-interval="100">416</span>
          <hr>
          <span class="counter-box_heading">Наших партнеров</span>
        </div>
      </div>
      <div class="col-md-3 col-md-release col-sm-6 col-sm-clear col-xs-6 col-xs-clear text-center">
        <div class="counter-box">
          <span class="fa fa-paper-plane"></span>
                        <span class="count" data-from="0" data-to="602"
                              data-speed="3000" data-refresh-interval="50">602</span>
          <hr>
          <span class="counter-box_heading">Открытых депозитов</span>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 text-center">
        <div class="counter-box">
          <span class="fa fa-thumbs-o-up"></span>
                        <span class="count" data-from="0" data-to="38"
                              data-speed="3000" data-refresh-interval="100">38</span>
          <hr>
          <span class="counter-box_heading">Счастливых отзывов</span>
        </div>
      </div>
    </div>
  </div>
</section>

<?php /*
<section class="well2 brdr-t">
  <div class="container">
    <div class="row">

      <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="progress-wrap" data-speed="2500">

          <h6>Lorem ipsum dolor</h6>

          <div class="progress">
            <div class="progress-bar progress-bar__secondary3" data-progress-percent="43%">
              <span class="progress-count" data-progress-count="43"></span>
            </div>
          </div>

          <h6>consectetuer adipiscing</h6>

          <div class="progress">
            <div class="progress-bar progress-bar__primary" data-progress-percent="78%">
              <span class="progress-count" data-progress-count="78"></span>
            </div>
          </div>

          <h6>Praesent vestibulum</h6>

          <div class="progress">
            <div class="progress-bar progress-bar__secondary" data-progress-percent="75%">
              <span class="progress-count" data-progress-count="75"></span>
            </div>
          </div>

          <h6>onsectetuer adipis</h6>

          <div class="progress">
            <div class="progress-bar progress-bar__secondary2" data-progress-percent="80%">
              <span class="progress-count" data-progress-count="80"></span>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
        <h2 class="text-default">
          Перспективы развития компании на 2017-18 годы.
        </h2>
        <p>
          Основываясь на многолетнем опыте ведения успешного бизнеса крупными компаниями в сфере продаж страховых и финансовых продуктов мы пришли к выводу что самое эффективное стимулирование продаж это создание условий при которых доход агентов формируется не только от личных продаж новым клиентам но и от построения многоуровневой сети партнеров обеспечивающей постоянно ростущий стабильный доход. В январе 2017 года на ежегодной бизнес-встречи учредителей компании была утверждена  десяти уровневая партнерская программа.
        </p>
        <p>
                  </p>
        <a href="#" class="btn btn-primary"><span>Узнать больше >></span></a>
      </div>
    </div>
  </div>
</section>
*/?>

<section class="well2 brdr-t">
  <div class="container">
    <h2>
      Публикации
    </h2>
    <div class="row row_ins1">

      <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay=".3s">
        <h4 class="bold clr-secondary3">
          Поправки в законодательстве РФ
        </h4>
        <p>
          С 2015 года в Российской Федерации вступили поправки в Федеральный Закон о Микро Финансовой Деятельности согласно которым размер прямых инвестиций от частных лиц и организаций в Микрофинансовые Организации не может быть менее 1500000 рублей. Не для кого не является секретом тот факт что доходность предлагаемая Микро Финансовыми Организациями своим инвесторам в разы превышает доходность традиционных банковских депозитов и нередко доходит до 100% и даже 200% годовых.
        </p>
        <?php /* <a href="#" class="btn btn-secondary3"><span>Подробнее >></span></a> */?>
      </div>

      <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay=".6s">
        <h4 class="bold clr-secondary3">
          Сфера МФО в России
        </h4>
        <p>
          Несмотря на то, что численность МФО в России значительно упала из-за проводимых Центробанком мероприятий по обеспечению надежности работы этого сектора, количество российских микрофинансовых организаций до сих пор является самым большим в мире.
          На сегодняшний момент в РФ насчитывается 4182 МФО. И это при том, что за 2014 год с данного рынка ушло 1300 подобного рода компаний.
        </p>
        <?php /* <a href="#" class="btn btn-secondary3"><span>Подробнее >></span></a> */?>
      </div>

      <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay=".9s">
        <h4 class="bold clr-secondary3">
          Развитие МФО в РФ
        </h4>
        <p>
          Развитие финансовой сферы в России идет стремительными шагами. Всего в России действует более 4000 микрофинансовых организаций, и их позиции на рынке только укрепляются, поскольку Центробанк активно борется со злоупотреблениями в этой сфере. В результате развиваются только те организации, которые работают в соответствии с требованиями законодательства и соблюдают интересы клиентов.        </p>
        <?php /* <a href="#" class="btn btn-secondary3"><span>Подробнее >></span></a> */?>
      </div>
    </div>
  </div>
</section>

<section class="well3 parallax" data-url="images/parallax3.jpg" data-mobile="true">
  <div class="container text-center">
    <div class="h1 clr-white italic">
        Развивайтесь и добивайтесь успеха вместе с Нами!
    </div>
  </div>
</section>
<?php /*
<section class="well2">
  <div class="container text-center wow fadeInUp" data-wow-delay=".6s">
    <h2 class="text-left">
      отзывы наших клиетов
    </h2>
    <div class="row">

      <div class="col-md-4 col-sm-6 col-xs-6">
        <a class="thumb" href="images/page-1_img9_original.jpg">
          <img  src="images/page-1_img9.jpg" alt="">
          <span class="thumb_overlay"></span>
        </a>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-6">
        <a class="thumb" href="images/page-1_img10_original.jpg">
          <img  src="images/page-1_img10.jpg" alt="">
          <span class="thumb_overlay"></span>
        </a>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-6">
        <a class="thumb" href="images/page-1_img11_original.jpg">
          <img  src="images/page-1_img11.jpg" alt="">
          <span class="thumb_overlay"></span>
        </a>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-6">
        <a class="thumb" href="images/page-1_img12_original.jpg">
          <img  src="images/page-1_img12.jpg" alt="">
          <span class="thumb_overlay"></span>
        </a>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-6">
        <a class="thumb" href="images/page-1_img13_original.jpg">
          <img  src="images/page-1_img13.jpg" alt="">
          <span class="thumb_overlay"></span>
        </a>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-6">
        <a class="thumb" href="images/page-1_img14_original.jpg">
          <img  src="images/page-1_img14.jpg" alt="">
          <span class="thumb_overlay"></span>
        </a>
      </div>

    </div>
    <a href="#" class="btn btn-primary btn-md"><span>View all >></span></a>
  </div>
</section>
*/?>

<?php /*
<section class="well5 bg1 wow fadeIn" data-wow-duration='3s'>
  <div class="container text-center">

    <ul class="flex-list">
      <li>
        <a href="#">
          <img src="images/page-1_img15.png" alt="">
        </a>
      </li>
      <li>
        <a href="#">
          <img src="images/page-1_img16.png" alt="">
        </a>
      </li>
      <li>
        <a href="#">
          <img src="images/page-1_img17.png" alt="">
        </a>
      </li>
      <li>
        <a href="#">
          <img src="images/page-1_img18.png" alt="">
        </a>
      </li>
      <li>
        <a href="#">
          <img src="images/page-1_img19.png" alt="">
        </a>
      </li>
      <li>
        <a href="#">
          <img src="images/page-1_img20.png" alt="">
        </a>
      </li>
    </ul>

    <ul class="flex-list flex-list__mod">
      <li>
        <a href="#">
          <img src="images/page-1_img21.png" alt="">
        </a>
      </li>
      <li>
        <a href="#">
          <img src="images/page-1_img22.png" alt="">
        </a>
      </li>
      <li>
        <a href="#">
          <img src="images/page-1_img23.png" alt="">
        </a>
      </li>
      <li>
        <a href="#">
          <img src="images/page-1_img24.png" alt="">
        </a>
      </li>
    </ul>
  </div>
</section>
*/?>
