<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
		'label' => $message,
		'url' => '/404'
	],
]
?>

</header>

<section class="breadcrumb-classic">

<div class="container">
  <div class="row">
  
    <div class="shell section-34 section-sm-50">
      <div class="range range-lg-middle">
        <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-division icon icon-white"></span></div>
        <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
          <h2 style = "text-align: center; margin-top:20px; margin-bottom: 10px;"><span class="big"><?= Yii::t('app', 'Ошибка');?></span></h2>
        </div>
        <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
            <h1 style ="margin-top: 50px; margin-bottom: 80px; text-align: center;"><?= $message;?></h1>
        </div>
      </div>
    </div>

  </div>
</div>

</section>
