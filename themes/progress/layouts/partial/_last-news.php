<?php
use yii\helpers\Url;
?>
<section class="well well2 well2_ins">
  <div class="container">
    <h2>
      Новости и события
    </h2>
    <div class="row">

      <?php foreach($lastNews as $num => $lastNewItem):?>
        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".<?php echo $num * 3;?>s">
          <article class="box">
            <div class="img_asside">
              <a href="news/show/<?php echo $lastNewItem->slug;?>"><img src="/<?php echo $lastNewItem->image;?>" alt="" style = "width: 122px;"></a>
            </div>
            <div class="box_cnt">
              <time datetime="<?php echo date('Y-m-d', strtotime($lastNewItem->date));?>"><?php echo date('Y/m/d', strtotime($lastNewItem->date));?></time>
              <h5>
                <a href="news/show/<?php echo $lastNewItem->slug;?>"><?php echo $lastNewItem->title;?></a>
              </h5>
              <p>
                <?php echo $lastNewItem->smallDescription;?>
              </p>
              <a href="news/show/<?php echo $lastNewItem->slug;?>" class="btn-link">Узнать подробне >></a>
            </div>
          </article>
        </div>
      <?php endforeach;?>

    </div>
  </div>
</section>
