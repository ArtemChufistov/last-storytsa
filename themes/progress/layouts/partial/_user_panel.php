<?php
use yii\helpers\Html;
use lowbase\user\models\User;
?>

<!-- Sidebar user panel -->
<div class="user-panel">
  <div class="pull-left image">
        <?php
        if ($user->identity->image) {
            echo "<img src='/".$user->identity->image."' class='img-circle' alt='" . Yii::t('app', 'Фото') . "'>";
        } else {
            echo "<img src='/images/default-avatar.jpg' class='img-circle' alt='" . Yii::t('app', 'Фото') . "'>";
        }
        ?>
  </div>
  <div class="pull-left info">
    <p><?php echo $user->identity->login;?></p>
    <a href="/profile/office/index"><i class="fa fa-circle text-success"></i> Online</a>
  </div>
</div>