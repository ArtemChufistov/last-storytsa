<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use app\modules\menu\widgets\MenuWidget;
use app\assets\SignUpAppAsset;

SignUpAppAsset::register($this);
?>

<style type="text/css">
	#subscriber-mail{
	border: 1px solid #333333;
    margin-top: 10px;
    padding: 10px;
    width: 84%;
    color: black;
	}
</style>

<?php $form = ActiveForm::begin(); ?>

<div class="col-md-3 col-sm-6 col-xs-12">
  <h3><?php echo Yii::t('app', 'Подписка на новости');?></h3>
  <p style = "text-align: justify"><?php echo Yii::t('app', 'Чтобы всегда быть в курсе событий Вы можете подписаться на новости, достаточно только указать свой E-mail');?></p>

	<?= $form->field($subscriber, 'mail')->textInput([
            'maxlength' => true,
            'placeholder' => Yii::t('app', 'Введите свой E-mail')
        ])->label(''); ?>

    <?= Html::submitButton(Yii::t('app', '<span>Подписаться >></span>'), ['class' => 'btn btn-dark btn-lg']);?>

	<?php if(Yii::$app->session->getFlash('succsessSubscribe')): ?>

		<h2 class="text-center"><?php echo Yii::t('app', 'Поздравляем!')?></h2>

		<p>
	  		<?php echo Yii::$app->session->getFlash('succsessSubscribe');?>
	  	</p>

	<?php endif;?>

</div>

<?php ActiveForm::end();?>