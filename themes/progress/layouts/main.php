<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\FrontendAppAsset;
use app\modules\menu\widgets\MenuWidget;

FrontendAppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
  <head>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

  </head>
  <body>
    <div class="page">

    <?php $this->beginBody() ?>

    <header>
      <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']);?>
    </header>

    <main>
    <?= $content ?>
    </main>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/_footer');?>

    <?php //$this->endBody(); ?>

    </div>
    <script src="/js/jquery.fancybox.js"></script>
    <script src="/js/script.js"></script>
  </body>
</html>
<?php $this->endPage() ?>
