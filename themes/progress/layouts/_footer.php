<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\menu\widgets\MenuWidget;
use app\modules\profile\widgets\SubscribeWidget;
use app\modules\news\widgets\LastNewsWidgetWidget;

?>

<footer>
  <section class="well_ft-1 bg2 clr-default-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-6">
          <h3>Наш адресс</h3>
          </br>
          <div class="contact-info">
            <address>119021, г. Москва, Россия </address>
            <address>ул. Тимура Фрунзе, 11, к. 49</address>
            <address>БЦ Красная Роза</address>
            </br>
            <dl class="freephone">
              <dt>Телефон: </dt>
              <dd><a href="callto:#">8 800 770 7456</a></dd>
              </br>
            </dl>
            <dl class="telephone">
              <dt>Абон. отдел:</dt>
              <dd><a href="callto:#">+7 495 792 43 00</a></dd>
            </dl>
            <dl class="email">
              <dt>E-mail: </dt>
              <dd><a href="mailto:#">Info@investprogress.net</a></dd>
            </dl>
          </div>
        </div>

        <?php echo MenuWidget::widget(['menuName' => 'botomMenu', 'view' => 'bottom-menu']);?>

        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_soc-icon-footer');?>

        <?php echo SubscribeWidget::widget();?>
      </div>
    </div>
  </section>

  <section  class="rights">
    <div class="container">
      <p>
        &#169; <span>2017</span> Progress investments. Все права защищены
      </p>
    </div>
  </section>
<?php /*
  <section class="map">
    <div id="google-map" class="map_model"></div>
    <ul class="map_locations">
      <li data-x="-73.9874068" data-y="40.643180">
        <p> 9870 St Vincent Place, Glasgow, DC 45 Fr 45. <span>800 2345-6789</span></p>
      </li>
    </ul>
  </section>
*/?>
</footer>
