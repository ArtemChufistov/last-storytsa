<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>
<div class="box box-success">
  <div class="box-header">
    <i class="fa fa-bookmark-o" aria-hidden="true"></i>

    <h3 class="box-title"><?php echo Yii::t('app', 'Ваш тикет');?></h3>

    <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
      <div class="btn-group" data-toggle="btn-toggle">
        <button class="btn btn-success btn-sm" type="button" data-widget="remove">
        <i class="fa fa-times"></i>
        </button>
      </div>
    </div>
  </div>
  <div class="box-body">
      <div class="form-group">
        <label class="control-label" ><?php echo Yii::t('app', 'Отдел');?></label>
        <pre class="form-control" ><?php echo $currentTicket->getDepartment()->one()->name;?></pre>
      </div>
      <div class="form-group">
        <label class="control-label" ><?php echo Yii::t('app', 'Текст');?></label>
        <pre class="form-control" style = "height: 80px;"><?php echo $currentTicket->text;?></pre>
      </div>
  </div>
</div>

<div class="box box-success">
  <div class="box-header">
    <i class="fa fa-comments-o"></i>

    <h3 class="box-title"><?php echo Yii::t('app', 'Чат с поддержкой');?></h3>

    <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
      <div class="btn-group" data-toggle="btn-toggle">
        <button class="btn btn-success btn-sm" type="button" data-widget="remove">
        <i class="fa fa-times"></i>
        </button>
      </div>
    </div>
  </div>
  <div class="box-body chat" id="chat-box">
    <?php foreach($ticketMessages as $ticketMessageItem): ?>
      <div class="item">
        <img src="/<?php echo $ticketMessageItem->getUser()->one()->image;?>" alt="user image" class="offline">

        <p class="message">
          <a href="#" class="name">
            <?php echo $ticketMessageItem->getUser()->one()->login;?>
          </a>
          <?php echo $ticketMessageItem->text;?>
        </p>
      </div>
    <?php endforeach;?>
  </div>
  <!-- /.chat -->
  <div class="box-footer">
    <?php $form = ActiveForm::begin([
      'options' => [
          'class'=>'input-group support-message',
      ],
    ]); ?>

        <?= $form->field($ticketMessage, 'text')->textInput([
            'maxlength' => true,
            'placeholder' => $ticketMessage->getAttributeLabel('text')
        ]) ?>

      <div class="input-group-btn">
          <?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-success']) ?>
      </div>
      <?php ActiveForm::end();?>

  </div>
</div>