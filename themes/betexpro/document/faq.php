<?php
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'Faq',
    'url' => '/faq'
  ],
];
$this->title = $model->title;
?>

<!-- Page Content-->
<main class="page-content">
  <!-- Faq Variant 2-->
  <section class="section-66 section-sm-top-110 section-lg-bottom-0">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-lg-6"><img src="images/faq.png" width="456" height="600" alt="" class="veil reveal-lg-inline-block"></div>
        <div class="cell-sm-9 cell-lg-6 offset-top-0">
          <h3><?php echo Yii::t('app', 'Основные вопросы');?></h3>
          <hr class="divider bg-mantis hr-lg-left-0">
          <div class="offset-top-41 offset-lg-top-66">
            <div role="tablist" aria-multiselectable="true" id="accordion-1" class="panel-group accordion offset-top-0">
              <div class="panel panel-default">
                <div role="tab" id="headingOne" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Как зарегистрироваться в проекте?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingOne" id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body"><?= Yii::t('app', 'В разработке');?>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div role="tab" id="headingTwo" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Как принять участие в проекте?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingTwo" id="collapseTwo" class="panel-collapse collapse">
                  <div class="panel-body"><?= Yii::t('app', 'В разработке');?>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div role="tab" id="headingThree" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Сколько я могу получить подарков?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingThree" id="collapseThree" class="panel-collapse collapse">
                  <div class="panel-body"><div class="panel-body"><?= Yii::t('app', 'В разработке');?></div>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div role="tab" id="headingFour" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Как подтвердить свой E-mail');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingFour" id="collapseFour" class="panel-collapse collapse">
                  <div class="panel-body"><div class="panel-body"><?= Yii::t('app', 'В разработке');?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- First Workout - Free-->
  <section class="section-66 context-dark bg-blue-gray">
    <div class="shell">
      <h4><?= Yii::t('app', 'Если вы не нашли ответ на свой вопрос вы можете');?></h4><a href="<?php echo Url::to(['contact/contact/index']);?>" class="btn btn-default btn-icon btn-icon-left offset-top-20"><span class="icon mdi mdi-email-outline"></span><?= Yii::t('app', 'Связаться с нами');?></a></br></br>
    </div>
  </section>
</main>