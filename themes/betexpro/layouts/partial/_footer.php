<?php
use app\modules\menu\widgets\MenuWidget;
use app\modules\news\widgets\LastNewsWidget;
use app\modules\profile\widgets\SubscribeWidget;
?>

<footer class="page-footer bg-darkest section-top-13 section-bottom-15 section-lg-top-90 text-sm-left">
  <div class="shell">
    <div class="range range-lg-right">

      <?php echo MenuWidget::widget(['menuName' => 'botomMenu', 'view' => 'bottom-menu']);?>
      <?php echo SubscribeWidget::widget();?>
      <div class="cell-sm-6 cell-lg-4 offset-top-45 offset-lg-top-0">
        <h5 class="text-white"><?php echo Yii::t('app', 'Кто мы?');?></h5>
        <p class="text-lighter offset-top-17"><?php echo Yii::t('app', 'Сообщество людей, которые готовы принимать решения для общей прибыли');?></p>
        <div class="offset-top-30 offset-md-top-55">
          <div class="group-xl"><a href="https://ru.wikipedia.org/wiki/MasterCard"><img src="images/footer-2.png" width="254" height="65" alt=""></a><a href="https://www.skrill.com/ru/"><img src="images/footer-1.png" width="129" height="65" alt=""></a></div>
        </div>
      </div>
      <div class="cell-lg-9 offset-top-45 offset-md-top-110">
        <p class="font-size-10"><a href="privacy.html" class="text-white">Пользовательское соглашение</a><span>&nbsp; | &nbsp;</span><a href="about" class="text-white">О Нас</a><span class="inset-md-left-100 inset-lg-left-220">&nbsp;  &#169;</span><span id="copyright-year"></span><span class="text-white">&nbsp; Betexpro &nbsp;</span><?php echo Yii::t('app', 'Все права защищены');?>
        </p>
      </div>
    </div>
  </div>
</footer>