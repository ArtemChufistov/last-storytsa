<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\BetexproFrontAppAsset;
use app\modules\menu\widgets\MenuWidget;

BetexproFrontAppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
  <head>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>

    <?php $this->head() ?>
  </head>
  <body>
    <?php $this->beginBody() ?>

    <div class="page text-center">
      <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']);?>

      <?= $content ?>

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_footer');?>

    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>