<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\web\View;

$this->title = Yii::t('app', 'Связаться с нами');

$this->params['breadcrumbs']= [ [
    'label' => $this->title,
    'url' => '/contact'
  ],
]

?>

</header>

<?php if(Yii::$app->session->hasFlash('success')): ?>

	<div class="successModal modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><?= Yii::t('app', 'Ваше сообщение принято');?></h4>
	      </div>
	      <div class="modal-body">
	        <p><?= Yii::t('app', 'Спасибо, что оставили нам сообщение, мы обязательно свяжемся с вам в ближайшее время');?></p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Закрыть');?></button>
	        <button type="button" class="btn btn-primary" data-dismiss="modal"><?= Yii::t('app', 'Ok');?></button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<?php echo $this->registerJs("$('.successModal').modal('show');", View::POS_END);?>

<?php endif;?>

<main class="page-content">
  <section class="section-top-75 section-md-top-135">
    <div class="shell">
      <h3><?php echo Yii::t('app', 'Связаться с нами');?></h3>
      <hr class="hr offset-top-35">
      <div class="range range-xs-center offset-top-0 features-list-variant-2 section-top-70 section-bottom-80">
        <div class="cell-xs-8 cell-sm-10 cell-lg-6 text-sm-left">
          <div class="unit unit-sm-horizontal unit-spacing-sm">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="unit-left"><img src="images/contact_images.png" alt="" width="322" height="607" class="img-responsive img-rounded img-fullwidth"/>
            </div>
          </div>
          
        </div>
        <div class="cell-sm-10 cell-lg-6 offset-top-50 offset-lg-top-0">
          <div class="inset-lg-left-110">
            <h5><?php echo Yii::t('app', 'Напишите нам и мы ответим вам в ближайшее время');?></h5>
            <!-- RD Mailform-->
            <?php $form = ActiveForm::begin([
              'options' => [
                  'class' => 'rd-mailform text-center offset-top-30',
                  'data-form-type' => 'contact',
                  'data-form-output' => 'form-output-global',
                  'enctype'=>'multipart/form-data'
              ],
            ]); ?>

              <div class="form-group">
                <?= $form->field($model, 'name') ?>
              </div>
              <div class="form-group">
                <?= $form->field($model, 'email') ?>
              </div>
              <div class="form-group">
                <?= $form->field($model, 'message')->textArea(['rows' => '6']); ?>
              </div>

              <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary offset-top-30']) ?>
            <?php ActiveForm::end(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>