<ul>
    <?php foreach($menu->children()->all() as $num => $children):?>
      <li class="<?php if($requestUrl == $children->link):?>active<?php endif;?>" ><a href="<?= $children->link;?>"><?= $children->title;?></a>
        <?php if (!empty($children->children()->all())):?>
          <ul>
            <?php foreach($children->children()->all() as $numChild => $childChildren):?>
              <li><a href="<?= $childChildren->link;?>"><?= $childChildren->title;?></a>
                <ul>
                  <li><a href=""><?= $childChildren->title;?></a>
                  </li>
                </ul>
              </li>
            <?php endforeach;?>
          </ul>
        <?php endif;?>
      </li>
    <?php endforeach;?>
</ul>