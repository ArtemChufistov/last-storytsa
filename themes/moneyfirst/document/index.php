
    <div class="banner">
        <div class="container">
            <div class="slider">
                <ul>
                    <li>
                        <h1>Как получить деньги??</h1>
                        <h2>Мы всегда отвечаем на индивидуальные потребности наших клиентов</h2>

                        <div class="actions">
                            <a href="#" class="slider-first-button">Получить займ</a>
                            <a href="#" class="slider-second-button">Подробнее</a>
                        </div>
                    </li>
                    <li>
                        <h1>Как получить деньги?? 2</h1>
                        <h2>Мы всегда отвечаем на индивидуальные потребности наших клиентов</h2>

                        <div class="actions">
                            <a href="#" class="slider-first-button">Получить займ</a>
                            <a href="#" class="slider-second-button">Подробнее</a>
                        </div>
                    </li>
                    <li>
                        <h1>Как получить деньги?? 3</h1>
                        <h2>Мы всегда отвечаем на индивидуальные потребности наших клиентов</h2>

                        <div class="actions">
                            <a href="#" class="slider-first-button">Получить займ</a>
                            <a href="#" class="slider-second-button">Подробнее</a>
                        </div>
                    </li>
                    <li>
                        <h1>Как получить деньги?? 4</h1>
                        <h2>Мы всегда отвечаем на индивидуальные потребности наших клиентов</h2>

                        <div class="actions">
                            <a href="#" class="slider-first-button">Получить займ</a>
                            <a href="#" class="slider-second-button">Подробнее</a>
                        </div>
                    </li>
                    <li>
                        <h1>Как получить деньги?? 5</h1>
                        <h2>Мы всегда отвечаем на индивидуальные потребности наших клиентов</h2>

                        <div class="actions">
                            <a href="#" class="slider-first-button">Получить займ</a>
                            <a href="#" class="slider-second-button">Подробнее</a>
                        </div>
                    </li>
                </ul>
            </div>

            <ul class="dots">
                <a href="#" class="active">
                    <li></li>
                </a>
                <a href="#">
                    <li></li>
                </a>
                <a href="#">
                    <li></li>
                </a>
                <a href="#">
                    <li></li>
                </a>
                <a href="#">
                    <li></li>
                </a>
            </ul>
        </div>
    </div>

    <div class="calculator">
        <div class="container">
            <ul class="controls">
                <li class="active"><a id="cash-payment-button" href="#"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Наличными</a></li>
                <li><a id="card-payment-button" href="#"><i class="fa fa-credit-card-alt" aria-hidden="true"></i>&nbsp;&nbsp;На карту</a></li>
            </ul>
            <div id="cash-grid" class="grid active">
                <div class="data">
                    <h1>Рассчитать займ</h1>

                    <div class="input-group">
                        <label for="sum">Сумма займа:</label>
                        <div class="input-field">
                            <input type="range" name="sum" class="sum" min="100" max="6000" step="100" value="3000">
                        </div>
                        <span class="input-details sum-details">
              3000 руб
            </span>
                    </div>

                    <div class="input-group">
                        <label for="time">Срок займа:</label>
                        <div class="input-field">
                            <input type="range" name="time" class="time" min="1" max="30" step="1" value="6">
                        </div>
                        <span class="input-details time-details">
              6 дней
            </span>
                    </div>

                    <h2>Всего к оплате: 0 руб. * до 01.01.1970 (включительно)</h2>
                    <h3>* расчет произведен по ставке для займа "Выгодный"</h3>

                    <a href="#" class="cash-button">Получить деньги</a>
                </div>
                <img src="images/calculator-bg.png" class="calculator-bg">
            </div>
            <div id="card-grid" class="grid">
                <div class="data">
                    <h1>Рассчитать займ 2</h1>

                    <div class="input-group">
                        <label for="sum">Сумма займа:</label>
                        <div class="input-field">
                            <input type="range" name="sum" class="sum" min="100" max="6000" step="100" value="3000">
                        </div>
                        <span class="input-details sum-details">
              3000 руб
            </span>
                    </div>

                    <div class="input-group">
                        <label for="time">Срок займа:</label>
                        <div class="input-field">
                            <input type="range" name="time" class="time" min="1" max="30" step="1" value="6">
                        </div>
                        <span class="input-details time-details">
              6 дней
            </span>
                    </div>

                    <h2>Всего к оплате: 0 руб. * до 01.01.1970 (включительно)</h2>
                    <h3>* расчет произведен по ставке для займа "Выгодный"</h3>

                    <a href="#" class="cash-button">Получить деньги</a>
                </div>
                <img src="images/calculator-bg.png" class="calculator-bg">
            </div>
        </div>
    </div>

    <div class="contents">
        <div class="container">
            <div class="about">
                <h1>О компании</h1>
                <div class="underline"></div>

                <p>На сегодняшний день “MoneyFirst” — одна из ведущих компаний, предлагающих форекс трейдинг. Безупречная репутация, клиентоориентированность и инновационный подход к развитию бизнеса позволили компании твердо занять свою нишу. Внести денежные средства в счет погашения займа через сервис «Золотая Корона – Погашение кредитов» можно более чем в 10 тыс. пунктах обслуживания по всей России, которые расположены в кассах партнёров сервиса - ведущих федеральных и региональных торговых сетях, например, таких, как сети салонов «Евросеть», «МТС», «Билайн», «Кари» и другие.– ФИО заемщика и номер мобильного телефона. При последующих обращениях – достаточно номер мобильного телефона и/или карту-идентификатор, выданную при первом обращении.</p>

                <a href="#" class="about-more-link">Подробнее >></a>
            </div>

            <div class="news">
                <h1>Новости</h1>
                <div class="underline"></div>

                <div class="article">
                    <img src="images/first-article-icon.png" class="article-icon">
                    <span class="article-description">Здесь идет краткий анонс новости или же можно заменить на заголовок...</span>
                </div>

                <div class="article">
                    <img src="images/second-article-icon.png" class="article-icon">
                    <span class="article-description">Здесь идет краткий анонс новости или же можно заменить на заголовок...</span>
                </div>

                <div class="article">
                    <img src="images/third-article-icon.png" class="article-icon">
                    <span class="article-description">Здесь идет краткий анонс новости или же можно заменить на заголовок...</span>
                </div>

                <a href="#" class="news-more-link">Все новости</a>
            </div>
        </div>
    </div>

    <div class="advantages">
        <div class="container">
            <h1>Взять микрозайм очень просто!</h1>

            <p>Наша миссия – помочь клиентам в решении финансовых затруднений. Ведь довольно часто у людей возникают вопросы, которые требуют быстрого решения.</p>
            <p>Преимущества срочных микрозаймов в MoneyFirst:</p>

            <ul>
                <li>Взять микрозайм можно только по паспорту, без оригиналов и копий документов! Вы сэкономите время и сохраните нервные клетки – ломать голову над тем, где взять немного денег, больше не нужно.Чтобы оформить микрозайм Вам потребуется паспорт и мобильный телефон, а также банковская карта в случае получения онлайн займа.</li>
                <li>Выдаем микрозайм быстро! Зачем тратить время на то, чтобы бегать по банкам, стоять в очередях? Решение о выдаче займа в MoneyFirst принимают очень быстро, в отличие от банков, где его можно ждать неделю, а то и две. Это очень быстрые деньги до зарплаты. Вы можете оформить нужную Вам сумму денег микрозаймом за 15 минут и даже в выходные дни!</li>
                <li>Выдаем микрозаймы по всей России не выходя из дома! Вы можете взять небольшую сумму денег в кредит через интернет или в одной из более чем 500 точек выдачи.</li>
                <li>Мы выдаем микрозаймы с плохой кредитной историей.</li>
                <li>У нас минимальные требования по возрасту заемщика. Взять срочный микрозайм с 20 лет! Онлайн заявка на микрозайм денег на сайте, сокращает время одобрения заявки до 15 минут. Онлайн займы работают круглосуточно и доступны через интернет сайт. Что такое микрозайм? Микрозайм - это займ денег на небольшую сумму до 100 000р для физических лиц и до 1 млн. рублей для предпринимателей и владельцев бизнеса. В России микрокредитование регулируется законом «О микрофинансовой деятельности». По закону компания, предоставляющая микрозаймы должна иметь разрешение и состоять в реестре микрофинансовых организаций. В России насчитывается более 4000 МФО, которые могут осуществлять выдачу микрозаймов на легальных основаниях. Одни компании выдают онлайн микрозаймы, другие привозят микрозайм на дом или выдают наличными в офисах. Оформить быстрый микрозайм на карту, банковский счет или электронный кошелек в инновационных МФО можно, не выходя из дома! Сторонники традиций могут выбрать вариант, предусматривающий получение денег наличными.</li>
            </ul>
        </div>
    </div>