<footer>
    <div class="container">
        <div class="grid">
            <div class="info">
                <p>© 2017, ООО «ПЕРВЫЕ ДЕНЬГИ»</p>
                <br>
                <p>Все права защищены.</p>
                <p>Информация, размещённая на данной странице, не является публичной офертой.</p>

                <ul class="social-buttons">
                    <li>
                        <a href="#"><img src="images/facebook-small-icon.png"></a>
                    </li>
                    <li>
                        <a href="#"><img src="images/twitter-small-icon.png"></a>
                    </li>
                    <li>
                        <a href="#"><img src="images/vk-small-icon.png"></a>
                    </li>
                    <li>
                        <a href="#"><img src="images/ok-small-icon.png"></a>
                    </li>
                    <li>
                        <a href="#"><img src="images/youtube-small-icon.png"></a>
                    </li>
                </ul>
            </div>
            <ul class="sitemap">
                <li><a href="#">Помощь</a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#">Статьи</a></li>
                <li><a href="#">Обратная связь</a></li>
            </ul>
        </div>
    </div>
</footer>