<div class="cell-xs-7 text-xs-left cell-md-4 cell-lg-3 cell-lg-push-4">
  <h6 class="text-uppercase text-spacing-60"><?php echo Yii::t('app', 'Последние новости');?></h6>
    <?php foreach($lastNews as $lastNewItem):?>
      <article class="post widget-post text-left text-picton-blue"><a href="/news/<?php echo $lastNewItem->slug;?>">
        <div class="unit unit-horizontal unit-spacing-xs unit-middle">
          <div class="unit-body">
            <div class="post-meta"><span class="icon-xxs mdi mdi-arrow-right"></span>
              <time datetime="<?php echo date('Y-m-d', strtotime($lastNewItem->date));?>" class="text-dark"><?php echo date('d/m/Y', strtotime($lastNewItem->date));?></time>
            </div>
            <div class="post-title">
              <h6 class="text-regular">><?php echo $lastNewItem->title;?></h6>
            </div>
          </div>
        </div></a>
      </article>
    <?php endforeach;?>
</div>