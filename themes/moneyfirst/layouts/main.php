<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\FrontendAppAsset;
use app\modules\menu\widgets\MenuWidget;

FrontendAppAsset::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=805, initial-scale=1.0">
    <title>Money First</title>
    <link rel="stylesheet" href="css/index.css">
</head>

<body>
    <header>
        <div class="container">
            <a href = "/"><img class="logo" src="images/logo.png"></a>
            <a href="/" class="locations-button">
                <img class="locations-button-icon" src="images/locations-button-icon.png">
                <span>места выдачи займов</span>
            </a>
            <div class="right-bar">
                <a href="#" class="personal-office-button"><i></i><i class="fa fa-bank"></i>&nbsp;&nbsp;&nbsp;Личный кабинет</a>
                <span class="contacts-phone-number">тел.: 8 (800) 222 50 80</span>
            </div>
        </div>
    </header>

    <nav>
        <div class="container">
            <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']);?>
        </div>
    </nav>

    <?= $content ?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/_footer');?>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://use.fontawesome.com/934ebf4b5c.js"></script>
<script src="js/index.js"></script>

</html>


<?php $this->endPage() ?>