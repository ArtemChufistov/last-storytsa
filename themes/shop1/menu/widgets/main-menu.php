<?php
use app\modules\shop\models\Category;
use app\modules\shop\models\Product;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="header-bg">
    <div class="header-main" id="header-main-fixed">
        <div class="header-main-block1">
            <div class="container">
                <div id="container-fixed">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="/" class="header-logo"> <img src="/shop1/img/logo-big-shop.png" alt=""></a>
                        </div>
                        <div class="col-md-5">
                            <div class="top-search-form pull-left">
                                <?php $sortform = ActiveForm::begin(['method' => 'get', 'action' => ['/productsearch']]); ?>
                                    <?php echo Html::textInput('word', null, ['placeholder' => Yii::t('app', 'Поиск...'), 'class' => 'form-control']);?>
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                <?php ActiveForm::end();?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="top-icons">
                                <div class="top-icon"><a href="" title="Wishlist"><i class="fa fa-heart"></i></a></div>
                                <div class="top-icon"><a href="" title="Notification"><i class="fa fa-bell"></i></a><span>12</span></div>
                                <div class="top-icon"><a href="" title="Login"><i class="fa fa-lock"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="header-main-block2">
            <nav class="navbar yamm  navbar-main" role="navigation">

                <div class="container">
                    <div class="navbar-header">
                        <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                        <a href="/" class="navbar-brand"><i class="fa fa-home"></i></a>
                    </div>
                    <div id="navbar-collapse-1" class="navbar-collapse collapse ">
                        <ul class="nav navbar-nav">
                            <!-- Classic list -->
                            <li class="dropdown"><a href="/" data-toggle="dropdown" class="dropdown-toggle"><?php echo Yii::t('app', 'Главная');?> <i class="fa fa-caret-right fa-rotate-45"></i></a>
                                <ul class="dropdown-menu list-unstyled fadeInUp animated">
                                    <li>
                                        <a  href="/#main-slider"> <?php echo Yii::t('app', 'Новый стиль');?> </a>
                                    </li>
                                    <li>
                                        <a  href="/#main-bestseller"> <?php echo Yii::t('app', 'Топ продаж');?></a>
                                    </li>
                                    <li>
                                        <a  href="/#special-offer"> <?php echo Yii::t('app', 'Спец предложения');?><span><?php echo Yii::t('app', 'Скидки');?></span></a>
                                    </li>
                                    <li>
                                        <a  href="/#new-collection"> <?php echo Yii::t('app', 'Новая коллекция');?></a>
                                    </li>
                                    <li>
                                        <a  href="/#our-partners"> <?php echo Yii::t('app', 'Наши партнёры');?></a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown  yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><?php echo Yii::t('app', 'Каталог');?> <i class="fa fa-caret-right fa-rotate-45"></i> <span><?php echo Yii::t('app', 'Новинки');?></span></a>
                                <ul class="dropdown-menu list-unstyled  fadeInUp animated">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="header-menu">
                                                        <h4><?php echo  Yii::t('app', 'Основные категории');?></h4>
                                                    </div> 
                                                    <ul class="list-unstyled">
                                                        <?php $categoryArray = Category::find()->where(['parent_id' => 0])->all();?>
                                                        <?php foreach($categoryArray as $category):?>
                                                            <li>
                                                                <a href="/category/<?php echo $category->key; ?>"> <?php echo $category->name;?> </a>
                                                            </li>
                                                        <?php endforeach;?>
                                                    </ul>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="header-menu">
                                                        <h4><?php echo  Yii::t('app', 'Популярные товары');?></h4>
                                                    </div>
                                                    <ul class="list-unstyled">
                                                        <?php $productArray = Product::find()->orderBy(['id' => SORT_DESC])->limit(10)->all();?>
                                                        <?php foreach($productArray as $product):?>
                                                            <li>
                                                                <a href="/product/<?php echo $product->key;?>"><i class="fa fa-angle-right"></i> <?php echo $product->name; ?> </a>
                                                            </li>
                                                        <?php endforeach;?>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="banner">
                                                        <a href="#">
                                                            <img src="http://placehold.it/900x1200" class="img-responsive" alt="">
                                                        </a> 
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </li>

                            <!-- With content -->
                            <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><?php echo Yii::t('app', 'Скидки');?> <i class="fa fa-caret-right fa-rotate-45"></i></a>
                                <ul class="dropdown-menu list-unstyled  fadeInUp animated">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="header-menu">
                                                        <h4>Women</h4>
                                                    </div> 
                                                    <ul class="list-unstyled">
                                                        <li><a href="#">Dresses</a></li>
                                                        <li><a href="#">Bags</a></li>
                                                        <li><a href="#">Jeans</a></li>
                                                        <li><a href="#">Shirts</a></li>
                                                        <li><a href="#">T-shirts</a></li>
                                                        <li><a href="#">Wallets</a></li>
                                                        <li><a href="#">Hair Accessories</a></li>
                                                        <li><a href="#">Short dresses</a></li>

                                                    </ul>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="header-menu">
                                                        <h4>Men</h4>
                                                    </div>
                                                    <ul class="list-unstyled">
                                                        <li><a href="#">Jeans</a></li>
                                                        <li><a href="#">Shirts</a></li>
                                                        <li><a href="#">T-shirts</a></li>
                                                        <li><a href="#">Blazers</a></li>
                                                        <li><a href="#">Sport Bags</a></li>
                                                        <li><a href="#">Jacekts</a></li>
                                                        <li><a href="#">Coats</a></li>
                                                        <li><a href="#">Sandals</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6">
                                                    <article class="banner">
                                                        <a href="#">
                                                            <img src="http://placehold.it/900x677" class="img-responsive" alt="">
                                                        </a> 
                                                    </article>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><?php echo Yii::t('app', 'О нас');?> <i class="fa fa-caret-right fa-rotate-45"></i></a>
                                <ul class="dropdown-menu list-unstyled fadeInUp animated">
                                    <li class="dropdown-submenu">
                                        <a href="#"> <?php echo Yii::t('app', 'Новости');?> </a>
                                        <ul class="dropdown-menu list-unstyled">
                                            <li>
                                                <a href="#"> <?php echo Yii::t('app', 'Новая линейка товаров');?> </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#"> <?php echo Yii::t('app', 'Информация');?> </a>
                                        <ul class="dropdown-menu list-unstyled">
                                            <li>
                                                <a href="#"> <?php echo Yii::t('app', 'Как приобрести товар');?> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <?php echo Yii::t('app', 'Способы доставки');?> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <?php echo Yii::t('app', 'Наши поставщики');?> </a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>

                            <li><a href="/#tags"><?php echo Yii::t('app', 'Тэги');?></a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/contact/feedback"><?php echo Yii::t('app', 'Связаться с нами');?></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>