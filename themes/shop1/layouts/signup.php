<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\menu\widgets\MenuWidget;
use app\assets\lkpool\SignupTopAsset;
use app\assets\lkpool\SignupBottomAsset;

SignupTopAsset::register($this);
SignupBottomAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/meta-icon'); ?>

    <?= Html::csrfMetaTags() ?>

	<title><?= Html::encode($this->title) ?></title>

	<?php $this->head() ?>

    <script type="text/javascript">
    window.onload = function()
    {
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header ">
  	<?php $this->beginBody() ?>

  		<?php echo $content;?>

    <?php $this->endBody() ?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/particles'); ?>

  </body>
</html>
<?php $this->endPage() ?>