<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\lkpool\OfficeTopAsset;
use app\assets\lkpool\OfficeBottomAsset;
use app\modules\menu\widgets\MenuWidget;
use app\modules\profile\widgets\ChangeEmailWidget;

OfficeTopAsset::register($this);
OfficeBottomAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/meta-icon'); ?>

    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

  </head>
  <body class="fixed-header dashboard">
    <?php $this->beginBody() ?>

    <?= ChangeEmailWidget::widget(['user' => $this->params['user']->identity]); ?>

    <?php echo MenuWidget::widget(['menuName' => 'profileMenu', 'view' => 'office-menu']);?>

    <div class="page-container ">
      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/office-header'); ?>
      <?php echo $content;?>
    </div>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/quickview'); ?>

    <!-- END PAGE LEVEL JS -->
    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>