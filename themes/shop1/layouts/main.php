<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\components\LayoutComponent;
use app\assets\shop1\FrontTopAsset;
use app\assets\shop1\FrontBottomAsset;
use app\modules\menu\widgets\MenuWidget;

FrontTopAsset::register($this);
FrontBottomAsset::register($this);

?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">``
        <![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <!--[if IE 7]>
          <link rel="stylesheet" href="css/font-awesome-ie7.css">
        <![endif]-->

      <?= Html::csrfMetaTags() ?>

      <title><?= Html::encode($this->title) ?></title>

      <?php $this->head() ?>

<style type="text/css">
    .figure-hover-overlay{
        height: 300px;
    }
    .grid-product-img{
        left: 0;
        right: 0;
        margin-left: auto;
        margin-right: auto;
        max-height: 300px;
        top: 50%;
        transform: translateY(-50%);
    }
    .product-name{
        height: 48px;
    }
</style>

    </head>
    <body>
      <?php $this->beginBody() ?>
        <header id="header">
          <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/head-info')); ?>
          <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']); ?>
        </header>

        <?php echo $content;?>

        <footer id="footer-block">
            <div class="social">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="socials">
                                <a href="#"><i class="fa fa-skype"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-youtube"></i></a>
                            </div>   
                        </div>
                        <div class="col-md-6">
                            <form class="form-horizontal">
                                <input type="text" class="input form-control" placeholder="Newsletter">
                                <button type="submit"> Sign up <i class="fa fa-angle-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-information">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="header-footer">
                                <h3><?php echo Yii::t('app', 'Информация');?></h3>
                            </div>
                            <ul class="footer-categories list-unstyled">
                                <li><a href="#"><?php echo Yii::t('app', 'О Нас');?></a></li>
                                <li><a href="#"><?php echo Yii::t('app', 'Контакты');?></a></li>
                                <li><a href="#"><?php echo Yii::t('app', 'Доставка');?></a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Site Map</a></li>
                                <li><a href="#">Returns</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <div class="header-footer">
                                <h3>My Account</h3>
                            </div>
                            <ul class="footer-categories list-unstyled">
                                <li><a href="#">My Account</a></li>
                                <li><a href="#">Wish List</a></li>
                                <li><a href="#">Order History</a></li>
                                <li><a href="#">Brands</a></li>
                                <li><a href="#">Specials</a></li>
                                <li><a href="#">Newsletter</a></li>
                                <li><a href="#">Secure payment</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <div class="header-footer">
                                <h3>I want ...</h3>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            </p>
                            <div class="want">
                                <form class="form-horizonta">
                                    <textarea class="form-control" placeholder="I want ..."></textarea>
                                    <button type="submit"> Send us <i class="fa fa-angle-right"></i></button>
                                </form>
                            </div>



                        </div>
                        <div class="col-md-3">
                            <div class="header-footer">
                                <h3>Get In Touch</h3>
                            </div>
                            <p><strong>Phone: 1-000-000-0000</strong><br><strong>Email:</strong> info@yourdomain.com</p>
                            <p><strong>Your Company LTD.</strong><br>Street Name, 000000, City Name</p>
                            <p><a href=""><i class="icon-map-marker"></i> Location in Google Maps</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-copy color-scheme-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="index.html" class="logo-copy pull-left"></a>
                        </div>
                        <div class="col-md-4">
                            <p class="text-center">
                                <a href="">Copyright В© 2014</a> Effect | All rights reserved.<br>Designed by <a href="http://mosaicdesign.uz" target="_blank">Mosaic Design</a>
                            </p>
                        </div>
                        <div class="col-md-4">
                            <ul class="footer-payments pull-right">
                                <li><img src="/shop1/img/payment-maestro.jpg" alt="payment" /></li>
                                <li><img src="/shop1/img/payment-discover.jpg" alt="payment" /></li>
                                <li><img src="/shop1/img/payment-visa.jpg" alt="payment" /></li>
                                <li><img src="/shop1/img/payment-american-express.jpg" alt="payment" /></li>
                                <li><img src="/shop1/img/payment-paypal.jpg" alt="payment" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

      <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>