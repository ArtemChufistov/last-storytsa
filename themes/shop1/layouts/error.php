<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\menu\widgets\MenuWidget;
use app\assets\lkpool\SignupTopAsset;
use app\assets\lkpool\SignupBottomAsset;

SignupTopAsset::register($this);
SignupBottomAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/meta-icon'); ?>

    <?= Html::csrfMetaTags() ?>

  <title><?= Html::encode($this->title) ?></title>

  <?php $this->head() ?>

    <script type="text/javascript">
    window.onload = function()
    {
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header ">
    <?php $this->beginBody() ?>

      <div class="login-wrapper ">
        <div class="bg-pic">
          <div id="particles-js">
          </div>
            <img src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src-retina="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" alt="" class="lazy" style = "width: 100%;">
        </div>
      </div>

      <div class="errorModal modal fade slide-up disable-scroll" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content-wrapper">
          <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
              <div class="col-xs-12 text-center" style = "text-align: center;">
                <?=$message;?>
                <br/>
                <br/>
                <a href="/" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40"><?php echo Yii::t('app', 'Вернуться на главную');?></a>
              </div>
            </div>

          </div>
          </div>
        </div>
      </div>

      <script type="text/javascript">
      $(function() {
        $('.errorModal').modal('show');
      });
      </script>

    <?php $this->endBody() ?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/particles'); ?>

  </body>
</html>
<?php $this->endPage() ?>