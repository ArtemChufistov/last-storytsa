<?php
use app\components\LayoutComponent;
use app\modules\shop\models\ProductReview;
use app\modules\shop\widgets\ProductReviews;

$this->title = $campaign->name . ' ' . $product->name;

$breadcrumbs = [];
$breadcrumbs[] = ['url' => '/', 'title' => Yii::t('app', 'Главная')];
$breadcrumbs[] = ['url' => '/campaign/' . $campaign->key, 'title' => $campaign->name];

foreach($category->ancestors()->all() as $categoryItem){
    $breadcrumbs[] = ['url' => '/category/' . $categoryItem->key, 'title' => $categoryItem->name];
}

$breadcrumbs[] = ['url' => '/category/' . $category->key, 'title' => $category->name];
$breadcrumbs[] = ['url' => '/product/' . $product->key, 'title' => $product->name];

Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => Yii::$app->name . ' | ' . $campaign->name . ' | ' . Yii::t('app', 'Продукт: {product}', ['product' => $product->name])
        ]);
Yii::$app->view->registerMetaTag([
            'name' => 'keywords',
            'content' => Yii::$app->name . ' | ' . $campaign->name . ' | ' . Yii::t('app', 'Продукт: {product}', ['product' => $product->name])
        ]);

?>
<section>
    <div class="second-page-container">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <div class="block-breadcrumb">
                        <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/breadcrumb'), [
                            'breadcrumbs' => $breadcrumbs
                        ]); ?>
                    </div>

                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s"><?php echo $product->name;?></h1>
                    </div>

                    <div class="block-product-detail">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="product-image">
                                    <img id="product-zoom"  src='<?php echo $product->pictures;?>' data-zoom-image="<?php echo $product->pictures;?>" alt="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">


                                <div class="product-detail-section">
                                    <h3><?php echo $product->typePrefix; ?></h3>
                                    <div class="product-rating">
                                        <div class="stars">
                                            <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                        </div>
                                        <a href="" class="review">
                                            <?php echo ProductReview::find()->where(['product_id' => $product->id])->count();?> <?php echo Yii::t('app', 'Отзывов');?>
                                        </a>
                                    </div>

                                    <div class="product-information">
                                        <div class="clearfix">
                                            <label class="pull-left"><?php echo Yii::t('app', 'Брэнд');?>:</label> <a href="#"><?php echo $product->vendor; ?></a><br>
                                        </div>
                                        <div class="clearfix">
                                            <label class="pull-left"><?php echo Yii::t('app', 'Артикул');?>:</label> <?php echo $product->vendorCode; ?>
                                        </div>
                                        <div class="clearfix">
                                            <label class="pull-left"><?php echo Yii::t('app', 'В наличии');?>:</label>
                                            <p><?php echo $product->available == 1 ? Yii::t('app', 'да') : Yii::t('app', 'нет');?></p>
                                        </div>
                                        <div class="clearfix">
                                            <label class="pull-left"><?php echo Yii::t('app', 'Описание'); ?>:</label>
                                            <p class="description">
                                                <?php if (!empty($product->description)):?>
                                                    <?php echo $product->description;?>
                                                <?php else:?>
                                                    <?php echo Yii::t('app', 'Отсутствует');?>
                                                <?php endif;?>
                                            </p>
                                        </div>
                                        <div class="clearfix">
                                            <label class="pull-left"><?php echo Yii::t('app', 'Цена');?>:</label>
                                            <p class="product-price"><?php echo $product->price;?> <?php echo Yii::t('app', 'Руб');?></p>
                                        </div>
                                        <div class="shopping-cart-buttons">
                                            <a rel="nofollow" href="/order/<?php echo $product->key;?>" title="<?php echo Yii::t('app', 'Купить'); ?>" class="shoping">
                                                <i class="fa fa-shopping-cart"></i>  <?php echo Yii::t('app', 'Купить'); ?>
                                            </a>
                                            <a href="#" title="<?php echo Yii::t('app', 'Мне нравиться');?>"><i class="fa fa-heart-o"></i></a>
                                            <a href="#" title="<?php echo Yii::t('app', 'Поделиться');?>"><i class="fa fa-random"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo ProductReviews::widget(['product' => $product]);?>
                </div>
                <div class="col-md-3">
                    <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/category-tree'), ['category' => $category]); ?>

                    <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/same-product'), ['sameProducts' => $sameProducts]); ?>

                    <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/last-see-product'), ['orderProducts' => $orderProducts]); ?>
                </div>
            </div>
        </div>
    </div>
</section>