<?php
use app\modules\shop\models\search\ProductSearch;
use app\components\LayoutComponent;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = $campaign->name . ' ' . $category->name;
$productArray = $productProvider->getModels();

$breadcrumbs = [];
$breadcrumbs[] = ['url' => '/', 'title' => Yii::t('app', 'Главная')];
$breadcrumbs[] = ['url' => '/campaign/' . $campaign->key, 'title' => $campaign->name];

foreach($parentCategoryes as $categoryItem){
    $breadcrumbs[] = ['url' => '/category/' . $categoryItem->key, 'title' => $categoryItem->name];
}

$breadcrumbs[] = ['url' => '/category/' . $category->key, 'title' => $category->name];

Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => Yii::$app->name . ' | ' . $campaign->name . ' | ' . Yii::t('app', 'Категория: {category}', ['category' => $category->name])
        ]);
Yii::$app->view->registerMetaTag([
            'name' => 'keywords',
            'content' => Yii::$app->name . ' | ' . $campaign->name . ' | ' . Yii::t('app', 'Товары в категории: {category}', ['category' => $category->name])
        ]);
?>
<section>
    <div class="second-page-container">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <div class="block-breadcrumb">
                        <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/breadcrumb'), [
                            'breadcrumbs' => $breadcrumbs
                        ]); ?>
                    </div>

                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s"><?php echo $campaign->name;?> <span><?php echo $category->name;?></span></h1>
                    </div>

                    <div class="block-products-modes color-scheme-2">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="product-view-mode">
                                    <a href="#grid" class="product-view-type" type = "grid-view"><i class="fa fa-th-large"></i></a>
                                    <a href="#list" class="product-view-type" type = "list-view"><i class="fa fa-th-list"></i></a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                <div class="row">
                                    <?php $sortform = ActiveForm::begin(['method' => 'get']); ?>
                                        <div class="col-md-3 col-md-offset-1">
                                            <label class="pull-right"><?php echo Yii::t('app', 'Сортировать по');?></label>
                                        </div>
                                        <div class="col-md-5">
                                            <?php echo Html::dropDownList(
                                                'sort',
                                                !empty($_GET['sort']) ? $_GET['sort'] : '',
                                                [
                                                    '0' => '--',
                                                    '+price' => Yii::t('app', 'Цена: сначало дешевле'),
                                                    '-price' => Yii::t('app', 'Цена: сначало дороже'),
                                                    '+name' => Yii::t('app', 'По алфавиту: А-Я'),
                                                ],
                                                ['class' => 'form-control', 'onchange'=>'this.form.submit()']
                                            );?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo Html::dropDownList(
                                                'per-page',
                                                $productProvider->getPagination()->getLimit(),
                                                [20 => 20, 30 => 30, 50 => 50],
                                                ['class' => 'form-control', 'onchange' => 'this.form.submit()']
                                            );?>
                                        </div>
                                    <?php ActiveForm::end();?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row product-view grid-view" style = "display: none;">
                        <?php foreach($productArray as $productItem):?>
                            <div class="col-xs-12 col-sm-6 col-md-4 text-center mb-25">
                                <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/grid-product'), [
                                    'product' => $productItem,
                                ]); ?>
                            </div>
                        <?php endforeach;?>
                        <div class="col-xs-12 text-center mb-25">
                            <?php echo LinkPager::widget(['pagination' => $productProvider->pagination]);?>
                        </div>
                    </div>
                    <div class="row product-view list-view" style = "display: none;">
                        <?php foreach($productArray as $productItem):?>
                            <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/list-product'), [
                                'product' => $productItem,
                            ]); ?>
                        <?php endforeach;?>
                        <div class="col-xs-12 text-center mb-25">
                            <?php echo LinkPager::widget(['pagination' => $productProvider->pagination]);?>
                        </div>
                    </div>
                </div>
                <aside class="col-md-3">
                    <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/category-tree'), ['category' => $category]); ?>

                    <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/product-params'), [
                        'productModel' => $productModel,
                        'category' => $category,
                    ]); ?>

                    <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/last-see-product'), ['orderProducts' => $orderProducts]); ?>
                </aside>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

var hash = window.location.hash;

if (!hash) {
    hash = '#grid';
}

var type = $( "a[href='" + hash + "']" ).attr('type');
$( "a[href='" + hash + "']" ).addClass('active');
$('.' + type).show();

$('.product-view-type').click(function(){
    var type = $(this).attr('type');
    $('.product-view-type').removeClass('active');
    $(this).addClass('active');
    $('.product-view').hide();
    $('.' + type).show();
})
</script>