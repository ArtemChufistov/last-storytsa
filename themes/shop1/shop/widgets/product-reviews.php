<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\captcha\Captcha;
?>
<div class="box-border block-form">
    <!-- Nav tabs -->
    <ul class="nav nav-pills  nav-justified">
        <li class="active"><a href="#review" data-toggle="tab"><?php echo Yii::t('app', 'Отзывы');?></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="review">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <h3><?php echo Yii::t('app', 'Отзывы покупателей');?></h3>
                    <hr>
                    <?php foreach($productReviews as $productReview): ?>
                        <div class="review-header">
                            <h5><?php echo $productReview->name;?></h5>
                            <div class="product-rating">
                                <div class="stars">
                                    <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                </div>
                            </div>
                            <small class="text-muted"><?php echo date('d.m.Y', strtotime($productReview->date_add));?></small>
                        </div>
                        <div class="review-body">
                            <p><?php echo $productReview->text;?></p>
                        </div>
                        <hr>
                    <?php endforeach;?>
                </div>
            </div>
            <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($reviewForm, 'name') ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($reviewForm, 'text')->textarea(['rows' => '3']) ?>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label  class="control-label"><?php echo Yii::t('app', 'Ваша оценка');?>:</label>
                            <div class="product-rating">
                                <div class="stars">
                                    <span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($reviewForm, 'captcha')->widget(Captcha::className(), [
                            'captchaAction' => '/shop/captcha',
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => $reviewForm->getAttributeLabel('captcha')
                            ],
                            'template' => '<div class="row">
                            <div class="col-lg-8">{input}</div>
                            <div class="col-lg-4">{image}</div>
                            </div>',
                        ]);?>
                    </div>
                </div>
                <?= Html::submitButton(Yii::t('app', 'Добавить отзыв'), ['class' => 'btn-default-1']) ?>
            <?php ActiveForm::end();?>
        </div>
    </div>
</div>