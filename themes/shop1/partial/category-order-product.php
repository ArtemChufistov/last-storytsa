<div class="widget-title">
    <i class="fa fa-thumbs-up"></i> <?php echo Yii::t('app', 'Лучшие продажи');?>
</div>
<?php foreach ($orderProducts as $orderProduct):?>
    <?php $product = $orderProduct->getProduct()->one();?>
    <div class="product light last-sale">
        <figure class="figure-hover-overlay">
            <a href="#"  class="figure-href"></a>
            <div class="product-sale">Save <br> 7%</div>
            <div class="product-sale-time"><p class="time"></p></div>
            <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
            <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
            <img src="http://placehold.it/400x500" class="img-overlay img-responsive" alt="">
            <img src="http://placehold.it/400x500" class="img-responsive" alt="">
        </figure>
        <div class="product-caption">
            <div class="block-name">
                <a href="#" class="product-name"><?php echo $product->price;?></a>
                <p class="product-price"><span>$330</span> $320.99</p>
            </div>
            <div class="product-cart">
                <a href="#"><i class="fa fa-shopping-cart"></i> </a>
            </div>
        </div>
    </div>
<?php endforeach;?>