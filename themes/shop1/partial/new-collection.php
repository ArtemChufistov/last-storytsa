<section class="block-chess-banners">
    <div class="block">
        <div class="container">
            <div class="header-for-dark">
                <h1 class="wow fadeInRight animated" data-wow-duration="2s"><?php echo Yii::t('app', 'Новая');?> <span><?php echo Yii::t('app', 'Коллекция');?></span></h1>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <article class="block-chess wow fadeInLeft" data-wow-duration="2s">
                        <div class="row">
                            <div class="col-md-4">
                                <a href="#"><img class="img-responsive" src="http://placehold.it/900x677" alt=""></a>

                            </div>
                            <div class="col-md-8">
                                <div class="chess-caption-right">
                                    <a href="#" class="col-name">Modern collection</a>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-md-3">
                    <article class="block-chess wow fadeInRight" data-wow-duration="2s">
                        <a href="#"><img class="img-responsive" src="http://placehold.it/900x677" alt=""></a>
                    </article>
                </div>
            </div> 
            <div class="row">

                <div class="col-md-3">
                    <article class="block-chess wow fadeInLeft" data-wow-duration="2s">
                        <a href="#"><img class="img-responsive" src="http://placehold.it/900x677" alt=""></a>
                    </article>
                </div>
                <div class="col-md-9">
                    <article class="block-chess wow fadeInRight" data-wow-duration="2s">
                        <div class="row">

                            <div class="col-md-8">
                                <div class="chess-caption-left">
                                    <a href="#" class="col-name">Classic collection</a>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <a href="#"><img class="img-responsive" src="http://placehold.it/900x677" alt=""></a> 
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>