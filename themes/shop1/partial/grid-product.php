<article class="product light">
    <figure class="figure-hover-overlay">
        <a href="/product/<?php echo $product->key;?>"  class="figure-href"></a>
        <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
        <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
        <img src="<?php echo $product->pictures;?>" class="img-overlay img-responsive grid-product-img" alt="">
        <img src="<?php echo $product->pictures;?>" class="img-responsive grid-product-img" alt="">
    </figure>
    <div class="product-caption" style="height: 160px;">
        <div class="block-name">
            <a href="/product/<?php echo $product->key;?>" class="product-name"><?php echo $product->name;?></a>
            <p class="product-price"><?php echo $product->price;?> <?php echo Yii::t('app', 'руб');?></p>
        </div>
        <div class="product-cart">
            <a rel="nofollow" href="/order/<?php echo $product->key;?>"><i class="fa fa-shopping-cart"></i> </a>
        </div>
        <div class="product-rating">
            <div class="stars">
                <span class="star"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
            </div>
            <a href="" class="review">8 <?php echo Yii::t('app', 'Отзывов');?></a>
        </div>
        <?php if (!empty($product->description)):?>
            <p class="description"><?php echo mb_substr($product->description, 0, 60);?>...</p>
        <?php endif;?>
    </div>

</article>