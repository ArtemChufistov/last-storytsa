<div class="header-top-row">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="top-welcome hidden-xs hidden-sm">
                    <p>
                        <?php echo Yii::t('app', 'Добро пожаловать');?> &nbsp;&nbsp;<i class="fa fa-phone"></i>
                        <?php echo Yii::t('app', '707-505-8008');?> &nbsp; <i class="fa fa-envelope"></i>
                        <?php echo Yii::t('app', 'support@megashop.ru');?>
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <!-- header - language -->
                    <div id="lang" class="pull-right">
                        <a href="#" class="lang-title">
                            <img src="/shop1/img/Flag_of_Russia.png" style = "height: 14px;" alt="<?php echo Yii::t('app', 'Русский');?>"" title="<?php echo Yii::t('app', 'Русский');?>"> <?php echo Yii::t('app', 'Русский');?> <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="list-unstyled lang-item">
                            <li class="active">
                                <a href=""><img src="/shop1/img/f-gb.png" alt="<?php echo Yii::t('app', 'Русский');?>" title="">
                                    <?php echo Yii::t('app', 'English');?>
                                </a>
                            </li>
                            <li>
                                <a href=""><img src="/shop1/img/f-fr.png" alt="French" title="<?php echo Yii::t('app', 'English');?>">
                                    <?php echo Yii::t('app', 'English');?>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- header - currency -->
                    <div id="currency" class="pull-right">
                        <a href="" class="currency-title">$ USD <i class="fa fa-angle-down"></i> </a>
                        <ul class="list-unstyled currency-item">
                            <li><a href="">€ EURO</a></li>
                            <li><a href="">£ POUND</a></li>
                        </ul>
                    </div>
                    <!-- /header - currency -->

                    <!-- header-account menu -->
                    <div id="account-menu" class="pull-right">
                        <a href="" class="account-menu-title"><i class="fa fa-user"></i>&nbsp; Account <i class="fa fa-angle-down"></i> </a>
                        <ul class="list-unstyled account-menu-item">
                            <li><a href=""><i class="fa fa-heart"></i>&nbsp; Wishlist</a></li>
                            <li><a href=""><i class="fa fa-check"></i>&nbsp; Checkout</a></li>
                            <li><a href=""><i class="fa fa-shopping-cart"></i>&nbsp; Cart</a></li>
                        </ul>
                    </div>
                    <!-- /header-account menu -->

                    <!-- header - currency -->
                    <div class="socials-block pull-right">
                        <ul class="list-unstyled list-inline">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                    <!-- /header - currency -->
                </div>
            </div>
        </div>
    </div>
</div>