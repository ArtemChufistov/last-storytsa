<ul class="breadcrumb">
    <?php foreach($breadcrumbs as $num => $breadcrumb):?>
        <?php if ($num < (count($breadcrumbs) - 1)):?>
            <li><a href="<?php echo $breadcrumb['url'];?>"><?php echo $breadcrumb['title'];?></a></li>
        <?php else:?>
            <li class="active"><?php echo $breadcrumb['title'];?></li>
        <?php endif;?>
    <?php endforeach;?>
</ul>