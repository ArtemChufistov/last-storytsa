<?php if (!empty($sameProducts)):?>
    <div class="widget-title">
        <i class="fa fa-check"></i> <?php echo Yii::t('app', 'Похожие товары');?>
    </div>
    <?php foreach($sameProducts as $sameProduct): ?>
        <div class="product light last-sale">
            <figure class="figure-hover-overlay">
                <a href="/product/<?php echo $sameProduct->key;?>"  class="figure-href"></a>
                <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                <img src="<?php echo $sameProduct->pictures;?>" class="img-overlay img-responsive" alt="">
                <img src="<?php echo $sameProduct->pictures;?>" class="img-responsive" alt="">
            </figure>
            <div class="product-caption">
                <div class="block-name">
                    <a rel="nofollow" href="/order/<?php echo $sameProduct->key?>" class="product-name"><?php echo $sameProduct->name;?></a>
                    <p class="product-price"><?php echo $sameProduct->price;?> <?php echo Yii::t('app', 'руб');?></p>
                </div>
                <div class="product-cart">
                    <a href="#"><i class="fa fa-shopping-cart"></i> </a>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>