<?php
$children = $category->children()->all();
if (empty($children)){
    $parentcategory = $category->parent()->one();
}else{
    $parentcategory = $category;
}

if (!empty($parentcategory)){
    $children = $parentcategory->children()->all();
}
?>
<div class="main-category-block ">
    <div class="main-category-title">
        <i class="fa fa-list"></i> <?php echo Yii::t('app', 'Категории');?>
    </div>
</div>
<div class="widget-block">
    <ul class="list-unstyled ul-side-category">
        <?php foreach($children as $child):?>
            <li>
                <a href="/category/<?php echo $child->key;?>" <?php if($category->id == $child->id):?>style="background-color: #eaeaea;"<?php endif;?> >
                    <i class="fa fa-angle-right"></i>
                    <?php echo $child->name;?> <?php $countCategory = $child->children()->count();?> <?php if ($countCategory > 0 ):?>/ <?php echo $countCategory; ?><?php endif;?>
                </a>
                <?php $children2 = $child->children()->all();?>
                <?php if (!empty($children2)):?>
                    <ul class="sub-category">
                        <?php foreach ($children2 as $child2):?>
                        <li><a href=""><?php echo $child->name;?></a>
                            <ul class="sub-category">
                                <li><a href="#">Dress 1</a></li>
                                <li><a href="#">Dress 2</a></li>
                            </ul>
                        </li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
            </li>
        <?php endforeach;?>
    </ul>
</div>