<?php if (!empty($orderProducts)):?>
    <div class="widget-title">
        <i class="fa fa-thumbs-up"></i> <?php echo Yii::t('app', 'Лучшие продажи');?>
    </div>
    <div class="widget-block">
        <?php foreach ($orderProducts as $orderProduct):?>
            <?php $product = $orderProduct->getProduct()->one();?>
            <div class="row">
                <div class="col-md-4">
                    <a href="/product/<?php echo $product->key?>"><img class="img-responsive" src="<?php echo $product->pictures;?>" alt="" title=""></a>
                </div>
                <div class="col-md-8">
                    <div class="block-name">
                        <a href="/product/<?php echo $product->key?>" class="product-name"><?php echo $product->name;?></a>
                        <p class="product-price"><span>$330</span> $<?php echo $product->price;?></p>
                    </div>
                    <div class="product-rating">
                        <div class="stars">
                            <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                        </div>
                        <a href="" class="review">8 Reviews</a>
                    </div>
                    <p class="description">Lorem ipsum dolor.</p>
                </div>
            </div>
        <?php endforeach;?>
    </div>
<?php endif;?>