<?php
use app\modules\news\widgets\LastNewsWidget;
?>
<section>
    <div class="block">
        <div class="container">
            <div class="row">
                <article class="col-md-4">
                    <div class="widget-title">
                        <i class="fa fa-thumbs-up"></i> <?php echo Yii::t('app', 'Последние продажи');?>
                    </div>
                    <div class="widget-block">
                        <div class="row">
                            <div class="col-md-4 col-sm-2 col-xs-3">
                                <img class="img-responsive" src="http://placehold.it/400x500.jpg" alt="" title="">
                            </div>
                            <div class="col-md-8  col-sm-10 col-xs-9">
                                <div class="block-name">
                                    <a href="#" class="product-name">Product name</a>
                                    <p class="product-price"><span>$330</span> $320.99</p>

                                </div>
                                <div class="product-rating">
                                    <div class="stars">
                                        <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                    </div>
                                    <a href="" class="review">8 Reviews</a>
                                </div>
                                <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>
                    <div class="widget-block">
                        <div class="row">
                            <div class="col-md-4 col-sm-2 col-xs-3">
                                <img class="img-responsive" src="http://placehold.it/400x500" alt="" title="">
                            </div>
                            <div class="col-md-8 col-sm-10 col-xs-9">
                                <div class="block-name">
                                    <a href="#" class="product-name">Product name</a>
                                    <p class="product-price"><span>$330</span> $320.99</p>
                                </div>
                                <div class="product-rating">
                                    <div class="stars">
                                        <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                    </div>
                                    <a href="" class="review">8 Reviews</a>
                                </div>
                                <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="col-md-4">
                    <div class="widget-title">
                        <i class="fa fa-user"></i> <?php echo Yii::t('app', 'О магазине');?>
                    </div>
                    <p>
                        <span class="dropcap"><?php echo Yii::t('app', 'Н');?></span><?php echo Yii::t('app', 'Наш магазин создан людьми для людей, чтобы было приятно и безопасно совершать покупки в интернете, а так-же выбирать понравившийся товар для себя и не только. У нас вы найдёте огромный выбор товаров, достаточно выбрать соответствующую категорию или подкатегорию');?>
                    </p>
                    <blockquote>
                        <?php echo Yii::t('app', 'Если вы не нашли в категориях то, что вам нужно, вы можете воспользоваться поиском по товарам');?>
                    </blockquote>
                </article>
                <?php echo LastNewsWidget::widget(['count' => 4, 'view' => '_last-news']);?>
            </div>
        </div>
    </div>
</section>