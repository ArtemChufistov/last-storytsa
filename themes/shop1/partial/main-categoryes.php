<?php
use app\modules\shop\models\Category;
use app\modules\shop\models\Product;
?>
<?php $categoryArray = Category::find()->where(['parent_id' => 0])->all();?>

<section>
    <div class="home-category color-scheme-2">
        <div class="container">
            <div class="row">
                <?php if (!empty($categoryArray[0])): ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <article class="home-category-block">
                            <div class="home-category-title">
                                <i class="fa fa-male"></i> <a href="/category/<?php echo $categoryArray[0]->key;?>"><?php echo $categoryArray[0]->name;?></a> 
                            </div>
                            <div class="home-category-option">
                                <ul class="list-unstyled home-category-list">
                                    <?php foreach($categoryArray[0]->children()->limit(10)->all() as $category):?>
                                        <li><a href="/category/<?php echo $category->key; ?>"><i class="fa fa-check"></i><?php echo $category->name?></a></li>
                                    <?php endforeach;?>
                                </ul>
                                <img src="http://placehold.it/800x800" class="img-responsive" alt="">
                            </div>
                        </article>
                    </div>
                <?php endif;?>
                <?php if (!empty($categoryArray[1])): ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <article class="home-category-block">
                            <div class="home-category-title">
                                <i class="fa fa-female"></i>
                                    <a href="/category/<?php echo $categoryArray[1]->key;?>">
                                        <?php echo $categoryArray[1]->name;?>
                                    </a>
                            </div>
                            <div class="home-category-option">
                                <ul class="list-unstyled home-category-list">
                                    <?php foreach($categoryArray[1]->children()->limit(10)->all() as $category):?>
                                        <li><a href="/category/<?php echo $category->key; ?>"><i class="fa fa-check"></i><?php echo $category->name?></a></li>
                                    <?php endforeach;?>
                                </ul>
                                <img src="http://placehold.it/800x800" class="img-responsive" alt="">

                            </div>
                        </article>
                    </div>
                <?php endif;?>
                <?php if (!empty($categoryArray[2])): ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <article class="home-category-block">
                            <div class="home-category-title">
                                <i class="fa fa-child"></i>
                                <a href="/category/<?php echo $categoryArray[2]->key;?>">
                                    <?php echo $categoryArray[2]->name;?>
                                </a>
                            </div>
                            <div class="home-category-option">
                                <ul class="list-unstyled home-category-list">
                                    <?php foreach($categoryArray[2]->children()->limit(6)->all() as $category):?>
                                        <li><a href="/category/<?php echo $category->key; ?>"><i class="fa fa-check"></i><?php echo $category->name?></a></li>
                                    <?php endforeach;?>
                                </ul>
                                <img src="http://placehold.it/800x800" class="img-responsive" alt="">
                            </div>
                        </article>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="color-scheme-2">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <article class="banner">
                        <a href="#">
                            <img src="http://placehold.it/900x677" class="img-responsive" alt="">
                        </a> 
                    </article>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <article class="banner">
                        <a href="#">
                            <img src="http://placehold.it/900x677" class="img-responsive" alt="">
                        </a> 
                    </article>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <article class="banner">
                        <a href="#">
                            <img src="http://placehold.it/900x677" class="img-responsive" alt="">
                        </a> 
                    </article>
                </div>
            </div>
        </div>  
    </div>
</section>