<?php
use app\components\LayoutComponent;
use app\modules\shop\models\Product;
?>
<section>
    <div class="block color-scheme-2">
        <div class="container" id="main-bestseller">
            <div class="header-for-light">
                <h1 class="wow fadeInRight animated" data-wow-duration="1s">
                    <?php echo Yii::t('app', 'Наши');?> 
                    <span><?php echo Yii::t('app', 'Лучшие продажи');?></span>
                </h1>
                <div class="toolbar-for-light" id="nav-bestseller">
                    <a href="javascript:;" data-role="prev" class="prev"><i class="fa fa-angle-left"></i></a>
                    <a href="javascript:;" data-role="next" class="next"><i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div id="owl-bestseller"  class="owl-carousel">
                <?php foreach(Product::find()->where(['bestseller' => Product::BESTSELLER_TRUE])->all() as $product): ?>
                    <div class="text-center">
                        <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/grid-product'), [
                            'product' => $product,
                        ]); ?>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>