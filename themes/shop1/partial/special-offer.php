<?php
use app\components\LayoutComponent;
use app\modules\shop\models\Product;
?>
<section id="sale-section">
    <div class="block">
        <div class="container" id="special-offer">
            <div class="header-for-light  hidden-xs hidden-sm">
                <h1 class="wow fadeInRight animated" data-wow-duration="1s"><?php echo Yii::t('app', 'Наши');?> <span><?php echo Yii::t('app', 'специальные предложения');?></span></h1>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="title-block light wow fadeInLeft">

                        <h2>Summer sale collection</h2>
                        <h1>Effect</h1>
                        <p>Sed posuere consectetur est at lobortis. Donec ullamcorper nulla non metus auctor fringilla auctor fringilla. </p>
                        <div class="text-center">
                            <div class="toolbar-for-light" id="nav-summer-sale">
                                <a href="javascript:;" data-role="prev" class="prev"><i class="fa fa-angle-left"></i></a>
                                <a href="javascript:;" data-role="next" class="next"><i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div id="owl-summer-sale"  class="owl-carousel">
                        <?php foreach(Product::find()->where(['special' => Product::SPECIAL_TRUE])->all() as $product): ?>
                            <div class="text-center">
                                <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/grid-product'), [
                                    'product' => $product,
                                ]); ?>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>