<article class="product list">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 text-center">
            <figure class="figure-hover-overlay">
                <a href="/product/<?php echo $product->key;?>"  class="figure-href"></a>
                <a href="#" class="product-compare"><i class="fa fa-random"></i></a>
                <a href="#" class="product-wishlist"><i class="fa fa-heart-o"></i></a>
                <img src="<?php echo $product->pictures;?>" class="img-overlay img-responsive" alt="">
                <img src="<?php echo $product->pictures;?>" class="img-responsive" alt="">
            </figure>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="product-caption">
                <div class="block-name">
                    <a href="/product/<?php echo $product->key;?>" class="product-name"><?php echo $product->name;?></a>
                    <p class="product-price"><?php echo $product->price;?> <?php echo Yii::t('app', 'руб');?></p>
                </div>

                <div class="product-rating">
                    <div class="stars">
                        <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                    </div>
                    <a href="" class="review">8 <?php echo Yii::t('app', 'Отзывов');?></a>
                </div>
                <?php if (!empty($product->description)):?>
                    <p class="description"><?php echo mb_substr($product->description, 0, 60);?>...</p>
                <?php endif;?>
                <div class="product-cart">
                    <a rel="nofollow" href="/order/<?php echo $product->key;?>"><i class="fa fa-shopping-cart"></i> <?php echo Yii::t('app', 'Купить');?></a>
                </div>
            </div>
        </div>
    </div>
</article>