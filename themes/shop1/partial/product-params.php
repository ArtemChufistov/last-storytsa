<?php
use app\modules\shop\models\ParamCategory;
?>
<div class="widget-title">
    <i class="fa fa-money"></i> <?php echo Yii::t('app', 'Цена от/до');?>
</div>

<div class="widget-block">
    <div class="row">
        <div class="col-md-6">
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" id="price-from" class="form-control" value="<?php echo $category->min_product_price?>">
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" id="price-to" class="form-control" value="<?php echo $category->max_product_price?>">
            </div>
        </div>
    </div>
</div>

<?php $paramCategoryArray = ParamCategory::find()
    ->where(['in', 'category_id', $productModel->categoryes])
    ->groupBy(['param_id'])
    ->all();
?>

<?php foreach($paramCategoryArray as $paramCategory):?>
    <?php $param = $paramCategory->getParam()->one();?>
    <div class="widget-title">
        <i class="fa fa-dashboard"></i> <?php echo $param->name;?>
    </div>

    <?php $paramValueArray = ParamCategory::find()
        ->where(['in', 'category_id', $productModel->categoryes])
        ->andWhere(['param_id' => $param->id])
        ->groupBy(['value'])
        ->all();?>

    <?php foreach($paramValueArray as $paramValue):?>
        <?php echo $paramValue->value;?>
    <?php endforeach;?>

    <div class="widget-block">
        <ul class="colors clearfix list-unstyled">
            <li><a href="" rel="003d71"></a></li>
            <li><a href="" rel="c42c39"></a></li>
            <li><a href="" rel="f4bc08"></a></li>
            <li><a href="" rel="02882c"></a></li>
            <li><a href="" rel="000000"></a></li>
            <li><a href="" rel="caccce"></a></li>
            <li><a href="" rel="ffffff"></a></li>
            <li><a href="" rel="f9e7b6"></a></li>
            <li><a href="" rel="ef8a07"></a></li>
            <li><a href="" rel="5a433f"></a></li>
        </ul>
    </div>
<?php endforeach; ?>