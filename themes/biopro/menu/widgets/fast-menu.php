<?php
$colorMenuArray = ['info', 'danger', 'purple', 'azure', 'yellow', 'warning'];
?>
<div class="dropdown" id="dashnav-btn">
    <a href="#" data-toggle="dropdown" data-placement="bottom" class="popover-button-header tooltip-button" title="<?php echo Yii::t('app', 'Навигация');?>">
        <i class="glyph-icon icon-linecons-cog"></i>
    </a>
    <div class="dropdown-menu float-right">
        <div class="box-sm">

            <div class="pad5T pad5B pad10L pad10R dashboard-buttons clearfix">
                <?php foreach($menu->children($menu->depth + 1)->all() as $num => $child):?>
                    <?php $randColorNum = array_rand($colorMenuArray, 1);?>
                    <a href="<?= $child->link;?>" class="btn vertical-button remove-border btn-<?php echo $colorMenuArray[$randColorNum];?>" title="">
                        <?= $child->icon;?>
                        <?= str_replace(' ', '</br>', $child->title);?>
                    </a>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
<!--

                <a href="#" class="btn vertical-button remove-border btn-purple" title="">
                    <span class="glyph-icon icon-separator-vertical pad0A medium">
                        <i class="glyph-icon icon-fire opacity-80 font-size-20"></i>
                    </span>
                    Tables
                </a>
                <a href="#" class="btn vertical-button remove-border btn-azure" title="">
                    <span class="glyph-icon icon-separator-vertical pad0A medium">
                        <i class="glyph-icon icon-bar-chart-o opacity-80 font-size-20"></i>
                    </span>
                    Charts
                </a>
                <a href="#" class="btn vertical-button remove-border btn-yellow" title="">
                    <span class="glyph-icon icon-separator-vertical pad0A medium">
                        <i class="glyph-icon icon-laptop opacity-80 font-size-20"></i>
                    </span>
                    Buttons
                </a>
                <a href="#" class="btn vertical-button remove-border btn-warning" title="">
                    <span class="glyph-icon icon-separator-vertical pad0A medium">
                        <i class="glyph-icon icon-code opacity-80 font-size-20"></i>
                    </span>
                    Panels
                </a>
-->