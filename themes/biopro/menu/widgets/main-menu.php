<nav class="navbar fixed-top navbar-toggleable-md navbar-inverse" id="mainNav">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="#page-top">Metanika</a>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <?php foreach ($menu->children()->all() as $num => $children): ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?=$children->link;?>" title="<?=$children->title;?>"><?=$children->title;?></a>
                </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</nav>