<?php
use yii\web\View;
use yii\helpers\Url;
?>
<div style="position:relative" class="cell-lg-2">
  <div class="background-aside-left"></div>
  <ul class="rd-navbar-nav">
    <?php foreach($menu->children($menu->depth + 1)->all() as $num => $child):?>
      <li><a href="<?= $child->link;?>"><?= $child->title;?></a></li>
    <?php endforeach;?>
  </ul>
</div>