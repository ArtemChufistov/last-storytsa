<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php if ($paymentForm->isNewRecord):?>
    <?php $form = ActiveForm::begin(); ?>

    	<p><?php echo Yii::t('app', 'Укажите сумму, на которую хотите пополнить свой личный счёт в кабинете');?></p>

    	<?= $form->field($paymentForm, 'currency_id')->textInput(['readonly' => true, 'value' => $paymentForm->getCurrency()->one()->title]) ?>

        <?= $form->field($paymentForm, 'sum') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>
<?php else:?>
	<?php echo Yii::t('app', 'Для пополнения своего личного счета на сумму: <strong>{sum} {currency}</strong> Вам необходимо перевести указаную сумму на кошелек <strong>{wallet}</strong>',[
		'sum' => $paymentForm->sum,
		'currency' => $paymentForm->getCurrency()->one()->title,
		'wallet' => $paymentForm->wallet,
	])?>

	<p class="margin"><?php echo Yii::t('app', 'Кошелек BitCoin');?></p>
  	<div class="input-group">
		<div class="input-group-btn">
			<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-wallet"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
		</div>
		<input class="form-control" readonly="true" type="text" id = "payment-wallet" value = "<?php echo $paymentForm->wallet;?>">
	</div>
	</br>
	<p class="margin"><?php echo Yii::t('app', 'Сумма BTC');?></p>
  	<div class="input-group">
		<div class="input-group-btn">
			<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-sum"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
		</div>
		<input class="form-control" readonly="true" type="text" id = "payment-sum" value = "<?php echo $paymentForm->sum;?>">
	</div>
	<br><p class="margin"><?php echo Yii::t('app', 'Или вы можете отсканировать QR код:');?></p>
	<img class = "pull-left" src = "/profile/office/qrcode/<?php echo $paymentForm->wallet;?>/<?php echo $paymentForm->sum;?>">
<?php endif;?>