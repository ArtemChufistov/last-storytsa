<?php
use app\modules\finance\models\Currency;

?>
<div class="row">
    <div class="col-lg-3">
        <div class="dummy-logo">
            <strong style = "font-size: 30px;">CITT</strong>
            </br>
            </br>
            <p style = "font-size: 14px;">DIGITAL ASSETS MANAGMENT</p>
        </div>
    </div>
    <div class="col-lg-5 float-left text-left">
		<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Информация'); ?>:</h2>
		<ul class="reset-ul">
			<li>
				<b><?php echo Yii::t('app', 'Количество CITT'); ?>:</b> <strong style = "font-size: 22px;"> <?php echo $paymentForm->to_sum; ?></strong>
			</li>
			<li>
				<b><?php echo Yii::t('app', 'Сумма к оплате'); ?>:</b>
				<strong style = "font-size: 22px;"><?php echo number_format($paymentForm->fromSumWithComission(), 6); ?> <?php echo $paymentForm->getFromCurrency()->one()->key; ?></strong>
			</li>

			<li>
				<b><?php echo Yii::t('app', 'Кошелёк для оплаты'); ?>:</b></br></br> <strong style = "font-size: 22px;"> <?php echo $paymentForm->wallet; ?></strong>
			</li>
		</ul>
    </div>
    <div class="col-md-4">
		<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Дополнительная информация'); ?>:</h2>
		<p><?php echo Yii::t('app', 'Для покупки <strong>{countCitt} CITT</strong> вам необходимо перевести <strong>{sum} {currency}</strong> на кошелёк: <strong>{wallet}</strong>', [
	'countCitt' => $paymentForm->to_sum,
	'sum' => number_format($paymentForm->fromSumWithComission(), 6),
	'currency' => $paymentForm->getFromCurrency()->one()->key,
	'wallet' => $paymentForm->wallet,
]); ?></p>
		</br>
		<?php if ($paymentForm->getFromCurrency()->one()->key == Currency::KEY_BTC): ?>
			<?php echo Yii::t('app', 'Для удобства перевода вы можете воспользоваться QR кодом:'); ?>
			<img style = "width: 200px; float: left;" src = "/profile/office/qrcode/<?php echo $paymentForm->wallet; ?>/<?php echo $paymentForm->fromSumWithComission(); ?>">
		<?php endif;?>

	</div>
</div>
