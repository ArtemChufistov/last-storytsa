<div id="page-content-wrapper">
  <div id="page-content">
    <div class="container">
      <div id="page-title">
          <h2><?php echo $this->title; ?></h2>
          <p><?php echo Yii::t('app', 'Здесь вы можете обменять <strong>CITT</strong>'); ?></p>
      </div>
      <?php echo Yii::$app->controller->renderPartial(
	'@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_' . strtolower(\yii\helpers\StringHelper::basename(get_class($paymentForm))),
	['paymentForm' => $paymentForm, 'user' => $user, 'currencyArray' => $currencyArray]); ?>
    </div>
  </div>
</div>