<?php
use app\modules\finance\models\CryptoWallet;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyGraph;
use app\modules\finance\models\Payment;
use app\modules\finance\models\Transaction;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Панель управления');
$this->params['breadcrumbs'][] = $this->title;

$currencyVoucher = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();

?>
<style>
.highcharts-credits{
  display: none;
}
</style>

<div id="page-content-wrapper">
  <div id="page-content">
    <div class="container">

      <div id="page-title">
          <h2><?php echo $this->title; ?></h2>
          <p><?php echo Yii::t('app', 'Сводная информация о ваших финансах'); ?></p>
      </div>

      <div class="row">
          <div class="col-lg-5">
              <div class="dashboard-box dashboard-box-chart bg-white content-box">
                  <div class="content-wrapper">
                      <div class="header">

                        <p style = "font-size: 16px;">
                          <i class="glyph-icon icon-bar-chart"></i> <?php echo Yii::t('app', 'Текущий курс CITT: <strong>{curs}$</strong>',
	['curs' => $currencyVoucher->course_to_usd]); ?>
                        </p>
                      </div>
                      <?php $graph30MinLastDay = CurrencyGraph::find()
	->where(['type' => CurrencyGraph::TYPE_30_MINUTE])
	->andWhere(['currency_id' => $currencyVoucher->id])
	->andWhere(['>=', 'date', date('Y-m-d H:i:s', time() - 60 * 60 * 24)])
	->orderBy(['date' => SORT_ASC])
	->all();?>

                      <?php
$xaxis30MinLastDay = [];
$yaxis30MinLastDay = [];
foreach ($graph30MinLastDay as $item) {
	$xaxis30MinLastDay[] = date('H:i', strtotime($item->date));
	$yaxis30MinLastDay[] = $item->value;
}

$curs30MinLastDay = $yaxis30MinLastDay[count($yaxis30MinLastDay) - 1] - $yaxis30MinLastDay[0];
?>

                      <div class="bs-label bg-<?php if ($curs30MinLastDay > 0): ?>green<?php else: ?>red<?php endif;?>">
                        <?php if ($curs30MinLastDay > 0): ?>
                          +<?php echo $curs30MinLastDay; ?>$
                        <?php else: ?>
                          <?php echo $curs30MinLastDay; ?>$
                        <?php endif;?>
                      </div>

                      <?php echo Highcharts::widget([
	'options' => [
		'chart' => [
			'type' => 'spline',
		],
		'title' => [
			'text' => '',
		],
		'subtitle' => [
			'text' => Yii::t('app', 'Статистика за 24 часа'),
		],
		'xAxis' => [
			'categories' => $xaxis30MinLastDay,
		],
		'yAxis' => [
			'title' => [
				'text' => Yii::t('app', 'Цена'),
			],
		],
		'plotOptions' => [
			'spline' => [
				'marker' => [
					'enabled' => true,
				],
				'color' => '#29b765',
				'animation' => true,
			],
		],
		'legend' => [
			'enabled' => false,
		],
		'series' => [[
			'name' => Yii::t('app', 'Цена CITT $'),
			'data' => $yaxis30MinLastDay,
		],
		],
	]]);
?>
                  </div>
                  <div class="button-pane">
                      <div class="size-md float-left">
                        <?php echo Yii::t('app', 'Итого изменение курса за 24 часа на: <strong>{sum}$</strong>',
	['sum' => $curs30MinLastDay]);
?>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-lg-5">
              <div class="dashboard-box dashboard-box-chart bg-white content-box">
                <div class="content-wrapper">
                      <div class="header">
                        <p style = "font-size: 16px;">
                          <i class="glyph-icon icon-bar-chart"></i> <?php echo Yii::t('app', 'Каждые 4 часа'); ?>
                        </p>
                      </div>

                      <?php $graph2hLast7Day = CurrencyGraph::find()
	->where(['type' => CurrencyGraph::TYPE_4_HOURE])
	->andWhere(['currency_id' => $currencyVoucher->id])
	->andWhere(['>=', 'date', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 7)])
	->orderBy(['date' => SORT_ASC])
	->all();?>

                      <?php
$xaxis2hLast7Day = [];
$yaxis2hLast7Day = [];
foreach ($graph2hLast7Day as $item) {
	$xaxis2hLast7Day[] = date('H:i d-m', strtotime($item->date));
	$yaxis2hLast7Day[] = $item->value;
}

$curs2hLast7Day = $yaxis2hLast7Day[count($yaxis2hLast7Day) - 1] - $yaxis2hLast7Day[0];
?>
                      <div class="header">

                      </div>
                      <div class="bs-label bg-<?php if ($curs2hLast7Day > 0): ?>green<?php else: ?>red<?php endif;?>">
                        <?php if ($curs2hLast7Day > 0): ?>
                          +<?php echo $curs2hLast7Day; ?>$
                        <?php else: ?>
                          <?php echo $curs2hLast7Day; ?>$
                        <?php endif;?>
                      </div>

                      <?php echo Highcharts::widget([
	'options' => [
		'chart' => [
			'type' => 'spline',
		],
		'title' => [
			'text' => '',
		],
		'subtitle' => [
			'text' => Yii::t('app', 'Статистика за 7 дней'),
		],
		'xAxis' => [
			'categories' => $xaxis2hLast7Day,
		],
		'yAxis' => [
			'title' => [
				'text' => Yii::t('app', 'Цена'),
			],
		],
		'plotOptions' => [
			'spline' => [
				'marker' => [
					'enabled' => true,
				],
				'color' => '#308dcc',
				'animation' => true,
			],
		],
		'legend' => [
			'enabled' => false,
		],
		'series' => [[
			'name' => Yii::t('app', 'Цена CITT $'),
			'data' => $yaxis2hLast7Day,
		],
		],
	]]);
?>
                  </div>
                  <div class="button-pane">
                      <div class="size-md float-left">
                        <?php echo Yii::t('app', 'Итого изменение курса за 7 дней на: <strong>{sum}$</strong>',
	['sum' => $curs2hLast7Day]);
?>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-lg-2">
              <div class="dashboard-box dashboard-box-chart bg-white content-box">
                  <div class="content-wrapper">
                      <div class="header">
                          <span><?php echo Yii::t('app', 'Ваш баланс: '); ?></span>
                          <?php foreach ($user->getBalances() as $balance): ?>
                            <p style = "text-align: center;"><?php echo $balance->showValue(); ?> <?php echo $balance->getCurrency()->one()->title; ?></p>
                          <?php endforeach;?>
                      </div>
                      <div class="example-box-wrapper">
                        <a href = "<?php echo Url::to(['office/in']) ?>" class="btn btn-success btn-lg btn-block" type="button"><?php echo Yii::t('app', 'Получить CITT'); ?></a>
                        <a href = "<?php echo Url::to(['office/out']) ?>" class="btn btn-purple btn-lg btn-block" type="button"><?php echo Yii::t('app', 'Обменять CITT'); ?></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
        <div class="row">
            <div class="col-lg-8">
              <div class="dashboard-box dashboard-box-chart bg-white content-box">
                <div class="content-wrapper">

                      <div class="header">
                        <p style = "font-size: 16px;">
                          <i class="glyph-icon icon-bar-chart"></i> <?php echo Yii::t('app', 'Каждый день'); ?>
                        </p>
                      </div>

                      <?php $graph1dLast30Day = CurrencyGraph::find()
	->where(['type' => CurrencyGraph::TYPE_1_DAY])
	->andWhere(['currency_id' => $currencyVoucher->id])
	->andWhere(['>=', 'date', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 30)])
	->orderBy(['date' => SORT_ASC])
	->all();?>

                      <?php
$xaxis1dLast30Day = [];
$yaxis1dLast30Day = [];
foreach ($graph1dLast30Day as $item) {
	$xaxis1dLast30Day[] = date('d-m', strtotime($item->date));
	$yaxis1dLast30Day[] = $item->value;
}

$curs1dLast30Day = $yaxis1dLast30Day[count($yaxis1dLast30Day) - 1] - $yaxis1dLast30Day[0];
?>
                      <div class="header">

                      </div>
                      <div class="bs-label bg-<?php if ($curs1dLast30Day > 0): ?>green<?php else: ?>red<?php endif;?>">
                        <?php if ($curs1dLast30Day > 0): ?>
                          +<?php echo $curs1dLast30Day; ?>$
                        <?php else: ?>
                          <?php echo $curs1dLast30Day; ?>$
                        <?php endif;?>
                      </div>

                      <?php echo Highcharts::widget([
	'options' => [
		'chart' => [
			'type' => 'spline',
		],
		'title' => [
			'text' => '',
		],
		'subtitle' => [
			'text' => Yii::t('app', 'Статистика за 30 дней'),
		],
		'xAxis' => [
			'categories' => $xaxis1dLast30Day,
		],
		'yAxis' => [
			'title' => [
				'text' => Yii::t('app', 'Цена'),
			],
		],
		'plotOptions' => [
			'spline' => [
				'marker' => [
					'enabled' => true,
				],
				'color' => '#7a3ecc',
				'animation' => true,
			],
		],
		'legend' => [
			'enabled' => false,
		],
		'series' => [[
			'name' => Yii::t('app', 'Цена CITT $'),
			'data' => $yaxis1dLast30Day,
		],
		],
	]]);
?>
                  </div>
                  <div class="button-pane">
                      <div class="size-md float-left">
                        <?php echo Yii::t('app', 'Итого изменение курса за 30 дней на: <strong>{sum}$</strong>',
	['sum' => $curs1dLast30Day]);
?>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-4">
          <div class="panel">
              <div class="panel-body">
                  <h3 class="title-hero">
                      <i class="glyph-icon icon-tachometer"></i> <?php echo Yii::t('app', 'Суммарная информация'); ?>
                  </h3>
                  <div class="example-box-wrapper">
                      <div class="timeline-box timeline-box-left">
                          <div class="tl-row">
                              <div class="tl-item float-right">
                                  <div class="tl-icon bg-red">
                                      <i class="glyph-icon icon-bank"></i>
                                  </div>
                                  <div class="popover left">
                                      <div class="arrow"></div>
                                      <div class="popover-content">
                                        <?php $summSitt = Transaction::find()->where(['not', ['to_user_id' => null]])->sum('sum');?>
                                        <div class="tl-label bs-label label-info" style = "font-size: 20px; font-weight: bold;"><?php echo round($summSitt, 3); ?></div>
                                        <p class="tl-content"><?php echo Yii::t('app', 'Всего выпущено CITT') ?> </p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="tl-row">
                              <div class="tl-item float-right">
                                  <div class="tl-icon bg-primary">
                                      <i class="glyph-icon icon-bars"></i>
                                  </div>
                                  <div class="popover left">
                                      <div class="arrow"></div>
                                      <div class="popover-content">
                                        <?php $summSittInUsd = Payment::find()->where(['not', ['to_user_id' => null]])->andWhere(['status' => Payment::STATUS_OK])->sum('from_sum * course_to_usd');?>
                                        <div class="tl-label bs-label label-primary" style = "font-size: 20px; font-weight: bold;"><?php echo round($summSittInUsd, 3); ?></div>
                                        <p class="tl-content"><?php echo Yii::t('app', 'Потрачено на покупку CITT'); ?> </p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="tl-row">
                              <div class="tl-item float-right">
                                  <div class="tl-icon bg-black">
                                      <i class="glyph-icon icon-usd"></i>
                                  </div>
                                  <div class="popover left">
                                      <div class="arrow"></div>
                                      <div class="popover-content">
                                        <?php $saveSum = 0;?>
                                        <?php foreach (CryptoWallet::find()->all() as $cryptoWallet): ?>
                                          <?php $saveSum += $cryptoWallet->balance * $cryptoWallet->getCurrency()->one()->course_to_usd;?>
                                        <?php endforeach;?>
                                        <div class="tl-label bs-label bg-green" style = "font-size: 20px; font-weight: bold;"><?php echo round($saveSum, 2); ?></div>
                                        <p class="tl-content"><?php echo Yii::t('app', 'Сумма хранимых средств в USD'); ?></p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs('
$(function() {

    var cos = [];

    for (var i = 0; i < 354; i += 31) {
        cos.push([i, Math.random(i)]);
    }

    var plot = $.plot($("#data-example-1"),
        [{ data: cos}], {
            series: {
                shadowSize: 0,
                lines: {
                    show: true,
                    lineWidth: 2
                },
                points: { show: true }
            },
            grid: {
                labelMargin: 10,
                hoverable: true,
                clickable: true,
                borderWidth: 1,
                borderColor: \'rgba(82, 167, 224, 0.06)\'
            },
            legend: {
                backgroundColor: \'#fff\'
            },
            yaxis: { tickColor: \'rgba(0, 0, 0, 0.06)\', font: {color: \'rgba(0, 0, 0, 0.4)\'}},
            xaxis: { tickColor: \'rgba(0, 0, 0, 0.06)\', font: {color: \'rgba(0, 0, 0, 0.4)\'}},
            colors: [getUIColor(\'success\'), getUIColor(\'gray\')],
            tooltip: true,
            tooltipOpts: {
                content: "цена: %x, y: %y"
            }
        });

    $("#data-example-1").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));
    });
});
');?>
