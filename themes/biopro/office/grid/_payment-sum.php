<?php if ($model->from_user_id == \Yii::$app->user->identity->id):?>
	<span class="badge bg-red">-<?php echo $model->realSum;?> <?php echo $model->getCurrency()->one()->title;?></span>
<?php else:?>
	<span class="badge bg-green">+<?php echo $model->realSum;?> <?php echo $model->getCurrency()->one()->title;?></span>
<?php endif;?>