<?php
use app\assets\biopro\FrontAppAssetBottom;
use app\assets\biopro\FrontAppAssetTop;
use app\modules\menu\widgets\MenuWidget;
use yii\helpers\Html;

FrontAppAssetTop::register($this);
FrontAppAssetBottom::register($this);

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="en">
<head>

    <?=Html::csrfMetaTags()?>
    <title><?=Html::encode($this->title)?></title>
    <?php $this->head()?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_head_meta'); ?>
</head>

<body id="page-top">
    <?php $this->beginBody()?>

    <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']); ?>

    <?=$content?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_footer'); ?>

    <?php $this->endBody()?>
</body>
</html>
<?php $this->endPage()?>