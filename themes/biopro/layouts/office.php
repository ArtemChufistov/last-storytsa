<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\modules\menu\widgets\MenuWidget;
use app\assets\cryptofund\BackAppAssetTop;
use app\assets\cryptofund\BackAppAssetBottom;
use app\modules\profile\widgets\ChangeEmailWidget;

BackAppAssetTop::register($this);
BackAppAssetBottom::register($this);
?>

<?php $this->beginPage() ?>

<html>
<head>
  <meta charset="UTF-8">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>
</head>
<body class="hold-transition skin-purple sidebar-mini">
<?php $this->beginBody() ?>
  <div id="sb-site">
    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <div id="page-wrapper">
      <div id="page-header" class="bg-gradient-9">
        <div id="mobile-navigation">
            <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
            <a href="index.html" class="logo-content-small" title="Sitt.io"></a>
        </div>
        <div id="header-logo" class="logo-bg">
            <a href="/" class="logo-content-big" title="Citt.io">
                Sitt <i>Io</i>
                <span><?php echo Yii::t('app', 'Фонд');?></span>
            </a>
            <a href="/" class="logo-content-small" title="Citt.io">
                Sitt <i>Io</i>
                <span><?php echo Yii::t('app', 'Фонд');?></span>
            </a>
            <a id="close-sidebar" href="#" title="<?php echo Yii::t('app', 'Закрыть панель');?>">
                <i class="glyph-icon icon-angle-left"></i>
            </a>
        </div>
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_left-nav');?>
        <div id="header-nav-right">
            <a href="#" class="hdr-btn" id="fullscreen-btn" title="<?php echo Yii::t('app', 'Во весь экран');?>">
                <i class="glyph-icon icon-arrows-alt"></i>
            </a>
            
            <a class="header-btn" href="<?php echo Url::to(['office/dashboard']);?>" title="<?php echo Yii::t('app', 'Статистика CITT');?>">
                <i class="glyph-icon icon-linecons-star"></i>
            </a>

            <?php echo MenuWidget::widget(['menuName' => 'rightLkMenu', 'view' => 'fast-menu']);?>

            <a class="header-btn" id="logout-btn" href="/logout" title="<?php echo Yii::t('app', 'Выход');?>">
                <i class="glyph-icon icon-linecons-lock"></i>
            </a>
        </div>
      </div>
      <div id="page-sidebar" class = "bg-black font-inverse">
        <div class="slimScrollDiv">
          <?php echo MenuWidget::widget(['menuName' => 'profileMenu', 'view' => 'office-menu']);?>
        </div>
      </div>
      <?php echo $content;?>
  </div>
</div>
<?= ChangeEmailWidget::widget(['user' => $this->params['user']->identity]) ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>