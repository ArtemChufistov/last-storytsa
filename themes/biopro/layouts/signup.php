<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\menu\widgets\MenuWidget;
use app\assets\cryptofund\BackAppAssetTop;
use app\assets\cryptofund\BackAppAssetBottom;

BackAppAssetTop::register($this);
BackAppAssetBottom::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <?= Html::csrfMetaTags() ?>
      <title><?= Html::encode($this->title) ?></title>
      <?php $this->head() ?>

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>
  </head>
  <body>
    <?php $this->beginBody() ?>
      <div id="loading">
          <div class="spinner">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
          </div>
      </div>

      <style type="text/css">
          html,body {
              height: 100%;
          }
      </style>

      <script type="text/javascript">
          wow = new WOW({
              animateClass: 'animated',
              offset: 100
          });
          wow.init();
      </script>

      <img src="/cryptofund/image-resources/blurred-bg/blurred-bg-3.jpg" class="login-img wow fadeIn" alt="">

      <?= $content ?>

    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>