<?php
use yii\helpers\Url;
?>
<div id="header-nav-left">
    <div class="user-account-btn dropdown">
        <a href="#" title="<?php echo Yii::t('app', 'Мой аккаунт')?>" class="user-profile clearfix" data-toggle="dropdown">
            <img width="28" src="<?php echo $this->params['user']->identity->getImage();?>" alt="<?php echo Yii::t('app', 'Ваше изобращение');?>">
            <span><?php echo $this->params['user']->identity->login;?></span>
            <?php foreach($this->params['user']->identity->getBalances() as $balance):?>
                <span>
                    <?php echo $balance->showValue();?> <?php echo $balance->getCurrency()->one()->title; ?>
                </span>
            <?php endforeach;?>
            <i class="glyph-icon icon-angle-down"></i>
        </a>
        <div class="dropdown-menu float-left">
            <div class="box-sm">
                <div class="login-box clearfix">
                    <div class="user-img">
                        <a href="<?php echo Url::to(['office/index']);?>" title="" class="change-img"><?php echo Yii::t('app', 'Сменить фото');?></a>
                        <img src="<?php echo $this->params['user']->identity->getImage();?>" alt="<?php echo $this->params['user']->identity->login;?>">
                    </div>
                    <div class="user-info">
                        <span>
                            <?php echo $this->params['user']->identity->first_name;?> <?php echo $this->params['user']->identity->last_name;?>
                            <i><?php echo Yii::t('app', 'Участник проекта');?></i>
                        </span>
                        <a href="<?php echo Url::to(['office/index']);?>" title="Edit profile"><?php echo Yii::t('app', 'О себе');?></a>
                        <a href="<?php echo Url::to(['office/dashboard']);?>" title="View notifications"><?php echo Yii::t('app', 'Статистика CITT');?></a>
                    </div>
                </div>
                <div class="divider"></div>
                <ul class="reset-ul mrg5B">
                    <li>
                        <a href="<?php echo Url::to(['office/in']);?>">
                            <?php echo Yii::t('app', 'Ваш баланс:');?>
                        </a>
                    </li>
                  <?php foreach($this->params['user']->identity->getBalances() as $balance):?>
                    <li>
                        <a href="<?php echo Url::to(['office/in']);?>">
                            <i class="glyph-icon float-right icon-caret-right"></i>
                            <?php echo $balance->showValue();?> <?php echo $balance->getCurrency()->one()->title; ?>
                        </a>
                    </li>
                  <?php endforeach;?>
                </ul>
                <div class="pad5A button-pane button-pane-alt text-center">
                    <a href="<?php echo Url::to(['office/logout']);?>" class="btn display-block font-normal btn-danger">
                        <i class="glyph-icon icon-power-off"></i>
                        <?php echo Yii::t('app', 'Выход');?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>