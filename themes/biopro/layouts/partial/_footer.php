<?php
?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright"><?php echo Yii::t('app', 'Copyright &copy; metanika.io 2017'); ?></span>
            </div>

        </div>
    </div>
</footer>