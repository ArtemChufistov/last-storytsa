<?php

$this->title = $model->title;
?>

<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in"><?php echo Yii::t('app', 'Научно-технологический производственный комплекс биотехнологии белка из метана'); ?></div>
            <div class="intro-heading"><?php echo Yii::t('app', 'БИОПРОТЕИН ИЗ МЕТАНА'); ?></div>
            <a href="#services" class="btn btn-xl"><?php echo Yii::t('app', 'Узнать больше'); ?></a>
        </div>
    </div>
</header>

<style type="text/css">
    .img-fluid{
        cursor: pointer;

    }
    .img-fluid:hover{
        opacity: 0.7;
    }
</style>

<!-- Services -->
<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><?php echo Yii::t('app', 'ЦЕЛЬ ПРОЕКТА'); ?></h2>
                <h3 class="section-subheading text-muted"><?php echo Yii::t('app', 'Создание кластера  биотехнологии синтеза белка из газа метана'); ?></h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-cogs fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading"><?php echo Yii::t('app', 'Внедрение'); ?></h4>
                <p class="text-muted"><?php echo Yii::t('app', 'Внедрение собственных ноу-хау в технологию биосинтеза белка из метана.'); ?></p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-cubes fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading"><?php echo Yii::t('app', 'Производство'); ?></h4>
                <p class="text-muted"><?php echo Yii::t('app', 'Организация собственного производства и сбыта белка на мировом рынке.'); ?></p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-arrows-alt fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading"><?php echo Yii::t('app', 'Масштабность'); ?></h4>
                <p class="text-muted"><?php echo Yii::t('app', 'Серийное изготовление заводов и оборудования по производству белка'); ?></p>
            </div>
        </div>
    </div>
</section>

<!-- Portfolio Grid -->
<section class="bg-faded" id="Portfolio">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center" id = "howitworks">
                <h2 class="section-heading">Наша бизнес модель</h2>
                <h3 class="section-subheading text-muted"><?php echo Yii::t('app', 'Планы развития и анализ рынка.'); ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 portfolio-item">
                <div class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">

                        </div>
                    </div>
                    <img class="img-fluid" src="biopro/img/portfolio/1.jpg" alt="">
                </div>
                <div class="portfolio-caption">
                    <h4>Мировой рынок</h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'оценивается в 460 млд $'); ?></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <div class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">

                        </div>
                    </div>
                    <img class="img-fluid" src="biopro/img/portfolio/2.jpg" alt="">
                </div>
                <div class="portfolio-caption">
                    <h4>Производство</h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'даст колоссальную прибыль'); ?></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <div class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">

                        </div>
                    </div>
                    <img class="img-fluid" src="biopro/img/portfolio/3.jpg" alt="">
                </div>
                <div class="portfolio-caption">
                    <h4>Описание продукта</h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'биопротеин ТУ-11249895-12-09-92'); ?></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <div class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">

                        </div>
                    </div>
                    <img class="img-fluid" src="biopro/img/portfolio/4.jpg" alt="">
                </div>
                <div class="portfolio-caption">
                    <h4>Экологически чист</h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Биопротеин из метана не токсичен'); ?></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <div class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">

                        </div>
                    </div>
                    <img class="img-fluid" src="biopro/img/portfolio/5.jpg" alt="">
                </div>
                <div class="portfolio-caption">
                    <h4>Маркетинговый анализ</h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'обладает значительным потенциалом'); ?></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <div class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">

                        </div>
                    </div>
                    <img class="img-fluid" src="biopro/img/portfolio/6.jpg" alt="">
                </div>
                <div class="portfolio-caption">
                    <h4>Резюме</h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'инвесторы  получат высокий доход.'); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- About -->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><?php echo Yii::t('app', 'Этапы и периоды реализации проекта '); ?></h2>
                <h3 class="section-subheading text-muted"><?php echo Yii::t('app', 'Все этапы выполняются одновременно. Общий срок 24 месяца'); ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="biopro/img/about/1.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4><?php echo Yii::t('app', 'Бюджет'); ?></h4>
                                <h4 class="subheading"><?php echo Yii::t('app', '100 000 $'); ?></h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted"><?php echo Yii::t('app', 'Этап 1. Подача заявок на изобретения и патентование, всего – не менее 15.'); ?></p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="biopro/img/about/2.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4><?php echo Yii::t('app', 'Бюджет'); ?></h4>
                                <h4 class="subheading"><?php echo Yii::t('app', '500 000 $'); ?></h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted"><?php echo Yii::t('app', 'Этап 2. Изготовление лабораторной установки биосинтеза белка из метана, срок (5-6 мес.)'); ?></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="biopro/img/about/3.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4><?php echo Yii::t('app', 'Бюджет'); ?></h4>
                                <h4 class="subheading"><?php echo Yii::t('app', '3 000 000 $'); ?></h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted"><?php echo Yii::t('app', 'Этап 3. Изготовление пилотной  промышленной установки и мобильного комплекса  биоситнеза белка из метана , с демонстрацией заказчикам (16-18 мес.).'); ?></p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="biopro/img/about/4.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4><?php echo Yii::t('app', 'Бюджет'); ?></h4>
                                <h4 class="subheading"><?php echo Yii::t('app', '8 700 000 $'); ?></h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted"><?php echo Yii::t('app', 'Этап 4. Строительство Научно-технологического производственного комплекса биотехнологии белка из метана (НТПК).'); ?></p>
                            </div>
                        </div>
                    </li>
                     <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="biopro/img/about/5.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4><?php echo Yii::t('app', 'Бюджет'); ?></h4>
                                <h4 class="subheading"><?php echo Yii::t('app', '34 700 000 '); ?></h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted"><?php echo Yii::t('app', 'Этап 5. Строительство завода по производству биопротеина мощностью 12 500 тонн  белка в год. 24 мес.'); ?></p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4><?php echo Yii::t('app', 'Будь частью
                                <br>Нашей
                                <br>Истории!'); ?></h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Team -->
<section class="bg-faded" id="team">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center" id = "about-block">
                <h2 class="section-heading"><?php echo Yii::t('app', 'Наша команда'); ?></h2>
                <h3 class="section-subheading text-muted"><?php echo Yii::t('app', 'В нашей современной команде представлены ученые и специалисты,  непосредственно работавшие над этим проектом долгое время.'); ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t1.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Егоров Иван'); ?></h4>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t2.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Солошенко Владимир'); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Академик РАН, доктор сельскохозяйственных наук, профессор.'); ?></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t3.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Афанасьев Валерий'); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Доктор технических наук, профессор. Президент «Союза Комбикормщиков».'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t4.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Гальченко Валерий '); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Член-корреспондент РАН, доктор биологических наук.'); ?></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t5.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Самойленко Владимир '); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Кандидат биологических наук. Заведующий  Центром биотехнологии.'); ?></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t6.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Ванагс  Юрис'); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Доктор инженерных наук. Директор JSC «Biotehniskais».'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t7.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Лень Вячеслав '); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'СЕО. Стратегическое и оперативное управление компанией. '); ?></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t8.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Глухих Сергей'); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Научный руководитель проекта. Биотехнолог.'); ?></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t9.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Рослякова Юлия '); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Начальник отдела продаж. Специалист по продвижению продуктов.'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t11.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Шищенко Александр'); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Финансовый Директор. Экономист, инвестиционный управляющий. '); ?></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="biopro/img/team/t12.jpg" alt="">
                    <h4><?php echo Yii::t('app', 'Кутафин Николай'); ?></h4>
                    <p class="text-muted"><?php echo Yii::t('app', 'Коммерческий Директор. Коммерциализация разработок .'); ?></p>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <p class="large text-muted"><?php echo Yii::t('app', 'Мы хотим на имеющемся у нас богатом опыте создать уникальную технологию и оборудование следующего поколения в биотехнологической природоподобной отрасли – метанотрофии.'); ?></p>
            </div>
        </div>
    </div>
</section>

<!-- Clients -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <a href="#">
                    <img class="img-fluid d-block mx-auto" src="biopro/img/logos/CryptoBazaar.jpg" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="#">
                    <img class="img-fluid d-block mx-auto" src="biopro/img/logos/skolkovotec.jpg" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="#">
                    <img class="img-fluid d-block mx-auto" src="biopro/img/logos/evatech.jpg" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="#">
                    <img class="img-fluid d-block mx-auto" src="biopro/img/logos/ethereum.jpg" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<!-- Contact -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center" id = "invest-block">
                <h2 class="section-heading"><?php echo Yii::t('app', 'Инвестировать'); ?></h2>
                <h3 class="section-subheading" style = "color: #fed136;"><?php echo Yii::t('app', 'Для того чтобы увидеть адрес Ethereum для инвестиций вы должны подтвердить согласие по всем пунктам соглашений и нажать кнопку инвестировать.'); ?></h3>
            </div>
        </div>
<div class="container" id = "investContainer">
    <div class="row">
        <div class="col-lg-6">
            <div class="input-group">
                <span class="input-group-addon beautiful">
                    <input type="checkbox" id = "checkboxAgree1">
                </span>
                <span  class="form-control"><?php echo Yii::t('app', 'Нажмите здесь, чтобы подтвердить, что вы не являетесь гражданином США, резидентом или юридическим лицом, и вы не покупаете или не подписываете от имени гражданина США'); ?></span>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="input-group">
                <span class="input-group-addon beautiful">
                    <input type="checkbox" id = "checkboxAgree2">
                </span>
                <span  class="form-control"><?php echo Yii::t('app', 'Нажмите здесь, чтобы подтвердить что вы прочитали, поняли и согласились с <a style = "color: #292b2c; font-weight: bold;" target = "_blank" href = "/biopro/Tokens_MtCoin_PURCHASE_AGREEMENT.pdf">MTCOIN Purchase Agreement</a>'); ?></span>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col-lg-6">
            <div class="input-group">
                <span class="input-group-addon beautiful">
                    <input type="checkbox" id = "checkboxAgree3">
                </span>
                <span  class="form-control"><?php echo Yii::t('app', 'Нажмите здесь, чтобы подтвердить что вы прочитали и поняли <a style = "color: #292b2c; font-weight: bold;" target = "_blank" href = "/biopro/Mtcoin_wp_v.0.1_ru.pdf">metanika.io technical White Papper</a>'); ?></span>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="input-group">
                <span class="input-group-addon beautiful">
                    <input type="checkbox" id = "checkboxAgree4">
                </span>
                <span  class="form-control"><?php echo Yii::t('app', 'Нажмите здесь, чтобы подтвердить что вы прочитали и поняли <a style = "color: #292b2c; font-weight: bold;" target = "_blank" href = "/biopro/Terms_of_Use_Metanika.pdf">metanika.io Term of Use</a>'); ?></span>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div classs = "row" style="margin-top: 20px;">
        <div class="col-lg-12 text-center">
            <div id="success"></div>
            <button class="btn btn-xl" type="button" id = "investButton" style = "cursor: pointer;"><?php echo Yii::t('app', 'Инвестировать'); ?></button>
        </div>
        <div class="col-lg-12 text-center" id = "errorsCheckbox" style = "color: red; display: none;">
            <?php echo Yii::t('app', 'Необходимо согласиться со всеми пукнтами'); ?>
        </div>
    </div>

</div>
    <div class = "container" id = "walletContainer" style = "display:none;">
        <div classs = "row">
            <div class="col-lg-12 text-center" style = "color: #fed136;">
                <?php echo Yii::t('app', 'Вам необходимо перевести эфиры на адрес:'); ?></br>
                <h3 class = "wallett" style = "color: #fed136;">3Hb6y6DpEsZp24w61EZbuyvJYfD94orRbS</h3>
            </div>
        </div>
    </div>
    </div>
</section>
<style type="text/css">
    .wallett::selection {
background: #ffb7b7;
}
</style>

<script type="text/javascript">
    $('#investButton').click(function(){

        if ($('#checkboxAgree1').is(':checked') ){
            if ($('#checkboxAgree2').is(':checked') ){
                if ($('#checkboxAgree3').is(':checked') ){
                    if ($('#checkboxAgree4').is(':checked') ){
                        $('#investContainer').hide();
                        $('#walletContainer').show();
                    }else{
                        console.log(4);
                        $('#errorsCheckbox').show();
                    }
                }else{
                    console.log(3);
                    $('#errorsCheckbox').show();
                }
            }else{
                console.log(2);
                $('#errorsCheckbox').show();
            }
        }else{
            console.log(1);
            $('#errorsCheckbox').show();
        }
        console.log('+');
        //console.log($('#checkboxAgree2').attr("checked"));
    })
</script>

<!-- Portfolio Modals -->

<!-- Modal 1 -->
<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2><?php echo Yii::t('app', 'Мировой рынок'); ?></h2>
                            <p class="item-intro text-muted"><?php echo Yii::t('app', 'продукции из белка
оценивается в <span style = "color:red">460 млрд. $</span> в год'); ?></p>
                            <img class="img-fluid d-block mx-auto" src="biopro/img/portfolio/1.jpg" alt="">
                            <?php echo Yii::t('app', '<p>Объем мирового рынка комбикормов – около
1,1 млрд. тонн в год.</p><p>
Годовой прирост потребления - 0,8 - 1,1 млн.
тонн в год.
</p>
<p>
ВЕДУЩИЕ СТРАНЫ – ПРОИЗВОДИТЕЛИ
КОМБИКОРМОВ (ПО СОСТОЯНИЮ НА 2016Г.)
</p>
<ul class="list-inline">
<li>-КНР, - 180 млн. т/год, 8550 комбикормовыхзаводов,</li>
<li>-США, - 173 млн. т/год, 6012 заводов,</li>
<li>-Бразилия, - 69 млн. т/год, 1556 заводов.</li>
</ul>
<p>
ВЕДУЩИЕ КОМПАНИИ – ПРОИЗВОДИТЕЛИ
КОМБИКОРМОВ
</p>
<ul class="list-inline">
<li>-CP Group (Таиланд) – 27,6 млн. тонн</li>
<li>-Cargill (США) – 19,5 млн. тонн</li>
<li>-New Hope Liuhe (Китай) – 15,7 млн.тонн</li>
</ul>
'); ?>

                            <button class="btn btn-primary" data-dismiss="modal" type="button"><i class="fa fa-times"></i> <?php echo Yii::t('app', 'Закрыть'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal 2 -->
<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2><?php echo Yii::t('app', 'Стабильное производство'); ?></h2>
                            <p class="item-intro text-muted"><?php echo Yii::t('app', 'независимо от
сезонных и климатические изменений с
непрерывным циклом поставок по
фиксированным ценам.'); ?></p>
                            <img class="img-fluid d-block mx-auto" src="biopro/img/portfolio/2.jpg" alt="">
                            <?php echo Yii::t('app', '<p>Снижение воздействия на окружающую
среду.</p>
<p>
Продукция стабильно высокого качества
обеспечивающая экономически
эффективное с/х производство.
</p>
<p>
Дополнительные выгоды, получаемые от
более глубокой переработки (пищевое и
фармацевтическое производство).
</p>
<p>
Стабильность хранения.
</p>
'); ?>
                            <button class="btn btn-primary" data-dismiss="modal" type="button"><i class="fa fa-times"></i> <?php echo Yii::t('app', 'Закрыть'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal 3 -->
<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2><?php echo Yii::t('app', 'Биопротеин из метана'); ?></h2>
                            <img class="img-fluid d-block mx-auto" src="biopro/img/portfolio/3.jpg" alt="">
                            <?php echo Yii::t('app', '<p>
Биопротеин из метана является полноценным
микробиологическим белком И предназначен для
использования в качестве основного функционального
компонента комбикормов и белково - витаминныхдобавок
(БВД).

                            </p>
<p>
Благодаря ему можно производить полнорационные,
сбалансированные комбикорма, обеспечивающие
интенсивное развитие животноводства, птицеводства и
рыбоводства.
</p>
<p>
Биопротеин содержит: сырой протеин 70-79 . По своему
составу он является полноценным белковым продуктом с
высоким содержанием витаминов группы В (особенноВ12),
аминокислот и микроэлементов, полностью
обеспечивающий в них потребности животных различных
групп и возрастов.
</p>
<p>
Кроме того, по сравнению с белковыми кормами
растительного происхождения (включая злаковые и бобовые
культуры, в том числе и сою), биопротеин из метана
обеспечивает сбалансированное аминокислотное питание
животных, в первую очередь по лизину, серину и метионину.
</p>
<p>
Биопротеин является высокоэффективной кормовой
добавкой и характеризуется наличием всех незаменимых
аминокислот, в том числе лизина 5,3 и серосодержащих
аминокислот - 1,7 .
</p>

                            '); ?>
                            <button class="btn btn-primary" data-dismiss="modal" type="button"><i class="fa fa-times"></i> <?php echo Yii::t('app', 'Закрыть'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal 4 -->
<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2><?php echo Yii::t('app', 'БИОПРОТЕИН - ЭКОЛОГИЧЕСКАЯ БЕЗОПАСНОСТЬ'); ?></h2>
                            <img class="img-fluid d-block mx-auto" src="biopro/img/portfolio/4.jpg" alt="">
                            <?php echo Yii::t('app', '<p>
Биопротеин из метана - не токсичен,не обладает
канцерогенным и кумулятивным действием.
                            </p>
<p>
Биопротеин из метана успешно прошел комплексные
испытания на всех видах животных, птицы, рыбы и
пушных зверей. Испытания проводились на всех
возрастных группах и на нескольких поколениях с/х
животных и птицы.
</p>
<p>
Мясопродукция, полученная с использованием
биопротеина в кормах животных, безвредна для человека
- это доказано многолетним применением биопротеина в
кормах животных на Западе, в частности в Европе. ЕС:
Директива 95/33/EC от 10 июля 1995 года и
Постановление No 575/2011 от 16 июня 2011 года одобрили
использование биопротеина из метана в корм для
животных и рыб.
</p>
<p>
На биопротеин имеются акты о безопасности и
безвредности для животных и птицы, имеются ТУ и
Наставление по применению.
</p>
                            '); ?>

                            <button class="btn btn-primary" data-dismiss="modal" type="button"><i class="fa fa-times"></i> <?php echo Yii::t('app', 'Закрыть'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal 5 -->
<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2><?php echo Yii::t('app', 'МАРКЕТИНГОВЫЙ АНАЛИЗ'); ?></h2>
                            <img class="img-fluid d-block mx-auto" src="biopro/img/portfolio/5.jpg" alt="">
                            <?php echo Yii::t('app', '
<p>
Технология производства белка из метана является
очень перспективной и обладает значительным
потенциалом, о чем свидетельствует то, что
зарубежные компании уже пытаются реализовать
подобные проекты.
</p>
<p>
Так Американская компания Calysta планирует
применить новые технологии по конвертации метана в
кормовой белок.
</p>
<p>
Представители Calysta отмечают, что технология
конвертации газа в кормовой белок сегодня намного
более востребована, чем 15 лет назад по причине роста
цен на рыбную муку и прочие источники белка для
животноводства, птицеводства и аквакультуры.
Мировой рынок продукции из белка они оценивают в
<span style = "color: red;">460 млрд. $</span> в год. Calysta также объявила о
партнерстве с Cargill для производства биопротеина в
Северной Америке и маркетинга во всем мире.
</p>
                            '); ?>
                            <button class="btn btn-primary" data-dismiss="modal" type="button"><i class="fa fa-times"></i> <?php echo Yii::t('app', 'Закрыть'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal 6 -->
<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2><?php echo Yii::t('app', 'Резюме'); ?></h2>
                            <img class="img-fluid d-block mx-auto" src="biopro/img/portfolio/6.jpg" alt="">
                            <?php echo Yii::t('app', '<p>Диверсификация экспорта газа, с созданием
продукта большей добавленной стоимости в том
числе и на экспорт</p>
<p>
Решение проблемы дефицита белка
</p>
<p>
Удешевление производства продуктов питания
</p>
<p>
Высокая автоматизация производственного
процесса и модульная компоновка оборудования
позволяет легко тиражировать проект, как в России
так и за рубежом
</p>
<p>
Развитие сырьевой базы для фармацевтики,
медицинской парфюмерной и других отраслей за
счет продуктов глубокой переработки белка.
</p>
<p>Решение экологических задач</p>
<p style = "color: red;">
Все это позволит компании «МЕТАНИКА», занять
лидирующее положение на рынке, а её инвесторам
получать высокий, стабильный доход.
</p>
'); ?>

                            <button class="btn btn-primary" data-dismiss="modal" type="button"><i class="fa fa-times"></i> <?php echo Yii::t('app', 'Закрыть'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>