<?php
use yii\widgets\Breadcrumbs;
use yii\web\View;
use yii\helpers\Url;
$this->params['breadcrumbs']= [ [
    'label' => 'О Нас',
    'url' => '/about'
  ],
];
$this->title = $model->title;
?>

<div class="hero-box hero-box-smaller full-bg-7 font-inverse" data-top-bottom="background-position: 50% 0px;" data-bottom-top="background-position: 50% -600px;">
    <div class="container" style = "margin-top: 170px;">
        <h1 class="hero-heading wow fadeInDown" data-wow-duration="0.6s"><?php echo $this->title;?></h1>
        <p class="hero-text wow bounceInUp" data-wow-duration="0.9s" data-wow-delay="0.2s"><?php echo Yii::t('app', 'Красивая вода на заказте солнца');?></p>
    </div>
    <div class="hero-overlay bg-black"></div>
</div>

<div class="container">
    <h3 class="p-title">
        <span><?php echo Yii::t('app', 'Заголовочек');?></span>
    </h3>
    <div class="row">
        <div class="col-md-6">
            <div class="icon-box icon-box-left mrg20B">
                <i class="icon-alt font-secondary glyph-icon icon-linecons-diamond wow bounceIn" data-wow-duration="0.8s"></i>
                <div class="icon-wrapper">
                    <h5 class="icon-title wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.1s"><?php echo Yii::t('app', 'Лазурный алмаз');?></h5>
                    <p class="icon-content wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.2s"><?php echo Yii::t('app', 'Какое-то описание, текст под картинкой и надписью');?></p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="icon-box icon-box-left mrg20T">
                <i class="icon-alt font-secondary glyph-icon icon-linecons-photo wow bounceIn" data-wow-duration="0.8s" data-wow-delay="0.3s"></i>
                <div class="icon-wrapper">
                    <h5 class="icon-title wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.4s"><?php echo Yii::t('app', 'Прямоугольная иконка');?></h5>
                    <p class="icon-content wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.5s"><?php echo Yii::t('app', 'Тут какое-то описание, текст содержащий информацию о чём-то важном, но сейчас здесь находится рыба, которую надо изменить');?></p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="icon-box icon-box-left mrg20B">
                <i class="icon-alt font-secondary glyph-icon icon-linecons-cog wow bounceIn" data-wow-duration="0.8s" data-wow-delay="0.6s"></i>
                <div class="icon-wrapper">
                    <h5 class="icon-title wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.7s"><?php echo Yii::t('app', 'Лазурный алмаз');?></h5>
                    <p class="icon-content wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.8s"><?php echo Yii::t('app', 'Тут какое-то описание, текст содержащий информацию о чём-то важном, но сейчас здесь находится рыба, которую надо изменить');?></p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="icon-box icon-box-left mrg20T">
                <i class="icon-alt font-secondary glyph-icon icon-linecons-graduation-cap wow bounceIn" data-wow-duration="0.8s" data-wow-delay="0.9s"></i>
                <div class="icon-wrapper">
                    <h5 class="icon-title wow bounceIn" data-wow-duration="0.6s" data-wow-delay="1s"><?php echo Yii::t('app', 'Шестерёнка');?></h5>
                    <p class="icon-content wow bounceIn" data-wow-duration="0.6s" data-wow-delay="1.1s"><?php echo Yii::t('app', 'Тут какое-то описание, текст содержащий информацию о чём-то важном, но сейчас здесь находится рыба, которую надо изменить');?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<h3 class="p-title">
    <span><?php echo Yii::t('app', 'Заголовочек');?></span>
</h3>
<div class="hero-box fixed-bg hero-box-smaller full-bg-10 font-inverse">
    <div class="container">


        <div class="col-md-6">
            <div class="icon-box icon-box-left mrg25B">
                <i class="icon-alt glyph-icon icon-linecons-params wow bounceIn" data-wow-duration="0.8s"></i>
                <div class="icon-wrapper">
                    <h4 class="icon-title wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.1s"><?php echo Yii::t('app', 'Иконочка');?></h4>
                    <p class="icon-content wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.2s"><?php echo Yii::t('app', 'Тут какое-то описание, текст содержащий информацию о чём-то важном, но сейчас здесь находится рыба, которую надо изменить');?></p>
                    <a class="read-more wow fadeInUp" data-wow-delay="1.2s" href="#" title="Learn more about customizing AUI"><?= Yii::t('app', 'Присоединиться');?></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="icon-box icon-box-left mrg25T">
                <i class="icon-alt glyph-icon icon-linecons-beaker wow bounceIn" data-wow-duration="0.8s" data-wow-delay="0.3s"></i>
                <div class="icon-wrapper">
                    <h4 class="icon-title wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.4s"><?php echo Yii::t('app', 'Иконочка');?></h4>
                    <p class="icon-content wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.5s"><?php echo Yii::t('app', 'Тут какое-то описание, текст содержащий информацию о чём-то важном, но сейчас здесь находится рыба, которую надо изменить');?></p>
                    <a class="read-more wow fadeInUp" data-wow-delay="1.4s" href="#" title="Learn more about AUI widgets &amp; plugins"><?= Yii::t('app', 'Присоединиться');?></a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="icon-box icon-box-left mrg25B">
                <i class="icon-alt glyph-icon icon-linecons-mobile wow bounceIn" data-wow-duration="0.8s" data-wow-delay="0.6s"></i>
                <div class="icon-wrapper">
                    <h4 class="icon-title wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.7s"><?php echo Yii::t('app', 'Иконочка');?></h4>
                    <p class="icon-content wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.8s"><?php echo Yii::t('app', 'Тут какое-то описание, текст содержащий информацию о чём-то важном, но сейчас здесь находится рыба, которую надо изменить');?></p>
                    <a class="read-more wow fadeInUp" data-wow-delay="1.6s" href="#" title="Learn more about AUI responsive design techiques"><?= Yii::t('app', 'Присоединиться');?></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="icon-box icon-box-left mrg25T">
                <i class="icon-alt glyph-icon icon-linecons-graduation-cap wow bounceIn" data-wow-duration="0.8s" data-wow-delay="0.9s"></i>
                <div class="icon-wrapper">
                    <h4 class="icon-title wow bounceIn" data-wow-duration="0.6s" data-wow-delay="1s"><?php echo Yii::t('app', 'Иконочка');?></h4>
                    <p class="icon-content wow bounceIn" data-wow-duration="0.6s" data-wow-delay="1.1s"><?php echo Yii::t('app', 'Тут какое-то описание, текст содержащий информацию о чём-то важном, но сейчас здесь находится рыба, которую надо изменить');?></p>
                    <a class="read-more wow fadeInUp" data-wow-delay="1.8s" href="#" title="Learn more about AUI extensive documentation"><?= Yii::t('app', 'Присоединиться');?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-overlay opacity-80 bg-black"></div>
    <div class="hero-pattern pattern-bg-2"></div>
</div>



<div class="row no-gutter">
    <div class="col-md-4">
        <div class="bg-green pad25A">
            <div class="pad25A wow flipInX" data-wow-delay="0.5s">
                <h4 class="text-transform-upr font-size-17">Extensive documentation</h4>
                <p class="opacity-80 mrg25T mrg25B">No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful pursue.</p>
                <button class="btn btn-alt btn-hover btn-lg btn-outline-inverse remove-bg">
                    <span>Send us an email</span>
                    <i class="glyph-icon icon-arrow-right"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="bg-orange pad25A">
            <div class="pad25A wow flipInX" data-wow-delay="0.7s">
                <h4 class="text-transform-upr font-size-17">Responsive & Mobile Layouts</h4>
                <p class="opacity-80 mrg25T mrg25B">Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure.</p>
                <button class="btn btn-alt btn-hover btn-lg btn-outline-inverse remove-bg">
                    <span>Read documentation</span>
                    <i class="glyph-icon icon-bullhorn"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="bg-blue pad25A">
            <div class="pad25A wow flipInX" data-wow-delay="0.9s">
                <h4 class="text-transform-upr font-size-17">Based on Bootstrap 3.3</h4>
                <p class="opacity-80 mrg25T mrg25B">Obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</p>
                <button class="btn btn-alt btn-hover btn-lg btn-outline-inverse remove-bg">
                    <span>Find out more</span>
                    <i class="glyph-icon icon-arrow-right"></i>
                </button>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <h3 class="p-title">
        <span>Basic</span>
    </h3>
    <div class="row">
        <div class="icon-box col-md-3">
            <i class="icon-large glyph-icon bg-primary icon-picture-o"></i>
            <h3 class="icon-title">Multiple themes</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box col-md-3">
            <i class="icon-large glyph-icon bg-red icon-tablet"></i>
            <h3 class="icon-title">Fully responsive</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box col-md-3">
            <i class="icon-large glyph-icon bg-green icon-bar-chart-o"></i>
            <h3 class="icon-title">Production-ready</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box col-md-3">
            <i class="icon-large glyph-icon bg-gray font-black icon-gift"></i>
            <h3 class="icon-title">Tons of widgets</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
    </div>


    <h3 class="p-title">
        <span>Multiple sizes</span>
    </h3>
    <div class="row">
        <div class="icon-box col-md-4">
            <i class="icon-large glyph-icon bg-primary icon-picture-o"></i>
            <h3 class="icon-title">Multiple themes</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box col-md-4">
            <i class="icon-medium glyph-icon bg-red icon-tablet"></i>
            <h3 class="icon-title">Fully responsive</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box col-md-4">
            <i class="icon-small glyph-icon bg-green icon-bar-chart-o"></i>
            <h3 class="icon-title">Production-ready</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
    </div>


    <h3 class="p-title">
        <span>Alternate style</span>
    </h3>
    <div class="row">
        <div class="icon-box col-md-3">
            <i class="icon-medium glyph-icon border-green font-green icon-border icon-picture-o"></i>
            <h3 class="icon-title">Multiple themes</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box col-md-3">
            <i class="icon-medium glyph-icon border-red font-red icon-border icon-tablet"></i>
            <h3 class="icon-title">Fully responsive</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box col-md-3">
            <i class="icon-medium glyph-icon border-orange font-gray icon-border icon-bar-chart-o"></i>
            <h3 class="icon-title">Production-ready</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box col-md-3">
            <i class="icon-medium glyph-icon border-blue font-blue-alt icon-border font-black icon-gift"></i>
            <h3 class="icon-title">Tons of widgets</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
    </div>


    <h3 class="p-title">
        <span>Left aligned icons</span>
    </h3>
    <div class="row">
        <div class="icon-box icon-box-left col-md-3">
            <i class="icon-small glyph-icon border-green font-green icon-border icon-picture-o"></i>
            <h3 class="icon-title">Multiple themes</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box icon-box-left col-md-3">
            <i class="icon-small glyph-icon border-red font-red icon-border icon-tablet"></i>
            <h3 class="icon-title">Fully responsive</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box icon-box-left col-md-3">
            <i class="icon-small glyph-icon border-orange font-gray icon-border icon-bar-chart-o"></i>
            <h3 class="icon-title">Production-ready</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
        <div class="icon-box icon-box-left col-md-3">
            <i class="icon-small glyph-icon border-blue font-blue-alt icon-border font-black icon-gift"></i>
            <h3 class="icon-title">Tons of widgets</h3>
            <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
        </div>
    </div>


    <h3 class="p-title">
        <span>Offset icons</span>
    </h3>
    <div class="row">
        <div class="col-md-4">
            <div class="icon-box icon-box-offset-large bg-black inverse icon-boxed">
                <i class="icon-large glyph-icon bg-primary icon-picture-o"></i>
                <h3 class="icon-title">Fully responsive</h3>
                <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="icon-box icon-box-offset-medium inverse bg-blue icon-boxed">
                <i class="icon-medium glyph-icon bg-black icon-tablet"></i>
                <h3 class="icon-title">Production-ready</h3>
                <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="icon-box icon-box-offset-small inverse bg-primary icon-boxed">
                <i class="icon-small glyph-icon bg-black icon-bar-chart-o"></i>
                <h3 class="icon-title">Tons of widgets</h3>
                <p class="icon-content">Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper, magna quam.</p>
            </div>
        </div>
    </div>
</div>