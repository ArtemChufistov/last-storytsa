<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use kartik\grid\GridView;
//use kartik\form\ActiveForm;
use kartik\widgets\Growl;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use kartik\widgets\Select2;

$this->title = Yii::t('app', 'Мерчант: ({m_id}) - {merchName}', ['merchName' => $model->name, 'm_id' => $model->id]);
?>



<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/title-breadcrumbs', [
    //'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/m-account/index'), 'title' => Yii::t('app', 'Мерчант')],
        ['link' => Url::to('/m-account/wallets', ['id' => $model->id]), 'title' => $this->title],
    ]
]); ?>


<style>
    .coin-shop tbody tr td {
        vertical-align: inherit;
    }

    .coin-image {
        margin-right: 15px;
    }
</style>

<div class="row">

    <div class="col-lg-8">
        <div class="panel">
            <div class="panel-heading"><?php echo Yii::t('app', 'Кошельки: ({merchantId}) - {mName}', ['merchantId' => $model->id, 'mName' => $model->name]); ?></div>
            <div class="table-responsive coin-shop">
                <table class="table table-hover manage-u-table">
                    <thead>
                    <tr>
                        <th width="70" class="text-center">#</th>
                        <th><?php echo Yii::t('app', 'Наименование валюты'); ?></th>
                        <th><?php echo Yii::t('app', 'Обозначение валюты'); ?></th>
                        <th><?php echo Yii::t('app', 'Сумма в валюте'); ?></th>
                        <th><?php echo Yii::t('app', 'Операции'); ?></th>
                    </tr>
                    </thead>
                    <tbody>


                    <?php

                    $i = 0;
                    foreach ($walletDataProvider->getModels() as $wallet) {
                        /** @var \app\modules\merchant\models\MCoinmarketcapListings $coinInfoModel */
                        $coinInfoModel = \app\modules\merchant\models\MCoinmarketcapListings::coinInfo($wallet['name']);
                        ?>

                        <tr>
                            <td class="text-center"><?= ++$i ?></td>
                            <td><img class="coin-image" src="/cmt/icons/coins/64x64/<?= $coinInfoModel->id ?>.png"
                                     style="height: 35px;"> <?= $coinInfoModel->name ?></td>
                            <td><?= $coinInfoModel->symbol ?></td>
                            <td><?php

                               echo $wallet['balance'] == -1 ? 0 : $wallet['balance'];

//                                if ($wallet['balance'] >= 0) {
//                                    echo $wallet['balance'];
////                                                    return '<strong>Работает...</strong>';
//                                } else {
//                                    echo 'Нет соединения';
//                                }


                                ?></td>
                            <td>
                                <?php
                                echo Html::a(Html::button('<i class="ti-wallet"></i>', ['class' => "btn btn-info btn-outline btn-circle btn-lg m-r-5"]), ['/m-account/wallet-addresses', 'id' => $wallet['m_id'], 'currency' => $wallet['name']]) . ' ';
                                echo Html::a(Html::button('<i class="ti-layout-grid3-alt"></i>', ['class' => "btn btn-info btn-outline btn-circle btn-lg m-r-5"]), ['/m-account/listtransactions', 'id' => $wallet['m_id'], 'currency' => $wallet['name']]/*, ['data-confirm' => 'Удалить?']*/);
                                echo Html::button('<i class="ti-plus"></i>', ['class' => "btn btn-info btn-outline btn-circle btn-lg m-r-5 coin-addres-create-modal-btn",
                                        'new_addres_url' => Url::toRoute(['/m-account/newaddress', 'id' => $wallet['m_id'], 'currency' => $wallet['name']]),
                                        'data-toggle' => "modal",
                                        'data-target' => "#coinWalletsModal"
                                    ]) . ' ';
                                ?>

                            </td>
                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>


<script>
    $(function () {
//get the click of the search button
        $('.coin-addres-create-modal-btn').click(function () {
            // alert($(this).attr("new_addres_url"));
            $.ajax({
                url: $(this).attr("new_addres_url"),
                success: function (result) {
                    $("#coinWalletsModal .modal-body").html(result.mNewAddress);
                }
            });
        });
    });
</script>

<div class="modal fade" id="coinWalletsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel1">Создание адреса</h4></div>
            <div class="modal-body">
                Загружается...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>