<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use kartik\widgets\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use kartik\widgets\RangeInput;
?>

<?php $form = ActiveForm::begin([
    'action' => 'all-transactions',
    'method' => 'get',
    'class' => 'form',
]); ?>

<div class = "row">
    <div class = "col-lg-3">
        <?= $form->field($model, 'id')->label(Yii::t('app', 'ID транзакции')); ?>
    </div>
    <div class = "col-lg-3">
        <?= $form->field($model, 'txid')->label(Yii::t('app', 'Хэш транзакции')); ?>
    </div>
    <div class = "col-lg-3">
        <?= $form->field($model, 'm_account_id')->dropDownList(array_merge(['' => Yii::t('app', 'Все')], MerchantHelper::userMerchantsArray()), [
            'class' => 'selectpicker',
            'data-style' => 'form-control'
        ]); ?>
    </div>
    <div class = "col-lg-3">
        <?= $form->field($model, 'currency')->dropDownList(array_merge(['' => Yii::t('app', 'Все')], MerchantHelper::avalableWalletsFromMerchantModuleArray()), [
            'class' => 'selectpicker',
            'data-style' => 'form-control'
        ]); ?>
    </div>
</div>
<div class = "row">
    <div class = "col-lg-6">
        <?php echo $form->field($model, 'createTimeRange', [
            'options' => ['class' => 'drp-container form-group'],
        ])->label(Yii::t('app', 'Время создания транзакции'))
            ->widget(DateRangePicker::class, [
                'convertFormat' => true,
                'useWithAddon' => true,
                'startAttribute' => 'createTimeStart',
                'endAttribute' => 'createTimeEnd',
                'presetDropdown'=>true,
                'pluginOptions' => [
                    'showDropdowns' => true,
                    'timePicker' => true,
                    'timePickerIncrement' => 30,
                    'timePicker24Hour' => true,
                    'locale' => ['format' => 'Y-m-d H:i'],
                ]
            ]);
        ?>
    </div>
    <div class = "col-lg-6">

    <?php echo $form->field($model, 'confirmations')->label(Yii::t('app', 'Подтверждений от'))->widget(RangeInput::classname(), [
        'options' => ['placeholder' => Yii::t('app', 'от...')],
        'html5Options' => ['min' => 0, 'max' => 100],
    ]); ?>
    </div>
</div>
<div class = "row">
    <div class="form-group m-b-0">
        <div class="col-lg-12">
            <?= Html::submitButton(Yii::t('app', 'Найти'), ['class' => 'btn btn-info waves-effect waves-light m-t-10']) ?>
            <a href = "/m-account/all-transactions" class="btn btn-success waves-effect waves-light m-t-10"><?php echo Yii::t('app', 'Сбросить фильтр');?></a>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>