<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use kartik\widgets\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\widgets\RangeInput;


?>

<div class="maccount-search">

    <?php $form = ActiveForm::begin([
        'action' => 'all-addresses',
        'method' => 'get',
        'class' => 'form',
    ]); ?>

        <div class = "row">
            <div class = "col-lg-3">
                <?= $form->field($model, 'currency_wallet') ?>
            </div>
            <div class = "col-lg-3">
                <?= $form->field($model, 'm_account_id')->dropDownList(array_merge(['' => Yii::t('app', 'Все')], MerchantHelper::userMerchantsArray()), [
                    'class' => 'selectpicker',
                    'data-style' => 'form-control'
                ]); ?>
            </div>
            <div class = "col-lg-3">
                <?= $form->field($model, 'currency_type')->dropDownList(array_merge(['' => Yii::t('app', 'Все')], MerchantHelper::avalableWalletsFromMerchantModuleArray()), [
                    'class' => 'selectpicker',
                    'data-style' => 'form-control'
                ]); ?>
            </div>
            <div class = "col-lg-3">
                <?= $form->field($model, 'createTimeRange', [
                    'options' => ['class' => 'drp-container form-group'],
                ])->label('Время создания транзакции')
                    ->widget(DateRangePicker::class, [
                        'convertFormat' => true,
                        'useWithAddon' => true,
                        'startAttribute' => 'createTimeStart',
                        'endAttribute' => 'createTimeEnd',
                        'presetDropdown'=>true,
                        'pluginOptions' => [
                            'showDropdowns' => true,
                            'timePicker' => true,
                            'timePickerIncrement' => 30,
                            'timePicker24Hour' => true,
                            'locale' => ['format' => 'Y-m-d H:i'],
                        ]
                    ]);
                ?>
            </div>
        </div>
        <div class = "row">
            <div class = "col-lg-3">
                <?= Html::submitButton(Yii::t('app', 'Найти'), ['class' => 'btn btn-info waves-effect waves-light m-t-10']) ?>
                <a href = "/m-account/all-addresses" class="btn btn-success waves-effect waves-light m-t-10"><?php echo Yii::t('app', 'Сбросить фильтр');?></a>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
