<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Редактирование магазина: {name}', ['name' => $model->name]);
?>
<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => Yii::t('app', 'Панель управления')],
        ['link' => Url::to('/m-account/index'), 'title' => 'Мерчант'],
        ['link' => Url::to('/m-account/update?id=' . $model->id), 'title' => $this->title],
    ]
]);?>
<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/merchant-page-style');?>


<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div class="maccount-update">

                <?= $this->render('_form', [
                    'model' => $model,
                    'update' => true
                ]) ?>

            </div>
        </div>
    </div>
</div>