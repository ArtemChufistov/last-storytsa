<?php
use yii\helpers\Html;
use kartik\widgets\SwitchInput;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccount */
/* @var $form yii\widgets\ActiveForm */
?>

<style type="text/css">
    .bootstrap-switch-wrapper{
        margin-left: 15px;
    }
</style>

<div class="maccount-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
            'role' => 'form'
        ]
    ]); ?>

        <?= $form->field($model, 'name', [
            'template' => '{label}<div class="col-sm-6">{input}{hint}{error}</div>',
            'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
        ])->textInput(); ?>

        <?= $form->field($model, 'url', [
            'template' => '{label}<div class="col-sm-6">{input}{hint}{error}</div>',
            'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
        ])->textInput(); ?>

        <?= $form->field($model, 'comment', [
            'template' => '{label}<div class="col-sm-6">{input}{hint}{error}</div>',
            'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
        ])->textInput(); ?>

        <?= $form->field($model, 'secret', [
            'template' => '{label}<div class="col-sm-4">{input}{hint}{error}</div><div class="col-sm-3"><button class = "btn btn-info waves-effect waves-light" type="button" id="gsecret">' . Yii::t('app', 'Сгенерировать ключ') . '</button></div>',
            'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
        ])->textInput(); ?>

        <?= $form->field($model, 'image', [
            'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>',
            'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
        ])->fileInput(); ?>

        <?= $form->field($model, 'is_turn_on', [
            'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>',
            'labelOptions' => [ 'class' => 'col-sm-3 control-label' ]
            ])->widget(SwitchInput::classname(), ['pluginOptions' => [
                'size' => 'small',
                'onColor' => 'success',
                'offColor' => 'danger',
            ],
        ]); ?>
        <div class="form-group m-b-0">
            <div class="col-sm-offset-3 col-sm-9">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-info waves-effect waves-light m-t-10']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>





    
<script>
function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 16; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
$( "#gsecret" ).click(function() {
        $('#maccount-secret').val(makeid());
    });
</script>