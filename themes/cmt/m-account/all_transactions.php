<?php
use app\modules\merchant\models\MNodesTransactions;
use yii\grid\GridView;
use app\modules\merchant\models\MAccount;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

?>

<?php
$this->title = Yii::t('user', 'Все транзакции');
?>
<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => 'Панель управления'],
        ['link' => Url::to('/m-account/all-transactions'), 'title' => $this->title],
    ]
]); ?>
<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/merchant-page-style'); ?>


<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <?php echo $this->render('_search_all_transactions', ['model' => $searchModel]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div class="maccount-update">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">

                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'columns' => [
                                    'id',
                                    [
                                        'attribute' => 'currency',
                                        'format' => 'raw',
                                        'headerOptions' => ['width' => '150'],
                                        'filter' => Html::activeDropDownList($searchModel, 'currency', MerchantHelper::avalableWalletsFromMerchantModuleArray() ,[
                                            'class' => 'selectpicker',
                                            'data-style' => 'form-control',
                                        ]),
                                        'value' => function ($model, $index, $widget) {
                                            $logo = "<table><td><img width='20px' src='" . MerchantHelper::coinLogoUrl($model->currency) . "'>&nbsp;&nbsp;<td>";
                                            return $logo . MerchantHelper::avalableWalletsFromMerchantModuleArray()[$model->currency]."</table>";
                                        },
                                    ],[
                                        'attribute' => 'm_account_id',
                                        'format' => 'raw',
                                        'headerOptions' => ['width' => '150'],
                                    ],[
                                        'attribute' => 'txid',
                                        'format' => 'raw',
                                        'label' => Yii::t('app', 'Хэш транзакции'),
                                        'headerOptions' => ['width' => '150'],
                                        'value' => function ($model, $index, $widget) {
                                            return MerchantHelper::transactionBlockchainLink($model->currency, $model->txid);
                                        },
                                    ],[
                                        'label' => Yii::t('app', 'Сумма'),
                                        'attribute' => 'amount',
                                        'value' => function($model, $index, $widget){
                                            return sprintf("%.8f", $model->amount);
                                        }
                                    ],[
                                        'label' => Yii::t('app', 'Комиссия'),
                                        'attribute' => 'fee',
                                        'value' => function($model, $index, $widget){
                                            return sprintf("%.8f", $model->fee);
                                        }
                                    ],[
                                        'class' => 'yii\grid\DataColumn',
                                        'label' => Yii::t('app', 'Тип'),
                                        'format' => 'html',
                                        'attribute' => 'type',
                                        'filter' => Html::activeDropDownList($searchModel, 'category', MNodesTransactions::getCategoryArray(),[
                                            'class' => 'selectpicker',
                                            'data-style' => 'form-control',
                                            'prompt' => 'Все'
                                        ]),
                                        'value' => function ($model, $index, $widget) {
                                            return MerchantHelper::inOutLable($model->amount);
                                        },
                                    ],[
                                        'attribute' => 'to_addr',
                                        'format' => 'raw',
                                        'label' => Yii::t('app', 'Адрес'),
                                        'headerOptions' => ['width' => '150'],
                                        'value' => function ($model, $index, $widget) {
                                            return MerchantHelper::addressBlockchainLink($model->currency, $model->to_addr);
                                        },
                                    ],[
                                        'attribute' => 'confirmations',
                                        'label' => Yii::t('app', 'Подтверждений'),
                                        'format' => 'raw',
                                    ],[
                                        'attribute' => 'blockchain_time',
                                        'label' => Yii::t('app', 'Дата создания'),
                                        'format' => 'raw',
                                        'headerOptions' => ['width' => '150'],
                                        'filter' => false
                                    ]
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
td {
    font-weight: bolder;
}
.white-box{
    overflow-y: scroll;
}
</style>