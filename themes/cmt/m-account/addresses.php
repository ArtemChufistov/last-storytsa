<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;

$this->title = MerchantHelper::prettyCoinName($currency) . ' ' . Yii::t('user',  'адреса');
?>
<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/m-account/index'), 'title' => Yii::t('app', 'Мерчант')],
        ['link' => Url::to('/m-account/wallets?id=' . $model->id), 'title' => Yii::t('app', 'Магазин: ({m_id}) - {merchName}', ['merchName' => $model->name, 'm_id' => $model->id])],
        ['link' => Url::to('/m-account/wallet-addresses'), 'title' => $this->title],
    ]
]);?>
<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/merchant-page-style');?>



<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title"><?php echo Yii::t('app', 'Магазин: {merchName}', ['merchName' => $model->name]); ?></h3>
            <div class="log-index">
                <table class="table">
                    <tr>
                        <th><?php echo Yii::t('app', 'Адрес');?></th>
                        <th><?php echo Yii::t('app', 'Сумма');?></th>
                        <th><?php echo Yii::t('app', 'Действие');?></th>
                    </tr>
                    <?php foreach ($addresses as $address) { ?>
                        <tr>
                            <td>
                                <?= MerchantHelper::addressBlockchainLink($currency, $address) ?>
                            </td>
                            <td>
                                <?= MerchantHelper::coinPriceUSD($currency, Yii::$app->get($currency)->getAddressBalance($model->id, $address, 0), true); ?>
                            </td>
                            <td>
                                <?= Html::a(Html::button('Список транзакций', ['class' => "btn btn-info"]), ['/m-account/listtransactions-by-address', 'id' => $model->id,
                                    'currency' => $currency, 'address' => $address]) ?>
                            </td>
                        </tr>
                        <?php 
                    } ?>
                </table>
            </div>
        </div>
    </div>
</div>
