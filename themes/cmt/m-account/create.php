<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Добавление мерчанта');
$this->params['breadcrumbs'][] = $this->title;

?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/m-account/index'), 'title' => $this->title],
    ]
]);?>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div class="maccount-create">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>

            </div>
        </div>
    </div>
</div>