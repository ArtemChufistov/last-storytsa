<?php
use app\modules\merchant\models\MAccount;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
?>

<?php
$this->title = Yii::t('user', 'Все адреса');
?>
<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => 'Панель управления'],
        ['link' => Url::to('/m-account/create'), 'title' => $this->title],
    ]
]); ?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/merchant-page-style'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <?php echo $this->render('_search_all_addresses', ['model' => $searchModel]); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <?php echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                [
                                    'attribute' => 'currency_type',
                                    'format' => 'raw',
                                    'headerOptions' => ['width' => '150'],
                                    'filter' => Html::activeDropDownList($searchModel, 'currency_type', array_merge(['all' => Yii::t('app', 'Все')], MerchantHelper::avalableWalletsFromMerchantModuleArray()) ,[
                                            'class' => 'selectpicker',
                                            'data-style' => 'form-control',
                                    ]),
                                    'value' => function ($model, $index, $widget) {
                                        $logo = "<table><td><img width='20px' src='" . MerchantHelper::coinLogoUrl($model->currency_type) . "'>&nbsp;&nbsp;<td>";
                                        return $logo.MerchantHelper::avalableWalletsFromMerchantModuleArray()[$model->currency_type]."</table>";
                                    },
                                ],[
                                    'attribute' => 'm_account_id',
                                    'format' => 'raw',
                                    'headerOptions' => ['width' => '150'],
                                ],[
                                    'attribute' => 'currency_wallet',
                                    'format' => 'raw',
                                    'headerOptions' => ['width' => '150'],
                                    'value' => function ($model, $index, $widget) {
                                        return MerchantHelper::addressBlockchainLink($model->currency_type, $model->currency_wallet);
                                    },
                                ],[
                                    'attribute' => 'created_at',
                                    'format' => 'raw',
                                    'headerOptions' => ['width' => '150'],
                                    'filter' => false
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
td {
    font-weight: bolder;
}
</style>