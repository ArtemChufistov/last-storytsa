<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;

$this->title = MerchantHelper::prettyCoinName($currency) . ' ' .Yii::t('user', 'транзакции') . ' для адреса ' .Yii::$app->request->get('address');
?>
<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/m-account/index'), 'title' => Yii::t('app', 'Мерчант')],
        ['link' => Url::to('/m-account/wallets?id=' . $model->id), 'title' => Yii::t('app', 'Магазин: ({m_id}) - {merchName}', ['merchName' => $model->name, 'm_id' => $model->id])],
        ['link' => Url::to('/m-account/wallet-addresses?id=' . $model->id . '&currency=' . $currency), 'title' => MerchantHelper::prettyCoinName($currency) . ' ' . Yii::t('user',  'адреса')],
        ['link' => Url::to('/m-account/listtransactions'), 'title' => $this->title],

    ]
]); ?>
<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/merchant-page-style'); ?>


<?= $this->render('_tx_table', [
    'model' => $model, 'transactions' => $transactions, 'currency' => $currency
]) ?>