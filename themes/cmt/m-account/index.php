<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;
use app\modules\merchant\components\twofa\widgets\TwoFaQr;

/* @var $this yii\web\View */
/* @var $searchModel app\admin\modules\altcoin\models\search\MAccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Список мерчантов');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .coin-text-size {
        font-size: 17px;
    }

</style>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => Yii::t('app', 'Панель управления')],
        ['link' => Url::to('/m-account/index'), 'title' => $this->title],
    ]
]);?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/merchant-page-style');?>


<?php if (Yii::$app->session->hasFlash('secret')) { ?>

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <?= Yii::$app->session->getFlash('secret') ?>
    </div>

<?php } ?>

<p>
    <?= Html::a(Yii::t('app', 'Добавить Мерчант'), ['m-account/create'], ['class' => 'btn btn-success']) ?>
</p>

<?php
/*
echo TwoFaQr::widget([
    'accountName' => $model->user->email,
    'secret' => $model->secret,
    'issuer' => Yii::$app->params['twoFaIssuer'],
    'size' => 300
]);
*/
?>

<div class = "row">
    <?php echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_merchant',
        'layout' => "{items}",
    ]);
    ?>
</div>




<?php $this->registerJs("
    $('.slimscroll').slimScroll({
        height: '250px'
    });
")?>
