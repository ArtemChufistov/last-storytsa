<?php

use app\modules\merchant\components\merchant\helpers\MerchantHelper;

?>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title"><?php echo Yii::t('app', 'Магазин: {merchName}', ['merchName' => $model->name]); ?></h3>
            <div class="log-index">
                <table class="table">
                    <tr>
                        <th><?php echo Yii::t('app', 'От кого'); ?></th>
                        <th></th>
                        <th><?php echo Yii::t('app', 'Сумма'); ?></th>
                        <th><?php echo Yii::t('app', 'Коммисия'); ?></th>
                        <th><?php echo Yii::t('app', 'Кому'); ?></th>
                        <th><?php echo Yii::t('app', 'Подтверждений'); ?></th>
                        <th><?php echo Yii::t('app', 'Дата'); ?></th>
                        <th><?php echo Yii::t('app', 'Хэш транзакции'); ?></th>
                    </tr>
                    <?php foreach ($transactions as $transaction): ?>

                        <?php // echo "<pre>"; print_r($transaction); ?>
                        <tr>
                            <td>
                                <?= MerchantHelper::addressBlockchainLink($currency, isset($transaction['from']) ? $transaction['from'] : '') ?>
                            </td>
                            <td>
                                <?= MerchantHelper::inOutLable($transaction['amount']) ?>
                            </td>
                            <td>
                                <?= MerchantHelper::coinPriceUSD($currency, $transaction['amount'], true); ?>
                            </td>
                            <td>
                                <?= MerchantHelper::coinPriceUSD($currency, isset($transaction['fee']) ? $transaction['fee'] : 0, true); ?>
                            </td>
                            <td>
                                <?= MerchantHelper::addressBlockchainLink($currency, $transaction['address']) ?>
                            </td>
                            <td>
                                <?= $transaction['confirmations']; ?>
                            </td>
                            <td>
                                <?= date("d.m.Y H:i:s", $transaction['time']); ?>
                            </td>
                            <td>
                                <?= MerchantHelper::transactionBlockchainLink($currency, $transaction) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>