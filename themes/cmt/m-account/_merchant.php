<?php
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-md-4">
    <div class="white-box">
        <h3 class="box-title" style = "text-align: center;"><span style = "font-weight: normal;"><?php echo Yii::t('app', $model->name); ?></h3>
        <h3 class="box-title" style = "text-align: center;"><span style = "font-weight: normal;"><?php echo Yii::t('app', 'ID МЕРЧАНТА:'); ?> <?php echo $model->id;?></h3>
        <div class="row">
            <div class="col-lg-12 col-xs-12 shop-info">
                <?php $img_url = "/cmt/icons/m-logo/{$model->id}.png"; ?>
                <img src="<?= is_file("/app/web/cmt/icons/m-logo/{$model->id}.png") ? $img_url : "/cmt/images/big/img1.jpg" ?>" style="height: 250px; margin: 0 auto;" class="img-responsive"/>
                <div class="padding-top-20" style="width: 300px; margin: 0 auto;">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <style>
                                .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
                                .toggle.ios .toggle-handle { border-radius: 20px; }
                            </style>
                        </div>
                    </div>
                    <div class="row padding-top-20">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <a href="<?= Url::toRoute(['/m-account/update', 'id' => $model['id']]) ?>"
                               class="btn btn-block btn-outline btn-rounded btn-info"><?php echo Yii::t('app', 'НАСТРОЙКИ'); ?></a>
                            <a href="<?= Url::toRoute(['//m-account/test-msg-response', 'id' => $model['id']]) ?>"
                               class="btn btn-block btn-outline btn-rounded btn-info"><i
                                        class="ti-sign"></i> <?php echo Yii::t('app', 'ТЕСТ УВЕДОМЛЕНИЙ'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
