<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($model->url == '') {
    echo "<h2>Заполните поле URL</h2>";
} else {
    ?>


    <h2><?= $model->id ?></h2>
    <h2><?= $model->url ?></h2>

    <style type="text/css">
        .bootstrap-switch-wrapper {
            margin-left: 15px;
        }
    </style>

    <div class="maccount-form">

        <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'role' => 'form'
            ]
        ]); ?>

        <div class="form-group m-b-0">
            <div class="col-sm-offset-3 col-sm-9">
                <?= Html::submitButton('Отправить тестовый запрос', ['class' => 'btn btn-info waves-effect waves-light m-t-10']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


    <?php if ($answer['request'] != '') { ?>

        <h2>Тест запрос</h2>
        <?php
        echo "<pre>";
        print_r($answer['request']);
        echo "</pre>";
        ?>

        <h2>Ответ</h2>
        <h4>Положительный ответ должен быть *ok*</h4>
        <?php
        echo "<pre>";
        print_r($answer['response']);
        echo "</pre>";
        ?>

    <?php }
} ?>
