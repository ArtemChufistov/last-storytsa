<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountApiRequestLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maccount-api-request-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'm_account_id')->textInput() ?>

    <?= $form->field($model, 'request')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
