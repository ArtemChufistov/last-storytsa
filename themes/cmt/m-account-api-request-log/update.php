<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountApiRequestLog */

$this->title = 'Update M Account Api Request Log: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'M Account Api Request Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maccount-api-request-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
