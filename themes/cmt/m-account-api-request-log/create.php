<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountApiRequestLog */

$this->title = 'Create M Account Api Request Log';
$this->params['breadcrumbs'][] = ['label' => 'M Account Api Request Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maccount-api-request-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
