<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\web\View;
use app\modules\support\models\TicketDepartment;

$this->title = Yii::t('user', 'Техническая поддержка');
$this->params['breadcrumbs'][] = $this->title;

echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => Yii::t('app', 'Панель управления')],
        ['link' => Url::to('/office/user'), 'title' => $this->title],
    ]
]);

$departments = TicketDepartment::find()->all();

$departmentArray = [];
foreach($departments as $department){
  $departmentArray[$department->id] = Yii::t('app', $department->name);
}
?>

<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="white-box">
      <div class="tab-content">
        <div class="content-box-wrapper text-center clearfix" style = "text-align: justify;">
          <?php echo Yii::t('app', '<p style = "text-align: left;"><strong style = "text-align: left;">Внимание! ответ может занять до 24 часов.</strong></p><p style = "text-align: left;">Здесь вы можете задавать вопросы администрации проекта, или внести своё предложение по улучшению сервиса, а так-же дополнительным разделам личного кабинета.</p><p style = "text-align: left;"> Возможно у Вас возникли трудности технического характера, если так, то прежде чем их задавать попробуйте найти решение в разделе FAQ.</p><p style = "text-align: left;"> Если-же вы всеравно не нашли решение своей проблемы, то обратитесь к нам и мы постараемся в кратчайшие сроки помочь. Просьба относится с внимательностью к выбору отдела, так как это упростит сотрудничество</p>');?>
        </div>
      </div>
    </div>
    <div class="white-box">
      <div class="tab-content">
        <h3 class="box-title"><?php echo Yii::t('app', 'Создать тикет');?></h3>
        <div class="content-box-wrapper text-center clearfix" style = "text-align: justify;">
          <?php $form = ActiveForm::begin([
            'options' => [
                'class'=>'form',
                'enctype'=>'multipart/form-data'
            ],
            ]); ?>
          <div class="form-group">
            <div class="col-md-12">
              <?php echo $form->field($ticket, 'department')->widget(Select2::classname(), [
                  'data' =>  $departmentArray,
                  'options' => ['placeholder' => Yii::t('app', 'Выберите отдел')],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <?= $form->field($ticket, 'text')->textArea(['rows' => '4', 'value' => '', 'class' => 'textarea form-control']); ?>
            </div>
          </div>
          <div class="form-group">
            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить'), [
                'class' => 'btn btn-info pull-left',
                'name' => 'signup-button']) ?>
          </div>
          <?php ActiveForm::end();?>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-xs-12">
    <div class="white-box">
      <div class="tab-content">
        <h3 class="box-title"><?php echo Yii::t('app', 'Ваши тикеты');?></h3>
        <div class="tab-content">
         <table class="table ticketTable">
              <tr>
                <th style="width: 10px">№</th>
                <th><?php echo Yii::t('app', 'Дата');?></th>
                <th><?php echo Yii::t('app', 'Статус');?></th>
                <th><?php echo Yii::t('app', 'Отдел');?></th>
                <th><?php echo Yii::t('app', 'Новых (Всего)');?></th>
              </tr>
              <?php foreach(array_reverse($ticketArray) as $ticket):?>
                <tr ticketId="<?php echo $ticket->id;?>">
                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo $ticket->id;?>.</a></td>
                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo date('d-m-Y', strtotime($ticket->dateAdd));?></a></td>
                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo Yii::t('app', $ticket->getStatus()->one()->titleName());?></a></td>
                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo Yii::t('app', $ticket->getDepartment()->one()->name);?></a></td>
                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>">
                    <?php if (count($ticket->getNotReadMessages($user->id)) > 0):?>
                      <span class="label label-success label-rouded"><?php echo count($ticket->getNotReadMessages($user->id));?> (<?php echo $ticket->getMessages()->count();?>)</span></a>
                    <?php else:?>
                      <span class="label label-warning label-rounded"><?php echo count($ticket->getNotReadMessages($user->id));?> (<?php echo $ticket->getMessages()->count();?>)</span></a>
                    <?php endif;?>
                  </td>
                </tr>
              <?php endforeach;?>
            </table>
        </div>
      </div>
    </div>
    <?php if (!empty($currentTicket)):?>

      <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/office/partial/_support-message', [
        'ticketMessages' => $ticketMessages,
        'ticketMessage' => $ticketMessage,
        'currentTicket' => $currentTicket,
        'user' => $user
        ]);?>

    <?php endif;?>
  </div>
</div>

<?php $this->registerJs("$(function () {jQuery('.textarea').wysihtml5();});", View::POS_END);?>

<?php /*
<div id="page-title">
          <h2><?php echo $this->title;?></h2>
          <p><?php echo Yii::t('app', 'Задайте свой вопрос и мы на него ответим');?></p>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="content-box">
            <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Поддержка');?>
              <div class="font-size-11 float-right">
                <a href="#" title=""><i class="glyph-icon opacity-hover icon-lightbulb-o"></i></a>
              </div>
            </h3>
            <div class="content-box-wrapper text-center clearfix" style = "text-align: justify;">
              <?php echo Yii::t('app', '<p style = "text-align: left;"><strong style = "text-align: left;">Внимание! раздел находится в стадии дорабоки, ответ может занять до 24 часов.</strong></p><p style = "text-align: left;">Здесь вы можете задавать вопросы администрации проекта, или внести своё предложение по улучшению сервиса, а так-же дополнительным разделам личного кабинета.</p><p style = "text-align: left;"> Возможно у Вас возникли трудности технического характера, если так, то прежде чем их задавать попробуйте найти решение в разделе <a href="/faq"><strong>FAQ</strong></a>.</p><p style = "text-align: left;"> Если-же вы всеравно не нашли решение своей проблемы, то обратитесь к нам и мы постараемся в кратчайшие сроки помочь. Просьба относится с внимательностью к выбору отдела, так как это упростит сотрудничество</p>');?>
            </div>
          </div>

          <div class="content-box mrg15B">
            <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Создать тикет');?>
              <div class="font-size-11 float-right">
                <a href="#" title=""><i class="glyph-icon opacity-hover icon-life-ring"></i></a>
              </div>
            </h3>
            <div class="content-box-wrapper text-center clearfix">
              <?php $form = ActiveForm::begin([
              'id' => 'form-profile',
              'options' => [
                  'class'=>'form',
                  'enctype'=>'multipart/form-data'
              ],
              ]); ?>

              <?php echo $form->field($ticket, 'department')->widget(Select2::classname(), [
                  'data' =>  ArrayHelper::map(TicketDepartment::find()->all(), 'id', 'name'),
                  'options' => ['placeholder' => Yii::t('app', 'Выберите отдел')],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);?>

              <?= $form->field($ticket, 'text')->textArea(['rows' => '4', 'value' => '', 'class' => 'textarea form-control']); ?>

              <div class="form-group">
                  <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить'), [
                      'class' => 'btn btn-info pull-left',
                      'name' => 'signup-button']) ?>
              </div>

              <?php ActiveForm::end();?>

            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <?php if (!empty($currentTicket)):?>

            <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/office/partial/_support-message', [
              'ticketMessages' => $ticketMessages,
              'ticketMessage' => $ticketMessage,
              'currentTicket' => $currentTicket,
              ]);?>

          <?php endif;?>

          <?php if (!empty($ticketArray)):?>

            <div class="content-box mrg15B">
              <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Ваши тикеты');?>
                <div class="font-size-11 float-right">
                  <a href="#" title=""><i class="glyph-icon opacity-hover icon-bars"></i></a>
                </div>
              </h3>
              <div class="content-box-wrapper text-center clearfix">
                <table class="table ticketTable">
                  <tr>
                    <th style="width: 10px">№</th>
                    <th><?php echo Yii::t('app', 'Дата');?></th>
                    <th><?php echo Yii::t('app', 'Статус');?></th>
                    <th><?php echo Yii::t('app', 'Отдел');?></th>
                    <th><?php echo Yii::t('app', 'Новых сообщений');?></th>
                  </tr>
                  <?php foreach($ticketArray as $ticket):?>
                    <tr ticketId="<?php echo $ticket->id;?>">
                      <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo $ticket->id;?>.</a></td>
                      <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo date('d-m-Y', strtotime($ticket->dateAdd));?></a></td>
                      <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo $ticket->getStatus()->one()->titleName();?></a></td>
                      <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo $ticket->getDepartment()->one()->name;?></a></td>
                      <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><span class="badge bg-red">0</span></a></td>
                    </tr>
                  <?php endforeach;?>
                </table>
              </div>
            </div>

          <?php endif;?>
        </div>
      </div>



*/?>

<style>
.ticketTable tr:hover{
  background-color: #f1f2f7 !important;
}
</style>