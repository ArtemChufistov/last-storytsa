<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('user', 'Информация по магазину: {merchantId}', ['merchantId' => $merchantId]);
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => Yii::t('app', 'Панель управления')],
        ['link' => Url::to('//m-account/index'), 'title' => Yii::t('app', 'Мерчант')],
        ['link' => Url::to('/office/user'), 'title' => $this->title],
    ]
]);?>

<style>
.coin-shop tbody tr td{
	vertical-align: inherit;
}
.coin-image {
	margin-right: 15px;
}
</style>

<div class="row">
    <div class="col-lg-4">
        <div class="white-box">
            <center>
                <h3 class="box-title m-b-0"><?php echo Yii::t('app', 'Создать транзакцию');?></h3>
                <p class="text-muted m-b-30 font-13"><?php echo Yii::t('app', 'Заполните поля');?></p>
            </center>    
                <form class="no-bg-addon">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="exampleInputuname" placeholder="<?php echo Yii::t('app', 'Адрес');?>">
                            <div class="input-group-addon"><i class="ti-user"></i></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="<?php echo Yii::t('app', 'Сумма');?>">
                            <div class="input-group-addon"><i class="ti-email"></i></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control">
                            <option><?php echo Yii::t('app', 'Выберите валюту');?></option>
                            <option>BTC</option>
                        </select>
                    </div>
                    <button class="btn btn-block btn-success btn-lg btn-rounded"><?php echo Yii::t('app', 'Подтвердить');?></button>
                </form>
            
        </div>
    </div>

    <div class="col-lg-8">
        <div class="panel">
            <div class="panel-heading"><?php echo Yii::t('app', 'Детали мерчанта: {merchantId}', ['merchantId' => $merchantId]);?></div>
            <div class="table-responsive coin-shop">
                <table class="table table-hover manage-u-table">
                    <thead>
                        <tr>
                            <th width="70" class="text-center">#</th>
                            <th><?php echo Yii::t('app', 'Наименование валюты');?></th>
                            <th><?php echo Yii::t('app', 'Обозначение валюты');?></th>
                            <th><?php echo Yii::t('app', 'Сумма в валюте');?></th>
                            <th><?php echo Yii::t('app', 'Операции');?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">1</td>
                            <td><img class = "coin-image" src = "/cmt/icons/btc.png" style = "height: 35px;"> <?php echo Yii::t('app', 'BitCoin');?></td>
                            <td><?php echo Yii::t('app', 'BTC');?></td>
                            <td>0.0000000</td>
                            <td>
                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-wallet"></i></button>
                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-layout-grid3-alt"></i></button>
                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-plus"></i></button>
 
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>