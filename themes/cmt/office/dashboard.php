<?php

use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use app\modules\merchant\models\MCoinmarketcapListings;
use app\modules\merchant\models\MNodesTransactions;
use app\modules\merchant\models\MSendTxid;
use borales\extensions\phoneInput\PhoneInput;
use app\modules\profile\models\User;
use kartik\widgets\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('user', 'Панель управления');
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => $this->title]
    ]
]);?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
             <div class="row row-in">
                  <div class="col-lg-3 col-sm-6 row-in-br">
                    <ul class="col-in">
                            <li>
                                <span class="circle circle-md bg-success"><i class="ti-server"></i></span>
                            </li>
                            <li class="col-last"><h3 class="counter text-right m-t-15"><?= MerchantHelper::totalUserTransaction(Yii::$app->user->identity->id) ?></h3></li>
                            <li class="col-middle">
                                <h4><?php echo Yii::t('app', 'Всего транзакций');?></h4>
                                <div class="progress">
                                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> 
                                      <span class="sr-only">100% Complete (success)</span> 
                                  </div>
                                </div>
                            </li>
                    </ul>
                  </div>
                  <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                    <ul class="col-in">
                            <li>
                                <span class="circle circle-md bg-info"><i class="ti-reload"></i></span>
                            </li>
                            <li class="col-last"><h3 class="counter text-right m-t-15"><?= MerchantHelper::totalUserTransactionWithZeroConformation(Yii::$app->user->identity->id) ?></h3></li>
                            <li class="col-middle">
                                <h4><?php echo Yii::t('app', 'Транзакций в обработке');?></h4>
                                <div class="progress">
                                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> 
                                      <span class="sr-only">100% Complete (success)</span> 
                                  </div>
                                </div>
                            </li>
                    </ul>
                  </div>
                  <div class="col-lg-3 col-sm-6 row-in-br">
                    <ul class="col-in">
                            <li>
                                <span class="circle circle-md bg-success"><i class="ti-layout-grid2"></i></span>
                            </li>
                            <li class="col-last"><h3 class="counter text-right m-t-15"><?= MerchantHelper::totalUserTransactionWithUSDSumm(Yii::$app->user->identity->id) ?></h3></li>
                            <li class="col-middle">
                                <h4><?php echo Yii::t('app', 'Транзакций на сумму');?></h4>
                                <div class="progress">
                                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> 
                                      <span class="sr-only">100% Complete (success)</span> 
                                  </div>
                                </div>
                            </li>
                    </ul>
                  </div>
                  <div class="col-lg-3 col-sm-6  b-0">
                    <ul class="col-in">
                            <li>
                                <span class="circle circle-md bg-info"><i class="fa fa-dollar"></i></span>
                            </li>
                            <li class="col-last"><h3 class="counter text-right m-t-15"><?= MerchantHelper::totalUserBalanceByTransactionInUSD() ?></h3></li>
                            <li class="col-middle">
                                <h4><?php echo Yii::t('app', 'Общий баланс в USD');?></h4>
                                <div class="progress">
                                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                      <span class="sr-only">100% Complete (success)</span> 
                                  </div>
                                </div>
                            </li>

                    </ul>
                  </div>
                </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
        <div class="manage-users">
            <div class="sttabs tabs-style-iconbox">
                <nav>
                    <ul>
                        <li><a href="#section-iconbox-1" class="sticon ti-wallet"><span><?php echo Yii::t('app', 'Балансы');?></span></a></li>
                        <li><a href="#section-iconbox-2" class="sticon ti-receipt"><span><?php echo Yii::t('app', 'Последние транзакции');?></span></a></li>
                        <li><a href="#section-iconbox-3" class="sticon ti-check-box"><span><?php echo Yii::t('app', 'Кошельки аккаунта');?></span></a></li>
                    </ul>
                </nav>
                <div class="content-wrap">
                    <section id="section-iconbox-1">
                        <div class="p-20 row">
                            <div class="col-sm-12">
                                <h3 class="m-t-0"><?php echo Yii::t('app', 'Баланс аккаунта по валютам');?></h3>
                            </div>
                        </div>
                        <div class="table-responsive manage-table">
                            <table class="table" cellspacing="14">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><?php echo Yii::t('app', 'Валюты');?></th>
                                        <th><?php echo Yii::t('app', 'Название');?></th>
                                        <th><?php echo Yii::t('app', 'Сумма');?></th>
                                        <th><?php echo Yii::t('app', 'Сумма в  USD');?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach(MerchantHelper::avalableWalletsFromMerchantModuleArray() as $currency => $currencyName):?>
                                        <?php
                                         $coinInfo = MCoinmarketcapListings::coinInfo($currency);
                                         $currencyBalance = MerchantHelper::getAccountBalance($currency);
                                        ?>
                                        <tr class="advance-table-row">
                                            <td></td>
                                            <td width="60"><img src="<?php echo MerchantHelper::coinLogoUrl($currency);?>?>" class="img-circle" width="30" /></td>
                                            <td><?= MerchantHelper::coinLogo($currency);?></td>
                                            <td><?= $coinInfo->name ?></td>
                                            <td><?php echo $currencyBalance;?></td>
                                            <td><?php echo MerchantHelper::coinPriceUSD($currency, $currencyBalance);?></td>
                                        </tr>
                                    <?php endforeach;?>
                                    <tr>
                                        <td colspan="7" class="sm-pd"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </section>
                    <section id="section-iconbox-2">
                        <div class="p-20 row">
                            <div class="col-sm-6">
                                <h3 class="m-t-0"><?php echo Yii::t('app', 'Последние 10 транзакций');?></h3>
                            </div>
                            <div class="col-sm-6">
                                <a href="/m-account/all-transactions" class="btn btn-info pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                                    <?php echo Yii::t('app', 'Все транзакции');?>
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive manage-table">
                            <table class="table" cellspacing="14">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th><?php echo Yii::t('app', 'Валюта');?></th>
                                        <th></th>
                                        <th><?php echo Yii::t('app', 'Сумма');?></th>
                                        <th><?php echo Yii::t('app', 'Транзакция');?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach(MNodesTransactions::find()->where(['user_id' => Yii::$app->user->identity->id])->orderBy('created_at DESC')->limit(10)->all() as $row):?>
                                    <tr class="advance-table-row">
                                        <td width="10"></td>
                                        <td width="30"><img src="<?php echo MerchantHelper::coinLogoUrl($row->currency);?>?>" class="img-circle" width="30" /></td>
                                        <td width="20"><?= MerchantHelper::coinLogo($row->currency);?></td>
                                        <td width="30"> <?= sprintf("%.8f", $row->amount) ?> </td>
                                        <td style = "margin-right: 10px;"> <?= MerchantHelper::transactionBlockchainLink($row->currency, $row->txid, false) ?> </td>
                                    </tr>
                                <?php endforeach;?>
                                    <tr>
                                        <td colspan="3" class="sm-pd"></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </section>
                    <section id="section-iconbox-3">
                        <div class="p-20 row">
                            <div class="col-sm-6">
                                <h3 class="m-t-0"><?php echo Yii::t('app', 'Последние 10 созданных кошельков');?></h3>
                            </div>
                            <div class="col-sm-6">
                                <a href="/m-account/all-addresses" class="btn btn-info pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
                                    <?php echo Yii::t('app', 'Все кошельки');?>
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive manage-table">
                            <table class="table" cellspacing="14">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Валюта</th>
                                        <th></th>
                                        <th>Кошелек</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach(\app\modules\merchant\models\MAccountWallet::find()->where(['user_id' => Yii::$app->user->identity->id])->orderBy('created_at DESC')->limit(10)->all() as $row):?>
                                    <tr class="advance-table-row">
                                        <td width="10"></td>
                                        <td width="30"><img src="<?php echo MerchantHelper::coinLogoUrl($row->currency_type);?>?>" class="img-circle" width="30" /></td>
                                        <td width="20"><?= MerchantHelper::coinLogo($row->currency_type);?></td>
                                        <td> <?= MerchantHelper::addressBlockchainLink($row->currency_type, $row->currency_wallet) ?> </td>
                                    </tr>
                                <?php endforeach;?>
                                    <tr>
                                        <td colspan="7" class="sm-pd"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

<?php
$paymentsTasks = \app\modules\merchant\models\MPaymentTasks::find()->where(['user_id' => Yii::$app->user->identity->id])
    ->andWhere('status != 100')->orderBy('id DESC')->limit(4)->all();
if(count($paymentsTasks) > 0) {
?>
    <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
        <div class="white-box" style = "min-height: 422px;">
            <h3 class="box-title"><?php echo Yii::t('app', 'Транзакции в обработке');?></h3>
            <div class="steamline">
                <?php foreach ($paymentsTasks as $paymentTask):?>
                    <div class="sl-item">
                        <div class="sl-left"> <i class="ti-reload"></i> </div>
                        <div class="sl-right">
                            <div class="m-l-40"><a href="javascript:void(0)" class="text-info"><?php echo Yii::t('app', 'Создание транзакции');?></a>
                                <span class="sl-date"><?php echo Yii::t('app', date("d.m.Y H:i", strtotime($paymentTask->created_at)));?></span>
                                <p class="m-t-10"><?php echo Yii::t('app', 'Ожидание запершения перевода '.MerchantHelper::prettyCoinName($paymentTask->currency)
                                        .' на сумму '.sprintf("%.8f", -1 * $paymentTask->amount).' '.MerchantHelper::coinLogo($paymentTask->currency).' на адрес ' . $paymentTask->to_addr);?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <?php } ?>

    <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
        <div class="white-box" style = "min-height: 422px;">
            <h3 class="box-title"><?php echo Yii::t('app', 'Последние события');?></h3>
            <div class="steamline">
                <?php foreach (MNodesTransactions::find()->where(['user_id' => Yii::$app->user->identity->id])->orderBy('created_at DESC')->limit(4)->all() as $tx_model):?>
                    <div class="sl-item">
                        <div class="sl-left"> <i class="ti-check "></i> </div>
                        <div class="sl-right">
                            <div class="m-l-40"><a href="javascript:void(0)" class="text-info"><?php echo Yii::t('app', 'Создание транзакции');?></a>
                                <span class="sl-date"><?php echo Yii::t('app', date("d.m.Y H:i", strtotime($tx_model->blockchain_time)));?></span>
                                <p class="m-t-10"><?php echo Yii::t('app', 'Создание транзакции в сети '.MerchantHelper::prettyCoinName($tx_model->currency).' на сумму '.sprintf("%.8f", $tx_model->amount).' '.MerchantHelper::coinLogo($tx_model->currency).'');?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.steamline .sl-left {
    background-color: #53e69d;
}
.sticon:hover {
    background-color: #3bbcf5;
}
</style>