<?php

use app\modules\merchant\components\twofa\widgets\TwoFaQr;
use borales\extensions\phoneInput\PhoneInput;
use app\modules\profile\models\User;
use kartik\widgets\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('user', 'Профиль');
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => Yii::t('app', 'Панель управления')],
        ['link' => Url::to('/office/user'), 'title' => $this->title],
    ]
]); ?>


    <div class="row">
<?php $form = ActiveForm::begin([
    'id' => 'form-profile',
    'fieldConfig' => [
        'options' => [
            'tag' => 'div',
        ],
    ],
    'options' => [
        'class' => 'form-horizontal form-material',
        'enctype' => 'multipart/form-data'
    ],
]); ?>

    <style type="text/css">
        .intl-tel-input {
            width: 100%;
        }

        .intl-tel-input .country-list {
            z-index: 100;
        }
    </style>



        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg" style="height: 350px;"><img width="100%" alt="<?php echo $user->login; ?>"
                                                                 src="<?php echo $user->getImage(); ?>">
                    <div class="overlay-box">
                        <div class="user-content">
                            <img src="<?php echo $user->getImage(); ?>" class="thumb-lg img-circle"
                                 alt="<?php echo $user->login; ?>">
                            <br/>
                            <br/>
                            <?php if ($user->image): ?>
                                <?php echo Html::a(Yii::t('app', 'Удалить фото'), ['/profile/office/remove'], ['class' => 'btn btn-default btn-file']); ?>
                                <br/>
                            <?php else: ?>
                                <?= $form->field($user, 'photo', ['options' => ['class' => 'btn btn-default btn-file'], 'template' => '<i class="fa fa-paperclip"></i> ' . Yii::t('app', 'Ваше изображение') . '{input}'])->fileInput() ?>
                                <br/>
                                <br/>
                                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Сохранить'), [
                                    'class' => 'btn btn-info',
                                    'name' => 'signup-button']) ?>
                            <?php endif; ?>

                            <h4 class="text-white"><?php echo $user->login; ?></h4>
                            <h5 class="text-white"><?php echo $user->email; ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="white-box">
                <h5 class="box-title"><?php echo Yii::t('app', 'Смена пароля'); ?></h5>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= $form->field($user, 'old_password')->passwordInput([
                            'maxlength' => true,
                            'placeholder' => $user->getAttributeLabel('old_password'),
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= $form->field($user, 'password')->passwordInput([
                            'maxlength' => true,
                            'placeholder' => $user->getAttributeLabel('password'),
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= $form->field($user, 'repeat_password')->passwordInput([
                            'maxlength' => true,
                            'placeholder' => $user->getAttributeLabel('repeat_password'),
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Сохранить'), [
                            'class' => 'btn btn-info pull-left',
                            'value' => 1,
                            'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <h5 class="box-title"><?php echo Yii::t('app', 'Личные данные'); ?></h5>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= $form->field($user, 'first_name')->textInput([
                            'maxlength' => true,
                            'placeholder' => $user->getAttributeLabel('first_name'),
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= $form->field($user, 'last_name')->textInput([
                            'maxlength' => true,
                            'placeholder' => $user->getAttributeLabel('last_name'),
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                </div>
                <div class="form-group " style="overflow: visible;">
                    <div class="col-xs-12">
                        <label class="control-label"
                               for="profileform-last_name"><?php echo Yii::t('app', 'Телефон'); ?></label>
                        <?php echo $form->field($user, 'phone')->widget(PhoneInput::className(), [
                            'jsOptions' => [
                                'preferredCountries' => ['no', 'pl', 'ua'],
                            ]
                        ])->label('');
                        ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= $form->field($user, 'birthday')
                            ->widget(DatePicker::classname(), [
                                'options' => ['placeholder' => $user->getAttributeLabel('birthday')],
                                'language' => \Yii::$app->language,
                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd.mm.yyyy'
                                ]
                            ]); ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= $form->field($user, 'skype')->textInput([
                            'maxlength' => true,
                            'placeholder' => $user->getAttributeLabel('skype'),
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="radio-head"><?php echo Yii::t('app', 'Пол'); ?></label>
                        <?= $form->field($user, 'sex')
                            ->radioList(
                                User::getSexArray(),
                                [
                                    'item' => function ($index, $label, $name, $checked, $value) {

                                        $checked = $checked == true ? 'checked="checked"' : '';
                                        $return = '<label class="radio-inline">';
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"  ' . $checked . ' >';
                                        $return .= '<i></i>';
                                        $return .= '<span>' . ucwords($label) . '</span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )
                            ->label(false); ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Сохранить'), [
                            'class' => 'btn btn-info pull-left',
                            'value' => 1,
                            'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <h5 class="box-title"><?php echo Yii::t('app', 'Сменить E-mail'); ?></h5>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= $form->field($user, 'email')->textInput([
                            'maxlength' => true,
                            'placeholder' => $user->getAttributeLabel('email'),
                            'class' => 'form-control',
                            'readonly' => true,
                        ]) ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?= $form->field($user, 'email_confirm_token', ['options' => [
                            'style' => $oldUser->email_confirm_token == '' ? 'display:none;' : 'display:block;']])->textInput([
                            'maxlength' => true,
                            'value' => '',
                            'class' => 'form-control',
                            'placeholder' => $user->getAttributeLabel('email_confirm_token')
                        ]) ?>
                    </div>
                </div>
                <div class="form-group ">
                    <?php if ($oldUser->email_confirm_token == ''): ?>
                        <div class="col-lg-12">
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Сменить E-mail'), [
                                'class' => 'btn btn-info pull-left',
                                'value' => 1,
                                'name' => (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                        </div>
                    <?php else: ?>
                        <div class="col-lg-12">
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Сменить E-mail'), [
                                'class' => 'btn btn-danger pull-left',
                                'value' => 1,
                                'name' => (new \ReflectionClass($user))->getShortName() . '[confirm_token_submit]']) ?>
                        </div>
                        <br/>
                        <br/>
                        <div class="col-lg-12">
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Отправить повторно код подтверждения'), [
                                'class' => 'btn btn-success pull-left',
                                'value' => 1,
                                'name' => (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

<?php ActiveForm::end(); ?>

<?= \app\modules\merchant\widgets\ToFaUserWidget::widget([]) ?>

    </div>
    </div>

<?php $successMessage = Yii::$app->session->getFlash('success'); ?>

<?php if (!empty($successMessage)): ?>

    <div class="modal fade successModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <div style="width: 90%; font-weight: bold;"
                         class="modal-title"><?php echo Yii::t('app', 'Внимание !'); ?></div>
                </div>
                <div class="modal-body">
                    <?php echo $successMessage; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success"
                            data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть'); ?></button>
                </div>
            </div>
        </div>
    </div>

    <?php $this->registerJs("
  jQuery('.successModal').modal('show');
", View::POS_END); ?>
<?php endif; ?>