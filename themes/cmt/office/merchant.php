<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('user', 'Мерчант');
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => Yii::t('app', 'Панель управления')],
        ['link' => Url::to('/office/user'), 'title' => $this->title],
    ]
]);?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/merchant-page-style');?>


<?php if (Yii::$app->session->hasFlash('secret')) { ?>
    <div class="alert alert-success">
        <?= Yii::$app->session->getFlash('secret') ?>
    </div>
<?php } ?>

    <p>
        <?= Html::a('Создать Магазин', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title"><?php echo Yii::t('app', 'Магазин по продаже пончиков');?></h3>
            <div class="row">
                <div class="col-lg-9 col-xs-12 shop-info">
                	<img src="/cmt/images/big/img1.jpg" style = "height: 250px;" class="img-responsive float-left margin-right-20" />
                	<div class="float-left padding-top-20">
	                	<div class = "row">
	                		<div class="col-lg-12 col-sm-12 col-xs-12">
			                	<p class="box-title"><?php echo Yii::t('app', '<span>ID магазина:</span> {shopId}', ['shopId' => 1012]);?></p>
			                	<p class="box-title"><?php echo Yii::t('app', '<span>URL магазина:</span> {shopUrl}', ['shopUrl' => 'http://trampampam.ru']);?></p>
	                		</div>
		                </div>
	                	<div class = "row padding-top-20">
							<div class="col-lg-10 col-sm-10 col-xs-10">
	                            <a href = "/office/coindetail/1" class="btn btn-block btn-outline btn-rounded btn-info"><i class="ti-settings"></i> <?php echo Yii::t('app', 'НАСТРОЙКИ');?></a>
	                            <a class="btn btn-block btn-outline btn-rounded btn-info"><i class="ti-share-alt"></i> <?php echo Yii::t('app', 'ВЫВЕСТИ');?></a>
	                            <a class="btn btn-block btn-outline btn-rounded btn-info"><i class="ti-share"></i> <?php echo Yii::t('app', 'ПОПОЛНИТЬ');?></a>
	                        </div>
	                	</div>
	                </div>
                </div>

                <div class="col-lg-3 col-xs-12 coin-wrap slimscroll">
                    <table class = "coin-table">
                    	<tr>
                    		<td class ="text-left"><strong><?php echo Yii::t('app', 'Название');?></strong></td>
                    		<td class ="text-right"><strong><?php echo Yii::t('app', 'Баланс');?></strong></td>
                    	</tr>
                    	<tr>
                    		<td class ="text-left">BitCoin BTC</td>
                    		<td class ="text-right">0.00000000</td>
                    	</tr>
                        <tr>
                    		<td class ="text-left"><strong><?php echo Yii::t('app', 'Название');?></strong></td>
                    		<td class ="text-right"><strong><?php echo Yii::t('app', 'Баланс');?></strong></td>
                    	</tr>
                    	<tr>
                    		<td class ="text-left">BitCoin BTC</td>
                    		<td class ="text-right">0.00000000</td>
                    	</tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




<?php $this->registerJs("
    $('.slimscroll').slimScroll({
        height: '250px'
    });
")?>