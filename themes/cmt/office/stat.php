<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('user', 'Статистика');
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/title-breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => Yii::t('app', 'Панель управления')],
        ['link' => Url::to('/office/user'), 'title' => $this->title],
    ]
]);?>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title"><?php echo Yii::t('app', 'Раздел находится в разработке');?></h3>
        </div>
    </div>
</div>