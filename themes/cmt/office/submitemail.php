<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Подтверждение почты');
$this->context->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/background';

?>
<div style="text-align: center;">
	<?php echo Yii::t('app', 'Поздравляем вы успешно подтвердили свой <br/><strong>E-mail</strong><br/>');?>
	</br>
	<p><?php echo Yii::t('app', 'Теперь Вам доступен полный функционал личного кабинета');?></p>
	</br>
	<a href = "<?php echo Url::to(['/profile/office/dashboard']);?>" style = "font-weight:bold; text-decoration: underline;">
		<?php echo Yii::t('app', 'Перейти в личный кабинет');?>
	</a>
</div>