<?php
use app\modules\profile\widgets\SubscribeWidget;
$this->title = Yii::t('app', 'Crypchant - Главная');
?>

<section class="section bg-gray-800">
  <div class="slick-group-slider bg-gray-700">
    <div class="carousel-parent-outer">
      <svg class="carousel-parent-shape" width="1216px" height="625px" viewBox="0 0 1216 625" preserveAspectRatio="none">
        <path fill-rule="evenodd" d="M-0.000,311.382 C-0.000,311.382 62.999,372.037 102.727,273.078 C118.498,233.793 168.280,306.657 186.342,249.138 C203.245,195.310 231.848,195.447 246.067,143.801 C266.856,68.285 291.661,3.867 310.569,0.159 C341.089,-5.826 374.891,182.287 422.852,134.224 C468.160,88.821 511.245,170.842 535.135,177.317 C545.468,180.117 579.312,194.321 613.972,189.287 C635.941,186.097 655.397,155.251 678.475,172.529 C726.537,208.512 730.880,342.989 781.202,397.567 C807.168,425.730 831.757,373.189 862.428,356.869 C890.078,342.156 923.937,365.255 946.043,356.869 C975.409,345.727 998.875,341.716 1015.324,294.624 C1027.442,259.931 1032.880,184.101 1048.770,155.771 C1068.789,120.077 1110.838,156.070 1129.996,129.436 C1169.461,74.571 1216.000,43.251 1216.000,43.251 L1216.000,624.999 L-0.000,624.999 L-0.000,311.382 Z"></path>
      </svg>
      <div class="parallax-scene parallax-scene-js" data-scalar-x="5" data-scalar-y="10">
        <div class="layer-1">
          <div class="layer" data-depth=".55"><img src="/cmt/images/bitcoin-424x427.png" alt="" width="424" height="427"/>
          </div>
        </div>
        <div class="layer-2">
          <div class="layer" data-depth=".25"><img src="/cmt/images/bitcoin-424x427.png" alt="" width="424" height="427"/>
          </div>
        </div>
        <div class="layer-3">
          <div class="layer" data-depth=".2"><img src="/cmt/images/bitcoin-424x427.png" alt="" width="424" height="427"/>
          </div>
        </div>
        <div class="layer-4">
          <div class="layer" data-depth=".25"><img src="/cmt/images/bitcoin-424x427.png" alt="" width="424" height="427"/>
          </div>
        </div>
      </div>
      <div class="slick-slider carousel-parent" data-arrows="true" data-loop="true" data-dots="true" data-swipe="true" data-items="1" data-fade="true" data-child="#slider-child-carousel" data-for="#slider-child-carousel">
        <div>
          <div class="slick-slide-caption">
            <h6><?php echo Yii::t('app', 'Ваш #1 Крипто Мерчант');?></h6>
            <h1><?php echo Yii::t('app', 'Криптовалюты </h1>');?>
            <h3 class="decoration-heading-1"><?php echo Yii::t('app', 'Принимайте на сайте');?></h3>
            <p><?php Yii::t('app', 'Прием платежей для вашего бизнеса с Лучшим сервисом для Вас. Правильное решение для умной компании');?></p>
            <div class="slick-slide-caption__footer"><a class="button button-primary" href="/signup"><?php echo Yii::t('app', 'Создать');?></a></div>
          </div>
        </div>
        <div>
          <div class="slick-slide-caption">
            <h6><?php echo Yii::t('app', 'Ваш #1 Крипто Мерчант');?></h6>
            <h1><?php echo Yii::t('app', 'Магазины');?></h1>
            <h3 class="decoration-heading-1"><?php Yii::t('app', 'В несколько кликов');?></h3>
            <p><?php echo Yii::t('app', 'Подключайте магазины и увеличивайте продажи для Вашего бизнеса. Поддерживаем все популярные криптовалюты.');?></p>
            <div class="slick-slide-caption__footer"><a class="button button-primary" href="/signup"><?php echo Yii::t('app', 'Создать');?></a></div>
          </div>
        </div>
        <div>
          <div class="slick-slide-caption">
            <h6><?php echo Yii::t('app', 'Ваш #1 Крипто Мерчант');?></h6>
            <h1><?php echo Yii::t('app', 'Выплаты');?></h1>
            <h3 class="decoration-heading-1"><?php Yii::t('app', 'Получение и отправка');?></h3>
            <p><?php echo Yii::t('app', 'Осуществляйте массовый прием и вывод средств с вашего магазина и смотрите расширенную статистику.');?></p>
            <div class="slick-slide-caption__footer"><a class="button button-primary" href="/signup"><?php echo Yii::t('app', 'Создать');?></a></div>
          </div>
        </div>
      </div>
    </div>
    <div class="slick-slider carousel-child" id="slider-child-carousel" data-for=".carousel-parent" data-arrows="false" data-loop="true" data-dots="false" data-swipe="false" data-fade="true" data-items="1" data-slide-to-scroll="1">
      <div class="item" style="background-image: url(/cmt/images/slider-slide-1.jpg);"></div>
      <div class="item" style="background-image: url(/cmt/images/slider-slide-2.jpg);"></div>
      <div class="item" style="background-image: url(/cmt/images/slider-slide-3.jpg);"></div>
    </div>
  </div>
</section>

<!-- What bitcoin offers you-->
<section class="section section-md bg-white text-center">
  <div class="container">
    <h6><?php echo Yii::t('app', 'Принимайте Биткоины на своем сайте');?></h6>
    <h2><?php echo Yii::t('app', 'Мы разработали <strong>оптимальную систему</strong> для бизнеса');?></h2>
    <p><span style="max-width: 430px;"><?php Yii::t('app', 'Интегрированный шлюз оплаты для таких криптовалют как Bitcoin, Litecoin, Ethereum, Bitcoin Cash и другие Altcoins с комиссией всего 0.5%.');?></span></p>
    <div class="row row-30">
      <div class="col-lg-6 wow fadeInUpSmall">
        <!-- Link Box--><a class="link-box" href="/signup"><span class="icon link-box__icon linearicons-mouse-right"></span>
          <div class="link-box__main">
            <h4><?php echo Yii::t('app', 'Моментальное подключение');?></h4>
            <p><?php echo Yii::t('app', 'Легко получить доступ к своему кошельку за несколько кликов.');?></p>
          </div></a>
      </div>
      <div class="col-lg-6 wow fadeInUpSmall" data-wow-delay=".02s">
        <!-- Link Box--><a class="link-box" href="/signup"><span class="icon link-box__icon linearicons-cart-exchange"></span>
          <div class="link-box__main">
            <h4><?php echo Yii::t('app', 'Принимайте криптовалюты');?></h4>
            <p><?php echo Yii::t('app', 'Начните принимать Биткоин и другие криптовалюты для расширения вашнго бизнеса.');?></p>
          </div></a>
      </div>
      <div class="col-lg-6 wow fadeInUpSmall" data-wow-delay=".1s">
        <!-- Link Box--><a class="link-box" href="/signup"><span class="icon link-box__icon linearicons-bag-dollar"></span>
          <div class="link-box__main">
            <h4><?php echo Yii::t('app', 'Настройте магазин');?></h4>
            <p><?php echo Yii::t('app', 'Начните принимать криптовалюту на своем сайте в несколько кликов.');?></p>
          </div></a>
      </div>
      <div class="col-lg-6 wow fadeInUpSmall" data-wow-delay=".12s">
        <!-- Link Box --><a class="link-box" href="/signup"><span class="icon link-box__icon linearicons-folder-search"></span>
          <div class="link-box__main">
            <h4><?php echo Yii::t('app', 'Минимальная модерация');?></h4>
            <p><?php echo Yii::t('app', 'Мы не просим вас много рассказывать о вашем бизнесе. Ваши данные защищены.');?></p>
          </div></a>
      </div>
    </div>
  </div>
</section>

<!-- A few words about bitcoin-->
<section class="section bg-gray-100">
  <div class="range">
    <div class="cell-lg-6 height-fill">
      <div class="thumbnail-video context-dark"><img class="thumbnail-video__image" src="/cmt/images/video-preview-1-961x598.jpg" alt="" width="961" height="598"/><a class="button-video" href="https://youtu.be/vBOf7ndTO5I" data-lightgallery="item" data-lg-fullscreen="false"></a>
      </div>
    </div>
    <div class="cell-lg-6 align-self-center">
      <div class="cell-inner">
        <div class="box-inset-1">
          <h6 class="wow fadeInRightSmall"><?php echo Yii::t('app', 'Несколько слов о нас');?></h6>
          <h2 class="wow fadeInRightSmall" data-wow-delay=".1s"><?php echo Yii::t('app', '<strong>Удобный</strong> API');?> </h2>
          <!-- Bootstrap tabs-->
          <div class="tabs-custom tabs-horizontal tabs-line wow fadeInRightSmall" id="tabs-about" data-wow-delay=".2s">
            <!-- Nav tabs-->
            <ul class="nav nav-tabs">
              <li class="nav-item" role="presentation"><a class="nav-link active" href="#tabs-about-1" data-toggle="tab"><?php echo Yii::t('app', 'Удобный API</a>');?></li>
              <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-about-2" data-toggle="tab"><?php echo Yii::t('app', 'Массовые выплаты</a>');?></li>
              <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-about-3" data-toggle="tab"><?php echo Yii::t('app', 'Криптовалюты</a>');?></li>
            </ul>
            <!-- Tab panes-->
            <div class="tab-content">
              <div class="tab-pane fade show active" id="tabs-about-1">
                <p><?php echo Yii::t('app', 'Простота подключения. Минимум затрат времени на установку.');?></p>
              </div>
              <div class="tab-pane fade" id="tabs-about-2">
                <p><?php echo Yii::t('app', 'Осуществляйте массовый прием и вывод средств с вашего магазина.');?></p>
              </div>
              <div class="tab-pane fade" id="tabs-about-3">
                <p><?php echo Yii::t('app', 'Поддерживаем Bitcoin, Litecoin, Ethereum, Bitcoin Cash, Dash, Zcash и другие.');?></p>
              </div>
            </div>
          </div><a class="button button-default-outline wow fadeIn" data-wow-delay=".3s" href="/signup"><?php echo Yii::t('app', 'Зарегистрировать');?></a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Be one step ahead-->
<section class="section bg-gray-700 particles-js-outer">
  <div id="particles-js"></div>
  <div class="container">
    <div class="row justify-content-center justify-content-xl-between align-items-end">
      <div class="col-md-10 col-lg-6">
        <div class="section-lg">
          <h6 class="wow fadeInLeftSmall"><?php echo Yii::t('app', 'Будь на шаг впереди всего Мира');?></h6>
          <h2 class="wow fadeInLeftSmall" data-wow-delay=".1s"><?php echo Yii::t('app', '<strong>Больше прибыли</strong> для Вашего бизнеса');?></h2>
          <p class="lead wow fadeInLeftSmall" data-wow-delay=".15s"><?php echo Yii::t('app', 'Подключите прием криптовалюты уже сегодня и увеличьте конверсию продаж.');?></p>
          <!-- RD Mailform-->
          <?php echo SubscribeWidget::widget();?>
          
          <p class="wow fadeInLeftSmall" data-wow-delay=".25s"><span style="opacity: .25;"><?php echo Yii::t('app', '*Мы обещаем не отправлять вам СПАМ');?></span></p>
        </div>
      </div>
      <div class="col-md-10 col-lg-6 col-xl-5">
        <div class="bitcoin-widget bitcoin-widget_windowed bitcoin-widget_windowed-1"><img class="bitcoin-widget__top-panel" src="/cmt/images/decoration-browser-panel-564x36.png" alt="" width="564" height="36"/>
          <div class="btcwdgt-chart" data-bw-theme="light"></div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- How it works-->
<section class="section section-lg bg-white text-center">
  <div class="container container-md-smaller">
    <div class="row justify-content-center">
      <div class="col-sm-10 col-md-12">
        <h6 class="wow fadeInUpSmall"><?php echo Yii::t('app', 'Как это работает?');?></h6>
        <h2 class="wow fadeInUpSmall" data-wow-delay=".1s"><span class="d-inline-block" style="max-width: 570px;"><?php echo Yii::t('app', 'Увеличте <strong>продажи</strong> подключив криптовалюты на свой сайт</span>');?></h2>
      </div>
    </div>
    <ul class="list-steps">
      <li class="list-steps__item wow fadeInLeftSmall" data-wow-delay=".1s">
        <div class="list-steps__item-counter"></div>
        <div class="list-steps__item-divider"></div>
        <div class="list-steps__item-main">
          <h4><a href="/signup"><?php echo Yii::t('app', 'Регистрация');?></a></h4>
          <p><?php echo Yii::t('app', 'Зарегистрируйтесь в личном кабинете.');?></p>
        </div>
      </li>
      <li class="list-steps__item wow fadeInLeftSmall" data-wow-delay=".2s">
        <div class="list-steps__item-counter"></div>
        <div class="list-steps__item-divider"></div>
        <div class="list-steps__item-main">
          <h4><a href="/signup"><?php echo Yii::t('app', 'Добавьте магазин');?></a></h4>
          <p><?php echo Yii::t('app', 'В личном кабинете создайте новый магазин.');?></p>
        </div>
      </li>
      <li class="list-steps__item wow fadeInLeftSmall" data-wow-delay=".3s">
        <div class="list-steps__item-counter"></div>
        <div class="list-steps__item-divider"></div>
        <div class="list-steps__item-main">
          <h4><a href="/signup"><?php echo Yii::t('app', 'Настройте платежи');?></a></h4>
          <p><?php echo Yii::t('app', 'Настройте свои платежи в пару кликов.');?></p>
        </div>
      </li>
    </ul>
  </div>
</section>

<!-- CTA-->
<section class="section parallax-container section-md bg-gray-700 section-overlay-1 text-center text-lg-left" data-parallax-img="/cmt/images/home-parallax-1.jpg">
  <div class="parallax-content">
    <div class="container">
      <div class="row row-30 align-items-center">
        <div class="col-lg-9 wow fadeInLeftSmall">
          <h2><?php echo Yii::t('app', 'Получите<strong> мультивалютный</strong>, удобный кошелек');?></h2>
          <p class="big"><?php echo Yii::t('app', 'Наш мультивалютный веб-кошелек - идеальное решение для бизнеса.');?></p>
        </div>
        <div class="col-lg-3 wow fadeInRightSmall text-lg-right">
          <a class="button button-default-outline" href = "/signup"><?php echo Yii::t('app', 'Получить сейчас');?></a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Features-->
<section class="section bg-white">
  <div class="container">
    <div class="row row-bordered-1">
      <div class="col-sm-6 col-lg-4 wow fadeIn">
              <!-- Box Minimal-->
              <article class="box-minimal"><span class="icon box-minimal__icon linearicons-lock"></span>
                <h4 class="box-minimal__title"><?php echo Yii::t('app', 'Безопасный');?></h4>
                <div class="box-minimal__divider"></div>
                <p><?php echo Yii::t('app', 'Будьте уверены в безопасности своего аккаунта и своих средств.');?></p>
              </article>
      </div>
      <div class="col-sm-6 col-lg-4 wow fadeIn" data-wow-delay=".1s">
              <!-- Box Minimal-->
              <article class="box-minimal"><span class="icon box-minimal__icon linearicons-cloud-upload"></span>
                <h4 class="box-minimal__title"><?php echo Yii::t('app', 'Удобный API');?></h4>
                <div class="box-minimal__divider"></div>
                <p><?php echo Yii::t('app', 'Простота подключения. Минимум затрат времени на установку.');?></p>
              </article>
      </div>
      <div class="col-sm-6 col-lg-4 wow fadeIn" data-wow-delay=".2s">
              <!-- Box Minimal-->
              <article class="box-minimal"><span class="icon box-minimal__icon linearicons-umbrella2"></span>
                <h4 class="box-minimal__title"><?php echo Yii::t('app', 'Грамотная поддержка');?></h4>
                <div class="box-minimal__divider"></div>
                <p><?php echo Yii::t('app', 'Мы оказываем 24/7 экспертную поддержку нашим клиентам.');?></p>
              </article>
      </div>
      <div class="col-sm-6 col-lg-4 wow fadeIn">
              <!-- Box Minimal-->
              <article class="box-minimal"><span class="icon box-minimal__icon linearicons-tablet2"></span>
                <h4 class="box-minimal__title"><?php echo Yii::t('app', 'Статистика');?></h4>
                <div class="box-minimal__divider"></div>
                <p><?php echo Yii::t('app', 'Расширенная статистика по всем входящим и исходящим платежам.');?></p>
              </article>
      </div>
      <div class="col-sm-6 col-lg-4 wow fadeIn" data-wow-delay=".1s">
              <!-- Box Minimal-->
              <article class="box-minimal"><span class="icon box-minimal__icon linearicons-credit-card"></span>
                <h4 class="box-minimal__title"><?php echo Yii::t('app', 'Прозрачность');?></h4>
                <div class="box-minimal__divider"></div>
                <p><?php echo Yii::t('app', 'Отсутствие скрытых комиссий и платежей.');?></p>
              </article>
      </div>
      <div class="col-sm-6 col-lg-4 wow fadeIn" data-wow-delay=".2s">
              <!-- Box Minimal-->
              <article class="box-minimal"><span class="icon box-minimal__icon linearicons-sync"></span>
                <h4 class="box-minimal__title"><?php echo Yii::t('app', 'Массовые платежи');?></h4>
                <div class="box-minimal__divider"></div>
                <p><?php echo Yii::t('app', 'Осуществляйте массовый прием и вывод средств с вашего магазина.');?></p>
              </article>
      </div>
    </div>
  </div>
</section>

<section class="section parallax-container section-md bg-gray-700 section-overlay-1 text-center text-lg-left" data-parallax-img="/cmt/images/home-parallax-1.jpg">
  <div class="parallax-content">
    <div class="container">
      <div class="row row-30 align-items-center">
        <div class="col-lg-9 wow fadeInLeftSmall">
          <h2><?php echo Yii::t('app', 'Получите<strong> мультивалютный</strong>, удобный кошелек');?></h2>
          <p class="big"><?php echo Yii::t('app', 'Наш мультивалютный веб-кошелек - идеальное решение для бизнеса.');?></p>
        </div>
        <div class="col-lg-3 wow fadeInRightSmall text-lg-right">
          <a class="button button-default-outline" href = "/signup"><?php echo Yii::t('app', 'Получить сейчас');?></a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Easily convert bitcoin into any currency-->
<section class="section section-md bg-white">
  <div class="container">
    <div class="row row-30 justify-content-center justify-content-lg-between align-items-end">
      <div class="col-md-10 col-lg-6">
        <div class="box-inset-4">
          <h2 class="wow fadeInLeftSmall"><?php echo Yii::t('app', 'Рассчситать стоимость <strong>BitCoin</strong> очень просто');?></h2>
          <p class="wow fadeInLeftSmall" data-wow-delay=".1s">
          <?php echo Yii::t('app', 'Миллионы людей полагаются на удачу BitCoin! Просчитайте текущий курс покупки/продажи BitCoin');?></p>
        </div>
        <form class="form form-lg form-calculator form-btc-calculator wow fadeInLeftSmall" data-wow-delay=".2s">
          <input class="form-input" name="btc-calculator-value" value="1">
          <div class="form-info">BTC =</div>
          <input class="form-input form-input-result" name="btc-calculator-result">
          <div class="form-wrap">
            <select class="form-input select-currency select-primary" name="btc-calculator-currency" data-dropdown-class="select-primary-dropdown"></select>
          </div>
        </form>
        <p class="wow fadeInLeftSmall" data-wow-delay=".2s"><span style="opacity: .5;"><?php echo Yii::t('app', 'Информация обновялется каждые 15 минут');?></span></p>
      </div>
    </div>
  </div>
</section>