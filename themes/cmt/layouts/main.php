<?php
use app\modules\menu\widgets\MenuWidget;
use app\assets\cmt\FrontBottomAsset;
use app\assets\cmt\FrontTopAsset;
use yii\helpers\Html;

FrontTopAsset::register($this);
FrontBottomAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>

    <title><?= Html::encode($this->title) ?></title>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/main-meta'); ?>

    <?= Html::csrfMetaTags() ?>

    <?php $this->head(); ?>

  </head>
  <body>
  	<?php $this->beginBody(); ?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/main-preloader'); ?>
    <div class="page">

	  <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']);?>

	  <?php echo $content; ?>

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/main-footer'); ?>

    </div>

    <div class="snackbars" id="form-output-global"></div>

    <?php $this->endBody(); ?>

  </body>
</html>
<?php $this->endPage(); ?>