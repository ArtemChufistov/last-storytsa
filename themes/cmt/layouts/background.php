<?php
use yii\helpers\Html;
use app\assets\cmt\ProfileTopAsset;
use app\assets\cmt\ProfileBottomAsset;

ProfileTopAsset::register($this);
ProfileBottomAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/profile-meta'); ?>

    <?= Html::csrfMetaTags() ?>

  	<title><?= Html::encode($this->title) ?></title>

  	<?php $this->head(); ?>

  </head>
  <body class="fixed-header ">
  	<?php $this->beginBody(); ?>

    <div class="preloader">
      <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="login-register">
      <div id="particles-js"></div>
      <div class="login-box" style = "position: relative; z-index: 100;">
        <div class="white-box">
          <?php echo $content;?>
        </div>
      </div>

    </section>

    <?php $this->endBody(); ?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/particles'); ?>

  </body>
</html>
<?php $this->endPage(); ?>