<?php
use app\modules\profile\widgets\VerifyEmailWidget;
use app\modules\menu\widgets\MenuWidget;
use app\assets\cmt\OfficeBottomAsset;
use app\assets\cmt\OfficeTopAsset;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;

OfficeTopAsset::register($this);
OfficeBottomAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/profile-meta'); ?>

    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head(); ?>

  </head>
  <body class="fix-header">
    <?php $this->beginBody(); ?>

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/preloader'); ?>

      <div id="wrapper">

        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/office-navbar'); ?>

        <?php echo MenuWidget::widget(['menuName' => 'profileMenu', 'view' => 'office-menu']);?>

        <div id="page-wrapper">
          <div class="container-fluid">

            <?php echo $content;?>

            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/office-right-sidebar'); ?>

          </div>
        </div>

        <footer class="footer text-center"> <?php echo Yii::t('app', '2018 &copy; Crypchant - crypto currency merchant');?></footer>

      </div>

    <?= VerifyEmailWidget::widget(['user' => $this->params['user']->identity]); ?>

    <?php $this->endBody(); ?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/stabbs'); ?>

  </body>
</html>
<?php $this->endPage(); ?>