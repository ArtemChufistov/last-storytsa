<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Ошибка');
$this->context->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/background';

?>
<div style = "text-align: center;">
	<h3 class="box-title m-b-20"><?php echo $this->title;?></h3>
	<p><?=$message;?></p>
	</br/>
	</br/>
	<a href = "/" class = "btn btn-info btn-block text-uppercase waves-effect waves-light"><?php echo Yii::t('app', 'Перейти на главную');?></a>

	<a href = "/login" class = "btn btn-primary btn-block text-uppercase waves-effect waves-light"><?php echo Yii::t('app', 'В личный кабинет');?></a>
</div>