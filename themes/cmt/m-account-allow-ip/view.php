<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountAllowIp */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'M Account Allow Ips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maccount-allow-ip-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'm_account_id',
            'allow_ip',
            'created_at',
            'is_delete',
        ],
    ]) ?>

</div>
