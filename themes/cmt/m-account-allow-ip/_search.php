<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\search\MAccountAllowIpSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maccount-allow-ip-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'm_account_id') ?>

    <?= $form->field($model, 'allow_ip') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'is_delete') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
