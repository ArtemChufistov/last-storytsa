<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountAllowIp */

$this->title = 'Create M Account Allow Ip';
$this->params['breadcrumbs'][] = ['label' => 'M Account Allow Ips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maccount-allow-ip-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
