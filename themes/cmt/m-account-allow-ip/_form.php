<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountAllowIp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maccount-allow-ip-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'm_account_id')->textInput() ?>

    <?= $form->field($model, 'allow_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'is_delete')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
