<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountAllowIp */

$this->title = 'Update M Account Allow Ip: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'M Account Allow Ips', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maccount-allow-ip-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
