<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\admin\modules\altcoin\models\search\MAccountAllowIpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'M Account Allow Ips';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maccount-allow-ip-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create M Account Allow Ip', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'm_account_id',
            'allow_ip',
            'created_at',
            'is_delete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
