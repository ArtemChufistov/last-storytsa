<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountWallet */

$this->title = 'Update M Account Wallet: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'M Account Wallets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maccount-wallet-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
