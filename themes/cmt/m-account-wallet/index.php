<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\admin\modules\altcoin\models\search\MAccountWalletSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'M Account Wallets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maccount-wallet-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create M Account Wallet', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'm_account_id',
            'currency_wallet',
            'currency_type',
            //'balance',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
