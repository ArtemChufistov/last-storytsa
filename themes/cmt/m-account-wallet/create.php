<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountWallet */

$this->title = 'Create M Account Wallet';
$this->params['breadcrumbs'][] = ['label' => 'M Account Wallets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maccount-wallet-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
