<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\admin\modules\altcoin\models\MAccountWallet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maccount-wallet-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'm_account_id')->textInput() ?>

    <?= $form->field($model, 'currency_wallet')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'balance')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
