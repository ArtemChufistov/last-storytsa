<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\captcha\Captcha;
use yii\web\View;

$this->title = Yii::t('app', 'Связаться с нами');

$this->params['breadcrumbs']= [ [
    'label' => $this->title,
    'url' => '/contact'
  ],
]

?>

<style type="text/css">
  #contactmessage-captcha-image{
    cursor: pointer;
  }
</style>

<?php if(Yii::$app->session->hasFlash('success')): ?>

    <div class="successModal modal modal-login-register fade" id="modal-login-register" tabindex="-1" role="dialog" aria-labelledby="modal-login-register-label" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><?= Yii::t('app', 'Ваше сообщение принято');?></h4>
          </div>
          <div class="modal-body">
            <p><?= Yii::t('app', 'Спасибо, что оставили нам сообщение, мы обязательно свяжемся с вам в ближайшее время');?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Закрыть');?></button>
            <button type="button" class="btn btn-primary" data-dismiss="modal"><?= Yii::t('app', 'Ok');?></button>
          </div>
        </div>
      </div>
    </div>

  <?php echo $this->registerJs("$('.successModal').modal('show');", View::POS_END);?>

<?php endif;?>

<!-- Breadcrumbs -->
<section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(/cmt/images/breadcrumbs-image-7.jpg);">
  <div class="container">
    <div class="breadcrumbs-custom__main">
      <h1 class="breadcrumbs-custom-title"><?php echo Yii::t('app', 'Контакты');?></h1>
    </div>
    <ul class="breadcrumbs-custom__path">
      <li><a href="index.html"><?php echo Yii::t('app', 'Главная');?></a></li>
      <li class="active"><?php echo Yii::t('app', 'Контакты');?></li>
    </ul>
  </div>
</section>

<!-- Mailform-->
<section class="section section-lg">
  <div class="container">
    <div class="row justify-content-center justify-content-lg-between row-2-columns-bordered row-50">
      <div class="col-md-12 col-lg-12">
        <h3><?php echo Yii::t('app', 'Напишите нам');?></h3>

        <?php $form = ActiveForm::begin([
              'options' => [

              ]
        ]); ?>
          <div class="row row-20">
            <div class="col-md-6">
              <div class="form-wrap">
                <?= $form->field($model, 'name')->textInput(['placeholder' => Yii::t('app', 'Иван')]) ?>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-wrap">
                <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app', 'example@example.ru')]) ?>
              </div>
            </div>
            <div class="col-12">
              <div class="form-wrap">
                <?= $form->field($model, 'message')->textArea(['rows' => '6', 'placeholder' => Yii::t('app', 'Ваше сообщение')]); ?>
              </div>
            </div>
            <div class="col-md-6">
                <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
                      'captchaAction' => '/contact/contact/captcha',
                      'options' => [
                          'class' => 'form-control',
                          'placeholder' => $model->getAttributeLabel('captcha')
                      ],
                      'template' => '{input} <div style = "margin-top: 5px;">{image}</div>',
                  ]);
                  ?>
            </div>
            <div class="col-md-6">
              <div class = "form-group" style = "padding-top: 22px;">
                <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'button button-block button-primary']) ?>
              </div>
            </div>
          </div>
        <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</section>