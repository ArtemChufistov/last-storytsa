<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

?>

<?php $form = ActiveForm::begin([
  'options' => [
      'class' => 'form form-lg form-inline wow fadeInLeftSmall',
  ],
]); ?>
  <div class="form-wrap">

    <?= $form->field($subscriber, 'mail')->textInput([
            'maxlength' => true,
            'class' => 'form-input',
            'placeholder' => Yii::t('app', 'Введите свой E-mail'),

        ])->label(''); ?>

  </div>
  <div class="form-button">
    <button class="button button-primary" type="submit"><?php echo Yii::t('app', 'Подтвердить');?></button>
  </div>
<?php ActiveForm::end();?>


<?php if(Yii::$app->session->hasFlash('succsessSubscribe')): ?>

    <div class="successModal modal modal-login-register fade" id="modal-login-register" tabindex="-1" role="dialog" aria-labelledby="modal-login-register-label" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><?= Yii::t('app', 'Ваше сообщение принято');?></h4>
          </div>
          <div class="modal-body">
            <p style = "color: #151515;"><?php echo Yii::$app->session->getFlash('succsessSubscribe');?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Закрыть');?></button>
            <button type="button" class="btn btn-primary" data-dismiss="modal"><?= Yii::t('app', 'Ok');?></button>
          </div>
        </div>
      </div>
    </div>

  <?php echo $this->registerJs("$('.successModal').modal('show');", View::POS_END);?>

<?php endif;?>