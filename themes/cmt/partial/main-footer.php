<?php
use app\modules\profile\widgets\SubscribeWidget;
use app\modules\menu\widgets\MenuWidget;
use yii\helpers\Url;
?>

<footer class="section footer-classic context-dark">
  <div class="container">
    <div class="footer-classic__main">
      <div class="row row-50">
        <div class="col-lg-6">
          <div class="unit unit-spacing-sm flex-column flex-sm-row align-items-sm-center">
            <div class="unit-left"><a class="brand" href="/"><img class="brand-logo-dark" src="/cmt/crypchant_logo_125px_white.png" alt="" style="max-height: 100px;" height="125" srcset="/cmt/crypchant_logo_125px_white.png 2x"/><img class="brand-logo-light" style="max-height: 100px;" src="/cmt/crypchant_logo_125px_white.png" alt="" height="125" srcset="/cmt/crypchant_logo_125px_white.png 2x"/></a>
            </div>
            <div class="unit-body">
              <p><span style="max-width: 380px;"><?php echo Yii::t('app', 'Прием платежей для вашего бизнеса с Лучшим сервисом для Вас. Правильное решение для умной компании.');?></span></p>
            </div>
          </div>
          <div class="group group-lg group-middle">
            <p class="large"></p>
            <div class="group-item">
              <ul class="list-inline list-inline-sm">
                <!--<li><a class="icon icon-lg link-default mdi mdi-facebook" href="#"></a></li>
                <li><a class="icon icon-lg link-default mdi mdi-twitter" href="#"></a></li>
                <li><a class="icon icon-lg link-default mdi mdi-youtube-play" href="#"></a></li>
                <li><a class="icon icon-lg link-default mdi mdi-instagram" href="#"></a></li>-->
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="box-inset-3">
            <h4><?php echo Yii::t('app', 'Подпишитесь на наши новости');?></h4>

            <?php echo SubscribeWidget::widget(['view' => 'subscribe-footer']);?>

          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="footer-classic__aside">

      <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-footer-menu']);?>

      <div class="list-wrap">
        <ul class="list-bordered">
          <li>
            <!-- Rights-->
            <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span>&nbsp;</span><span>crypchant.com</span><span></p>
          </li>
          <li><a href="mailto:info@cmt.finance">info@crypchant.com</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>