<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo $breadcrumbs[count($breadcrumbs) - 1]['title'];?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <a href="/m-account/all-transactions" class="btn btn-info pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light"><?php echo Yii::t('app', 'Мои транзакции');?></a>
        <ol class="breadcrumb">
        	<?php foreach(array_reverse($breadcrumbs) as $num => $breadcrumb): ?>
            	<li><a href="<?php echo $breadcrumb['link'];?>" <?php if ($num > 0):?>class="active"<?php endif;?>><?= $breadcrumb['title'];?></a></li>
            <?php endforeach;?>
            <li><a class="active" href = "/"><?php echo Yii::t('app', 'Главная');?></a></li>
        </ol>
    </div>
</div>