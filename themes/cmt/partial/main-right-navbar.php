<div class="rd-navbar-panel">
  <!-- RD Navbar Toggle-->
  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-outer"><span></span></button>
  <!-- RD Navbar Brand-->
  <div class="rd-navbar-brand"><a class="brand" href="index.html"><img class="brand-logo-dark" src="/cmt/сrypchant_logo_150px.png" alt="" height="100" srcset="/cmt/сrypchant_logo_150px.png 2x"/><img class="brand-logo-light" src="/cmt/сrypchant_logo_150px.png" alt="" height="100" srcset="/cmt/сrypchant_logo_150px.png 2x"/></a>
  </div>
  <div class="rd-sidebar">
    <button class="rd-sidebar__button" data-rd-navbar-toggle="#rd-sidebar__list">
      <svg class="rd-sidebar__shape" version="1.1" x="0px" y="0px" width="28px" height="27px" viewbox="0 0 28 27">
        <path d="M10,7c0,1.657-1.343,3-3,3H3c-1.657,0-3-1.343-3-3V3c0-1.657,1.343-3,3-3h4c1.657,0,3,1.343,3,3V7z"></path>
        <path d="M28,7c0,1.657-1.343,3-3,3h-4c-1.657,0-3-1.343-3-3V3c0-1.657,1.343-3,3-3h4c1.657,0,3,1.343,3,3V7z"></path>
        <path d="M10,24c0,1.657-1.343,3-3,3H3c-1.657,0-3-1.343-3-3v-4c0-1.657,1.343-3,3-3h4c1.657,0,3,1.343,3,3V24z"></path>
        <path d="M28,24c0,1.657-1.343,3-3,3h-4c-1.657,0-3-1.343-3-3v-4c0-1.657,1.343-3,3-3h4c1.657,0,3,1.343,3,3V24z"></path>
      </svg>
    </button>
    <ul class="rd-sidebar__list context-dark" id="rd-sidebar__list">
      <li class="rd-sidebar__list-item">
        <div class="rd-sidebar__list-item-inner">
          <div class="icon rd-sidebar__list-item-icon linearicons-envelope"></div>
          <div class="rd-sidebar__list-item-main"><a class="rd-sidebar__list-item-link" href="/contact/feedback"><?php echo Yii::t('app', 'Связаться с нами');?></a></div>
        </div>
      </li>
      <li class="rd-sidebar__list-item">
        <div class="rd-sidebar__list-item-inner">
          <div class="icon rd-sidebar__list-item-icon linearicons-cash-dollar"></div>
          <div class="rd-sidebar__list-item-main"><a class="rd-sidebar__list-item-link" href="/signup"><?php echo Yii::t('app', 'Создать кошелёк');?></a></div>
        </div>
      </li>
      <li class="rd-sidebar__list-item">
        <div class="rd-sidebar__list-item-inner">
          <div class="icon rd-sidebar__list-item-icon linearicons-share2"></div>
          <div class="rd-sidebar__list-item-main">
            <ul class="list-inline list-inline-xs">
              <li><a class="icon icon-md link-light mdi mdi-facebook" href="http://facebook.com"></a></li>
              <li><a class="icon icon-md link-light mdi mdi-twitter" href="http://twitter.com"></a></li>
              <li><a class="icon icon-md link-light mdi mdi-youtube-play" href="http://youtube.ru"></a></li>
              <li><a class="icon icon-md link-light mdi mdi-instagram" href="https://www.instagram.com"></a></li>
            </ul>
          </div>
        </div>
      </li>
      <li class="rd-sidebar__list-item">
        <div class="rd-sidebar__list-item-inner">
          <div class="icon rd-sidebar__list-item-icon linearicons-bubble-dots"></div>
          <div class="rd-sidebar__list-item-main"><a class="rd-sidebar__list-item-link" href="/contact/feedback"><?php echo Yii::t('app', 'Вопросы');?></a></div>
        </div>
      </li>
    </ul>
  </div>
</div>