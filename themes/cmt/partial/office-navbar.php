<?php
use app\modules\merchant\components\merchant\helpers\MerchantHelper;

$user = $this->params['user']->identity;
?>

<style type="text/css">
.svg-wrapper{
    overflow: hidden;
    margin-top: 10px;
    height: 50px;
    margin-left: 12px;
}
</style>

<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <div class="top-left-part">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/blue-logo', ['scale' => 0.5]); ?>
        </div>
        <ul class="nav navbar-top-links navbar-left" style = "display: none;">
            <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
            <li class="dropdown">
                <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="javascript:void(0)"> <i class="mdi mdi-blur"></i>
                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                </a>
                <ul class="dropdown-menu mailbox animated bounceInDown">
                    <li>
                        <div class="drop-title"><?php echo Yii::t('app', 'Транзакций в обработке: {count}', ['count' => 4]);?></div>
                    </li>
                    <li>
                        <div class="message-center">
                            <a href="javascript:void(0)">
                                <div class="user-img"> <i class="ti-reload" style = "font-size: 30px;"></i> </div>
                                <div class="mail-contnet">
                                    <h5>0.00245 BTC</h5> <span class="mail-desc"><?php echo Yii::t('app', 'Транзакция BitCoin');?></span> <span class="time">14:25 13/05/2018</span> </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <a class="text-center" href="javascript:void(0);"> <strong><?php echo Yii::t('app', 'Список транзакций');?></strong> <i class="fa fa-angle-right"></i> </a>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li>
                <div class="app-search hidden-sm hidden-xs m-r-10">
                    <div class="form-control" style = "font-size: 14px; width: auto;">
                        <span style = "margin-top: -7px; display: block; float: left;"> <?php echo Yii::t('app', 'Баланс:');?></span> 
                        <span style = "margin-top: -7px; display: block; float: right; margin-left: 10px;"> <?= MerchantHelper::totalUserBalanceByTransactionInUSD() ?> USD</span>
                    </div>
                </div>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="javascript:void(0)"> <img src="<?php echo $user->getImage(); ?>" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo $user->login; ?></b><span class="caret"></span> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-img"><img src="<?php echo $user->getImage(); ?>" alt="user" /></div>
                            <div class="u-text">
                                <h4><?php echo $user->login; ?></h4>
                                <p class="text-muted"><?php echo $user->email; ?></p><a href="profile.html" class="btn btn-rounded btn-danger btn-sm"><?php echo Yii::t('app', 'Перейти в профиль');?></a></div>
                        </div>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/office/user"><i class="ti-user"></i> <?php echo Yii::t('app', 'Мой профиль');?></a></li>
                    <li><a href="javascript:void(0)"><i class="ti-wallet"></i> <?php echo Yii::t('app', 'Баланс');?></a></li>
                    <li><a href="javascript:void(0)"><i class="ti-email"></i> <?php echo Yii::t('app', 'Транзакции');?></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/office/support"><i class="ti-settings"></i> <?php echo Yii::t('app', 'Техническая поддержка');?></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/logout"><i class="fa fa-power-off"></i> <?php echo Yii::t('app', 'Выход');?></a></li>
                </ul>

            </li>
        </ul>
    </div>
</nav>