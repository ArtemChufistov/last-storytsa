<?php
use app\modules\merchant\models\search\MAccountSearch;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

$searchModel = new MAccountSearch();
$dataProvider = $searchModel->search(ArrayHelper::merge(
    Yii::$app->request->queryParams,
    [$searchModel->formName() => ['user_id' => Yii::$app->user->identity->id, 'is_delete' => 0]]
));
$dataProvider->pagination = ['pageSize' => 1000];
$dataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];
?>
<style type="text/css">
	.sidebar-m-link{
		color: #797979 !important;
		width: 100% !important;
	}
</style>
<div class="right-sidebar">
    <div class="slimscrollright">
        <div class="rpanel-title"> <?php echo Yii::t('app', 'Последние транзакции');?> <span><i class="ti-close right-side-toggle"></i></span> </div>
        <div class="r-panel-body">
            <ul id="themecolors" class="m-t-20">
            	<?php echo ListView::widget([
				'dataProvider' => $dataProvider,
				'layout' => "{items}",
				'itemView' => function($model, $key, $index) {
					return '<li><a class = "sidebar-m-link" href = "/m-account/wallets?id=' . $model->id . '"> ' .  ( $model->id ). '. <strong>' . $model->name . '</strong></a></li>';
					},
				]);
				?>
            </ul>
        </div>
    </div>
</div>