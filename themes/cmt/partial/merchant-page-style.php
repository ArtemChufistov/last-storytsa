<style>
.shop-info {
	padding-top: 20px;
}
.shop-info .box-title{
	font-size: 13px !important;
}
.shop-info .box-title span {
	font-weight: normal;
	margin-right: 10px;
}
.coin-wrap .coin-table{
	width: 100%;
}
.coin-wrap{
	width: 100% !important;
}
.text-left{
	text-align: left;
}
.text-right{
	text-align: right;
}
.float-left{
	float: left;
}
.margin-right-20{
	margin-right: 20px; 
}
.padding-top-20{
	padding-top: 20px;
}
</style>