<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Вход на сайт');
?>

<?php $form = ActiveForm::begin([
    'id' => 'loginform',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}",
        'options' => [
            'tag' => 'div',
        ],
    ],
    'options' => [
        'class' => 'form-horizontal form-material',
        'role' => 'form'
    ]
  ]); ?>

  <h3 class="box-title m-t-40 m-b-0"><?php echo $this->title;?></h3><small><?php echo Yii::t('app', 'Введите свой логин и пароль');?></small> 
  <div class="form-group m-t-20">
    <div class="col-xs-12">
      <?= $form->field($model, 'loginOrEmail')->textInput([
          'maxlength' => true,
          'placeholder' => $model->getAttributeLabel('loginOrEmail'),
          'class' => 'form-control',
          'required' => true
      ]); ?>
    </div>
  </div>
  <div class="form-group ">
    <div class="col-xs-12">
      <?= $form->field($model, 'password')->textInput([
          'maxlength' => true,
          'type' => 'password',
          'placeholder' => $model->getAttributeLabel('password'),
          'class' => 'form-control',
          'required' => true
      ]); ?>
    </div>
  </div>
  <div class="form-group text-center m-t-20">
    <div class="col-xs-12">
      <?= Html::submitButton('<i class="glyphicon glyphicon-log-in"></i> '.Yii::t('user', 'Войти'), [
            'class' => 'btn btn-success btn-block text-uppercase waves-effect waves-light',
            'name' => 'login-button']) ?>
    </div>
  </div>
  <div class="form-group">
      <div class="col-md-12">
        <?=Html::a('<i class="glyphicon glyphicon-share-alt"></i> ' . Yii::t('user', 'Восстановить пароль'), ['/resetpasswd'], [
            'class' => 'btn btn-info btn-block text-uppercase waves-effect waves-light'
        ])?>
      </div>
    </div>
  <div class="form-group m-b-0">
    <div class="col-sm-12 text-center">
      <p><?php echo Yii::t('app', 'Вы ещё не зарегистрированы ?');?> <a href="/signup" class="text-primary m-l-5"><b><?php echo Yii::t('app', 'Зарегистрироваться');?></b></a></p>
    </div>
  </div>

<?php ActiveForm::end(); ?>