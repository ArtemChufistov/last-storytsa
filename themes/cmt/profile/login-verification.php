<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Вход на сайт');
?>

<?php $form = ActiveForm::begin([
    'id' => 'loginform',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}",
        'options' => [
            'tag' => 'div',
        ],
    ],
    'options' => [
        'class' => 'form-horizontal form-material',
        'role' => 'form'
    ]
  ]); ?>


  <h3 class="box-title m-t-40 m-b-0"><?php echo $this->title;?></h3><small><?php echo Yii::t('app', 'Введите Код Аутентификации');?></small>
  <div class="form-group m-t-20">
    <div class="col-xs-12">
      <?= $form->field($model, 'code')->textInput([
          'maxlength' => true,
          'placeholder' => $model->getAttributeLabel(Yii::t('app', 'Код Аутентификации')),
          'class' => 'form-control',
          'required' => true
      ]); ?>
    </div>
  </div>
  <div class="form-group text-center m-t-20">
    <div class="col-xs-12">
      <?= Html::submitButton('<i class="glyphicon glyphicon-log-in"></i> '.Yii::t('user', 'Войти'), [
            'class' => 'btn btn-success btn-block text-uppercase waves-effect waves-light',
            'name' => 'login-button']) ?>
    </div>
  </div>


<?php ActiveForm::end(); ?>