<?php
use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use lowbase\user\components\AuthChoice;
use app\modules\mainpage\models\Preference;

$this->title = Yii::t('user', 'Регистрация');
?>


<?php $form = ActiveForm::begin([
    'id' => 'loginform',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}",
        'options' => [
            'tag' => 'div',
        ],
    ],
    'options' => [
        'class' => 'form-horizontal form-material',
        'role' => 'form'
    ]
  ]); ?>

  <?php $readonlyPref = Preference::find()->where(['key' => Preference::KEY_USER_REG_CAN_PARENT_EDIT])->one()->value == 1 ? false : true;?>

  <h3 class="box-title m-t-40 m-b-0"><?php echo $this->title;?></h3><small><?php echo Yii::t('app', 'Заполните поля');?></small> 
  <div class="form-group m-t-20">
    <div class="col-xs-12">
      <?= $form->field($model, 'login')->textInput([
          'maxlength' => true,
          'placeholder' => $model->getAttributeLabel('login'),
          'class' => 'form-control',
          'required' => true
      ]); ?>
    </div>
  </div>
  <div class="form-group ">
    <div class="col-xs-12">
      <?= $form->field($model, 'email')->textInput([
          'maxlength' => true,
          'placeholder' => $model->getAttributeLabel('email'),
          'class' => 'form-control',
          'required' => true
      ]); ?>
    </div>
  </div>
  <div class="form-group ">
    <div class="col-xs-12">
      <?= $form->field($model, 'password')->textInput([
          'maxlength' => true,
          'type' => 'password',
          'placeholder' => $model->getAttributeLabel('password'),
          'class' => 'form-control',
          'required' => true
      ]); ?>
    </div>
  </div>
  <div class="form-group ">
    <div class="col-xs-12">
      <?= $form->field($model, 'passwordRepeat')->textInput([
          'maxlength' => true,
          'type' => 'password',
          'placeholder' => $model->getAttributeLabel('passwordRepeat'),
          'class' => 'form-control',
          'required' => true
      ]); ?>
    </div>
  </div>
  <div class="form-group form-group-default">
      <div class="col-xs-12">
          <div class="checkbox checkbox-primary pull-left p-t-0" style = "margin-top: 0px; margin-bottom: 0px;">
              <?php $labelLicense = Yii::t('app', 'Я согласен с условиями') . ' ' . Html::a(Yii::t('app', 'лицензионного соглашения'), ['/licenseagreement'], ['target' => '_blank']);?>

              <?= $form->field($model, 'licenseAgree', ['template' => '{input}{label}{error}', 'options' => ['tag' => false, 'required' => "required",]])->checkBox([], false)->label($labelLicense,['class'=>'label-text']);?>
          </div>
      </div>
  </div>

  <div class="form-group form-group-default">
    <div class = "col-xs-12">
        <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
            'imageOptions' => [
                'id' => 'my-captcha-image',
            ],
            'captchaAction' => '/profile/profile/captcha',
            'options' => [
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('captcha'),
                'required' => "required",
            ],
            'template' => '
            <div class="row">
              <div class="col-lg-8">{input}</div>
            </div>
            <div class="row">
              <div class="col-lg-4">{image}</div>
            </div>',
        ]);
        ?>
    </div>
  </div>

  <div class="form-group text-center m-t-20">
    <div class="col-xs-12">
      <?= Html::submitButton('<i class="glyphicon glyphicon-user"></i> '.Yii::t('user', 'Регистрация'), [
            'class' => 'btn btn-info btn-block text-uppercase waves-effect waves-light',
            'name' => 'login-button']) ?>
    </div>
  </div>

<?php ActiveForm::end(); ?>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
      <?= Yii::t('app', 'Если регистрировались ранее, можете')?> <?=Html::a(Yii::t('app', '<strong>войти на сайт</strong>'), ['/login'], ['class' => ['text-picton-blue']])?>,
      <?= Yii::t('app', 'используя Логин или E-mail')?>
  </div>
</div>