<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Восстановление пароля');
$this->context->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/background';

?>

<?php $successMessage = Yii::$app->session->getFlash('reset-success');?>

<?php if (!empty($successMessage)):?>
	<?php echo $successMessage;?>
<?php endif;?>

<?php $form = ActiveForm::begin([
    'id' => 'loginform',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}",
        'options' => [
            'tag' => 'div',
        ],
    ],
    'options' => [
        'class' => 'form-horizontal form-material',
        'role' => 'form'
    ]
  ]); ?>

	<h3 class="box-title m-b-20"><?php echo $this->title;?></h3>
	<div class="form-group ">
	  <div class="col-xs-12">
	      <?= $form->field($forget, 'email')->textInput([
	          'maxlength' => true,
	          'placeholder' => $forget->getAttributeLabel('email'),
	          'class' => 'form-control',
	          'required' => true
	      ]); ?>
	  </div>
	</div>
	<div class="form-group">
	  <div class="col-xs-12">
	      <?= $form->field($forget, 'password')->textInput([
	          'maxlength' => true,
	          'type' => 'password',
	          'placeholder' => $forget->getAttributeLabel('password'),
	          'class' => 'form-control',
	          'required' => true
	      ]); ?>
	  </div>
	</div>
	<div class="form-group">
	  <div class="col-xs-12">
	        <?= $form->field($forget, 'captcha')->widget(Captcha::className(), [
	            'captchaAction' => '/profile/profile/captcha',
	            'options' => [
	                'class' => 'form-control',
	                'placeholder' => $forget->getAttributeLabel('captcha')
	            ]
	        ]);
	        ?>
	  </div>
	</div>
	<div class="form-group text-center m-t-20">
	  <div class="col-xs-12">
	    <?= Html::submitButton(Yii::t('user', 'Сменить пароль'), [
                'class' => 'btn btn-primary btn-block text-uppercase waves-effect waves-light',
                'name' => 'login-button']) ?>
	  </div>
	</div>

<?php ActiveForm::end();?>

<div class="offset-top-30 text-sm-left text-dark text-extra-small row">
  <div class="form-group " style = "margin-top: 20px;">
    <div class="col-xs-12">
      <?=Html::a(Yii::t('user', 'Личный кабинет'), ['/login'], ['class' => 'text-picton-blue'])?>
    </div>
    <div class="col-xs-12">
      <?=Html::a(Yii::t('user', 'Регистрация'), ['/signup'], ['class' => 'text-picton-blue'])?>
      </div>
  </div>
</div>