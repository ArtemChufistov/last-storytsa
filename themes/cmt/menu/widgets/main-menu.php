<?php
use yii\web\View;
use yii\helpers\Url;
?>

<header class="section page-header">
  <div class="rd-navbar-wrap">
    <nav class="rd-navbar rd-navbar-classic" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="40px" data-xl-stick-up-offset="40px" data-xxl-stick-up-offset="40px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
      <div class="rd-navbar-main">
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/main-right-navbar'); ?>

        <div class="rd-navbar-nav-outer">
          <ul class="rd-navbar-nav">
            <?php foreach ($menu->children(1)->all() as $num => $child): ?>
            
            <li class="rd-nav-item <?php if (Url::to([$requestUrl]) == Url::to([$child->link])):?>active<?php endif;?>">
              <a class="rd-nav-link" href="<?=Url::to([$child->link]);?>"><?php echo $child->title?></a>

              <?php $children = $child->children(2)->all();?>
              <?php if (!empty($children)):?>
              <ul class="rd-menu rd-navbar-megamenu">
                <li class="rd-megamenu-item">
                  <ul class="rd-megamenu-list">
                    <?php foreach ($children as $num => $childChild): ?>
                      <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="<?=Url::to([$childChild->link]);?>"><?php echo $childChild->title?></a></li>
                    <?php endforeach;?>
                  </ul>
                </li>
              </ul>
              <?php endif;?>
            </li>
            <?php endforeach;?>
          </ul>
        </div>
        <div class="btc-to-usd">
          <p class="btc-to-usd__title">BTC\USD</p>
          <div class="btc-price">
            <div class="btcwdgt-price" data-bw-theme="light"></div>
          </div>
        </div>
      </div>
    </nav>
  </div>
</header>