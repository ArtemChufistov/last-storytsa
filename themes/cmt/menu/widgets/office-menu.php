<?php
use yii\web\View;
use yii\helpers\Url;
use app\modules\merchant\widgets\GetCoinsWidget;
use app\modules\merchant\widgets\CreateTransactionWidget;
?>

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu"><?php echo Yii::t('app', 'Меню');?></span></h3>
        </div>
        <ul class="nav" id="side-menu">
            <?php foreach ($menu->children(1)->all() as $num => $child): ?>
                <?php $children = $child->children(2)->all();?>
                <li>
                    <a href="<?=Url::to([$child->link]);?>" class="waves-effect <?php if (Url::to([$requestUrl]) == Url::to([$child->link])):?>active<?php endif;?>">
                        <?php echo $child->icon; ?>
                        <span class="hide-menu">
                            <?php echo $child->title?><?php if (!empty($children)):?><span class="fa arrow"></span><?php endif;?>
                        </span>
                    </a>
                    <?php if (!empty($children)):?>
                        <ul class="nav nav-second-level">
                            <?php foreach ($children as $num => $childChild): ?>
                                <li>
                                    <a href="<?=Url::to([$childChild->link]);?>">
                                        <?php echo $childChild->icon; ?>
                                        <span class="hide-menu"><?php echo $childChild->title?> </span>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif;?>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
</div>

<?php echo CreateTransactionWidget::widget();?>
<?php echo GetCoinsWidget::widget();?>