<?php
use yii\helpers\Url;
?>
<div class="list-wrap">
  <ul class="list-nav">
    <?php foreach ($menu->children(1)->all() as $num => $child): ?>
      <li><a href="<?=Url::to([$child->link]);?>"><?php echo $child->title?></a></li>
    <?php endforeach;?>
  </ul>
</div>