<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\merchant\models\search\MNodesTransactionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mnodes-transactions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'txid') ?>

    <?= $form->field($model, 'confirmations') ?>

    <?= $form->field($model, 'currency') ?>

    <?= $form->field($model, 'category') ?>

    <?php // echo $form->field($model, 'send_msg_status') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'fee') ?>

    <?php // echo $form->field($model, 'from_addr') ?>

    <?php // echo $form->field($model, 'to_addr') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'blockchain_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
