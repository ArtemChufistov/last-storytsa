<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\merchant\models\MNodesTransactions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mnodes-transactions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'txid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'confirmations')->textInput() ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'send_msg_status')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'fee')->textInput() ?>

    <?= $form->field($model, 'from_addr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_addr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'blockchain_time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
