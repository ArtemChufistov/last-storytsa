<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\merchant\models\search\MNodesTransactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mnodes Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mnodes-transactions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mnodes Transactions', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'txid',
            'confirmations',
            'currency',
            'category',
            // 'send_msg_status',
            // 'amount',
            // 'fee',
            // 'from_addr',
            // 'to_addr',
            // 'created_at',
            // 'blockchain_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
