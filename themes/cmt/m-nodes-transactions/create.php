<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\merchant\models\MNodesTransactions */

$this->title = 'Create Mnodes Transactions';
$this->params['breadcrumbs'][] = ['label' => 'Mnodes Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mnodes-transactions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
