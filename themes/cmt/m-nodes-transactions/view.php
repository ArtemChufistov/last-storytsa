<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\merchant\models\MNodesTransactions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mnodes Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mnodes-transactions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'txid',
            'confirmations',
            'currency',
            'category',
            'send_msg_status',
            'amount',
            'fee',
            'from_addr',
            'to_addr',
            'created_at',
            'blockchain_time',
        ],
    ]) ?>

</div>
