<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
?>
<div id="transactionModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><?php echo Yii::t('app', 'Создание транзакции');?></h4>
            </div>

            <?php $form = ActiveForm::begin([
                'fieldConfig' => [ 'options' => [
                        'tag' => 'div',
                ]]]); ?>

                <div class="modal-body">
                    <?= $form->field($transferForm,'address')->textInput([
                        'placeholder' => Yii::t('app', 'Адрес') . ': 3FP8y2nhH44HZ9nuM9dVVGkvCna31gXAHD',
                        'maxlength' => true,
                        'class' => 'form-control',
                    ]); ?>

                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($transferForm, 'amount')->textInput(['placeholder' => Yii::t('app', 'Сумма'), 'maxlength' => true, 'class' => 'form-control']) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($transferForm, 'currency')->dropDownList(MerchantHelper::avalableWalletsFromMerchantModuleArray(), [
                                'class' => 'selectpicker',
                                'data-style' => 'form-control'
                            ]); ?>
                        </div>

                        <?php if (Yii::$app->session->hasFlash('mTransferError')):?>
                            <p style = "text-align: center;"><?php echo Yii::$app->session->getFlash('mTransferError');?></p>
                        <?php endif;?>
                    </div>

                </div>

                <div class="modal-footer">
                    <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-info btn-block btn-rounded text-uppercase waves-effect waves-light']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php if (Yii::$app->session->hasFlash('mTransferSuccess')):?>
    <div id="mTransferSuccess" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"><?php echo Yii::t('app', 'Транзакции создана'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12" style = "display: inline-grid;">
                            <p style = "text-align: center;">
                                <?php echo Yii::$app->session->getFlash('mTransferSuccess');?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->registerJs('$("#mTransferSuccess").modal();', yii\web\View::POS_END); ?>
<?php endif; ?>

<?php $this->registerJs('
$(\'a[href="/createtransaction"\').click(function(){
    $(\'#transactionModal\').modal();
    return false;
})
', yii\web\View::POS_END); ?>

<?php if ($transferForm->getErrors() || Yii::$app->session->hasFlash('mTransferError')): ?>
    <?php $this->registerJs('$(\'#transactionModal\').modal();', yii\web\View::POS_END); ?>
<?php endif; ?>