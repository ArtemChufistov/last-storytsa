<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<?php $form = ActiveForm::begin([
    'id' => 'twofaform',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}",
        'options' => [
            'tag' => 'div',
        ],
    ],
    'options' => [
        'class' => 'form-horizontal form-material',
        'role' => 'form'
    ]
]); ?>


    <div class="col-md-4 col-xs-12">
        <div class="white-box">
            <h5 class="box-title"><?php use app\modules\merchant\components\twofa\widgets\TwoFaQr;

                echo Yii::t('app', 'ДВУХФАКТОРНАЯ АУТЕНТИФИКАЦИЯ'); ?></h5>


            <style>
                .two-factor h3 .sub-title {
                    display: block;
                    font-size: 12px;
                    line-height: 16px;
                    color: #666;
                    text-transform: initial;
                    margin-top: 8px;
                    font-weight: lighter;
                }

                .two-factor .disabled-content .disabled-item .circle {
                    display: inline-block;
                    width: 24px;
                    height: 24px;
                    border-radius: 50%;
                    border: 2px solid #0084D4;
                    font-size: 1.25em;
                }

                .color-blue {
                    color: #0084D4;
                }

                .status-panel h4 span.un-verify {
                    color: #F75535;
                }

                h4 {
                    line-height: 19px;
                    margin: 0 0 12px 0;
                    font-size: 16px;
                    letter-spacing: 0.4px;
                    color: #B2C3DB;
                }

                .status-panel h4 {
                    margin: 0;
                    line-height: 24px;
                    font-size: 20px;
                    font-weight: bold;
                    color: dimgrey;
                }

                .status-panel h4 span.verified {
                    color: #00A45B;
                }

                .status-panel h4 span.un-verify {
                    color: #F75535;
                }

                .icon-2fa-red {
                    display: inline-block;
                    width: 32px;
                    height: 22px;
                    background: url(/cmt/icons/icon-2fa-red.svg);
                    background-size: contain;
                    background-repeat: no-repeat;
                    background-position: center bottom;
                }

                .icon-2fa-green {
                    display: inline-block;
                    width: 32px;
                    height: 22px;
                    background: url(/cmt/icons/icon-2fa-green.svg);
                    background-size: contain;
                    background-repeat: no-repeat;
                    background-position: center bottom;
                }

                .two-factor .disabled-content .disabled-item .google-au .caption span {
                    color: dimgrey;
                    font-size: 1.1em;
                    font-weight: bold;
                }
            </style>
            <div class="form-group two-factor" id="two_factor">


                <div class="col-lg-12">


                    <h3>
                        <span class="sub-title">
                            Значительно улучшает безопасность требуя ваш пароль и другую форму аутентификации.
                        </span>
                    </h3>


                    <?php if (!$model->user->hasTwoFaEnabled()): ?>


                        <div class="panel status-panel">
                            <h4>
                                Статус:
                                <span class="un-verify">
                                <i class="icon-2fa-red">
                                </i>
                                Отключен
                            </span>
                            </h4>
                            <p class="text-success"></p>
                        </div>

                        <div class="disabled-content">
                            <h4>
                                Как включить Двухфакторную аутентификацию
                            </h4>
                            <!--Step 1-->
                            <div class="disabled-item">
                                <div class="clearfix">
                                    <div class="col-md-1 col-sm-1 no-padding pull-left">
                                    <span class="circle text-center color-blue font-bold">
                                        1
                                    </span>
                                    </div>
                                    <p class="col-md-11 col-sm-11 no-padding pull-left step">
                                        Загрузите Google Authenticator на ваше мобильное устройство
                                    </p>
                                </div>
                                <div class="thumbnail google-au">
                                    <img src="/cmt/icons/google-authenticator.png" alt="Google Authenticator">
                                    <div class="caption">
                                    <span>
                                        Google Authenticator
                                    </span>
                                    </div>
                                </div>
                                <div class="app-store text-center">
                                    <a href="http://www.apple.com/" target="_blank">
                                        <img src="/cmt/icons/download-apple-store.png"
                                             alt="Download on the App Store">
                                    </a>
                                    <a href="https://play.google.com/store" target="_blank">
                                        <img src="/cmt/icons/download-google-store.png" alt="Get it on Google Play">
                                    </a>
                                </div>
                            </div>
                            <!--Step 2-->
                            <div class="disabled-item">
                                <div class="clearfix">
                                    <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                                    <span class="circle text-center color-blue font-bold">
                                        2
                                    </span>
                                    </div>
                                    <p class="col-md-11 col-sm-11 col-xs-11 no-padding QRCode step">
                                        Используйте Google Authenticator для сканирования QRCode
                                    </p>
                                </div>
                                <div class="text-center">
                                    <input id="QRCodeString" name="QRCodeString" type="hidden"
                                           value="otpauth://totp/Bittrex?secret=WZHHPA25S54K6XUK">
                                    <input id="Secret2FA" name="Secret2FA" type="hidden" value="WZHHPA25S54K6XUK">


                                    <?php
                                    echo TwoFaQr::widget([
                                        'accountName' => 'crypchant.com',
                                        'secret' => $model->secret,
                                        'issuer' => 'Crypchant',
                                        'size' => 300,
                                        'showSecret' => false
                                    ]); ?>


                                </div>
                            </div>
                            <!--Step 2 iOS & 768px & 1024px-->
                            <div class="disabled-item hidden step-two">
                                <div class="clearfix">
                                    <div class="pull-left">
                                    <span class="circle text-center color-blue font-bold">
                                        2
                                    </span>
                                    </div>
                                    <p class="pull-left QRCode step">
                                        Откройте Google Authenticator и нажмите на
                                        <span class="glyphicon glyphicon-plus">
                                    </span>
                                        иконку для получения нового ключа аутентификации
                                    </p>
                                </div>
                            </div>
                            <!--Step 3 iOS & 768px & 1024px-->
                            <div class="disabled-item hidden step-three">
                                <div class="clearfix">
                                    <div class="pull-left">
                                    <span class="circle text-center color-blue font-bold">
                                        3
                                    </span>
                                    </div>
                                    <p class="pull-left QRCode step">
                                        Выберите ручной режим ввода
                                    </p>
                                </div>
                                <div class="manual-entry panel">
                                <span class="icon-edit">
                                </span>
                                    <span>
                                    Ручной ввод
                                </span>
                                </div>
                            </div>
                            <!--Step 4 iOS & 768px & 1024px-->
                            <div class="disabled-item hidden back-up">
                                <div class="clearfix">
                                    <div class="pull-left">
                                    <span class="circle text-center color-blue font-bold">
                                        4
                                    </span>
                                    </div>
                                    <p class="pull-left QRCode step">
                                        Скопируйте ключ аккаунта и вставте его в Google Authenticator
                                    </p>
                                </div>

                                <label for="">
                                    аккаунт
                                </label>
                                <div class="input-group">
                                    <input type="text" value="email@gmail.com">
                                    <span class="input-group-addon">
                                        Скопировать
                                    </span>
                                </div>
                                <label for="">
                                    ключ
                                </label>
                                <div class="input-group">
                                    <input type="text" value="123456abcdef">
                                    <span class="input-group-addon">
                                        Скопировать
                                    </span>
                                </div>

                            </div>
                            <!--Step 5 iOS & 768px & 1024px & Step 3-->
                            <div class="disabled-item back-up step-five">
                                <div class="clearfix">
                                    <div class="col-md-1 col-sm-1 pull-left no-padding">
                                    <span class="circle text-center color-blue font-bold">
                                        3
                                    </span>
                                        <span class="circle text-center color-blue font-bold hidden">
                                        5
                                    </span>
                                    </div>
                                    <p class="col-md-11 col-sm-11 pull-left no-padding step">
                                        Сохраните ваш Ключ безопасности.
                                    </p>
                                    <p class="col-md-11 col-sm-11 pull-left col-sm-offset-1 sub-step no-padding">
                                        Переустановка Двухфакторной аутентификации требует обращение в поддержку и
                                        может занимать до 48 часов.
                                    </p>
                                </div>
                                <div class="copy-form">
                                    <label for="copyable2FA" style="text-align: left;">
                                        Ключ безопасности
                                    </label>
                                    <div class="input-group">
                                        <?= $form->field($model, 'secret')->hiddenInput([
                                            'value' => $model->secret
                                        ])->label(false) ?>
                                        <input type="text" id="copyable2FA" style="width:100%;"
                                               value="<?= $model->secret ?>" readonly="">
                                        <span class="input-group-addon copyable" style="width:29%;"
                                              data-clipboard-action="copy" data-clipboard-target="#copyable2FA">
                                        Скопировать
                                    </span>
                                    </div>

                                </div>
                            </div>
                            <!--Step 6 iOS & 768px & 1024px & Step 4-->
                            <div class="disabled-item step-six">
                                <div class="clearfix">
                                    <div class="col-md-1 col-sm-1 pull-left no-padding">
                                    <span class="circle text-center color-blue font-bold">
                                        4
                                    </span>
                                        <span class="circle text-center color-blue font-bold hidden">
                                        6
                                    </span>
                                    </div>
                                    <p class="col-md-11 col-sm-11 pull-left no-padding step">
                                        Введите 6 значный код аутентификации сгенерированый Google Authenticator
                                    </p>
                                </div>
                                <!--                            <div class="input-group">-->
                                <!--                                <label for="authenticatorCode" class="control-label no-margin color-gray-6">Код Аутентификации</label>-->
                                <!--                                <input autocomplete="off" class="form-control" data-val="true" data-val-regex="Ваш код аутентификации 6 значный номер" data-val-regex-pattern="^[0-9]{6}$"-->
                                <!--                                       id="AuthenticatorCode" name="AuthenticatorCode" placeholder="Введите 6 значный код аутентификации" type="text" value="">-->
                                <!--<!--                                <button class="btn form-btn enabled-button" data-bind="css: { active: isSaving }, click: submitInformation">-->
                                <!--<!--                                    <span class="spinner">-->
                                <!--<!--                                    </span>-->
                                <!--<!--                                    Enable 2FA-->
                                <!--<!--                                </button>-->
                                <!--                            </div>-->


                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <?= $form->field($model, 'code')->textInput([
                                            'maxlength' => true,
                                            'placeholder' => $model->getAttributeLabel('Введите 6 значный код аутентификации'),
                                            'class' => 'form-control',
                                        ]) ?>
                                    </div>
                                </div>


                            </div>
                        </div>


                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Enable 2FA'), [
                            'class' => 'btn btn-info pull-left',
                            /*'value' => 1,
                            'name' => (new \ReflectionClass($user))->getShortName() . '[send_two_fa_confirm_submit]'*/]) ?>


                    <?php else: ?>


                    <div class="panel status-panel">
                        <h4>
                            Статус:
                            <span class="verified">
                                <i class="icon-2fa-green">
                                </i>
                                Включен
                            </span>
                        </h4>
                        <p class="text-success"></p>
                    </div>

                    <div class="disabled-content">
                    </div>
                </div>
                <div class="row">
                    <div class="validation-summary-valid" data-valmsg-summary="true">
                        <ul>
                            <li style="display:none"></li>
                        </ul>
                    </div>
                </div>
                <div class="pending-content enabled-content">

                    <div class="input-group" name="twoFaFormEnable" novalidate="">
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <?= $form->field($model, 'code')->textInput([
                                    'maxlength' => true,
                                    'placeholder' => $model->getAttributeLabel('Введите 6 значный код аутентификации'),
                                    'class' => 'form-control',
                                ]) ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Disable 2FA'), [
                    'class' => 'btn btn-warning pull-left',
                   /* 'value' => 1,
                    'name' => (new \ReflectionClass($user))->getShortName() . '[send_two_fa_confirm_submit]'*/]) ?>
                        </div>
                    </div>
                </div>   <!-- /.pending-content -->

                <?php endif; ?>
            </div>
        </div>
    </div>


<?php ActiveForm::end(); ?>