<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;

?>
<div id="getCoinModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><?php echo Yii::t('app', 'Получить коины'); ?></h4>
            </div>

            <?php $form = ActiveForm::begin([
                'fieldConfig' => ['options' => [
                    'tag' => 'div',
                ]]]); ?>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">

                        <?= $form->field($model, 'currency')->dropDownList(MerchantHelper::avalableWalletsFromMerchantModuleArray(), [
                            'class' => 'selectpicker',
                            'data-style' => 'form-control'
                        ]); ?>

                        <?php if (Yii::$app->session->hasFlash('mNewAddressError')):?>
                            <?= Yii::$app->session->getFlash('mNewAddressError');?>
                        <?php endif;?>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <?= Html::submitButton(Yii::t('app', 'Получить'), ['class' => 'btn btn-info btn-block btn-rounded text-uppercase waves-effect waves-light']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php if (Yii::$app->session->hasFlash('mNewAddress')):?>
    <div id="getCoinModalSuccess" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"><?php echo Yii::t('app', 'Кошелёк успешно создан'); ?></h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12" style = "display: inline-grid;">
                            <p style = "text-align: center;">
                                <?php echo Yii::t('app', 'Новый адрес {currency}', [
                                    'currency' => MerchantHelper::prettyCoinName($model->currency),
                                ]); ?>
                                <br/>
                                <br/>
                                <span style = "font-weight: bold; font-size: 16px;"><?php echo Yii::$app->session->getFlash('mNewAddress');?></span>
                            </p>
                            <br/>
                            <div style = "margin: 0 auto;" class = "qrCodeWallet"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->registerJs('
        $("#getCoinModalSuccess").modal();

        jQuery(".qrCodeWallet").qrcode({
            width: 128,
            height: 128,
            text: "' . MerchantHelper::prettyCoinName($model->currency) . ':' . Yii::$app->session->getFlash('mNewAddress') . '?amount=0.1"
        });
    ', yii\web\View::POS_END); ?>
<?php endif; ?>

<?php $this->registerJs('
    $(\'a[href="/getcoins"\').click(function () {
        $(\'#getCoinModal\').modal();
        return false;
    })
', yii\web\View::POS_END); ?>

<?php if ($model->getErrors() || Yii::$app->session->hasFlash('mNewAddressError')): ?>
    <?php $this->registerJs('$("#getCoinModal").modal();', yii\web\View::POS_END); ?>
<?php endif; ?>