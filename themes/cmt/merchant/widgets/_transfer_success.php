<?php
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
?>
<p style = "text-align: center;">
    <?php echo Yii::t('app', 'Транзакция на сумму:')?>
    <br/>
    <span style = "font-weight: bold; font-size: 16px;"><?php echo $tx_inf['amount'];?> <?php echo MerchantHelper::coinLogo($tx_inf['currency']);?></span>
    <br/>
    <?php echo Yii::t('app', 'на кошелёк:')?>
    <br/>
    <span style = "font-weight: bold; font-size: 16px;"><?php echo $tx_inf['address'];?></span>
    <br/>
    <?php echo Yii::t('app', 'успешно создана!')?>
</p>