<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\FrontendAppAsset;
use app\modules\menu\widgets\MenuWidget;

FrontendAppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
  <head>
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>
    <?php $this->head() ?>
  </head>
  <body class="one-screen-page bg-gray-darker bg-image bg-image-2">
    <?php $this->beginBody() ?>
    
    <?php echo $content;?>

    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>