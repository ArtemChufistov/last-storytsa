<?php
$this->context->layout = 'errorLayout';
?>
<div class="page">

  <header class="page-head">
    <div class="shell shell-fluid">
      <div class="range range-condensed">
        <div class="cell-md-preffix-1 cell-md-10">
          <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head-logo-svg');?>
        </div>
      </div>
    </div>
  </header>

  <!-- Page Content-->
  <main class="page-content">
    <section>
      <div class="shell shell-fluid">
        <div class="range range-condensed">
          <div class="cell-sm-7 cell-md-preffix-1 cell-md-7 cell-lg-6 cell-xl-4">
            <h3><?php echo Yii::t('app', 'Ошибка');?></h3>
            <p class="text-extra-large">404</p>
            <h5 style="max-width: 600px;" class="text-silver offset-top-0 offset-lg-top-30"><?= $message;?></h5>
            <a href="/" class="btn btn-white-outline offset-xl-top-60"><?php echo Yii::t('app', 'Вернуться на главную');?></a>
          </div>
        </div>
      </div>
    </section>

  </main>

  <!-- Page Footer-->
  <footer class="page-foot">
    <div class="shell shell-fluid">
      <div class="range range-condensed">
        <div class="cell-md-preffix-1 cell-md-10">
          <p class="rights small">&nbsp;&#169;&nbsp;<span id="copyright-year"></span>&nbsp;<?php echo Yii::t('app', 'Все права защищены');?>&nbsp;<br class="veil-xs"><a href="#">
              <?php echo Yii::t('app', 'Лицензионное соглашение');?></a></p>
        </div>
      </div>
    </div>
  </footer>
</div>