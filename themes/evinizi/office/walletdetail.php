<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use app\modules\payment\widgets\PayWalletPasswordWidget;
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\payment\models\Currency;

$this->title = Yii::t('app', 'Платёжные реквизиты');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<div class="row">
  <div class="col-md-4">

    <?php foreach($userWallets as $wallet):?>
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-credit-card" aria-hidden="true"></i>
          <?php echo Yii::t('app', 'Кошелек');?>
          <?php echo $wallet->getPaySystem()->one()->name;?>
          <?php echo $wallet->getCurrency()->one()->title;?>
        </h3>
      </div>
      <div class="box-body">
        <?php $form = ActiveForm::begin([
          'id' => 'form-profile',
          'options' => [
              'class'=>'form',
              'enctype'=>'multipart/form-data'
          ],
          ]); ?>

          <?php foreach($wallet->getInfoFields() as $field):?>

          <?= $form->field($wallet, $field)->textInput([
              'maxlength' => true,
              'placeholder' => $wallet->getAttributeLabel($field)
          ]) ?>

          <?php endforeach;?>

          <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
              'class' => 'btn btn-success pull-left',
              'name' => 'save-wallet']) ?>

        <?php ActiveForm::end();?>

        <?php if (!empty($wallet->wallet) && $wallet->getCurrency()->one()->key == Currency::KEY_BTC):?>
          <img class = "pull-right" src = "/profile/office/qrcode/<?php echo $wallet->wallet;?>">
        <?php endif;?>
      </div>
    </div>
  <?php endforeach;?>
  </div>

  <div class="col-md-4">
    <?= PayWalletPasswordWidget::widget(['user' => $this->params['user']->identity]) ?>
  </div>

  <div class="col-md-4">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true"></i><?php echo Yii::t('app', 'Важно!');?></h3>
      </div>
      <div class="box-body text-trancpancy">
      	<strong><?php echo Yii::t('app', 'Мы рекомендуем использовать кошелек bitcoin на <a href = "www.coinbase.com">www.coinbase.com</a>, по нашему опыту работы это самый надёжный сервис BitCoin кошельков');?></strong>
      	</br>

      </div>
    </div>
  </div>

</div>