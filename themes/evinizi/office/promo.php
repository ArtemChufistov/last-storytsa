
<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Рекламные материалы');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>


<?php $form = ActiveForm::begin([
  'id' => 'form-profile',
  'options' => [
      'class'=>'form',
      'enctype'=>'multipart/form-data'
  ],
  ]); ?>
<div class="row banners">
  <div class="col-lg-5">
	  <div class="box box-info">
	    <div class="box-header with-border">
	      <h3 class="box-title"><i class="fa fa-picture-o" aria-hidden="true"></i><?php echo Yii::t('app', 'Рекламные баннеры, размер 485x60');?></h3>

	      <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	        </button>
	      </div>
	      <!-- /.box-tools -->
	    </div>
	    <!-- /.box-header -->
	    <div class="box-body banner-body">
	      <img style = "margin-bottom:45px;" src="<?php echo Url::home(true);?>volcano/485х60.gif">
	      <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal48560']);?>
	    </div>

		<div class="modal fade modal48560">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 485x60');?></h4>
		      </div>
		      <div class="modal-body">
		        <p><textarea class = "form-control" id = "clipboardCode48560"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>volcano/485х60.gif"></a></textarea></p>
		      </div>
		      <div class="modal-footer">
		        <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode48560']);?>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
		      </div>
		    </div>
		  </div>
		</div>

	  </div>
	</div>
	<div class="col-lg-7">
	  <div class="box box-info">
	    <div class="box-header with-border">
	      <h3 class="box-title"><i class="fa fa-picture-o" aria-hidden="true"></i><?php echo Yii::t('app', 'Рекламные баннеры, размер 728x90');?></h3>

	      <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	        </button>
	      </div>
	      <!-- /.box-tools -->
	    </div>
	    <!-- /.box-header -->
	    <div class="box-body banner-body">
	      <img src="<?php echo Url::home(true);?>volcano/728х90.gif">
	      <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal72890']);?>
	    </div>

		<div class="modal fade modal72890">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 728x90');?></h4>
		      </div>
		      <div class="modal-body">
		        <p><textarea class = "form-control" id = "clipboardCode72890"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>volcano/728х90.gif"></a></textarea></p>
		      </div>
		      <div class="modal-footer">
		        <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode72890']);?>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
		      </div>
		    </div>
		  </div>
		</div>

	  </div>
  </div>
</div>
<div class="row banners">
  <div class="col-lg-4">
	  <div class="box box-info">
	    <div class="box-header with-border">
	      <h3 class="box-title"><i class="fa fa-picture-o" aria-hidden="true"></i><?php echo Yii::t('app', 'Рекламные баннеры, размер 250x250');?></h3>

	      <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	        </button>
	      </div>
	      <!-- /.box-tools -->
	    </div>
	    <!-- /.box-header -->
	    <div class="box-body banner-body">
	      <img src="<?php echo Url::home(true);?>volcano/250х250.gif">
	      <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal250250']);?>
	    </div>

		<div class="modal fade modal250250">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 250x250');?></h4>
		      </div>
		      <div class="modal-body">
		        <p><textarea class = "form-control" id = "clipboardCode250250"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>volcano/250х250.gif"></a></textarea></p>
		      </div>
		      <div class="modal-footer">
		        <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode250250']);?>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
		      </div>
		    </div>
		  </div>
		</div>

	  </div>

	  <div class="box box-info">
	    <div class="box-header with-border">
	      <h3 class="box-title"><i class="fa fa-picture-o" aria-hidden="true"></i><?php echo Yii::t('app', 'Рекламные баннеры, размер 128х128');?></h3>

	      <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	        </button>
	      </div>
	      <!-- /.box-tools -->
	    </div>
	    <!-- /.box-header -->
	    <div class="box-body banner-body">
	      <img src="<?php echo Url::home(true);?>volcano/128х128.gif">
	      <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal125125']);?>
	    </div>

		<div class="modal fade modal125125">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 125x125');?></h4>
		      </div>
		      <div class="modal-body">
		        <p><textarea class = "form-control" id = "clipboardCode125125"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>volcano/128х128.gif"></a></textarea></p>
		      </div>
		      <div class="modal-footer">
		        <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode125125']);?>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
		      </div>
		    </div>
		  </div>
		</div>

	  </div>
  </div>

  <div class="col-lg-4">
	  <div class="box box-info">
	    <div class="box-header with-border">
	      <h3 class="box-title"><i class="fa fa-picture-o" aria-hidden="true"></i><?php echo Yii::t('app', 'Рекламные баннеры, размер 240x400');?></h3>

	      <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	        </button>
	      </div>
	      <!-- /.box-tools -->
	    </div>
	    <!-- /.box-header -->
	    <div class="box-body banner-body">
	      <img src="<?php echo Url::home(true);?>volcano/240х400.gif">
		  <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal240400']);?>
	    </div>
	  </div>

		<div class="modal fade modal240400">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 240x400');?></h4>
		      </div>
		      <div class="modal-body">
		        <p><textarea class = "form-control" id = "clipboardCode240400"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>volcano/240х400.gif"></a></textarea></p>
		      </div>
		      <div class="modal-footer">
		        <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode240400']);?>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
		      </div>
		    </div>
		  </div>
		</div>

  </div>

  <div class="col-lg-4">
	  <div class="box box-info">
	    <div class="box-header with-border">
	      <h3 class="box-title"><i class="fa fa-picture-o" aria-hidden="true"></i><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x300');?></h3>

	      <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	        </button>
	      </div>
	      <!-- /.box-tools -->
	    </div>
	    <!-- /.box-header -->
	    <div class="box-body banner-body">
	      <img src="<?php echo Url::home(true);?>volcano/300х300.gif">
	      <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal300300']);?>
	    </div>
	    <!-- /.box-body -->
	  </div>

		<div class="modal fade modal300300">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 300х300');?></h4>
		      </div>
		      <div class="modal-body">
		        <p><textarea class = "form-control" id = "clipboardCode300300"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>volcano/300х300.gif"></a></textarea></p>
		      </div>
		      <div class="modal-footer">
		        <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode300300']);?>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
		      </div>
		    </div>
		  </div>
		</div>

	  <div class="box box-info">
	    <div class="box-header with-border">
	      <h3 class="box-title"><i class="fa fa-picture-o" aria-hidden="true"></i><?php echo Yii::t('app', 'QR коды с зашитой REF ссылкой');?></h3>

	      <div class="box-tools pull-right">
	        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	        </button>
	      </div>
	      <!-- /.box-tools -->
	    </div>
	    <!-- /.box-header -->
	    <div class="box-body banner-body">
	      <img src="<?php echo Url::home(true);?>qrcodemain/<?php echo $user->login;?>">
	      <img src="<?php echo Url::home(true);?>qrcodereg/<?php echo $user->login;?>">
	      <?php echo Html::input('button', '', Yii::t('app', 'Получить код'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modalqr']);?>
	    </div>
	    <!-- /.box-body -->
	  </div>

		<div class="modal fade modalqr">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 240x400');?></h4>
		      </div>
		      <div class="modal-body">
		        <p><textarea class = "form-control" id = "qrcodemain"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>qrcodemain/<?php echo $user->login;?>"></a></textarea></p>
		        <p><textarea class = "form-control" id = "qrcodereg"><a href = "<?php echo Url::home(true);?>signup?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>qrcodereg/<?php echo $user->login;?>"></a></textarea></p>
		      </div>
		      <div class="modal-footer">
		        <?php echo Html::input('button', '', Yii::t('app', 'Ссылка на главную в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#qrcodemain']);?>
		        <?php echo Html::input('button', '', Yii::t('app', 'Ссылка на регистрацию в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#qrcodereg']);?>
		      </div>
		    </div>
		  </div>
		</div>

  </div>
</div>
<?php ActiveForm::end();?>

<?php $this->registerJs('new Clipboard(".btn-clipboard");');?>

