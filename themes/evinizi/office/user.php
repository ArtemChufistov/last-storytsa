<?php

use app\modules\profile\components\AuthKeysManager;
use lowbase\user\models\Country;
use app\modules\profile\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Мой профиль');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-profile',
    'options' => [
        'class'=>'form row',
        'enctype'=>'multipart/form-data'
    ],

    ]); ?>

<div class="col-md-4">

  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-user" aria-hidden="true"></i><?php echo Yii::t('app', 'О себе');?></h3>
    </div>
    <div class="box-body">
        <?= $form->field($user, 'first_name')->textInput([
            'maxlength' => true,
            'placeholder' => $user->getAttributeLabel('first_name')
        ]) ?>

        <?= $form->field($user, 'last_name')->textInput([
            'maxlength' => true,
            'placeholder' => $user->getAttributeLabel('last_name')
        ]) ?>

        <?= $form->field($user, 'phone')->textInput([
            'maxlength' => true,
            'placeholder' => $user->getAttributeLabel('phone')
        ]) ?>

        <?= $form->field($user, 'skype')->textInput([
            'maxlength' => true,
            'placeholder' => $user->getAttributeLabel('skype')
        ]) ?>

        <?= $form->field($user, 'birthday')
            ->widget(DatePicker::classname(), [
                'options' => ['placeholder' => $user->getAttributeLabel('birthday')],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy'
                ]
                ]); ?>
<!--
        <?= $form->field($user, 'country_id')->widget(Select2::classname(), [
            'data' => Country::getAll(),
            'options' => ['placeholder' => $user->getAttributeLabel('country_id')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($user, 'city_id')->widget(Select2::classname(), [
            'initValueText' => ($user->city_id && $user->city) ? $user->city->city .
                ' (' . $user->city->state.", ".$user->city->region . ")": '',
            'options' => ['placeholder' => $user->getAttributeLabel('city_id')],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => Url::to(['city/find']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ]); ?>

        <?= $form->field($user, 'address')->textInput([
            'maxlength' => true,
            'placeholder' => $user->getAttributeLabel('address')
        ]) ?>
-->


        <div class="input-wrap">
            <div class="clearfix" id="UserLogin-gender">
                <label class="radio-head"><?php echo Yii::t('app', 'Пол');?></label>
                <?=
                $form->field($user, 'sex')
                    ->radioList(
                        [null => Yii::t('app', 'Не указан')] + User::getSexArray(),
                        [
                            'item' => function($index, $label, $name, $checked, $value) {

                                $checked = $checked == true ? 'checked="checked"' : '';
                                $return = '<label class="modal-radio">';
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" class="flat-red" ' . $checked . ' >';
                                $return .= '<i></i>';
                                $return .= '<span>' . ucwords($label) . '</span>';
                                $return .= '</label>';

                                return $return;
                            }
                        ]
                    )
                ->label(false);
                ?>
            </div>
            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                'class' => 'btn btn-info pull-right',
                'name' => 'signup-button']) ?>
        </div>
    </div>
    <div class="box-header with-border">
        <h3 class="box-title">
        <i class="fa fa-user" aria-hidden="true"></i><?php echo Yii::t('app', 'Социальные сети');?>
        </h3>
    </div>
    <div class="box-body">
        <?= AuthKeysManager::widget([
            'baseAuthUrl' => ['/profile/auth/index'],
        ]) ?>

    </div>
  </div>
</div>
<div class="col-md-4">
  <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-user" aria-hidden="true"></i><?php echo Yii::t('app', 'Дополнительная информация');?></h3>
    </div>
    <div class="box-body">
        <div class="lb-user-module-profile-image">
            <?php
            if ($user->image) {
                echo "<img src='/".$user->image."' class='thumbnail'>";
                echo "<p>" . Html::a(Yii::t('app', 'Удалить фото'), ['/profile/office/remove']) . "</p>";
            } else {
                    echo "<img src='/images/default-avatar.jpg' class='thumbnail'>";
            }
            ?>
              <?= $form->field($user, 'photo', ['options' => [
                'class' => 'btn btn-default btn-file'], 'template' => '<i class="fa fa-paperclip"></i> ' . Yii::t('app', 'Ваше изображение'). '{input}'])->fileInput() ?>

        </div>

        <div class="form-group">
            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                'class' => 'btn btn-warning pull-left',
                'name' => 'signup-button']) ?>
        </div>

    </div>
  </div>
  <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-key" aria-hidden="true"></i><?php echo Yii::t('app', 'Смена пароля');?></h3>
    </div>
    <div class="box-body">

        <div class="lb-user-user-profile-password">
            <?= $form->field($user, 'old_password')->passwordInput([
                'maxlength' => true,
                'value' => '',
                'placeholder' => $user->getAttributeLabel('old_password'),
                'class' => 'form-control password'
            ]) ?>
        </div>

        <div class="lb-user-user-profile-password">
            <?= $form->field($user, 'password')->passwordInput([
                'maxlength' => true,
                'placeholder' => $user->getAttributeLabel('password'),
                'class' => 'form-control password'
            ]) ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                'class' => 'btn btn-warning pull-left',
                'value' => 1,
                'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
        </div>

    </div>
  </div>
</div>

<div class="col-md-4">
  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_ref', ['user' => $user]);?>

  <div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-envelope" aria-hidden="true"></i><?php echo Yii::t('app', 'Сменить E-mail');?></h3>
    </div>
    <div class="box-body">
        <?= $form->field($user, 'email')->textInput([
            'maxlength' => true,
            'readonly' => true,
            'placeholder' => $user->getAttributeLabel('email')
        ]) ?>

        <?= $form->field($user, 'email_confirm_token',['options' => [
            'style' => $oldUser->email_confirm_token == '' ? 'display:none;' : 'display:block;']])->textInput([
            'maxlength' => true,
            'value' => '',
            'placeholder' => $user->getAttributeLabel('email_confirm_token')
        ]) ?>

        <div class="form-group">

            <?php if ($oldUser->email_confirm_token == ''):?>
                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отвязать'), [
                    'class' => 'btn btn-danger pull-left',
                    'value' => 1,
                    'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
            <?php else:?>
                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отвязать'), [
                    'class' => 'btn btn-danger pull-left',
                    'value' => 1,
                    'name' =>  (new \ReflectionClass($user))->getShortName() . '[confirm_token_submit]']) ?>

                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить повторно код подтверждения'), [
                    'class' => 'btn btn-success pull-right',
                    'value' => 1,
                    'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
            <?php endif;?>

        </div>
    </div>
  </div>

</div>

<?php ActiveForm::end();?>

<?php if(Yii::$app->session->getFlash('success')): ?>
  <div class="modalError modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('success');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalError').modal('show');
", View::POS_END);?>

<?php endif;?>
