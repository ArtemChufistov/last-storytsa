
<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use app\modules\support\models\TicketDepartment;

$this->title = Yii::t('user', 'Поддержка');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<div class="row">
  <div class="col-md-6">

    <?php $form = ActiveForm::begin([
      'id' => 'form-profile',
      'options' => [
          'class'=>'form',
          'enctype'=>'multipart/form-data'
      ],
      ]); ?>

    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-fire" aria-hidden="true"></i><?php echo Yii::t('app', 'Поддержка');?></h3>
      </div>
      <div class="box-body">
        <?php echo Yii::t('app', '<strong>Внимание! раздел находится в стадии дорабоки, ответ может занять до 24 часов.</strong><p>Здесь вы можете задавать вопросы администрации проекта, или внести своё предложение по улучшению сервиса, а так-же дополнительным разделам личного кабинета.</p><p> Возможно у Вас возникли трудности технического характера, если так, то прежде чем их задавать попробуйте найти решение в разделе <a href="/faq">FAQ</a>.</p> Если-же вы всеравно не нашли решение своей проблемы, то обратитесь к нам и мы постараемся в кратчайшие сроки помочь. Просьба относится с внимательностью к выбору отдела, так как это упростит сотрудничество');?>
      </div>
    </div>

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true"></i></i><?php echo Yii::t('app', 'Создать тикет');?></h3>
      </div>
      <div class="box-body">

          <?php echo $form->field($ticket, 'department')->widget(Select2::classname(), [
              'data' =>  ArrayHelper::map(TicketDepartment::find()->all(), 'id', 'name'),
              'options' => ['placeholder' => Yii::t('app', 'Выберите отдел')],
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ]);?>

          <?= $form->field($ticket, 'text')->textArea(['rows' => '4', 'value' => '', 'class' => 'textarea form-control']); ?>

          <div class="form-group">
              <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить'), [
                  'class' => 'btn btn-info pull-left',
                  'name' => 'signup-button']) ?>
          </div>
      </div>
    </div>
  <?php ActiveForm::end();?>
  </div>

  <div class="col-md-6">
    
    <?php if (!empty($currentTicket)):?>

      <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/office/partial/_support-message', [
        'ticketMessages' => $ticketMessages,
        'ticketMessage' => $ticketMessage,
        'currentTicket' => $currentTicket,
        ]);?>

    <?php endif;?>

    <?php if (!empty($ticketArray)):?>
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i></i><?php echo Yii::t('app', 'Ваши тикеты');?></h3>
        </div>
          <div class="box-body no-padding">
            <table class="table ticketTable">
              <tr>
                <th style="width: 10px">№</th>
                <th><?php echo Yii::t('app', 'Дата');?></th>
                <th><?php echo Yii::t('app', 'Статус');?></th>
                <th><?php echo Yii::t('app', 'Отдел');?></th>
                <th><?php echo Yii::t('app', 'Новых сообщений');?></th>
              </tr>
              <?php foreach($ticketArray as $ticket):?>
                <tr ticketId="<?php echo $ticket->id;?>">
                  <td><a href = "/profile/office/support/<?php echo $ticket->id;?>"><?php echo $ticket->id;?>.</a></td>
                  <td><a href = "/profile/office/support/<?php echo $ticket->id;?>"><?php echo date('d-m-Y', strtotime($ticket->dateAdd));?></a></td>
                  <td><a href = "/profile/office/support/<?php echo $ticket->id;?>"><?php echo $ticket->getStatus()->one()->titleName();?></a></td>
                  <td><a href = "/profile/office/support/<?php echo $ticket->id;?>"><?php echo $ticket->getDepartment()->one()->name;?></a></td>
                  <td><a href = "/profile/office/support/<?php echo $ticket->id;?>"><span class="badge bg-red"><?php echo count($ticket->getNotReadMessages($user->id));?></span></a></td>
                </tr>
              <?php endforeach;?>
            </table>
          </div>
      </div>
    <?php endif;?>
  </div>

</div>

<?php $this->registerJs("jQuery(function () {jQuery('.textarea').wysihtml5();});", View::POS_END);?>
