<?php
use app\modules\profile\widgets\UserTreeElementWidget;
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\matrix\models\MatrixPref;
use app\modules\matrix\models\Matrix;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;


$childs = $currentPlace->children()->orderBy(['sort' => 'asc'])->all();

foreach($childs as $childItem){

    if ($childItem->sort == 0){
        $child1  = $childItem;
        $childs1 = $child1->children()->orderBy(['sort' => 'asc'])->all();
        foreach($childs1 as $childItem1){
            if ($childItem1->sort == 0){
                $child11 = $childItem1;
            }elseif($childItem1->sort == 1){
                $child12 = $childItem1;
            }elseif($childItem1->sort == 2){
                $child13 = $childItem1;
            }
        }
    }elseif($childItem->sort == 1){
        $child2  = $childItem;
        $childs2 = $child2->children()->orderBy(['sort' => 'asc'])->all();
        foreach($childs2 as $childItem2){
            if ($childItem2->sort == 0){
                $child21 = $childItem2;
            }elseif($childItem2->sort == 1){
                $child22 = $childItem2;
            }elseif($childItem2->sort == 2){
                $child23 = $childItem2;
            }
        }
    }elseif($childItem->sort == 2){
        $child3  = $childItem;
        $childs3 = $child3->children()->orderBy(['sort' => 'asc'])->all();
        foreach($childs3 as $childItem3){
            if ($childItem3->sort == 0){    
                $child31 = $childItem3;
            }elseif($childItem3->sort == 1){
                $child32 = $childItem3;
            }elseif($childItem3->sort == 2){
                $child33 = $childItem3;
            }
        }
    }
}

$depth = MatrixPref::find()->where(['key' => MatrixPref::KEY_DEPTH, 'matrix_id' => $rootMatrix->id])->one();
$countChildren = MatrixPref::find()->where(['key' => MatrixPref::KEY_COUNT_CHILDREN, 'matrix_id' => $rootMatrix->id])->one();


?>
<section class="management-hierarchy">

    <div class="hv-container">

        <div class="hv-wrapper">

            <ul class="timeline">

                <?php foreach($currentPlace->path()->having('depth <=' . $currentLevel)->all() as $placeItem):?>
                <li>
                    <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $placeItem->slug, 'parentPlaceSlug' => $parentPlace->slug]);?>">
                        <img class="fa" src = "<?php echo $placeItem->getUser()->one()->getImage();?>">
                    </a>
                        <div class="timeline-item"></div>
                </li>
                <?php endforeach;?>
            </ul>

            <div class="hv-item">
                <div class="hv-item-parent parent1">
                    <?php echo UserTreeElementWidget::widget([
                        'matrixCategory' => $matrixCategory,
                        'currentPlace' => $currentPlace,
                        'parentPlace' => $parentPlace,
                        'currentUser' => $user,
                        'rootMatrix' => $rootMatrix,
                        ]);?>
                </div>

                <div class="hv-item-children">

                    <div class="hv-item-child">
                        <!-- Key component -->
                        <div class="hv-item">

                            <div class="hv-item-parent <?php if($depth->value == 2):?>parent11<?php endif;?>" <?php if($depth->value == 1 || $countChildren->value == 2):?>style = "width: 200px;"<?php endif;?>>
                                <?php echo UserTreeElementWidget::widget([
                                    'matrixCategory' => $matrixCategory,
                                    'ancestorPlace' => $currentPlace,
                                    'currentPlace' => $child1,
                                    'parentPlace' => $parentPlace,
                                    'currentUser' => $user,
                                    'rootMatrix' => $rootMatrix,
                                    'sort' => 0,
                                ]);?>
                            </div>

                            <?php if($depth->value == 2):?>
                                <div class="hv-item-children hv-child1">
                                    <div class="hv-item-child child1">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child1,
                                            'currentPlace' => $child11,
                                            'parentPlace' => $parentPlace,
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 0,
                                        ]);?>
                                    </div>

                                    <div class="hv-item-child child2">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child1,
                                            'currentPlace' => $child12,
                                            'parentPlace' => $parentPlace,
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 1,
                                        ]);?>
                                    </div>

                                    <?php if ($countChildren->value > 2):?>
                                        <div class="hv-item-child child3">
                                            <?php echo UserTreeElementWidget::widget([
                                                'matrixCategory' => $matrixCategory,
                                                'ancestorPlace' => $child1,
                                                'currentPlace' => $child13,
                                                'parentPlace' => $parentPlace,
                                                'currentUser' => $user,
                                                'rootMatrix' => $rootMatrix,
                                                'sort' => 2,
                                            ]);?>
                                        </div>
                                    <?php endif;?>

                                </div>
                            <?php endif;?>
                        </div>
                    </div>

                    <div class="hv-item-child">
                        <!-- Key component -->
                        <div class="hv-item">

                            <div class="hv-item-parent <?php if($depth->value == 2):?>parent12<?php endif;?>" <?php if($depth->value == 1 || $countChildren->value == 2):?>style = "width: 200px;"<?php endif;?>>
                                <?php echo UserTreeElementWidget::widget([
                                    'matrixCategory' => $matrixCategory,
                                    'ancestorPlace' => $currentPlace,
                                    'currentPlace' => $child2, 
                                    'parentPlace' => $parentPlace, 
                                    'currentUser' => $user,
                                    'rootMatrix' => $rootMatrix,
                                    'sort' => 1,
                                ]);?>
                            </div>

                            <?php if($depth->value == 2):?>
                                <div class="hv-item-children hv-child2">

                                    <div class="hv-item-child child4">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child2,
                                            'currentPlace' => $child21, 
                                            'parentPlace' => $parentPlace, 
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 0,
                                        ]);?>
                                    </div>


                                    <div class="hv-item-child child5">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child2,
                                            'currentPlace' => $child22, 
                                            'parentPlace' => $parentPlace, 
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 1,
                                        ]);?>
                                    </div>

                                    <?php if ($countChildren->value > 2):?>
                                        <div class="hv-item-child child6">
                                            <?php echo UserTreeElementWidget::widget([
                                                'matrixCategory' => $matrixCategory,
                                                'ancestorPlace' => $child2,
                                                'currentPlace' => $child23, 
                                                'parentPlace' => $parentPlace, 
                                                'currentUser' => $user,
                                                'rootMatrix' => $rootMatrix,
                                                'sort' => 2,
                                            ]);?>
                                        </div>
                                    <?php endif;?>

                                </div>
                            <?php endif;?>
                        </div>
                    </div>

                    <?php if ($countChildren->value > 2):?>
                        <div class="hv-item-child">
                            <!-- Key component -->
                            <div class="hv-item">

                                <div class="hv-item-parent <?php if($depth->value == 2):?>parent13<?php endif;?>" <?php if($depth->value == 1):?>style = "width: 200px;"<?php endif;?>>
                                    <?php echo UserTreeElementWidget::widget([
                                        'matrixCategory' => $matrixCategory,
                                        'ancestorPlace' => $currentPlace,
                                        'currentPlace' => $child3, 
                                        'parentPlace' => $parentPlace, 
                                        'currentUser' => $user,
                                        'rootMatrix' => $rootMatrix,
                                        'sort' => 2,
                                    ]);?>
                                </div>

                                <?php if($depth->value == 2):?>
                                    <div class="hv-item-children hv-child3">

                                        <div class="hv-item-child child7">
                                            <?php echo UserTreeElementWidget::widget([
                                                'matrixCategory' => $matrixCategory,
                                                'ancestorPlace' => $child3,
                                                'currentPlace' => $child31, 
                                                'parentPlace' => $parentPlace, 
                                                'currentUser' => $user,
                                                'rootMatrix' => $rootMatrix,
                                                'sort' => 0,
                                            ]);?>
                                        </div>

                                        <div class="hv-item-child child8">
                                            <?php echo UserTreeElementWidget::widget([
                                                'matrixCategory' => $matrixCategory,
                                                'ancestorPlace' => $child3,
                                                'currentPlace' => $child32, 
                                                'parentPlace' => $parentPlace, 
                                                'currentUser' => $user,
                                                'rootMatrix' => $rootMatrix,
                                                'sort' => 1,
                                            ]);?>
                                        </div>

                                        <div class="hv-item-child child9">
                                            <?php echo UserTreeElementWidget::widget([
                                                'matrixCategory' => $matrixCategory,
                                                'ancestorPlace' => $child3,
                                                'currentPlace' => $child33, 
                                                'parentPlace' => $parentPlace, 
                                                'currentUser' => $user,
                                                'rootMatrix' => $rootMatrix,
                                                'sort' => 2,
                                            ]);?>
                                        </div>

                                    </div>
                                <?php endif;?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->registerJs("
$('.checkBoxPlace').on('ifChecked', function(event){
  $(this).closest('form').submit();
});
$('.checkBoxPlace').on('ifUnchecked', function(event){
  $(this).closest('form').submit();
});
$('.imgAvatar').one('click', function(){
        location.href = $(this).attr('href');
})
")?>