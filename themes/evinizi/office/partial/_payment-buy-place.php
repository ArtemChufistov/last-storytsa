<?php

use app\modules\matrix\components\StorytsaMarketing;
use app\modules\profile\widgets\UserTreeElementWidget;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>

<div class="box box-success box-solid">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo Yii::t('app', 'Старт');?></h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" type="button" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<div class="box-body" style="display: block;"><?php echo Yii::t('app', 'После регистрации вам нужно оплатить место на сайте. Выбираете из предложенных удобную для вас систему электронных платежей.</br> Сумма должна быть на кошельке + проценты перевода системы.
На сайт должны поступить <strong>100$</strong></br>
Приглашаете 3 партнера по <strong>100$</strong></br>
Когда третий партнер активирован в программу, вам в кошелек поступает <strong>100$</strong></br>
Можно заказать выплату или использовать для активации нового партнера.
Вы переходите на следующий круг.</br>
Стоимость участия: <strong>100$</strong>
');?>
</br>
</br>
<?php $form = ActiveForm::begin(); ?>
	<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', '<strong>Принять участие</strong>'), [
	  'class' => 'btn btn-block btn-success pull-left',
	  'value' => 1,
	  'name' => 'marketing-matrix']) ?>
<?php ActiveForm::end();?>

</div>
</div>

<div class="box box-warning collapsed-box box-solid">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo Yii::t('app', 'Накопительная');?></h3>

	  <div class="box-tools pull-right">
	    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
	    </button>
	  </div>

	</div>

	<div class="box-body">
	  <?php echo Yii::t('app', 'Вы стоите на первом месте вверху</br>
		Ваши 3 партнера становятся под вами во второй ряд.</br>
		Третий ряд заполняется слева направо партнерами ваших партнеров.</br>
		При заполнении первого места удерживается <strong>50$</strong> на содержание проекта и в кошелек за каждого нового партнера в 3 ряду с 1-го по 8-е место вам начисляется по <strong>25$</strong>, и за закрытие 9-го места - <strong>50$</strong></br>
		Всего в этом круге вами заработано <strong>250$</strong>.');?>
	</div>
</div>

<div class="box box-primary collapsed-box box-solid">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo Yii::t('app', 'Премиальная');?></h3>

	  <div class="box-tools pull-right">
	    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
	    </button>
	  </div>

	</div>

	<div class="box-body">
	  <?php echo Yii::t('app', 'Также как и в предыдущем круге вы занимаете первое верхнее место.</br>
		Под вами становятся 3 партнера вашей структуры, закрывшие раньше свой второй круг. Здесь имеет место обгон.</br>
		Ваша задача помочь отставшим. Для этого предусмотрен ваучер* для активации новых партнеров. Его можно обналичить, только через активацию новых мест в первом круге. Этот ваучер на <strong>300$</strong> вы получаете когда начинает заполняться третий ряд и первый партнер встал в нижний ряд. Благодаря этим ваучерам, система постоянно подпитывается новыми партнерами, что дает быстрое продвижение.</br>
		Вам на кошелек начисляют <strong>100$</strong>, их можно вывести.</br>
		По мере заполнения со <strong>2-го</strong> по <strong>9-е</strong> места за каждого партнера вам на кошелек для вывода приходит по <strong>200$</strong></br>
		Итого за этот круг вы имеете <strong>200$х8=1600$+100$=1700$</strong> и плюс ваучер <strong>300$</strong>.</br>
		ВСЕГО <strong>2000$</strong>');?>
	</div>
</div>

<div class="box box-success collapsed-box box-solid">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo Yii::t('app', 'Призовая');?></h3>

	  <div class="box-tools pull-right">
	    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
	    </button>
	  </div>

	</div>

	<div class="box-body">
	  <?php echo Yii::t('app', 'Суть <strong>НОУ-ХАУ</strong> нашего проекта состоит в том, что в финишный круг попадают партнеры из любой структуры, которые первыми закрыли предыдущий круг. Происходит смешивание структур, что дает эффект значительного ускорения, снимая проблему замедления прохождения финальных матриц.</br>
			Кроме этого, так же как и в премиальном круге, когда первый партнер займет первое место в нижней четверке, вы получаете ваучер №2 на <strong>300$</strong>, и имеете право на выбор:</br>
			<strong>1.</strong> активировать 3 новых партнеров в проект , или -</br>
			<strong>2.</strong> инвестировать благотворительный взнос в международную кроуфандинговую площадку, с правом получать пожертвования до <strong>3,5</strong> миллионов евро.</br>
			Вам на кошелек начислят по <strong>10000$</strong> за каждое заполненное место в нижней четверке. Их можно выводить поэтапно.</br>
			По желанию партнера, будет рекомендовано купить пай в накопительном фонде и в процессе заработка следующей суммы - увеличить первоначальную.</br>
			Вы получаете логин реинвеста в предыдущий премиальный круг. И снова зарабатываете.</br>
			Также вас премируют билетом участника Первого Семинара лидеров с участием представителей агентств по недвижимости Турции и других стан.</br>
			Всего заработано в этом круге <strong>40000$ + 300$</strong> ваучер + <strong>1500$</strong> логин реинвеста Х билет с перелетом на Семинар в Турцию.</br>
			На семинаре или онлайн вы сможете заключить договор на подбор желаемой недвижимости с любым агентством. При наличии такого договора у вас будет возможность поэтапного вывода начисленных заработков в премиальном и финишном кругах.</br>
			Всего ваш логин реинвеста заработает за повтор в 2 круга (премиальный и финишный) <strong>47 400$ + 2 x 300 = 600$</strong> ваучерами.</br>
			ваучер*- срок действия ваучера 3 месяца ');?>
	</div>
</div>
