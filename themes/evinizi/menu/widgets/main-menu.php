<?php
use app\modules\lang\widgets\WLang;
use yii\web\View;
use yii\helpers\Url;
?>

<header class="page-head">
  <div class="rd-navbar-wrap rd-navbar-default rd-navbar-transparent headMen">
    <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-clone="false" data-md-stick-up-offset="0px" data-lg-stick-up-offset="0px" data-body-class="rd-navbar-transparent-linked" class="rd-navbar">
      <div class="shell shell-fluid">
        <div class="rd-navbar-inner">
          <div class="rd-navbar-panel">
            <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head-logo-svg');?>
          </div>
          <!-- RD Navbar Nav-->
          <div class="rd-navbar-nav-wrap">

            <ul class="rd-navbar-nav">
              <?php foreach($menu->children()->all() as $num => $children):?>
                <li class="<?php if($requestUrl == Url::to([$children->link])):?>active<?php endif;?>" ><a href="<?= Url::to([$children->link]);?>"><span><?= $children->title;?></span></a>
                  <?php if (!empty($children->children()->all())):?>
                    <ul class="rd-navbar-dropdown">
                      <?php foreach($children->children()->all() as $numChild => $childChildren):?>
                        <li><a href="<?= Url::to([$childChildren->link]);?>"><?= $childChildren->title;?></a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="<?= $childChildren->link;?>"><?= $childChildren->title;?></a>
                            </li>
                          </ul>
                        </li>
                      <?php endforeach;?>
                    </ul>
                  <?php endif;?>
                </li>
              <?php endforeach;?>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  </div>
</header>