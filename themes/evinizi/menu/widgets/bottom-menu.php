<?php
use yii\web\View;
use yii\helpers\Url;
?>

<section class="section-55 section-sm-top-60 section-sm-bottom-55 section-lg-top-90">
  <div class="shell shell-fluid">
    <div class="range range-no-gutter range-sm-center">
      <div class="cell-xl-10">
        <div class="range range-condensed">
          <div class="cell-xs-12">
            <hr>
          </div>
          <div class="cell-xs-12 offset-top-26">
            <div class="range range-condensed range-xs-bottom text-center">
              <div class="cell-md-8 text-md-left">
                <nav class="nav-custom">
                  <ul>
                    <?php foreach($menu->children($menu->depth + 1)->all() as $num => $child):?>
                      <li><a href="<?= $child->link;?>"><?= $child->title;?></a></li>
                    <?php endforeach;?>
                  </ul>
                </nav>
              </div>
              <div class="cell-md-4 offset-top-30 offset-lg-top-0 text-md-right">
                <ul class="list-inline list-inline-sizing-1 list-inline-md">

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>