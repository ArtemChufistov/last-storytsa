<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use lowbase\user\UserAsset;
use yii\captcha\Captcha;
use yii\web\View;


UserAsset::register($this);
$this->title = Yii::t('app', 'Связаться с нами');

$this->params['breadcrumbs']= [ [
    'label' => $this->title,
    'url' => '/contact'
  ],
]

?>

</header>

<main class="page-content">
  <!-- Breadcrumbs & Page title-->
  <section class="section-30 section-sm-top-60 section-md-top-80 section-xl-top-120 bg-image bg-image-1">
    <div class="shell shell-fluid">
      <div class="range range-condensed">
        <div class="cell-xs-12 text-center">
          <p class="h6 subheader"><?php echo Yii::t('app', 'Техническая 24/7 Поддержка');?></p>
          <p class="h1"><?php echo $this->title;?></p>
          <?= Breadcrumbs::widget([
            'options' => ['class' => 'breadcrumbs-custom'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>
        </div>
      </div>
    </div>
  </section>

  <!-- Buttons-->
  <section class="section-30 section-sm-80 section-xl-120">
    <div class="shell shell-fluid">
      <div class="range range-condensed">
        <div class="cell-md-7 cell-lg-6 cell-xl-preffix-1">
          <div class="cell-box-medium">
            <h2 class="text-regular"><?php echo Yii::t('app', 'Напишите нам');?></h2>
            <p class="offset-top-30 offset-md-top-50"><?php echo Yii::t('app', 'Вы можете задать интересующий вопрос вопрос по работе системы или сделать предложение по улучшению сервиса');?></p>
              <?php $form = ActiveForm::begin(['options' => [
                'data-form-output' => 'form-output-global',
                'data-form-type' => 'contact',
                'class' => 'form-classic',
              ]]); ?>
              <div class="range">
                <div class="cell-sm-6 cell-md-12 cell-lg-6">
                  <div class="form-group">
                    <?= $form->field($model, 'name') ?>
                  </div>
                </div>
                <div class="cell-sm-6 cell-md-12 cell-lg-6 offset-top-17 offset-sm-top-0 offset-md-top-17 offset-lg-top-0">
                  <div class="form-group">
                    <?= $form->field($model, 'email') ?>
                  </div>
                </div>
                <div class="cell-xs-12 offset-top-17">
                  <div class="form-group">
                    <?= $form->field($model, 'message')->textArea(['rows' => '6']); ?>
                  </div>
                </div>
                <div class="cell-xs-12 offset-top-30">
                  <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
                      'captchaAction' => '/contact/contact/captcha',
                      'options' => [
                          'class' => 'form-control',
                          'placeholder' => $model->getAttributeLabel('captcha')
                      ],
                      'template' => '<div class="row">
                      <div class="col-lg-8">{input}</div>
                      <div class="col-lg-4">{image}</div>
                      </div>',
                  ]);
                  ?>
                </div>
                <div class="cell-xs-12 offset-top-30">
                  <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary']) ?>
                </div>
              </div>
              <?php ActiveForm::end(); ?>
          </div>
        </div>
        <div class="cell-md-4 cell-md-preffix-1 cell-xl-3 offset-top-60 offset-md-top-0">
          <div class="range">
            <div class="cell-sm-6 cell-md-12">



              </div>

            </div>
            <div class="cell-sm-6 cell-md-12 offset-top-50 offset-sm-top-0 offset-md-top-60 offset-lg-top-82">
<div class="swiper-slide-image"><img src="/images/contact_img.jpg" alt="" width="571" height="442"/>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>