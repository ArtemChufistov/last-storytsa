<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
		'label' => 'Как это работает',
		'url' => '/howitworks'
	],
];
$this->title = $model->title;
?>

<main class="page-content">
  <!-- Breadcrumbs & Page title-->
  <section class="section-30 section-sm-top-60 section-md-top-80 section-xl-top-120 bg-image bg-image-1">
    <div class="shell shell-fluid">
      <div class="range range-condensed">
        <div class="cell-xs-12 text-center">
          <p class="h6 subheader"><?php echo Yii::t('app', 'о системе');?></p>
          <p class="h1"><?php echo Yii::t('app', 'Как это работает');?></p>
          <?= Breadcrumbs::widget([
            'options' => ['class' => 'breadcrumbs-custom'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>
        </div>
      </div>
    </div>
  </section>

  <!-- About Us-->
  <section class="section-60 section-sm-80 section-lg-120">
    <div class="shell shell-wide">
      <div class="range range-condensed">
        <div class="cell-md-6 cell-xl-preffix-1 cell-xl-4">
          <div class="cell-box">
           
            <h2><?php echo Yii::t('app', 'Выгодный маркетинг');?></h2>
            <div class="offset-top-40 offset-sm-top-50">
              <div class="inset-lg-right-30 inset-xl-right-55">
                
              </div>
            </div>
            <p class="offset-top-20"><?php echo Yii::t('app', 'Цель данного маркетинга - позволить любому партнеру с небольшими финансовыми возможностями купить жилье в элитном месте, пригодное как для отдыха, сдачи в аренду, так и для проживания постоянно.
');?></p>
            <p>
              <?php echo Yii::t('app', 'Здесь не просто накопление, но есть и постоянный заработок на каждом этапе. Имеете возможность выполнив определенные условия осуществить поездку в Турцию   на семинар. Заключить договор на подбор жилья  с приглашенными представителями агентств недвижимости ,  получить полную консультацию профессионалов и  выбрать подходящий вариант недвижимости.  Важная задача маркетинга, что бы не было потерь вложенных средств участниками. Такой компромисс найден в нашем маркетинге. Если партнер выполнил хотя бы первый шаг, он вернет все, что вложил в начале пути, и далее имеет только заработок.  Работают различные ускорительные механизмы.');?>
             
            </p>
          </div>
        </div>
        <div class="cell-md-6 cell-xl-preffix-1 cell-xl-5 offset-top-40 offset-sm-top-60 offset-md-top-0">
          <div class="cell-box">
            <!-- RD Video Player-->
            <div data-rd-video-path="/evinizi/Evinizi" data-rd-video-title="Evinizi" data-rd-video-muted="true" data-rd-video-preview="images/homev2-1-800x450.jpg" class="rd-video-player max-width-710 play-on-scroll">
              <div class="rd-video-wrap embed-responsive-16by9">
                <div class="rd-video-preloader"></div>
                <video preload="metadata"></video>
                <div class="rd-video-preview"></div>
                <div class="rd-video-top-controls">
                  <!-- Title--><span class="rd-video-title"></span><a href="#" class="rd-video-fullscreen mdi mdi-fullscreen rd-video-icon"></a>
                </div>
                <div class="rd-video-controls">
                  <div class="rd-video-controls-buttons">
                    <!-- Play\Pause button--><a href="#" class="rd-video-play-pause mdi mdi-play"></a>
                    <!-- Progress bar-->
                  </div>
                  <div class="rd-video-progress-bar"></div>
                  <div class="rd-video-time"><span class="rd-video-current-time"></span><span class="rd-video-time-divider">:</span><span class="rd-video-duration"></span></div>
                  <div class="rd-video-volume-wrap">
                    <!-- Volume button--><a href="#" class="rd-video-volume mdi mdi-volume-high rd-video-icon"></a>
                    <div class="rd-video-volume-bar-wrap">
                      <!-- Volume bar-->
                      <div class="rd-video-volume-bar">
                        <!-- Volume bar slider-->
                        <div class="rd-video-volume-bar-slider"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="object-wrap object-wrap-left">
    <div class="shell shell-wide">
      <div class="range range-condensed">
        <div class="cell-md-7 cell-xl-6 cell-md-preffix-5">
          <div class="section-60 section-lg-100 section-xl-120 bg-wrap bg-wrap-right bg-gray-lighter">
            <div class="cell-box-wide inset-xl-left-160">
              <h2><?php echo Yii::t('app', 'Как это работает.');?></h2>
              <ul class="timeline offset-top-40 offset-md-top-50">
                <li class="timeline-item">
                  <div class="timeline-item-header">
                    <p class="text-bold"><?php echo Yii::t('app', 'ШАГ 1');?></p>
                  </div>
                  <div class="timeline-item-body">
                    <p><?php echo Yii::t('app', 'СТАРТ. После регистрации  вам нужно  оплатит место  на сайте.  Выбираете из предложенных удобную для вас систему электронных платежей. Сумма должна быть на кошельке + проценты перевода  системы. <br>
На сайт должны поступить 100$ <br>
Приглашаете 3 партнера по 100$<br>
Когда третий партнер активирован в программу, вам в кошелек поступает 100$ <br>
Можно заказать выплату или использовать для активации нового партнера.<br>
Вы переходите на следующий круг.');?></p>
                  </div>
                </li>
                <li class="timeline-item">
                  <div class="timeline-item-header">
                    <p class="text-bold"><?php echo Yii::t('app', 'ШАГ 2');?></p>
                  </div>
                  <div class="timeline-item-body">
                    <p><?php echo Yii::t('app', 'НАКОПИТЕЛЬНАЯ. Вы стоите на первом месте вверху<br>
Ваши 3 партнера становятся под вами во второй ряд. <br>
Третий ряд заполняется слева направо  партнерами ваших партнеров.<br>
При заполнении первого места удерживается 50$ на содержание проекта и 
в кошелек за каждого нового партнера в 3 ряду  с 1-го по 8-е место 
вам начисляется  по 25$, и за закрытие 9-го места  - 50$<br>
Всего в этом круге вами заработано 250$.');?></p>
                  </div>
                </li>
                <li class="timeline-item">
                  <div class="timeline-item-header">
                    <p class="text-bold"><?php echo Yii::t('app', 'ШАГ 3');?></p>
                  </div>
                  <div class="timeline-item-body">
                    <p><?php echo Yii::t('app', 'ПРЕМИАЛЬНАЯ. Также как и в предыдущем круге вы занимаете первое верхнее  место.<br> 
Под вами становятся 3 партнера вашей структуры, закрывшие раньше свой второй круг. Здесь имеет место обгон.<br>
Ваша задача помочь отставшим. Для этого предусмотрен  ваучер* для активации новых партнеров.  Его можно обналичить, только через активацию новых мест в первом круге.
Этот ваучер  на 300$ вы получаете когда  начинает заполняться третий ряд и первый партнер встал в нижний ряд.
Благодаря этим ваучерам, система постоянно подпитывается новыми партнерами, что дает быстрое продвижение. <br>
Вам на  кошелек начисляют 100$, их можно вывести.<br>
По мере заполнения  со 2-го  по 9-е места  за каждого партнера вам на кошелек для вывода приходит по 200$<br>
Итого за этот круг вы имеете 200$х8=1600$+100$=1700$  и плюс ваучер  300$.<br>
ВСЕГО 2000$
');?></p>
                  </div>
                </li>
                <li class="timeline-item">
                  <div class="timeline-item-header">
                    <p class="text-bold"><?php echo Yii::t('app', 'ШАГ 4');?></p>
                  </div>
                  <div class="timeline-item-body">
                    <p><?php echo Yii::t('app', 'ФИНИШНАЯ-ПРИЗОВАЯ, Суть НОУ-ХАУ нашего проекта состоит в том, что в финишный круг попадают партнеры из любой структуры, которые первыми закрыли предыдущий круг. Происходит смештвание структур, что дает эффект значительного ускорения, снимая проблему замедления прохождения финальных матриц.<br>
Кроме этого, так же как и в премиальном круге, когда первый партнер займет первое место в нижней четверке, вы получаете  ваучер№2  на 300$, и имеете право на выбор: <br>
1. активировать 3 новых партнеров  в проект ,  или - <br>
2.  инвестировать благотворительный взнос в международную кроуфандинговую площадку, с правом получать пожертвования до 3,5 миллионов евро.<br>
Вам на кошелек начислят  по 10000$ за каждое заполненное место в нижней четверке. Их можно выводить поэтапно.<br> 
По желанию партнера,  будет рекомендовано купить пай в накопительном фонде и в процессе заработка следующей суммы - увеличить первоначальную.<br>   
Вы получаете логин реинвеста в предыдущий  премиальный круг.  И снова зарабатываете. <br>
Также вас премируют  билетом участника Первого Семинара лидеров с участием представителей агентств по недвижимости  Турции и других стан.<br>
Всего   заработано в этом круге 40000$ + 300$ ваучер + 1500$ логин реинвеста, ПЛЮС билет с перелетом на Семинар в Турцию.<br>
На семинаре  или онлайн вы сможете заключить договор на подбор желаемой недвижимости с любым агентством. При наличии такого договора у вас будет возможность поэтапного вывода начисленных заработков в премиальном круге.<br>
Всего ваш логин реинвеста заработает  за  повтор в  2 круга (премиальный и финишный)   47 400$   + 2 x 300 = 600$  ваучерами.<br><br> Всего в проекте зарабатываете 90350$.<br><br>

ваучер*- срок действия ваучера 3 месяца 
');?></p>
                  </div>
                </li>
                 </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="object-wrap-body md-width-41p"><img src="/images/history-1-800x754.jpg" alt="" width="800" height="754"/>
    </div>
  </section>

  <!-- Testimonials
  <section class="section-60 section-sm--80 section-xl-120">
    <div class="shell shell-fluid text-center">
      <div class="range range-xs-center range-condensed">
        <div class="cell-xl-10">
          <p class="h6 subheader"><?php echo Yii::t('app', 'Подзаголовок');?></p>
          <h2><?php echo Yii::t('app', 'Большой заголовок');?></h2>
        </div>
        <div class="cell-xl-10 offset-top-30 offset-sm-top-60">
          <!-- Owl Carousel 
          <div data-items="1" data-md-items="2" data-stage-padding="0" data-loop="true" data-margin="30" data-nav="true" data-dots="true" data-animation-in="fadeIn" data-animation-out="fadeOut" class="owl-carousel owl-carousel-variant-1 owl-nav-justify">
            <div class="item">
              <!-- Blockquote default 
              <blockquote class="quote-default quote-default-centered">
                <div class="quote-body">
                  <p>
                    <q><?php echo Yii::t('app', 'Абзац с текстом и какое-то небольшое описание, скорее всего тут надо написать про маркетинг. Абзац с текстом и какое-то небольшое описание, скорее всего тут надо написать про маркетинг');?></q>
                  </p>
                </div>
                <div class="quote-footer">
                  <div class="unit unit-horizontal unit-spacing-sm unit-middle unit-align-center">
                    <div class="unit-left">
                      <figure class="quote-image"><img src="/images/testimonials-1-60x60.jpg" alt="" width="60" height="60"/>
                      </figure>
                    </div>
                    <div class="unit-body">
                      <cite>Patrick Pool</cite><small>Sales Manager</small>
                    </div>
                  </div>
                </div>
              </blockquote>
            </div>
            <div class="item">
              <!-- Blockquote default 
              <blockquote class="quote-default quote-default-centered">
                <div class="quote-body">
                  <p>
                    <q><?php echo Yii::t('app', 'Абзац с текстом и какое-то небольшое описание, скорее всего тут надо написать про маркетинг. Абзац с текстом и какое-то небольшое описание, скорее всего тут надо написать про маркетинг');?></q>
                  </p>
                </div>
                <div class="quote-footer">
                  <div class="unit unit-horizontal unit-spacing-sm unit-middle unit-align-center">
                    <div class="unit-left">
                      <figure class="quote-image"><img src="/images/testimonials-2-60x60.jpg" alt="" width="60" height="60"/>
                      </figure>
                    </div>
                    <div class="unit-body">
                      <cite>Sharon Gray</cite><small>Top Model</small>
                    </div>
                  </div>
                </div>
              </blockquote>
            </div>
            <div class="item">
              <!-- Blockquote default 
              <blockquote class="quote-default quote-default-centered">
                <div class="quote-body">
                  <p>
                    <q><?php echo Yii::t('app', 'Абзац с текстом и какое-то небольшое описание, скорее всего тут надо написать про маркетинг. Абзац с текстом и какое-то небольшое описание, скорее всего тут надо написать про маркетинг');?></q>
                  </p>
                </div>
                <div class="quote-footer">
                  <div class="unit unit-horizontal unit-spacing-sm unit-middle unit-align-center">
                    <div class="unit-left">
                      <figure class="quote-image"><img src="/images/testimonials-3-60x60.jpg" alt="" width="60" height="60"/>
                      </figure>
                    </div>
                    <div class="unit-body">
                      <cite>Sam McDonald</cite><small>Teacher</small>
                    </div>
                  </div>
                </div>
              </blockquote>
            </div>
            <div class="item">
              <!-- Blockquote default 
              <blockquote class="quote-default quote-default-centered">
                <div class="quote-body">
                  <p>
                    <q><?php echo Yii::t('app', 'Абзац с текстом и какое-то небольшое описание, скорее всего тут надо написать про маркетинг. Абзац с текстом и какое-то небольшое описание, скорее всего тут надо написать про маркетинг');?></q>
                  </p>
                </div>
                <div class="quote-footer">
                  <div class="unit unit-horizontal unit-spacing-sm unit-middle unit-align-center">
                    <div class="unit-left">
                      <figure class="quote-image"><img src="/images/testimonials-4-60x60.jpg" alt="" width="60" height="60"/>
                      </figure>
                    </div>
                    <div class="unit-body">
                      <cite>Megan Evans</cite><small>Photographer</small>
                    </div>
                  </div>
                </div>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
-->
  <!-- Flickrfeed
  <section>
    <div class="shell shell-wide">
      <div data-flickr-tags="tm-61178" data-photo-swipe-gallery="gallery" class="range range-condensed flickr">
        <div class="cell-xs-6 cell-sm-4 cell-lg-2">
          <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item="data-photo-swipe-item" class="thumbnail thumbnail-flickr">
              <figure><img src="/images/_blank.png" data-image_z="src" data-title="alt" width="165" height="165" alt=""/></figure>
              <div class="caption"><span class="icon icon-lg icon-white fa fa-instagram"></span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-heart"></span>15</span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-comment"></span>3</span></div></a></div>
        </div>
        <div class="cell-xs-6 cell-sm-4 cell-lg-2">
          <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item="data-photo-swipe-item" class="thumbnail thumbnail-flickr">
              <figure><img src="/images/_blank.png" data-image_z="src" data-title="alt" width="165" height="165" alt=""/></figure>
              <div class="caption"><span class="icon icon-lg icon-white fa fa-instagram"></span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-heart"></span>15</span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-comment"></span>3</span></div></a></div>
        </div>
        <div class="cell-xs-6 cell-sm-4 cell-lg-2">
          <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item="data-photo-swipe-item" class="thumbnail thumbnail-flickr">
              <figure><img src="/images/_blank.png" data-image_z="src" data-title="alt" width="165" height="165" alt=""/></figure>
              <div class="caption"><span class="icon icon-lg icon-white fa fa-instagram"></span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-heart"></span>15</span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-comment"></span>3</span></div></a></div>
        </div>
        <div class="cell-xs-6 cell-sm-4 cell-lg-2">
          <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item="data-photo-swipe-item" class="thumbnail thumbnail-flickr">
              <figure><img src="/images/_blank.png" data-image_z="src" data-title="alt" width="165" height="165" alt=""/></figure>
              <div class="caption"><span class="icon icon-lg icon-white fa fa-instagram"></span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-heart"></span>15</span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-comment"></span>3</span></div></a></div>
        </div>
        <div class="cell-xs-6 cell-sm-4 cell-lg-2">
          <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item="data-photo-swipe-item" class="thumbnail thumbnail-flickr">
              <figure><img src="/images/_blank.png" data-image_z="src" data-title="alt" width="165" height="165" alt=""/></figure>
              <div class="caption"><span class="icon icon-lg icon-white fa fa-instagram"></span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-heart"></span>15</span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-comment"></span>3</span></div></a></div>
        </div>
        <div class="cell-xs-6 cell-sm-4 cell-lg-2">
          <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item="data-photo-swipe-item" class="thumbnail thumbnail-flickr">
              <figure><img src="/images/_blank.png" data-image_z="src" data-title="alt" width="165" height="165" alt=""/></figure>
              <div class="caption"><span class="icon icon-lg icon-white fa fa-instagram"></span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-heart"></span>15</span><span class="block-iconed block-iconed-white"><span class="icon icon-xs-variant-1 fa fa-comment"></span>3</span></div></a></div>
        </div>
      </div>
    </div>
  </section>
-->
  <!-- Contact info
  <section class="section-60 text-center">
    <div class="shell shell-fluid">
      <div class="range range-condensed range-bordered">
        <div class="cell-xs-6 cell-sm-4 height-fill">
          <div class="range-bordered-item">
            <address class="contact-info contact-info-sm">
              <div class="icon-wrap"><span class="icon icon-md icon-abbey mdi mdi-cellphone-android"></span></div>
              <div class="link-wrap"><a href="callto:#" class="link link-md link-gray-darker"><?php echo Yii::t('app', '123-456-789');?></a></div>
            </address>
          </div>
        </div>
        <div class="cell-xs-6 cell-sm-4 height-fill offset-top-40 offset-xs-top-0">
          <div class="range-bordered-item">
            <address class="contact-info contact-info-sm">
              <div class="icon-wrap"><span class="icon icon-md icon-abbey mdi mdi-map-marker"></span></div>
              <div class="link-wrap"><a href="#" class="link link-md link-gray-darker"><?php echo Yii::t('app', 'Какойто адрес');?><br>CA 94117-1080 USA</a></div>
            </address>
          </div>
        </div>
        <div class="cell-xs-6 cell-sm-4 height-fill offset-top-40 offset-sm-top-0">
          <div class="range-bordered-item">
            <address class="contact-info contact-info-sm">
              <div class="icon-wrap"><span class="icon icon-md icon-abbey mdi mdi-email-outline"></span></div>
              <div class="link-wrap"><a href="mailto:#" class="link link-md link-gray-darker"><?php echo Yii::t('app', 'info@evinizi.com');?></a></div>
            </address>
          </div>
        </div>
      </div>
    </div>
  </section>-->

</main>