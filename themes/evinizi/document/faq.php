<?php
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'Faq',
    'url' => '/faq'
  ],
];
$this->title = $model->title;
?>
<!-- Page Content-->
<main class="page-content">
  <!-- Breadcrumbs & Page title-->
  <section class="section-30 section-sm-top-60 section-md-top-80 section-xl-top-120 bg-image bg-image-1">
    <div class="shell shell-fluid">
      <div class="range range-condensed">
        <div class="cell-xs-12 text-center">
          <p class="h6 subheader"><?php echo Yii::t('app', 'Часто задаваемые вопросы');?></p>
          <p class="h1"><?php echo Yii::t('app', 'FAQ');?></p>
            <?= Breadcrumbs::widget([
              'options' => ['class' => 'breadcrumbs-custom'],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
      </div>
    </div>
  </section>

  <!-- Accordion FAQ-->
  <section class="section-30 section-md-0">
    <div class="shell shell-wide">
      <div class="range range-condensed range-md-middle">
        <div class="cell-md-6 cell-xl-preffix-1 cell-xl-5">
          <div class="section-md-80 section-lg-120">
            <div class="cell-box-minimal">
              <h2><?php echo Yii::t('app', 'Вопрос-ответ');?></h2>
              <!-- Responsive-tabs-->
              <div data-type="accordion" class="responsive-tabs responsive-tabs-button">
                <ul class="resp-tabs-list">
                  <li><?php echo Yii::t('app', 'Как стать участником системы?');?></li>
                  <li><?php echo Yii::t('app', 'Могу ли я изменить свои данные после регистрации');?></li>
                  <li><?php echo Yii::t('app', 'Как верифицировать E-mail');?></li>
                  <li><?php echo Yii::t('app', 'Как установить или изменить свои реквизиты');?></li>

                  <li><?php echo Yii::t('app', 'Какие варианты оплаты присутствуют');?></li>
                  <li><?php echo Yii::t('app', 'Сколько времени даётся на активацию аккаунта');?></li>
                  <li><?php echo Yii::t('app', 'Нужно ли ждать подтверждение оплаты или подтверждать получение');?></li>
                  <li><?php echo Yii::t('app', 'Где мне увидеть мою реферальную ссылку');?></li>
                </ul>
                <div class="resp-tabs-container">
                  <div>
                    <p><?php echo Yii::t('app', 'Для того, чтобы стать участником, Вам достаточно пройти простую форму регистрации на нашем сайте, перейдя по реферальной ссылке Вашего пригласителя. Обращаем Ваше внимание на то, что данные указываемые при регистрации должны быть достоверны.');?></p>
                  </div>
                  <div>
                    <p><?php echo Yii::t('app', 'Вам доступно любое изменение данных своего кабинета, кроме изменения Вашего логина и Вашего спонсора. ');?></p>
                  </div>
                  <div>
                    <p><?php echo Yii::t('app', 'Для того чтобы верифицировать e-mail вам необходимо в разделе профиль указать действующую почту и получить активационную ссылку. После того, как вы это сделаете, ваш e-mail будет верифицирован.');?></p>
                  </div>
                  <div>
                    <p>
                      <?php echo Yii::t('app', 'Прежде всего необходимо получить платежный пароль. Это можно сделать в разделе реквизиты. После этого, вы сможете добавить/изменить реквизиты. Получить платежный пароль возможно только на верифицированную почту. ');?>
                    </p>
                  </div>
                  <div>
                    <p>
                      <?php echo Yii::t('app', 'Perfect Money, Advanced Cash, Payeer ');?>
                    </p>
                  </div>
                  <div>
                    <p>
                      <?php echo Yii::t('app', 'Вы можете произвести активацию в любой момент времени после того как пройдете регистрацию. Ваша регистрация будет действительна постоянно. Но настоятельно рекомендуем Вам не тянуть с активацией, так как Вы будете терять выплаты от переливов общего распределения участников в структуре.  ');?>
                    </p>
                  </div>
                  <div>
                    <p>
                      <?php echo Yii::t('app', 'В проекте все переводы между участниками полностью автоматизированы и не требуют подтверждения от их получателя. Транзакция считается успешной при подтверждении ее сетью. ');?>
                    </p>
                  </div>
                  <div>
                    <p>
                      <?php echo Yii::t('app', 'Реферальная ссылка находится в разделе профиль вашего личного кабинета. Она становится доступной вам, только после активации аккаунта и получения места в программе. ');?>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="cell-md-6 offset-top-40 offset-md-top-0 text-center">
          <div class="inset-xl-left-80 inset-xl-right-80">
            <figure><img src="/images/faq-1-737x438.jpg" alt="" width="737" height="438"/>
            </figure>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="bg-gray-lighter">
    <div class="shell shell-wide">
      <div class="range range-condensed range-md-middle">
        <div class="cell-md-5 cell-lg-6 height-fill veil reveal-sm-flex image-column-wrap">
          <div class="range range-condensed image-column-inner">
            <div class="cell-sm-6 cell-md-12 cell-xl-6 height-fill">
              <div class="image-column image-column-2"><img src="/images/faq-2-480x642.jpg" alt="" width="480" height="642"/><img src="/images/faq-3-480x642.jpg" alt="" width="480" height="642"/>
              </div>
            </div>
            <div class="cell-sm-6 cell-md-12 cell-xl-6 height-fill">
              <div class="image-column image-column-3"><img src="/images/faq-4-480x321.jpg" alt="" width="480" height="321"/><img src="/images/faq-5-480x642.jpg" alt="" width="480" height="642"/><img src="/images/faq-6-480x321.jpg" alt="" width="480" height="321"/>
              </div>
            </div>
          </div>
        </div>
        <div class="cell-md-7 cell-lg-6 cell-xl-preffix-1 cell-xl-4 offset-top-0 content-scroll-wrap">
          <div class="content-scroll-content section-60 section-sm-80 section-xl-100">
            <div class="cell-box-wide">
              <h2><?php echo Yii::t('app', 'Дополнительные</br> вопросы-ответы');?></h2>
              <ul class="list-marked list-abbey offset-top-40 offset-sm-top-50">
                <li><a href="#q1" data-custom-scroll-to="q1"><?php echo Yii::t('app', 'Как я смогу использовать свою реферальную ссылку');?></a></li>
                <li><a href="#q2" data-custom-scroll-to="q2"><?php echo Yii::t('app', 'Как долго ждать подтверждения транзакции в сети');?></a></li>
                <li><a href="#q3" data-custom-scroll-to="q3"><?php echo Yii::t('app', 'Сколько человек я могу пригласить в систему');?></a></li>
                <li><a href="#q4" data-custom-scroll-to="q4"><?php echo Yii::t('app', 'Сколько я смогу заработать');?></a></li>
                <li><a href="#q5" data-custom-scroll-to="q5"><?php echo Yii::t('app', 'Что делать если забыли пароль');?></a></li>
                <li><a href="#q6" data-custom-scroll-to="q5"><?php echo Yii::t('app', 'Остались вопросы');?></a></li>
              </ul>
              <dl class="list-terms-variant-1">
                <dt id="q1" class="h5"><?php echo Yii::t('app', 'Как я смогу использовать свою реферальную ссылку');?></dt>
                <dd>
                  <?php echo Yii::t('app', 'Реферальная ссылка для каждого участника генерируется индивидуально и дает Вам возможность привлекать людей именно в Вашу структуру. Вы можете размещать ее в социальных сетях, блогах, тематических форумах и прикреплять к письмам в E-mail рассылках. Это обеспечит Вас постоянным притоком новых участников в Вашу структуру, а значит, Ваш доход будет расти. ');?>
                </dd>
                <dt id="q2" class="h5"><?php echo Yii::t('app', 'Как долго ждать подтверждения транзакции в сети');?></dt>
                <dd>
                  <?php echo Yii::t('app', 'Если переводы проходят в сети биткоин и требуется время, для того чтобы транзакция принялась сетью и подтвердилась. С учетом особенности сети ожидание подтверждения может занимать несколько часов. Статус вашей транзакции можно увидеть в деталях заявки. ');?>
                </dd>
                <dt id="q3" class="h5"><?php echo Yii::t('app', 'Сколько человек я могу пригласить в систему');?></dt>
                <dd>
                  <?php echo Yii::t('app', 'Вы можете приглашать сколько угодно новых участников. Если они будут занимать свободные места в вашей структуре ');?>
                </dd>
                <dt id="q4" class="h5"><?php echo Yii::t('app', 'Сколько я смогу заработать');?></dt>
                <dd>
                  <?php echo Yii::t('app', 'Доход за полный цикл составляет: 90350$ . С учетом реинвестов и повторных закрытий вы сможете получать пассивный доход бесконечное количество раз ');?>
                </dd>
                <dt id="q5" class="h5"><?php echo Yii::t('app', 'Что делать если забыли пароль');?></dt>
                <dd><?php echo Yii::t('app', 'Воспользуйтесь функцией восстановления пароля. ');?></dd>
                <dt id="q6" class="h5"><?php echo Yii::t('app', 'Остались вопросы');?></dt>
                <dd><?php echo Yii::t('app', 'Задать вопрос вы можете на странице обратной связи или в личном кабинете в разделе "Техническая поддержка" ');?></dd>
              </dl>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</main>