<?php
use yii\widgets\Breadcrumbs;
use yii\web\View;
use yii\helpers\Url;


$this->params['breadcrumbs']= [ [
    'label' => 'О Нас',
    'url' => '/about'
  ],
];
$this->title = $model->title;
?>

<main class="page-content">
  <!-- Breadcrumbs & Page title-->
  <section class="section-30 section-sm-top-60 section-md-top-80 section-xl-top-120 bg-image bg-image-1">
    <div class="shell shell-fluid">
      <div class="range range-condensed">
        <div class="cell-xs-12 text-center">
          <p class="h6 subheader"><?php echo Yii::t('app', 'информация');?></p>
          <p class="h1"><?php echo Yii::t('app', 'О нас');?></p>
          <?= Breadcrumbs::widget([
            'options' => ['class' => 'breadcrumbs-custom'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>
        </div>
      </div>
    </div>
  </section>

  <!-- What We Do-->
  <section class="bg-gray-lighter">
    <div class="shell shell-wide">
      <div class="range range-condensed range-md-middle">
        <div class="cell-md-6 cell-xl-preffix-1 cell-xl-4">
          <div class="cell-box">
            <div class="section-60">
              
              <h2><?php echo Yii::t('app', 'Наши преимущества');?></h2>
              <div class="offset-top-40 offset-sm-top-50">
                <div class="inset-lg-right-30 inset-xl-right-55">
                  <p class="h5"><?php echo Yii::t('app', 'НАДЕЖНО, ДЕНЕЖНО, КОМФОРТНО!');?></p>
                </div>
              </div>
              <p class="offset-top-20"><?php echo Yii::t('app', '<strong>УДОБНО.</strong>
По статистике 27% россиян заняты в интернет бизнесе.  В США эта цифра достигает 67%.
Работа из дома по свободному графику отличная возможность, в первую очередь для молодых мам, студентов, пенсионеров и вообще всех, кому нужен заработок. Работа в таких проектах  не требует почасового присутствия .  Каждый может работать по личному плану. Уделяя работе столько, сколько желает зарабатывать.  Работа рекламного менеджера с  недвижимостью на финише, не требует ежемесячных закупок и продаж товара , что конечно удобнее и менее хлопотно. <br>
<strong>ВЫГОДНО.</strong> 
Каждый партнер сам решает, есть ли это основной заработок, или дополнительный.  В заисимости от личных планов, уделяя этому бизнесу столько времени, сколько сам желает.  Выполнив обязательные первоначальные условия можно строить команду дальше. Заниматься обучением и консультациями новых партнеров. Быстро выстроенная и обученная ведению бизнеса структура даст быстрые и высокие результаты. <br>
<strong>ПРОФЕССИОНАЛЬНО. </strong>
Вступая в бизнес  каждый партнер имеет необходимые инструменты : профессиональный сайт, со всей необходимой для успешной  работы информацией в круглосуточном включении, удобный в пользовании личный кабинет, автоматические системы перевода денег в личном кабинете. Постоянно проводимые лидерами презентации и конференции, где можно получить ответ на все возникшие в работе вопросы, запись вебинаров на канале Youtube. Связь с техподдержкой на сайте. Обучающие интернет бизнесу лидерские школы. <br>
<strong>ИНТЕРЕСНО</strong> - промоушены. 
Для стимулирования результатов организовываются заманчивые промоушены и премии для партнеров, активно развивающих  свои структуры. <br>
1. <strong>ГЛАВНЫМ СОБЫТИЕМ</strong> первого полугодия станет Большая встреча- семинар  успешных лидеров   с представителями агентств по недвижимости в Турции. Пригласительный билет с проживанием и перелетом, развлекательной программой и программой  семинара получит каждый партнер, выполнивший условие - закрывший финишный круг. На семинар будут приглашены менеджеры ведущих агентств недвижимости Турции и не только.<br>
2. <strong>ПРОМОУШЕН </strong> действует первый месяц после открытия:<br>
Пригласивший в первом круге   более 3х партнеров получает премию  по 10 дол за каждого , если тот  пригласил 3 партнера и вышел на выплату 100$.
 ');?></p>
              <div class="offset-top-30">
                <div class="group-lg"><a href="<?php echo Url::to(['/profile/profile/signup']);?>" class="btn btn-abbey"><?php echo Yii::t('app', 'Присоединиться');?></a>
                <a href="<?php echo Url::to(['/profile/profile/login']);?>" class="btn btn-ash-outline"><?php echo Yii::t('app', 'Личный кабинет');?></a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="cell-md-6 cell-xl-preffix-1 height-fill">
          <article class="box-custom box-custom-link box-custom-right">
            <figure class="box-image"><img src="/evinizi/udobno1.png" alt="" width="480" height="351"/>
            </figure>
            <div class="box-body">
              <div class="box-content">
                <div class="box-link-minimal">
                  <div class="box-link-icon">
                    <img src="/evinizi/About-1.png" style = "width: 59px;">
                  </div>
                  <div class="box-link-header">
                    <h4><span><?php echo Yii::t('app', 'Удобно');?></span></h4>
                    <div class="divider divider-sm"></div>
                  </div>
                </div>
              </div>
            </div>
          </article>
          <article class="box-custom box-custom-primary box-custom-link box-custom-left">
            <figure class="box-image"><img src="/evinizi/udobno2.png" alt="" width="480" height="351"/>
            </figure>
            <div class="box-body">
              <div class="box-content">
                <div class="box-link-minimal box-link-primary-filled">
                  <div class="box-link-icon">
                    <img src="/evinizi/About-2.png" style = "width: 59px;">
                  </div>
                  <div class="box-link-header">
                    <h4><span><?php echo Yii::t('app', 'Выгодно');?></span></h4>
                    <div class="divider divider-sm"></div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
        <div class="cell-md-6">
          <article class="box-custom box-custom-abbey box-custom-link box-custom-right">
            <figure class="box-image"><img src="/evinizi/prof.png" alt="" width="480" height="351"/>
            </figure>
            <div class="box-body">
              <div class="box-content">
                <div class="box-link-minimal box-link-abbey-filled">
                  <div class="box-link-icon">
                    <img src="/evinizi/About-3.png" style = "width: 59px;">
                  </div>
                  <div class="box-link-header">
                    <h4><span style="color:white;"><?php echo Yii::t('app', 'Профессионально');?></span></h4>
                    <div class="divider divider-sm"></div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
        <div class="cell-md-6">
          <article class="box-custom box-custom-link box-custom-left box-custom-md-right">
            <figure class="box-image"><img src="/evinizi/interesno.png" alt="" width="480" height="351"/>
            </figure>
            <div class="box-body">
              <div class="box-content">
                <div class="box-link-minimal box-link-abbey-colored">
                  <div class="box-link-icon">
                    <img src="/evinizi/About-4.png" style = "width: 59px;">
                  </div>
                  <div class="box-link-header">
                    <h4><span><?php echo Yii::t('app', 'Интересно');?></span></h4>
                    <div class="divider divider-sm"></div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>
    </div>
  </section>





</main>