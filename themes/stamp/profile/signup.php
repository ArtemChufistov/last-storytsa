<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use app\modules\mainpage\models\Preference;
use app\modules\profile\components\AuthChoice;

$this->title = Yii::t('app', 'Регистрация');

?>

<?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "{input}\n{hint}\n{error}"]]); ?>
  <div class="row">
    <h4><?php echo Yii::t('app', 'Регистрация');?></h4>
    
    <?= $form->field($model, 'login', ['template'=>'
    <div class="input-group input-group-icon">
      {input}
      <div class="input-icon">
        <i class="fa fa-user"></i>
      </div>
      {error}
    </div>'])->textInput([
        'maxlength' => true,
        'placeholder' => $model->getAttributeLabel('login'),
    ]);?>
    
    <?= $form->field($model, 'email', ['template'=>'
    <div class="input-group input-group-icon">
      {input}
      <div class="input-icon">
        <i class="fa fa-envelope"></i>
      </div>
      {error}
    </div>'])->textInput([
        'maxlength' => true,
        'placeholder' => $model->getAttributeLabel('email'),
    ]);?>
    
    <?= $form->field($model, 'password', ['template'=>'
    <div class="input-group input-group-icon">
      {input}
      <div class="input-icon">
        <i class="fa fa-key"></i>
      </div>
      {error}
    </div>'])->passwordInput([
        'maxlength' => true,
        'placeholder' => $model->getAttributeLabel('password'),
    ]);?>
  </div>
  <div class="row">
    <h4><?php echo Yii::t('app', 'Подтверждение');?></h4>
    <?= $form->field($model, 'passwordRepeat', ['template'=>'
    <div class="input-group input-group-icon">
      {input}
      <div class="input-icon">
        <i class="fa fa-key"></i>
      </div>
      {error}
    </div>'])->passwordInput([
        'maxlength' => true,
        'placeholder' => $model->getAttributeLabel('passwordRepeat'),
    ]);?>
    
    <div class="input-group input-group-icon">
    <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
        'imageOptions' => [
            'id' => 'my-captcha-image',
        ],
        'captchaAction' => '/profile/profile/captcha',
        'options' => [
            'class' => 'form-control',
            'placeholder' => $model->getAttributeLabel('captcha')
        ],
        'template' => '
        <div class="row">
          <div class="col-captcha-input">
              {input}
              <div class="input-icon">
                <i class="fa fa-lock"></i>
              </div>
            </div>
          <div class="col-captcha-image">{image}</div>
        </div>',
    ]);
    ?>
    </div>
  </div>
  <div class="row">
    <div class="input-group">
      <?= $form->field($model, 'licenseAgree', ['template' => '{input}<label for="inlineCheckbox111">' . Yii::t('app', 'Я согласен с условиями') . ' ' . Html::a(Yii::t('app', 'лицензионного соглашения'), ['#'], [
        'data-fancybox' => 'license-agreement',
        'data-src' => '#license-agreement',
      ]). '</label>{error}', 'options'=> ['tag' => 'div']])->checkBox(['label' => false, 'class' => 'custom-checkbox', 'id' => 'inlineCheckbox111']);?>
    </div>
  </div>
  <div class="row">
    <?= Html::submitButton(Yii::t('app', 'Зарегистрироваться'));?>
  </div>
<?php ActiveForm::end(); ?>

<?php echo $this->render('partial/_licenseAgreement');?>