<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use app\modules\profile\components\AuthChoice;

$this->title = Yii::t('user', 'Вход на сайт');

?>

<?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "{input}\n{hint}\n{error}"]]); ?>
  <div class="row">
    <h4><?php echo Yii::t('app', 'Личный кабинет');?></h4>
    <?= $form->field($model, 'loginOrEmail', ['template'=>'
    <div class="input-group input-group-icon">
      {input}
      <div class="input-icon">
        <i class="fa fa-user"></i>
      </div>
      {error}
    </div>'])->textInput([
        'maxlength' => true,
        'placeholder' => $model->getAttributeLabel('loginOrEmail'),
    ]);?>
    
    <?= $form->field($model, 'password', ['template'=>'
    <div class="input-group input-group-icon">
      {input}
      <div class="input-icon">
        <i class="fa fa-key"></i>
      </div>
      {error}
    </div>'])->passwordInput([
        'maxlength' => true,
        'placeholder' => $model->getAttributeLabel('password'),
    ]);?>
  </div>
  <div class="row">
    <div class="input-group">
      <?= $form->field($model, 'rememberMe', ['template' => '
      {input}
      <label for="inlineCheckbox111">
        ' . Yii::t('app', 'Запомнить меня'). '
      </label>
      {error}
      ', 'options'=> ['tag' => 'div']])->checkBox(['label' => false, 'class' => 'custom-checkbox', 'id' => 'inlineCheckbox111']);?>
    </div>
  </div>
  <div class="row">
    <?= Html::submitButton(Yii::t('app', 'Войти'));?>
  </div>
  <div class="row">
    <p>
      <?=Yii::t('user', 'Если')?> <?=Html::a(Yii::t('user', 'регистрировались'), ['/signup'])?> <?=Yii::t('user', 'ранее, но забыли пароль, нажмите')?>
      <?=Html::a(Yii::t('user', 'восстановить пароль'), ['#'], [
          'data-fancybox' => 'forgot-password',
          'data-src' => '#forgot-password',
          'id' => 'forgot-link'
      ])?>.
    </p>
  </div>
<?php ActiveForm::end(); ?>

<?php echo $this->render('partial/_pass', ['model' => $forget]);?>