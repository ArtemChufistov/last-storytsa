<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
?>

<div id="forgot-password" class="hidden" style="display: none;">
  <h4><?=Yii::t('app', 'Восстановление пароля');?></h4>
  
  <?php if (Yii::$app->session->hasFlash('reset-success')):?>
      <div class='text-center'><?php echo Yii::$app->session->getFlash('reset-success');?></div>
  <?php else: ?>
  <?php $form = ActiveForm::begin([
      'id' => 'pass-form',
      'fieldConfig' => [
          'template' => "{input}\n{hint}\n{error}"
      ],
  ]);?>
  
    <div class="row">
      <?= $form->field($model, 'email', ['template'=>'
      <div class="input-group input-group-icon">
        {input}
        <div class="input-icon">
          <i class="fa fa-user"></i>
        </div>
        {error}
      </div>'])->textInput([
          'maxlength' => true,
          'placeholder' => $model->getAttributeLabel('email'),
      ]);?>
    </div>
    
    <div class="row">
      <?= $form->field($model, 'password', ['template'=>'
      <div class="input-group input-group-icon">
        {input}
        <div class="input-icon">
          <i class="fa fa-key"></i>
        </div>
        {error}
      </div>'])->passwordInput([
          'maxlength' => true,
          'placeholder' => $model->getAttributeLabel('password'),
      ]);?>
    </div>
  
    <div class="row">
      <div class="input-group input-group-icon">
        <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
            'imageOptions' => [
                'id' => 'my-captcha-image',
            ],
            'captchaAction' => '/profile/profile/captcha',
            'options' => [
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('captcha')
            ],
            'template' => '
            <div class="row">
              <div class="col-captcha-input">
                  {input}
                  <div class="input-icon">
                    <i class="fa fa-lock"></i>
                  </div>
                </div>
              <div class="col-captcha-image">{image}</div>
            </div>',
        ]);
        ?>
      </div>
    </div>
  
    <p class="hint-block">
        <?= Yii::t('app', 'Ссылка с активацией нового пароля будет отправлена на Email, указанный при регистрации') ?>.
    </p>

    <div class="form-group text-center">
        <?= Html::submitButton(Yii::t('app', 'Сбросить пароль'));?>
    </div>
  
    <?php ActiveForm::end();?>
    <?php endif;?>
</div>

<?php if (Yii::$app->session->hasFlash('reset-success')|| $model->hasErrors()) {
    $this->registerJs('$("#forgot-link").trigger("click")');
}
