<div id="license-agreement" class="hidden" style="display: none;">
  <h4><?=Yii::t('app', 'Лицензионное соглашение');?></h4>
  <p>
  <?=Yii::t('app', '
  Please read carefully these Terms and Conditions (hereinafter – the “Terms”) before using a website “www.citt.fund” 
  (hereinafter – the “Website”), as they affect your obligations and legal rights, including, but not limited to waivers 
  of rights and limitation of liability. If you intend to hold CITT Tokens from the CITT Initial Coin Offering 
  (hereinafter – the “Token Launch”), you should also read these Terms and accept them. If you do not agree with these Terms, 
  you shall not use the Website or buy CITT Tokens.

  1. DEFINITIONS

  1.1 Account – a User’s account on the Website, which is created and used to hold CITT Tokens. A User is given the access to an 
  Account upon its successful creation through providing CITT Fund with all the required information. Only authorized Users have a 
  right to hold CITT Tokens on the terms provided herein.

  1.2 Agreement – these Terms and all other operating rules, policies, and procedures that may be issued by CITT and published from 
  time to time on the Website (including privacy policy, cookie policy etc.).

  1.3 Bitcoin or BTC – a consensus network that enables a new payment system and a completely digital money; the first decentralized 
  peer-to-peer payment network that is powered by its users with no central authority or middlemen.

  1.4 Blockchain – type of distributed ledger, comprised of unchangeable, digitally recorded, data in packages called blocks.

  1.5 Ethereum Smart Contracts – account holding objects on the Ethereum Blockchain, which contain code functions and can interact 
  with other contracts, make decisions, store data, and send “ether” to others.

  1.6 Kepler – a cryptocurrency portfolio management platform that covers a complete investment cycle and consists of individualized 
  market research tools, integrated order management and sophisticated performance forecasting etc.

  1.7 CITT Initial Coin Offering – a restricted offering to eligible Users only from March 27, 2017, to April 27, 2017, when a User 
  was able to buy CITT Tokens.

  1.8 CITT Tokens – cryptographic tokens, which are software product (digital resources), created by the Website Owner as a proof of 
  membership of their holders in the CITT Fund (a digital system, not a legal entity). Though CITT Tokens may have aspects in common 
  with securities, CITT Tokens are not securities, are not registered with any government entity as a security, and shall not be 
  considered as such.

  1.9 User – anyone who uses the Website, with or without prior registration and authorization using the Account.

  1.10 Website - the website maintained by CITT Fund Pte, Ltd. at http://citt.fund

  1.11 Website Owner , CITT Fund, Company, we, us – first tokenized closed-end fund designated to Blockchain assets; CITT Fund Pte.Ltd.,
  a company, registered under the laws of Singapore. In no way shall CITT Fund be deemed a partner, employer or agent for any User or 
  providing any financial services thereto.

  2. GENERAL INFORMATION

  2.1 These Terms are a legally binding Agreement between you, the User, on the one part, and the Website Owner, on the other part, also individually referred to as a “Party” and collectively as the “Parties”.
  2.2 These Terms define basic mutual rights and obligations of the Website Owner and the Users, either registered or just visiting certain pages of the Website, during their use of the Website, including but without limitation, for the purpose of buying the CITT Tokens.
  2.3 By using the Website, the Users accept these Terms in full and agree to be bound thereby and comply therewith.
  2.4 These Terms are effective at the time the Users begin using the Website. The Users may withdraw from their obligation under the Terms at any time by discontinuing the use of the Website.
  2.5 The User acknowledges and accepts that:
  - these Terms are subject to change, modifications, amendments, alterations or supplements at any time without prior written notice, at Website Owner’s sole discretion, by updating this posting at the “Last Updated” section; the User’s continued use of the Website after the amendments etc. shall constitute the User’s consent hereto and acceptance hereof;
  - the Website Owner reserves the right, at its own and complete discretion, to modify or to temporarily or permanently suspend or eliminate the Website, and/or disable any access to the Website.
  2.6 By using this Website, you covenant, represent, and warrant that:
  - you are of an age of majority to enter into this Agreement, meet all other eligibility and residency requirements, and are fully able and legally competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth herein and to abide by and comply herewith;
  - you are aware of all the merits, risks and any restrictions associated with cryptographic tokens (their buying and use), cryptocurrencies and Blockchain-based systems, as well as you know how to manage them, and you are solely responsible for any evaluations based on such your knowledge;
  - you have necessary and relevant experience and knowledge to deal with cryptographic tokens, cryptocurrencies and Blockchain-based systems, as well as you have full understanding of their framework.
  2.7 You shall not use the Website if you are prohibited under the applicable law from using it. Any User that is in any manner limited or prohibited from the purchase, possession, transfer, use or other transaction involving any amount of CITT Tokens under the applicable law should not access this Website and is prohibited accessing, referencing, engaging, or otherwise using this Website.

  3. SALE OF CITT TOKENS

  3.1 CITT Tokens were available for purchase to eligible buyers only during the period of CITT Initial Coin Offering set out herein. No more CITT Tokens are available for purchase at this time.
  3.2 During the Token Launch, the Website Owner sold approximately US$7.963 million of CITT Tokens. Price of one CITT Token is $1 in BTC. The number of the CITT Tokens allowed for purchase by one User was not limited. The CITT Tokens will not be issued again. The CITT Tokens will be Ethereum-based cryptographic tokens of value.
  3.3 The CITT Tokens are the proof of their holders’ membership in the CITT Fund (a digital system, not a legal entity). The CITT Tokens provide to their holders a right to receive 50% of quarterly profits. At the same time, 25% of profit will be reinvested back into the portfolio. The CITT Tokens also provide to their holders a right to claim a free 1-year access to the Kepler. The Website Owner reserves the right to consult with the holders of the CITT Tokens on the matters related to the functioning of the Company.
  3.4 Profit distribution will be carried out using specially designed Ethereum Smart Contract (0xe7775a6e9bcf904eb39da2b68c5efb4f9360e08c).
  3.5 CITT Fund does not accept fiat currency as payment for CITT Tokens. In order to buy CITT Tokens with legal tender user shall convert such funds into Bitcoin (BTC) or Ethereum (ETH).
  3.6 The CITT Tokens purchased during the Token Launch may be sold and transferred by the User at any time after the Token Launch via cryptocurrency exchanges if the CITT Tokens are listed by that exchange. It is hereby stipulated and declared to be the intention of the Parties that 10% of the funds raised during Token Launch shall be used as a starting base for the reserve fund. These funds may be used by the Website Owner in order to provide refund possibility (payout liquidity) in exceptional circumstances, as set out in clause 7.3. hereof.
  3.7 BY BUYING CITT TOKENS HEREUNDER THE USER REPRESENTS AND WARRANTS THAT HIS/HER FUNDS IN NO WAY CAME FROM ILLEGAL OR UNETHICAL SOURCES, THAT THE USER IS NOT USING ANY PROCEEDS OF CRIMINAL OR ILLEGAL ACTIVITY, AND THAT NO TRANSACTION INVOLVING CITT TOKENS ARE BEING USED TO FACILITATE ANY CRIMINAL OR ILLEGAL ACTIVITY.
  3.8 UNITED STATES CITIZENS AND RESIDENTS ARE NOT ELIGIBLE TO PARTICIPATE IN THE Token Launch. YOU ARE ONLY ALLOWED TO PURCHASE CITT TOKENS IF AND BY BUYING CITT TOKENS YOU COVENANT, REPRESENT, AND WARRANT THAT YOU ARE NEITHER A U.S. CITIZEN OR PERMANENT RESIDENT OF THE UNITED STATES, NOR DO YOU HAVE A PRIMARY RESIDENCE OR DOMICILE IN THE UNITED STATES, INCLUDING PUERTO RICO, THE U.S. VIRGIN ISLANDS, AND ANY OTHER POSSESSIONS OF THE UNITED STATES. IN ORDER TO BUY CITT TOKENS AND BY BUYING CITT TOKENS YOU COVENANT, REPRESENT, AND WARRANT THAT NONE OF THE OWNERS OF THE COMPANY, OF WHICH YOU ARE AN AUTHORIZED OFFICER, ARE U.S. CITIZEN OR PERMANENT RESIDENT OF THE UNITED STATES, NOR DO YOU HAVE A PRIMARY RESIDENCE OR DOMICILE IN THE UNITED STATES, INCLUDING PUERTO RICO, THE U.S. VIRGIN ISLANDS, AND ANY OTHER POSSESSIONS OF THE UNITED STATES. SHOULD THIS CHANGE AT ANY TIME, YOU SHALL IMMEDIATELY NOTIFY CITT FUND. THE COMPANY SHALL RESERVE THE RIGHT TO REFUSE SELLING CITT TOKENS TO ANYONE WHO DOES NOT MEET CRITERIA NECESSARY FOR THEIR BUYING, AS SET OUT HEREUNDER AND BY THE APPLICABLE LAW. IN PARTICULAR, THE COMPANY MAY REFUSE SELLING CITT TOKENS TO U.S. CITIZENS, PERMANENT RESIDENTS OF THE UNITED STATES AND THOSE USERS WHO DO NOT MEET ANY CRITERIA SPECIFIED IN CLAUSE 2.6.

  4. USER REGISTRATION AND ACCOUNT

  4.1 For the purpose of buying CITT Tokens, Website Owner will register you, upon your request, on the Website and create an individual Account including a login and a password. You warrant that all information you have provided for your Account is current, complete and accurate. Registration data and other information about you are subject to CITT Privacy Policy available on the Website.
  4.2 You hereby expressly consent that you are solely responsible for the use of your login and password and for everything done using your registration details. You agree to keep your login information and password private and to immediately notify CITT Fund at once of any unauthorized Account activity you may be aware of and and modify your login Information. You are solely responsible for any loss or damage you or we may suffer as a result of your failure to do so.
  4.3 You may deactivate your registration with the Website, at any time and for any reason, by sending an email request to support@citt.fund. We may terminate your use of and registration with the Website at any time if you violate these Terms, at our sole discretion and without prior notice and without any liability or further obligation of any kind whatsoever to you or any other party, when we find such measures reasonable and/or necessary in a particular situation.

  5. THIRD-PARTY WEBSITES AND SERVICES

  5.1 The pages of the Website may contain links to third-party websites and services. Such links are provided for your convenience, but their presence does not mean that they are recommended by the CITT Fund. In addition, the CITT Fund does not guarantee their safety and conformity with any user expectations. Furthermore, we are not responsible for maintaining any materials referenced from another site, and makes no warranties for that site or this service in such context.
  5.2 The CITT Fund assumes no obligations in the event of any damage or loss, or any other impact, directly or indirectly resulting from the use of any content, goods or services available on or through any such third-party websites and resources.

  6. INDEMNIFICATION

  6.1 To the extent allowable pursuant to applicable law, the User shall indemnify, defend, and hold the CITT Fund and/or its subsidiaries, affiliates, directors, officers, employees, agents, successors, and permitted assignees harmless from and against any and all claims, damages, losses, suits, actions, demands, proceedings, expenses, and/or liabilities (including but not limited to reasonable attorneys’ fees incurred and/or those necessary to successfully establish the right to indemnification) filed/incurred by any third party against the CITT Fund arising out of a breach of any warranty, representation, or obligation hereunder.

  7. DISCLAIMER OF WARRANTIES AND LIMITATION OR LIABILITY

  7.1 THIS WEBSITE AND THE CITT TOKENS ARE PROVIDED ON AN “AS IS” BASIS AND WITHOUT ANY WARRANTIES OF ANY KIND, EITHER EXPRESSED OR IMPLIED. YOU ASSUME ALL RESPONSIBILITY AND RISK WITH RESPECT TO YOUR USE OF THE WEBSITE AND BUYING OF ANY AMOUNT OF THE CITT TOKENS AND THEIR USE.
  7.2 YOU HEREBY EXPRESSLY AGREE THAT, TO THE MAXIMUM EXTENT PERMITTED BY THE APPLICABLE LAW, THE WEBSITE OWNER DOES NOT ACCEPT ANY LIABILITY FOR ANY DAMAGE OR LOSS, INCLUDING LOSS OF BUSINESS, REVENUE, OR PROFITS, OR LOSS OF OR DAMAGE TO DATA, EQUIPMENT, OR SOFTWARE (DIRECT, INDIRECT, PUNITIVE, ACTUAL, CONSEQUENTIAL, INCIDENTAL, SPECIAL, EXEMPLARY OR OTHERWISE), RESULTING FROM ANY USE OF, OR INABILITY TO USE, THIS WEBSITE OR THE MATERIAL, INFORMATION, SOFTWARE, FACILITIES, SERVICES OR CONTENT ON THIS WEBSITE, FROM BUYING OF THE CITT TOKENS OR THEIR USE BY THE USER, REGARDLESS OF THE BASIS, UPON WHICH THE LIABILITY IS CLAIMED AND EVEN IF WEBSITE OWNER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS OR DAMAGE.
  7.3 YOU UNDERSTAND AND AGREE THAT THE WEBSITE OWNER SHALL NOT BE HELD LIABLE TO AND SHALL NOT ACCEPT ANY LIABILITY, OBLIGATION OR RESPONSIBILITY WHATSOEVER FOR ANY CHANGE OF THE VALUE OF THE CITT TOKENS OR BTCS. THE WEBSITE OWNER SHALL PROVIDE TO THE USER REFUND POSSIBILITY (PAYOUT LIQUIDITY) FOR PURCHASED CITT TOKENS ONLY IN CASE ETHEREUM ECOSYSTEM IS OUT OF ORDER AND NOT FUNCTIONING. THE USER UNDERSTANDS AND EXPRESSLY AGREES THAT THE WEBSITE OWNER SHALL NOT GUARANTY IN ANY WAY THAT THE CITT TOKENS MIGHT BE SOLD OR TRANSFERRED DURING OR AFTER THE TOKEN LAUNCH.
  7.4 AT ANY CASE, TOTAL AMOUNT OF OUR AGGREGATE LIABILITY HEREUNDER MAY NOT EXCEED 500 (FIVE HUNDRED) US DOLLARS. IF APPLICABLE LAW DOES NOT ALLOW ALL OR ANY PART OF THE ABOVE LIMITATION OF LIABILITY TO APPLY TO YOU, THE LIMITATIONS WILL APPLY TO YOU ONLY TO THE EXTENT PERMITTED BY APPLICABLE LAW. YOU UNDERSTAND AND AGREE THAT IT IS YOUR OBLIGATION TO ENSURE COMPLIANCE WITH ANY LEGISLATION RELEVANT TO YOUR COUNTRY OF DOMICILE CONCERNING USE OF THIS WEBSITE AND USE AND BUYING OF THE CITT TOKENS, AND THAT THE WEBSITE OWNER SHOULD NOT ACCEPT ANY LIABILITY FOR ANY ILLEGAL OR UNAUTHORIZED USE OF THIS WEBSITE AND USE AND BUYING OF THE CITT TOKENS. YOU AGREE TO BE SOLELY RESPONSIBLE FOR ANY APPLICABLE TAXES IMPOSED ON TOKENS PURCHASED HEREUNDER.
  7.5 THE WEBSITE OWNER DOES NOT WARRANT OR REPRESENT THAT ANY INFORMATION ON THE WEBSITE IS ACCURATE OR RELIABLE OR THAT THE WEBSITE WILL BE FREE OF ERRORS OR VIRUSES, THAT DEFECTS WILL BE CORRECTED, OR THAT THE SERVICE OR THE SERVER THAT MAKES IT AVAILABLE IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. WEBSITE OWNER SHALL NOT BE LIABLE FOR UNINTERPRETED AVAILABILITY OF THE WEBSITE AT ALL TIMES, IN ALL COUNTRIES AND/OR ALL GEOGRAPHIC LOCATIONS, OR AT ANY GIVEN TIME.

  8. INTELLECTUAL PROPERTY RIGHTS

  8.1 The Website Owner has valid, unrestricted and exclusive ownership of rights to use the patents, trademarks, trademark registrations, trade names, copyrights, know-how, technology and other intellectual property necessary to the conduct of selling of the CITT Tokens and his activities generally.
  8.2 In no way shall this Agreement entitle the User for any intellectual property of the Website Owner, including the intellectual property rights for the Website and all text, graphics, user interface, visual interface, photographs, trademarks, logos, artwork, and computer code, design, structure, selection, coordination, expression and other content connected to the Website. Arrangement of such content is owned by the CITT Fund and is protected by the Intellectual Property Rights and fair competition laws.
  8.3 There are no implied licenses under the Agreement, and any rights not expressly granted to the User hereunder are reserved by the CITT Fund.

  9. JURISDICTION AND DISPUTE RESOLUTION

  9.1 All questions concerning the construction, validity, enforcement and interpretation of this Agreement shall be governed by and construed and enforced in accordance with the laws of Singapore.
  9.2 To resolve any dispute, controversy or claim between them arising out of or relating to this Agreement, or the breach thereof, the Parties agree first to negotiate in good faith for a period of not less than sixty (60) days following written notification of such controversy or claim to the other Party.
  9.3 If the negotiations do not resolve the dispute, controversy or claim to the reasonable satisfaction of all Parties during such period, then the Parties irrevocably and unconditionally submit to the exclusive jurisdiction of Singapore courts under the applicable law, as set out in clause 9.1. hereof.

  10. MISCELLANEOUS

  10.1 Entire Agreement. This Agreement is intended to fully reflect the terms of the original agreement between the Parties. No provision of the Agreement shall be considered waived unless such waiver is in writing and signed by the Party that benefits from the enforcement of such provision. No waiver of any provision in the Agreement, however, will be deemed a waiver of a subsequent breach of such provision or a waiver of a similar provision. In addition, a waiver of any breach or a failure to enforce any term or condition of the Agreement will not in any way affect, limit, or waive a Party’s rights hereunder at any time to enforce strict compliance thereafter with every term and condition hereof.
  10.2 Assignment. The CITT Fund may, at its sole discretion, assign its rights and/or delegate its duties under this Agreement. You may not assign your rights or delegate your duties, and any assignment or delegation without the written consent of the CITT Fund, which the CITT Fund may withhold at its sole discretion, shall be void.
  10.3 Severability. If any term, provision, covenant or restriction of this Agreement is held by a court of competent jurisdiction to be invalid, illegal, void or unenforceable, the remainder of the terms, provisions, covenants and restrictions set forth herein shall remain in full force and effect and shall in no way be affected, impaired or invalidated, and the Parties hereto shall use their commercially reasonable efforts to find and employ an alternative means to achieve the same or substantially the same result as that contemplated by such term, provision, covenant or restriction. It is hereby stipulated and declared to be the intention of the Parties that they would have executed the remaining terms, provisions, covenants and restrictions without including any of such that may be hereafter declared invalid, illegal, void or unenforceable.
  10.4 The User may send any questions regarding the use of the Website of the CITT Tokens or regarding this Agreement via e-mail at support@citt.fund.')?>
  </p>
</div>
