<?php

use app\modules\profile\components\AuthKeysManager;
use yii\authclient\widgets\AuthChoice;
use lowbase\user\models\Country;
use app\modules\profile\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use kartik\widgets\DatePicker;
//use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Мой профиль');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="content">
    <?php $form = ActiveForm::begin([
        'id' => 'form-profile',
        'options' => [
          'enctype'=>'multipart/form-data'
        ],
    ]); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 col-md-push-7 col-lg-4 col-lg-push-8">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <img class="img" src="<?=isset($user->image)?'/'.$user->image:'/cryptofund/images/gravater.jpg'?>" />
                    </div>

                    <div class="content">
                        <h6 class="category text-gray"><?php echo Yii::t('app', 'Участник проекта');?></h6>
                        <h4 class="card-title"><?php echo $user->login;?></h4>
                        <div class="card-content">
                            <?= $form->field($user, 'photo', ['options' => [
                              'class' => 'btn btn-primary btn-file'], 'template' => '<i class="material-icons">attach_file</i> <span id="profileform-filename">' . Yii::t('app', 'Выбрать аватар'). '</span>{input}'])->fileInput()
                            ?>
                            <?=$user->image?'<p>'.Html::a(Yii::t('app', '<i class="material-icons">delete</i> Удалить аватар'), ['/profile/office/remove']).'</p>':''?>
                        </div>
                        <?= Html::submitButton('<i class="material-icons">save</i> '.Yii::t('app', 'Загрузить'), [
                              'class' => 'btn btn-primary btn-round',
                              'name' => 'signup-button']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-md-pull-5 col-lg-8 col-lg-pull-4">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Мой профиль</h4>
                        <p class="category">Редактирование информации о себе</p>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($user, 'first_name', [
                                    'template' => '
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . $user->getAttributeLabel('first_name') . '</label>
                                        {input}
                                    </div>
                                    '
                                ])->textInput([
                                    'maxlength' => true,
                                    'placeholder' => '',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($user, 'last_name', [
                                    'template' => '
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . $user->getAttributeLabel('last_name') . '</label>
                                        {input}
                                    </div>
                                    '
                                ])->textInput([
                                    'maxlength' => true,
                                    'placeholder' => '',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($user, 'phone', [
                                    'template' => '
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . $user->getAttributeLabel('phone') . '</label>
                                        {input}
                                    </div>
                                    '
                                ])->textInput([
                                    'maxlength' => true,
                                    'placeholder' => '',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($user, 'skype', [
                                    'template' => '
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . $user->getAttributeLabel('skype') . '</label>
                                        {input}
                                    </div>
                                    '
                                ])->textInput([
                                    'maxlength' => true,
                                    'placeholder' => '',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($user, 'birthday', [
                                    'template' => '
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . $user->getAttributeLabel('birthday') . '</label>
                                        {input}
                                    </div>
                                    '
                                ])->textInput([
                                    'maxlength' => true,
                                    'placeholder' => '',
                                    'value' => date("d.m.Y", strtotime($user->birthday)),
                                    'class' => 'form-control datepicker',
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Пол</label>
                                    <?php
                                        $items=[null => Yii::t('app', 'Не указан')] + User::getSexArray();
                                        $param=[];
                                        echo $form->field($user, 'sex', ['template'=>'
                                            {input}
                                        '])->dropDownList($items, $param, [
                                            'class' => 'form-control valid'
                                        ]);
                                    ?>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary pull-right"><i class="material-icons">save</i> Сохранить</button>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-lg-8">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Сменить пароль</h4>
                        <p class="category">Редактирование пароля для данного аккаунта</p>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($user, 'old_password', [
                                    'template' => '
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . $user->getAttributeLabel('old_password') . '</label>
                                        {input}
                                    </div>
                                    '
                                ])->passwordInput([
                                    'maxlength' => true,
                                    'placeholder' => '',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($user, 'password', [
                                    'template' => '
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . $user->getAttributeLabel('password') . '</label>
                                        {input}
                                    </div>
                                    '
                                ])->passwordInput([
                                    'maxlength' => true,
                                    'placeholder' => '',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>
                      
                        <?= Html::submitButton('<i class="material-icons">save</i> '.Yii::t('app', 'Сохранить'), [
                          'class' => 'btn btn-primary pull-right',
                          'value' => 1,
                          'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-lg-8">
                <div class="card">
                    <div class="card-header" data-background-color="red">
                        <h4 class="title">Сменить E-mail</h4>
                        <p class="category">Редактирование E-mail адреса</p>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($user, 'email', [
                                    'template' => '
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . $user->getAttributeLabel('email') . '</label>
                                        {input}
                                    </div>
                                    '
                                ])->textInput([
                                    'maxlength' => true,
                                    'disabled' => true,
                                    'placeholder' => '',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($user, 'email_confirm_token', [
                                    'template' => '
                                    <div class="form-group label-floating">
                                        <label class="control-label">' . $user->getAttributeLabel('email_confirm_token') . '</label>
                                        {input}
                                    </div>',
                                    'options' => [
                                        'style' => $oldUser->email_confirm_token == '' ? 'display:none;' : 'display:block;'
                                    ]
                                ])->textInput([
                                    'maxlength' => true,
                                    'value' => '',
                                    'placeholder' => '',
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>
                      
                        <?php if ($oldUser->email_confirm_token == ''):?>
                          <?= Html::submitButton('<i class="material-icons">clear</i> '.Yii::t('app', 'Отвязать'), [
                              'class' => 'btn btn-danger pull-left',
                              'value' => 1,
                              'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                        <?php else:?>
                          <?= Html::submitButton('<i class="material-icons">clear</i> '.Yii::t('app', 'Отвязать'), [
                              'class' => 'btn btn-danger pull-left',
                              'value' => 1,
                              'name' =>  (new \ReflectionClass($user))->getShortName() . '[confirm_token_submit]']) ?>

                          <?= Html::submitButton('<i class="material-icons">autorenew</i> '.Yii::t('app', 'Отправить код повторно'), [
                              'class' => 'btn btn-warning pull-left',
                              'value' => 1,
                              'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                        <?php endif;?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end();?>
</div>

<style>
    #profileform-photo {
        position: absolute;
        top: 0;
        right: 0;
        width: 100%;
        height: 100%;
        margin: 0;
        font-size: 23px;
        cursor: pointer;
        filter: alpha(opacity=0);
        opacity: 0;
        direction: ltr;
    }
</style>

<?php $this->registerJs("
$('.datepicker').datetimepicker({
    format: 'DD.MM.YYYY',
    locale: 'ru',
    icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove',
        inline: true
    }
});
$('#profileform-photo').change(function() {
    var filename = $('#profileform-photo').val().split('\\\').pop();
    if (filename == '')
        filename = 'Выбрать аватар'
    if (filename.length > 19) {
        filename = filename.substring(0, 8) + '..' + filename.substring(filename.length - 8, filename.length)
    }
    $('#profileform-filename').text(filename);
});
", View::POS_END);?>

<?php if(Yii::$app->session->getFlash('success')): ?>

<?php $this->registerJs("
$.notify({
    icon: 'feedback',
    message: '" . Yii::$app->session->getFlash('success') . "'

},{
    type: 'info',
    timer: 4000,
    placement: {
        from: 'bottom',
        align: 'left'
    }
});
", View::POS_END);?>

<?php endif;?>