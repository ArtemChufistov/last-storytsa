<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Рекламные материалы');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<div class="content">
    <?php $form = ActiveForm::begin([
        'id' => 'form-profile',
        'options' => [
          'enctype'=>'multipart/form-data'
        ],
    ]); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 485x60');?></h4>
                        </div>
                        <div class="card-content">
                            <img src="<?php echo Url::home(true);?>privat/banner3.jpeg">
                            <p><textarea class="form-control" id="clipboardCode48560" readonly><a href="<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>privat/banner3.jpeg"></a></textarea></p>
                            <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode48560']);?>
                        </div>
                    </div>
                    
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 250x250');?></h4>
                        </div>
                        <div class="card-content">
                            <img src="<?php echo Url::home(true);?>privat/banner4.jpeg">
                            <p><textarea class="form-control" id="clipboardCode250250" readonly><a href="<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>privat/banner4.jpeg"></a></textarea></p>
                            <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode250250']);?>
                        </div>
                    </div>
                    
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 125x125');?></h4>
                        </div>
                        <div class="card-content">
                            <img src="<?php echo Url::home(true);?>privat/banner1.jpeg">
                            <p><textarea class="form-control" id="clipboardCode125125" readonly><a href="<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>privat/banner1.jpeg"></a></textarea></p>
                            <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode125125']);?>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 240x400');?></h4>
                                </div>
                                <div class="card-content">
                                    <img src="<?php echo Url::home(true);?>privat/banner2.jpeg">
                                    <p><textarea class="form-control" id="clipboardCode240400" readonly><a href="<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>privat/banner2.jpeg"></a></textarea></p>
                                    <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode240400']);?>
                                </div>
                            </div>
                            
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x300');?></h4>
                                </div>
                                <div class="card-content">
                                    <img src="<?php echo Url::home(true);?>privat/banner5.jpeg">
                                    <p><textarea class="form-control" id="clipboardCode300300" readonly><a href="<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>privat/banner5.jpeg"></a></textarea></p>
                                    <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode300300']);?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 728x90');?></h4>
                                </div>
                                <div class="card-content">
                                    <img src="<?php echo Url::home(true);?>privat/banner6.jpeg">
                                    <p><textarea class="form-control" id="clipboardCode72890" readonly><a href="<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>privat/banner6.jpeg"></a></textarea></p>
                                    <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode72890']);?>
                                </div>
                            </div>
                            
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title"><?php echo Yii::t('app', 'QR код с зашитой REF ссылкой на главную страницу');?></h4>
                                </div>
                                <div class="card-content">
                                    <h4>Ссылка на главную страницу</h4>
                                    <img src="<?php echo Url::home(true);?>qrcodemain/<?php echo $user->login;?>">
                                    <textarea class = "form-control" id = "qrcodemain"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>qrcodemain/<?php echo $user->login;?>"></a></textarea>
                                    <?php echo Html::input('button', '', Yii::t('app', 'Копировать ссылку'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#qrcodemain']);?>
                                </div>
                            </div>
                            
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title"><?php echo Yii::t('app', 'QR коды с зашитой REF ссылкой на регистрацию');?></h4>
                                </div>
                                <div class="card-content">
                                    <h4>Ссылка на регистрацию</h4>
                                    <img src="<?php echo Url::home(true);?>qrcodereg/<?php echo $user->login;?>">
                                    <textarea class = "form-control" id = "qrcodereg"><a href = "<?php echo Url::home(true);?>signup?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>qrcodereg/<?php echo $user->login;?>"></a></textarea>
                                    <?php echo Html::input('button', '', Yii::t('app', 'Копировать ссылку'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#qrcodereg']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php ActiveForm::end();?>
</div>

<?php $this->registerJs('new Clipboard(".btn-clipboard");');?>