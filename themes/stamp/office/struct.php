<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Структура');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title"><?php echo Yii::t('app', 'Поиск по дереву');?></h4>
                        <p class="category"><?php echo Yii::t('app', 'Введите имя участника для поиска по дереву приглашенных');?></p>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                                <form> 
                                    <div class="form-group label-floating">
                                      <label class="control-label"><?php echo Yii::t('app', 'Имя участника');?></label>
                                      <input type="input" class="form-control" id="input-search" value="">
                                    </div>
                                    <button type="button" class="btn btn-primary" id="btn-search">Поиск</button>
                                    <button type="button" class="btn btn-default" id="btn-clear-search">Очистить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_ref', ['user' => $user]);?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title"><?php echo Yii::t('app', 'Приглашенные участники');?></h4>
                        <p class="category"><?php echo Yii::t('app', 'Пригласите своих друзей и знакомых, пусть они станут инвесторами, как и Вы');?></p>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                                <h5><?php echo Yii::t('app', 'Предусмотренная реферальная программа позволяет получать 10% единоразово от суммы приобретаемого пакета лично  приглашенного Инвестора зарегистрированного по Вашей реферальной ссылке.');?></h5>
                                <div id="treeview-searchable" class=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs("
  var defaultData = " . json_encode($userTree) . "; 
  $(function() {

    var csrfToken = '" . Yii::$app->request->getCsrfToken() . "';

    function showUser(login)
    {
      if ($('.userList').find('[login=\"' + login + '\"]').html() != undefined){
        $('.userList').find('[login=\"' + login + '\"]').show();
        return;
      }

      $.post('/profile/office/showuser', { login: login, _csrf: csrfToken }, function(data){
        $('.userList').append(data);
      });
    }

    jQuery(document).on('click', '.list-group-item', function(){
      showUser(jQuery(this).find('.login').html());
    })

    jQuery('#search-output').on('click', 'td', function(){
      showUser(jQuery(this).find('span').html());
    })

    jQuery('.userList').on('click', '.close', function(){
      $(this).parents('.userInfoPartial').remove();
    })

    var searchableTree = $('#treeview-searchable').treeview({
      data: defaultData,
    });

    var search = function(e) {
      var pattern = $('#input-search').val();

      var results = searchableTree.treeview('search', [ pattern ]);

      var output = '<tr><td>' + results.length + ' " . Yii::t('app', 'совпадений найдено') . "</td></tr>';

      $.each(results, function (index, result) {
        output += '<tr><td>' + result.text + '</td></tr>';
      });

      $('#search-output').html(output);
      $('.resultBox').show();
    }

    $('#btn-search').on('click', search);
    $('#input-search').on('keyup', search);

    $('#btn-clear-search').on('click', function (e) {
      searchableTree.treeview('clearSearch');
      $('#input-search').val('');
      $('#search-output').html('');
    });
  });
", View::POS_END);?>