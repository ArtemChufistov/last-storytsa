<?php
use yii\helpers\Url;
?>
<br>
<div class="row">
    <div class="col-sm-2 col-sm-push-1 step-card<?php if (in_array($active, [1, 2, 3, 4, 5])): ?> active<?php endif;?>">
        <span class="number">1</span>
        <center>
            <h5><?php echo Yii::t('app', 'Количество токенов'); ?></h5>
            <p><?php echo Yii::t('app', 'Сколько токенов вы хотите обменять'); ?></p>
        </center>
    </div>
    <div class="col-sm-2 col-sm-push-1 step-card<?php if (in_array($active, [2, 3, 4, 5])): ?> active<?php endif;?>">
        <span class="number">2</span>
        <center>
            <h5><?php echo Yii::t('app', 'Выбор валюты'); ?></h5>
            <p><?php echo Yii::t('app', 'В какой валюте'); ?></p>
        </center>
    </div>
    <div class="col-sm-2 col-sm-push-1 step-card<?php if (in_array($active, [3, 4, 5])): ?> active<?php endif;?>">
        <span class="number">3</span>
        <center>
            <h5><?php echo Yii::t('app', 'Выбор платёжной системы'); ?></h5>
            <p><?php echo Yii::t('app', 'На какую платёжную систему'); ?></p>
        </center>
    </div>
    <div class="col-sm-2 col-sm-push-1 step-card<?php if (in_array($active, [4, 5])): ?> active<?php endif;?>">
        <span class="number">4</span>
        <center>
            <h5><?php echo Yii::t('app', 'Реквизиты'); ?></h5>
            <p><?php echo Yii::t('app', 'Укажите ваши платёжные реквизиты'); ?></p>
        </center>
    </div>
    <div class="col-sm-2 col-sm-push-1 step-card<?php if (in_array($active, [5])): ?> active<?php endif;?>">
        <span class="number">5</span>
        <center>
            <h5><?php echo Yii::t('app', 'Получение CITT'); ?></h5>
            <p><?php echo Yii::t('app', 'Сумма к получению'); ?></p>
        </center>
    </div>
</div>