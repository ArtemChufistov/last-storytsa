<?php
use yii\helpers\Url;
?>
<br>
<div class="row">
    <div class="col-sm-3 step-card<?php if (in_array($active, [1, 2, 3, 4])): ?> active<?php endif;?>">
        <span class="number">1</span>
        <center>
            <h5><?php echo Yii::t('app', 'Количество токенов'); ?></h5>
            <p><?php echo Yii::t('app', 'Сколько токенов вы хотите приобрести'); ?></p>
        </center>
    </div>
    <div class="col-sm-3 step-card<?php if (in_array($active, [2, 3, 4])): ?> active<?php endif;?>">
        <span class="number">2</span>
        <center>
            <h5><?php echo Yii::t('app', 'Выбор валюты'); ?></h5>
            <p><?php echo Yii::t('app', 'Какой валютой вы хотите произвести расчет'); ?></p>
        </center>
    </div>
    <div class="col-sm-3 step-card<?php if (in_array($active, [3, 4])): ?> active<?php endif;?>">
        <span class="number">3</span>
        <center>
            <h5><?php echo Yii::t('app', 'Выбор платёжной системы'); ?></h5>
            <p><?php echo Yii::t('app', 'Какую платёжную системы вы используете'); ?></p>
        </center>
    </div>
    <div class="col-sm-3 step-card<?php if (in_array($active, [4])): ?> active<?php endif;?>">
        <span class="number">4</span>
        <center>
            <h5><?php echo Yii::t('app', 'Оплата'); ?></h5>
            <p><?php echo Yii::t('app', 'Проведение платежа и начисление CITT токенов'); ?></p>
        </center>
    </div>
</div>