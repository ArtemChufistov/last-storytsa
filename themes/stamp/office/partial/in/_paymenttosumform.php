<?php
use app\modules\finance\models\Currency;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Получить CITT - Ввод суммы');
$this->params['breadcrumbs'][] = $this->title;

$currencyVoucher = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();

$defaultBuySitt = 2;
?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header" data-background-color="purple">
            <h4 class="title"><?php echo Yii::t('app', 'Пополнение'); ?></h4>
            <p class="category"><?php echo Yii::t('app', 'Внесение средств на личный счет'); ?></p>
        </div>
        <div class="card-content">
          <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_tabs', [
	'active' => 1,
]); ?>
          <h3><?php echo Yii::t('app', 'Количество токенов'); ?></h3>
          <?php $form = ActiveForm::begin([
              'method' => 'get',
              'options' => [
                'enctype'=>'multipart/form-data'
              ],
          ]); ?>
          <div class="row">
              <div class="col-md-4">
                  <?= $form->field($paymentForm, 'to_sum', [
                      'template' => '
                      <div class="form-group label-floating">
                          <label class="control-label">' . Yii::t('app', 'Количество CITT') . '</label>
                          {input}
                          {error}
                      </div>
                      '
                  ])->textInput([
                      'maxlength' => true,
                      'placeholder' => '',
                      'class' => 'form-control summBuySitt',
                      'value' => $defaultBuySitt
                  ]) ?>
              </div>
              <div class="col-md-4">
                  <div class="form-group field-user-phone">
                      <div class="form-group label-floating">
                          <label class="control-label"><?php echo Yii::t('app', 'Текущий курс CITT'); ?></label>
                          <input type="text" id="user-phone" class="form-control courseInUsd" value="<?=$currencyVoucher->course_to_usd?>" maxlength="100" placeholder="" aria-invalid="false" disabled>
                          <span class="material-input"></span>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group field-user-phone">
                      <div class="form-group label-floating">
                          <label class="control-label"><?php echo Yii::t('app', 'Итого в USD'); ?></label>
                          <input type="text" id="user-phone" class="form-control resultInUsd" value="<?=$defaultBuySitt * $currencyVoucher->course_to_usd?>" maxlength="100" placeholder="" aria-invalid="false" disabled>
                          <span class="material-input"></span>
                      </div>
                  </div>
              </div>
          </div>
          <?=Html::submitButton(Yii::t('app', 'Далее'), ['class' => 'btn btn-primary btn-lg pull-right'])?>
          <?php ActiveForm::end();?>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
jQuery(function () {

  $('.summBuySitt').keyup(function(){
    reculcResUsd();
  })

  $('.bootstrap-touchspin-up').on('click', function(){
    reculcResUsd();
  })

  $('.bootstrap-touchspin-down').on('click', function(){
    reculcResUsd();
  })

  function reculcResUsd()
  {
    var sum = parseFloat($('.courseInUsd').attr('value') * $('.summBuySitt').val());
    $('.resultInUsd').html(sum);
  }
});
", View::POS_END);?>