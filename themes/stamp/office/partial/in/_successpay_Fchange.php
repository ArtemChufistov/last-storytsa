<?php
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;
use app\modules\mainpage\models\Preference;

$prefFchangeName = Preference::find()->where(['key' => Preference::KEY_PREF_FCHANGE_NAME])->one();
$prefFchangeSuccessUrl = Preference::find()->where(['key' => Preference::KEY_PREF_FCHANGE_SUCCESS_URL])->one();
$prefFchangeErrorUrl = Preference::find()->where(['key' => Preference::KEY_PREF_FCHANGE_ERROR_URL])->one();

$fchangeInfo = json_decode($paymentForm->additional_info, true);

$paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
$currencyCourse = CurrencyCourse::find()
	->where(['from_pay_system_id' => $paymentForm->from_pay_system_id])
	->andWhere(['to_pay_system_id' => $paySystem->id])
	->andWhere(['from_currency' => $paymentForm->from_currency_id])
	->one();

?>
<div class="row">
    <div class="col-lg-3">
        <div class="dummy-logo">
            <strong style = "font-size: 30px;">CITT</strong>
            </br>
            </br>
            <p style = "font-size: 14px;">DIGITAL ASSETS MANAGMENT</p>
        </div>
    </div>
    <div class="col-lg-9 float-right text-right">
    	<form name="send_form" action="http://f-change.biz/merchant_pay" method="post">
	        <h4 class="invoice-title"><?php echo Yii::t('app', 'Оплата'); ?></h4>
	        <b>№ <?php echo $paymentForm->hash; ?></b>

	        <div class="divider"></div>
	        <div class="invoice-date mrg20B"><?php echo date('H:i d-m-Y', strtotime($paymentForm->date_add)); ?></div>

	        <input type="hidden" name="merchant_name" value="<?php echo $prefFchangeName->value; ?>" />
			<input type="hidden" name="merchant_title" value="citt.fund" />
			<input type="hidden" name="payed_paysys" value="<?php echo $fchangeInfo['send_paysys_identificator']; ?>" />
			<input type="hidden" name="amount" value="<?php echo $paymentForm->from_sum + $paymentForm->comission_from_sum; ?>" />
			<input type="hidden" name="payment_info" value="<?php echo Yii::t('app', 'Покупка CITT участник: {login}', ['login' => $user->login]) ?>" />
			<input type="hidden" name="payment_num" value="<?php echo $paymentForm->hash; ?>" />
			<input type="hidden" name="sucess_url" value="<?php echo $prefFchangeSuccessUrl->value; ?>" />
			<input type="hidden" name="error_url" value="<?php echo $prefFchangeErrorUrl->value; ?>" />

	        <button class="btn btn-alt btn-hover btn-info" type="submit">
	            <span><?php echo Yii::t('app', 'Перейти к оплате'); ?></span>
	            <i class="glyph-icon icon-play"></i>
	        </button>
	        <button class="btn btn-alt btn-hover btn-danger">
	            <span><?php echo Yii::t('app', 'Отменить'); ?></span>
	            <i class="glyph-icon icon-trash"></i>
	        </button>
		</form>
    </div>
</div>
<div class="row">
	<div class="col-lg-4">
	</div>
	<div class="col-lg-4">
		<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Информация'); ?>:</h2>
		<ul class="reset-ul">
			<li>
				<b><?php echo Yii::t('app', 'Количество CITT'); ?>:</b> <strong style = "font-size: 22px;"> <?php echo $paymentForm->to_sum; ?></strong>
			</li>
			<li>
				<b><?php echo Yii::t('app', 'Платёжная система'); ?>:</b>
				<strong style = "font-size: 22px;"> <?php echo $fchangeInfo['send_paysys_title']; ?> </strong>
			</li>
			<li>
				<b><?php echo Yii::t('app', 'Комисия системы'); ?>:</b>
				<strong style = "font-size: 22px;"> <?php echo $currencyCourse->comission; ?>%
				<?php echo Yii::t('app', 'Включены в сумму'); ?></strong>
			</li>
		</ul>
	</div>
	<div class="col-lg-4">
	</div>
</div>
