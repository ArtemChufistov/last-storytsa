<?php
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Получить CITT - Выбор валюты');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$btnColors = ['success', 'info', 'blue-alt', 'primary', 'purple', 'azure'];

?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header" data-background-color="purple">
            <h4 class="title">Пополнение</h4>
            <p class="category">Внесение средств на личный счет</p>
        </div>
        <div class="card-content">
          <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_tabs', [
	'active' => 2,
]); ?>
          <h3><?php echo Yii::t('app', 'Выберите валюту'); ?></h3>
          <?php $form = ActiveForm::begin([
              'method' => 'get',
              'options' => [
                'enctype'=>'multipart/form-data'
              ],
          ]); ?>
          <div class="row">
              <div class="col-sm-6 col-sm-push-3">
                  <select id="profileform-sex" class="form-control" name="<?=StringHelper::basename(get_class($paymentForm))?>[to_currency_id]" aria-invalid="false">
                      <?php foreach ($currencyArray as $currency): ?>
                          <option value="<?=$currency->id?>"><?=$currency->key?></option>,
                      <?php endforeach;?>
                  </select>
                  

                  <?=$form->errorSummary($paymentForm);?>
              </div>
          </div>
          <?=Html::submitButton(Yii::t('app', 'Далее'), ['class' => 'btn btn-primary btn-lg pull-right'])?>
          <?php ActiveForm::end();?>
        </div>
      </div>
    </div>
  </div>