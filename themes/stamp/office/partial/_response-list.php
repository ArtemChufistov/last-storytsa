
<?php
use yii\web\View;
?>

<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i><?php echo Yii::t('app', 'Ваши отзывы');?></h3>
  </div>
  <div class="box-body no-padding">
    <?php if (empty($responseArray)):?>
      <div class="col-md-12">
        <p style = "margin-top: 15px;margin-bottom: 15px; font-size: 16px;"><?= Yii::t('app', 'У вас нет отзывов, пожалуйста добавьте свой отзыв о проекте');?></p>
      </div>
    <?php else:?>
      <table class="table table-striped">
        <tr>
          <th><?php echo Yii::t('app', 'Имя');?></th>
          <th><?php echo Yii::t('app', 'Дата добавления');?></th>
          <th><?php echo Yii::t('app', 'Текст');?></th>
          <th><?php echo Yii::t('app', 'Действие');?></th>
        </tr>

        <?php foreach($responseArray as $response):?>
          <tr>
            <td><a href = "" data-toggle="modal" data-target=".modalResp<?php echo $response->id;?>"><?php echo $response->name;?></a></td>
            <td><a href = "" data-toggle="modal" data-target=".modalResp<?php echo $response->id;?>"><?php echo date('d-m-Y',strtotime($response->date));?></a></td>
            <td><a href = "" data-toggle="modal" data-target=".modalResp<?php echo $response->id;?>"><?php echo substr(strip_tags($response->text), 0, 30);?></a></td>
            <td style="width: 100px;"><a class = "deleteRespone" href = "" id = "<?php echo $response->id;?>"><?php echo Yii::t('app', 'Удалить');?></a></td>
          </tr>

          <div class="modalResp<?php echo $response->id;?> modal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Ваш отзыв');?></h4>
                </div>
                <div class="modal-body">

                  <div class="form-group">
                    <label class="control-label" for="response-name"><?php echo Yii::t('app', 'Имя');?></label>
                    <input class="form-control" value = "<?php echo $response->name;?>" readonly = "true">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="response-name"><?php echo Yii::t('app', 'Текст');?></label>
                    <pre class="form-control" style = "height: 80px;"><?php echo $response->text;?></pre>
                  </div>
                  <div class="form-group">
                    <!--<label class="control-label" for="response-name"><?php echo Yii::t('app', 'Фото');?></label>-->
                    <?php for($i = 1; $i <= 3; $i++){?>
                      <?php $imgName = 'img' . $i;?>
                        <?php if (!empty($response->$imgName)):?>
                          <pre class="form-control" style = "height: 120px; text-align: center;">
                            <img src='/<?php echo $response->$imgName; ?>' class='thumbnail'>
                          </pre>
                        <?php endif;?>
                    <?php };?>
                  </div>
                  <div class="form-group">
                    <!--<label class="control-label" for="response-name"><?php echo Yii::t('app', 'Видео');?></label>-->
                    <?php for($i = 1; $i <= 3; $i++){?>
                      <?php $videoName = 'video' . $i;?>
                        <?php if (!empty($response->$videoName)):?>
                          <pre class="form-control" style = "height: 120px; text-align: center;">
                            <?php echo $response->$videoName; ?>
                          </pre>
                        <?php endif;?>
                    <?php };?>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>

        <?php endforeach;?>

      </table>
    <?php endif;?>
  </div>
</div>

<?php $this->registerJs("
  jQuery(function () {
    jQuery('.deleteRespone').on('click', function(){
      $.get('/profile/office/deleteresponse/' + $(this).attr('id'), function() {
        console.log(1111);
        return false;
      });
    })
    jQuery('.textarea').wysihtml5();
  });
", View::POS_END);?>