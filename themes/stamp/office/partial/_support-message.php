<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>
<div class="content-box mrg15B">
  <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Ваш тикет');?>
    <div class="font-size-11 float-right">
      <a href="#" title=""><i class="glyph-icon opacity-hover icon-bookmark-o"></i></a>
    </div>
  </h3>
  <div class="content-box-wrapper text-center clearfix">
    <div class="form-group">
      <label class="control-label" ><?php echo Yii::t('app', 'Отдел');?></label>
      <pre class="form-control" ><?php echo $currentTicket->getDepartment()->one()->name;?></pre>
    </div>
    <div class="form-group">
      <label class="control-label" ><?php echo Yii::t('app', 'Текст');?></label>
      <pre class="form-control" style = "height: 80px;"><?php echo $currentTicket->text;?></pre>
    </div>
  </div>
</div>

<div class="content-box mrg15B">
  <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Чат с поддержкой');?>
    <div class="font-size-11 float-right">
      <a href="#" title=""><i class="glyph-icon opacity-hover icon-times"></i></a>
    </div>
  </h3>
  <div class="content-box-wrapper text-center clearfix chat" id="chat-box">
    <?php foreach($ticketMessages as $ticketMessageItem): ?>
      <div class="item">
        <img src="/<?php echo $ticketMessageItem->getUser()->one()->image;?>" alt="user image" class="offline">

        <p class="message">
          <a href="#" class="name">
            <?php echo $ticketMessageItem->getUser()->one()->login;?>
          </a>
          <?php echo $ticketMessageItem->text;?>
        </p>
      </div>
    <?php endforeach;?>
  </div>
  <div class="box-footer">
    <?php $form = ActiveForm::begin([
      'options' => [
          'class'=>'input-group support-message',
      ],
    ]); ?>

        <?= $form->field($ticketMessage, 'text')->textInput([
            'maxlength' => true,
            'placeholder' => $ticketMessage->getAttributeLabel('text')
        ]) ?>

      <div class="input-group-btn">
          <?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-success']) ?>
      </div>
      <?php ActiveForm::end();?>

  </div>
</div>
