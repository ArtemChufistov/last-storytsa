<?php
use app\modules\profile\models\AuthAssignment;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
?>

<!--$.fancybox.open('<div class="message"><h2>Hello!</h2><p>You are awesome!</p></div>');-->

<div class="modal modalNotVerifiedUser fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <?php $form = ActiveForm::begin([
          'id' => 'form-profile',
          'options' => [
              'class'=>'form',
              'enctype'=>'multipart/form-data'
          ],

          ]); ?>

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
        <p><?php echo Yii::t('app', 'Вам необходимо подтвердить свой E-mail. После подтверждения Вам будет доступна полная версия кабинета');?></p>

            <?= $form->field($changeEmailForm, 'changing_email')->textInput([
                'maxlength' => true,
                'value' => $user->changing_email == '' ?  $user->email : $user->changing_email,
                'placeholder' => $changeEmailForm->getAttributeLabel('changing_email')
            ]) ?>
            
      </div>
      <div class="modal-footer">

        <?= Html::submitButton('<i class="glyphicon glyphicon-send"></i> '.Yii::t('app', 'Отправить код подтверждения'), [
            'class' => 'btn btn-info',
            'value' => 1,
            'name' => (new \ReflectionClass($changeEmailForm))->getShortName() . '[send_mail_submit]']) ?>

        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
      <?php ActiveForm::end();?>
    </div>
  </div>
</div>

<?php $this->registerJs("
function showModalNotVerrified()
{
  jQuery('.modalNotVerifiedUser').modal('show');
}

jQuery('.treeview').on('click', 'a', function(){
  if ($(this).attr('href') != '/logout'){
    showModalNotVerrified();
    return false;
  }
})

jQuery(function() {
  showModalNotVerrified();
});
", View::POS_END);?>