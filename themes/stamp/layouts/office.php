<?php
use app\assets\stamp\BackAppAssetBottom;
use app\assets\stamp\BackAppAssetTop;
use app\modules\menu\widgets\MenuWidget;
use app\modules\profile\widgets\ChangeEmailWidget;
use yii\helpers\Html;

BackAppAssetTop::register($this);
BackAppAssetBottom::register($this);
?>

<?php $this->beginPage()?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="/stamp/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="/stamp/img/favicon.png" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<title><?=Html::encode($this->title)?></title>

    <?=Html::csrfMetaTags()?>
    <?php $this->head()?>
    
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_head_meta'); ?>
</head>

<body>
    <?php $this->beginBody()?>
	<div class="wrapper">
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_left-nav'); ?>
        
	    <div class="main-panel">
			<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_office_head'); ?>

			<?php echo $content; ?>

			<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_office_footer'); ?>
		</div>
	</div>
    <?=ChangeEmailWidget::widget(['user' => $this->params['user']->identity])?>
    <?php $this->endBody()?>
</body>
</html>
<?php $this->endPage()?>