<?php
use app\modules\menu\widgets\MenuWidget;
use app\modules\news\widgets\LastNewsWidget;
use app\modules\profile\widgets\SubscribeWidget;
?>
<footer class="well5">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInLeft">
      <span class="colorText1"><?= Yii::t('app', 'Cashyx ');?></span> <?= Yii::t('app', '&copy;');?> <span id="copyright-year"></span> <?= Yii::t('app', '|');?>
        <a href="index-5.html"><?= Yii::t('app', 'Privacy Policy');?></a>
        <?= Yii::t('app', 'More Financial Advisor Website Templates at ');?><a rel="nofollow" href="http://www.templatemonster.com/category/financial-advisor-website-templates/" target="_blank"><?= Yii::t('app', 'TemplateMonster.com');?></a>
        <ul class="follow_icon">
          <li>
            <a class="fa fa-instagram" href="#"></a>
          </li>
          <li>
            <a class="fa fa-twitter" href="#"></a>
          </li>
          <li>
            <a class="fa fa-facebook" href="#"></a>
          </li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-12 col-xs-12">
         <h3><?= Yii::t('app', 'Get in touch<br>with us');?></h3>
         <p>
           <?= Yii::t('app', 'Mes cuml dia sed in lacus ut eniascet ipsu ingerto aliiqt es site amet eismod ictor uten ligulate ameti dapibu ticdute numtsen lusto dolor ltissim comes cuml dia sed inertio');?>
         </p>
         <address>
          <dl>
          <dt><?= Yii::t('app', 'Telephone:');?></dt>
            <dd>
              <a href="callto:#"><?= Yii::t('app', '+1 800 603 6035');?></a>
            </dd>
            <dt><?= Yii::t('app', 'E-mail:');?></dt>
            <dd class="mail1">
              <a href="mailto:#"><?= Yii::t('app', 'mail@demolink.org');?></a>
            </dd>
          </dl>
        </address>
      </div>
      <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInRight">
         <address class="offsNo">
         <p>
           <?= Yii::t('app', 'Cashyx<br>
           28 Jackson Blvd Ste 1020<br>
           Chicago, IL 60604-2340');?>
         </p>
        </address>
      </div>
    </div>
  </div>
</footer>