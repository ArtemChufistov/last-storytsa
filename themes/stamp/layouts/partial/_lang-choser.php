<?php
use yii\helpers\Html;
?>
<li><a href="#"><?=$current->name;?></a>
  <!-- RD Navbar Dropdown-->
  <ul class="rd-navbar-dropdown">
    <?php foreach ($langs as $lang): ?>
        <li class="item-lang">
            <?=Html::a($lang->name, '/' . $lang->url . Yii::$app->getRequest()->getLangUrl())?>
        </li>
    <?php endforeach;?>
  </ul>
</li>