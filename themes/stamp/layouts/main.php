<?php
use app\assets\stamp\FrontAppAssetBottom;
use app\assets\stamp\FrontAppAssetTop;
use app\modules\menu\widgets\MenuWidget;
use yii\helpers\Html;

FrontAppAssetTop::register($this);
FrontAppAssetBottom::register($this);

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?=Html::csrfMetaTags()?>
  <title><?=Html::encode($this->title)?></title>
  <?php $this->head()?>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_head_meta'); ?>
  
  <!--[if lt IE 9]> 
  <html class="lt-ie9">
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
      <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
           alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
    </a>
  </div>
  <script src="js/html5shiv.js"></script>
  <![endif]-->
</head>
<body>
<?php $this->beginBody()?>
<div class="page">
  <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']); ?>
  
  <?=$content?>
  
  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_footer'); ?>
</div>
<?php $this->endBody()?>
</body>
</html>
<?php $this->endPage()?>