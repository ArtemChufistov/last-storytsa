<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\web\View;
use app\assets\stamp\ContactAppAsset;

ContactAppAsset::register($this);

$this->title = Yii::t('app', 'Связаться с нами');

$this->params['breadcrumbs']= [ [
    'label' => $this->title,
    'url' => '/contact'
  ],
]

?>

<main>
    <section class="map">
      <div id="google-map" class="map_model"></div>
      <ul class="map_locations" style="z-index: 999999">
        <li data-x="-73.9874068" data-y="40.643180">
          <p><?= Yii::t('app', ' 9870 St Vincent Place, Glasgow, DC 45 Fr 45. <span>800 2345-6789</span>');?></p>
        </li>
      </ul>
    </section>
    <section class="well15">
      <div class="container">
        <address>
          <ul class="list1 row">
            <li class="col-md-4 col-sm-4 col-xs-12">
              <div class="box">
                <div class="aside">
                  <a class="fa4 fa-map-marker" href="#"></a>
                </div>
                <div class="cnt">
                 <p>
                  <?= Yii::t('app', '9870 St Vincent Place,<br>Glasgow, DC 45 Fr 45.');?>
                </p>
              </div>
            </div>
          </li>
          <li class="col-md-4 col-sm-4 col-xs-12">
            <div class="box">
              <div class="aside">
                <a class="fa4 fa-mobile" href="#"></a>
              </div>
              <div class="cnt offs8">
                <a href="callto:#"><?= Yii::t('app', '+1 800 603 6035');?></a>
              </div>
            </div>
          </li>
          <li class="col-md-4 col-sm-4 col-xs-12">
          <div class="box">
            <div class="aside">
              <a class="fa4 fa5 fa-envelope" href="#"></a>
            </div>
            <div class="cnt offs8">
             <a href="mailto:#"><?= Yii::t('app', 'mail@demolink.org');?></a>
           </div>
         </div>
       </li>
       </ul>
     </address>
    </div>
  </section>
<section class="well16 bg2">
  <div class="container">
    <h2>Contact<span> Form</span></h2>
    <form class='mailform advanced' method="post" action="bat/rd-mailform.php">
      <input type="hidden" name="form-type" value="contact"/>
      <fieldset class="row">
        <div class="aside col-md-5 col-sm-12 col-xs-12">
          <label data-add-placeholder="">
            <input type="text"
            name="name"
            placeholder="Name"
            data-constraints="@LettersOnly @NotEmpty"/>
          </label>
          <label data-add-placeholder="">
            <input type="text"
            name="phone"
            placeholder="Phone"
            data-constraints="@Phone"/>
          </label>
          <label data-add-placeholder="">
            <input type="text"
            name="email"
            placeholder="Email"
            data-constraints="@Email @NotEmpty"/>
          </label>
        </div>
        <div class="cnt col-md-7 col-sm-12 col-xs-12">
          <label data-add-placeholder="">
            <textarea name="message" placeholder="comments"
            data-constraints="@NotEmpty"></textarea>
          </label>
        </div>
        <div class="mfControls">
          <button class="" type="submit"><?= Yii::t('app', 'Submit comment');?></button>
        </div>
      </fieldset>
    </form>
  </div>
</section> 
</main>
<script>
function initMap() {
  map = new google.maps.Map(document.getElementById('google-map'), {
    center: {lat: 40.643180, lng: -73.9874068},
    zoom: 15
  });
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDLcni62mBN4yYjY2I4WhbpyoQY5ou3DC4&callback=initMap" async defer></script>