<?php
use app\modules\finance\models\CryptoWallet;

$this->title = Yii::t('app', $model->title);
?>

<main>
  <section>
    <div class="camera_container">
      <div id="camera" class="camera_wrap">
        <div data-src="stamp/images/page-1_slide01.jpg">
          <div class="camera_caption fadeIn">
            <h2><?= Yii::t('app', 'A Wide Range of<br><span>Loan Terms</span>');?></h2>
            <a class="fa fa-file-text-o" href="#"></a>
          </div>
        </div>
        <div data-src="stamp/images/page-1_slide02.jpg">
          <div class="camera_caption fadeIn">
            <h2><?= Yii::t('app', 'A Licensed<br><span>Moneylender</span>');?></h2>
            <a class="fa fa-file-text-o" href="#"></a>
          </div>
        </div>
        <div data-src="stamp/images/page-1_slide03.jpg">
          <div class="camera_caption fadeIn">
            <h2><?= Yii::t('app', 'The Best Private<br><span>Funding Rates</span>');?></h2>
            <a class="fa fa-file-text-o" href="#"></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="well bg1">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInLeft">
        <div class="textBlock1">
          <h3><?= Yii::t('app', 'Business Loans<br>&#38; Finance');?></h3>
          <h4><?= Yii::t('app', 'Vestibulum port mauris.');?></h4>
          <p>
            <?= Yii::t('app', 'Lorem ipsum dolor sit amet, consectetur adipisc elit. Proin ultricies vestibulum velit, bibendum et condimentu metusip dolore.');?> 
          </p>
          <a class="fa fa-file-text-o" href="#"></a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="textBlock1">
          <h3 class="color1"><?= Yii::t('app', 'Home Loans<br>&#38; Mortgages');?></h3>
          <h4><?= Yii::t('app', 'Vestibulum port mauris.');?></h4>
          <p>
            <?= Yii::t('app', 'Lorem ipsum dolor sit amet, consectetur adipisc elit. Proin ultricies vestibulum velit, bibendum et condimentu metusip dolore.');?> 
          </p>
          <a class="fa fa-file-text-o color1" href="#"></a>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInRight">
        <div class="textBlock1">
          <h3 class="color2"><?= Yii::t('app', 'Personal<br>Loans');?></h3>
          <h4><?= Yii::t('app', 'Vestibulum port mauris.');?></h4>
          <p>
            <?= Yii::t('app', 'Lorem ipsum dolor sit amet, consectetur adipisc elit. Proin ultricies vestibulum velit, bibendum et condimentu metusip dolore.');?> 
          </p>
          <a class="fa fa-file-text-o color2" href="#"></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="well1">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12 wow fadeInLeft">
          <h2><?= Yii::t('app', 'Company<span>&#38; Vision</span>');?></h2>
          <div class="box">
            <div class="aside">
              <img src="stamp/images/page-1_img01.png" alt="">
            </div>
            <div class="cnt">
              <h4><?= Yii::t('app', '
                Mes cuml dia sed ineniasi dolore 
                ipsum comnetus consec.
              ');?></h4>
              <p>
                <?= Yii::t('app', 'Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultri cie upien disse comete dolo lectus fgilla itolli cil tua ludin dolor nec met quam accumsan dolo. Dolore condime netus lullam utlacus adipiscing ipsum molestie euismod lore estibu lum vel libero ipsum sit amet sollicitu din ante. Aenean imperdiet aliquet hendrerit.');?>
              </p>
            </div>
          </div>
          <?= Yii::t('app', '<a class="fa1" href="#">More info</a>');?>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInRight">
          <h2><?= Yii::t('app', 'Why<br>Choose Us');?></h2>
          <h4><?= Yii::t('app', '
            Mes cuml dia sed ineniasing dolor.
          ');?></h4>
          <p>
            <?= Yii::t('app', 'Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultricie upien disse comete dolo lectus fgilla itollicil tua ludin dolor nec met quam accumsan doloreco.');?>
          </p>
          <?= Yii::t('app', '<a class="fa1" href="#">More info</a>');?>
        </div>
      </div>
    </div>
  </section>
  <section class="well2 bg2">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInLeft">
          <h3 class="offs1"><?= Yii::t('app', 'Exceptional<br>Flexibility');?></h3>
          <img src="stamp/images/page-1_img02.png" alt="">
          <h4><?= Yii::t('app', 'Mes cuml dia sed ineniasing dolor.');?></h4>
          <p>
            <?= Yii::t('app', 'Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultricie upien disse comete dolo lectus fgilla itollicil tua ludin dolor nec met quam accumsan doloreco.');?>
          </p>
          <a class="fa1 offs2" href="#"><?= Yii::t('app', 'More info');?></a>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12 wow fadeInRight">
          <h2><?= Yii::t('app', '<span>Examples of Loans</span>');?></h2>
          <h4 class="colorText1">
            <?= Yii::t('app', 'Donec sollicitudin erat sit amet nisi eleifend, a viverra dui molestie. Curabitur finibus aliquam eleifend. 
          ');?></h4>
          <p>
            <?= Yii::t('app', 'Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultri cie upien disse comete dolo lectus fgilla itolli cil tua ludin dolor nec met quam accumsan dolo. Dolore condime netus lullam utlacus adipiscing ipsum molestie euismod lore estibu lum vel libero ipsum sit amet sollicitu din ante. Aenean imperdiet aliquet hendrerit unc interdum ullamcorper lectus et pellentesque enim interdum at. Suspendisse malesuada dignissim est, a facilisis ligula rutrum sed.');?>
          </p>
          <div class="row">
          <ul class="marked-list col-md-6 col-sm-12 col-xs-12">
            <li><a href="#"><?= Yii::t('app', 'Lorem ipsum dolor ipsu come.');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'consectetur adipisci aenean.');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'In euismod est quis aliquet.');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'venenatis imperdiet dol ips.');?></a></li>
          </ul>
          <ul class="marked-list col-md-6 col-sm-12 col-xs-12">
            <li><a href="#"><?= Yii::t('app', 'Lorem ipsum dolor ipsu come.');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'consectetur adipisci aenean.');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'In euismod est quis aliquet.');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'venenatis imperdiet dol ips.');?></a></li>
          </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="well3">
    <div class="container ">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft">
          <a class="fa2 fa-thumbs-o-up" href="#"></a> 
          <h2><?= Yii::t('app', 'Proven<br>Reliability ');?></h2>
          <h4 class="offs3">
            <?= Yii::t('app', 'Mes cuml dia sed ineniasinge dolor<br>
            Dolor ipsum commete ipsum.');?> 
          ');?></h4>
          <p>
            <?= Yii::t('app', 'Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultricie upi disse comete dolo lectus fgilla itollicil tua ludin dolor nec met quam accumsan. Dolore con dime netus lullam utlacus adipiscing ipsum molestie euismod lore estibulum vel libero ipsum sit amet sollicitudin.');?>
          </p>
          <a class="fa1" href="#">More info</a>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
          <a class="fa2 fa-lock" href="#"></a> 
          <h2><?= Yii::t('app', 'Rapid<br>Closings');?></h2>
          <h4 class="offs3">
            <?= Yii::t('app', 'Mes cuml dia sed ineniasinge dolor <br>
            Dolor ipsum commete ipsum. 
          ');?></h4>
          <p>
            <?= Yii::t('app', 'Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultricie upi disse comete dolo lectus fgilla itollicil tua ludin dolor nec met quam accumsan. Dolore con dime netus lullam utlacus adipiscing ipsum molestie euismod lore estibulum vel libero ipsum sit amet sollicitudin.');?>
          </p>
          <a class="fa1" href="#"><?= Yii::t('app', 'More info');?></a>
        </div>
      </div>
    </div>
  </section>
  <section class="well4 bg3">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft">
          <div class="box box1">
            <div class="aside">
              <span class="fa3 fa-envelope-o"></span>
            </div>
            <div class="cnt">
              <p>
                <?= Yii::t('app', 'Subscribe to<br>Our Newsletter');?>
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight subscribe-form">
          <form class='mailform' method="post" action="bat/rd-mailform.php">
            <input type="hidden" name="form-type" value="contact"/>
            <fieldset class="row">
              <div class="aside col-md-8 col-sm-7 col-xs-12">
                <label data-add-placeholder="">
                  <input type="text"
                  name="email"
                  placeholder="your email address"
                  data-constraints="@Email @NotEmpty"/>
                </label>
              </div>
              <div class="cnt col-md-4 col-sm-5 col-xs-12">
                <div class="mfControls">
                  <button class="" type="submit"><?= Yii::t('app', 'Subscribe');?></button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </section>
</main>