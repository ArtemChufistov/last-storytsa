<?php
use yii\widgets\Breadcrumbs;
use yii\web\View;
use yii\helpers\Url;
$this->params['breadcrumbs']= [ [
    'label' => 'О Нас',
    'url' => '/about'
  ],
];
$this->title = $model->title;
?>

<main>
  <section class="well6">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 ">
          <h2><?= Yii::t('app', 'Personal<br><span>Loan</span>');?></h2>
          <h4 class="colorText1">
           <?= Yii::t('app', 'amet nisi eleifend, a viverra dui mol estie Curabitur finibus ');?>
         </h4>
         <p>
          <?= Yii::t('app', 'Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultri cie upien disse comete dolo lectus fgilla itolli cil tua ludin dolor nec met quam accumsan dolo. Dolore condime netus lullam utlacus adipiscing');?>
        </p>
        <ul class="marked-list offs4">
          <li><a href="#"><?= Yii::t('app', 'Lorem ipsum dolor ipsu come.');?></a></li>
          <li><a href="#"><?= Yii::t('app', 'consectetur adipisci aenean.');?></a></li>
          <li><a href="#"><?= Yii::t('app', 'In euismod est quis aliquet.');?></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 ">
          <h2><?= Yii::t('app', 'Business<br><span>Loan</span>');?></h2>
          <h4 class="colorText1">
           <?= Yii::t('app', 'Nullam convallis lacus a arcu fac lisisnec auctor lacussu ');?>
         </h4>
         <p>
          <?= Yii::t('app', 'Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultri cie upien disse comete dolo lectus fgilla itolli cil tua ludin dolor nec met quam accumsan dolo. Dolore condime netus lullam utlacus adipiscing');?>
        </p>
        <ul class="marked-list offs4">
          <li><a href="#"><?= Yii::t('app', 'Nullam convallis lacus a.');?></a></li>
          <li><a href="#"><?= Yii::t('app', 'arcu facilisis nec auctor lacu.');?></a></li>
          <li><a href="#"><?= Yii::t('app', 'sconsequat Nam nec turp.');?></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-12 col-xs-12 ">
          <h2><?= Yii::t('app', 'Traders<br><span>Loan</span>');?></h2>
          <h4 class="colorText1">
           <?= Yii::t('app', 'lacus a arcu facilisis nec auctorla cus consequat Nam ');?>
         </h4>
         <p>
          <?= Yii::t('app', 'Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultri cie upien disse comete dolo lectus fgilla itolli cil tua ludin dolor nec met quam accumsan dolo. Dolore condime netus lullam utlacus adipiscing');?>
        </p>
        <ul class="marked-list offs4">
          <li><a href="#"><?= Yii::t('app', 'In euismod est quis aliquet.');?></a></li>
          <li><a href="#"><?= Yii::t('app', 'venenatis imperdiet dol ips.');?></a></li>
          <li><a href="#"><?= Yii::t('app', 'Lorem ipsum dolor.');?></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="well7 bg2">
    <div class="container">
          <h2 class="textLetSpas"><?= Yii::t('app', 'Private<span>Moneylenders Financing</span>');?></h2>
          <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
              <img src="stamp/images/page-2_img01.png" alt="">
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
              <h4 class="colorText1">
                <?= Yii::t('app', 'Donec sollicitudin erat sit amet nisi eleifend, a viverra dui molestie. 
                Curabitur finibus aliquam eleifend. ');?>
              </h4>
              <p>
                <?= Yii::t('app', 'Nunc sagittis ante sed neque fringilla ornare. In hendrerit, justo vitae sodales dictum, lacus ligula rutrum justo, vel acc
                umsan diam lectus et lectus. Nullam tincidunt vel diam dictum maximus. Praesent imperdiet libero sed massa aliquet portt
                itor. Cras eros erat, tristique at felis nec, congue tincidunt lectus. Morbi tempor placerat molestie. Curabitur lectus urna, sollicitudin ultricies ante at, sollicitudin commodo nisi. Integer vel sagittis mauris.  ');?>
              </p>
               <p class="offs5">
                <?= Yii::t('app', 'Duis et nunc neque. Curabitur eget purus vel enim dignissim mattis et eget mi. Nam ut mi ut elit venenatis euismod. Su
                spendisse ornare nisi vehicula orci consequat, dignissim dictum leo laoreet. Aenean interdum feugiat ullamcorper. Nullam hendrerit augue id ex ultricies condimentum. Etiam purus elit, tincidunt sodales nisi sed, congue molestie diam. Mauris viverra faucibus neque, ac sodales lorem dapibus in. Vestibulum in risus ');?>
              </p>
            </div>
          </div>
    </div>
  </section>
  <section class="well8">
    <div class="container ">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft">
          <a class="fa2 fa-area-chart" href="#"></a> 
          <h2><?= Yii::t('app', 'Non Recourse<br>Stock Loans ');?></h2>
          <h4 class="offs3">
           <?= Yii::t('app', 'Proin tincidunt sollicitudin magna, vitae posuere do or vehicula sit amet Suspendisse ');?>
          </h4>
          <p>
            <?= Yii::t('app', 'Nunc sagittis ante sed neque fringilla ornare. In hendrerit, justo vitae sodales dictum, lacus ligula rutrum justo, vel accumsan diam lectus et lectus. Nullam tincidunt vel diam dictum maximus. Praesent imperdiet libero sed massa aliquet porttitor. Cras eros erat, tristique at felis nec, congue tincidunt lectus. Morbi tempor ');?>
          </p>
          <a class="fa1" href="#"><?= Yii::t('app', 'More info');?></a>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
          <a class="fa2 fa-bar-chart" href="#"></a> 
          <h2 class="textLetSpas1"><?= Yii::t('app', 'International<br>Commercial Loans');?></h2>
          <h4 class="offs3">
            <?= Yii::t('app', 'vitae posuere dolor vehicula sit amet. Suspendisse ce ngue viverra arcu eget rhoncu');?>
          </h4>
          <p>
            <?= Yii::t('app', 'Nullam tincidunt vel diam dictum maximus. Praesent imperdiet libero sed massa aliquet porttitor. Cras eros erat, tristique at felis nec, congue tincidunt lectus. Morbi tempor placerat molestie. Curabitur lectus urna, sollicitudin ultricies ante at, sollicitudin commodo nisi. Integer vel sagittis mauris. Duis et nunc neque. ');?>
          </p>
          <a class="fa1" href="#"><?= Yii::t('app', 'More info');?></a>
        </div>
      </div>
    </div>
  </section>
</main>