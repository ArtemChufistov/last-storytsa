<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
		'label' => 'Как это работает',
		'url' => '/howitworks'
	],
];

$this->title = $model->title;
?>

<main>   
    <section class="well9">
      <div class="container">
        <h2><?= Yii::t('app', 'Eligibility Criteria<br><span>for Personal Loan</span>');?></h2>
        <div class="box">
          <div class="aside">
            <img src="stamp/images/page-3_img01.png" alt="">
          </div>
          <div class="cnt">
            <h4 class="colorText1">
              <?= Yii::t('app', 'Phasellus a fringilla nisi Suspendisse potenti In hac habitasse platea dic tumst Interdum et malesuada fames actricies ');?>
            </h4>
            <p>
             <?= Yii::t('app', 'Etiam tellus nulla, posuere quis tincidunt in, rhoncus id nisl. Ut aliquet semper ex laoreet luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi ultricies ipsum in magna convallis pharetra. Integer maximus velit quis finibus sagittis. Praesent rhoncus id lorem nec ornare. Suspendisse fringilla augue eu mauris vestibulum rhoncus. Vestibulum ante ipsum primis in faucibus orci luctus   ');?>
           </p>
           <div class="row">
             <ul class="marked-list offs1 col-md-6 col-sm-12 col-xs-12 ">
              <li><a href="#"><?= Yii::t('app', 'Lorem ipsum dolor ipsu come.');?></a></li>
              <li><a href="#"><?= Yii::t('app', 'consectetur adipisci aenean.');?></a></li>
              <li><a href="#"><?= Yii::t('app', 'In euismod est quis aliquet.');?></a></li>
            </ul>
            <ul class="marked-list offs1 col-md-6 col-sm-12 col-xs-12 ">
              <li><a href="#"><?= Yii::t('app', 'Lorem ipsum dolor ipsu come.');?></a></li>
              <li><a href="#"><?= Yii::t('app', 'consectetur adipisci aenean.');?></a></li>
              <li><a href="#"><?= Yii::t('app', 'In euismod est quis aliquet.');?></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="well10 bg2">
    <div class="container">
      <h2><?= Yii::t('app', 'Solutions for<span> Challenging Situations</span>');?></h2>
      <div class="row offs6">
        <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft">
          <div class="box">
            <div class="aside">
              <img src="stamp/images/page-3_img02.png" alt="">
            </div>
            <div class="cnt">
              <h4 class="colorText1">
               <a href="#"><?= Yii::t('app', ' sit amet nisi eleifend, a viv erra dui molestie');?></a>
             </h4>
             <p>
               <?= Yii::t('app', 'Nullam tincidunt vel diam dictum maximus. Praesent imperdiet libero sed massa aliquet porttitor. Cras eros erat, tristique at felis nec, congue tincidunt lectus. Morbi tempor placerat molestie. Curabitur lec tus urna, sollicitudin ultricies ante at, sollicitudin commodo nisi.  ');?>
             </p>
           </div>
         </div>
       </div>
       <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
        <div class="box">
          <div class="aside">
            <img src="stamp/images/page-3_img03.png" alt="">
          </div>
          <div class="cnt">
            <h4 class="colorText1">
             <a href="#"><?= Yii::t('app', ' sit amet nisi eleifend, a viv erra dui molestie');?></a>
           </h4>
           <p>
             <?= Yii::t('app', 'Nullam tincidunt vel diam dictum maximus. Praesent imperdiet libero sed massa aliquet porttitor. Cras eros erat, tristique at felis nec, congue tincidunt lectus. Morbi tempor placerat molestie. Curabitur lec tus urna, sollicitudin ultricies ante at, sollicitudin commodo nisi.  ');?> 
           </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="well11">
    <div class="container">
      <h2><?= Yii::t('app', 'Improving Your<br><span> Credit Rating</span>');?></h2>
      <div class="row offs7">
       <ul class="marked-list offs1 col-md-4 col-sm-6 col-xs-12 ">
        <li><a href="#"><?= Yii::t('app', 'Ultrices posuere itudin');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Suspendisse sollic velitaugue');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Ut pharetra augue nec');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Nam elit agna endrerit sit amet');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Tincidunt ac viverra');?></a></li>
      </ul>
      <ul class="marked-list offs1 col-md-4 col-sm-6 col-xs-12 ">
        <li><a href="#"><?= Yii::t('app', 'Ut pharetra augue nec');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Nam elit agna endrerit sit amet');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Tincidunt ac viverra');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Donec porta diam eu massa');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Quisque diam lorem');?></a></li>
      </ul>
      <ul class="marked-list offs1 col-md-4 col-sm-12 col-xs-12 ">
        <li><a href="#"><?= Yii::t('app', 'Tincidunt ac viverra');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Donec porta diam eu massa');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Quisque diam lorem');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Interdum vitae dapibus pulvinar');?></a></li>
        <li><a href="#"><?= Yii::t('app', 'Scelerisque vitae pede');?></a></li>
      </ul>
    </div>
  </div>
</section>
  </main>