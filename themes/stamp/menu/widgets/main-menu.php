<?php
use app\modules\lang\widgets\WLang;
use yii\helpers\Url;
?>
<header>
    <div class="top-row">
      <div class="container">
        <div class="brand">
          <h1 class="brand_name">
            <a href="./"><?= Yii::t('app', 'Cashyx');?></a>
          </h1>
          <p class="brand_slogan">
            <?= Yii::t('app', 'Moneylender');?>
          </p>
        </div>
        <div class="phone1">
          <p><?= Yii::t('app', 'Call for info');?></p>
          <strong>
            <a href="callto:#"><?= Yii::t('app', '+1-234-567-8900');?></a>
          </strong>
        </div>
      </div>
    </div>
    <div id="stuck_container" class="stuck_container">
      <div class="container">
        <nav class="nav">
          <ul class="sf-menu" data-type="navbar">
            <?php foreach ($menu->children()->all() as $num => $children): ?>
            <li class="<?php if ($requestUrl == Url::to([$children->link])): ?>active<?php endif;?>">
              <a href="<?=Url::to([$children->link]);?>" title="<?=$children->title;?>"><?=$children->title;?></a>
              <?php if (!empty($children->children()->all())): ?>
              <ul>
                <?php foreach ($children->children()->all() as $numChild => $childChildren): ?>
                <li>
                  <a href="<?=Url::to([$childChildren->link]);?>" title="<?=$childChildren->title;?>"><?=$childChildren->title;?></a>
                </li>
                <?php endforeach;?>
              </ul>
              <?php endif;?>
            </li>
            <?php endforeach;?>
          </ul>
        </nav>
      </div>
    </div>
  </header>