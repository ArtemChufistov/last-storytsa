<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = Yii::t('user', 'Рекомендации');

?>

<style>
.skachat_arhiv{
	font-weight: bold;
}
.book_href img{
	height: 500px;
}
</style>

<div class = "row">
	<div class = "col-md-12" style = "margin-bottom: 25px;">
		<br/>
		<h3><?php echo Yii::t('app', 'Архив книг');?></h3>
	</div>
</div>

<div class = "row">
	<div class = "col-md-4">
		<div class="content-box">
			<div class="content-box-wrapper text-center clearfix" style="text-align: justify;">
				<a class ="book_href" href = "/bitinfo/books/Adam_temper_bitcoin_dengi_dliz_vseh.pdf" target="_blank">
					<img src = "/bitinfo/books/Adam_temper_bitcoin_dengi_dliz_vseh.jpg">
				</a>
				<br/>
				<br/>
				<a class = "skachat_arhiv" href = "/bitinfo/books/Adam_temper_bitcoin_dengi_dliz_vseh.rar"><?php echo Yii::t('app', 'СКАЧАТЬ АРХИВ');?></a>
			</div>
		</div>
	</div>
	<div class = "col-md-4">
		<div class="content-box">
			<div class="content-box-wrapper text-center clearfix" style="text-align: justify;">
				<a class ="book_href" href = "/bitinfo/books/epoha_kriptovalut.pdf" target="_blank">
					<img src = "/bitinfo/books/epoha_kriptovalut.png">
				</a>
				<br/>
				<br/>
				<a class = "skachat_arhiv" href = "/bitinfo/books/epoha_kriptovalut.rar"><?php echo Yii::t('app', 'СКАЧАТЬ АРХИВ');?></a>
			</div>
		</div>
	</div>
	<div class = "col-md-4">
		<div class="content-box">
			<div class="content-box-wrapper text-center clearfix" style="text-align: justify;">
				<a class ="book_href" href = "/bitinfo/books/Tsifrovoe_Zoloto.pdf" target="_blank">
					<img src = "/bitinfo/books/Tsifrovoe_Zoloto.jpg">
				</a>
				<br/>
				<br/>
				<a class = "skachat_arhiv" href = "/bitinfo/books/Tsifrovoe_Zoloto.rar"><?php echo Yii::t('app', 'СКАЧАТЬ АРХИВ');?></a>
			</div>
		</div>
	</div>
</div>