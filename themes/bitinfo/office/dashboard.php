<?php
use app\modules\finance\models\CryptoWallet;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyGraph;
use app\modules\finance\models\Payment;
use app\modules\matrix\models\Matrix;
use app\modules\finance\models\Transaction;
use app\modules\event\models\Event;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Панель управления');
$this->params['breadcrumbs'][] = $this->title;


?>

<div id="page-title">
    <h2><?php echo $this->title;?></h2>
    <p><?php echo Yii::t('user', 'Сводная информация по вашему аккаунту');?></p>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="tile-box tile-box-alt mrg20B bg-blue-alt">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Ваш баланс:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-suitcase"></i>
                <div class="tile-content">

                    <?php foreach ($user->getBalances() as $balance): ?>
                        <span>$</span>
                        <?php echo $balance->showValue(); ?> <?php echo $balance->getCurrency()->one()->title; ?>
                    <?php endforeach;?>

                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Ваш личный финансовый счёт');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/finance']);?>" class="tile-footer tooltip-button" data-placement="bottom" title="<?php echo Yii::t('app', 'Перейти в раздел финансы');?>">
                <?php echo Yii::t('app', 'Перейти в раздел финансы');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="tile-box tile-box-alt mrg20B bg-purple">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Ваши доходные места:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-money"></i>
                <div class="tile-content">
                    <?php $matrixArray = Matrix::find()->where(['user_id' => $user->id])->all(); echo count($matrixArray);?>
                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Основной раздел, отображающий ваш доход');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/program']);?>" class="tile-footer tooltip-button" data-placement="bottom" title="<?php echo Yii::t('app', 'Перейти в раздел доходности');?>">
                <?php echo Yii::t('app', 'Перейти в раздел доходности');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="tile-box tile-box-alt mrg20B bg-green">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Ваши партнёры:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-users"></i>
                <div class="tile-content">
                    <?php $descendants = $user->descendants()->all(); echo count($descendants);?>
                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Общее количество партнёров');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/struct']);?>" class="tile-footer tooltip-button" data-placement="bottom" title="<?php echo Yii::t('app', 'Перейти в раздел вашей структуры');?>">
                <?php echo Yii::t('app', 'Перейти в раздел вашей структуры');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="tile-box tile-box-alt mrg20B bg-orange">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Промо материалы:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-bullhorn"></i>
                <div class="tile-content">
                    6
                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Рекламная информация для сайта');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/promo']);?>" class="tile-footer tooltip-button" data-placement="bottom" title="<?php echo Yii::t('app', 'Перейти в раздел промо материалов');?>">
                <?php echo Yii::t('app', 'Перейти в раздел промо материалов');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                <?php echo Yii::t('app', 'Как это работает');?>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%">
                    
                    <?php echo Yii::t('app', '

                        <img src ="/bitinfo/blockchain.jpg" style = "float: right; width: 300px;">
                    Благодаря «Bitinfo» любой человек имеет возможность разобраться в новом направлении и получать неограниченный доход.
                    «Bitinfo» доступен даже тем, кто делает свои первые шаги в освоении технологии блокчейн и криптовалют. А если у Вас есть опыт построения структур и Вы будете проявлять активность, то Ваша прибыль увеличится в разы, за счёт привлекательного плана партнёрских вознаграждений.

                    Уникальный маркетинг системы «Bit.info» разработан лучшими интернет - маркетологами. Вам не придется проходить несколько матриц для того, чтобы выйти на вознаграждение, Вы начнёте получать прибыль сразу. Благодаря информации полученной в нашей системе «Bit.info» и ее уникальному маркетингу, а также особенностям партнерской программы, Вы легко соберете доходный и сбалансированный инвестиционный портфель из криптовалют,  разовьете свой бизнес и выйдите на постоянный, пассивный доход!
                    ');?>
                    <br/>
                    <br/>
                    <div class="example-box-wrapper">
                        <a href="/howitworks" target = "_blank" class="btn btn-lg btn-default" title=""><?php echo Yii::t('app', 'Как работает маркетинг');?></a>
                        <a href="/office/program" class="btn btn-lg btn-primary" title=""><?php echo Yii::t('app', 'Перейти к инвестированию');?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">

<div class="panel">
<div class="panel-body">
<h3 class="title-hero">
    <?php echo Yii::t('app', 'Последние 10 операций');?>
</h3>
<div class="example-box-wrapper">
<table id="datatable-tabletools" class="table table-striped table-bordered" cellspacing="0" width="100%">
<thead>
    <thead>
        <tr>
            <th><?php echo Yii::t('app', 'Тип');?></th>
            <th><?php echo Yii::t('app', 'Сумма');?></th>
            <th><?php echo Yii::t('app', 'Дата');?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach(Transaction::find()->where(['from_user_id' => $user->id])->orWHere(['to_user_id' => $user->id])->limit(10)->orderBy(['id' => SORT_DESC])->all() as $transaction):?>
            <?php $toUser = $transaction->getToUser()->one();?>
            <?php $fromUser = $transaction->getFromUser()->one();?>
            <tr>
                <td><?php echo $transaction->getTypeTitle();?></td>
                <td>
                    <?php if (!empty($toUser) && $toUser->id == $user->id):?>
                        <span class="badge bg-green">+<?php echo $transaction->sum;?> <?php echo $transaction->getCurrency()->one()->key;?></span>
                    <?php else:?>
                        <span class="badge bg-red">-<?php echo $transaction->sum;?> <?php echo $transaction->getCurrency()->one()->key;?></span>
                    <?php endif;?>
                </td>
                <td><?php echo date("H-i-s d-m-y", strtotime($transaction->date_add));?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
</div>
</div>

</div>


    </div>
    <div class="col-md-3">

<div class="panel">
<div class="panel-body">
<h3 class="title-hero">
    <?php echo Yii::t('app', 'Посление <strong>10</strong> событий');?>
</h3>
<div class="example-box-wrapper">
<table id="datatable-tabletools" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th><?php echo Yii::t('app', 'Событие');?></th>
            <th><?php echo Yii::t('app', 'Логин');?></th>
            <th><?php echo Yii::t('app', 'Дата');?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach(Event::find()->where(['user_id' => $user->id])->limit(10)->orderBy(['date_add' => SORT_DESC])->all() as $event):?>
            <?php $toUser = $event->getToUser()->one();?>
            <?php if (!empty($toUser)):?>
                <tr>
                    <td><?php echo $event->getTypeTitle();?></td>
                    <td><?php echo $toUser->login;?></td>
                    <td><?php echo date("H-i-s d-m-y", strtotime($event->date_add));?></td>
                </tr>
            <?php endif;?>
        <?php endforeach;?>
    </tbody>
</table>
</div>
</div>
</div>


    </div>
</div>