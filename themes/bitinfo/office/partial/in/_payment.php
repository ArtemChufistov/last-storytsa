<?php
use lowbase\user\UserAsset;

$this->title = Yii::t('user', 'Финансы - Пополнение личного счёта');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

?>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div id="form-wizard-3" class="form-wizard">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_tabs', [
	'active' => 4,
	'paymentForm' => $paymentForm,
]); ?>
            <div class="tab-content">
              <div class="tab-pane active" id="step-4">
                  <div class="content-box">
                      <h3 class="content-box-header bg-purple">
                          <?php echo Yii::t('app', 'Ваша оплата'); ?>
                      </h3>
                    <div class="content-box-wrapper">
                      <?php echo Yii::$app->controller->renderPartial(
	'@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_successpay_' . $paymentForm->getFromPaySystem()->one()->key, [
		'paymentForm' => $paymentForm,
		'user' => $user,
	]); ?>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
