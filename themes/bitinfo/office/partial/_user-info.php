<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

$defaultImage = '/images/money_hand.png';
?>

<div style="width: 300px;">
	<div class="col-md-12">
	    <div class="profile-box content-box">
	        <div class="content-box-header clearfix bg-orange">
	            <img src="<?php echo empty($user) ? $defaultImage : $user->getImage();?>" width="54" alt="" class="img-circle">
	            <div class="user-details">
	                <?php echo empty($user) ? Yii::t('app', 'Свободное') : $user->login;?>
	                <span><?php echo empty($user) ? Yii::t('app', 'место') : Yii::t('app', 'Участник системы');?></span>
	            </div>
	        </div>
	        <div class="list-group">
                <?php if(!empty($user) && !empty($user->email)):?>
	            	<span class="list-group-item">
	            		<?php echo Yii::t('app', 'E-mail: {email}', ['email' => $user->email]);?>
	            	</span>
                <?php endif;?>
                <?php if(!empty($user) && !empty($user->phone)):?>
	            	<span class="list-group-item">
	            		<?php echo Yii::t('app', 'Телефон: {phone}', ['phone' => $user->phone]);?>
	            	</span>
                <?php endif;?>
                <?php if(!empty($user) && !empty($user->first_name)):?>
	            	<span class="list-group-item">
	            		<?php echo Yii::t('app', 'Имя: {first_name}', ['first_name' => $user->first_name]);?>
	            	</span>
                <?php endif;?>
                <?php if(!empty($user) && !empty($user->last_name)):?>
	            	<span class="list-group-item">
	            		<?php echo Yii::t('app', 'Фамилия: {last_name}', ['last_name' => $user->last_name]);?>
	            	</span>
                <?php endif;?>
                <?php if(!empty($user) && !empty($user->skype)):?>
	            	<span class="list-group-item">
	            		<?php echo Yii::t('app', 'Skype: {skype}', ['skype' => $user->skype]);?>
	            	</span>
                <?php endif;?>
	        </div>
	    </div>
	</div>
</div>