<?php

use app\modules\matrix\components\StorytsaMarketing;
use app\modules\profile\widgets\UserTreeElementWidget;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>

<div id = "animationSandbox">
    <div class="content-box">
      <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Как это работает');?>
        <div class="font-size-11 float-right">
          <a href="#" title=""><i class="glyph-icon opacity-hover icon-star-o	"></i></a>
        </div>
      </h3>
      <div class="content-box-wrapper clearfix">
        <p style = "text-align: left;">
        	<?php echo Yii::t('app', '<img src = "/bitinfo/kar_rabotat.jpg" style = "float: right; margin-left: 20px; width: 300px;">
            <p>Преимущества маркетинга системы «Bit.info»:</p>
<br/>• Оплата участия 100 $ единоразово;
<br/>• Многократное получение прибыли;
<br/>• Нет обязательных приглашений;
<br/>• Очень короткая матрица 1 + 3;
<br/>• Равномерное распределение участников;
<br/>• Заполнение матрицы на 2/3 клонами;
<br/>• Выгодная партнерская программа;
<br/>• Бесплатный доступ к платным Информационным Продуктам.
<br/>
<br/>
«Bit.info» это реальный шанс достичь финансового благополучия!
        	');?>
        </p>

        <?php $form = ActiveForm::begin(); ?>
        <br/>
			<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
			  'class' => 'butt js--triggerAnimation btn btn-success btn-lg mrg20B',
			  'value' => 1,
			  'name' => 'marketing-matrix']) ?>

        <a href="/howitworks" target = "_blank" class="btn btn-lg btn-default" style = "float: right;" title=""><?php echo Yii::t('app', 'Как работает маркетинг');?></a>

		<?php ActiveForm::end();?>
      </div>
    </div>
</div>


