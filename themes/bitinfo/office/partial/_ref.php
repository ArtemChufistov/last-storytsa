<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;

$matrix = Matrix::find()->where(['user_id' => $user->id])->one();
?>


<div class="content-box">
  <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Ваша реферальная ссылка');?>
    <div class="font-size-11 float-right">
      <a href="#" title=""><i class="glyph-icon opacity-hover icon-user-md"></i></a>
    </div>
  </h3>
  <div class="content-box-wrapper text-center clearfix">
    <?php if (!empty($matrix)):?>
      <div class="form-group">
          <?php echo Html::input('text', 'refLink', Url::home(true) . '?ref=' . $user->login, ['id' => 'refLink', 'class' => 'form-control' , 'readonly' => true]);?>
      </div>
      <div class="form-group">
          <?php echo Html::input('button', '', Yii::t('app', 'Скопировать в буфер обмена'), ['class' => 'btn-clipboard btn btn-success pull-left', 'data-clipboard-target' => '#refLink']);?>
      </div>
    <?php else:?>
      <?php echo Yii::t('app', 'Реферальная ссылка будет доступна только после</br> <a style = "font-weight: bold; text-decoration:underline;" href = "/profile/office/program/1">принятия участия в программе.</a>')?>
    <?php endif;?>
  </div>
</div>

<?php $this->registerJs('new Clipboard(".btn-clipboard");');?>