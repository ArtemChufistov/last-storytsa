<?php if (!empty($parentUser)):?>

<div class="panel-layout">
    <div class="panel-box">

        <div class="panel-content image-box" style = "height: 100px;">
            <div class="corner-ribbon">
                <a href="#" class="btn-success tooltip-button" title="<?php echo $parentUser->login;?>">
                    <i class="glyph-icon icon-user"></i>
                </a>
            </div>
            <img src="/image-resources/blurred-bg/blurred-bg-14.jpg" alt="">

        </div>
        <div class="panel-content pad0A bg-white">
            <div class="meta-box meta-box-offset">
                <img src="<?php echo $parentUser->getImage();?>" alt="" class="meta-image img-bordered img-circle" style = "width: 85px; height: 85px;">
                <h3 class="meta-heading font-size-16"><?php echo $parentUser->login;?></h3>
                <h4 class="meta-subheading font-size-13 font-gray"><?php echo Yii::t('app', 'Участник проекта');?></h4>
            </div>
            <table class="table mrg0B mrg10T">
                <tbody>
                <tr>
                    <td class="text-left">
                        <b><?php echo Yii::t('app', 'E-mail:');?></b>
                    </td>
                    <td style="width: 50%;">
                        <?php echo $parentUser->email;?>
                    </td>
                </tr>
                <?php if (!empty($parentUser->first_name)):?>
	                <tr>
	                    <td class="text-left">
	                        <b><?php echo Yii::t('app', 'Имя:');?></b>
	                    </td>
	                    <td style="width: 50%;">
	                        <?php echo $parentUser->first_name;?>
	                    </td>
	                </tr>
	            <?php endif;?>
                <?php if (!empty($parentUser->last_name)):?>
	                <tr>
	                    <td class="text-left">
	                        <b><?php echo Yii::t('app', 'Фамилия:');?></b>
	                    </td>
	                    <td style="width: 50%;">
	                        <?php echo $parentUser->last_name;?>
	                    </td>
	                </tr>
	            <?php endif;?>
                <?php if (!empty($parentUser->skype)):?>
	                <tr>
	                    <td class="text-left">
	                        <b><?php echo Yii::t('app', 'Skype:');?></b>
	                    </td>
	                    <td style="width: 50%;">
	                        <?php echo $parentUser->skype;?>
	                    </td>
	                </tr>
	            <?php endif;?>
                </tbody>
            </table>
        </div>

    </div>
</div>
<?php endif;?>