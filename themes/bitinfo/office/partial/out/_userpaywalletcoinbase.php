<?php
use app\modules\finance\widgets\PayWalletPasswordWidget;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Финансы - Снятие с личного счёта');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

?>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div id="form-wizard-3" class="form-wizard">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_tabs', [
	'active' => 4,
	'paymentForm' => $paymentForm,
]); ?>
            <div class="tab-content">
              <div class="tab-pane active" id="step-4">
                  <div class="content-box">
                      <h3 class="content-box-header bg-purple">
                          <?php echo Yii::t('app', 'Укажите ваш кошелёк <strong>BitCoin</strong>'); ?>
                      </h3>
                    <div class="content-box-wrapper">
						<?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

						  <?=$form->field($paymentForm, 'wallet', ['template' => '<label class="col-sm-3 control-label">' . Yii::t('app', 'Платёжный кошелёк:') . '</label><div class="col-sm-4">{input}{error}</div>'])->textInput(['value' => $paymentForm->wallet, 'class' => 'form-control'])?>

						  <?=$form->field($paymentForm, 'pay_password', ['template' =>
	'<label class="col-sm-3 control-label">' . Yii::t('app', 'Платёжный пароль:') . '</label>
						  <div class="col-sm-4">{input}{error}</div>', ])->textInput(['value' => $paymentForm->pay_password, 'class' => 'form-control'])?>

						  <div class="form-group">
						  	<div class="col-sm-3">
						  	</div>
						    <div class="col-sm-4">
						      <?=Html::submitButton(Yii::t('app', 'Далее'), ['class' => 'btn btn-lg  bg-green'])?>
						    </div>
						  </div>

						<?php ActiveForm::end();?>

						<?php echo PayWalletPasswordWidget::widget(['user' => $user]); ?>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
