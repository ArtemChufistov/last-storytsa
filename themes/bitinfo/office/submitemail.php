<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::t('user', 'E-mail подтверждён');
?>




<div class="panel section-34 section-sm-41 inset-left-20 inset-right-20 inset-sm-left-20 inset-sm-right-20 inset-lg-left-30 inset-lg-right-30 bg-white shadow-drop-md">
  <span class="icon icon-circle icon-bordered icon-lg icon-default mdi mdi-account-multiple-outline"></span>
  <div>
    <div class="offset-top-24 text-darker big text-bold">
      <?php echo Yii::t('app', 'Поздравляем вы успешно подтвердили свой <strong>E-mail</strong>');?>
    </div>
  </div>
  
  <div class="content-box-wrapper" style = "text-align: center;">
    <p><?php echo Yii::t('app', 'Теперь Вам доступен полный функционал личного кабинета');?></p>
    </br>
    <a href = "<?php echo Url::to(['/profile/office/dashboard']);?>" style = "font-weight:bold; text-decoration: underline;">
      <?php echo Yii::t('app', 'Перейти в личный кабинет');?>
    </a>
  </div>
</div>