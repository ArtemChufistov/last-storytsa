
<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Payment;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = Yii::t('user', 'Финансы');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<div id="page-title">
  <h2><?php echo $this->title;?></h2>
  <p><?php echo Yii::t('app', 'Управление вашими финансами');?></p>
</div>

<div class="row">
  <div class="col-lg-6">
    <div class="content-box">
      <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Выберите действие');?>
        <div class="font-size-11 float-right">
          <a href="#" title=""><i class="glyph-icon opacity-hover icon-bookmark"></i></a>
        </div>
      </h3>
      <div class="content-box-wrapper text-center clearfix" style = "text-align: justify;">
        <p><?php echo Yii::t('app', 'Для <strong>Пополнения/Снятия</strong> средств личного кабинета выберите соответствующее действие');?></p>
        <br/>
        <a href= "<?php echo Url::to(['/profile/office/in']);?>" class="btn btn-success pull-left" >
          <i class="fa fa-level-down"></i> <?php echo Yii::t('app', 'Пополнить личный счёт');?>
        </a>
        <a href= "<?php echo Url::to(['/profile/office/out']);?>" class="btn btn-success pull-right" >
          <i class="fa fa-level-up"></i> <?php echo Yii::t('app', 'Снять с личного счёта');?>
        </a>
      </div>
    </div>

  <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $paymentProvider,
        'filterModel' => $payment,
        'id' => 'payment-grid',
        'layout' => '
          <div class="content-box">
            <h3 class="content-box-header clearfix">' . Yii::t('app', 'Ваши Пополнения/Снятия') . '
              <div class="font-size-11 float-right">
                {pager}
              </div>
            </h3>
            <div class="content-box-wrapper text-center clearfix">
              {items}
            </div>
          </div>',
        'columns' => [
            [
              'attribute' => 'type',
              'format' => 'raw',
              'filter' => Select2::widget([
                  'name' => 'PaymentFinanceSearch[type]',
                  'data' => Payment::getFinanceTypeArray(),
                  'theme' => Select2::THEME_BOOTSTRAP,
                  'hideSearch' => true,
                  'options' => [
                      'placeholder' => Yii::t('app', 'Выберите тип'),
                      'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                  ]
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-type', ['model' => $model]);},
            ],[
              'attribute' => 'realSum',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payments-sum', ['model' => $model]);},
            ],[
              'attribute' => 'status',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-status', ['model' => $model]);},
              'filter' => Select2::widget([
                  'name' => 'PaymentFinanceSearch[status]',
                  'data' => Payment::getStatusArray(),
                  'theme' => Select2::THEME_BOOTSTRAP,
                  'hideSearch' => true,
                  'options' => [
                      'placeholder' => Yii::t('app', 'Выберите статус'),
                      'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                  ]
              ]),
            ],[
              'attribute' => 'date_add',
              'format' => 'raw',
              'filter' => true,
              'filter' => \yii\jui\DatePicker::widget([
                  'model'=>$payment,
                  'attribute'=>'date_add',
                  'options' => ['class' => 'form-control'],
                  'language' => 'ru',
                  'dateFormat' => 'dd-MM-yyyy',
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-date-add', ['model' => $model]);},
            ]
        ],
    ]); ?>
  <?php Pjax::end(); ?>

</div>

  <?php if (!empty($transactionProvider->getModels())):?>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $transactionProvider,
        'filterModel' => $transaction,
        'id' => 'payment-grid',
        'layout' => '
          <div class="col-lg-6">
            <div class="content-box">
              <h3 class="content-box-header clearfix">' . Yii::t('app', 'Движения по счёту') . '
                <div class="font-size-11 float-right">
                  {pager}<a href="#" title=""><i class="glyph-icon opacity-hover icon--show-thumbnails"></i></a>
                </div>
              </h3>
            <div class="content-box-wrapper text-center clearfix">
              {items}
            </div>
          </div>',
        'columns' => [
            [
              'attribute' => 'type',
              'format' => 'raw',
              'filter' => Select2::widget([
                  'name' => 'TransactionFinanceSearch[type]',
                  'data' => Transaction::getOfficeTypeArray(),
                  'theme' => Select2::THEME_BOOTSTRAP,
                  'hideSearch' => true,
                  'options' => [
                      'placeholder' => Yii::t('app', 'Выберите тип'),
                      'value' => isset($_GET['TransactionFinanceSearch[type]']) ? $_GET['TransactionFinanceSearch[type]'] : null
                  ]
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-type', ['model' => $model]);},
            ],[
              'attribute' => 'data',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-data', ['model' => $model]);},
            ],[
              'attribute' => 'sum',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-sum', ['model' => $model]);},
            ],[
              'attribute' => 'date_add',
              'format' => 'raw',
              'filter' => true,
              'filter' => \yii\jui\DatePicker::widget([
                  'model'=>$transaction,
                  'attribute'=>'date_add',
                  'options' => ['class' => 'form-control'],
                  'language' => 'ru',
                  'dateFormat' => 'dd-MM-yyyy',
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-date-add', ['model' => $model]);},
            ]
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  <?php endif;?>

  

    <?php if (!empty($additionPaymentArray)):?>
      <div class="col-md-3">
          <div class="box additionPaymentInfo">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-bell-o" aria-hidden="true"></i><?php echo Yii::t('app', 'Доп. Информация');?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th style="width: 10px"><?php echo Yii::t('app', 'Сумма');?></th>
                  <th style="width: 10px"><?php echo Yii::t('app', 'Тип');?></th>
                  <th style="width: 40px"><?php echo Yii::t('app', 'Описание');?></th>
                </tr>
                <?php foreach($additionPaymentArray as $additionPayment):?>
                  <tr>
                    <td><?php echo $additionPayment->realSum; ?></td>
                    <td><?php echo $additionPayment->getTypeTitle(); ?></td>
                    <td><?php echo $additionPayment->additional_info; ?></td>
                  </tr>
                <?php endforeach;?>
              </table>
            </div>
          </div>
      </div>
    <?php endif;?>


<div class="modal fade paymentInfoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo Yii::t('app', 'Информация о платеже');?></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs('
  $(".content").on("click", ".showPaymentInfo", function(){
    $.get("' . Url::to("/profile/office/showpaymentinfo") . '/" + $(this).attr("paymentHash"), function( data ) {
      $(".paymentInfoModal").find(".modal-body").html(data);
      $(".paymentInfoModal").modal("show");
    });
  })
');?>
