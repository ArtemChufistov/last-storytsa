<?php

use app\modules\profile\components\AuthKeysManager;
use yii\authclient\widgets\AuthChoice;
use lowbase\user\models\Country;
use app\modules\profile\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Мой профиль');
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
.auth-clients{
  display: inline-table;
  padding-left: 0px;
}
</style>

<?php $form = ActiveForm::begin([
  'id' => 'form-profile',
  'options' => [
      'class'=>'form row',
      'enctype'=>'multipart/form-data'
  ],
]); ?>

<div class="col-md-5">
    <div class="panel-layout">
        <div class="panel-box">

            <div class="panel-content image-box" style="max-height: 425px;">
                <div class="ribbon">
                    <div class="bg-primary">BITINFO</div>
                </div>
                <div class="image-content font-white" >

                    <div class="meta-box meta-box-bottom" style = "position:relative;">
                     <?php
                      if ($user->image) {
                          echo "<img style = 'width: 120px; height: 120px; margin-top: 50px;' src='/".$user->image."' class='meta-image img-bordered img-circle'>";
                          echo "<p>" . Html::a(Yii::t('app', 'Удалить фото'), ['/profile/office/remove']) . "</p>";
                      } else {
                              echo "<img style = 'width: 120px; height: 120px;' src='/bitinfo/user3.png' class='meta-image img-bordered img-circle'>";
                      }
                      ?>
                     <h3 class="meta-heading"><?php echo $user->login;?></h3>
                     <h4 class="meta-subheading"><?php echo Yii::t('app', 'Участник проекта');?></h4>
                     </br>
                     <?= $form->field($user, 'photo', ['options' => [
                      'class' => 'btn btn-default btn-file'], 'template' => '<i class="fa fa-paperclip"></i> ' . Yii::t('app', 'Ваше изображение'). '{input}'])->fileInput() ?>
                    </br>
                    </br>
                     <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                      'class' => 'btn btn-warning',
                      'name' => 'signup-button']) ?>
                    </div>

                </div>
                <img src="/image-resources/blurred-bg/blurred-bg-13.jpg" alt="">

            </div>
        </div>
    </div>

    <div class="content-box mrg15B">
       <h3 class="content-box-header clearfix">
          <?php echo Yii::t('app', 'Смена пароля');?>
          <div class="font-size-11 float-right">
             <a href="#" title="">
             <i class="glyph-icon opacity-hover icon-key"></i>
             </a>
          </div>
       </h3>
       <div class="content-box-wrapper text-center clearfix">
        <div class="lb-user-user-profile-password">
            <?= $form->field($user, 'old_password')->passwordInput([
                'maxlength' => true,
                'value' => '',
                'placeholder' => $user->getAttributeLabel('old_password'),
                'class' => 'form-control password'
            ]) ?>
        </div>

        <div class="lb-user-user-profile-password">
            <?= $form->field($user, 'password')->passwordInput([
                'maxlength' => true,
                'placeholder' => $user->getAttributeLabel('password'),
                'class' => 'form-control password'
            ]) ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                'class' => 'btn btn-warning pull-left',
                'value' => 1,
                'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
        </div>
       </div>
    </div>

</div>

<div class="col-md-5">
  <div class="content-box mrg15B">
     <h3 class="content-box-header clearfix">
        <?php echo Yii::t('app', 'О себе');?>
        <div class="font-size-11 float-right">
           <a href="#" title="">
           <i class="glyph-icon opacity-hover icon-cog"></i>
           </a>
        </div>
     </h3>
     <div class="content-box-wrapper text-center clearfix">

      <?= $form->field($user, 'first_name')->textInput([
          'maxlength' => true,
          'placeholder' => $user->getAttributeLabel('first_name')
      ]) ?>

      <?= $form->field($user, 'last_name')->textInput([
          'maxlength' => true,
          'placeholder' => $user->getAttributeLabel('last_name')
      ]) ?>

      <?= $form->field($user, 'phone')->textInput([
          'maxlength' => true,
          'placeholder' => $user->getAttributeLabel('phone')
      ]) ?>

      <?= $form->field($user, 'skype')->textInput([
          'maxlength' => true,
          'placeholder' => $user->getAttributeLabel('skype')
      ]) ?>

      <?= $form->field($user, 'birthday')
          ->widget(DatePicker::classname(), [
              'options' => ['placeholder' => $user->getAttributeLabel('birthday')],
              'type' => DatePicker::TYPE_COMPONENT_APPEND,
              'pluginOptions' => [
                  'autoclose'=>true,
                  'format' => 'dd.mm.yyyy'
              ]
      ]); ?>

      <div class="input-wrap">
          <div class="clearfix" id="UserLogin-gender">
              <label class="radio-head"><?php echo Yii::t('app', 'Пол');?></label>
              <?=
              $form->field($user, 'sex')
                  ->radioList(
                      [null => Yii::t('app', 'Не указан')] + User::getSexArray(),
                      [
                          'item' => function($index, $label, $name, $checked, $value) {

                              $checked = $checked == true ? 'checked="checked"' : '';
                              $return = '<label class="radio-inline">';
                              $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"  ' . $checked . ' >';
                              $return .= '<i></i>';
                              $return .= '<span>' . ucwords($label) . '</span>';
                              $return .= '</label>';

                              return $return;
                          }
                      ]
                  )
              ->label(false);
              ?>
          </div>
          <div class="help-block"></div>
      </div>

      <div class="form-group">
          <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
              'class' => 'btn btn-info pull-right',
              'name' => 'signup-button']) ?>
      </div>

     </div>
  </div>

  <div class="content-box mrg15B">
     <h3 class="content-box-header clearfix">
        <?php echo Yii::t('app', 'Сменить E-mail');?>
        <div class="font-size-11 float-right">
           <a href="#" title="">
           <i class="glyph-icon opacity-hover icon-envelope"></i>
           </a>
        </div>
     </h3>
     <div class="content-box-wrapper text-center clearfix">
      <?= $form->field($user, 'email')->textInput([
          'maxlength' => true,
          'readonly' => true,
          'placeholder' => $user->getAttributeLabel('email')
      ]) ?>

      <?= $form->field($user, 'email_confirm_token',['options' => [
          'style' => $oldUser->email_confirm_token == '' ? 'display:none;' : 'display:block;']])->textInput([
          'maxlength' => true,
          'value' => '',
          'placeholder' => $user->getAttributeLabel('email_confirm_token')
      ]) ?>

      <div class="form-group">

          <?php if ($oldUser->email_confirm_token == ''):?>
              <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отвязать'), [
                  'class' => 'btn btn-danger pull-left',
                  'value' => 1,
                  'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
          <?php else:?>
              <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отвязать'), [
                  'class' => 'btn btn-danger pull-left',
                  'value' => 1,
                  'name' =>  (new \ReflectionClass($user))->getShortName() . '[confirm_token_submit]']) ?>

              <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить повторно код подтверждения'), [
                  'class' => 'btn btn-success pull-right',
                  'value' => 1,
                  'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
          <?php endif;?>

      </div>
     </div>
  </div>
</div>

<?php ActiveForm::end();?>