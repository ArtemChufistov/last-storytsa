<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Рекламные материалы');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<div id="page-title">
      <h2><?php echo $this->title;?></h2>
      <p><?php echo Yii::t('app', 'Рекламные материалы');?></p>
</div>

<div class="row">
  <div class="col-lg-5">
    <div class="content-box">
      <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'QR коды с зашитой REF ссылкой');?>
        <div class="font-size-11 float-right">
          <a href="#" title=""><i class="glyph-icon opacity-hover icon-star"></i></a>
        </div>
      </h3>
      <div class="content-box-wrapper text-center clearfix">
      	<img src="<?php echo Url::home(true);?>qrcodemain/<?php echo $user->login;?>">
	    <img src="<?php echo Url::home(true);?>qrcodereg/<?php echo $user->login;?>">
	      <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal48560']);?>
		<div class="modal fade modal48560">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title"><?php echo Yii::t('app', 'QR коды с зашитой REF ссылкой');?></h4>
		      </div>
		      <div class="modal-body">
				<p><textarea class = "form-control" id = "qrcodemain"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>qrcodemain/<?php echo $user->login;?>"></a></textarea></p>
		        <p><textarea class = "form-control" id = "qrcodereg"><a href = "<?php echo Url::home(true);?>signup?ref=<?php echo $user->login;?>"><img src="<?php echo Url::home(true);?>qrcodereg/<?php echo $user->login;?>"></a></textarea></p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
		      </div>
		    </div>
		  </div>
		</div>

	  </div>
	</div>
</div>
	
<?php $this->registerJs('new Clipboard(".btn-clipboard");');?>

