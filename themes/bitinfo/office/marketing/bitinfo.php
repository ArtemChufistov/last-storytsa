<div class="row">
  <div class="col-lg-8">
    <div id = "animationSandbox">
        <div class="content-box">
          <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Как это работает');?>
            <div class="font-size-11 float-right">
              <a href="#" title=""><i class="glyph-icon opacity-hover icon-star-o	"></i></a>
            </div>
          </h3>
          <div class="content-box-wrapper text-center clearfix">
            <p style = "text-align: justify;">
            	<?php echo Yii::t('app', '<img src = "/images/gallery-grid-condensed-2-640x420.jpg" style = "float: right; margin-left: 20px; width: 300px;">Современные технологии достигли такого уровня, что сложившаяся структура организации способствует повышению качества новых предложений. Внезапно, акционеры крупнейших компаний и по сей день остаются уделом либералов, которые жаждут быть подвергнуты целой серии независимых исследований. Кстати, сторонники тоталитаризма в науке являются только методом политического участия и объективно рассмотрены соответствующими инстанциями.<br/><br/>
            		Генерация рыбатекста происходит довольно просто: есть несколько фиксированных наборов фраз и словочетаний, из которых в определенном порядке формируются предложения. Предложения складываются в абзацы – и вы наслаждетесь очередным бредошедевром.

    Сама идея работы генератора заимствована у псевдосоветского "универсального кода речей", из которого мы выдернули используемые в нем словосочетания, запилили приличное количество собственных, в несколько раз усложнили алгоритм, добавив новые схемы сборки, – и оформили в виде быстрого и удобного сервиса для получения тестового контента.<br/><br/>
    Другое название – "универсальный генератор речей". По легенде, всякие депутаты и руководители в СССР использовали в своих выступлениях заготовленный набор совмещающихся между собой словосочетаний, что позволяло нести псевдоумную ахинею часами. Что-то вроде дорвеев для политсобраний
            	');?>
            </p>

            <button class="butt js--triggerAnimation btn btn-success btn-lg mrg20B"><?php echo Yii::t('app', 'Принять участие');?></button>
          </div>
        </div>
    </div>

    <div class="content-box">
      <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Ваша текущее место');?>
        <div class="font-size-11 float-right">
          <a href="#" title=""><i class="glyph-icon opacity-hover icon-cubes"></i></a>
        </div>
      </h3>
      <div class="content-box-wrapper text-center clearfix overflow-wrap">

          <?php foreach(array_reverse($rootMatrixes) as $num => $rootMatrixItem):?>
            <div class="chart tab-pane <?php if ($rootMatrixItem->slug == $rootMatrix->slug):?>active<?php endif;?>" id="<?php echo $rootMatrix->slug;?>">
                <?php if(!empty($currentPlace)):?>
                    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_matrix', [
                        'matrixCategory' => $matrixCategory,
                        'currentPlace' => $currentPlace,
                        'currentLevel' => $currentLevel,
                        'parentPlace' => $parentPlace,
                        'rootMatrix' => $rootMatrix,
                        'user' => $user,
                    ]);?>
                <?php else:?>
                    <h4 style = "text-align:center;margin-top:30px; margin-bottom: 30px;"><?php echo Yii::t('app', 'У вас еще нет места в программе');?></h4>
                <?php endif;?>
            </div>
          <?php endforeach;?>

      </div>
    </div>

  </div>
  <div class="col-lg-4">
    <?php if (!empty($parentUser)):?>
        <div class="box box-widget widget-user">
            <div class="widget-user-header bg-aqua-active">
                <h3 class="widget-user-username"><?php echo Yii::t('app', 'Ваш пригласитель')?></h3>
                <h5 class="widget-user-desc">
                    <?php if (empty($parentUser->first_name) || empty($parentUser->last_name)):?>
                        <?php echo $parentUser->login;?>
                    <?php else:?>
                        <?php echo $parentUser->first_name;?> <?php echo $parentUser->last_name;?>
                    <?php endif;?>
                </h5>
            </div>
            <div class="widget-user-image">
                <img class="img-circle" src="<?php echo $parentUser->getImage();?>" alt="<?php echo $parentUser->login;?>">
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-4 border-right">
                        <div class="description-block">
                            <h5 class="description-header"><?php echo $parentUser->email;?></h5>
                            <span class="description-text">E-mail</span>
                        </div>
                    </div>
                <div class="col-sm-4 border-right">
                    <div class="description-block">
                        <h5 class="description-header"><?php echo $parentUser->skype;?></h5>
                        <span class="description-text">Skype</span>
                    </div>
                </div>
                    <div class="col-sm-4">
                        <div class="description-block">
                            <h5 class="description-header"><?php echo $parentUser->phone;?></h5>
                            <span class="description-text"><?php echo Yii::t('app', 'Телефон')?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_matrix-places', [
        'matrixCategory' => $matrixCategory,
        'rootMatrixes' => $rootMatrixes,
        'user' => $user,
    ]);?>

  </div>
</div>
