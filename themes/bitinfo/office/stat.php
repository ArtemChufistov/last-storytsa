<?php
use app\modules\event\models\Event;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Статистика');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>
<div class = "row stat">
  <div class="col-sm-3">

    <div class="tile-box tile-box-alt mrg20B bg-green">
        <div class="tile-header">
            <?php echo Yii::t('app', 'Участников в структуре');?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-group"></i>
            <div class="tile-content">
                <?php echo $user->descendants()->count();?>
            </div>
            <small>
                <i class="glyph-icon icon-caret-up"></i>
                <?php echo Yii::t('app', 'Количество партнёров со всех уровней');?>
            </small>
        </div>
        <a href="/office/struct" class="tile-footer tooltip-button" data-placement="bottom" title="" data-original-title="<?php echo Yii::t('app', 'Посмотреть структуру');?>">
            <?php echo Yii::t('app', 'Посмотреть структуру');?>
            <i class="glyph-icon icon-arrow-right"></i>
        </a>
    </div>

    <div class="tile-box tile-box-alt mrg20B bg-orange">
        <div class="tile-header">
            <?php echo Yii::t('app', 'Личных партнёров');?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-pie-chart"></i>
            <div class="tile-content">
                <?php echo $user->children()->count();?>
            </div>
            <small>
                <i class="glyph-icon icon-caret-up"></i>
                <?php echo Yii::t('app', 'Количество личноприглашённых партнёров');?>
            </small>
        </div>
        <a href="/office/struct" class="tile-footer tooltip-button" data-placement="bottom" title="" data-original-title="<?php echo Yii::t('app', 'Посмотреть структуру');?>">
            <?php echo Yii::t('app', 'Посмотреть информацию');?>
            <i class="glyph-icon icon-arrow-right"></i>
        </a>
    </div>

    <div class="tile-box tile-box-alt mrg20B bg-blue-alt">
        <div class="tile-header">
            <?php echo Yii::t('app', 'Переходов по REF ссылке');?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-tags"></i>
            <div class="tile-content">
                <?php echo $user->getEvents()->where(['type' => Event::TYPE_REF_LINK])->count();?>
            </div>
            <small>
                <i class="glyph-icon icon-caret-up"></i>
                <?php echo Yii::t('app', 'Количество переходов по вашей REF ссылки');?>
            </small>
        </div>
        <a href="#" class="tile-footer tooltip-button" data-placement="bottom" title="" data-original-title="<?php echo Yii::t('app', 'Посмотреть структуру');?>">
            <?php echo Yii::t('app', 'Информация по событиям');?>
            <i class="glyph-icon icon-arrow-right"></i>
        </a>
    </div>

  </div>

  <div class = "col-sm-4">
      <div class="box box-success">

          <?php Pjax::begin(); ?>

          <?= GridView::widget([
              'dataProvider' => $eventProvider,
              'filterModel' => $eventModel,
              'layout' => '
                            <div class="box-header">
                              <h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i>' . Yii::t('app', 'События') . '</h3>
                              <div class="box-tools"><div class="pagination page-success pagination-sm no-margin pull-right">{pager}</div></div>
                            </div>
                           </div><div class="box-body no-padding eventTable">{items}</div>',
              'columns' => [
                  ['class' => 'yii\grid\SerialColumn'],
                  [
                      'attribute' => 'type',
                      'format' => 'html',
                      'filter' => false,
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-type', ['model' => $model]);},
                  ],
                  [
                      'attribute' => 'date_add',
                      'format' => 'html',
                      'filter' => false,
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-date', ['model' => $model]);},
                  ],
              ],
          ]); ?>
          <?php Pjax::end(); ?>
      </div>
  </div>
</div>