<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use lowbase\user\components\AuthChoice;
use app\modules\mainpage\models\Preference;

$this->title = Yii::t('app', 'Регистрация');
$this->params['breadcrumbs'][] = $this->title;
UserAsset::register($this);

?>

<div class="panel section-34 section-sm-41 inset-left-20 inset-right-20 inset-sm-left-20 inset-sm-right-20 inset-lg-left-30 inset-lg-right-30 bg-white shadow-drop-md">
  <span class="icon icon-circle icon-bordered icon-lg icon-default mdi mdi-account-multiple-outline"></span>
  <div>
    <div class="offset-top-24 text-darker big text-bold"><?= Html::encode($this->title) ?></div>
    <p class="text-extra-small text-dark offset-top-4"><?= Yii::t('app', 'Пожалуйста укажите информацию о себе');?></p>
  </div>

<?php echo $this->render('partial/_licenseAgreement');?>

    <?php $form = ActiveForm::begin([
        'id' => 'form-signup',
        'fieldConfig' => [
            'template' => "{input}\n{hint}\n{error}",
   
        ],
        'options' => [
            'data-form-output' => 'form-output-global',
            'data-form-type' => 'contact',
            'class' => 'text-left offset-top-30'
        ]
    ]); ?>

    <div class="form-group">

        <?php $readonlyPref = Preference::find()->where(['key' => Preference::KEY_USER_REG_CAN_PARENT_EDIT])->one()->value == 1 ? false : true;?>

        <?= $form->field($model, 'ref',
          ['template'=>'<div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-star-variant"></span></span>{input}</div>{error}']
          )->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('ref'),
            'readonly' => $readonlyPref,
            'id' => 'ref-user-name',
        ]); ?>
      
    </div>

    <div class="form-group-sign offset-top-30">

        <?= $form->field($model, 'login',
          ['template'=>'<div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-outline"></span></span>{input}</div>{error}']
          )->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('login'),
            'id' => 'login-user-name',
        ]); ?>

    </div>

    <div class="form-group-sign offset-top-20">

        <?= $form->field($model, 'email',
          ['template'=>'<div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-email-outline"></span></span>{input}</div>{error}']
          )->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('email'),
            'id' => 'email-user-name',
        ]); ?>

    </div>
    <div class="form-group-sign offset-top-20">

        <?= $form->field($model, 'password',
          ['template'=>'<div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-lock-open-outline"></span></span>{input}</div>{error}']
          )->passwordInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('password'),
        ]); ?>

    </div>
    <div class="form-group-sign offset-top-20">

        <?= $form->field($model, 'passwordRepeat',
          ['template'=>'<div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-lock-open-outline"></span></span>{input}</div>{error}']
          )->passwordInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('passwordRepeat'),
        ]); ?>

    </div>
    <div class="form-group-sign offset-top-20">

        <?php
        echo $form->field($model, 'captcha')->widget(Captcha::className(), [
            'imageOptions' => [
                'id' => 'my-captcha-image',
            ],
            'captchaAction' => '/profile/profile/captcha',
            'options' => [
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('captcha')
            ],
            'template' => '<div class="row">
            <div class="col-lg-8">{input}</div>
            <div class="col-lg-4">{image}</div>
            </div>',
        ]);
        ?>

    </div>
    <div class="form-group-sign offset-top-24">

        <?php $labelLicense = '<span class="checkbox-custom-dummy"></span><span class="text-dark text-extra-small">' . Yii::t('app', 'Я согласен с условиями') . ' ' . Html::a(Yii::t('app', 'лицензионного соглашения'), ['#'], [
              'data-toggle' => 'modal',
              'data-target' => '#pass',
              'class' => 'text-picton-blue'
          ]). '</span>';?>

        <?= $form->field($model, 'licenseAgree', ['template' => '<label class="checkbox-inline">{input}{error}</label>', 'options'=> ['tag' => 'div']])->checkBox(['label' => $labelLicense, 'class' => 'checkbox-custom']);?>

    </div>

    <div class="form-group-sign offset-top-26">
        <?= Html::submitButton('<i class="glyphicon glyphicon-user"></i> '.Yii::t('app', 'Зарегистрироваться'), [
            'class' => 'btn btn-xs btn-icon btn-block btn-primary offset-top-20 offset-bottom-10',
            ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="offset-top-28">
        <?php /*
        <br/>
        <span class="text-dark text-extra-small"><?= Yii::t('app', 'Зарегистрироваться с помощью социальных сетей')?>:</span>
        <br/>
        <br/>

        <div class="text-center" style="text-align: center">
              <?=AuthChoice::widget([
                'baseAuthUrl' => ['/profile/auth/index'],
                'clientCssClass' => 'col-xs-3',
              ])?>
        </div>
*/
  ?>
        <span class="text-dark text-extra-small">
            <?= Yii::t('app', 'Если регистрировались ранее, можете')?> <?=Html::a(Yii::t('app', 'войти на сайт'), ['/login'], ['class' => ['text-picton-blue']])?>,
            <?= Yii::t('app', 'используя Логин или E-mail')?>.
        </span>
    </div>

</div>


<style>
#my-captcha-image{
    cursor: pointer;
}