<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\bitinfo\OfficeHeadAsset;
use app\assets\bitinfo\OfficeHeadAssetBott;
use app\modules\menu\widgets\MenuWidget;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\widgets\ChangeEmailWidget;

OfficeHeadAsset::register($this);
OfficeHeadAssetBott::register($this);

?>

<?php $this->beginPage() ?>

<html lang="en">
<head>
    <?php $this->head() ?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta_office');?>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

</head>

  <body>
    <?php $this->beginBody() ?>
    <div id="sb-site">

      <div id="loading">
        <div class="spinner">
          <div class="bounce1"></div>
          <div class="bounce2"></div>
          <div class="bounce3"></div>
        </div>
      </div>

    <div id="page-wrapper">
      <div id="page-header" class="bg-gradient-9">
        <div id="mobile-navigation">
            <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
            <a href="/" class="logo-content-small" title="BitInfo"></a>
        </div>
        <div id="header-logo" class="logo-bg">
            <a href="/" class="logo-content-big" title="BitInfo">
                BitInfo <i>UI</i>
            </a>
            <a href="/" class="logo-content-small" title="BitInfo">
                BitInfo <i>UI</i>
            </a>
            <a id="close-sidebar" href="#" title="Close sidebar">
                <i class="glyph-icon icon-angle-left"></i>
            </a>
        </div>
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/header-nav-left', ['user' => $this->params['user']]);?>

        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/header-nav-right', ['user' => $this->params['user']]);?>

      </div>
      <?php echo MenuWidget::widget(['menuName' => 'profileMenu', 'view' => 'office-menu']);?>
          <div id="page-content-wrapper">
              <div id="page-content">
                <div class="container">
                  <?php echo $content;?>
                </div>
              </div>
          </div>
        </div>
    </div>

    <?= ChangeEmailWidget::widget(['user' => $this->params['user']->identity]) ?>
    
    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>