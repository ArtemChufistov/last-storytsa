<?php
use app\modules\menu\widgets\MenuWidget;
use app\modules\news\widgets\LastNewsWidget;
use app\modules\profile\widgets\SubscribeWidget;
?>

<div class="main-footer bg-gradient-4 clearfix">
  <div class="container clearfix">
    <div class="logo" style = "margin-bottom: 20px;">&copy; <?= Yii::t('app', 'Citt.fund - все права защищены');?></div>
  </div>
</div>
