<?php
use yii\helpers\Url;
?>
<div id="header-nav-left">
    <div class="user-account-btn dropdown">
        <a href="#" title="<?php echo Yii::t('app', 'Мой аккаунт');?>" class="user-profile clearfix" data-toggle="dropdown">
            <img width="28" src="<?php echo $user->identity->getImage();?>" alt="Profile image">
            <span><?= $user->identity->login;?></span>
            <?php foreach ($this->params['user']->identity->getBalances() as $balance): ?>
                <span>
                    <?php echo $balance->showValue(); ?> <?php echo $balance->getCurrency()->one()->title; ?>
                </span>
            <?php endforeach;?>
            <i class="glyph-icon icon-angle-down"></i>
        </a>
        <div class="dropdown-menu float-left">
            <div class="box-sm">
                <div class="login-box clearfix">
                    <div class="user-img">
                        <a href="<?php echo Url::to(['/profile/office/user']);?>" title="<?= $user->identity->login;?>" class="change-img"><?php echo Yii::t('app', 'Сменить аватар');?></a>
                        <img src="<?php echo $user->identity->getImage();?>" alt="">
                    </div>
                    <div class="user-info">
                        <span>
                            <?= $user->identity->email;?>
                            <i><?php echo Yii::t('app', 'Участник проекта');?></i>
                        </span>
                        <a href="<?= Url::to(['/profile/office/user']);?>" title="<?php echo Yii::t('app', 'Профиль');?>"><?php echo Yii::t('app', 'Профиль');?></a>
                        <a href="<?= Url::to(['/profile/office/struct']);?>" title="<?php echo Yii::t('app', 'Структура');?>"><?php echo Yii::t('app', 'Структура');?></a>
                    </div>
                </div>
                <div class="divider"></div>
                <ul class="reset-ul mrg5B">
                    <li>
                        <a href="<?= Url::to(['/profile/office/finance']);?>">
                            <i class="glyph-icon float-right icon-caret-right"></i>
                            <?php echo Yii::t('app', 'Баланс:');?>
                        </a>
                    </li>
                  <?php foreach ($this->params['user']->identity->getBalances() as $balance): ?>
                    <li>
                        <a href="<?php echo Url::to(['office/in']); ?>">
                            <i class="glyph-icon float-right icon-caret-right"></i>
                            <?php echo $balance->showValue(); ?> <?php echo $balance->getCurrency()->one()->title; ?>
                        </a>
                    </li>
                  <?php endforeach;?>
                </ul>
                <div class="pad5A button-pane button-pane-alt text-center">
                    <a href="/logout" class="btn display-block font-normal btn-danger">
                        <i class="glyph-icon icon-power-off"></i>
                        <?php echo Yii::t('app', 'Выход');?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>