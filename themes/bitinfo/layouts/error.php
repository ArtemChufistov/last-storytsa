<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'] = [[
	'label' => $message,
	'url' => '/404',
],
];

$this->title = $message;
?>

<div class="page text-center">
  <!-- Page Content-->
  <main style="background: #191919 url(/images/backgrounds/background-16-1920x955.jpg) 0/cover no-repeat" class="page-content">
    <div class="one-page">
      <div class="one-page-header">
        <!--Navbar Brand-->
        <div class="rd-navbar-brand"><a href="index.html"><img style='margin-top: -5px;margin-left: -15px;' width='138' height='31' src='images/intense/logo-light.png' alt=''/></a></div>
      </div>
      <!-- 404-->
      <section class="context-dark">
        <div class="shell">
          <div class="range">
            <div class="section-110 section-cover range range-xs-center range-xs-middle">
              <div class="cell-lg-6">
                <h3><?=$message;?></h3>
                <div class="group offset-top-30"><a href="/" class="btn btn-icon btn-icon-left btn-primary"><span class="icon mdi mdi-arrow-left"></span><?php echo Yii::t('app', 'Вернуться на главную'); ?></a><a href="<?php echo Url::to(['profile/profile/login']);?>" class="btn btn-icon btn-icon-left btn-default"><span class="icon mdi mdi-user-outline"></span><?php echo Yii::t('app', 'Личный кабинет'); ?></a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </main>
</div>
<div class="hero-box hero-box-smaller poly-bg-2 font-inverse" data-top-bottom="background-position: 50% 0px;" data-bottom-top="background-position: 50% -600px;">
    <div class="container" style = "margin-top: 170px;">
        <h1 class="hero-heading wow fadeInDown" data-wow-duration="0.6s"><?php echo $this->title; ?></h1>
        <p class="hero-text wow bounceInUp" data-wow-duration="0.9s" data-wow-delay="0.2s"><?php echo Yii::t('app', 'Ошибка'); ?></p>
    </div>
    <div class="hero-overlay bg-black"></div>
</div>

<style type="text/css">
    html,body {
        height: 100%;
    }
    body {
        background: #fff;
        overflow: hidden;
    }

</style>
