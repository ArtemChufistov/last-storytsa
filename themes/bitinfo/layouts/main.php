<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\bitinfo\FrontAppAssetTop;
use app\assets\bitinfo\FrontAssetBottom;
use app\modules\menu\widgets\MenuWidget;

FrontAppAssetTop::register($this);
FrontAssetBottom::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
  <head>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

  </head>
  <body>

  <?php $this->beginBody() ?>

    <div class="page text-center">
      <div class="page-loader page-loader-variant-1">
        <div><img class='img-responsive' style='margin-top: -20px;margin-left: -18px;' width='330' height='67' src='/bitinfo/logo-light-big.png' alt=''/>
          <div class="offset-top-41 text-center">
            <div class="spinner"></div>
          </div>
        </div>
      </div>

      <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']);?>
      <?php echo $content;?>
      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/_footer');?>

  <?php $this->endBody() ?>

  </body>
</html>
<?php $this->endPage() ?>