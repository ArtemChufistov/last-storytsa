<?php
use yii\web\View;
use yii\helpers\Url;
use app\modules\lang\widgets\WLang;
?>

<!-- <li><a href="<?php echo Url::to(['/profile/profile/login']);?>" class="text-uppercase text-ubold"><small><?php echo Yii::t('app', 'Личный кабинет');?></small></a></li>
<li><a href="<?php echo Url::to(['/profile/profile/signup']);?>" class="text-uppercase text-ubold"><small><?php echo Yii::t('app', 'Регистрация');?></small></a></li> -->

<!-- RD Navbar Transparent-->
<div class="rd-navbar-wrap">
  <nav data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" class="rd-navbar rd-navbar-default rd-navbar-transparent" data-lg-auto-height="true" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
    <div class="rd-navbar-inner">
      <!-- RD Navbar Panel-->
      <div class="rd-navbar-panel">
        <!-- RD Navbar Toggle-->
        <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
        <!--Navbar Brand-->
        <div class="rd-navbar-brand"><a href="/"><img style='margin-top: -5px;margin-left: -15px;' width='138' height='31' src='/images/childs/restaurant-logo-light.png' alt=''/></a></div>
      </div>
      <div class="rd-navbar-menu-wrap">
        <div class="rd-navbar-nav-wrap">
          <div class="rd-navbar-mobile-scroll">
            <!--Navbar Brand Mobile-->
            <div class="rd-navbar-mobile-brand"><a href="/"><img style='margin-top: -5px;margin-left: -15px;' width='138' height='31' src='/images/childs/restaurant-logo-light.png' alt=''/></a></div>
            <div class="form-search-wrap">
              <!-- RD Search Form-->
              <form action="search-results.html" method="GET" class="form-search rd-search">
                <div class="form-group">
                  <label for="rd-navbar-form-search-widget" class="form-label form-search-label form-label-sm">Search</label>
                  <input id="rd-navbar-form-search-widget" type="text" name="s" autocomplete="off" class="form-search-input input-sm form-control form-control-gray-lightest input-sm"/>
                </div>
                <button type="submit" class="form-search-submit"><span class="mdi mdi-magnify"></span></button>
              </form>
            </div>
            <!-- RD Navbar Nav-->
            <ul class="rd-navbar-nav">
              <?php foreach($menu->children()->all() as $num => $children):?>
                <li class="<?php if($requestUrl == $children->link):?>active<?php endif;?>" ><a href="<?= Url::to([$children->link]);?>"><span><?= $children->title;?></span></a>
                  <?php if (!empty($children->children()->all())):?>
                    <ul class="rd-navbar-dropdown">
                      <?php foreach($children->children()->all() as $numChild => $childChildren):?>
                        <li><a href="<?= Url::to([$childChildren->link]);?>"><span class="text-middle"><?= $childChildren->title;?></span></a>
                        </li>
                      <?php endforeach;?>
                    </ul>
                  <?php endif;?>
                </li>
              <?php endforeach;?>
              <?= WLang::widget();?>
            </ul>
          </div>
        </div>

      </div>
    </div>
  </nav>
</div>
