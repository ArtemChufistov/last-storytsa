<div class="cell-xs-12 offset-top-66 cell-lg-3 cell-lg-push-1 offset-lg-top-0">
  <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_logo_light_1');?>
  <p class="text-darker offset-top-4"><?php echo \Yii::t('app', 'Мы в социальных сетях');?></p>
  <ul class="list-inline">
    <li><a href="https://www.facebook.com/groups/718628941639184" class="icon fa fa-facebook icon-xxs icon-circle icon-darkest-filled"></a></li>
    <li><a href="https://vk.com/club141094511" class="icon fa fa-vk icon-xxs icon-circle icon-darkest-filled"></a></li>
    <li><a href="#" class="icon fa fa-twitter icon-xxs icon-circle icon-darkest-filled"></a></li>

  </ul>
</div>
