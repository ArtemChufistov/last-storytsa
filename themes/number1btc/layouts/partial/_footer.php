<?php
use app\modules\menu\widgets\MenuWidget;
use app\modules\news\widgets\LastNewsWidget;
use app\modules\profile\widgets\SubscribeWidget;
?>

<footer class="section-relative section-top-66 section-bottom-34 page-footer bg-gray-base context-dark">
  <div class="shell">
    <div class="range range-sm-center text-lg-left">
      <div class="cell-sm-8 cell-md-12">
        <div class="range range-xs-center">
          <?php echo LastNewsWidget::widget();?>

          <?php echo SubscribeWidget::widget();?>
          <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_soc-icon-footer');?>
        </div>
      </div>
    </div>
  </div>
  <div class="shell offset-top-50">
    <p class="small text-darker">&copy; <?php echo Yii::t('app', 'Bitcoin Express');?>
      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_yandex_metrika-footer');?>
    </p>
  </div>
</footer>
