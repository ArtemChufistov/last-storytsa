<?php
use yii\helpers\Html;
?>
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <?php echo Yii::t('app', 'Язык');?>: <?= $current->name;?>
    </a>
    <ul class="dropdown-menu" style = "width: 100px; height: 47px;">
      <li>
        <ul class="menu">
          <?php foreach ($langs as $lang):?>
	        <li>
	            <?= Html::a($lang->name, '/'.$lang->url.Yii::$app->getRequest()->getLangUrl()); ?>
	        </li>
          <?php endforeach;?>
        </ul>
      </li>
    </ul>
 </li>