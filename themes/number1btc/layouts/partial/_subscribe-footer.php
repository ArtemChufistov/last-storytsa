<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

?>

<div class="cell-xs-12 offset-top-41 cell-md-5 offset-md-top-0 text-md-left cell-lg-4 cell-lg-push-2">
  <h6 class="text-uppercase text-spacing-60"><?php echo Yii::t('app', 'Подписка на новости');?></h6>
  <p><?php echo Yii::t('app', 'Чтобы всегда быть в курсе событий Вы можете подписаться на новости, достаточно только указать свой E-mail');?></p>
  <div class="offset-top-30">
    <?php $form = ActiveForm::begin([
      'options' => [

          'data-form-type' => 'subscribe',
          'data-form-output' => 'form-subscribe-footer',
          'enctype'=>'multipart/form-data'
      ],
    ]); ?>

        <?= $form->field($subscriber, 'mail', ['template' => '<div class="form-group"><div class="input-group input-group-sm"><span class="input-group-addon"><span class="input-group-icon mdi mdi-email"></span></span>{input}<span class="input-group-btn"><button type="submit" class="btn btn-sm btn-primary">' . Yii::t('app', 'Подписаться') .'</button></span></div>{error}{hint}'])->textInput([
            'maxlength' => true,
            'placeholder' => Yii::t('app', 'Введите свой E-mail')
        ])->label(''); ?>
      </div>
      <div id="form-subscribe-footer" class="form-output"></div>
    <?php ActiveForm::end();?>
  </div>
</div>

<?php if(Yii::$app->session->getFlash('succsessSubscribe')): ?>

  <?php Modal::begin([
      'header' => '<h1 class="text-center">' . Yii::t('app', 'Поздравляем!') . '</h1>',
      'toggleButton' => false,
      'id' => 'successSubscribe'
  ]);
  ?>
  <?php echo Yii::$app->session->getFlash('succsessSubscribe');?>

<?php Modal::end();?>

<?php $this->registerJs("
  $('#successSubscribe').modal('show');
", View::POS_END);?>

<?php endif;?>
