<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

Modal::begin([
    'header' => '<h1 class="text-center">' . Yii::t('app', 'Ошибка при регистрации') . '</h1>',
    'toggleButton' => false,
    'id' => 'error'
]);
?>
    <p class="hint-block">
        <?php echo $error;?>
    </p>
<?php Modal::end();?>

<script>
<?php $this->registerJs("
  $('#error').modal('show');
", View::POS_END);?>
</script>