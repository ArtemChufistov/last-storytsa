<?php

use app\modules\matrix\components\StorytsaMarketing;
use app\modules\profile\widgets\UserTreeElementWidget;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>

<p><?php echo Yii::t('app', '<p style = "text-align: justify;">Платформа Bitcoin-Express позволяет обмениваться денежными средствами на добровольных началах физическим и юридическим лицам. Каждый участник платформы получает возможность жертвовать разные суммы другим участникам, присоединившимся к платформе раньше него и стоящим от одного до трёх уровней выше. Каждый участник также получает возможность получать пожертвования от других участников, присоединившихся позже него и стоящих на трёх линиях ниже.</br></br>

Для открытия первого уровня Вы отправляете пожертвование тому, кто еще не получил на этом уровне два возможных пожертвования. Пожертвование на первом уровне имеет фиксированную сумму в размере 0.03 биткоина, которую Вы отправляете из своего личного биткоин кошелька на кошелек участника, который система показывает Вам в кабинете в момент оплаты. </br></br>

Так после того как Вы отправили свое первое пожертвование и имеете 1 открытый уровень, для Вас откроются все остальные преимущества системы.</p>');?></p>
<?php $form = ActiveForm::begin(); ?>
	<?= Html::submitButton('<i class="fa fa-user-plus" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
	  'class' => 'btn btn-block btn-success pull-left',
	  'value' => 1,
	  'name' => 'marketing-matrix']) ?>
<?php ActiveForm::end();?>