<?php
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\payment\models\Payment;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$matrixPlace = $user->getMatrixPlaces()->one();
$matrixGift = $matrixPlace->getMatrixGift()->one();

$paymentLevels = [];
for ($i = 1; $i <= 6; $i++) { 
	$paymentLevels[$i] = Payment::find()
	->where(['type' => Payment::TYPE_BUY_LEVEL, 'additional_info' => $i, 'status' => Payment::STATUS_OK, 'from_user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();
}

$paymentMatrix = Payment::find()->where(['type' => Payment::TYPE_BUY_PLACE, 'status' => Payment::STATUS_OK, 'from_user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();


$day30 = 60 * 60 * 24 * 30;

$levelLinkPath  = '@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_level-link';
$nolevelLinkPath  = '@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_level-link-no-active';

?>

<div class = "row">
	  <div class = "col-sm-12 text-trancpancy">
    <?php echo Yii::t('app', 'Мы разработали для Вас универсальную и эффективную стратегию, которая позволит Вам в кратчайшие сроки добиться наилучших результатов.', [

  ]);?>
	  </div>
 </div>
</br>
</br>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center">
		<?php echo Yii::t('app', 'Стоимость</br> уровня');?>
	</div>
	<div class ="col-sm-2 text-center">
		<?php echo Yii::t('app', 'Приглашено</br> участников');?>
	</div>
	<div class ="col-sm-2 text-center">
		<?php echo Yii::t('app', 'Прибыль</br> составит');?>
	</div>
</div>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center color-big-red">
		<?php echo Yii::t('app', '0.03 BTC');?>
	</div>
	<div class ="col-sm-2">
		<input type="text" value="" class="slider1 form-control" data-slider-min="0" data-slider-value="2" data-slider-max="2" data-slider-step="1" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
	</div>
	<div class ="col-sm-2 text-center color-big-red">
		<span class = "slidevrValue1"><?php echo Yii::t('app', '0.06');?></span> <span>BTC</span>
	</div>
	<?php if (count($matrixPlace->children()->all()) == 0):?>
		<div class ="col-sm-3">
			<?php echo Yii::$app->controller->renderPartial($nolevelLinkPath, ['matrixGift' => $matrixGift, 'level' => 1]);?>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', '1 линия не открыта');?>
		</div>
	<?php elseif (!empty($matrixGift) && $matrixGift->buy_level_1 > $matrixGift->buy_level_3 ):?>
		<div class ="col-sm-3">
			<?php echo Yii::$app->controller->renderPartial($nolevelLinkPath, ['matrixGift' => $matrixGift, 'level' => 1]);?>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Необходима покупка уровня 3');?>
		</div>
	<?php else:?>
		<div class ="col-sm-3">
			<?php echo Yii::$app->controller->renderPartial($levelLinkPath, ['matrixGift' => $matrixGift, 'level' => 1]);?>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Купить уровень');?>
		</div>
	<?php endif;?>
</div>
</br>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center color-big-red">
		<?php echo Yii::t('app', '0.05 BTC');?>
	</div>
	<div class ="col-sm-2">
		<input type="text" value="" class="slider2 form-control" data-slider-min="0" data-slider-value="4" data-slider-max="4" data-slider-step="1"  data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
	</div>
	<div class ="col-sm-2 text-center color-big-red">
		<span class = "slidevrValue2"><?php echo Yii::t('app', '0.2');?></span> <span>BTC</span>
	</div>
	<?php if (count($matrixPlace->descendantsForLevel(2)) == 0):?>
		<div class ="col-sm-3">
			<?php echo Yii::$app->controller->renderPartial($nolevelLinkPath, ['matrixGift' => $matrixGift, 'level' => 2]);?>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', '2 линия не открыта');?>
		</div>
	<?php elseif ($matrixGift->buy_level_2 >= $matrixGift->buy_level_1):?>
		<div class ="col-sm-3">
			<?php echo Yii::$app->controller->renderPartial($nolevelLinkPath, ['matrixGift' => $matrixGift, 'level' => 2]);?>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Необходима покупка уровня 1');?>
		</div>
	<?php else:?>
		<div class ="col-sm-3">
			<?php echo Yii::$app->controller->renderPartial($levelLinkPath, ['matrixGift' => $matrixGift, 'level' => 2]);?>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Купить уровень');?>
		</div>
	<?php endif;?>
</div>
</br>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center color-big-red">
		<?php echo Yii::t('app', '0.15 BTC');?>
	</div>
	<div class ="col-sm-2">
		<input type="text" value="" class="slider3 form-control" data-slider-min="0" data-slider-value="8" data-slider-max="8" data-slider-step="1" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
	</div>
	<div class ="col-sm-2 text-center color-big-red">
		<span class = "slidevrValue3"><?php echo Yii::t('app', '1.2');?></span> <span>BTC</span>
	</div>
	<?php if (count($matrixPlace->descendantsForLevel(3)) == 0):?>
		<div class ="col-sm-3">
			<?php echo Yii::$app->controller->renderPartial($nolevelLinkPath, ['matrixGift' => $matrixGift, 'level' => 3]);?>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', '3 линия не открыта');?>
		</div>
	<?php elseif ($matrixGift->buy_level_3 >= $matrixGift->buy_level_2):?>
		<div class ="col-sm-3">
			<?php echo Yii::$app->controller->renderPartial($nolevelLinkPath, ['matrixGift' => $matrixGift, 'level' => 3]);?>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Необходима покупка уровня 2');?>
		</div>
	<?php else:?>
		<div class ="col-sm-3">
			<?php echo Yii::$app->controller->renderPartial($levelLinkPath, ['matrixGift' => $matrixGift, 'level' => 3]);?>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Купить уровень');?>
		</div>
	<?php endif;?>
</div>

<?php $this->registerJs("
	$(function () {
		$(function () { $(\"[data-toggle = 'tooltip']\").tooltip(); });
		\$('.slider1').slider({
			formatter: function(value) {
				var btcvalue = 0.03 * value;
				jQuery('.slidevrValue1').html(btcvalue);
				return 'Текущее значение: ' + value;
			}
		});
		\$('.slider2').slider({
			formatter: function(value) {
				var btcvalue = 0.05 * value;
				jQuery('.slidevrValue2').html(btcvalue.toFixed(2));
				return 'Текущее значение: ' + value;
			}
		});
		\$('.slider3').slider({
			formatter: function(value) {
				var btcvalue = 0.15 * value;
				jQuery('.slidevrValue3').html(btcvalue.toFixed(2));
				return 'Текущее значение: ' + value;
			}
		});
	});
", View::POS_END);?>