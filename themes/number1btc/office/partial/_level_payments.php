<?php
$matrixPlace = $user->getMatrixPlaces()->one();
$matrixGift = $matrixPlace->getMatrixGift()->one();
?>
<div class="nav-tabs-custom">
  <!-- Tabs within a box -->
  <ul class="nav nav-tabs pull-right">
    <li class="pull-left header"><i class="ion ion-android-favorite"></i><?php echo Yii::t('app', 'Покупки уровней')?></li>
    <?php for($i = 3; $i >= 1; $i--){?>
      <?php $buy_level_num = 'buy_level_' . $i;?>
      <li <?php if ($i == 1):?>class="active"<?php endif;?>><a href="#buyLevel<?php echo $i;?>" data-toggle="tab"><?php echo Yii::t('app', 'Уровень {num} ({count})', [
        'num' => $i, 'count' => $matrixGift->$buy_level_num]);?></a></li>
    <?php }?>
  </ul>
  <div class="tab-content no-padding">
    <?php for($i = 3; $i >= 1; $i--){?>
      <div class="chart tab-pane <?php if ($i == 1):?>active<?php endif;?>" id="buyLevel<?php echo $i;?>" >

        <div class="box-body no-padding">
          <ul class="users-list clearfix">
            <h3 style = "text-align: center; margin-top: 20px; margin-bottom: 20px;"><?php echo Yii::t('app', 'На этом уровне у вас пока нет приглашённых');?></h3>
          </ul>
        </div>

      </div>
    <?php }?>
  </div>
</div>