<?php
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixGift;

$matrix = Matrix::find()->where(['user_id' => $user->id])->one();
$matrixGift = MatrixGift::find()->where(['matrix_id' => $matrix->id])->one();
?>
<li>
	<img style = "width: 110px;height:110px;" src="<?php echo !empty($user) ? $user->getImage(): ''; ?>" alt="<?php echo $user->login; ?>">
	<span class="users-list-name" href="#"><?php echo $user->login; ?></span>
	<span class="users-list-date" style = "margin-top:5px; margin-bottom:5px;">
		<span class="label label-success" >
			<?php echo Yii::t('app', 'Оплаченые уровни');?>
			<?php for($i = 1; $i <= 6; $i++){?>
				<?php $buyLevelNum = 'buy_level_' . $i; ?>
				<?php echo $matrixGift->$buyLevelNum > 0 ? $i : '';?>
			<?php };?>
		</span>
	</span>
	<span class="users-list-date">E-mail: <?php echo $user->email;?></span>
	<span class="users-list-date"><?php echo Yii::t('app', 'Телефон');?>: <?php echo $user->phone == '' ? Yii::t('app', 'Не указан') : $user->phone;?></span>
	<span class="users-list-date">Skype: <?php echo $user->skype == '' ? Yii::t('app', 'Не указан') : $user->skype;?></span>
</li>