<?php $matrixGift = $user->getMatrixPlaces()->one()->getMatrixGift()->one();?>
<?php if (!empty($matrixGift)):?>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo Yii::t('app', 'Ваши уровни');?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <ul class="products-list product-list-in-box">
        <?php for($i = 1; $i <= 3; $i++){?>
          <?php $buyLevelNum = 'buy_level_' . $i; ?>
          <li class="item">
            <div class="product-img">
              <?php echo $i;?>
            </div>
            <div class="product-info">
              <a href="" class="product-title"><?php echo Yii::t('app', 'уровень')?>
                <span class="label label-warning pull-right"><?php echo $matrixGift->$buyLevelNum;?></span>
              </a>
              <span class="product-description">
                <?php echo Yii::t('app', 'количество покупок данного уровня');?>
              </span>
            </div>
          </li>
        <?php };?>
      </ul>
    </div>
    <div class="box-footer text-center">
      <a target = "_blank" href="/howitworks" class="uppercase"><?php echo Yii::t('app', 'Прочитать маркетинг')?></a>
    </div>
  </div>

  <style>
  .product-img{
    background-color: #3c8dbc;
    color: white;
    font-size: 20px;
    height: 40px;
    padding-top: 5px;
    text-align: center;
    width: 40px;
  }
  </style>
<?php endif;?>