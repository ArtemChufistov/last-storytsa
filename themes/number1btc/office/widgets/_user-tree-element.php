<span class="person tooltips" >

	<?php if (!empty($user)):?>
		<div class="box box-widget widget-user-2">
			<div class="widget-user-header bg-aqua">
				<div class="widget-user-image">
					<img class="img-circle imgAvatar" href = "/profile/office/bitcoinexpress<?php echo !empty($startPlace) ? '/'. $startPlace->slug : '';?><?php echo !empty($currentPlace) ? '/' . $currentPlace->slug : '';?>" src="<?php echo $user->getImage();?>" alt="<?php echo $user->login;?>">
				</div>
				<h3 class="widget-user-username"><?php if(empty($user->first_name) || empty($user->last_name)){echo $user->login;}else{ echo $user->first_name . ' ' . $user->last_name;}?></h3>
				<h5 class="widget-user-desc"><?php echo  $user->email;?></h5>
			</div>
			<div class="box-footer no-padding">
				<ul class="nav nav-stacked">
					<li>
					<?php if (!empty($user->skype)):?>
						<span class="pull-left badge bg-aqua"><?php echo Yii::t('app', 'Skype');?>: <?php echo  $user->skype;?></span> 
					<?php endif;?> 
						<span class="pull-left badge bg-aqua"><?php echo Yii::t('app', 'Логин');?>: <?php echo  $user->login;?></span> 
					<?php if (!empty($user->phone)):?>
						<span class="pull-left badge bg-aqua"><?php echo Yii::t('app', 'Телефон');?>: <?php echo  $user->phone;?></span>
					<?php endif;?>
					</li>
				</ul>
			</div>
		</div>
	<?php endif;?>

	<?php if (!empty($user)):?>
    	<img src="<?php echo $user->getImage(); ?>" alt="" class = "imgAvatar" href = "/profile/office/bitcoinexpress<?php echo !empty($startPlace) ? '/'. $startPlace->slug : '';?><?php echo !empty($currentPlace) ? '/' . $currentPlace->slug : '';?>">
	<?php else:?>
    	<img src="/images/mr-question.jpg" alt="">
	<?php endif;?>

    <p class="name">
    	<?php echo !empty($user) ? $user->login . '</br><b> ' . $user->email . '</b>' : Yii::t('app', 'Свободное</br> <b>место</b>');?>
    </p>
</span>
