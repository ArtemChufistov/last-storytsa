<?php

use app\modules\matrix\components\StorytsaMarketing;
use app\modules\profile\widgets\UserTreeElementWidget;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Маркетинг');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<?php if(!empty($fromUserPayment)): ?>
	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_from-user-payment', ['payment' => $fromUserPayment]);?>
<?php endif;?>

<?php if(!empty($toUserPayment)): ?>
	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_to-user-payment', ['payment' => $toUserPayment]);?>
<?php endif;?>

<?php if(Yii::$app->session->getFlash('message')): ?>
  <div class="modalMessage modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('message');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalMessage').modal('show');
", View::POS_END);?>

<?php endif;?>

<div class="row">

<div class="col-md-8">

<div class="box box-danger">
	<div class="box-header with-border">
	  <h3 class="box-title"><i class="fa fa-fire" aria-hidden="true"></i></i><?php echo Yii::t('app', 'Информация');?></h3>
	</div>
	<div class="box-body gold-background">
		<?php if (!empty($buttons[StorytsaMarketing::BUTTON_BUY_PLACE])):?>
			<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_payment-buy-place', ['user' => $user]);?>
	    <?php else:?>
	    	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_payment-buy-level', ['user' => $user]);?>
	    <?php endif;?>
	</div>
</div>

<?php if(!empty($currentPlace)):?>
	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_matrix', ['currentPlace' => $currentPlace, 'startPlace' => $startPlace, 'currentLevel' => $currentLevel]);?>
<?php endif;?>

<?php if(!empty($currentPlace)):?>
	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_matrix-user-by-levels', ['startPlace' => $startPlace]);?>
<?php endif;?>
</div>

<div class="col-md-4">

<?php if (empty($user->getPayWallet($paySystem->id, $currency->id))):?>
	<div class="box box-danger">
		<div class="box-header with-border">
		  <h3 class="box-title"><i class="fa fa-bell-o" aria-hidden="true"></i><?php echo Yii::t('app', 'Внимание!')?></h3>
		</div>
		<div class="box-body">
			<?php echo Yii::t('app', '<strong>Для того чтобы принять участие необходимо указать свой кошелек BitCoin в <a href = "/profile/office/walletdetail">Платёжных реквизитах.</a></strong>');?>
		</div>
	</div>
<?php endif;?>

<?php if (!empty($parentUser)):?>
	<div class="box box-widget widget-user">
		<div class="widget-user-header bg-aqua-active">
			<h3 class="widget-user-username"><?php echo Yii::t('app', 'Ваш пригласитель')?></h3>
			<h5 class="widget-user-desc">
				<?php if (empty($parentUser->first_name) || empty($parentUser->last_name)):?>
					<?php echo $parentUser->login;?>
				<?php else:?>
					<?php echo $parentUser->first_name;?> <?php echo $parentUser->last_name;?>
				<?php endif;?>
			</h5>
		</div>
		<div class="widget-user-image">
			<img class="img-circle" src="<?php echo $parentUser->getImage();?>" alt="User Avatar">
		</div>
		<div class="box-footer">
			<div class="row">
				<div class="col-sm-4 border-right">
					<div class="description-block">
						<h5 class="description-header"><?php echo $parentUser->email;?></h5>
						<span class="description-text">E-mail</span>
					</div>
				</div>
			<div class="col-sm-4 border-right">
				<div class="description-block">
					<h5 class="description-header"><?php echo $parentUser->skype;?></h5>
					<span class="description-text">Skype</span>
				</div>
			</div>
				<div class="col-sm-4">
					<div class="description-block">
						<h5 class="description-header"><?php echo $parentUser->phone;?></h5>
						<span class="description-text"><?php echo Yii::t('app', 'Телефон')?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif;?>

<?php if (!empty($fromUserPayments)):?>
	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_from-user-payments', ['fromUserPayments' => $fromUserPayments]);?>
<?php endif;?>

<?php if (!empty($toUserPayments)):?>
	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_to-user-payments', ['toUserPayments' => $toUserPayments]);?>
<?php endif;?>

<?php if (!empty($startPlace)):?>
	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_buy-levels', ['user' => $user]);?>
<?php endif;?>
</div>

</div>