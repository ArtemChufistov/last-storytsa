<?php
use yii\web\View;
use yii\helpers\Url;
use app\modules\profile\models\User;
use app\modules\payment\models\Payment;

$this->title = Yii::t('app', $model->title);
?>

<div data-on="false" data-md-on="true" class="bg-gray-base context-dark rd-parallax">
  <div data-speed="0.45" data-type="media" data-url="/bitexp/mainslide1.jpg" class="rd-parallax-layer"></div>
  <div data-speed="0.3" data-type="html" data-md-fade="true" class="rd-parallax-layer">
    <div class="bg-overlay-gray-darkest">
      <div class="shell">
        <div class="range">
          <div class="section-110 section-cover range range-xs-center range-xs-middle">
            <div class="cell-lg-12">
              <h1><span data-caption-animate="fadeInUp" data-caption-delay="700" class="text-spacing-60 big text-bold text-uppercase"><?php echo Yii::t('app', 'BITCOIN EXPRESS');?></span></h1>
              <h4 data-caption-animate="fadeInUp" data-caption-delay="900" class="offset-top-34 hidden reveal-sm-block text-light">
                <?php echo Yii::t('app', 'Присоединившись к нашей сети каждый сможет улучшить уровень своей жизни быстро и эффективно.');?>
              </h4>
              <div class="group group-xl offset-top-20 offset-xs-top-50">
                <a href="<?php echo Url::to(['/login']);?>" data-caption-animate="fadeInUp" data-caption-delay="1200" class="btn btn-primary btn-lg btn-anis-effect"><span class="btn-text"><?php echo Yii::t('app', 'Личный кабинет');?></span></a>
                <a href="<?php echo Url::to(['/signup']);?>" data-custom-scroll-to="home-section-welcome" data-caption-animate="fadeInUp" data-caption-delay="1200" class="btn btn-default btn-lg btn-anis-effect"><span class="btn-text"><?php echo Yii::t('app', 'Регистрация');?></span></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</header>

<main class="page-content">
  <section id="home-section-welcome" class="section-top-98 section-sm-top-110 section-sm-bottom-110 section-lg-top-66 section-bottom-98 section-lg-bottom-0">
    <div class="shell">
      <div class="range range-sm-center range-md-middle">
        <div class="cell-lg-5 veil reveal-lg-inline-block"><img width="470" height="540" src="images/childs/restaurant-01-470x540.png" alt="" class="img-responsive center-block"></div>
        <div class="cell-sm-10 cell-lg-preffix-1 cell-lg-5 section-lg-bottom-66 text-lg-left">
          <h1 class="text-bold"><?php echo Yii::t('app', 'Присоединяйтесь к Bitcoin Express, чтобы  изменить  что-то для себя и для других.');?></h1>
          <hr class="divider bg-mantis hr-lg-left-0">
          <div class="offset-top-41">
            <p><?php echo Yii::t('app', 'Теперь с Bitcoin Express, самой надежной и доступной платформой, у вас будет возможность реализовать  ваши мечты и иметь большую финансовую свободу.Проект BitcoinExpress родился в результате опыта с двумя другими аналогичными системами.');?></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section-98 section-sm-110 bg-lightest">
    <div class="shell">
      <div class="range text-md-left">
        <div class="cell-md-4">
          <!-- Icon Box Type 1--><span class="icon icon-sm icon-primary mdi mdi-code-tags"></span>
          <div class="text-italic text-gray-dark small"><?php echo Yii::t('app', 'Множество циклов');?></div>
          <div class="offset-top-10">
            <h4 class="text-uppercase text-bold"><?php echo Yii::t('app', 'НЕОГРАНИЧЕННЫЙ ДОХОД');?></h4>
            <p><?php echo Yii::t('app', 'У вас не будет рамок, которые вас ставят другие проекты. Вы можете повторять циклы бесконечное количество раз и создать постоянный источник дохода.');?></p>
          </div>
        </div>
        <div class="cell-md-4">
          <!-- Icon Box Type 1--><span class="icon icon-sm icon-primary mdi mdi-cube-outline"></span>
          <div class="text-italic text-gray-dark small"><?php echo Yii::t('app', 'Командная работа');?></div>
          <div class="offset-top-10">
            <h4 class="text-uppercase text-bold"><?php echo Yii::t('app', 'ОБЪЕДИНЕНИЕ УСИЛИЙ');?></h4>
            <p><?php echo Yii::t('app', 'Создавайте собственные команды, и вместе ставят цели к осуществлению поставленных задач и целей. Работа в команде принесет вам море хороших эмоций.');?></p>
          </div>
        </div>
        <div class="cell-md-4">
          <!-- Icon Box Type 1--><span class="icon icon-sm icon-primary mdi mdi-monitor"></span>
          <div class="text-italic text-gray-dark small"><?php echo Yii::t('app', 'Хорошая прибыть');?></div>
          <div class="offset-top-10">
            <h4 class="text-uppercase text-bold"><?php echo Yii::t('app', '1.26 BTC ЕЖЕДНЕВНО');?></h4>
            <p><?php echo Yii::t('app', 'При хорошо построенной командной и личной работе именно на такой дневной вы можете рассчитывать. Работа в этом проекте принесет вам то? к чему вы долго шли.');?></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section-98 section-sm-110">
    <div class="shell">
      <h2 class="text-bold"><?php echo Yii::t('app', 'ПРЕИМУЩЕСТВА BITCOIN EXPRESS');?></h2>
      <hr class="divider bg-mantis">
      <div class="range range-md-middle offset-top-66 range-md-center">
        <div class="cell-md-3 cell-md-push-1"><img src="bitexp/bitrobot.png" alt="" width="247" height="470" class="center-block img-responsive"></div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 text-md-right">
          <div class="unit unit-spacing-sm unit-inverse unit-md unit-md-horizontal">
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Пассивный доход');?></h4>
              <p><?php echo Yii::t('app', 'Вы сможете создать свой пассивный источник дохода');?></p>
            </div>
            <div class="unit-right"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-chart-histogram"></span></div>
          </div>
          <div class="unit unit-spacing-sm unit-inverse unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Активный доход');?></h4>
              <p><?php echo Yii::t('app', 'Повышенные вознаграждения за активную работу ');?></p>
            </div>
            <div class="unit-right"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-chart-line"></span></div>
          </div>
          <div class="unit unit-spacing-sm unit-inverse unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Использование Bitcoin');?></h4>
              <p><?php echo Yii::t('app', 'На сегодня Bitcoin самая быстрорастущая валюта');?></p>
            </div>
            <div class="unit-right"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-currency-btc"></span></div>
          </div>
        </div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 text-md-left cell-md-push-1 offset-top-50 offset-md-top-0">
          <div class="unit unit-spacing-sm unit-md unit-md-horizontal">
            <div class="unit-left"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-square-inc-cash"></span></div>
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Крупные бонусы');?></h4>
              <p><?php echo Yii::t('app', 'Большие денежные вознаграждения за закрытие мест');?></p>
            </div>
          </div>
          <div class="unit unit-spacing-sm unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-left"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-thumb-up"></span></div>
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Легкий старт');?></h4>
              <p><?php echo Yii::t('app', 'Маленькая стоимость участия, доступная абсолютно каждому');?></p>
            </div>
          </div>
          <div class="unit unit-spacing-sm unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-left"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-web"></span></div>
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Множество стран');?></h4>
              <p><?php echo Yii::t('app', 'Принять участие может каждый, независимо от страны проживания');?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="range range-condensed">
      <div style="background-image: url(/bitexp/bitcoin-05-1.jpg)" class="cell-md-6 bg-image"></div>
      <div class="cell-md-6">
        <div class="bg-lightest">
          <div class="inset-left-11p inset-right-11p section-98 section-sm-110">
            <div class="small text-uppercase text-spacing-60 text-primary"><?php echo Yii::t('app', 'Видео-руководство');?></div>
            <h2 class="text-bold offset-top-14"><?php echo Yii::t('app', 'Видео ожидается');?></h2>
            <div class="offset-top-41">
              <!-- Media Elements-->
              <!-- RD Video-->
              <div data-rd-video-path="/bitexp/FinancialFreedom" data-rd-video-title="Bitcoin express <?php echo Yii::t('app', 'Финансовая свобода');?>" data-rd-video-muted="true" data-rd-video-preview="video/intense/intense-preview.jpg" class="rd-video-player play-on-scroll">
                <div class="rd-video-wrap embed-responsive-16by9">
                  <div class="rd-video-preloader"></div>
                  <video preload="metadata"></video>
                  <div class="rd-video-preview"></div>
                  <div class="rd-video-top-controls">
                    <!-- Title--><span class="rd-video-title"></span><a href="#" class="rd-video-fullscreen mdi mdi-fullscreen rd-video-icon"></a>
                  </div>
                  <div class="rd-video-controls">
                    <div class="rd-video-controls-buttons">
                      <!-- Play\Pause button--><a href="#" class="rd-video-play-pause mdi mdi-play"></a>
                      <!-- Progress bar-->
                    </div>
                    <div class="rd-video-progress-bar"></div>
                    <div class="rd-video-time"><span class="rd-video-current-time"></span> <span class="rd-video-time-divider">:</span>  <span class="rd-video-duration"></span></div>
                    <div class="rd-video-volume-wrap">
                      <!-- Volume button--><a href="#" class="rd-video-volume mdi mdi-volume-high rd-video-icon"></a>
                      <div class="rd-video-volume-bar-wrap">
                        <!-- Volume bar-->
                        <div class="rd-video-volume-bar"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--include ../sections/_section-buy-now-->
  <section class="section-98 section-md-110 context-dark bg-gray-darkest">
    <div class="shell-fluid">
      <div class="range">
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-12 cell-md-preffix-0">
          <div class="range">
            <div class="cell-sm-6 cell-md-3 cell-md-preffix-0">
              <!-- Counter type 1-->
              <div class="counter-type-1">
                <div class="h1"><span data-step="3000" data-from="0" data-to="<?php echo User::find()->count();?>" class="big counter text-bold text-blue-gray"></span>
                  <hr class="divider bg-white"/>
                </div>
                <h6 class="text-uppercase text-bold text-spacing-60 offset-top-20"><?php echo Yii::t('app', 'ВСЕГО УЧАСТНИКОВ');?></h6>
              </div>
            </div>
            <div class="cell-sm-6 cell-md-3 offset-top-66 offset-sm-top-0">
              <!-- Counter type 1-->
              <div class="counter-type-1">
                <div class="h1"><span data-speed="2500" data-from="0" data-to="<?php echo Payment::find()->orWhere(['status' => Payment::STATUS_WAIT])->orWhere(['status' => Payment::STATUS_WAIT_SYSTEM])->orWhere(['status' => Payment::PROCESS_SYSTEM])->count();?>" class="big counter text-bold text-blue-gray"></span><span class="big text-bold text-blue-gray"></span>
                  <hr class="divider bg-white"/>
                </div>
                <h6 class="text-uppercase text-bold text-spacing-60 offset-top-20"><?php echo Yii::t('app', 'ХОТЯТ СДЕЛАТЬ ПОДАРОК');?></h6>
              </div>
            </div>
            <div class="cell-sm-6 cell-md-3 offset-top-66 offset-md-top-0">
              <!-- Counter type 1-->
              <div class="counter-type-1">
                <div class="h1"><span data-step="1500" data-from="0" data-to="<?php echo Payment::find()->where(['status' => Payment::STATUS_OK])->count();?>" class="big counter text-bold text-blue-gray"></span>
                  <hr class="divider bg-white"/>
                </div>
                <h6 class="text-uppercase text-bold text-spacing-60 offset-top-20"><?php echo Yii::t('app', 'ОТПРАВЛЕННЫХ ПОДАРКОВ');?></h6>
              </div>
            </div>
            <div class="cell-sm-6 cell-md-3 offset-top-66 offset-md-top-0">
              <!-- Counter type 1-->
              <div class="counter-type-1">
                <div class="h1"><span data-speed="1300" data-from="0" data-to="<?php echo Payment::find()->select('sum(sum) as sum')->where(['status' => Payment::STATUS_OK])->one()->sum;?>" class="big counter text-bold text-blue-gray"></span><span class="big text-bold text-blue-gray"></span>
                  <hr class="divider bg-white"/>
                </div>
                <h6 class="text-uppercase text-bold text-spacing-60 offset-top-20"><?php echo Yii::t('app', 'ПОДАРЕННО BTC');?></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="bg-lightest">
    <!-- Our Works-->
    <section id="home-section-childs" class="section-98 section-110">
      <div class="shell-wide">
        <h2 class="text-bold"><span class="big"><?php echo Yii::t('app', 'Решайся прямо сейчас!');?></span></h2>

        <div class="offset-top-41">
          <div class="isotope-wrap">
            <div class="row">
              <div class="col-lg-12 offset-top-34">
                <a href="<?php echo Url::to(['/singup']);?>" data-caption-animate="fadeInUp" data-caption-delay="1200" class="btn btn-primary btn-lg btn-anis-effect"><span class="btn-text"><?php echo Yii::t('app', 'Присоединиться');?></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</main>
