<?php
use yii\widgets\Breadcrumbs;
use yii\web\View;
use yii\helpers\Url;


$this->params['breadcrumbs']= [ [
    'label' => 'О Нас',
    'url' => '/about'
  ],
];
$this->title = Yii::t('app', $model->title);
?>

</header>

<section class="section-66 section-top-110 bg-malibu section-triangle section-triangle-bottom context-dark">
  <div class="shell">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-account-multiple icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h2><span class="big"><?= Yii::t('app', 'О Нас');?></span></h2>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
        <ul class="list-inline list-inline-dashed p">
          <?= Breadcrumbs::widget([
            'options' => ['class' => 'list-inline list-inline-dashed p'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>
        </ul>
      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="#6ebbe9"></polyline>
  </svg>
</section>
<main class="page-content">
  <!-- About Us-->
  <section class="section-98 section-sm-110">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-xs-10 cell-lg-6 cell-lg-push-2"><img src="/bitexp/biznes-marketing-perspektiva.jpg" width="960" height="540" alt="" class="img-responsive reveal-inline-block offset-top-10"></div>
        <div class="cell-xs-10 cell-lg-6 text-md-left inset-md-right-80 cell-lg-push-1 offset-top-50 offset-lg-top-0">
          <h1><?= Yii::t('app', 'О Нас');?></h1>
          <hr class="divider hr-lg-left-0 bg-mantis">
          <div class="offset-top-30 offset-md-top-50">
            <p><?php echo Yii::t('app', 'У вас есть большое желание или  идея как превратить желаемое в реальность? Теперь с BitcoinExpress вам не нужно беспокоиться, потому что при поддержке единомышленников BitcoinExpress поможет вам получить то финансирование, которое вам нужно. С BitcoinExpress  у вас будет онлайн платформа, которая даст возможность связываться с людьми, которые будут готовы вам помочь. В BitcoinExpress  мы поможем вам с прямым финансированием ваших потребностей. С помощью этой программы вы сможете обрести желаемую финансовую свободу.');?></p>
            <p><?php echo Yii::t('app', 'Команда BitcoinExpress благодарит вас за внимание.');?></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Why Choose Us-->
  <section class="section-top-98 section-sm-top-110 section-sm-bottom-110 section-lg-top-66 section-bottom-98 section-lg-bottom-0 bg-lightest">
    <div class="shell">
      <div class="range range-sm-center range-md-middle">
        <div class="cell-lg-5 veil reveal-lg-inline-block"><img width="470" height="770" src="/images/pages/about-us-01-470x770.png" alt="" class="img-responsive center-block"></div>
        <div class="cell-sm-10 cell-lg-5 section-lg-bottom-50">
          <h1 class="offset-none"><?php echo Yii::t('app', 'Почему мы');?></h1>
          <hr class="divider bg-mantis">
          <div class="offset-top-66 offset-lg-top-50">
                    <!-- Icon Box Type 2-->
                    <div class="unit unit-sm unit-sm-horizontal text-sm-left">
                      <div class="unit-left"><span class="icon text-gray mdi mdi-account-switch"></span></div>
                      <div class="unit-body">
                        <h4 class="text-bold text-mantis offset-sm-top-14"><?php echo Yii::t('app', 'Из рук в руки');?></h4>
                        <p><?php echo Yii::t('app', 'Процесс обмена подарками происходит только напрямую между участниками.');?></p>
                      </div>
                    </div>
            <div class="offset-top-66 offset-lg-top-34">
                      <!-- Icon Box Type 2-->
                      <div class="unit unit-sm unit-sm-horizontal text-sm-left">
                        <div class="unit-left"><span class="icon text-gray mdi mdi-wallet-travel"></span></div>
                        <div class="unit-body">
                          <h4 class="text-bold text-mantis offset-sm-top-14"><?php echo Yii::t('app', 'Нет скрытых комиссий');?></h4>
                          <p><?php echo Yii::t('app', 'Проект не взимает никаких комиссий за пользование сервисом.');?></p>
                        </div>
                      </div>
            </div>
            <div class="offset-top-66 offset-lg-top-34">
                      <!-- Icon Box Type 2-->
                      <div class="unit unit-sm unit-sm-horizontal text-sm-left">
                        <div class="unit-left"><span class="icon text-gray mdi mdi-airplane"></span></div>
                        <div class="unit-body">
                          <h4 class="text-bold text-mantis offset-sm-top-14"><?php echo Yii::t('app', 'Работа в любом месте');?></h4>
                          <p><?php echo Yii::t('app', 'Где бы вы не находились, вы сможете использовать сервис.');?></p>
                        </div>
                      </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
