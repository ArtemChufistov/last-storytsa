<?php
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'Faq',
    'url' => '/faq'
  ],
];
$this->title = Yii::t('app', $model->title);
?>

</header>

<section class="section-66 section-top-110 bg-malibu section-triangle section-triangle-bottom context-dark">
  <div class="shell">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-newspaper icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h2><span class="big"><?= Yii::t('app', 'FAQ');?></span></h2>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
        <ul class="list-inline list-inline-dashed p">
          <?= Breadcrumbs::widget([
            'options' => ['class' => 'list-inline list-inline-dashed p'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>
        </ul>
      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="#6ebbe9"></polyline>
  </svg>
</section>

<!-- Page Content-->
<main class="page-content">
  <!-- Faq variant 3-->
  <section class="section-md-top-110 section-md-bottom-0 section-98">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-lg-6"><img src="/images/pages/call-to-action-01-566x548.png" width="566" height="548" alt="" class="img-responsive veil reveal-lg-inline-block offset-lg-top-10"></div>
        <div class="cell-sm-9 cell-md-6 text-left offset-top-0">
          <div class="text-center">
            <h1><?php echo Yii::t('app', 'Часто задаваемые вопросы');?></h1>
            <hr class="divider bg-mantis">
          </div>
          <div class="section-top-0 section-sm-top-50 section-sm-bottom-50 offset-top-30 offset-lg-top-0">
                    <!-- Classic Accordion-->
                    <div data-type="accordion" class="responsive-tabs responsive-tabs-classic">
                      <ul data-group="tabs-group-default" class="resp-tabs-list tabs-group-default">
                        <li><?= Yii::t('app', 'КАК СТАТЬ УЧАСТНИКОМ СИСТЕМЫ?');?></li>
                        <li><?= Yii::t('app', 'МОГУ ЛИ Я ИЗМЕНИТЬ СВОИ ДАННЫЕ ПОСЛЕ РЕГИСТРАЦИИ?');?></li>
                        <li><?= Yii::t('app', 'КАК ВЕРИФИЦИРОВАТЬ E-MAIL?');?></li>
                        <li><?= Yii::t('app', 'КАК УСТАНОВИТЬ ИЛИ ИЗМЕНИТЬ СВОИ РЕКВИЗИТЫ?');?></li>
                        <li><?= Yii::t('app', 'КАКИЕ ВАРИАНТЫ ОПЛАТ ПРИСУТСТВУЮТ?');?></li>
                        <li><?= Yii::t('app', 'СКОЛЬКО ВРЕМЕНИ ДАЕТСЯ НА АКТИВАЦИЮ АККАУНТА?');?></li>
                        <li><?= Yii::t('app', 'НУЖНО ЛИ ЖДАТЬ ПОДТВЕРЖДЕНИЕ ОПЛАТЫ ИЛИ ПОДТВЕРЖДАТЬ ПОЛУЧЕНИЕ?');?></li>
                        <li><?= Yii::t('app', 'ГДЕ МНЕ УВИДЕТЬ МОЮ РЕФЕРАЛЬНУЮ ССЫЛКУ?');?></li>
                        <li><?= Yii::t('app', 'КАК Я МОГУ ИСПОЛЬЗОВАТЬ СВОЮ РЕФЕРАЛЬНУЮ ССЫЛКУ?');?></li>
                        <li><?= Yii::t('app', 'КАК ДОЛГО ЖДАТЬ ПОДТВЕРЖДЕНИЯ ТРАНЗАКЦИИ В СЕТИ?');?></li>
                        <li><?= Yii::t('app', 'СКОЛЬКО ЧЕЛОВЕК Я МОГУ ПРИГЛАСИТЬ В СИСТЕМУ?');?></li>
                        <li><?= Yii::t('app', 'СКОЛЬКО Я СМОГУ ЗАРАБОТАТЬ?');?></li>
                        <li><?= Yii::t('app', 'ЧТО ДЕЛАТЬ ЕСЛИ ЗАБЫЛИ ПАРОЛЬ?');?></li>
                        <li><?= Yii::t('app', 'ОСТАЛИСЬ ВОПРОСЫ?');?></li>
                      </ul>
                      <div data-group="tabs-group-default" class="resp-tabs-container tabs-group-default">
                        <div><?= Yii::t('app', 'Для того, чтобы стать участником, Вам достаточно пройти простую форму регистрации на нашем сайте, перейдя по реферальной ссылке Вашего пригласителя. Обращаем Ваше внимание на то, что данные указываемые при регистрации должны быть достоверны.');?></div>
                        <div><?= Yii::t('app', 'Вам доступно любое изменение данных своего кабинета, кроме изменения Вашего логина и Вашего спонсора.');?></div>
                        <div><?= Yii::t('app', 'Для того чтобы верифицировать e-mail вам необходимо в разделе профиль указать действующую почту и получить активационную ссылку. После того, как вы это сделаете, ваш e-mail будет верифицирован.');?></div>
                        <div><?= Yii::t('app', 'Прежде всего необходимо получить платежный пароль. Это можно сделать в разделе реквизиты. После этого, вы сможете добавить/изменить реквизиты. Получить платежный пароль возможно только на верифицированную почту.');?></div>
                        <div><?= Yii::t('app', 'Проект работает с криптовалютой Bitcoin');?></div>
                        <div><?= Yii::t('app', 'Вы можете произвести активацию в любой момент времени после того как пройдете регистрацию. Ваша регистрация будет действительна постоянно. Но настоятельно рекомендуем Вам не тянуть с активацией, так как Вы будете терять выплаты от переливов общего распределения участников в структуре.');?></div>
                        <div><?= Yii::t('app', 'В проекте все переводы между участниками полностью автоматизированы и не требуют подтверждения от их получателя. Транзакция считается успешной при подтверждении ее сетью.');?></div>
                        <div><?= Yii::t('app', 'Реферальная ссылка находится в разделе профиль вашего личного кабинета. Она становится доступной вам, только после активации аккаунта и получения места в программе.');?></div>
                        <div><?= Yii::t('app', 'Реферальная ссылка для каждого участника генерируется индивидуально и дает Вам возможность привлекать людей именно в Вашу структуру. Вы можете размещать ее в социальных сетях, блогах, тематических форумах и прикреплять к письмам в E-mail рассылках. Это обеспечит Вас постоянным притоком новых участников в Вашу структуру, а значит, Ваш доход будет расти.');?></div>
                        <div><?= Yii::t('app', 'Все переводы проходят в сети биткоин и требуется время, для того чтобы транзакция принялась сетью и подтвердилась. С учетом особенности сети ожидание подтверждения может занимать несколько часов. Статус вашей транзакции можно увидеть в деталях заявки.');?></div>
                        <div><?= Yii::t('app', 'Вы можете приглашать сколько угодно новых участников. Все они будут занимать свободные места в вашей структуре');?></div>
                        <div><?= Yii::t('app', 'Доход за полный цикл составляет: 1.26 BTC. С учетом реинвестов и повторных закрытий вы сможете получать пассивный доход бесконечное количество раз');?></div>
                        <div><?= Yii::t('app', 'Воспользуйтесь функцией восстановления пароля.');?></div>
                        <div><?= Yii::t('app', 'Задать вопрос вы можете на странице <a href="/contact">обратной связи</a> или в личном кабинете в разделе "Техническая поддержка"');?></div>
                      </div>
                    </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- First Lesson - Free-->
  <section class="bg-gray-base context-dark">
            <!-- RD Parallax-->
            <div data-on="false" data-md-on="true" class="rd-parallax">
              <div data-speed="0.35" data-type="media" data-url="images/backgrounds/background-44-1920x1024.jpg" class="rd-parallax-layer"></div>
              <div data-speed="0" data-type="html" class="rd-parallax-layer">
                <div class="shell section-66 context-dark">
                  <h2 class="text-bold"><?php echo Yii::t('app', 'Если у Вас остались вопросы, свяжитесь с нами!');?></h2><a href="/contact" class="btn btn-icon btn-icon-left btn-default offset-top-20"><span class="icon mdi-email-outline mdi"></span><?php echo Yii::t('app', 'Связаться с нами');?></a>
                </div>
              </div>
            </div>
  </section>
</main>
