<?php
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
		'label' => 'Как это работает',
		'url' => '/howitworks'
	],
];

$this->title = Yii::t('app', $model->title);
?>

</header>

<section class="section-66 section-top-110 bg-malibu section-triangle section-triangle-bottom context-dark">
  <div class="shell">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-account-check icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h2><span class="big"><?= Yii::t('app', 'Как это работает');?></span></h2>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
        <ul class="list-inline list-inline-dashed p">
          <?= Breadcrumbs::widget([
            'options' => ['class' => 'list-inline list-inline-dashed p'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>
        </ul>
      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="#6ebbe9"></polyline>
  </svg>
</section>

<!-- Page Content-->
<main class="page-content">
	<!-- section join our team-->
	<section class="section-124">
		<div class="shell">
			<div class="range range-xs-center">
				<div class="cell-xs-8">
					<h1><?php echo Yii::t('app', 'Присоединиться к нашей команде');?></h1>
					<div>
						<hr class="divider bg-mantis">
					</div>
					<div class="offset-top-20">
						<p><?php echo Yii::t('app', 'Присоединившись к нашей сети каждый сможет улучшить уровень своей жизни и  друг друга быстро и эффективно.Теперь с Bitcoin Express, самой надежной и доступной платформой, у вас будет возможность реализовать  ваши мечты и иметь большую финансовую свободу! ');?></p>
					</div>
					<div class="offset-top-34"><a href="<?php echo Url::to(['/signup']);?>" data-custom-scroll-to="careers-start" class="btn btn-primary btn-icon btn-icon-left"><span class="icon mdi mdi-check-all"></span><?php echo Yii::t('app', 'Начать сейчас');?></a></div>
				</div>
			</div>
		</div>
	</section>
	<!-- section reasons to get-->
	<section class="section-66 section-top-50 bg-mantis section-triangle section-triangle-bottom context-dark">
		<div class="shell">
			<div class="range range-sm-center">
				<h2><span class="big"> <?php echo Yii::t('app', 'Как это работает');?></span></h2>
				<div class="cell-md-8">
					<p><?php echo Yii::t('app', 'Вот как это работает. После того, как вы регистрируетесь по реферальной ссылке вашего вышестоящего спонсора и оплачиваете  ему 0,03 Биткоина за Цикл, вы приглашаете 2-ух партнёров вашего 1-го уровня, или же получаете их переливом по мере заполнения структуры от выше стоящих спонсоров . Как только вы получаете такую же сумму от каждого из двух приглашенных вами, вы из заработанной суммы оплачиваете переход на следующий 2-й уровень. По мере того, как этот процесс будет расширяться, вы  сможете заработать достаточно денег в системе для перехода на 3-й уровень.');?></p>
				</div>
			</div>
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
			<defs>
				<lineargradient id="grad2" x1="0%" y1="0%" x2="100%" y2="0%">
					<stop offset="0%" style="stop-color:rgb(99,189,98);stop-opacity:1"></stop>
					<stop offset="100%" style="stop-color:rgb(99,189,98);stop-opacity:1"></stop>
				</lineargradient>
			</defs>
			<polyline points="0,0 60,0 29,29" fill="#f87a19"></polyline>
		</svg>
	</section>
	<section class="section-66 section-sm-0">
		<div class="shell">
			<div class="range range-xs-center range-sm-left">
				<div class="cell-xs-10 cell-sm-6 section-image-aside section-image-aside-right text-left">
					<div style="background-image: url(images/pages/careers-01-960x540.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
					<div class="section-image-aside-body section-sm-66 inset-sm-right-30">
						<div>
							<h3 class="text-picton-blue">01</h3>
						</div>
						<div class="offset-top-10">
							<h2> <?php echo Yii::t('app', 'Уровень 1');?></h2>
						</div>
						<div class="offset-top-20">
							<p><?php echo Yii::t('app', '');?></p>
							<p><?php echo Yii::t('app', '2 x 0.03 BTC = 0.06 BTC – 0.05 BTC (оплата уровня 2)');?></p>
							<p><?php echo Yii::t('app', '= 0.01 BTC чистый доход от 1-го уровня за Цикл');?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="range range-xs-center range-sm-right offset-top-0">
				<div class="cell-xs-10 cell-sm-6 section-image-aside section-image-aside-left text-left">
					<div style="background-image: url(/bitexp/biznes-rukopozhatie-sdelka-2789.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
					<div class="section-image-aside-body offset-top-41 offset-sm-top-0 section-sm-66 inset-sm-left-50">
						<div>
							<h3 class="text-picton-blue">02</h3>
						</div>
						<div class="offset-top-10">
							<h2> <?php echo Yii::t('app', 'Уровень 2');?></h2>
						</div>
						<div class="offset-top-20">
							<p><?php echo Yii::t('app', '');?></p>
							<p><?php echo Yii::t('app', '4 x 0.05 BTC = 0.2 BTC – 0.15 BTC (оплата уровня 3)');?></p>
							<p><?php echo Yii::t('app', '= 0.05 BTC чистый доход от 2-го уровня за Цикл');?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="range range-xs-center range-sm-left offset-top-0">
				<div class="cell-xs-10 cell-sm-6 section-image-aside section-image-aside-right text-left">
					<div style="background-image: url(/images/pages/careers-03-960x540.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
					<div class="section-image-aside-body offset-top-41 offset-sm-top-0 section-sm-66 inset-sm-right-30">
						<div>
							<h3 class="text-picton-blue">03</h3>
						</div>
						<div class="offset-top-10">
							<h2> <?php echo Yii::t('app', 'Уровень 3');?></h2>
						</div>
						<div class="offset-top-20">
							<p><?php echo Yii::t('app', '');?></p>
							<p><?php echo Yii::t('app', '8 x 0.15 BTC = 1.20 BTC');?></p>
							<p><?php echo Yii::t('app', '= 1.20 BTC чистый доход от 3-го уровня за Цикл');?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="range range-xs-center range-sm-right offset-top-0">
				<div class="cell-xs-10 cell-sm-6 section-image-aside section-image-aside-left text-left">
					<div style="background-image: url(/images/pages/careers-04-960x540.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
					<div class="section-image-aside-body offset-top-41 offset-sm-top-0 section-sm-66 inset-sm-left-50">
						<div>
							<h3 class="text-picton-blue">04</h3>
						</div>
						<div class="offset-top-10">
							<h2> <?php echo Yii::t('app', 'Итого');?></h2>
						</div>
						<div class="offset-top-20">
							<p><?php echo Yii::t('app', 'Общий доход за прохождение всех трех уровней с одного цикла, за вычетом расходов, составит:');?></p>
							<p><?php echo Yii::t('app', '0.01 BTC + 0.05 BTC + 1.20 BTC = 1.26 BTC');?></p>
							<p><?php echo Yii::t('app', 'Это может стать вашим ежедневным доходом');?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
