<?php

use app\modules\user\components\LoginWidget;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\menu\widgets\MenuWidget;
use app\assets\cryptofund\FrontAppAssetTop;
use app\assets\cryptofund\FrontAppAssetBottom;
use app\modules\profile\components\AuthChoiceBitmoda;
use app\modules\lang\widgets\WLang;

FrontAppAssetTop::register($this);
FrontAppAssetBottom::register($this);

$this->title = Yii::t('app', 'Войти в личный кабинет');
?>

<a class="ut-offset-anchor" id="top" style="top:0px !important;"></a>
<div class="ut-loader-overlay"></div>

<!-- header section -->
<header id="header-section" class="ha-header centered ha-transparent ">
    <div class="grid-container">
        <div class="ha-header-perspective">
            <div class="ha-header-front">
                <div class="grid-20 tablet-grid-50 mobile-grid-50 ">
                    <div class="site-logo"> <img data-altlogo="/bitmoda/logoBT11.png" src="/bitmoda/logoBT11.png">
                    </div>
                </div>

                <?=WLang::widget();?>

            </div>
        </div>
    </div>
</header><!-- close header -->

<div class="clear"></div>

<!-- hero section -->
<section id="ut-hero" class=" hero ha-waypoint parallax-section parallax-background" data-animate-up="ha-header-hide" data-animate-down="ha-header-hide" name="ut-hero">

    <a id="ut-background-video-hero" class="ut-video-player" data-property="{ videoURL : 'http://youtube.com/watch?v=vhtOgr_Aikc' , containment : '#ut-hero', showControls: false, autoPlay : true, loop : true, mute : true, vol: 0, startAt : 0, opacity : 1}"></a>
    <div class="parallax-overlay parallax-overlay-pattern style_one ">
        <div class="grid-container">
            <!-- hero holder -->
            <div class="hero-holder grid-100 mobile-grid-100 tablet-grid-100 ut-hero-style-9">
                <div class="hero-inner" style="text-align:;">
                    <div class="hdh"><span class="hero-description"><?php echo Yii::t('app', 'ВХОД НА САЙТ ЧЕРЕЗ GOOGLE+<br/>ВАШ АККАУНТ ДОЛЖЕН БЫТЬ ЖЕНСКИМ');?></span></div>

                    <?=AuthChoiceBitmoda::widget([
                        'baseAuthUrl' => ['/profile/auth/index'],
                        'clientCssClass' => 'hero-btn-holder',
                    ])?>

                </div>
            </div><!-- close hero-holder -->
        </div>
    </div>
</section>
<!-- end hero section -->
<script type='text/javascript' src='/bitmoda/js_G/jquery.form.min.js'></script>
<script type='text/javascript' src='/bitmoda/js_G/jquery.prettyPhoto.min.js'></script>
<script type='text/javascript' src='/bitmoda/js_G/jquery.fitvids.js'></script>
<script type='text/javascript' src='/bitmoda/js_G/toucheffects.min.js'></script>
<script type='text/javascript' src='/bitmoda/js_G/jquery.easing.min.js'></script>
<script type='text/javascript' src='/bitmoda/js_G/superfish.min.js'></script>
<script type='text/javascript' src='/bitmoda/js_G/jquery.mb.YTPlayer.min.js'></script>
<script type='text/javascript' src='/bitmoda/js_G/jquery.waypoints.min.js'></script>
<script type='text/javascript' src='/bitmoda/js_G/ut-init.js'></script>
<script type="text/javascript">
    (function($){"use strict";$(document).ready(function(){ var brooklyn_scroll_offset = $("#header-section").outerHeight();window.matchMedia||(window.matchMedia=function(){var c=window.styleMedia||window.media;if(!c){var a=document.createElement("style"),d=document.getElementsByTagName("script")[0],e=null;a.type="text/css";a.id="matchmediajs-test";d.parentNode.insertBefore(a,d);e="getComputedStyle"in window&&window.getComputedStyle(a,null)||a.currentStyle;c={matchMedium:function(b){b="@media "+b+"{ #matchmediajs-test { width: 1px; } }";a.styleSheet?a.styleSheet.cssText=b:a.textContent=b;return"1px"===e.width}}}return function(a){return{matches:c.matchMedium(a|| "all"),media:a||"all"}}}());var ut_modern_media_query = window.matchMedia( "screen and (-webkit-min-device-pixel-ratio:2)");$("body").queryLoader2({showbar: "on",barColor: "#FFC09A",textColor: "#FFC09A",backgroundColor: "#121212",barHeight: 6,percentage: true,completeAnimation: "fade",minimumTime: 500,onComplete : function(){$(".ut-loader-overlay").fadeOut( 600 , "easeInOutExpo" , function(){$(this).remove();});}});$(window).load(function(){function show_slogan(){$(".hero-holder").animate({ opacity : 1 });}var execute_slogan = setTimeout ( show_slogan , 800);});$(".cta-btn a").click( function(event){ if(this.hash){$.scrollTo( this.hash , 650, { easing: "easeInOutExpo" , offset: -brooklyn_scroll_offset-1 , "axis":"y" });event.preventDefault();}});var $header = $("#header-section"),$logo= $(".site-logo img"),logo= $logo.attr("src"),logoalt = $logo.data("altlogo"),is_open = false,has_passed= false;var ut_update_header_skin = function(){if (($(window).width()> 979)&& is_open){$(".ut-mm-trigger").trigger("click");if( has_passed){$header.attr("class", "ha-header ut-header-dark centered ");} else {$header.attr("class", "ha-header ha-transparent centered");}} };var ut_nav_skin_changer = function( direction){if( direction === "down" && !is_open){$header.attr("class", "ha-header ut-header-dark centered ");$logo.attr("src" , logoalt);has_passed = true;} else if( direction === "up" && !is_open){$header.attr("class", "ha-header ha-transparent centered");$logo.attr("src" , logo);has_passed = false;} }; $(".ut-mm-trigger").click(function(event){ if( $header.hasClass("ha-transparent")&& !has_passed){$header.attr("class", "ha-header centered ut-header-dark");$logo.attr("src" , logoalt);} else if ( $header.hasClass("ut-header-dark")&& !has_passed){$header.attr("class", "ha-header centered ha-transparent");$logo.attr("src" , logo);}event.preventDefault();}).toggle(function(){ is_open = true; }, function(){ is_open = false; }); $(window).utresize(function(){ut_update_header_skin();});$( "#main-content").waypoint(function(direction){ut_nav_skin_changer(direction);if( direction === "down"){has_passed = true; } else if( direction === "up"){has_passed = false; }}, { offset: brooklyn_scroll_offset+1 });if( $("#ut-background-video-hero").length){$("#ut-background-video-hero").mb_YTPlayer();$("#ut-video-hero-control.youtube").click(function(event){if( $(this).hasClass("ut-unmute")){$(this).removeClass("ut-unmute").addClass("ut-mute").text("MUTE");$("#ut-background-video-hero").unmuteYTPVolume();$("#ut-background-video-hero").setYTPVolume(0);} else {$(this).removeClass("ut-mute").addClass("ut-unmute").text("UNMUTE");$("#ut-background-video-hero").muteYTPVolume();}event.preventDefault();});}$(".parallax-banner").addClass("fixed").each(function(){$(this).parallax( "50%", 0.6); });$("section").each(function(){var outerHeight = $(this).outerHeight(),offset= "90%",effect= $(this).data("effect");if( outerHeight > $(window).height()/ 2){offset = "70%";}$(this).waypoint( function( direction){var $this = $(this);if( direction === "down" && !$(this).hasClass( effect)){$this.find(".section-content").animate( { opacity: 1 } , 1600);$this.find(".section-header-holder").animate( { opacity: 1 } , 1600);}} , { offset: offset });});});})(jQuery);
</script>
<audio src="/bitmoda/bitmoda/backsound.mp3" autoplay="autoplay" loop></audio>