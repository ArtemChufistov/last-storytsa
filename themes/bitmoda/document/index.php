<?php
$this->title = Yii::t('app', 'Главная');
?>
<style>
  .is_overlay{ display: block; width: 100%; height: 100%; }
  
  #preloader {
    margin: auto; 
    left: 0; top: 0; right: 0; bottom: 0; 
    position: absolute;
    width: 4em;       
    height: 4em;
    text-align: center;
    vertical-align: middle;
    transition: opacity .3s;
    -webkit-transition: opacity .3s;
    -moz-transition: opacity .3s;
    -ms-transition: opacity .3s;
    -o-transition: opacity .3s;
    background-color: black;
    border: 1px solid black;
    border-radius: 10px;
    padding: 5px;
  }

  #wait { 
    -webkit-animation: spin 1.2s infinite linear; 
    -moz-animation: spin 1.2s infinite linear; 
    -ms-animation: spin 1.2s infinite linear; 
    -o-animation: spin 1.2s infinite linear; 
    animation: spin 1.2s infinite linear; 
    font-size: 4em;
    color: white;
  }
  

  body {
    background-color: black;
  }

  #trailer {
    position: fixed;
    top: 0; right: 0; bottom: 0; left: 0;
    overflow: hidden;
  }
  #trailer > video {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  /* 1. No object-fit support: */
  @media (min-aspect-ratio: 16/9) {
    #trailer > video { height: 300%; top: -100%; }
  }
  @media (max-aspect-ratio: 16/9) {
    #trailer > video { width: 300%; left: -100%; }
  }
  /* 2. If supporting object-fit, overriding (1): */
  @supports (object-fit: cover) {
    #trailer > video {
      top: 0; left: 0;
      width: 100%; height: 100%;
      object-fit: cover;
    }
  }

  #enter {
    position: fixed;
    z-index: 100;
    left: 50%;
    margin-left: -120px;
    display: table;
    bottom: 3em;
    opacity: 0;
    font-size: 12px;
    line-height: 1.2;
    transition: opacity 9.3s;
    -webkit-transition: opacity 9.3s;
    -moz-transition: opacity 9.3s;
    -ms-transition: opacity 9.3s;
    -o-transition: opacity 9.3s;
  }
  #to-about-section{
    padding: 3px;
    box-shadow: 0 0 10px rgba(0,0,0,0.5); /* Параметры тени */
  }
.touch body{
  background: url('/image/bg.jpg') no-repeat center center;
  background-size: cover;
}
  
</style>

<script src="/js/detect.min.js"></script>

<script type="text/javascript">
  var Preloader = function (element) {        
    var video =  document.getElementById(element),
      preloader = document.getElementById('preloader'),
      enter = document.getElementById('enter');

    var ua = detect.parse(navigator.userAgent);
    if ( ua.os.family === 'Android' ) {
      video.setAttribute( 'controls','controls' );
    }
    

    var api = {};

    api._removePreloader = function() {
      preloader.style.opacity = 0;
      enter.style.opacity = 1;
    };

    api.startCheckingLoading = function() {
      video.addEventListener('click',function(){
        video.play();
      },false);

      video.addEventListener( 'play', api._removePreloader() );     
    };

    return api;                
  }
  
  window.addEventListener('load', function() {
    var preloader = Preloader('video');
    preloader.startCheckingLoading();


    
  });

</script>

<div id="enter">
    <span class="hero-btn-holder"> 
        <a  target="_parent" href="/auth"> 
          <img src = "/bitmoda/enter1.png" style = "width: 200px;">
        </a>
    </span> 
</div>
<div id="trailer" class="is_overlay">
    <video id="video" width="100%" height="auto"  autoplay="autoplay" loop="loop" preload="auto" >
        <source src="/bitmoda/BitModa.mp4">
        <source src="/bitmoda/BitModa.webm" type="video/webm">
    </video>
</div>

<div id="preloader">
    <i id="wait" class="fa fa-spinner"></i>
</div>