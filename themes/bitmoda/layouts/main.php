<?php

use app\modules\user\components\LoginWidget;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\menu\widgets\MenuWidget;
use app\assets\cryptofund\FrontAppAssetTop;
use app\assets\cryptofund\FrontAppAssetBottom;
use app\modules\profile\components\AuthChoiceBitmoda;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>
</head>


<body id="ut-sitebody" class="home page page-id-35 page-template-default" data-scrolleffect="easeInOutExpo" data-scrollspeed="650">
<?php $this->beginBody() ?>

    <?php echo $content;?>

<?php $this->endBody() ?>
</body>
<script type="text/javascript">
    document.oncontextmenu = function (){return false};
</script>
</html>
<?php $this->endPage() ?>