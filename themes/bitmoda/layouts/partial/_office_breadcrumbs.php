<div id="breadcrumb">
  	<a href="/" title="<?php echo Yii::t('app', 'Вернуться на главную');?>" class="tip-bottom"><i class="icon-home"></i> <?php echo Yii::t('app', 'Главная');?></a>
	<?php foreach($breadcrumbs as $num => $breadcrumb):?>
  		<a 
  		<?php if ($num < count($breadcrumbs) - 1):?>href="<?php echo $breadcrumb['url']?>" <?php else:?>href = "#"<?php endif;?>
  		<?php if ($num < count($breadcrumbs) - 1):?>class="tip-bottom"<?php else:?>class="current"<?php endif;?>
  		title = "<?php echo $breadcrumb['title']?>">
  			<i class="<?php echo $breadcrumb['icon']?>"></i> <?php echo $breadcrumb['title']?>
  		</a>
  	<?php endforeach;?>
</div>