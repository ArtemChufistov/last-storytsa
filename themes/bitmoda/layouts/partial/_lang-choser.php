<?php
use yii\helpers\Html;
?>
<nav id="navigation" class="grid-80 hide-on-tablet hide-on-mobile ">
	<ul id="menu-main" class="menu">
		<li class="ut-home-link">
			<a href="#"><?=$current->name;?></a>
		</li>
		<?php foreach ($langs as $lang): ?>
        	<li class="menu-item menu-item-type-post_type menu-item-object-page">
        		<?=Html::a($lang->name, '/' . $lang->url . Yii::$app->getRequest()->getLangUrl())?>
        	</li>
        <?php endforeach;?>
    </ul>
</nav>
