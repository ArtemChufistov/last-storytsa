<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use lowbase\user\models\User;
use app\modules\profile\widgets\NotifyWidget;

?>

<!--Header-part-->
<div id="header">
  <h1><a href="dashboard.html"><?= Yii::t('app', 'Matrix Admin');?></a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text"><?= Yii::t('app', 'Добро пожаловать {login}', ['login' => $user->login]);?></span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo Url::to(['/office/dashboard'])?>"><i class="icon-user"></i> <?= Yii::t('app', 'Статистика');?></a></li>
        <li class="divider"></li>
        <li><a href="<?php echo Url::to('/office/bitmoda/marketing')?>"><i class="icon-check"></i> <?= Yii::t('app', 'Задания');?></a></li>
        <li class="divider"></li>
        <li><a href="/logout"><i class="icon-key"></i> <?= Yii::t('app', 'Выйти');?></a></li>
      </ul>
    </li>
    <li class=""><a title="" href="<?php echo Url::to(['/office/user'])?>"><i class="icon icon-cog"></i> <span class="text"><?= Yii::t('app', 'Мой профиль');?></span></a></li>
    <li class=""><a title="" href="/logout"><i class="icon icon-share-alt"></i> <span class="text"><?= Yii::t('app', 'Выйти');?></span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->

<!--start-top-serch-->
<div id="search">
  <?php foreach ($this->params['user']->identity->getBalances() as $balance): ?>
    <a href="<?php echo Url::to(['/office/finance']);?>" class = "btn-success" style = "padding: 3px 15px; font-weight: bold;"><?= Yii::t('app', 'Ваш баланс: {sum} {currency}', [
      'sum' => $balance->showValue(),
      'currency' => $balance->getCurrency()->one()->title,
      ]);?>
      </a>
  <?php endforeach;?>
</div>
<!--close-top-serch-->