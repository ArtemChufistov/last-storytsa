<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="widget-box">
  <div class="widget-title bg_lo"  data-toggle="collapse" href="#collapseG3" > <span class="icon"> <i class="icon-chevron-down"></i> </span>
    <h5><?php echo Yii::t('app', 'Посление новости');?></h5>
  </div>
  <div class="widget-content nopadding updates collapse in" id="collapseG3">
    <?php foreach($lastNews as $lastNewItem):?>
      <div class="new-update clearfix"><i class="icon-ok-sign"></i>
        <div class="update-done">
          <a title="" href="<?php echo Url::to(['/office/fullnew', 'slug' => $lastNewItem->slug]);?>"><strong><?php echo $lastNewItem->title;?></strong></a>
          <span><?php echo $lastNewItem->smallDescription;?></span>
        </div>
        <div class="update-date"><span class="update-day"><?php echo date('d', strtotime($lastNewItem->date));?></span><?php echo date('m.d', strtotime($lastNewItem->date));?></div>
      </div>
    <?php endforeach;?>
  </div>
</div>