<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\modules\menu\widgets\MenuWidget;
use app\assets\bitmoda\BackAppAssetTop;
use app\assets\bitmoda\BackAppAssetBottom;
use app\modules\profile\widgets\ChangeEmailWidget;

BackAppAssetTop::register($this);
BackAppAssetBottom::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <?= Html::csrfMetaTags() ?>
  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme']   .'/layouts/partial/_head_meta_office');?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body class="hold-transition skin-purple sidebar-mini">
  <?php $this->beginBody() ?>
  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_office_head', [ 'user' => $this->params['user']->identity]);?>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_left-nav');?>
    
  <?php echo $content;?>
  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>