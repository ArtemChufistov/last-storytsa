<?php
use yii\helpers\Url;

return Yii::$app->response->redirect(Url::to(['/']));
?>