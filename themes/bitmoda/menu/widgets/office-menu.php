<?php
use yii\helpers\Url;
?>
<ul>
  <?php foreach($menu->children($menu->depth + 1)->all() as $num => $child):?>
    <?php $children = $child->children()->all();?>
    <li<?php if(Url::to([$requestUrl]) == Url::to([$child->link])):?> class="active"<?php endif;?>><a href="<?= Url::to($child->link)?>"><?= $child->icon;?> <span><?= $child->title;?></span></a></li>
  <?php endforeach;?>
</ul>