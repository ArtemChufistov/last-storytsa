<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = Yii::t('user', 'Новости - {title}', ['title' => $newsModel->title]);

?>
<div id="content">
  <div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
      ['title' => $this->title, 'url' => '' , 'icon' => 'icon-bookmark-empty'],
    ]]);?>
  <h1><?= $this->title;?></h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">

		<div class="span12">
		    <div class="widget-box">
		        <div class="widget-title"> <span class="icon"> <i class="icon-rss"></i> </span>
		            <h5><?=$newsModel->title; ?></h5>
		        </div>
		        <div class="widget-content">
		            <div class="row-fluid">
		                <div class="span3">
		                    <img src="/<?php echo $newsModel->image;?>" alt="">
		                </div>

		                <div class="span9">
		                    <p>
		                      <?php echo $newsModel->fullDescription;?>
		                    </p>

		                </div>
		            </div>
		            <div class="row-fluid">
		            </div>
		        </div>
		    </div>
		</div>

    </div>
  </div>
</div>