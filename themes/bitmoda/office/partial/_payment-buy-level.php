<?php
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\payment\models\Payment;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$matrixGift = $user->getMatrixPlaces()->one()->getMatrixGift()->one();

$paymentLevels = [];
for ($i = 1; $i <= 6; $i++) { 
	$paymentLevels[$i] = Payment::find()
	->where(['type' => Payment::TYPE_BUY_LEVEL, 'additional_info' => $i, 'status' => Payment::STATUS_OK, 'from_user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();
}

$paymentMatrix = Payment::find()->where(['type' => Payment::TYPE_BUY_PLACE, 'status' => Payment::STATUS_OK, 'from_user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();


$day30 = 60 * 60 * 24 * 30;

?>

<div class = "row">
	  <div class = "col-sm-12 text-trancpancy">
<<<<<<< HEAD
    <?php echo Yii::t('app', 'Мы разработали для Вас универсальную и эффективную стратегию, которая позволит вам в кратшайшие сроки добиться наилучших результатов.', [
=======
    <?php echo Yii::t('app', 'Мы разработали для Вас универсальную и эффективную стратегию, которая позволит Вам в кратчайшие сроки добиться наилучших результатов.', [
>>>>>>> 8ffa37eb6c9f10af7c44453602b03dbe08acb587

  ]);?>
	  </div>
 </div>
</br>
</br>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center">
		<?php echo Yii::t('app', 'Стоимость</br> уровня');?>
	</div>
	<div class ="col-sm-2 text-center">
		<?php echo Yii::t('app', 'Приглашено</br> участников');?>
	</div>
	<div class ="col-sm-2 text-center">
		<?php echo Yii::t('app', 'Прибыль</br> составит');?>
	</div>
</div>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center color-big-red">
		<?php echo Yii::t('app', '0.05 BTC');?>
	</div>
	<div class ="col-sm-2">
		<input type="text" value="" class="slider1 form-control" data-slider-min="0" data-slider-value="2" data-slider-max="2" data-slider-step="1" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
	</div>
	<div class ="col-sm-2 text-center color-big-red">
		<span class = "slidevrValue1"><?php echo Yii::t('app', '0.1');?></span> <span>BTC</span>
	</div>
	<?php if (!empty($matrixGift) && $matrixGift->left_level_1 > 0 ):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Осталось подарков: {count}', ['count' => $matrixGift->left_level_1]);?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка после получения подарков');?>
		</div>
	<?php elseif(!empty($matrixGift) && !empty($paymentMatrix) && (strtotime($paymentMatrix->date_add) + $day30) > time()):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left">
				<?php $day = floor(((strtotime($paymentMatrix->date_add) + $day30) - time())/(60*60*24)); echo $day;?> <?php echo Yii::t('app', 'дней');?>
				<?php $hour = floor(((strtotime($paymentMatrix->date_add) + $day30 - $day * 60 * 60 * 24) - time())/(60*60)); echo $hour;?> <?php echo Yii::t('app', 'часов');?>
			</a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка не ранее');?>
		</div>
	<?php elseif(!empty($matrixGift) && !empty($paymentLevels[1]) && (strtotime($paymentLevels[1]->date_add) + $day30) > time()):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left">
				<?php $day = floor(((strtotime($paymentLevels[1]->date_add) + $day30) - time())/(60*60*24)); echo $day;?> <?php echo Yii::t('app', 'дней');?>
				<?php $hour = floor(((strtotime($paymentLevels[1]->date_add) + $day30 - $day * 60 * 60 * 24) - time())/(60*60)); echo $hour;?> <?php echo Yii::t('app', 'часов');?>
			</a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка не ранее');?>
		</div>
	<?php else:?>
		<div class ="col-sm-3">
			<a href = "/profile/office/matrix?buy=1&level=1" class = "btn btn-block btn-success pull-left"><?php echo Yii::t('app', 'Уровень № 1');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Купить уровень');?>
		</div>
	<?php endif;?>
</div>
</br>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center color-big-red">
		<?php echo Yii::t('app', '0.05 BTC');?>
	</div>
	<div class ="col-sm-2">
		<input type="text" value="" class="slider2 form-control" data-slider-min="0" data-slider-value="4" data-slider-max="4" data-slider-step="1"  data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
	</div>
	<div class ="col-sm-2 text-center color-big-red">
		<span class = "slidevrValue2"><?php echo Yii::t('app', '0.2');?></span> <span>BTC</span>
	</div>
	<?php if (!empty($matrixGift) && $matrixGift->left_level_2 > 0 ):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Осталось подарков: {count}', ['count' => $matrixGift->left_level_2]);?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка после получения подарков');?>
		</div>
	<?php elseif(!empty($matrixGift) && !empty($paymentLevels[2]) && (strtotime($paymentLevels[2]->date_add) + $day30) > time()):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left">
				<?php $day = floor(((strtotime($paymentLevels[2]->date_add) + $day30) - time())/(60*60*24)); echo $day;?> <?php echo Yii::t('app', 'дней');?>
				<?php $hour = floor(((strtotime($paymentLevels[2]->date_add) + $day30 - $day * 60 * 60 * 24) - time())/(60*60)); echo $hour;?> <?php echo Yii::t('app', 'часов');?>
			</a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка не ранее');?>
		</div>
	<?php elseif ((!empty($matrixGift) && (($matrixGift->buy_level_2 > 0 && $matrixGift->buy_level_2 == $matrixGift->buy_level_1 ) || $matrixGift->buy_level_1 == 0)) || empty($matrixGift)):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Оплатить уровень 2');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Необходима покупка уровня 1');?>
		</div>
	<?php else:?>
		<div class ="col-sm-3">
			<a href = "/profile/office/matrix?buy=1&level=2" class = "btn btn-block btn-success pull-left"><?php echo Yii::t('app', 'Уровень № 2');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Купить уровень');?>
		</div>
	<?php endif;?>
</div>
</br>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center color-big-red">
		<?php echo Yii::t('app', '0.1 BTC');?>
	</div>
	<div class ="col-sm-2">
		<input type="text" value="" class="slider3 form-control" data-slider-min="0" data-slider-value="8" data-slider-max="8" data-slider-step="1" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
	</div>
	<div class ="col-sm-2 text-center color-big-red">
		<span class = "slidevrValue3"><?php echo Yii::t('app', '0.8');?></span> <span>BTC</span>
	</div>
	<?php if (!empty($matrixGift) && $matrixGift->left_level_3 > 0 ):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Осталось подарков: {count}', ['count' => $matrixGift->left_level_3]);?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка после получения подарков');?>
		</div>
	<?php elseif(!empty($matrixGift) && !empty($paymentLevels[3]) && (strtotime($paymentLevels[3]->date_add) + $day30) > time()):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left">
				<?php $day = floor(((strtotime($paymentLevels[3]->date_add) + $day30) - time())/(60*60*24)); echo $day;?> <?php echo Yii::t('app', 'дней');?>
				<?php $hour = floor(((strtotime($paymentLevels[3]->date_add) + $day30 - $day * 60 * 60 * 24) - time())/(60*60)); echo $hour;?> <?php echo Yii::t('app', 'часов');?>
			</a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка не ранее');?>
		</div>
	<?php elseif ((!empty($matrixGift) && (($matrixGift->buy_level_3 > 0 && $matrixGift->buy_level_3 == $matrixGift->buy_level_2 ) || $matrixGift->buy_level_2 == 0)) || empty($matrixGift)):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Оплатить уровень 3');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Необходима покупка уровня 2');?>
		</div>
	<?php else:?>
		<div class ="col-sm-3">
			<a href = "/profile/office/matrix?buy=1&level=3" class = "btn btn-block btn-success pull-left"><?php echo Yii::t('app', 'Уровень № 3');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Купить уровень');?>
		</div>
	<?php endif;?>
</div>
</br>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center color-big-red">
		<?php echo Yii::t('app', '0.2 BTC');?>
	</div>
	<div class ="col-sm-2">
		<input type="text" value="" class="slider4 form-control" data-slider-min="0" data-slider-value="16" data-slider-max="16" data-slider-step="1" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
	</div>
	<div class ="col-sm-2 text-center color-big-red">
		<span class = "slidevrValue4"><?php echo Yii::t('app', '16');?></span> <span> BTC</span>
	</div>
	<?php if (!empty($matrixGift) && $matrixGift->left_level_4 > 0 ):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Осталось подарков: {count}', ['count' => $matrixGift->left_level_4]);?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка после получения подарков');?>
		</div>
	<?php elseif(!empty($matrixGift) && !empty($paymentLevels[4]) && (strtotime($paymentLevels[4]->date_add) + $day30) > time()):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left">
				<?php $day = floor(((strtotime($paymentLevels[4]->date_add) + $day30) - time())/(60*60*24)); echo $day;?> <?php echo Yii::t('app', 'дней');?>
				<?php $hour = floor(((strtotime($paymentLevels[4]->date_add) + $day30 - $day * 60 * 60 * 24) - time())/(60*60)); echo $hour;?> <?php echo Yii::t('app', 'часов');?>
			</a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка не ранее');?>
		</div>
	<?php elseif ((!empty($matrixGift) && (($matrixGift->buy_level_4 > 0 && $matrixGift->buy_level_4 == $matrixGift->buy_level_3 ) || $matrixGift->buy_level_3 == 0)) || empty($matrixGift)):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Оплатить уровень 4');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Необходима покупка уровня 3');?>
		</div>
	<?php else:?>
		<div class ="col-sm-3">
			<a href = "/profile/office/matrix?buy=1&level=4" class = "btn btn-block btn-success pull-left"><?php echo Yii::t('app', 'Уровень № 4');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Купить уровень');?>
		</div>
	<?php endif;?>
</div>
</br>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center color-big-red">
		<?php echo Yii::t('app', '0.5 BTC');?>
	</div>
	<div class ="col-sm-2">
		<input type="text" value="" class="slider5 form-control" data-slider-min="0" data-slider-value="32" data-slider-max="32" data-slider-step="1" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
	</div>
	<div class ="col-sm-2 text-center color-big-red">
		<span class = "slidevrValue5"><?php echo Yii::t('app', '32');?></span> <span>BTC</span>
	</div>
	<?php if (!empty($matrixGift) && $matrixGift->left_level_5 > 0 ):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Осталось подарков: {count}', ['count' => $matrixGift->left_level_5]);?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка после получения подарков');?>
		</div>
	<?php elseif(!empty($matrixGift) && !empty($paymentLevels[5]) && (strtotime($paymentLevels[5]->date_add) + $day30) > time()):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left">
				<?php $day = floor(((strtotime($paymentLevels[5]->date_add) + $day30) - time())/(60*60*24)); echo $day;?> <?php echo Yii::t('app', 'дней');?>
				<?php $hour = floor(((strtotime($paymentLevels[5]->date_add) + $day30 - $day * 60 * 60 * 24) - time())/(60*60)); echo $hour;?> <?php echo Yii::t('app', 'часов');?>
			</a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка не ранее');?>
		</div>
	<?php elseif ((!empty($matrixGift) && (($matrixGift->buy_level_5 > 0 && $matrixGift->buy_level_5 == $matrixGift->buy_level_4 ) || $matrixGift->buy_level_4 == 0)) || empty($matrixGift)):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Оплатить уровень 5');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Необходима покупка уровня 4');?>
		</div>
	<?php else:?>
		<div class ="col-sm-3">
			<a href = "/profile/office/matrix?buy=1&level=5" class = "btn btn-block btn-success pull-left"><?php echo Yii::t('app', 'Уровень № 5');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Купить уровень');?>
		</div>
	<?php endif;?>
</div>
</br>
<div class = "row no-margin">
	<div class ="col-sm-2 text-center color-big-red">
		<?php echo Yii::t('app', '1 BTC');?>
	</div>
	<div class ="col-sm-2">
		<input type="text" value="" class="slider6 form-control" data-slider-min="0" data-slider-value="64" data-slider-max="64" data-slider-step="1" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
	</div>
	<div class ="col-sm-2 text-center color-big-red">
		<span class = "slidevrValue6"><?php echo Yii::t('app', '64');?></span> <span>BTC</span>
	</div>
	<?php if (!empty($matrixGift) && $matrixGift->left_level_6 > 0 ):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Осталось подарков: {count}', ['count' => $matrixGift->left_level_6]);?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка после получения подарков');?>
		</div>
	<?php elseif(!empty($matrixGift) && !empty($paymentLevels[6]) && (strtotime($paymentLevels[6]->date_add) + $day30) > time()):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left">
				<?php $day = floor(((strtotime($paymentLevels[6]->date_add) + $day30) - time())/(60*60*24)); echo $day;?> <?php echo Yii::t('app', 'дней');?>
				<?php $hour = floor(((strtotime($paymentLevels[6]->date_add) + $day30 - $day * 60 * 60 * 24) - time())/(60*60)); echo $hour;?> <?php echo Yii::t('app', 'часов');?>
			</a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Следующая покупка не ранее');?>
		</div>
	<?php elseif ((!empty($matrixGift) && (($matrixGift->buy_level_6 > 0 && $matrixGift->buy_level_6 == $matrixGift->buy_level_5 ) || $matrixGift->buy_level_5 == 0)) || empty($matrixGift)):?>
		<div class ="col-sm-3">
			<a href = "#" class = "btn btn-block btn-default pull-left"><?php echo Yii::t('app', 'Оплатить уровень 6');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Необходима покупка уровня 5');?>
		</div>
	<?php else:?>
		<div class ="col-sm-3">
			<a href = "/profile/office/matrix?buy=1&level=6" class = "btn btn-block btn-success pull-left"><?php echo Yii::t('app', 'Уровень № 6');?></a>
		</div>
		<div class ="col-sm-3">
			<?php echo Yii::t('app', 'Купить уровень');?>
		</div>
	<?php endif;?>
</div>
<?php $this->registerJs("
	$(function () {
		\$('.slider1').slider({
			formatter: function(value) {
				var btcvalue = 0.05 * value;
				jQuery('.slidevrValue1').html(btcvalue);
				return 'Текущее значение: ' + value;
			}
		});
		\$('.slider2').slider({
			formatter: function(value) {
				var btcvalue = 0.05 * value;
				jQuery('.slidevrValue2').html(btcvalue.toFixed(2));
				return 'Текущее значение: ' + value;
			}
		});
		\$('.slider3').slider({
			formatter: function(value) {
				var btcvalue = 0.1 * value;
				jQuery('.slidevrValue3').html(btcvalue.toFixed(2));
				return 'Текущее значение: ' + value;
			}
		});
		\$('.slider4').slider({
			formatter: function(value) {
				var btcvalue = 0.2 * value;
				jQuery('.slidevrValue4').html(btcvalue.toFixed(2));
				return 'Текущее значение: ' + value;
			}
		});
		\$('.slider5').slider({
			formatter: function(value) {
				var btcvalue = 0.5 * value;
				jQuery('.slidevrValue5').html(btcvalue);
				return 'Текущее значение: ' + value;
			}
		});
		\$('.slider6').slider({
			formatter: function(value) {
				var btcvalue = 1 * value;
				jQuery('.slidevrValue6').html(btcvalue);
				return 'Текущее значение: ' + value;
			}
		});
	});
", View::POS_END);?>