<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;
use app\modules\finance\models\Transaction;

$matrix = Matrix::find()->where(['user_id' => $user->id])->one();

?>
<div class="widget-box">
  <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
    <h5><?php echo Yii::t('app', 'Ваша реферальная ссылка');?></h5>
  </div>
  <div class="widget-content">
    <?php if (!empty($matrix)):?>
      <div class="form-group">
          <?php echo Html::input('text', 'refLink', Url::home(true) . '?ref=' . $user->login, ['id' => 'refLink', 'class' => 'form-control span11' , 'readonly' => true]);?>
          <?php echo Html::input('button', '', Yii::t('app', 'Скопировать в буфер обмена'), ['class' => 'btn-clipboard btn btn-success pull-left span11', 'data-clipboard-target' => '#refLink']);?>
      </div>
    <?php else:?>
      <?php echo Yii::t('app', 'Реферальная ссылка будет доступна только после начала </br> <a style = "font-weight: bold; text-decoration:underline;" href = "/office/bitmoda/marketing">Выполнения задания 2</a>')?>
    <?php endif;?>
  </div>
</div>

<?php $this->registerJs('new Clipboard(".btn-clipboard");');?>
