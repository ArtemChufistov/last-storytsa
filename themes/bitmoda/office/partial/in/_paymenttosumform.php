<?php
use app\modules\finance\models\Currency;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Пополнение лицевого счёта - Введите сумму');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$currencyVoucher = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();

$defaultBuy = 0.006;
?>

<div class="span12">
  <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5><?=Yii::t('app', 'Ввод суммы')?></h5>
    </div>
    <div class="widget-content">
      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_tabs', [
        'active' => 1,
      ]); ?>
      <div class="row-fluid">
        <div class="span12">
          <h3 class="content-box-header bg-green" style = "text-align: center;">
            <?php echo Yii::t('app', 'Введите сумму для пополнения'); ?>
          </h3>
          <div class="content-box-wrapper">
            <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>
              <?=$form->field($paymentForm, 'to_sum', ['template' => '
              <div class="form-group">
                <div class="control-group">
                  <label class="control-label">' . Yii::t('app', 'Сумма к пополнению') . '</label>
                  <div class="controls">{input}{error}</div>
                </div>
              </div>'])->textInput(['value' => $defaultBuy, 'class' => 'span11', 'readonly' => true])?>

              <div class="form-actions">
                <?=Html::submitButton(Yii::t('app', 'Далее'), ['class' => 'btn btn-lg btn-success'])?>
              </div>
            <?php ActiveForm::end();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
