<?php
use lowbase\user\UserAsset;

$this->title = Yii::t('user', 'Пополнение лицевого счёта');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

?>

<div class="span12">
  <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5><?=Yii::t('app', 'Ваша оплата')?></h5>
    </div>
    <div class="widget-content">
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_tabs', [
          'active' => 2,
          'paymentForm' => $paymentForm,
        ]); ?>
      <div class="row-fluid">
        <div class="span12">
          <h3 class="content-box-header bg-green" style = "text-align: center;">
            <?php echo Yii::t('app', 'Ваш перевод'); ?>
          </h3>
          <div class="content-box-wrapper">
              <?php echo Yii::$app->controller->renderPartial(
              '@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_successpay_' . $paymentForm->getFromPaySystem()->one()->key, [
                'paymentForm' => $paymentForm,
                'user' => $user,
              ]); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>