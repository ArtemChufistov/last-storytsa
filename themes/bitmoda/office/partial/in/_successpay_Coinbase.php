<?php
use app\modules\finance\models\Currency;

?>

  <form action="#" class="form-horizontal">
    <div class="control-group" style = "text-align: center;">
    	</br>
		<b><?php echo Yii::t('app', 'Ваш кошелёк'); ?>:</b></br></br> <strong style = "font-size: 22px;"> <?php echo $paymentForm->wallet; ?></strong>
    	</br>
    	</br>
    </div>
    <div class="control-group" style = "text-align: center;">
		<b><?php echo Yii::t('app', 'Сумма'); ?>:</b>
		</br>
		</br>
		<strong style = "font-size: 22px;"><?php echo number_format($paymentForm->fromSumWithComission(), 6); ?> <?php echo $paymentForm->getFromCurrency()->one()->key; ?></strong>
		</br>
    	</br>
    </div>
    <div class="control-group" style = "text-align: center;">

    	<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Дополнительная информация'); ?>:</h2>
		<p><?php echo Yii::t('app', 'Для завершения <strong>задания №1</strong> вам необходимо пополнить свой кошелёк: <strong>{wallet}</strong> на сумму <strong>{sum} BTC</strong>', [
			'countCitt' => $paymentForm->to_sum,
			'sum' => number_format($paymentForm->fromSumWithComission(), 6),
			'currency' => $paymentForm->getFromCurrency()->one()->key,
			'wallet' => $paymentForm->wallet,
		]); ?></p>
    </div>
    <div class="control-group" style = "text-align: center;">
		</br>
		</br>
    	<?php if ($paymentForm->getFromCurrency()->one()->key == Currency::KEY_BTC): ?>
		<?php echo Yii::t('app', 'Для удобства перевода вы можете воспользоваться QR кодом:'); ?>
		</br>
			<img style = "width: 200px;" src = "/office/qrcode/<?php echo $paymentForm->wallet; ?>/<?php echo $paymentForm->fromSumWithComission(); ?>">
		<?php endif;?>
    </div>
  </form>



