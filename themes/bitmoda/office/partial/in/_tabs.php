<?php
use yii\helpers\Url;
?>
<br>
<div class="row-fluid">
  <div class="span6 step-card in<?php if (in_array($active, [1, 2])): ?> active<?php endif;?>">
    <span class="number">1</span>
    <center>
      <h5><?php echo Yii::t('app', '1 ШАГ'); ?></h5>
      <p><?php echo Yii::t('app', 'Ввод суммы'); ?></p>
    </center>
  </div>
  <div class="span6 step-card in<?php if (in_array($active, [2])): ?> active<?php endif;?>">
    <span class="number">2</span>
    <center>
      <h5><?php echo Yii::t('app', 'Пополнение'); ?></h5>
      <p><?php echo Yii::t('app', 'Проведение операции'); ?></p>
    </center>
  </div>
</div>
