<div class="box box-success">
  <div class="box-header">
		<h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i><?php echo Yii::t('app', 'Оплаты вам')?></h3>
  </div>

  <div class="box-body no-padding">
    <table class="table matrixPlaces">
      <tr>
        <th style="width: 10px">№</th>
        <th><?php echo Yii::t('app', 'Участник');?></th>
        <th><?php echo Yii::t('app', 'Статус');?></th>
        <th><?php echo Yii::t('app', 'Тип');?></th>
        <th><?php echo Yii::t('app', 'Сумма');?></th>
        <th><?php echo Yii::t('app', 'Дата');?></th>
      </tr>
      <?php foreach($toUserPayments as $num => $payment): ?>
            <tr>
              <td><a href = "/profile/office/matrix?paymenthash='<?php echo $payment->hash;?>"><?php echo $num + 1;?></a></td>
              <td><a href = "/profile/office/matrix?paymenthash='<?php echo $payment->hash;?>"><?php echo $payment->getFromUser()->one()->login;?></a></td>
              <td><a href = "/profile/office/matrix?paymenthash=<?php echo $payment->hash;?>"><?php echo $payment->getStatusTitle();?></a></td>
              <td><a href = "/profile/office/matrix?paymenthash=<?php echo $payment->hash;?>"><?php echo $payment->getTypeTitle();?></a></td>
              <td><a href = "/profile/office/matrix?paymenthash=<?php echo $payment->hash;?>"><span class="badge bg-green">+<?php echo $payment->sum . ' ' . $payment->getCurrency()->one()->title;?></span></a></td>
              <td><a href = "/profile/office/matrix?paymenthash=<?php echo $payment->hash;?>"><?php echo date('H:i d/m/Y', strtotime($payment->date_add));?></a></td>
            </tr>
        </a>
	<?php endforeach;?>
    </table>
  </div>
</div>