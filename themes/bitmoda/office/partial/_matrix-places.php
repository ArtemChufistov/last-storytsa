<?php
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixPref;
?>
<div class="box box-success">
	<div class="box-header with-border">
	  <h3 class="box-title"><i class="fa fa-database" aria-hidden="true"></i><?php echo Yii::t('app', 'Ваши места');?></h3>
	</div>
	<div class="box-body">
		<div class="panel-group user-places" id="accordion" role="tablist" aria-multiselectable="true">

		  <?php foreach($rootMatrixes as $num => $rootMatrix):?>
			  <?php 
				  $countChildren = MatrixPref::find()->where(['key' => MatrixPref::KEY_COUNT_CHILDREN, 'matrix_id' => $rootMatrix->id])->one();
				  $depth = MatrixPref::find()->where(['key' => MatrixPref::KEY_DEPTH, 'matrix_id' => $rootMatrix->id])->one();
				  $maxChildren = pow($countChildren->value, $depth->value);

				  if ($depth->value == 2 ){
				  	$maxChildren += pow($countChildren->value, $depth->value - 1);
				  }

				  $matrixPlaces = Matrix::find()->where(['user_id' => $user->id, 'root_id' => $rootMatrix->id])->all();

				  if (empty($matrixPlaces)){
				  	continue;
				  }
			  ;?>
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#ac<?php echo $rootMatrix->slug?>" aria-expanded="true" aria-controls="ac<?php echo $rootMatrix->slug?>">
			          <?php echo $rootMatrix->name;?>
			        </a>
			      </h4>
			    </div>
			    <div id="ac<?php echo $rootMatrix->slug?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
					<ul class="users-list clearfix">
						<?php if (empty($matrixPlaces)):?>
							<h4 style = "text-align: center"><?php echo Yii::t('app', 'У вас нет мест в этой программе,</br> <a target= "_blank" href="/howitworks">как получить место</a>');?></h4>
						<?php endif;?>
				        <?php foreach($matrixPlaces as $num => $matrixPlace):?>
				        	<?php $percent = ( 100 / $maxChildren) * $matrixPlace->descendants($depth->value)->count();?>
							<li>
								<a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $matrixPlace->slug]);?>">
									<img style="width: 110px;height:110px;" src="<?php echo $user->getImage();?>" alt="<?php echo $matrixPlace->name;?>">
								</a>
								<span class="users-list-name" href=""><?php echo Yii::t('app', '{num} место', ['num' => $num + 1])?></span>
								<span class="users-list-date" style="margin-top:5px; margin-bottom:5px;">
									<span class="label label-success"><?php echo Yii::t('app','Заполнено: {percent}%', ['percent' => round($percent)]);?> </span>
								</span>
							</li>
				        <?php endforeach;?>
				    </ul>
			      </div>
			    </div>
			  </div>
		  <?php endforeach;?>

		</div>
	</div>
</div>