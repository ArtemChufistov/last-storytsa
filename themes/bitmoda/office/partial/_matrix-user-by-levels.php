<?php
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\payment\models\Payment;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

for($i = 6; $i >= 1; $i--){
  foreach ($startPlace->descendants($i)->all() as $descendant){
    if ($descendant->getLevelTo($startPlace) == $i){
      $descendants[$i][] = $descendant->getUser()->one();
    }
  }
}
?>

<div class="nav-tabs-custom">
  <!-- Tabs within a box -->
  <ul class="nav nav-tabs pull-right">
    <li class="pull-left header"><i class="ion ion-clipboard"></i><?php echo Yii::t('app', 'Партнёры')?></li>
    <?php for($i = 6; $i >= 1; $i--){?>
      <li <?php if ($i == 1):?>class="active"<?php endif;?>><a href="#partnerLevel<?php echo $i;?>" data-toggle="tab"><?php echo Yii::t('app', 'Уровень {num} ({count})', [
        'num' => $i, 'count' => count($descendants[$i])]);?></a></li>
    <?php }?>
  </ul>
  <div class="tab-content no-padding">
    <?php for($i = 6; $i >= 1; $i--){?>
      <div class="chart tab-pane <?php if ($i == 1):?>active<?php endif;?>" id="partnerLevel<?php echo $i;?>" >

        <div class="box-body no-padding">
          <ul class="users-list clearfix">
            <?php if (!empty($descendants[$i])):?>
              <?php foreach ($descendants[$i] as $descendant):?>
                  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_matrix-level-place', ['user' => $descendant]);?>
              <?php endforeach;?>
            <?php else:?>
              <h3 style = "text-align: center; margin-top: 20px; margin-bottom: 20px;"><?php echo Yii::t('app', 'На этом уровне у вас пока нет приглашённых');?></h3>
            <?php endif;?>
          </ul>
        </div>

      </div>
    <?php }?>
  </div>
</div>