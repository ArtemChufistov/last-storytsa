<?php
use app\modules\profile\widgets\UserTreeElementWidget;
use app\modules\matrix\components\StorytsaMarketing;
<<<<<<< HEAD
use app\modules\matrix\models\MatrixPref;
use app\modules\matrix\models\Matrix;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;


$childs = $currentPlace->children()->orderBy(['sort' => 'asc'])->all();

foreach($childs as $childItem){

    if ($childItem->sort == 0){
        $child1  = $childItem;
        $childs1 = $child1->children()->orderBy(['sort' => 'asc'])->all();
        foreach($childs1 as $childItem1){
            if ($childItem1->sort == 0){
                $child11 = $childItem1;
            }elseif($childItem1->sort == 1){
                $child12 = $childItem1;
            }
        }
    }elseif($childItem->sort == 1){
        $child2  = $childItem;
        $childs2 = $child2->children()->orderBy(['sort' => 'asc'])->all();
        foreach($childs2 as $childItem2){
            if ($childItem2->sort == 0){
                $child21 = $childItem2;
            }elseif($childItem2->sort == 1){
                $child22 = $childItem2;
            }
        }
    }
}

$depth = MatrixPref::find()->where(['key' => MatrixPref::KEY_DEPTH, 'matrix_id' => $rootMatrix->id])->one();

?>
<section class="management-hierarchy">

    <div class="hv-container">

        <div class="hv-wrapper">

            <ul class="timeline">

                <?php foreach($currentPlace->path()->having('depth <=' . $currentLevel)->all() as $placeItem):?>
                <li>
                    <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $placeItem->slug, 'parentPlaceSlug' => $parentPlace->slug]);?>">
                        <img class="fa" src = "<?php echo $placeItem->getUser()->one()->getImage();?>">
                    </a>
                        <div class="timeline-item"></div>
                </li>
                <?php endforeach;?>
            </ul>

            <div class="hv-item">
                <div class="hv-item-parent parent1">
                    <?php echo UserTreeElementWidget::widget([
                        'matrixCategory' => $matrixCategory,
                        'currentPlace' => $currentPlace,
                        'parentPlace' => $parentPlace,
                        'currentUser' => $user,
                        'rootMatrix' => $rootMatrix,
                        ]);?>
                </div>

                <div class="hv-item-children">

                    <div class="hv-item-child">
                        <!-- Key component -->
                        <div class="hv-item">

                            <div class="hv-item-parent <?php if($depth->value == 2):?>parent11<?php endif;?>">
                                <?php echo UserTreeElementWidget::widget([
                                    'matrixCategory' => $matrixCategory,
                                    'ancestorPlace' => $currentPlace,
                                    'currentPlace' => $child1,
                                    'parentPlace' => $parentPlace,
                                    'currentUser' => $user,
                                    'rootMatrix' => $rootMatrix,
                                    'sort' => 0,
                                ]);?>
                            </div>

                            <?php if($depth->value == 2):?>
                                <div class="hv-item-children">
                                    <div class="hv-item-child child1">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child1,
                                            'currentPlace' => $child11,
                                            'parentPlace' => $parentPlace,
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 0,
                                        ]);?>
                                    </div>

                                    <div class="hv-item-child child2">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child1,
                                            'currentPlace' => $child12,
                                            'parentPlace' => $parentPlace,
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 1,
                                        ]);?>
                                    </div>

                                </div>
                            <?php endif;?>
                        </div>
                    </div>

                    <div class="hv-item-child">
                        <!-- Key component -->
                        <div class="hv-item">

                            <div class="hv-item-parent <?php if($depth->value == 2):?>parent12<?php endif;?>">
                                <?php echo UserTreeElementWidget::widget([
                                    'matrixCategory' => $matrixCategory,
                                    'ancestorPlace' => $currentPlace,
                                    'currentPlace' => $child2, 
                                    'parentPlace' => $parentPlace, 
                                    'currentUser' => $user,
                                    'rootMatrix' => $rootMatrix,
                                    'sort' => 1,
                                ]);?>
                            </div>

                            <?php if($depth->value == 2):?>
                                <div class="hv-item-children">

                                    <div class="hv-item-child child3">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child2,
                                            'currentPlace' => $child21, 
                                            'parentPlace' => $parentPlace, 
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 0,
                                        ]);?>
                                    </div>


                                    <div class="hv-item-child child4">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child2,
                                            'currentPlace' => $child22, 
                                            'parentPlace' => $parentPlace, 
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 1,
                                        ]);?>
                                    </div>

                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->registerJs("
$('.checkBoxPlace').on('ifChecked', function(event){
  $(this).closest('form').submit();
});
$('.checkBoxPlace').on('ifUnchecked', function(event){
  $(this).closest('form').submit();
});
$('.imgAvatar').one('click', function(){
        location.href = $(this).attr('href');
})
")?>
=======
use yii\widgets\ActiveForm;
use yii\web\View;

$child1 = $currentPlace->children()->orderBy(['sort' => 'asc'])->all()[0];
$child11 = !empty($child1) ? $child1->children()->orderBy(['sort' => 'asc'])->all()[0] : '';
$child12 = !empty($child1) ? $child1->children()->orderBy(['sort' => 'asc'])->all()[1] : '';
$child2 = $currentPlace->children()->orderBy(['sort' => 'asc'])->all()[1];
$child21 = !empty($child2) ? $child2->children()->orderBy(['sort' => 'asc'])->all()[0] : '';
$child22 = !empty($child2) ? $child2->children()->orderBy(['sort' => 'asc'])->all()[1] : '';

?>

<div class="box box-success">

	<?php $form = ActiveForm::begin([
	  'id' => 'form-profile',
	  'options' => [
	      'class'=>'form',
	      'enctype'=>'multipart/form-data'
	  ],
	  ]); ?>

	<div class="box-header with-border">
	  <h3 class="box-title"><i class="fa fa-users" aria-hidden="true"></i><?php echo Yii::t('app', 'Ваша структура');?></h3>
	</div>
	<div class="box-body">

	    <section class="management-hierarchy">

	        <div class="hv-container">

	            <div class="hv-wrapper">

	                <ul class="timeline">

	                	<?php foreach($currentPlace->path()->having('depth <=' . $currentLevel)->all() as $placeItem):?>
						<li>
							<a href = "/profile/office/matrix<?php echo !empty($startPlace) ? '/'. $startPlace->slug : '';?><?php echo '/' . $placeItem->slug;?>">
							    <img class="fa" src = "<?php echo $placeItem->getUser()->one()->getImage();?>">
							</a>
								<div class="timeline-item"></div>
						</li>
						<?php endforeach;?>
					</ul>

	                <div class="hv-item">
	                    <div class="hv-item-parent parent1">
	                    	<?php echo UserTreeElementWidget::widget(['currentPlace' => $currentPlace, 'startPlace' => $startPlace]);?>
	                    </div>

	                    <div class="hv-item-children">

	                        <div class="hv-item-child">
	                            <!-- Key component -->
	                            <div class="hv-item">

	                                <div class="hv-item-parent parent11">
	                                	<?php echo UserTreeElementWidget::widget(['currentPlace' => $child1, 'startPlace' => $startPlace]);?>
	                                </div>

	                                <div class="hv-item-children">
                               			<div class="hv-item-child child1">
	                                        <?php echo UserTreeElementWidget::widget(['currentPlace' => $child11, 'startPlace' => $startPlace]);?>
	                                    </div>

                                		<div class="hv-item-child child2">
		                                    <?php echo UserTreeElementWidget::widget(['currentPlace' => $child12, 'startPlace' => $startPlace]);?>
	                                    </div>

	                                </div>
	                            </div>
	                        </div>

	                        <div class="hv-item-child">
	                            <!-- Key component -->
	                            <div class="hv-item">

	                                <div class="hv-item-parent parent12">
										<?php echo UserTreeElementWidget::widget(['currentPlace' => $child2, 'startPlace' => $startPlace]);?>
	                                </div>

	                                <div class="hv-item-children">

	                                    <div class="hv-item-child child3">
	                                        <?php echo UserTreeElementWidget::widget(['currentPlace' => $child21, 'startPlace' => $startPlace]);?>
	                                    </div>


	                                    <div class="hv-item-child child4">
	                                        <?php echo UserTreeElementWidget::widget(['currentPlace' => $child22, 'startPlace' => $startPlace]);?>
	                                    </div>

	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>
</div>
<?php ActiveForm::end();?>

<?php $this->registerJs("
$('.imgAvatar').one('click', function(){
	location.href = $(this).attr('href');
})
")?>
>>>>>>> 8ffa37eb6c9f10af7c44453602b03dbe08acb587
