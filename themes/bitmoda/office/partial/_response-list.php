
<?php
use yii\web\View;
?>

<div class="widget-box">
  <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
    <h5><?php echo Yii::t('app', 'Ваши отзывы');?></h5>
  </div>
  <?php if (empty($responseArray)):?>
    <div class="widget-content">
      <p style = "font-size: 16px;"><?= Yii::t('app', 'У вас нет отзывов, пожалуйста добавьте свой отзыв о проекте');?></p>
    </div>
  <?php else:?>
  <div class="widget-content table-bordered nopadding">
    <table class="table table-striped">
      <tr>
        <th><?php echo Yii::t('app', 'Имя');?></th>
        <th><?php echo Yii::t('app', 'Дата добавления');?></th>
        <th><?php echo Yii::t('app', 'Текст');?></th>
        <th><?php echo Yii::t('app', 'Действие');?></th>
      </tr>

      <?php foreach($responseArray as $response):?>
        <tr>
          <td><a href = "" data-toggle="modal" data-target=".modalResp<?php echo $response->id;?>"><?php echo $response->name;?></a></td>
          <td><a href = "" data-toggle="modal" data-target=".modalResp<?php echo $response->id;?>"><?php echo date('d-m-Y',strtotime($response->date));?></a></td>
          <td><a href = "" data-toggle="modal" data-target=".modalResp<?php echo $response->id;?>"><?php echo substr(strip_tags($response->text), 0, 30);?></a></td>
          <td style="width: 100px;"><a class = "deleteRespone" href = "" id = "<?php echo $response->id;?>"><?php echo Yii::t('app', 'Удалить');?></a></td>
        </tr>
      <?php endforeach;?>

    </table>
  </div>
  <?php endif;?>
</div>

<?php $this->registerJs("
  jQuery(function () {
    jQuery('.deleteRespone').on('click', function(){
      $.get('/profile/office/deleteresponse/' + $(this).attr('id'), function() {
        console.log(1111);
        return false;
      });
    })
    jQuery('.textarea').wysihtml5();
  });
", View::POS_END);?>
