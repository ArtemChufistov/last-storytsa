  <form action="#" class="form-horizontal">

    <div class="control-group" style = "text-align: center;">
    	</br>
		<b><?php echo Yii::t('app', 'С учётом комиссии сети Bitcoin вы получите'); ?>:</b></br>
				<strong style = "font-size: 22px;"><?php echo number_format($paymentForm->to_sum - $paymentForm->comission_to_sum, 6); ?> <?php echo $paymentForm->getToCurrency()->one()->key; ?></strong>
    	</br>
    	</br>
    </div>
    <div class="control-group" style = "text-align: center;">
    	</br>
		<b><?php echo Yii::t('app', 'Кошелёк для перевода'); ?>:</b></br><strong style = "font-size: 22px;"> <?php echo $paymentForm->wallet; ?></strong>
    	</br>
    	</br>
    </div>
    <div class="control-group" style = "text-align: center;">
    	</br>
		<b><?php echo Yii::t('app', 'Статус'); ?>:</b></br><strong style = "font-size: 22px;"> <?php echo $paymentForm->getStatusTitle(); ?></strong>
    	</br>
    	</br>
    </div>
  </form>
