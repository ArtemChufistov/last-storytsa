<?php
use app\modules\finance\widgets\PayWalletPasswordWidget;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Перевод BitCoin - Платёжные реквизиты');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

?>

<div class="span12">
  <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5><?=Yii::t('app', 'Реквизиты перевода')?></h5>
    </div>
    <div class="widget-content">
      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_tabs', [
        'active' => 2,
      ]); ?>
      <div class="row-fluid">
        <div class="span12">
          <h3 class="content-box-header bg-green" style = "text-align: center;">
            <?php echo Yii::t('app', 'Реквизиты для перевода'); ?>
          </h3>

          <div class="content-box-wrapper">
            <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal']]]);?>

                <?=$form->field($paymentForm, 'wallet', ['template' => '<div class="form-group">
                <div class="control-group">
                  <label class="control-label">' . Yii::t('app', 'Кошелёк BitCoin') . '</label>
                  <div class="controls">{input}{error}</div>
                </div>
              </div>'])->textInput(['value' => $paymentForm->wallet, 'class' => 'span11'])?>

                <?=$form->field($paymentForm, 'pay_password', ['template' =>
                    '<div class="form-group">
                <div class="control-group">
                  <label class="control-label">' . Yii::t('app', 'Платёжный пароль') . '</label>
                  <div class="controls">{input}{error}</div>
                </div>
              </div>', ])->textInput(['value' => $paymentForm->pay_password, 'class' => 'span11'])?>

              <div class="form-actions">
                <?=Html::submitButton(Yii::t('app', 'Далее'), ['class' => 'btn btn-lg btn-success'])?>
              </div>
            <?php ActiveForm::end();?>

            <div style="text-align: center;"><?php echo PayWalletPasswordWidget::widget(['user' => $user]); ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>