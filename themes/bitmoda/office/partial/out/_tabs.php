<?php
use yii\helpers\Url;
?>
<br>
<div class="row-fluid">
  <div class="span4 step-card out<?php if (in_array($active, [1, 2, 3])): ?> active<?php endif;?>">
    <span class="number">1</span>
    <center>
      <h5><?php echo Yii::t('app', '1 ШАГ'); ?></h5>
      <p><?php echo Yii::t('app', 'Ввод суммы'); ?></p>
    </center>
  </div>
  <div class="span4 step-card out<?php if (in_array($active, [2, 3])): ?> active<?php endif;?>">
    <span class="number">2</span>
    <center>
      <h5><?php echo Yii::t('app', 'Реквизиты'); ?></h5>
      <p><?php echo Yii::t('app', 'Укажите кошелёк BitCoin'); ?></p>
    </center>
  </div>
  <div class="span4 step-card out<?php if (in_array($active, [3])): ?> active<?php endif;?>">
    <span class="number">3</span>
    <center>
      <h5><?php echo Yii::t('app', 'Перевод'); ?></h5>
      <p><?php echo Yii::t('app', 'Проведение операции'); ?></p>
    </center>
  </div>
</div>
