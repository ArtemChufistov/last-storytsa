<?php
use lowbase\user\UserAsset;


$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

?>

<div class="span12">
  <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5><?=Yii::t('app', 'Детали перевода')?></h5>
    </div>
    <div class="widget-content">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_tabs', [
  'active' => 3,
  'paymentForm' => $paymentForm,
]); ?>
      <div class="row-fluid">
        <div class="span12">
          <h3 class="content-box-header bg-green" style = "text-align: center;">
            <?php echo Yii::t('app', 'Операция сформирована'); ?>
          </h3>
          <div class="content-box-wrapper">
                      <?php echo Yii::$app->controller->renderPartial(
  '@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_successpay_' . $paymentForm->getToPaySystem()->one()->key, [
    'paymentForm' => $paymentForm,
    'user' => $user,
  ]); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
