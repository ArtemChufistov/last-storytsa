<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = Yii::t('user', 'Видео ролики');

?>
<div id="content">
  <div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
      ['title' => $this->title, 'url' => '' , 'icon' => 'icon-bookmark-empty'],
    ]]);?>
  <h1><?= $this->title;?></h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <?php echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_list_video',
            'layout' => "{items}\n{pager}",
        ]);

        ?>
    </div>
  </div>
</div>