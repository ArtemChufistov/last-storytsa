<?php
  $this->title = Yii::t('app', 'Пополнение счета');
?>
<div id="content">
<div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
      ['title' => 'Финансы', 'url' => '/office/finance' , 'icon' => 'icon-money'],
      ['title' => $this->title, 'url' => '/office/in' , 'icon' => 'icon-money'],
    ]]);?>
  <h1><?= $this->title;?></h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_' . strtolower(\yii\helpers\StringHelper::basename(get_class($paymentForm))),
    ['paymentForm' => $paymentForm, 'user' => $user]); ?>
  </div>
</div>
</div>
