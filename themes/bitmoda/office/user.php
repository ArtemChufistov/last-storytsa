<?php

use app\modules\profile\components\AuthKeysManager;
use yii\authclient\widgets\AuthChoice;
use lowbase\user\models\Country;
use app\modules\profile\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Мой профиль');

?>
<div id="content">
<div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
      ['title' => $this->title, 'url' => '/office/user' , 'icon' => 'icon-user-md'],
    ]]);?>
  <h1><?=Yii::t('app', 'Мой профиль')?></h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5><?=Yii::t('app', 'Личные данные')?></h5>
        </div>
        <div class="widget-content nopadding">
          <?php $form = ActiveForm::begin([
              'id' => 'form-profile',
              'options' => [
                'class' => 'form-horizontal',
                'enctype'=>'multipart/form-data'
              ],
          ]); ?>
            <?= $form->field($user, 'first_name', [
                'template' => '
                <div class="control-group">
                  <label class="control-label">' . $user->getAttributeLabel('first_name') . '</label>
                  <div class="controls">
                    {input}
                  </div>
                </div>
                '
            ])->textInput([
                'maxlength' => true,
                'placeholder' => $user->getAttributeLabel('first_name'),
                'class' => 'span11',
                'readonly' => true
            ]) ?>
            <?= $form->field($user, 'last_name', [
                'template' => '
                <div class="control-group">
                  <label class="control-label">' . $user->getAttributeLabel('last_name') . '</label>
                  <div class="controls">
                    {input}
                  </div>
                </div>
                '
            ])->textInput([
                'maxlength' => true,
                'placeholder' => $user->getAttributeLabel('last_name'),
                'class' => 'span11',
                'readonly' => true
            ]) ?>
            <?= $form->field($user, 'phone', [
                'template' => '
                <div class="control-group">
                  <label class="control-label">' . $user->getAttributeLabel('phone') . '</label>
                  <div class="controls">
                    {input}
                  </div>
                </div>
                '
            ])->textInput([
                'id' => 'mask-phone',
                'maxlength' => true,
                'placeholder' => $user->getAttributeLabel('phone'),
                'class' => 'span11',
            ]) ?>
            <?= $form->field($user, 'skype', [
                'template' => '
                <div class="control-group">
                  <label class="control-label">' . $user->getAttributeLabel('skype') . '</label>
                  <div class="controls">
                    {input}
                  </div>
                </div>
                '
            ])->textInput([
                'maxlength' => true,
                'placeholder' => $user->getAttributeLabel('skype'),
                'class' => 'span11'
            ]) ?>
            <?= $form->field($user, 'birthday', [
                'template' => '
                <div class="control-group">
                  <label class="control-label">' . $user->getAttributeLabel('birthday') . '</label>
                  <div class="controls">
                    {input}
                  </div>
                </div>
                '
            ])->textInput([
                'id' => 'mask-birthday',
                'maxlength' => true,
                'placeholder' => $user->getAttributeLabel('birthday'),
                'class' => 'datepicker span11',
                'data-date-format' => 'dd.mm.yyyy',
                'data-date' => date("d.m.Y", strtotime($user->birthday)),
                'value' => date("d.m.Y", strtotime($user->birthday)),
            ]) ?>
            <div class="form-actions">
              <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
            </div>
          <?php ActiveForm::end();?>
        </div>
      </div>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5><?=Yii::t('app', 'Ваш E-mail')?></h5>
        </div>
        <div class="widget-content nopadding">
          <?php $form = ActiveForm::begin([
              'id' => 'form-profile',
              'options' => [
                'class' => 'form-horizontal',
                'enctype'=>'multipart/form-data'
              ],
          ]); ?>
            <?= $form->field($user, 'email', [
                'template' => '
                <div class="control-group">
                  <label class="control-label">' . $user->getAttributeLabel('email') . '</label>
                  <div class="controls">
                      {input}
                  </div>
                </div>
                '
            ])->textInput([
                'maxlength' => true,
                'disabled' => true,
                'placeholder' => '',
                'class' => 'span11'
            ]) ?>
            <?= $form->field($user, 'email_confirm_token', [
                'template' => '
                <div class="control-group">
                  <label class="control-label">' . $user->getAttributeLabel('email_confirm_token') . '</label>
                  <div class="controls">
                      {input}
                  </div>
                </div>
                ',
                'options' => [
                    'style' => $oldUser->email_confirm_token == '' ? 'display: none;' : ''
                ]
            ])->textInput([
                'maxlength' => true,
                'value' => '',
                'placeholder' => '',
                'class' => 'span11'
            ]) ?>

          <?php ActiveForm::end();?>
        </div>
      </div>
    </div>
    <div class="span6">
      <div class="widget-box">
        <div class="widget-content nopadding">
          <?php $form = ActiveForm::begin([
              'id' => 'form-profile',
              'options' => [
                'class' => 'form-horizontal',
                'enctype'=>'multipart/form-data'
              ],
          ]); ?>
            <div class="control-group">
              <div class="avatar">
                <img class="img" src="<?=isset($user->image)?'/'.$user->image:'/cryptofund/images/gravater.jpg'?>" />
              </div>
              <label class="control-label"><?=Yii::t('app', 'Сменить аватар')?></label>
              <?= $form->field($user, 'photo',
              [
                'template' => '
                <div class="controls">
                  {input}
                </div>
                '
              ])->fileInput()
              ?>
            </div>
            <div class="form-actions">
              <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
              <?=$user->image?Html::a(Yii::t('app', 'Удалить'), ['/admin/profile/office/remove'], ['class' => 'btn btn-danger']):''?>
            </div>
          <?php ActiveForm::end();?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php $this->registerJs('
  $(document).ready(function(){
      $(\'input[type=checkbox],input[type=radio],input[type=file]\').uniform();

      $(\'select\').select2();
      $(\'.colorpicker\').colorpicker();
      $(\'.datepicker\').datepicker();
  });

  $(document).ready(function() {

      //------------- Tags plugin  -------------//

      $("#tags").select2({
          tags:["red", "green", "blue", "orange"]
      });

      //------------- Elastic textarea -------------//
      if ($(\'textarea\').hasClass(\'elastic\')) {
          $(\'.elastic\').elastic();
      }

      //------------- Input limiter -------------//
      if ($(\'textarea\').hasClass(\'limit\')) {
          $(\'.limit\').inputlimiter({
              limit: 100
          });
      }

      //------------- Masked input fields -------------//
      $("#mask-birthday").mask("99.99.9999");

      //------------- Toggle button  -------------//

      $(\'.normal-toggle-button\').toggleButtons();
      $(\'.text-toggle-button\').toggleButtons({
          width: 140,
          label: {
              enabled: "ONLINE",
              disabled: "OFFLINE"
          }
      });
      $(\'.iToggle-button\').toggleButtons({
          width: 70,
          label: {
              enabled: "<span class=\'icon16 icomoon-icon-checkmark-2 white\'></span>",
              disabled: "<span class=\'icon16 icomoon-icon-cancel-3 white marginL5\'></span>"
          }
      });

      //------------- Spinners with steps  -------------//
      $( "#spinner1" ).spinner();

      /*Demacial*/
      $( "#spinner2" ).spinner({
        step: 0.01,
        numberFormat: "n"
      });

      /*Custom step size*/
      $( "#spinner3" ).spinner({
        step: 5
      });

      /*Currency spinner*/
      $( "#spinner4" ).spinner({
          numberFormat: "C"
      });

      //------------- Colorpicker -------------//
      if($(\'div\').hasClass(\'picker\')){
          $(\'.picker\').farbtastic(\'#color\');
      }
      //------------- Datepicker -------------//
      if($(\'#datepicker\').length) {
          $("#datepicker").datepicker({
              showOtherMonths:true
          });
      }
      if($(\'#datepicker-inline\').length) {
          $(\'#datepicker-inline\').datepicker({
              inline: true,
              showOtherMonths:true
          });
      }

      //------------- Combined picker -------------//
      if($(\'#combined-picker\').length) {
          $(\'#combined-picker\').datetimepicker();
      }

      //------------- Time entry (picker) -------------//
      $(\'#timepicker\').timeEntry({
          show24Hours: true,
          spinnerImage: \'\'
      });
      $(\'#timepicker\').timeEntry(\'setTime\', \'22:15\')

      //------------- Select plugin -------------//
      $("#select1").select2();
      $("#select2").select2();

      //--------------- Dual multi select ------------------//
      $.configureBoxes();

      //--------------- Tinymce ------------------//
      $(\'textarea.tinymce\').tinymce({
          // Location of TinyMCE script
          script_url : \'plugins/forms/tiny_mce/tiny_mce.js\',

          // General options
          theme : "advanced",
          plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

          // Theme options
          theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
          theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
          theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
          theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
          theme_advanced_toolbar_location : "top",
          theme_advanced_toolbar_align : "left",
          theme_advanced_statusbar_location : "bottom",
          theme_advanced_resizing : true,

          // Example content CSS (should be your site CSS)
          content_css : "css/main.css",

          // Drop lists for link/image/media/template dialogs
          template_external_list_url : "lists/template_list.js",
          external_link_list_url : "lists/link_list.js",
          external_image_list_url : "lists/image_list.js",
          media_external_list_url : "lists/media_list.js",

          // Replace values for the template plugin
          template_replace_values : {
              username : "SuprUser",
              staffid : "991234"
          }
      });

      //Boostrap modal
      $(\'#myModal\').modal({ show: false});

      //add event to modal after closed
      $(\'#myModal\').on(\'hidden\', function () {
          console.log(\'modal is closed\');
      })

  });//End document ready functions

  //sparkline in sidebar area
  /*
  var positive = [1,5,3,7,8,6,10];
  var negative = [10,6,8,7,3,5,1]
  var negative1 = [7,6,8,7,6,5,4]

  $(\'#stat1\').sparkline(positive,{
      height:15,
      spotRadius: 0,
      barColor: \'#9FC569\',
      type: \'bar\'
  });
  $(\'#stat2\').sparkline(negative,{
      height:15,
      spotRadius: 0,
      barColor: \'#ED7A53\',
      type: \'bar\'
  });
  $(\'#stat3\').sparkline(negative1,{
      height:15,
      spotRadius: 0,
      barColor: \'#ED7A53\',
      type: \'bar\'
  });
  $(\'#stat4\').sparkline(positive,{
      height:15,
      spotRadius: 0,
      barColor: \'#9FC569\',
      type: \'bar\'
  });
  //sparkline in widget
  $(\'#stat5\').sparkline(positive,{
      height:15,
      spotRadius: 0,
      barColor: \'#9FC569\',
      type: \'bar\'
  });

  $(\'#stat6\').sparkline(positive, {
      width: 70,//Width of the chart - Defaults to \'auto\' - May be any valid css width - 1.5em, 20px, etc (using a number without a unit specifier won\'t do what you want) - This option does nothing for bar and tristate chars (see barWidth)
      height: 20,//Height of the chart - Defaults to \'auto\' (line height of the containing tag)
      lineColor: \'#88bbc8\',//Used by line and discrete charts to specify the colour of the line drawn as a CSS values string
      fillColor: \'#f2f7f9\',//Specify the colour used to fill the area under the graph as a CSS value. Set to false to disable fill
      spotColor: \'#e72828\',//The CSS colour of the final value marker. Set to false or an empty string to hide it
      maxSpotColor: \'#005e20\',//The CSS colour of the marker displayed for the maximum value. Set to false or an empty string to hide it
      minSpotColor: \'#f7941d\',//The CSS colour of the marker displayed for the mimum value. Set to false or an empty string to hide it
      spotRadius: 3,//Radius of all spot markers, In pixels (default: 1.5) - Integer
      lineWidth: 2//In pixels (default: 1) - Integer
  });
  */

  $(\'.textarea_editor\').wysihtml5();
', View::POS_END);?>

<?php if(Yii::$app->session->getFlash('success')): ?>

  <div class="modalError modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
          </div>
          <div class="modal-body">
            <?php echo Yii::$app->session->getFlash('success');?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
          </div>
        </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalError').modal('show');
", View::POS_END);?>

<?php endif;?>
