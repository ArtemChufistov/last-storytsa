<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use app\modules\support\models\TicketDepartment;

$this->title = Yii::t('user', 'Поддержка');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>
<div id="content">
  <div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
      ['title' => $this->title, 'url' => '' , 'icon' => 'icon-money'],
    ]]);?>
  <h1><?= $this->title;?></h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-cog"></i> </span>
            <h5><?=Yii::t('app', 'Техническая поддержка')?></h5>
          </div>
          <div class="widget-content">
              <p><?=Yii::t('app', 'Если у Вас есть какие-то технические проблемы, прежде чем спрашивать, ')?>
              <?=Yii::t('app', 'прочитайте раздел <a style = "color: #27a9e3; font-weight: bold;" href = "/office/faq">ОТВЕТЫ НА ВОПРОСЫ.</a>')?></p>
              <p><?=Yii::t('app', 'Если Вы не нашли решения своей проблемы, пожалуйста, свяжитесь с нами. Мы ответим Вам в ближайшие 24 часа.')?></p>
          </div>
        </div>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-cog"></i> </span>
            <h5><?=Yii::t('app', 'Создать тикет')?></h5>
          </div>
          <div class="widget-content nopadding">

            <?php $form = ActiveForm::begin([
              'id' => 'form-profile',
              'options' => [
                  'class'=>'form-horizontal',
                  'enctype'=>'multipart/form-data'
              ],
            ]); ?>

              <?php echo $form->field($ticket, 'department', ['template' => '
                     <label class="control-label">' . $user->getAttributeLabel('department') . '</label>
                     <div class="controls controls-row">
                        <div class="span11">
                            {input}
                       </div>
                     </div>
               '])->widget(Select2::classname(), [
                  'data' =>  ArrayHelper::map(TicketDepartment::find()->all(), 'id', 'name'),
                  'options' => ['placeholder' => Yii::t('app', 'Выберите отдел')],
                  'class' => '',
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);?>

            <?= $form->field($ticket, 'text', ['template' => '
              <label class="control-label">' . $user->getAttributeLabel('text') . '</label>
              <div class="controls controls-row">
                {input}
              </div>
            '])->textArea(['rows' => '4', 'value' => '', 'class' => 'textarea textarea_editor span11']); ?>

            <div class="form-actions">
                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить'), [
                    'class' => 'btn btn-info pull-left',
                    'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end();?>

          </div>
        </div>
      </div>
      <div class="span6">

      </div>
    </div>
  </div>
</div>

<?php $this->registerJs("jQuery(function () {jQuery('.textarea').wysihtml5();});", View::POS_END);?>
