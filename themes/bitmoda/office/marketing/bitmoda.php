<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Currency;
use app\modules\finance\models\Payment;
use app\modules\matrix\models\Matrix;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;
use app\modules\profile\models\YoutubeStat;

$this->title = Yii::t('user', 'Задания');

$assets = UserAsset::register($this);

$transaction = Transaction::find()->where(['to_user_id' => $user->id])
->andWhere(['type' => Transaction::TYPE_CHARGE_OFFICE])
->andWhere(['>','sum','0.005999'])
->one();

$currencyBTC = Currency::find()->where(['key' => Currency::KEY_BTC])->one();

?>
<div id="content">
	<div id="content-header">
	    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
	      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
	      ['title' => $this->title, 'url' => '/office/user' , 'icon' => 'icon-ok'],
	    ]]);?>
	  <h1><?= $this->title;?></h1>
	</div>
	<div class="container-fluid">
	  <hr>
	  <div class="row-fluid">
	    <div class="span6">
	      <div class="widget-box">
	        <div class="widget-title"> <span class="icon"> <i class="icon-ok"></i> </span>
	          <h5><?=Yii::t('app', 'Задание №1')?></h5>
	        </div>
	        <div class="widget-content">
				<?=Yii::t('app', 'Вы должны научиться работать с <strong>BTC FINANCE</strong>. Пополните Ваш BitCoin кошелек
				на <strong>0,006 BTC</strong>.<br/> Эти средства доступны на вывод в любой момент.</br>')?>
				</br>
				<hr>
				<?php if (empty($transaction)): ?>
					<a class="btn btn-success" href = "<?php echo Url::to('/office/in');?>"><?=Yii::t('app', 'Выполнить задание 1')?></a>
				<?php else:?>
					<a class="btn btn-primary"><?=Yii::t('app', 'Задание 1 выполнено')?></a>
				<?php endif;?>
	        </div>
	       </div>

	      <div class="widget-box">
	        <div class="widget-title"> <span class="icon"> <i class="icon-ok"></i> </span>
	          <h5><?=Yii::t('app', 'Задание №2')?></h5>
	        </div>
	        <div class="widget-content">
				<?=Yii::t('app', 'Приступив к выполнению <strong>ЗАДАНИЯ 2</strong> Ваши средства на кошельке временно замораживаются.
					Как только Вы подключите по <a style = "font-weight: bold;" href = "#" class = "btn-clipboard" data-clipboard-target= "#refLink">Вашей ссылке</a> двух участниц и они перейдут к <strong>ЗАДАНИЮ 2</strong>
					Ваш баланс полностью разморозится и Вы получите полный доступ к просмотру роликов <strong>и заработку в проекте.</strong>')?>
				</br>
				<hr>
				<?php $matrixUser = Matrix::find()->where(['user_id' => $user->id])->one();?>
				<?php if (empty($matrixUser)): ?>
					<a class="btn btn-success" href = "<?php echo Url::to('/office/bitmoda/marketing?doJob=1');?>"><?=Yii::t('app', 'Выполнить задание 2')?></a>
				<?php elseif (count($matrixUser->children()->all()) < 2):?>
					<a class="btn btn-success"><?=Yii::t('app', 'Задание 2 выполняется')?></a>
				<?php else: ?>
					<a class="btn btn-primary"><?=Yii::t('app', 'Задание 2 выполнено')?></a>
				<?php endif;?>
	        </div>
	       </div>

	      <div class="widget-box">
	        <div class="widget-title"> <span class="icon"> <i class="icon-th-list"></i> </span>
	          <h5><?=Yii::t('app', 'Статистика')?></h5>
	        </div>


	        <div class="widget-content">
	        	<?php $youTuveStatCountVideo = YoutubeStat::find()->where(['user_id' => $youTubeuserid])->andWhere(['showed' => 1])->all();?>
				<?=Yii::t('app', 'Всего просмотров видео: <strong>{count}</strong>', ['count' => count($youTuveStatCountVideo)])?>
				<hr>
				<?php $youTuveStatCountLike = YoutubeStat::find()->where(['user_id' => $youTubeuserid])->andWhere(['liked' => 1])->all();?>
				<?=Yii::t('app', 'Всего поставлено лайков: <strong>{count}</strong>', ['count' => count($youTuveStatCountLike)])?>
				<hr>
				<?php $transactionRefSumArray = Transaction::find()->where(['to_user_id' => $user->id])->andWhere(['currency_id' => $currencyBTC->id])->andWhere(['type' => Transaction::TYPE_PARTNER_BUY_PLACE])->all();?>


				<?php if (!empty($transactionRefSumArray)){$refSum = -$transactionRefSumArray[0]->sum;}else{$refSum = 0;}; foreach($transactionRefSumArray as $transactionRefSum):?>
					<?php $refSum += $transactionRefSum->sum;?>
				<?php endforeach;?>

				<?=Yii::t('app', 'Всего реферальских вознаграждений: <strong>{count} BTC</strong>', ['count' => $refSum])?>
				<hr>

				<?php $transactionVideoSumArray = Transaction::find()->where(['to_user_id' => $user->id])->andWhere(['currency_id' => $currencyBTC->id])->andWhere(['type' => Transaction::TYPE_FOR_VIDEO])->all();?>

				<?php $videoSum = 0; foreach($transactionVideoSumArray as $transactionVideoSum):?>
					<?php $videoSum += $transactionVideoSum->sum;?>
				<?php endforeach;?>

				<?=Yii::t('app', 'Всего заработано на просмотре видео: <strong>{count} BTC</strong>', ['count' => $videoSum])?>
	        </div>
	       </div>

	    </div>
	    <div class="span6">

	    	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_ref', [
	    		'user' => $user, 'transaction' => $transaction]);?>

	      <div class="widget-box">
	        <div class="widget-title"> <span class="icon"> <i class="icon-star"></i> </span>
	          <h5 style = "color: red;"><?=Yii::t('app', 'Внимание!')?></h5>
	        </div>
	        <div class="widget-content">
				<?=Yii::t('app', 'Вы получаете <strong>0.001 BTC</strong> за каждую, дополнительно приглашённую участницу', ['count' => 1])?>
	        </div>
	       </div>

	        <?php if ($videoId != false):?>

	        <div class="widget-box">
	          <div class="widget-title">
	            <ul class="nav nav-tabs">
	              <li class="active"><a data-toggle="tab" href="#video1"><?=Yii::t('app', 'Просмотр видео')?></a></li>
	            </ul>
	          </div>
	          <div class="widget-content tab-content">
	            <div id="video1" class="tab-pane active">
	              <div class="videoWrapper">
	                <div id="player" class="youtubePlayer"></div>
	              </div>
	              <br>

	              <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
	                <div class="todo">
	                  <ul>
	                    <li class="clearfix">
	                      <div class="txt">
	                        <?=$youtubeStat->showed?
	                        '<span class="date badge badge-success">Выполнено</span>':
	                        '<span class="date badge badge-important">Не выполнено</span>'?> <?php echo Yii::t('app', 'Посмотрите видео'); ?>
	                      </div>
	                      <?=!$youtubeStat->showed?
	                        '<div class="pull-right">' . Html::button('<i class="icon-play"></i> ' . Yii::t('app', 'Посмотреть'), [
	                          'class' => 'playButton btn btn-primary'
	                        ]) . Html::submitButton('', [
	                          'class' => 'youtubeContinue', 'value' => 1, 'name' => (new \ReflectionClass($youtubeStat))->getShortName() . '[play_button]', 'style' => 'display: none'
	                        ]) . '</div>':''?>
	                    </li>
	                    <li class="clearfix">
	                      <div class="txt">
	                        <?=$youtubeStat->liked?
	                        '<span class="date badge badge-success">Выполнено</span>':
	                        '<span class="date badge badge-important">Не выполнено</span>'?> <?php echo Yii::t('app', 'Поставьте видео лайк'); ?>
	                      </div>
	                      <?=!$youtubeStat->liked?'
	                        <div class="pull-right">' . Html::submitButton(
	                          '<i class="icon-thumbs-up"></i> ' . Yii::t('app', 'Поставить Лайк'),
	                          ['class' => 'btn btn-primary', 'value' => 1, 'name' =>  (new \ReflectionClass($youtubeStat))->getShortName() . '[like_button]']
	                        ) . '</div>':''
	                      ?>
	                    </li>
	                  </ul>
	                </div>
	              <?php ActiveForm::end();?>

	            </div>
	          </div>
	        </div>

	        <?php endif;?>

	    </div>
	  </div>
	</div>
	<div class="container-fluid">

    </div>
</div>

<?php if ($videoId != false):?>

<?php $this->registerJs("
  var player;

  function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      videoId: '" . $videoId . "',
      playerVars: {
        color: 'white'
      },
      events: {
        'onStateChange': onPlayerStateChange
      }
    });
  }

  function onPlayerStateChange(event) {
    if (event.data == 0) {
      player.stopVideo();
      $('.youtubeContinue').trigger('click');
    }
  }

  $('.playButton').click(function() {
    player.playVideo();
    $('.playButton').attr('disabled', 'true');
  });
", View::POS_END) ?>

<?php endif;?>