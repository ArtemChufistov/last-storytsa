<?php
use yii\helpers\Url;
?>
<div class="span12">
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-rss"></i> </span>
            <h5><?=$model->title; ?></h5>
        </div>
        <div class="widget-content">
            <div class="row-fluid">
                <div class="span3">
                    <a href="<?php echo Url::to(['/office/fullnew', 'slug' => $model->slug]);?>"><img src="/<?php echo $model->image;?>" alt=""></a>
                </div>

                <div class="span9">
                    <p>
                      <?php echo $model->smallDescription;?>
                    </p>

                </div>
            </div>
            <div class="row-fluid">
                <div class="span3"></div>
                <div class="span9">
                    <a href="<?php echo Url::to(['/office/fullnew', 'slug' => $model->slug]);?>" class="btn-link"><?php echo Yii::t('app', 'Читать далее');?> >></a>
                    <i class="pull-right"><?php echo Yii::t('app', 'Дата: {date}', ['date' => date('d-m-Y', strtotime($model->date))])?></i>
                </div>
            </div>
        </div>
    </div>
</div>