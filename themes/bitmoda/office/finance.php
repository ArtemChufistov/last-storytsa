
<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Payment;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = Yii::t('user', 'Финансы');

$assets = UserAsset::register($this);
?>
<div id="content">
<div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
      ['title' => $this->title, 'url' => '/office/user' , 'icon' => 'icon-money'],
    ]]);?>
  <h1><?= $this->title;?></h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span9">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-retweet"></i> </span>
          <h5><?=Yii::t('app', 'Финансовые операции')?></h5>
        </div>
        <div class="widget-content">
          <?php if(isset($paymentForm->type) && !empty($paymentForm->type) && in_array($paymentForm->type, [Payment::TYPE_IN_OFFICE, Payment::TYPE_OUT_OFFICE])):?>
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/payment/_payment-type-in-out-office', ['paymentForm' => $paymentForm, 'user' => $user]);?>
          <?php else:?>

              <a href= "<?php echo Url::to(['/profile/office/in']);?>" class="btn btn-success pull-left" >
                <i class="fa fa-level-down"></i> <?php echo Yii::t('app', 'Пополнить личный счёт');?>
              </a>
              <a href= "<?php echo Url::to(['/profile/office/out']);?>" class="btn btn-success pull-right" >
                <i class="fa fa-level-up"></i> <?php echo Yii::t('app', 'Снять с личного счёта');?>
              </a>

          <?php endif;?>
          <div style="clear: both"></div>
      </div>
    </div>

    <?php if (!empty($paymentProvider->getModels())):?>
      <div class="widget-box">
      <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $paymentProvider,
            'filterModel' => $payment,
            'id' => 'payment-grid',
            'layout' => '
                  <div class="widget-title">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                      <h5>' . Yii::t('app', 'Обрабатываемые платежи') . '</h5>
                    </div>
                  </div>
                 </div>
                 <div class="widget-content nopadding">
                  {items}
                  {pager}
                 </div>',
            'columns' => [['class' => 'yii\grid\SerialColumn'],
                [
                  'attribute' => 'type',
                  'format' => 'raw',
                  'filter' => Select2::widget([
                      'name' => 'PaymentFinanceSearch[type]',
                      'data' => Payment::getFinanceTypeArray(),
                      'theme' => Select2::THEME_KRAJEE,
                      'hideSearch' => true,
                      'options' => [
                          'placeholder' => Yii::t('app', 'Выберите тип'),
                          'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                      ]
                  ]),
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-type', ['model' => $model]);},
                ],[
                  'attribute' => 'realSum',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-sum', ['model' => $model]);},
                ],[
                  'attribute' => 'status',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-status', ['model' => $model]);},
                  'filter' => Select2::widget([
                      'name' => 'PaymentFinanceSearch[status]',
                      'data' => Payment::getStatusArray(),
                      'theme' => Select2::THEME_KRAJEE,
                      'hideSearch' => true,
                      'options' => [
                          'placeholder' => Yii::t('app', 'Выберите статус'),
                          'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                      ]
                  ]),
                ],[
                  'attribute' => 'wallet',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-wallet', ['model' => $model]);},
                ],[
                  'attribute' => 'transaction_hash',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-hash', ['model' => $model]);},
                ],[
                  'attribute' => 'date_add',
                  'format' => 'raw',
                  'filter' => true,
                  'filter' => \yii\jui\DatePicker::widget([
                      'model'=>$payment,
                      'attribute'=>'date_add',
                      'options' => ['class' => 'form-control'],
                      'language' => 'ru',
                      'dateFormat' => 'dd-MM-yyyy',
                  ]),
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-date-add', ['model' => $model]);},
                ]
            ],
        ]); ?>
      <?php Pjax::end(); ?>

    <?php endif;?>

      <div class="widget-box">

          <?php Pjax::begin(); ?>

          <?= GridView::widget([
              'dataProvider' => $transactionProvider,
              'filterModel' => $transaction,
              'id' => 'payment-grid',
              'layout' => '
                  <div class="widget-title">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                      <h5>' . Yii::t('app', 'История операций') . '</h5>
                    </div>
                  </div>
                 </div>
                 <div class="widget-content nopadding">
                  {items}
                  {pager}
                 </div>',
              'columns' => [['class' => 'yii\grid\SerialColumn'],
                  [
                    'attribute' => 'type',
                    'format' => 'raw',
                    'filter' => Select2::widget([
                        'name' => 'TransactionFinanceSearch[type]',
                        'data' => Transaction::getOfficeTypeArray(),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'hideSearch' => true,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Выберите тип'),
                            'value' => isset($_GET['TransactionFinanceSearch[type]']) ? $_GET['TransactionFinanceSearch[type]'] : null
                        ]
                    ]),
                    'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-type', ['model' => $model]);},
                  ],[
                    'attribute' => 'data',
                    'format' => 'raw',
                    'filter' => true,
                    'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-data', ['model' => $model]);},
                  ],[
                    'attribute' => 'sum',
                    'format' => 'raw',
                    'filter' => true,
                    'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-sum', ['model' => $model]);},
                  ],[
                    'attribute' => 'date_add',
                    'format' => 'raw',
                    'filter' => true,
                    'filter' => \yii\jui\DatePicker::widget([
                        'model'=>$transaction,
                        'attribute'=>'date_add',
                        'options' => ['class' => 'form-control'],
                        'language' => 'ru',
                        'dateFormat' => 'dd-MM-yyyy',
                    ]),
                    'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-date-add', ['model' => $model]);},
                  ]
              ],
          ]); ?>
          <?php Pjax::end(); ?>

      </div>

    <?php if (!empty($additionPaymentArray)):?>
      <div class="span3">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-bell"></i> </span>
            <h5><?=Yii::t('app', 'Доп. Информация')?></h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-striped">
              <tr>
                <th style="width: 10px"><?php echo Yii::t('app', 'Сумма');?></th>
                <th style="width: 10px"><?php echo Yii::t('app', 'Тип');?></th>
                <th style="width: 40px"><?php echo Yii::t('app', 'Описание');?></th>
              </tr>
              <?php foreach($additionPaymentArray as $additionPayment):?>
                <tr>
                  <td><?php echo $additionPayment->realSum; ?></td>
                  <td><?php echo $additionPayment->getTypeTitle(); ?></td>
                  <td><?php echo $additionPayment->additional_info; ?></td>
                </tr>
              <?php endforeach;?>
            </table>
        </div>
      </div>
    <?php endif;?>

    </div>

</div>

<div class="modal fade paymentInfoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo Yii::t('app', 'Информация о платеже');?></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs('
  $(".content").on("click", ".showPaymentInfo", function(){
    $.get("' . Url::to("/profile/office/showpaymentinfo") . '/" + $(this).attr("paymentHash"), function( data ) {
      $(".paymentInfoModal").find(".modal-body").html(data);
      $(".paymentInfoModal").modal("show");
    });
  })
');?>
