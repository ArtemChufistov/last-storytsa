<?php
use app\modules\event\models\Event;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Статистика');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="/admin/profile/office/dashboard" title="<?=Yii::t('app', 'Вернуться на главную')?>" class="tip-bottom"><i class="icon-home"></i> <?=Yii::t('app', 'Панель управления')?></a> <a href="#" class="current"><?=$this->title?></a> </div>
  <h1><?=Yii::t('app', 'Мой профиль')?></h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span4">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5><?php echo Yii::t('app', 'Участники структуры');?></h5>
        </div>
        <div class="widget-content">
          <p><?=Yii::t('app', 'Зарегистрированных участников в структуре: ') . $user->descendants()->count()?></p>
          <a href="/profile/office/struct" class="btn btn-success">
            <?php echo Yii::t('app', 'Посмотреть структуру');?> <i class="icon-arrow-right"></i>
          </a>
        </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5><?php echo Yii::t('app', 'Переходы по REF ссылке');?></h5>
        </div>
        <div class="widget-content">
          <p><?=Yii::t('app', 'Переходов по REF ссылке: ') . $user->getEvents()->where(['type' => Event::TYPE_REF_LINK])->count()?></p>
          <a href="#" class="btn btn-success">
            <?php echo Yii::t('app', 'Информация по событиям');?> <i class="icon-arrow-right"></i>
          </a>
        </div>
      </div>
    </div>
    <div class="span8">
      <?php Pjax::begin(); ?>

      <?= GridView::widget([
          'dataProvider' => $eventProvider,
          'filterModel' => $eventModel,
          'layout' => '
            <div class="widget-box">
              <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                <h5>' . Yii::t('app', 'События') . '</h5>
              </div>
              <div class="widget-content nopadding">
                <table class="table table-bordered data-table">
                  {items}
                </table>
                {pager}
              </div>
            </div>',
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              [
                  'attribute' => 'type',
                  'format' => 'html',
                  'filter' => false,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-type', ['model' => $model]);},
              ],[
                  'attribute' => 'to_user_id',
                  'format' => 'html',
                  'label' => Yii::t('app', 'Участник'),
                  'filter' => false,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_to_user_id', ['model' => $model]);},
              ],[
                  'attribute' => 'date_add',
                  'format' => 'html',
                  'filter' => false,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-date', ['model' => $model]);},
              ],
          ],
      ]); ?>
      <?php Pjax::end(); ?>
    </div>
