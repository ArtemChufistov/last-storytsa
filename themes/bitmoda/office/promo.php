
<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Рекламные материалы');
$assets = UserAsset::register($this);
?>

<div id="content">
<div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
      ['title' => $this->title, 'url' => '' , 'icon' => 'icon-heart'],
    ]]);?>
  <h1><?= $this->title;?></h1>
</div>
Здесь будет реф ссылка для приглашений, приглситель и статистика сколько людей зарегистрировали
</div>

<?php $this->registerJs('new Clipboard(".btn-clipboard");');?>
