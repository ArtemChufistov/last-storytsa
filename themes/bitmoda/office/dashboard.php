<?php
use app\modules\news\widgets\LastNewsWidget;
use app\modules\finance\models\CryptoWallet;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyGraph;
use app\modules\finance\models\Payment;
use app\modules\finance\models\Transaction;
use miloschuman\highcharts\Highcharts;
use app\modules\matrix\models\Matrix;
use app\modules\profile\models\YoutubeStat;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Панель управления');
$this->params['breadcrumbs'][] = $this->title;

$currencyVoucher = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();

$transaction = Transaction::find()->where(['to_user_id' => $user->id])
->andWhere(['type' => Transaction::TYPE_CHARGE_OFFICE])
->andWhere(['>','sum','0.005999'])
->one();

$transactionMinus = Transaction::find()->where(['from_user_id' => $user->id])
->andWhere(['type' => Transaction::TYPE_BUY_MATRIX_PLACE])
->andWhere(['>','sum','0.005999'])
->one();

$transactionPlus = Transaction::find()->where(['to_user_id' => $user->id])
->andWhere(['type' => Transaction::TYPE_PARTNER_BUY_PLACE])
->andWhere(['>','sum','0.005999'])
->one();

$matrixUser = Matrix::find()->where(['user_id' => $user->id])->one();

$countNewJob = 2;
$countDoJob = 0;
?>

<style>
.highcharts-credits{
  display: none;
}
</style>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard']
    ]]);?>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb">
          <a href="<?php echo Url::to(['/office/finance']);?>">
            <i class="icon-money"></i>
              <?php foreach ($this->params['user']->identity->getBalances() as $balance): ?>
                <strong><?php echo $balance->showValue();?></strong> <?php echo $balance->getCurrency()->one()->title;?>
              <?php endforeach;?>
              <?php echo Yii::t('app', ' - Доступно');?>
          </a>
        </li>
        <li class="bg_lo">
          <a href="<?php echo Url::to(['/office/finance']);?>">
            <i class="icon-ban-circle"></i>
              <strong><?php if (!empty($transactionMinus) && empty($transactionPlus)){echo $transactionMinus->sum;}else{ echo '0';};?></strong> <?php echo $balance->getCurrency()->one()->title;?>
              <?php echo Yii::t('app', ' - Заблокировано');?>
          </a>
        </li>

        <?php if (!empty($transaction)){$countDoJob = 1; $countNewJob = 1;}?>
        <?php if (!empty($matrixUser) && count($matrixUser->children()->all()) > 1){$countDoJob = 2; $countNewJob = 0;}?>

        <li class="bg_ls">
          <a href="<?php echo Url::to('/office/bitmoda/marketing');?>">
            <i class="icon-ok"></i>
            <span class="label label-important"><?php echo $countNewJob;?></span>
            <?php echo Yii::t('app', '<strong>{count}</strong> - Выполнено заданий', ['count' => $countDoJob]);?>
          </a>
        </li>
        <li class="bg_lg">
          <a href="<?php echo Url::to(['/office/promo']);?>">
            <i class="icon-sitemap"></i>
            <?php echo Yii::t('app', '<strong>{count}</strong> - Приглашено участников', ['count' => count($user->children()->all())]);?>
          </a>
        </li>

      </ul>
    </div>
<!--End-Action boxes-->

    <hr/>
    <div class="row-fluid">
      <div class="span8">

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
            <h5><?php echo Yii::t('app','О Нас');?></h5>
          </div>
          <div class="widget-content">
          <?php echo Yii::t('app', '

<p style = "text-align: justify;">
          	<img src = "/bitmoda/bitmoda/o_nas.jpg" style = "float: right; width: 350px; padding-left: 12px; padding-bottom: 12px;">
<strong>BitModa</strong>  - это проект для женской аудитории.  Женщины любят красивые вещи, любят модно одеваться, следят за новинками сезона, чтобы быть в тренде моды.  А что, если на этом увлечении ещё и зарабатывать?       С развитием технологий в сфере моды и электронной коммерции такая возможность появилась.                 Теперь  Вы будете получать за это деньги!  Просматривая ролики о модной одежде, Вы сможете зарабатывать <strong>до $300 в месяц</strong>, затрачивая при этом не более 30 минут в день. Стоимость одного минутного просмотра <strong>0.0001 BTC ($0.4 или ₽25)</strong>. Однако это не предел, и можно зарабатывать больше, используя партнёрскую программу.  За каждую новую женщину, подключённую в проект,  Вы получаете  <strong>0.001 BTC ($4 или ₽250)</strong>.                                                            Дорогие женщины - наслаждайтесь любимым  делом и получайте за это деньги!
</p>
<br/>
<h4>КАК ЭТО РАБОТАЕТ</h4>
<p>
<strong>Наша компания</strong> раскручивает бренды модной женской одежды.                                                                                                                 Цель проекта - набрать аудиторию из реально существующих женщин от 18 лет для создания таргетированного трафика на видео контент.  Для поднятия  видео  в рейтингах Ютуба, помимо просмотра, необходимо ещё ставить лайк.  Обычная реклама на Ютуб не позволяет выполнить этих задач, поэтому  мы набираем  сами нужную нам аудиторию. 
</p>
<p>
<strong>Для чего нам  нужно поднять видео контент в рейтинге?</strong> Для того, чтобы его увидело как можно больше женщин. Каждую модель одежды, показанную в роликах можно купить, перейдя по ссылке. Все эти ссылки ведут на магазины партнёров.  При покупке одежды магазины платят  от <strong>3% до 10%</strong> за каждую купленную модель.  Плюс  к этому будет ещё прямой доход от встроенной рекламы.  Всё эти деньги поступают на рекламный бюджет проекта, с которого  Вы и будете получать  своё вознаграждение за просмотры роликов.  При этом лично вам покупать ничего не нужно, только если сами захотите.
</p>
<p>
<strong>На первом этапе</strong>, пока набирается нужное количество женщин, оплата за просмотры будет начисляться из стартового бюджета  проекта. В этот период показ роликов будет в минимальном количестве. Как только мы наберём аудиторию порядка  <strong>10 тыс. женщин</strong>, проект выйдет на полную мощность.  Места в проекте ограничены!
</p>
<p>
<strong>Чтобы зарабатывать в проекте</strong>, Вам нужно иметь  google аккаунт, в котором указано Ваше имя и фотография.     Вы не сможете получать деньги, если Ваш аккаунт  будет не женским и без фотографии.  Все выплаты  в нашем проекте осуществляются в биткоинах <strong>(BTC)</strong>. 
</p>
<p>
<strong>Для того чтобы предотвратить</strong>  создание фейковых аккаунтов, в нашей системе предусмотрено выполнение двух заданий.  
</p>
<p>
<strong>В Первом задании</strong> Вам нужно научиться работать с биткоином.  Для этого Вам нужно пополнить свой кошелёк на сумму <strong>0.006 BTC</strong>.  Эти <strong>деньги  никуда не уходят</strong>  и остаются доступны для обратного вывода.     
</p>
<p>
<strong>
Второе задание 
</strong>
заключается в том, чтобы подключить в проект двух женщин.  Чтобы начать его выполнять, на Вашем  счету должна быть сумма <strong>0.006 BTC</strong>. Перейдя на выполнение второго задания, Ваш кошелёк временно замораживается. Как только Вы подключите двух женщин, которые в свою очередь перейдут к выполнению своего второго задания, Ваш кошелёк разморозится, и Вы получите полный доступ к просмотру роликов и заработку в проекте.  Внесённую сумму <strong>0.006 BTC</strong> можете выводить обратно. 
</p>
<p><strong>Данные меры решают ряд следующих задач:</strong></p>
<p>
1) Вы показываете, что умеете работать с биткоином.  Убирается ненужная нагрузка на службу технической поддержки, исключая вопросы на эту тему.                                                                                                                             
</p>
<p>
2) Решается важный вопрос  с предотвращением создания  фейковых аккаунтов.                                                                                                                
</p>
<p>
3) Вы помогаете быстрее набрать в проект необходимое количество женщин. Чем быстрее это будет, тем раньше проект выйдет на полную мощность и максимальные выплаты.
</p>
');?>
          </div>
        </div>
      </div>
      <div class="span4">
        <?php echo LastNewsWidget::widget();?>
      </div>
    </div>
  </div>
</div>

<!--end-main-container-part-->



<?php $this->registerJs('

$(document).ready(function(){



	// === Prepare peity charts === //
	maruti.peity();

	// === Prepare the chart data ===/
	var sin = [], cos = [];
    for (var i = 0; i < 14; i += 0.5) {
        sin.push([i, Math.sin(i)]);
        cos.push([i, Math.cos(i)]);
    }

	// === Make chart === //
    var plot = $.plot($(".chart"),
           [ { data: sin, label: "sin(x)", color: "#ee7951"}, { data: cos, label: "cos(x)",color: "#4fb9f0" } ], {
               series: {
                   lines: { show: true },
                   points: { show: true }
               },
               grid: { hoverable: true, clickable: true },
               yaxis: { min: -1.6, max: 1.6 }
		   });

	// === Point hover in chart === //
    var previousPoint = null;
    $(".chart").bind("plothover", function (event, pos, item) {

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $(\'#tooltip\').fadeOut(200,function(){
					$(this).remove();
				});
                var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);

                maruti.flot_tooltip(item.pageX, item.pageY,item.series.label + " of " + x + " = " + y);
            }

        } else {
			$(\'#tooltip\').fadeOut(200,function(){
					$(this).remove();
				});
            previousPoint = null;
        }
    });




    // === Calendar === //
    var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	$(\'.calendar\').fullCalendar({
		header: {
			left: \'prev,next\',
			center: \'title\',
			right: \'month,basicWeek,basicDay\'
		},
		editable: true,
		events: [
			{
				title: \'All day event\',
				start: new Date(y, m, 1)
			},
			{
				title: \'Long event\',
				start: new Date(y, m, 5),
				end: new Date(y, m, 8)
			},
			{
				id: 999,
				title: \'Repeating event\',
				start: new Date(y, m, 2, 16, 0),
				end: new Date(y, m, 3, 18, 0),
				allDay: false
			},
			{
				id: 999,
				title: \'Repeating event\',
				start: new Date(y, m, 9, 16, 0),
				end: new Date(y, m, 10, 18, 0),
				allDay: false
			},
			{
				title: \'Lunch\',
				start: new Date(y, m, 14, 12, 0),
				end: new Date(y, m, 15, 14, 0),
				allDay: false
			},
			{
				title: \'Birthday PARTY\',
				start: new Date(y, m, 18),
				end: new Date(y, m, 20),
				allDay: false
			},
			{
				title: \'Click for Google\',
				start: new Date(y, m, 27),
				end: new Date(y, m, 29),
				url: \'http://www.google.com\'
			}
		]
	});
});


maruti = {
		// === Peity charts === //
		peity: function(){
			$.fn.peity.defaults.line = {
				strokeWidth: 1,
				delimeter: ",",
				height: 24,
				max: null,
				min: 0,
				width: 50
			};
			$.fn.peity.defaults.bar = {
				delimeter: ",",
				height: 24,
				max: null,
				min: 0,
				width: 50
			};
			$(".peity_line_good span").peity("line", {
				colour: "#57a532",
				strokeColour: "#459D1C"
			});
			$(".peity_line_bad span").peity("line", {
				colour: "#FFC4C7",
				strokeColour: "#BA1E20"
			});
			$(".peity_line_neutral span").peity("line", {
				colour: "#CCCCCC",
				strokeColour: "#757575"
			});
			$(".peity_bar_good span").peity("bar", {
				colour: "#459D1C"
			});
			$(".peity_bar_bad span").peity("bar", {
				colour: "#BA1E20"
			});
			$(".peity_bar_neutral span").peity("bar", {
				colour: "#4fb9f0"
			});
		},

		// === Tooltip for flot charts === //
		flot_tooltip: function(x, y, contents) {

			$(\'<div id="tooltip">\' + contents + \'</div>\').css( {
				top: y + 5,
				left: x + 5
			}).appendTo("body").fadeIn(200);
		}
}

  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {

          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();
          }
          // else, send page to designated URL
          else {
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
');?>
