<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use yii\grid\GridView;
use moonland\tinymce\TinyMCE;

$this->title = Yii::t('user', 'Оставить отзыв');
$assets = UserAsset::register($this);
?>

  <!-- <div class="row">

    <div class="col-md-6">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo Yii::t('app', 'Добавить отзыв');?></h3>
        </div>
        <div class="box-body"> -->
  <div id="content">
    <div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
      ['title' => $this->title, 'url' => '' , 'icon' => 'icon-flag'],
    ]]);?>
  <h1><?= $this->title;?></h1>
    </div>
    <div class="container-fluid">
      <hr>
      <div class="row-fluid">
        <div class="span6">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-thumbs-up"></i> </span>
              <h5><?php echo Yii::t('app', 'Добавить отзыв');?></h5>
            </div>
            <div class="widget-content nopadding">
              <?php $form = ActiveForm::begin([
                'id' => 'form-profile',
                'options' => [
                    'class'=>'form-horizontal',
                    'enctype'=>'multipart/form-data'
                ],
              ]); ?>

                <?= $form->field($response, 'name', [
                    'template' => '
                    <div class="control-group">
                      <label class="control-label">' . $response->getAttributeLabel('name') . '</label>
                      <div class="controls">
                        {input}
                      </div>
                    </div>
                    '
                ])->textInput([
                    'maxlength' => true,
                    'placeholder' => $response->getAttributeLabel('name'),
                    'class' => 'span11'
                ]) ?>

                <?= $form->field($response, 'text', [
                    'template' => '
                    <div class="control-group">
                      <label class="control-label">' . $response->getAttributeLabel('text') . '</label>
                      <div class="controls">
                        {input}
                      </div>
                    </div>
                    '
                ])->textArea([
                    'maxlength' => true,
                    'placeholder' => $response->getAttributeLabel('text'),
                    'class' => 'span11 textarea',
                    'rows' => '9'
                ]) ?>

                <div class="control-group">
                  <label class="control-label"><?=Yii::t('app', 'Изображение № 1')?></label>
                  <?= $form->field($response, 'photo1',
                  [
                    'template' => '
                    <div class="controls">
                      {input}
                      <div class="help-block">' . Yii::t('app', 'Максимум') .' 2MB</div>
                    </div>
                    '
                  ])->fileInput()
                  ?>
                </div>

                <div class="control-group">
                  <label class="control-label"><?=Yii::t('app', 'Изображение № 2')?></label>
                  <?= $form->field($response, 'photo2',
                  [
                    'template' => '
                    <div class="controls">
                      {input}
                      <div class="help-block">' . Yii::t('app', 'Максимум') .' 2MB</div>
                    </div>
                    '
                  ])->fileInput()
                  ?>
                </div>

                <div class="control-group">
                  <label class="control-label"><?=Yii::t('app', 'Изображение № 3')?></label>
                  <?= $form->field($response, 'photo3',
                  [
                    'template' => '
                    <div class="controls">
                      {input}
                      <div class="help-block">' . Yii::t('app', 'Максимум') .' 2MB</div>
                    </div>
                    '
                  ])->fileInput()
                  ?>
                </div>

                <?= $form->field($response, 'video1', [
                    'template' => '
                    <div class="control-group">
                      <label class="control-label">' . $response->getAttributeLabel('video1') . '</label>
                      <div class="controls">
                        {input}
                      </div>
                    </div>
                    '
                ])->textInput([
                    'maxlength' => true,
                    'placeholder' => $response->getAttributeLabel('video1'),
                    'class' => 'span11'
                ]) ?>

                <?= $form->field($response, 'video2', [
                    'template' => '
                    <div class="control-group">
                      <label class="control-label">' . $response->getAttributeLabel('video2') . '</label>
                      <div class="controls">
                        {input}
                      </div>
                    </div>
                    '
                ])->textInput([
                    'maxlength' => true,
                    'placeholder' => $response->getAttributeLabel('video2'),
                    'class' => 'span11'
                ]) ?>

                <?= $form->field($response, 'video3', [
                    'template' => '
                    <div class="control-group">
                      <label class="control-label">' . $response->getAttributeLabel('video3') . '</label>
                      <div class="controls">
                        {input}
                      </div>
                    </div>
                    '
                ])->textInput([
                    'maxlength' => true,
                    'placeholder' => $response->getAttributeLabel('video3'),
                    'class' => 'span11'
                ]) ?>

                <div class="form-actions">
                    <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                        'class' => 'btn btn-success pull-right',
                        'name' => 'signup-button']) ?>
                </div>
              <?php ActiveForm::end();?>
            </div>
          </div>
        </div>

      <div class="span6">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-fire"></i> </span>
            <h5><?php echo Yii::t('app', 'Обратите внимание');?></h5>
          </div>
          <div class="widget-content">
            <?php echo Yii::t('app', '<p>Расскажите о Ваших впечатлениях, о проекте, о полученных выплатах, поделитесь своей историей успеха ! Постарайтесь написать более развернутый текст Вашего отзыва - новым участникам интересно знать Ваше мнение. Отзывы в одно предложение в стиле \'Все хорошо, спасибо.\' \'Отличный проект спасибо всем.\' - публиковаться не будут.</p></br><strong>Важно</strong><p>Отзывы размещаются на главной странице проекта. В них запрещена какая либо реклама своих ресурсов, личных контактов или реф ссылок. Главную страницу посещают участники разных команд и нужно уважать партнёров, которые с ними провели работу и пригласили в проект.</p>');?>
          </div>
        </div>

        <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_response-list', ['responseArray' => $responseArray]);?>
      </div>

    </div>
  </div>

<?php if(Yii::$app->session->getFlash('success')): ?>
  <div class="modalError modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('success');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalError').modal('show');
", View::POS_END);?>

<?php endif;?>

<?php $this->registerJs('
  $(document).ready(function(){
      $(\'input[type=checkbox],input[type=radio],input[type=file]\').uniform();

      $(\'select\').select2();
      $(\'.colorpicker\').colorpicker();
      $(\'.datepicker\').datepicker();
  });
', View::POS_END);?>
