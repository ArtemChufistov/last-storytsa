<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = Yii::t('user', 'Ответы на вопросы');

?>

<div id="content">
  <div id="content-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/layouts/partial/_office_breadcrumbs', ['breadcrumbs' => [
      ['title' => 'Панель управления', 'url' => '/office/dashboard' , 'icon' => 'icon-dashboard'],
      ['title' => $this->title, 'url' => '' , 'icon' => 'icon-umbrella'],
    ]]);?>
  <h1><?= $this->title;?></h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="accordion" id="collapse-group">
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5><?php echo Yii::t('app', 'Зачем нужно пополнять  счёт на 0.006 BTC?')?></h5>
                </a> </div>
            </div>
            <div class="collapse in accordion-body" id="collapseGOne">
              <div class="widget-content"><?php echo Yii::t('app', 'Прочитайте раздел  <a style = "color: #27a9e3; font-weight: bold;" href = "/office/dashboard">«КАК ЭТО РАБОТАЕТ».</a>')?></div>
            </div>
          </div>
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5><?php echo Yii::t('app', 'Как перечислить Биткоины на свой кошелёк?')?></h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseGTwo">
              <div class="widget-content"><?php echo Yii::t('app', 'Пополнить  свой кошелёк  можно через множество обменников. Откройте сайт  Bestchange.ru  и выберите обменник с наилучшим курсом.  Номер вашего кошелька Вы увидите у себя в кабинете  при выполнении Задания №1, он состоит из 34 символов. Скопируйте его и вставьте в обменнике  в поле «Кошелёк Биткоин».')?></div>
            </div>
          </div>
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGThree" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5><?php echo Yii::t('app', 'Деньги отправила, но на счёт не поступили?')?></h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseGThree">
              <div class="widget-content"><?php echo Yii::t('app', 'Для  подтверждения транзакции  требуется время от 20 минут до 2 часов. Так работает сеть Биткоина.  ')?></div>
            </div>
          </div>

          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseG4" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5><?php echo Yii::t('app', 'Как вывести деньги?')?></h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseG4">
              <div class="widget-content"><?php echo Yii::t('app', 'Вывод средств осуществляется по вторникам с 10 до 18-00.  Минимальная сумма 0.006 BTC. Для вывода укажите свой Биткоин кошелёк  на стороннем сервисе.  Если у вас его нет, то открыть его очень просто на официальном сайте  Blockchain.info')?></div>
            </div>
          </div>


          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseG5" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5><?php echo Yii::t('app', 'Как выплачивают партнёрские вознаграждения?')?></h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseG5">
              <div class="widget-content"><?php echo Yii::t('app', 'Вы получаете за каждую приглашённую женщину 0.001 BTC, кроме первых двух, необходимых для выполнения Задания №2. Деньги зачисляются на ваш счёт в тот момент, когда ваша приглашённая начнёт выполнять Задание №2 .')?></div>
            </div>
          </div>

          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseG6" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5><?php echo Yii::t('app', 'Можно ли отменить Задание №1?')?></h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseG6">
              <div class="widget-content"><?php echo Yii::t('app', 'Да, можно.  Если Вы передумали, то сумма 0.006 BTC доступна для обратного вывода.  Второе задание в этом случае выполнить будет невозможно. ')?></div>
            </div>
          </div>


          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseG7" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5><?php echo Yii::t('app', 'Можно ли отменить Задание №2?')?></h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseG7">
              <div class="widget-content"><?php echo Yii::t('app', 'Отменить второе задание невозможно.  Сумма 0.006 BTC будет временно заморожена  до тех пор, пока вы не выполните задание.  После выполнения, сумма 0.006 BTC доступна для вывода.')?></div>
            </div>
          </div>


          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseG8" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5 style = "white-space: nowrap;"><?php echo Yii::t('app', 'Можно ли на одном IP адресе участвовать нескольким  женщинам?')?></h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseG8">
              <div class="widget-content"><?php echo Yii::t('app', 'Да, можно.  Мы понимаем, что в одной семье может быть несколько женщин. Поэтому у нас сделана защита другого типа, в виде выполнения двух заданий,  чтобы предотвратить  создание фейковых аккаунтов.')?></div>
            </div>
          </div>

          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseG9" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5><?php echo Yii::t('app', 'Почему нельзя участвовать мужчинам?')?></h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseG9">
              <div class="widget-content"><?php echo Yii::t('app', 'Нам нужен таргетированный трафик. Исключительно женский!')?></div>
            </div>
          </div>

          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseG10" data-toggle="collapse"> <span class="icon"><i class="icon-magnet"></i></span>
                <h5><?php echo Yii::t('app', 'У  меня мало роликов для просмотра?')?></h5>
                </a> </div>
            </div>
            <div class="collapse accordion-body" id="collapseG10">
              <div class="widget-content"><?php echo Yii::t('app', 'На первом этапе, пока набирается нужное количество женщин, оплата за просмотры будет начисляться из стартового бюджета  проекта. В этот период показ роликов будет в минимальном количестве. Как только мы наберём аудиторию порядка  10 тыс. женщин, проект выйдет на полную мощность.  Места в проекте ограничены!')?></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>