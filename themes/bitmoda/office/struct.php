<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\finance\models\Transaction;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Моя сеть');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$transaction = Transaction::find()->where(['to_user_id' => $user->id])
->andWhere(['type' => Transaction::TYPE_CHARGE_OFFICE])
->andWhere(['>','sum','0.005999'])
->one();

?>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="/admin/profile/office/dashboard" title="<?=Yii::t('app', 'Вернуться на главную')?>" class="tip-bottom"><i class="icon-home"></i> <?=Yii::t('app', 'Панель управления')?></a> <a href="#" class="current"><?=$this->title?></a> </div>
  <h1><?=$this->title;?></h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span4">

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_ref', ['user' => $user, 'transaction' => $transaction]);?>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-search"></i> </span>
          <h5><?php echo Yii::t('app', 'Поиск участника');?></h5>
        </div>
        <div class="widget-content nopadding">
          <?php $form = ActiveForm::begin([
              'id' => 'form-profile',
              'options' => [
                'class' => 'form-horizontal',
                'enctype'=>'multipart/form-data'
              ],
          ]); ?>
            <div class="control-group">
              <label class="control-label"><?php echo Yii::t('app', 'Логин участника');?></label>
              <div class="controls">
                  <input type="input" class="span11" id="input-search" placeholder="Введите для поиска..." value="">
              </div>
            </div>

            <div class="form-actions">
              <?= Html::button(Yii::t('app', 'Поиск'), ['class' => 'btn btn-info', 'id' => 'btn-search']) ?>
              <?= Html::button(Yii::t('app', 'Очистить'), ['class' => 'btn btn-default', 'id' => 'btn-clear-search']) ?>
            </div>
          <?php ActiveForm::end();?>
        </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-user"></i> </span>
          <h5><?php echo Yii::t('app', 'Приглашенные участники');?></h5>
        </div>
        <div class="widget-content">
          <?php if (empty($userTree)):?>
            <p class ="text-trancpancy"><?php echo Yii::t('app', 'Пригласите своих подруг и знакомых, пусть они тоже получат возможность зарабатывать в индустрии моды и стиля. Места в проекте ограничены. Потом вам скажут спасибо! Для этого всего лишь нужно поделиться своей реферальной ссылкой.');?></p>
          <?php endif;?>
          <div id="treeview-searchable" class=""></div>
        </div>
      </div>
    </div>

    <div class="span4">
<div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 560x120');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/560x120_1.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal560x120']);?>
        </div>

          <div class="modal fade modal560x120">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 560x120');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode560120"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/560x120_1.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode560120']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 560x120');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/560x120_2.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal560x1202']);?>
        </div>

          <div class="modal fade modal560x1202">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 560x120');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode5601202"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/560x120_2.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode5601202']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 640x100');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/640x100_1.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal640x100']);?>
        </div>

          <div class="modal fade modal640x100">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 640x100');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode640100"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/640x100_1.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode640100']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 640x100');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/640x100_2.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal640x1002']);?>
        </div>

          <div class="modal fade modal640x1002">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 640x100');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode6401002"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/640x100_2.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode6401002']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 728x90');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/728x90_1.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal72890']);?>
        </div>

          <div class="modal fade modal72890">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 728x90');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode72890"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/728x90_1.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode72890']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 728x90');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/728x90_2.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal728902']);?>
        </div>

          <div class="modal fade modal728902">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 728x90');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode728902"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/728x90_2.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode728902']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x250');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/300x250_2.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal300x2502']);?>
        </div>

          <div class="modal fade modal300x2502">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x250');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode3002502"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/300x250_2.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode3002502']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x300');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/300x300_1.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal300x300']);?>
        </div>

          <div class="modal fade modal300x300">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x300');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode300300"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/300x300_1.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode300300']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>
      </div>

    </div>

    <div class="span4">

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 150x150');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/150x150_1.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal1501501']);?>
        </div>

          <div class="modal fade modal1501501">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 150x150');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode150150"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/150x150_1.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode150150']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>

      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 150x150');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/150x150_2.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal1501502']);?>
        </div>

          <div class="modal fade modal1501502">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 150x150');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode1501502"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/150x150_2.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode1501502']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>

      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 200x200');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/200x200_1.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal200200']);?>
        </div>

          <div class="modal fade modal200200">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 200x200');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode200200"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/200x200_1.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode200200']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>

      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 200x200');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/200x200_2.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal2002002']);?>
        </div>

          <div class="modal fade modal2002002">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 200x200');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode2002002"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/200x200_2.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode2002002']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>

      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x250');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/300x250_1.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal300x250']);?>
        </div>

          <div class="modal fade modal300x250">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x250');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode300250"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/300x250_1.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode300250']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>

      </div>
      
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-heart"></i> </span>
          <h5><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x300');?></h5>
        </div>
        <div class="widget-content" style = "text-align: center;">
            <img style = "margin-bottom:15px;" src="<?php echo Url::home(true);?>bitmoda/banners/300x300_2.gif">
            <?php echo Html::input('button', '', Yii::t('app', 'Получить код баннера'), ['class' => 'btn btn-info btn-block', 'data-toggle' => 'modal', 'data-target' => '.modal300x3002']);?>
        </div>

          <div class="modal fade modal300x3002">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title"><?php echo Yii::t('app', 'Рекламные баннеры, размер 300x300');?></h4>
                </div>
                <div class="modal-body form-horizontal">
                    <textarea style = "width: 100%;" class="span11" id = "clipboardCode3003002"><a href = "<?php echo Url::home(true);?>?ref=<?php echo $user->login;?>"><img src = "<?php echo Url::home(true);?>bitmoda/banners/300x300_2.gif"></a></textarea>
                </div>
                <div class="modal-footer">
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать код в буфер обмена'), ['class' => 'btn btn-primary btn-clipboard', 'data-clipboard-target' => '#clipboardCode3003002']);?>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
                </div>
              </div>
            </div>
          </div>
      </div>
      

      </div>

    </div>
  </div>
</div>


<?php $this->registerJs("
  var defaultData = " . json_encode($userTree) . ";
  $(function() {

    var csrfToken = '" . Yii::$app->request->getCsrfToken() . "';

    function showUser(login)
    {
      if ($('.userList').find('[login=\"' + login + '\"]').html() != undefined){
        $('.userList').find('[login=\"' + login + '\"]').show();
        return;
      }

      $.post('/profile/office/showuser', { login: login, _csrf: csrfToken }, function(data){
        $('.userList').append(data);
      });
    }

    jQuery(document).on('click', '.list-group-item', function(){
      showUser(jQuery(this).find('.login').html());
    })

    jQuery('#search-output').on('click', 'td', function(){
      showUser(jQuery(this).find('span').html());
    })

    jQuery('.userList').on('click', '.close', function(){
      $(this).parents('.userInfoPartial').remove();
    })

    var searchableTree = $('#treeview-searchable').treeview({
      data: defaultData,
    });

    var search = function(e) {
      var pattern = $('#input-search').val();

      var results = searchableTree.treeview('search', [ pattern ]);

      var output = '<tr><td>' + results.length + ' " . Yii::t('app', 'совпадений найдено') . "</td></tr>';

      $.each(results, function (index, result) {
        output += '<tr><td>' + result.text + '</td></tr>';
      });

      $('#search-output').html(output);
      $('.resultBox').show();
    }

    $('#btn-search').on('click', search);
    $('#input-search').on('keyup', search);

    $('#btn-clear-search').on('click', function (e) {
      searchableTree.treeview('clearSearch');
      $('#input-search').val('');
      $('#search-output').html('');
    });
  });
", View::POS_END);?>
