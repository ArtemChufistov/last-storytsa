<?php
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\CoinbaseTransaction;
?>
<?php if ($model->getFromPaySystem()->one()->key == PaySystem::KEY_COINBASE):?>
	<?php $coinbaseTransaction = CoinbaseTransaction::find()->where(['payment_id' => $model->id])->one();?>
	<?php if (!empty($coinbaseTransaction)):?>
		<a target = "_blank" href = "https://blockchain.info/ru/tx/<?php echo $coinbaseTransaction->network_hash;?>" class = "showPaymentInfo" paymentHash = "<?php echo $coinbaseTransaction->network_hash;?>"><?php echo Yii::t('app', 'Посмотреть на BlockChain');?></a>
	<?php endif;?>
<?php else:?>
	<?php echo $model->transaction_hash;?>
<?php endif;?>
<?php echo $model->transaction_hash;?>
