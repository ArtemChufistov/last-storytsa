<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
		'label' => 'FAQ',
		'url' => '/faq'
	],
];

$this->title = $model->title;
?>

<main>   
    <section class="well9">
      <div class="container">
        <h2><?= Yii::t('app', 'Eligibility Criteria<br><span>for Personal Loan</span>');?></h2>
<!--
        <p>
             <?= Yii::t('app', 'Etiam tellus nulla, posuere quis tincidunt in, rhoncus id nisl. Ut aliquet semper ex laoreet luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi ultricies ipsum in magna convallis pharetra. Integer maximus velit quis finibus sagittis. Praesent rhoncus id lorem nec ornare. Suspendisse fringilla augue eu mauris vestibulum rhoncus. Vestibulum ante ipsum primis in faucibus orci luctus   ');?>
           </p>
-->
          		<div class="ziehharmonika">
			<h3>Lorem ipsum</h3>
			<div>
				<p>Sed eu odio tristique, consectetur felis vulputate, bibendum nulla. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean commodo odio et ligula auctor lacinia at quis risus. Duis nec massa venenatis, blandit tortor a, porta libero. Sed at leo auctor nunc volutpat tempor.</p>
			</div>
			<h3>Dolor sit amet</h3>
			<div>
				<p>Sed accumsan, ipsum et tincidunt dapibus, ex ex porttitor lectus, eu egestas mi ante sit amet justo. Maecenas et velit non mi feugiat elementum. Fusce pharetra, mi non accumsan vehicula, urna purus iaculis est, vel pulvinar nunc nisi eget dui. Nam ac metus vitae odio porttitor congue posuere porttitor tellus. Vivamus cursus tristique nisi id faucibus.</p>
			</div>
			<h3>Consectetur adipiscing elit</h3>
			<div>
				<p>Maecenas euismod finibus eros in vehicula. Maecenas tempus eros vitae tellus vehicula porttitor. Aliquam sed ullamcorper nunc. Quisque aliquet sem congue enim suscipit, at tristique enim tincidunt. Aenean ut imperdiet felis. Duis pulvinar arcu lacus, in eleifend lectus dapibus in. Praesent vitae elit vitae justo tincidunt cursus.</p>
			</div>
		</div>
<!--
        <div class="box">
          <div class="aside">
            <img src="stamp/images/page-3_img01.png" alt="">
          </div>
          <div class="cnt">
            <h4 class="colorText1">
              <?= Yii::t('app', 'Phasellus a fringilla nisi Suspendisse potenti In hac habitasse platea dic tumst Interdum et malesuada fames actricies ');?>
            </h4>
            <p>
             <?= Yii::t('app', 'Etiam tellus nulla, posuere quis tincidunt in, rhoncus id nisl. Ut aliquet semper ex laoreet luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi ultricies ipsum in magna convallis pharetra. Integer maximus velit quis finibus sagittis. Praesent rhoncus id lorem nec ornare. Suspendisse fringilla augue eu mauris vestibulum rhoncus. Vestibulum ante ipsum primis in faucibus orci luctus   ');?>
           </p>
           <div class="row">
             <ul class="marked-list offs1 col-md-6 col-sm-12 col-xs-12 ">
              <li><a href="#"><?= Yii::t('app', 'Lorem ipsum dolor ipsu come.');?></a></li>
              <li><a href="#"><?= Yii::t('app', 'consectetur adipisci aenean.');?></a></li>
              <li><a href="#"><?= Yii::t('app', 'In euismod est quis aliquet.');?></a></li>
            </ul>
            <ul class="marked-list offs1 col-md-6 col-sm-12 col-xs-12 ">
              <li><a href="#"><?= Yii::t('app', 'Lorem ipsum dolor ipsu come.');?></a></li>
              <li><a href="#"><?= Yii::t('app', 'consectetur adipisci aenean.');?></a></li>
              <li><a href="#"><?= Yii::t('app', 'In euismod est quis aliquet.');?></a></li>
            </ul>
          </div>
        </div>
      </div>
-->
    </div>
  </section>
  </main>