<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\web\View;
use app\modules\profile\models\Occupation;
use app\modules\profile\models\Usastate;

$this->title = Yii::t('app', $model->title);
?>
<main class="page-content">
  <div class="jumbotron">
    <div class="shell text-md-left text-center">
      <div class="range">
        <div class="cell-md-8">
          <h1><?= Yii::t('app', 'Everybody has a chance to apply his knowledge');?></a></h1>
          <h3><?= Yii::t('app', 'Thank you for visiting our site!');?></a></h3><a href="#welcome-block" class="btn btn-primary"><?= Yii::t('app', 'Learn more');?></a>
        </div>
      </div>
    </div>
  </div>
  <section id="welcome-block" class="section-80">
    <div class="shell text-center text-md-left">
      <h2 class="text-tarawera"><?= Yii::t('app', 'Welcome to our site!');?></h2>
      <div class="range">
        <div class="cell-md-6">
          <div class="thumbnail"><a href="#" class="img-thumbnail"><img src="/job/images/home-02.jpg" alt=""></a>
            <div class="caption">
              <h3><a href="#"><?= Yii::t('app', 'Ensuring the highest level of customers satisfaction');?></a></h3>
              <p><?= Yii::t('app', 'Our company is your one-stop-solution for all needs. There is no doubt that we are the leaders and you don\'t have to worry about our image because it is perfect.');?></p>
            </div>
          </div>
        </div>
        <div class="cell-md-6">
          <div class="thumbnail"><a href="#" class="img-thumbnail"><img src="/job/images/home-03.jpg" alt=""></a>
            <div class="caption">
              <h3><a href="#"><?= Yii::t('app', 'Developing your opportunities within your business sector');?></a></h3>
              <p><?= Yii::t('app', 'Thank you for visiting our site! You are at the right place! We are focused on providing integrated solutions and services to customers around the world.');?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="bg-brand-warning">
    <div class="shell text-left section-top-54 section-bottom-48">
      <h2 class="text-white"><?= Yii::t('app', 'How can our agency help you?');?></h2>
    </div>
  </section>
  <section id="employers-block" class="bg-images-1">
    <div class="shell">
      <div class="range">
        <div class="cell-lg-6 cell-lg-preffix-6 cell-md-6 cell-md-preffix-6 bg-brand-primary section-80 section-sm-top-123 section-sm-bottom-120 text-left">
          <h2><?= Yii::t('app', ' For employers');?></h2>
          <p><?= Yii::t('app', 'Recruitment Agencies advertise their jobs on CV-Library. Enter your search criteria, including keywords and location, and all relevant jobs will be displayed for you to apply directly to.');?></p>
          <ul class="marked-list">
            <li><a href="#"><?= Yii::t('app', 'Use Our Advanced Search Tools to locate your most suitable recruiter');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'Register with Agency Seeker and forward all your Job Vacancies to recruiters');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'Request immediate call backs for your recruitment needs or choose to contact recruiters directly');?></a></li>
          </ul><a href="#" class="btn btn-default offset-top-30 offset-sm-top-68">Learn more</a>
        </div>
      </div>
    </div>
  </section>
  <section class="img-section"><img src="/job/images/home-04.jpg" alt=""></section>
  <section id="jobseekers-block" class="bg-images-2">
    <div class="shell">
      <div class="range">
        <div class="cell-lg-6 cell-md-7 bg-brand-secondary section-top-80 section-sm-top-123 section-bottom-78 text-left">
          <h2><?= Yii::t('app', ' For jobseekers');?></h2>
          <p><?= Yii::t('app', 'Searching for jobs, telephone interviews, in-person interviews,  waiting for a call that sometimes doesn\'t arrive can all add up to stress that makes it hard to focus and find the job you want!');?></p>
          <ul class="marked-list">
            <li><a href="#"><?= Yii::t('app', 'Use Our Advanced Search Tools to locate your most suitable recruiter');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'Register with Agency Seeker and forward your CV directly to recruiters');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'Request immediate call backs to discuss your recruitment needs');?></a></li>
            <li><a href="#"><?= Yii::t('app', 'View jobs that recruiters are advertising on Careers Seeker');?></a></li>
          </ul><a href="#" class="btn btn-default offset-top-30 offset-sm-top-51"><?= Yii::t('app', 'Learn more');?></a>
        </div>
      </div>
    </div>
  </section>
  <section id="why-block" class="section-80">
    <div class="shell text-center text-sm-left">
      <h2 class="text-tarawera"><?= Yii::t('app', ' Why our company?');?></h2>
      <div class="range text-center offset-top-82">
        <div class="cell-md-3 cell-sm-6"><span class="material-design-phone371 icon-lg icon icon-primary"></span>
          <h3 class="text-center text-sm-left offset-top-10 offset-md-top-51"><?= Yii::t('app', ' Our Commitment to You');?></h3>
          <p class="text-center text-sm-left"><?= Yii::t('app', 'We use the Commitment Charter to ensure all clients are looked after at every step of the way.');?></p>
        </div>
        <div class="cell-md-3 cell-sm-6 offset-top-51 offset-sm-top-0"><span class="material-design-two385 icon-lg icon icon-primary"></span>
          <h3 class="text-center text-sm-left offset-top-10 offset-md-top-51"><?= Yii::t('app', 'Fully trained outreach HR team');?></h3>
          <p class="text-center text-sm-left"><?= Yii::t('app', 'The department will be able to deal with any employment issues, including breaches of contract.');?></p>
        </div>
        <div class="cell-md-3 cell-sm-6 offset-top-51 offset-md-top-0"><span class="material-design-virtual2 icon-lg icon icon-primary"></span>
          <h3 class="text-center text-sm-left offset-top-10 offset-md-top-51"><?= Yii::t('app', 'Recruitment Consultants');?></h3>
          <p class="text-center text-sm-left"><?= Yii::t('app', 'It\'s important that you trust the person who represents you at the recruitment agency you choose.');?></p>
        </div>
        <div class="cell-md-3 cell-sm-6 offset-top-51 offset-md-top-0"><span class="material-design-set5 icon-lg icon icon-primary"></span>
          <h3 class="text-center text-sm-left offset-top-10 offset-md-top-51"><?= Yii::t('app', 'We run FastPay Payments');?></h3>
          <p class="text-center text-sm-left"><?= Yii::t('app', 'Our agency supports FastDay online payments that will hasten and secure the payment process on our website.');?></p>
        </div>
      </div>
    </div>
  </section>
  <section id="form-block" class="section-top-80 section-bottom-60 bg-brand-primary">
    <div class="shell text-left">
      <div class="range">
        <div class="cell-sm-12">
          <h2><?= Yii::t('app', ' Vacancy form');?></h2>
          <div class="range offset-top-0 offset-md-top-60">
            <div class="cell-sm-12">
              <?php
                $model = new \app\modules\profile\models\Jobinfo();
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                  $model->file = UploadedFile::getInstance($model, 'attach_file');
                  if ($model->file)
                      $model->file->saveAs('attach/vacancy/' . $model->file->baseName . '.' . $model->file->extension);

                  $model->save();
                  echo '
                  <div class="modal fade" id="vacancyFormSuccessModal" tabindex="-1" role="dialog" aria-labelledby="vacancyFormSuccessModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="vacancyFormSuccessModalLabel">' . Yii::t('app', 'Success') . '</h4>
                        </div>
                        <div class="modal-body">
                          ' . Yii::t('app', 'You have successfully sent a new vacancy.') . '
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">ОК</button>
                        </div>
                      </div>
                    </div>
                  </div>';
                  $this->registerJs("$('#vacancyFormSuccessModal').modal()", View::POS_END);
                  $model = new \app\modules\profile\models\Jobinfo();
                }
              ?>
              <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
                  <?= $form->field($model, 'company_name')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('company_name'),
                  ]) ?>
                  <?= $form->field($model, 'state_id')->dropDownList(
                      ArrayHelper::map(Usastate::find()->all(), 'id', 'name'),
                      [
                        'prompt' => Yii::t('app', 'Choose your state'),
                      ]
                  ); ?>
                  <?= $form->field($model, 'name')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('name'),
                  ]) ?>
                  <?= $form->field($model, 'phone')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('phone'),
                  ]) ?>
                  <?= $form->field($model, 'email')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('email'),
                  ]) ?>
                  <?= $form->field($model, 'website')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('website'),
                  ]) ?>
                  <?= $form->field($model, 'occupation_id')->dropDownList(
                      ArrayHelper::map(Occupation::find()->all(), 'id', 'title'),
                      [
                        'prompt' => Yii::t('app', 'Choose your occupation'),
                      ]
                  ); ?>
                  <?= $form->field($model, 'attach_file')->fileInput() ?>

                  <div class="form-group">
                      <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-default offset-top-30 offset-sm-top-51']) ?>
                  </div>
              <?php ActiveForm::end(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="testimonials-block" class="section-top-80 section-bottom-110">
    <div class="shell">
      <h2 class="text-tarawera text-center text-md-left"><?= Yii::t('app', 'Testimonials');?></h2>
      <div class="range">
        <div class="cell-sm-6">
          <div class="unit unit-md unit-md-horizontal">
            <div class="unit-left"><img src="/job/images/home-06.jpg" alt=""></div>
            <div class="unit-body text-md-left text-center">
              <h3><a href="#" class="text-rangoon-green"><?= Yii::t('app', 'Kevin Howard');?></a></h3>
              <p class="text-gunsmoke"><?= Yii::t('app', '"I highly recommend agency as a professional and  extremely competent consultant who helped me find the right position and identified the key criteria I was looking for in my next role."');?></p>
              <ul class="list-inline">
                <li><a href="#" class="fa-facebook-official"></a></li>
                <li><a href="#" class="fa-twitter"></a></li>
                <li><a href="#" class="fa-google-plus"></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="cell-sm-6">
          <div class="unit unit-md unit-md-horizontal offset-top-60 offset-xs-top-30 offset-sm-top-0">
            <div class="unit-left"><img src="/job/images/home-07.jpg" alt=""></div>
            <div class="unit-body text-md-left text-center">
              <h3><a href="#" class="text-rangoon-green"><?= Yii::t('app', 'Sarah Howard');?></a></h3>
              <p class="text-gunsmoke"><?= Yii::t('app', '"From my first meeting with the team I was most impressed with their level of professionalism and can-do attitude. They were both friendly and helpful and kept me fully informed throughout the whole process."');?></p>
              <ul class="list-inline">
                <li><a href="#" class="fa-facebook-official"></a></li>
                <li><a href="#" class="fa-twitter"></a></li>
                <li><a href="#" class="fa-google-plus"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="range">
        <div class="cell-sm-6">
          <div class="unit unit-md unit-md-horizontal">
            <div class="unit-left"><img src="/job/images/home-08.jpg" alt=""></div>
            <div class="unit-body text-md-left text-center">
              <h3><a href="#" class="text-rangoon-green"><?= Yii::t('app', 'Roberta Rivera');?></a></h3>
              <p class="text-gunsmoke"><?= Yii::t('app', '"We have worked with RAD for over 10 years and they really know what candidates are a good fit for our company - both for our customers and internally. They are very friendly and always willing to help."');?></p>
              <ul class="list-inline">
                <li><a href="#" class="fa-facebook-official"></a></li>
                <li><a href="#" class="fa-twitter"></a></li>
                <li><a href="#" class="fa-google-plus"></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="cell-sm-6">
          <div class="unit unit-md unit-md-horizontal offset-top-60 offset-xs-top-30 offset-sm-top-0">
            <div class="unit-left"><img src="/job/images/home-09.jpg" alt=""></div>
            <div class="unit-body text-md-left text-center">
              <h3><a href="#" class="text-rangoon-green"><?= Yii::t('app', 'James Price');?></a></h3>
              <p class="text-gunsmoke"><?= Yii::t('app', '"We have worked with RAD on two critical assignments. The team set out to thoroughly understand the brief, were knowledgeable and extremely helpful throughout the process which led to two very good appointments."');?></p>
              <ul class="list-inline">
                <li><a href="#" class="fa-facebook-official"></a></li>
                <li><a href="#" class="fa-twitter"></a></li>
                <li><a href="#" class="fa-google-plus"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
