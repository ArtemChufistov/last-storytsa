<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;
use yii\web\View;

$matrix = Matrix::find()->where(['user_id' => $user->id])->one();
?>

<div class="card">
    <div class="card-header" data-background-color="purple">
        <h4 class="title"><?php echo Yii::t('app', 'Ваша реферальная ссылка');?></h4>
        <p class="category"><?php echo Yii::t('app', 'Распространите эту ссылку среди друзей и знакомых для привлечения их к участию в партнерской программе');?></p>
    </div>
    <div class="card-content">
        <div class="row">
            <div class="col-md-12">
                <?php if (empty($matrix)):?>
                  <div class="form-group label-floating">
                      <label class="control-label"><?php echo Yii::t('app', 'Реферальная ссылка');?></label>
                      <?php echo Html::input('text', 'refLink', Url::home(true) . '?ref=' . $user->login, ['id' => 'refLink', 'class' => 'form-control' , 'readonly' => true]);?>
                  </div>
                  <?php echo Html::input('button', '', Yii::t('app', 'Скопировать в буфер обмена'), ['class' => 'btn-clipboard btn btn-primary pull-left', 'data-clipboard-target' => '#refLink']);?>
                <?php else:?>
                  <h5><?php echo Yii::t('app', 'Реферальная ссылка будет доступна только после</br> <a style = "font-weight: bold; text-decoration:underline;" href = "/profile/office/program/1">принятия участия в программе.</a>')?></h5>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs("
new Clipboard('.btn-clipboard');
", View::POS_END);?>