<?php
use app\assets\job\FrontAppAssetBottom;
use app\assets\job\FrontAppAssetTop;
use app\modules\menu\widgets\MenuWidget;
use yii\helpers\Html;

FrontAppAssetTop::register($this);
FrontAppAssetBottom::register($this);

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
  <head>
    <?=Html::csrfMetaTags()?>
    <title><?=Html::encode($this->title)?></title>
    <?php $this->head()?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_head_meta'); ?>

    <link rel="icon" type="image/png" href="images/favicon.png" />

    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <![endif]-->
  </head>
  <body>
    <div class="page text-center">
      <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']); ?>

      <?=$content?>

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_footer'); ?>
    </div>
    <?php $this->endBody()?>
  </body>
</html>
<?php $this->endPage()?>
