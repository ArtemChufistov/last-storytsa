<?php
use yii\helpers\Html;
?>
<div class="dropdown" id="progress-btn">
    <a data-toggle="dropdown" href="#" title="">
        <span class="small-badge bg-azure"></span>
        <i class="glyph-icon icon-linecons-params"></i>
    </a>
    <div class="dropdown-menu pad0A box-sm float-right" id="progress-dropdown">
        <div class="scrollable-content scrollable-slim-box">
            <ul class="no-border progress-box progress-box-links">
                <li>
                    <a href="#" title="">
                        <div class="progress-title">
                            <strong><?php echo Yii::t('app', 'Сменить язык'); ?></strong>
                        </div>
                    </a>
                </li>

                <?php foreach ($langs as $lang): ?>
                    <li>
                        <?=Html::a($lang->name, '/' . $lang->url . Yii::$app->getRequest()->getLangUrl())?>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>