<?php
use app\modules\menu\widgets\MenuWidget;
?>

<footer class="page-foot bg-tarawera section-top-80 section-bottom-60">
  <div class="shell text-md-left text-center"><a href="./"><img src="/job/images/brand-footer.png" alt=""></a>
    <?php echo MenuWidget::widget(['menuName' => 'botomMenu', 'view' => 'bottom-menu']); ?>
    <div class="range offset-top-51 offset-md-top-92">
      <div class="cell-md-6">
        <p><?= Yii::t('app', 'Sponsored Job &#169;<span id="copyright-year"></span> All rights reserved.');?></p>
        <p><a href="#"><?= Yii::t('app', 'Terms of use  |');?><a href="privacy.html"><?= Yii::t('app', 'Privacy Policy');?> </a>
        </p>
      </div>
      <div class="cell-md-4 cell-md-preffix-2">
        <ul class="list-inline">
          <li><a href="#"><span class="fa-facebook-official"></span><br>
              <div class="offset-top-4"><?= Yii::t('app', 'Facebook');?></a></div></a></li>
          <li><a href="#"><span class="fa-twitter"></span><br>
              <div class="offset-top-4"><?= Yii::t('app', 'Twitter');?></a></div></a></li>
          <li><a href="#"><span class="fa-google-plus"></span><br>
              <div class="offset-top-4"><?= Yii::t('app', 'Google+');?></a></div></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>
