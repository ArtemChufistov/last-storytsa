<?php
use yii\helpers\Url;
use app\modules\menu\widgets\MenuWidget;
?>
<div class="sidebar" data-color="purple" data-image="/stamp/img/sidebar-1.jpg">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->

    <div class="logo">
        <a href="#" class="simple-text">
            Waka
        </a>
    </div>

    <div class="sidebar-wrapper">
        <?php echo MenuWidget::widget(['menuName' => 'profileMenu', 'view' => 'office-menu']); ?>
    </div>
</div>