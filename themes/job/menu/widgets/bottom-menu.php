<?php
use yii\helpers\Url;
?>
<ul class="list-inline-1">
  <?php foreach ($menu->children($menu->depth + 1)->all() as $num => $child): ?>
  <li<?php if ($requestUrl == Url::to([$child->link])): ?> class="active"<?php endif;?>><a href="<?=Url::to([$child->link]);?>"><?=$child->title;?></a></li>
  <?php endforeach;?>
</ul>
