<?php
use app\modules\lang\widgets\WLang;
use yii\helpers\Url;
?>
<header class="page-head">
  <div class="rd-navbar-wrap">
    <nav data-layout="rd-navbar-fixed" data-sm-device-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-sm-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-static" class="rd-navbar">
      <div class="rd-navbar-inner">
        <div class="rd-navbar-panel">
          <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
          <div class="rd-navbar-brand"><a href="index.html" class="brand-name"><img src="/job/images/brand.png" alt=""></a></div>
        </div>
        <div class="rd-navbar-nav-wrap">
          <ul class="rd-navbar-nav">
            <?php foreach ($menu->children()->all() as $num => $children): ?>
            <li class="<?php if ($requestUrl == Url::to([$children->link])): ?>active<?php endif;?>">
              <a href="<?=Url::to([$children->link]);?>" title="<?=$children->title;?>"><?=$children->title;?></a>
              <?php if (!empty($children->children()->all())): ?>
              <ul class="rd-navbar-dropdown">
                <?php foreach ($children->children()->all() as $numChild => $childChildren): ?>
                <li>
                  <a href="<?=Url::to([$childChildren->link]);?>" title="<?=$childChildren->title;?>"><?=$childChildren->title;?></a>
                </li>
                <?php endforeach;?>
              </ul>
              <?php endif;?>
            </li>
            <?php endforeach;?>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</header>