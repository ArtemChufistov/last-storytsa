<?php
use yii\helpers\Url;
?>

<ul class="nav">
    <?php foreach ($menu->children($menu->depth + 1)->all() as $num => $child): ?>
    <li class="<?php if ($requestUrl == Url::to([$child->link])): ?>active<?php endif;?>">
        <a href="<?=Url::to([$child->link]);?>">
            <?=$child->icon;?>
            <p><?=$child->title;?></p>
        </a>
    </li>
    <?php endforeach;?>
</ul>