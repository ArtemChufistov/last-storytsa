<?php
use app\modules\profile\widgets\UserTreeElementWidget;
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\matrix\models\MatrixPref;
use app\modules\matrix\models\Matrix;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

$children = [];

$defaultImage = '/images/money_hand.png';
$childs = [0 => [0, 1, 2], 1 => [0, 1, 2], 2 => [0, 1, 2]];
?>

<style>
.timeline-box::before{
    border-left: 2px solid #00a65a;
    margin-top: 10px; 
}
.timeline-box .tl-row{
    height: 40px;
    display: block;
    cursor: pointer;
}
.timeline-box .bg-greeen{
    background-color: #00a65a;
}
.timeline-image{
    width: 30px;
    margin-top: 2px; 
}
</style>

<div class = "row">
    <div class = "col-md-1">
        <div class="timeline-box timeline-box-left" style = "height: 470px;">
            <?php foreach($currentPlace->path()->having('depth <=' . $currentLevel)->all() as $placeItem):?>
                <a class="tl-row" href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $placeItem->slug, 'parentPlaceSlug' => $parentPlace->slug]);?>">
                    <div class="tl-item float-right">
                        <div class="tl-icon bg-greeen">
                            <img class="glyph-icon timeline-image" src = "<?php echo $placeItem->getUser()->one()->getImage();?>">
                        </div>

                    </div>
                </a>
            <?php endforeach;?>
        </div>
    </div>
    <div class = "col-md-11">
        <div class="chartTree" id="custom-colored"></div>
    </div>
</div>

<script type="text/javascript">
var config = {
        container: "#custom-colored",
        nodeAlign: "TOP",
        node: {
            HTMLclass: 'nodeExample1'
        },
        connectors: {
            type: "curve",
            style: {
                "stroke-width": 2,
                "stroke": "#00a65a"
            }
        }
    },
    currentPlace = {
        text: {
            name: "<?php echo $currentPlace->getUser()->one()->login?>",
            title: "<?php echo Yii::t('app', 'Место №{name}', ['name' => $currentPlace->name]);?>"
        },
        image: "<?php echo empty($currentPlace->getUser()->one()->getImage()) ? $defaultImage : $currentPlace->getUser()->one()->getImage(); ?>"
    },

<?php foreach ($childs as $numChild1 => $child1):?>
    <?php $childName = 'child' . $numChild1; $children[] = $childName; $child = $currentPlace->getChildForSort($numChild1);?>
    <?php echo $childName; ?> = {
        parent: currentPlace,
        text:{
            name: "<?php echo empty($child) ? Yii::t('app', 'Свободное') : $child->getUser()->one()->login;?>",
            title: "<?php echo empty($child) ? Yii::t('app', 'место') : Yii::t('app', 'Место №{name}', ['name' => $child->name]);?>"
        },
        image: "<?php echo empty($child) ? $defaultImage : $child->getUser()->one()->getImage(); ?>"
    },

    <?php foreach ($child1 as $numChild2 => &$child2):?>

        <?php if (!empty($child)):?>
            <?php $childChild = $child->getChildForSort($numChild2);?>
        <?php else:?>
            <?php unset($childs[$numChild1][$numChild2]);continue;?>
        <?php endif;?>

        <?php $childChildName = $childName . $numChild2; $children[] = $childChildName;?>
        <?php echo $childChildName; ?> = {
            parent: <?php echo $childName;?>,
            text:{
                name: "<?php echo empty($childChild) ? Yii::t('app', 'Свободное') : $childChild->getUser()->one()->login;?>",
                title: "<?php echo empty($childChild) ? Yii::t('app', 'место') : Yii::t('app', 'Место №{name}', ['name' => $childChild->name]);?>"
            },
            image: "<?php echo empty($childChild) ? $defaultImage : $childChild->getUser()->one()->getImage(); ?>"
        },
    <?php endforeach;?>
<?php endforeach;?>

chart_config = [
    config,
    currentPlace, <?php echo implode(', ', $children)?>
];

new Treant( chart_config );

var myDiv = $('.chartTree');
var parentWidth = myDiv.parent().width();
var scrollto = (1350 - parentWidth) / 2;
myDiv.animate({ scrollLeft:  scrollto});

var node = $('.node');
node.mouseenter(function(){
    var result = null;
    $.ajax({url: '/office/userinfo/' + $(this).find('.node-name').html(), async: false, success: function(ajaxData){result = ajaxData;}})
    $(this).append('<div class = "user_info_wrap">' + result + '</div>');
}).mouseleave(function(){
    $('.user_info_wrap').remove();
});

node.click(function(){
    //location.href = '/profile/office/program?matrixCategory=' + $matrixCategory->id + '&matrix=' + $rootMatrix->slug + '&placeSlug=' + $currentPlace->slug + '&parentPlaceSlug' + $parentPlace->slug;
})
</script>