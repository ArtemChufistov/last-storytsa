<?php
use yii\helpers\Url;
?>
<ul>
  <li class = "<?php if (in_array($active, [1, 2, 3, 4])): ?>acti<?php endif;?>" >
    <a href="<?php echo Url::to(['office/in']); ?>">
      <label class="wizard-step">1</label>
      <span class="wizard-description">
         <?php echo Yii::t('app', 'Ввод суммы'); ?>
         <small><?php echo Yii::t('app', 'Введите сумму для пополнения'); ?></small>
      </span>
    </a>
  </li>
  <li class = "<?php if (in_array($active, [2, 3, 4])): ?>acti<?php endif;?>">
      <a href="<?php echo Url::to(['office/in']); ?>">
          <label class="wizard-step">2</label>
          <span class="wizard-description">
             <?php echo Yii::t('app', 'Выбор валюты'); ?>
             <small><?php echo Yii::t('app', 'Какой валютой вы хотите произвести расчет'); ?></small>
          </span>
      </a>
  </li>
  <li class = "<?php if (in_array($active, [3, 4])): ?>acti<?php endif;?>">
      <a href="<?php echo Url::to(['office/in']); ?>">
          <label class="wizard-step">3</label>
          <span class="wizard-description">
             <?php echo Yii::t('app', 'Выбор платёжной системы'); ?>
             <small><?php echo Yii::t('app', 'Какую платёжную системы вы используете'); ?></small>
          </span>
      </a>
  </li>
  <li class = "<?php if ($active == 4): ?>acti<?php endif;?>">
      <a href="<?php echo Url::to(['office/in']); ?>" >
          <label class="wizard-step">4</label>
          <span class="wizard-description">
             <?php echo Yii::t('app', 'Оплата'); ?>
             <small><?php echo Yii::t('app', 'Проведение платежа и начисление'); ?></small>
          </span>
      </a>
  </li>
</ul>