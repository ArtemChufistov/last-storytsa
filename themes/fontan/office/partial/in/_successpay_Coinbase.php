<?php
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;

$paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
$currencyCourse = CurrencyCourse::find()
	->where(['from_pay_system_id' => $paymentForm->from_pay_system_id])
	->andWhere(['to_pay_system_id' => $paySystem->id])
	->andWhere(['from_currency' => $paymentForm->from_currency_id])
	->one();

$this->title = Yii::t('user', 'Финансы');
?>
<div class="row">
    <div class="col-lg-3">
        <div class="dummy-logo">
            <img src = "/cryptofund/big_logo.jpeg" style = "width: 100px;">
            <p style = "font-size: 14px;">BITINFO</p>
        </div>
    </div>
    <div class="col-lg-5 float-left text-left">
		<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Информация'); ?>:</h2>
		<ul class="reset-ul">
			<li>
				<b><?php echo Yii::t('app', 'Количество USD'); ?>:</b> <strong style = "font-size: 22px;"> <?php echo $paymentForm->to_sum; ?></strong>
			</li>
			<li>
				<b><?php echo Yii::t('app', 'Сумма к оплате'); ?>:</b>
				<strong style = "font-size: 22px;"><?php echo number_format($paymentForm->fromSumWithComission(), 6); ?> <?php echo $paymentForm->getFromCurrency()->one()->key; ?></strong>
			</li>

			<li>
				<b><?php echo Yii::t('app', 'Кошелёк для оплаты'); ?>:</b></br></br> <strong style = "font-size: 22px;"> <?php echo $paymentForm->wallet; ?></strong>
			</li>
		</ul>
    </div>
    <div class="col-md-4">
		<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Дополнительная информация'); ?>:</h2>
		<p><?php echo Yii::t('app', 'Для пополнения личного кабинета на <strong>{countCitt} USD</strong> вам необходимо перевести <strong>{sum} {currency}</strong> на кошелёк: <strong>{wallet}</strong>', [
	'countCitt' => $paymentForm->to_sum,
	'sum' => number_format($paymentForm->fromSumWithComission(), 6),
	'currency' => $paymentForm->getFromCurrency()->one()->key,
	'wallet' => $paymentForm->wallet,
]); ?></p>
		</br>
		<?php if ($paymentForm->getFromCurrency()->one()->key == Currency::KEY_BTC): ?>
			<?php echo Yii::t('app', 'Для удобства перевода вы можете воспользоваться QR кодом:'); ?>
			<img style = "width: 200px; float: left;" src = "/office/qrcode/<?php echo $paymentForm->wallet; ?>/<?php echo $paymentForm->fromSumWithComission(); ?>">
		<?php endif;?>

	</div>
</div>
