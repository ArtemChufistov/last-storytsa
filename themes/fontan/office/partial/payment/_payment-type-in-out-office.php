<?php

use yii\helpers\Html;
use app\modules\payment\models\Payment;

?>
<div class="site-index">

    <?php if ($paymentForm->type == Payment::TYPE_IN_OFFICE):?>
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/payment/_payment-in-office', 
        		['paymentForm' => $paymentForm, 'user' => $user]);?>
    <?php else:?>
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/payment/_payment-out-office', 
        		['paymentForm' => $paymentForm, 'user' => $user]);?>
    <?php endif;?>

</div>
<?php $this->registerJs('new Clipboard(".payment-sum");');?>
<?php $this->registerJs('new Clipboard(".payment-wallet");');?>
