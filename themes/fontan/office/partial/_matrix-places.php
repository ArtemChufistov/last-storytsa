<?php
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixPref;
$counPlaces = 0;

$boxColors = ['warning', 'success', 'info', 'danger', 'primary'];
?>

<div class="content-box">
    <h3 class="content-box-header bg-blue-alt">
        <?php echo Yii::t('app', 'Ваши места');?>
    </h3>
    <div class="content-box-wrapper">
		<?php foreach($rootMatrixes as $num => $rootMatrix):?>
		  <?php 
			  $countChildren = MatrixPref::find()->where(['key' => MatrixPref::KEY_COUNT_CHILDREN, 'matrix_id' => $rootMatrix->id])->one();
			  $depth = MatrixPref::find()->where(['key' => MatrixPref::KEY_DEPTH, 'matrix_id' => $rootMatrix->id])->one();
			  $maxChildren = pow($countChildren->value, $depth->value);

			  if ($depth->value == 2 ){
			  	$maxChildren += pow($countChildren->value, $depth->value - 1);
			  }

			  $matrixPlaces = Matrix::find()->where(['user_id' => $user->id, 'root_id' => $rootMatrix->id])->all();

			  if (empty($matrixPlaces)){
			  	continue;
			  }
			  $counPlaces++;
		  ;?>
			<?php if (empty($matrixPlaces)):?>
				<h4 style = "text-align: center"><?php echo Yii::t('app', 'У вас нет мест в этой программе,</br> <a target= "_blank" href="/howitworks">как получить место</a>');?></h4>
			<?php endif;?>

			<div class = "row">
	        <?php foreach($matrixPlaces as $num => $matrixPlace):?>
	        	<?php $percent = ( 100 / $maxChildren) * $matrixPlace->descendants($depth->value)->count();?>
	        	<div class = "col-md-6" style = "margin-bottom: 20px;">
					<a href="<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $matrixPlace->slug]);?>" title="<?php echo Yii::t('app', 'Место №{numPlace}', ['numPlace' => $matrixPlace->name]);?>" class="tile-box tile-box-alt btn-<?php echo $boxColors[rand(0, count($boxColors) - 1)];?>">
	                    <div class="tile-header">
	                        <?php echo Yii::t('app', 'Место №{numPlace}', ['numPlace' => $matrixPlace->name]);?>
	                    </div>
	                    <div class="tile-content-wrapper">
	                        <div class="chart-alt-10 easyPieChart" data-percent="<?php echo $percent > 98 ? 100 : $percent;?>" style="width: 100px; height: 100px; line-height: 100px;"><span><?php echo $percent > 98 ? 100 : $percent;?></span>%<canvas width="100" height="100"></canvas></div>
	                    </div>
	                </a>
	            </div>
	        <?php endforeach;?>
	        </div>
	  <?php endforeach;?>
	  <?php if ($counPlaces == 0):?>
	  	<?php echo Yii::t('app', 'Для того чтобы получить доходное место нажмите на кнопку <strong>"Принять участие"</strong>');?>
	  <?php endif;?>
    </div>
</div>