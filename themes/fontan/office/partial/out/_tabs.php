<?php
use yii\helpers\Url;
?>
<ul>
  <li class = "<?php if (in_array($active, [1, 2, 3, 4, 5])): ?>acti<?php endif;?>" >
    <a href="<?php echo Url::to(['office/out']); ?>">
      <label class="wizard-step">1</label>
      <span class="wizard-description">
         <?php echo Yii::t('app', 'Сумма для снятия'); ?>
         <small><?php echo Yii::t('app', 'Укажите снимаемую сумму'); ?></small>
      </span>
    </a>
  </li>
  <li class = "<?php if (in_array($active, [2, 3, 4, 5])): ?>acti<?php endif;?>">
      <a href="<?php echo Url::to(['office/out']); ?>">
          <label class="wizard-step">2</label>
          <span class="wizard-description">
             <?php echo Yii::t('app', 'Выбор валюты'); ?>
             <small><?php echo Yii::t('app', 'В какой валюте'); ?></small>
          </span>
      </a>
  </li>
  <li class = "<?php if (in_array($active, [3, 4, 5])): ?>acti<?php endif;?>">
      <a href="<?php echo Url::to(['office/out']); ?>">
          <label class="wizard-step">3</label>
          <span class="wizard-description">
             <?php echo Yii::t('app', 'Выбор платёжной системы'); ?>
             <small><?php echo Yii::t('app', 'На какую платёжную систему'); ?></small>
          </span>
      </a>
  </li>
  <li class = "<?php if (in_array($active, [4, 5])): ?>acti<?php endif;?>">
      <a href="<?php echo Url::to(['office/out']); ?>" >
          <label class="wizard-step">4</label>
          <span class="wizard-description">
             <?php echo Yii::t('app', 'Реквизиты'); ?>
             <small><?php echo Yii::t('app', 'Укажите ваши платёжные реквизиты'); ?></small>
          </span>
      </a>
  </li>
  <li class = "<?php if ($active == 5): ?>acti<?php endif;?>">
      <a href="<?php echo Url::to(['office/out']); ?>" >
          <label class="wizard-step">4</label>
          <span class="wizard-description">
             <?php echo Yii::t('app', 'Получение CITT'); ?>
             <small><?php echo Yii::t('app', 'Сумма к получению'); ?></small>
          </span>
      </a>
  </li>
</ul>