<?php
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;

$paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
$currencyCourse = CurrencyCourse::find()
	->where(['to_pay_system_id' => $paymentForm->to_pay_system_id])
	->andWhere(['from_pay_system_id' => $paySystem->id])
	->andWhere(['to_currency' => $paymentForm->to_currency_id])
	->one();
?>
<div class="row">
    <div class="col-lg-3">
        <div class="dummy-logo">
            <img src = "/cryptofund/big_logo.jpeg" style = "width: 100px;">
            <p style = "font-size: 14px;">BITINFO</p>
        </div>
    </div>
    <div class="col-lg-9 float-left text-left">
		<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Информация'); ?>:</h2>
		<ul class="reset-ul">
			<li>
				<b><?php echo Yii::t('app', 'Списываемое количество CITT'); ?>:</b> <strong style = "font-size: 22px;"> <?php echo number_format($paymentForm->fromSumWithComission(), 6); ?> <?php echo $paymentForm->getFromCurrency()->one()->title; ?></strong>
			</li>
			<li>
				<b><?php echo Yii::t('app', 'Получаемая сумма с учётом комиссии'); ?>:</b>
				<strong style = "font-size: 22px;"><?php echo number_format($paymentForm->to_sum - $paymentForm->comission_to_sum, 6); ?> <?php echo $paymentForm->getToCurrency()->one()->key; ?></strong>
			</li>
			<li>
				<b><?php echo Yii::t('app', 'Кошелёк для оплаты'); ?>:</b><strong style = "font-size: 22px;"> <?php echo $paymentForm->wallet; ?></strong>
			</li>
			<li>
				<b><?php echo Yii::t('app', 'Статус'); ?>:</b><strong style = "font-size: 22px;"> <?php echo $paymentForm->getStatusTitle(); ?></strong>
			</li>
		</ul>
    </div>

</div>
