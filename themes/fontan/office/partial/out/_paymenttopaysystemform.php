<?php
use app\modules\finance\components\paysystem\FchangeComponent;
use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use lowbase\user\UserAsset;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Получить CITT - Выбор платёжной системы');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$fchangeCurrencyArray = FchangeComponent::getCurrencyCourseList();
$fchangePaySystem = PaySystem::find()->where(['key' => PaySystem::KEY_FCHANGE])->one();
$currencyCitt = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();
$sumInUsd = $paymentForm->to_sum * $currencyCitt->course_to_usd;
?>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div id="form-wizard-3" class="form-wizard">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_tabs', [
	'active' => 3,
	'paymentForm' => $paymentForm,
]); ?>
            <div class="tab-content">
              <div class="tab-pane active" id="step-3">
                <div class="content-box">
                    <h3 class="content-box-header bg-yellow">
                      <?php echo Yii::t('app', 'Выбор платёжной системы'); ?>
                    </h3>
                    <div class="content-box-wrapper">
                      <div class="form-group">
                        <div class="col-sm-12" style = "text-align: center;">
                          <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

                          1212

                          <?php ActiveForm::end();?>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  .paySysIcon{
    margin: 10px;
  }
  .paySysWrap{
    margin: 15px;
    height: 120px;
  }
</style>