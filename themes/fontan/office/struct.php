<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Структура');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<div id="page-title">
    <h2><?php echo $this->title;?></h2>
    <p><?php echo Yii::t('app', 'Информация о ваших партнёрах');?></p>
</div>

<div class="row">
  <div class="col-lg-4">
    <div class="content-box">
      <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Ваши бизнес партнёры');?>
        <div class="font-size-11 float-right">
          <a href="#" title=""><i class="glyph-icon opacity-hover icon-coffee"></i></a>
        </div>
      </h3>
      <div class="content-box-wrapper text-center clearfix">
        <?php if (empty($userTree)):?>
          <p class ="text-trancpancy"><?php echo Yii::t('app', 'Пригласите своих друзей и знакомых, пусть они станут участниками как и Вы. Все ваши приглашённые попадут в Вашу структуру, тем самым вы сможете получать подарки все вместе. Для этого необходимо всего-лишь поделиться своей реферальной ссылкой.');?></p>
        <?php endif;?>
        <div id="treeview-searchable" class=""></div>
      </div>
    </div>
  </div>

  <div class="col-lg-4">
    <div class="content-box">
      <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Ваши партнёры');?>
        <div class="font-size-11 float-right">
          <a href="#" title=""><i class="glyph-icon opacity-hover icon-search"></i></a>
        </div>
      </h3>
      <div class="content-box-wrapper text-center clearfix">
        <div class="form-group">
          <label for="input-search" class="sr-only"><?php echo Yii::t('app', 'Поиск по дереву');?>:</label>
          <input type="input" class="form-control" id="input-search" placeholder="Введите для поиска..." value="">
        </div>
        <button type="button" class="btn btn-info" id="btn-search">Поиск</button>
        <button type="button" class="btn btn-default" id="btn-clear-search">Очистить</button>
      </div>
    </div>

    <div class="box box-success resultBox" style = "display: none;">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo Yii::t('app', 'Результат');?></h3>
      </div>

      <div class="box-body no-padding">
        <table class="table table-striped" id = "search-output">
        </table>
      </div>
    </div>
  </div>

  <div class="col-lg-4">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_ref', ['user' => $user]);?>
  </div>

</div>

<?php $this->registerJs("
  var defaultData = " . json_encode($userTree) . "; 
  $(function() {

    var csrfToken = '" . Yii::$app->request->getCsrfToken() . "';

    function showUser(login)
    {
      if ($('.userList').find('[login=\"' + login + '\"]').html() != undefined){
        $('.userList').find('[login=\"' + login + '\"]').show();
        return;
      }

      $.post('/profile/office/showuser', { login: login, _csrf: csrfToken }, function(data){
        $('.userList').append(data);
      });
    }

    jQuery(document).on('click', '.list-group-item', function(){
      showUser(jQuery(this).find('.login').html());
    })

    jQuery('#search-output').on('click', 'td', function(){
      showUser(jQuery(this).find('span').html());
    })

    jQuery('.userList').on('click', '.close', function(){
      $(this).parents('.userInfoPartial').remove();
    })

    var searchableTree = $('#treeview-searchable').treeview({
      data: defaultData,
    });

    var search = function(e) {
      var pattern = $('#input-search').val();

      var results = searchableTree.treeview('search', [ pattern ]);

      var output = '<tr><td>' + results.length + ' " . Yii::t('app', 'совпадений найдено') . "</td></tr>';

      $.each(results, function (index, result) {
        output += '<tr><td>' + result.text + '</td></tr>';
      });

      $('#search-output').html(output);
      $('.resultBox').show();
    }

    $('#btn-search').on('click', search);
    $('#input-search').on('keyup', search);

    $('#btn-clear-search').on('click', function (e) {
      searchableTree.treeview('clearSearch');
      $('#input-search').val('');
      $('#search-output').html('');
    });
  });
", View::POS_END);?>
