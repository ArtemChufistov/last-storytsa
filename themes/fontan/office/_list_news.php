<?php
$bgColors = ['bg-blue-alt', 'bg-green', 'bg-red', 'bg-primary', 'bg-warning'];
?>
<div class="col-md-6">
    <div class="tile-box tile-box-alt <?php echo $bgColors[rand(0, count($bgColors) - 1)];?>">
        <div class="tile-header">
            <?php echo $model->title;?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-tag"></i>
            <small>
                <?php echo $model->smallDescription;?>
            </small>
        </div>
        <span href="#" class="tile-footer tooltip-button" data-placement="bottom">
            <?php echo date('d-m-Y', strtotime($model->date));?>
        </span>
    </div>
</div>