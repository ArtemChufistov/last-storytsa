<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\matrix\models\MatrixUserSetting;
?>
<span class="person tooltips" >

	<?php if (!empty($user)):?>


    	<img src="<?php echo $user->getImage(); ?>" alt="" class = "imgAvatar" 
    		href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $currentPlace->slug, 'parentPlaceSlug' => $parentPlace->slug]);?>">
    <?php elseif (!empty($ancestorPlace)):?>
    	<?php if ($matrixUserSetting->active == MatrixUserSetting::ACTIVE_TRUE):?>

    	<?php else:?>

    	<?php endif;?>

    	<img src="/volcano/whooo.jpg" alt="">
    <?php else:?>
    	<img src="/volcano/whooo.jpg" alt="">
    <?php endif;?>
    
	<?php if (!empty($user)):?>
		<p class="name">
    		<?php echo $user->login;?></br>
    		<b>
				<?php if ($currentUser->id == $user->id):?>
					<?php echo Yii::t('app', '{num} место', ['num' => $currentPlace->name]);?>
				<?php else:?>
					<?php echo  $user->email;?>
				<?php endif;?>

					<?php if ($_SERVER['REMOTE_ADDR'] == '212.20.46.243'):?>
						</br><?php echo $currentPlace->date_add;?>
					<?php endif;?>

    		</b>
    	</p>
	<?php elseif (!empty($ancestorPlace)):?>
        <?php $form = ActiveForm::begin(['options' => ['class' => 'name']]);?>
            <?= $form->field($matrixUserSetting, 'active', ['template' => '{input}'])->checkBox(['class' => 'minimal-red checkBoxPlace']); ?>
          	<?= $form->field($matrixUserSetting, 'sort', ['template' => '{input}'])->hiddenInput()->label(false); ?>
          	<?= $form->field($matrixUserSetting, 'user_login', ['template' => '{input}'])->hiddenInput()->label(false); ?>
          	<?= $form->field($matrixUserSetting, 'matrix_slug', ['template' => '{input}'])->hiddenInput()->label(false); ?>
          	<?= $form->field($matrixUserSetting, 'matrix_root_slug', ['template' => '{input}'])->hiddenInput()->label(false); ?>
        <?php ActiveForm::end(); ?>
    <?php else:?>
    	<p class="name"><?php echo Yii::t('app', 'Свободное</br> <b>место</b>');?></p>
    <?php endif;?>

</span>
