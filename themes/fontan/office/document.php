<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = $model->title;

?>

<div id="page-title">
    <h2><?php echo $this->title;?></h2>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="content-box">
    	<div class="content-box-wrapper text-center clearfix">
    		<?php echo $model->content;?>
    	</div>
    </div>
  </div>
</div>