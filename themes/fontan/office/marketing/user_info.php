<a href="#" class = "user-tree-elem">
    <img src = "/fontan/user3.png" class = "treeImg">

    <div class="panel-layout user-tree-wrap" style = "width: 350px;">
        <div class="panel-box">

            <div class="panel-content image-box" style="height: 100px;">
                <div class="corner-ribbon">
                    <a href="#" class="btn-success tooltip-button" title="Example ribbon">
                        <i class="glyph-icon icon-user"></i>
                    </a>
                </div>
                <img src="/image-resources/blurred-bg/blurred-bg-14.jpg" alt="">

            </div>
            <div class="panel-content pad0A bg-white">
                <div class="meta-box meta-box-offset">
                    <img src="/fontan/user3.png" alt="" class="meta-image img-bordered img-circle" style="width: 70px;">
                    <h3 class="meta-heading font-size-16">login</h3>
                    <h4 class="meta-subheading font-size-13 font-gray">Участник проекта</h4>
                </div>
                <table class="table mrg0B mrg10T">
                    <tbody>
                        <tr>
                            <td class="text-left">
                                <b>E-mail</b>
                            </td>
                            <td style="width: 50%;">
                                 waka@yandex.ru
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left">
                                <b>Skype</b>
                            </td>
                            <td style="width: 50%;">
                                wacken
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left">
                                <b>Телефон</b>
                            </td>
                            <td style="width: 50%;">
                                81234567890
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</a>