<?php
use app\modules\finance\models\Payment;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>

<style type="text/css">
.form-wizard .acti .wizard-step {
  background: #00bca4 none repeat scroll 0 0;
  color: #fff;
}
.form-wizard li a:hover{
  text-decoration: none;
}
</style>

<?php echo Yii::$app->controller->renderPartial(
  '@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_' . strtolower(\yii\helpers\StringHelper::basename(get_class($paymentForm))),
          ['paymentForm' => $paymentForm, 'user' => $user, 'currencyArray' => $currencyArray]); ?>