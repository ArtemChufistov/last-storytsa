<?php
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;

$matrix   = Matrix::find()->where(['user_id' => $this->params['user']->id])->one();
$dopLink  = Url::to(['/profile/office/news']);
$dopTitle = Yii::t('app', 'События');

?>

<div id="page-sidebar" class = "bg-black font-inverse">
  <div class="scroll-sidebar">

    <ul id="sidebar-menu">
    <li class="header"><span><?php echo Yii::t('app', 'Личный кабинет');?></span></li>

    <?php foreach ($menu->children($menu->depth + 1)->all() as $num => $child): ?>
      
      <li class="divider"></li>
      <li class="<?php if (Url::to([$requestUrl]) == Url::to([$child->link])): ?>sfHover<?php endif;?>">
          <a href="<?= Url::to([$child->link]);?>" title="<?=$child->title;?>" class = "sf-with-ul">
              <?=$child->icon;?>
              <span><?=$child->title;?></span>
          </a>
      </li>

      <?php if (!empty($matrix) && $num == 4):?>
        <li class="divider"></li>
        <li class="<?php if (Url::to([$requestUrl]) == $dopLink): ?>sfHover<?php endif;?>">
            <a href="<?=$dopLink;?>" title="<?= $dopTitle;?>" class = "sf-with-ul">
                <i class="glyph-icon icon-linecons-star"></i>
                <span>
                  <?= $dopTitle;?>
                </span>
            </a>
        </li>
      <?php endif;?>

    <?php endforeach;?>
    <li class="divider"></li>
  </div>
</div>