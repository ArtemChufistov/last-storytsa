<header class="page-head <?php if ($requestUrl == '/index' || $requestUrl == '/'):?>slider-menu-position<?php endif;?>">
  <div class="rd-navbar-wrap">
    <nav data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" class="rd-navbar rd-navbar-default rd-navbar-<?php if ($requestUrl == '/index' || $requestUrl == '/'):?>transparent<?php else:?>light<?php endif;?>" data-lg-auto-height="true" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
      <div class="rd-navbar-inner">
        <!-- RD Navbar Panel-->
        <div class="rd-navbar-panel">
          <!-- RD Navbar Toggle-->
          <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
          <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_logo_light_1');?>
        </div>
        <div class="rd-navbar-menu-wrap">
          <div class="rd-navbar-nav-wrap">
            <div class="rd-navbar-mobile-scroll">
              <!--Navbar Brand Mobile-->
              <div class="rd-navbar-mobile-brand"><a href="index.html"><img style='margin-top: -5px;margin-left: -15px;' width='138' height='31' src='/images/logo-light.png' alt=''/></a></div>
              <div class="form-search-wrap">
              </div>
                <ul class="rd-navbar-nav">

                  <?php foreach($menu->children()->all() as $num => $children):?>
                    <li class="<?php if($requestUrl == $children->link):?>active<?php endif;?>" ><a href="<?= $children->link;?>"><span><?= $children->title;?></span></a>
                      <?php if (!empty($children->children()->all())):?>
                        <ul class="rd-navbar-dropdown">
                          <?php foreach($children->children()->all() as $numChild => $childChildren):?>
                            <li><a href="<?= $childChildren->link;?>"><span class="text-middle"><?= $childChildren->title;?></span></a>
                              <ul class="rd-navbar-dropdown">
                                <li><a href="intense-photographer-portfolio"><span class="text-middle"><?= $childChildren->title;?></span></a>
                                </li>
                              </ul>
                            </li>
                          <?php endforeach;?>
                        </ul>
                      <?php endif;?>
                    </li>
                  <?php endforeach;?>
                  </li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
</header>

