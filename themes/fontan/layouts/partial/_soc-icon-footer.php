<div class="cell-xs-12 offset-top-66 cell-lg-3 cell-lg-push-1 offset-lg-top-0">

	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/logo-light-big');?>

  <p class="text-darker offset-top-4"><?php echo \Yii::t('app', 'Мы в социальных сетях');?></p>
  <ul class="list-inline list-inline-xs offset-top-40 offset-md-top-70">
    <li><a href="#" class="icon icon-circle icon-dark icon-xs fa-facebook"></a></li>
    <li><a href="#" class="icon icon-circle icon-dark icon-xs fa-twitter"></a></li>
    <li><a href="#" class="icon icon-circle icon-dark icon-xs fa-youtube"></a></li>
    <li><a href="#" class="icon icon-circle icon-dark icon-xs fa-linkedin"></a></li>
  </ul>
</div>