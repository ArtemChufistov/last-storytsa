<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

?>

<?php if(Yii::$app->session->getFlash('succsessSubscribe')): ?>

<?php Modal::begin([
    'header' => '<h1 class="text-center">' . Yii::t('app', 'Поздравляем!') . '</h1>',
    'toggleButton' => false,
    'id' => 'successSubscribe'
]);
?>
      <?php echo Yii::$app->session->getFlash('succsessSubscribe');?>

<?php Modal::end();?>

<?php $this->registerJs("
  $('#successSubscribe').modal('show');
", View::POS_END);?>

<?php endif;?>

<div class="cell-sm-6 cell-md-5 cell-lg-preffix-1 offset-top-45 offset-lg-top-0">
  <h5 class="text-white"><?php echo Yii::t('app', 'Подписка на новости');?></h5>
  <p class="font-size-10 text-lighter offset-top-5"><?php echo Yii::t('app', 'Чтобы всегда быть в курсе событий Вы можете подписаться на новости, достаточно только указать свой E-mail');?></p>
  <div class="offset-top-35">
      <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'rd-mailform form-subscribe',
            'data-form-type' => 'subscribe',
            'data-form-output' => 'form-subscribe-footer',
            'enctype'=>'multipart/form-data'
        ],
      ]); ?>

        <?= $form->field($subscriber, 'mail')->textInput([
            'maxlength' => true,
            'placeholder' => Yii::t('app', 'Введите свой E-mail')
        ])->label(''); ?>

   
        <div id="form-subscribe-footer" class="form-output"></div>
      <?php ActiveForm::end();?>
  </div>
</div>