<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\fontan\SignUpAppAsset;
use app\modules\menu\widgets\MenuWidget;
use app\assets\fontan\FrontAppAssetTopLogin;

FrontAppAssetTopLogin::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
  <head>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>

  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>

  <?php $this->head() ?>

  </head>
  <body>
    <?php $this->beginBody() ?>

    <div class="page text-center">
      <main class="page-content bg-shark-radio">
        <div class="one-page">
          <div class="one-page-header">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/logo-light-big');?>
          </div>
          <section>
            <div class="shell">
              <div class="range">
                <div class="section-110 section-cover range range-xs-center range-xs-middle">
                  <div class="cell-xs-8 cell-sm-6 cell-md-5">
                    <?= $content ?>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="one-page-footer">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/logo-light-big');?>
          </div>
        </div>
      </main>
    </div>

    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>