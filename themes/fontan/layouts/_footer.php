<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\menu\widgets\MenuWidget;
use app\modules\profile\widgets\SubscribeWidget;
use app\modules\news\widgets\LastNewsWidgetWidget;

?>

<footer class="section-relative section-top-66 section-bottom-34 page-footer bg-gray-base context-dark">
  <div class="shell">
    <div class="range range-sm-center text-lg-left">
      <div class="cell-sm-8 cell-md-12">
        <div class="range range-xs-center">
          <?php /* echo LastNewsWidgetWidget::widget();*/?>

          <?php echo MenuWidget::widget(['menuName' => 'bottomMenu', 'view' => 'bottom-menu']);?>

          <?php echo SubscribeWidget::widget();?>

          <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_soc-icon-footer');?>

        </div>
      </div>
    </div>
  </div>
  <div class="shell offset-top-50">

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46422471 = new Ya.Metrika({
                    id:46422471,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/46422471" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

    <p class="small text-darker">Bitinfo &copy; <span id="copyright-year"></span> . <?php echo Yii::t('app','Политика конфиденциальности');?>
      <!-- {%FOOTER_LINK}-->
    </p>
  </div>
</footer> 
