<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'Faq',
    'url' => '/faq'
  ],
];
$this->title = $model->title;
?>

</header>
<!-- Classic Breadcrumbs-->
<section class="breadcrumb-classic">
  <div class="shell section-34 section-sm-50">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-help-circle icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h2><span class="big">Faq</span></h2>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">

          <?= Breadcrumbs::widget([
            'options' => ['class' => 'list-inline list-inline-dashed p'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>

      </div>
    </div>
  </div>
</section>
<!-- Page Content-->
<main class="page-content">
  <!-- Faq variant 4-->
  <section class="section-66 section-sm-top-110 section-lg-bottom-0">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-sm-9 cell-lg-6">
          <h1 class="text-darker text-lg-left"><?= Yii::t('app', 'Основные вопросы');?></h1>
          <hr class="divider bg-mantis hr-lg-left-0">
          <div class="offset-top-41 offset-lg-top-66 text-left">
                    <!-- Bootstrap Accordion-->
                    <div role="tablist" aria-multiselectable="true" id="accordion-1" class="panel-group accordion offset-top-0">
                      <div class="panel panel-default">
                        <div role="tab" id="headingOne" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Кто имеет возможность стать участником Bit.info?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingOne" id="collapseOne" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Любой человек, независимо от пола, вероисповедания, города и страны проживания, который достиг совершеннолетия.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingTwo" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Что необходимо, чтобы  стать участником Bit.info?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingTwo" id="collapseTwo" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Если вы решили принять участие в работе нашего сервиса, вам необходимо перейти на страницу регистрации, заполнить все поля предложенной формы. После этого ознакомиться, отметить галочкой свое согласие с правилами и нажать на кнопку «Зарегистрироваться».');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingThree" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Как активировать свой личный кабинет?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingThree" id="collapseThree" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Для того, чтобы активировать свой Личный кабинет, каждый новый участник должен внести стартовый взнос  в размере 100 $ на любой из предложенных счетов в платёжных системах.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'На счета каких платежных систем возможен перевод и вывод средств?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseFour" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Программа Bit.info в своей работе использует самые популярные Международные системы электронных платежей и банки с автоматической конвертацией в bitcoin. 
Вывод так же осуществляется на любые bitcoin адреса (кошельки).');?>
                          </div>
                        </div>
                      </div>
                       <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'В течении какого времени производится вывод доступных средств?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseFive" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Согласно правилам проекта Bit.info, максимальный срок вывода заработанных Вами средств 72 часа. Но фактически вывод средств проводится намного быстрее.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSix" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Что получает участник за 100 долларов?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseSix" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'За 100 долларов Вы приобретаете качественные <strong>Информационные Материалы</strong> на тему криптовалют, платные книги, которых нет в свободном доступе, рекомендации специалистов по токенам, криптовалютам и биржам. 
Но самое главное – Вы получаете возможность неограниченного дохода.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSeven" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Каков принцип работы Bit.info?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseSeven" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Система Bit.info состоит из четырёхместной матрицы. Как только под Вами становится 3 человека (2 из которых клоны), матрица закрывается и Вам начисляется вознаграждение, часть средств идёт на открытие матриц клонов, которые так же будут приносить Вам прибыль. Благодаря такой структуре распределения вложений и автоматической конвертации самых популярных платежных систем в биткойн, наш сервис имеет ряд существенных преимуществ перед другими системами, работающими в интернете.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseEight" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Если я не смогу никого пригласить, получу ли я деньги?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseEight" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Да! Матрицы заполняются в общем порядке, независимо от личных приглашений. Но Вы должны понимать, что приглашая партнёров, Вы будете получать дополнительную прибыль постоянно. Причем Вы будете получать доход от лично приглашенных участников не единовременно, а многократно с каждого клона Вашего партнера.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseNine" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Каков максимальный размер моего дохода?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseNine" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Доход активных участников не ограничен.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTen" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Возможно ли зарегистрировать сразу два аккаунта?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseTen" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Да. В системе Bit.info это возможно, но необходимости в этом нет, т.к. у Вас после закрытия каждой матрицы, количество клонов удваивается.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseEleven" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Можно ли приглашать партнёров и зарабатывать не оплачивая 100 $?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseEleven" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Нет. Если вы не оплатите взнос, у вас не будет доступа в личный кабинет и Вы не сможете получать прибыль.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwelve" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Долго ли будет работать ваша система?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseTwelve" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Мы рассчитываем на очень долгую работу. Благодаря особенностям нашего уникального маркетинга (качественный информационный продукт, короткая матрица, множество клонов), для работоспособности нашей системы не требуется большого количества новых участников за счет реинвеста с каждой закрытой матрицы.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThirteen" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Не нашли нужного ответа?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseThirteen" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Вы можете обратиться в Службу поддержки пользователей, где оператор поможет вам получить ответы на все ваши вопросы о работе сервиса.');?>
                          </div>
                        </div>
                      </div>
                    </div>
          </div></br>
        </div>
        <div class="cell-lg-6 offset-top-0"><img src="images/pages/faq-01-531x699.png" width="531" height="699" alt="" class="veil reveal-lg-inline-block"></div>
      </div>
    </div>
  </section>
</main>
