<?php
use yii\helpers\Url;
use yii\helpers\Html;
use lowbase\document\DocumentAsset;
use app\modules\profile\models\User;
use app\modules\finance\models\Payment;

$this->title = $model->title;
?>

<div data-on="false" data-md-on="true" class="bg-gray-base context-dark rd-parallax">
  <div data-speed="0.45" data-type="media" data-url="images/intros/main_slide.jpg" class="rd-parallax-layer"></div>
  <div data-speed="0.3" data-type="html" data-md-fade="true" class="rd-parallax-layer">
    <div class="bg-overlay-gray-darkest">
      <div class="shell">
        <div class="range">
          <div class="section-110 section-cover range range-xs-center range-xs-middle">
            <div class="cell-lg-12">
              <h1><span data-caption-animate="fadeInUp" data-caption-delay="700" class="text-spacing-60 big text-bold text-uppercase"><?php echo Yii::t('app', 'Добро пожаловать');?></span></h1>
              <h4 data-caption-animate="fadeInUp" data-caption-delay="900" class="offset-top-34 hidden reveal-sm-block text-light">
                <?php echo Yii::t('app', 'Cильнейший инструмент для заработка и получения стабильного дохода на новых технологиях.');?>
              </h4>
              <div class="group group-xl offset-top-20 offset-xs-top-50">
              <a href="/login" data-caption-animate="fadeInUp" data-caption-delay="1200" class="btn btn-primary btn-lg btn-anis-effect"><span class="btn-text"><?php echo Yii::t('app', 'Личный кабинет');?></span></a>
              <a href="/signup" data-custom-scroll-to="home-section-welcome" data-caption-animate="fadeInUp" data-caption-delay="1200" class="btn btn-default btn-lg btn-anis-effect"><span class="btn-text"><?php echo Yii::t('app', 'Регистрация');?></span></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<main class="page-content">
  <section id="home-section-welcome" class="section-top-98 section-sm-top-110 section-sm-bottom-110 section-lg-top-66 section-bottom-98 section-lg-bottom-0">
    <div class="shell">
      <div class="range range-sm-center range-md-middle">
        <div class="cell-lg-5 veil reveal-lg-inline-block"><img width="470" height="540" src="images/pages/bit_center_info.jpg" alt="" class="img-responsive center-block"></div>
        <div class="cell-sm-10 cell-lg-preffix-1 cell-lg-5 section-lg-bottom-66 text-lg-left">
          <h1 class="text-bold"><?php echo Yii::t('app', 'BITINFO CLUB');?></h1>
          <hr class="divider bg-mantis hr-lg-left-0">
          <div class="offset-top-41">
            <p><?php echo Yii::t('app', 'это современная система по продаже информационных продуктов в сфере криптовалют и технологии блокчейн, с автоматизированной партнерская программой для активных участников.');?>.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section-98 section-sm-110 bg-lightest">
    <div class="shell">
      <div class="range text-md-left">
        <div class="cell-md-4">
          <!-- Icon Box Type 1--><span class="icon icon-sm icon-primary mdi mdi-code-tags"></span>
          <div class="text-italic text-gray-dark small"><?php echo Yii::t('app', 'Сайт рыбатекст поможет');?></div>
          <div class="offset-top-10">
            <h4 class="text-uppercase text-bold"><?php echo Yii::t('app', 'Доступно');?></h4>
            <p><?php echo Yii::t('app', ' Благодаря  «BITINFO.CLUB» любой человек имеет возможность разобраться в новом направлении и получать неограниченный доход.');?></p>
          </div>
        </div>
        <div class="cell-md-4">
          <!-- Icon Box Type 1--><span class="icon icon-sm icon-primary mdi mdi-cube-outline"></span>
          <div class="text-italic text-gray-dark small"><?php echo Yii::t('app', 'Задача организации, в особенности');?></div>
          <div class="offset-top-10">
            <h4 class="text-uppercase text-bold"><?php echo Yii::t('app', 'стабильно');?></h4>
            <p><?php echo Yii::t('app', ' С «BITINFO.CLUB» Ваша прибыль увеличится в разы, за счёт привлекательного плана партнёрских вознаграждений.');?></p>
          </div>
        </div>
        <div class="cell-md-4">
          <!-- Icon Box Type 1--><span class="icon icon-sm icon-primary mdi mdi-monitor"></span>
          <div class="text-italic text-gray-dark small"><?php echo Yii::t('app', 'Значимость этих проблем');?></div>
          <div class="offset-top-10">
            <h4 class="text-uppercase text-bold"><?php echo Yii::t('app', 'выгодно');?></h4>
            <p><?php echo Yii::t('app', 'Вам не придется проходить несколько матриц для того, чтобы выйти на вознаграждение, Вы начнёте получать прибыль сразу.');?></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section-98 section-sm-110">
    <div class="shell">
      <h2 class="text-bold"><?php echo Yii::t('app', '«BITINFO.CLUB» - это реальный шанс достичь финансового благополучия!');?></h2>
      <hr class="divider bg-mantis">
      <div class="range range-md-middle offset-top-66 range-md-center">
        <div class="cell-md-3 cell-md-push-1"><img src="images/pages/something-interesting-about-mlm-software.png" alt="" width="247" height="470" class="center-block img-responsive"></div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 text-md-right">
          <div class="unit unit-spacing-sm unit-inverse unit-md unit-md-horizontal">
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'неограниченные финансовые возможности');?></h4>
              <p><?php echo Yii::t('app', 'Пассивный доход является ключом к финансовой свободе, когда Вы можете наслаждаться свободным временем и деньгами. Это возможность зарабатывайте столько - сколько Вы захотите');?></p>
            </div>
            <div class="unit-right"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-message-text-outline"></span></div>
          </div>
          <div class="unit unit-spacing-sm unit-inverse unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Новые горизонты');?></h4>
              <p><?php echo Yii::t('app', 'Огромные перспективы и возможности к достижению поставленных целей.
Готовая уникальная система развития бизнеса для скоростного пути к вершине успеха');?></p>
            </div>
            <div class="unit-right"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-image-filter"></span></div>
          </div>
          <div class="unit unit-spacing-sm unit-inverse unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Работай из любой точки мира');?></h4>
              <p><?php echo Yii::t('app', 'Начните строить мощный и перспективный бизнес там, где Вам удобно. Путешествуйте и наслаждайтесь прекрасными моментами жизни с BITINFO.CLUB');?></p>
            </div>
            <div class="unit-right"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-emoticon-happy"></span></div>
          </div>
        </div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 text-md-left cell-md-push-1 offset-top-50 offset-md-top-0">
          <div class="unit unit-spacing-sm unit-md unit-md-horizontal">
            <div class="unit-left"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-cellphone-link"></span></div>
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Многократное получение прибыли');?></h4>
              <p><?php echo Yii::t('app', 'Преимущества умного механизма для достижения максимального эффекта в кратчайшие сроки');?><</p>
            </div>
          </div>
          <div class="unit unit-spacing-sm unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-left"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-television-guide"></span></div>
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Уникальный высокоэффективный маркетинг');?></h4>
              <p><?php echo Yii::t('app', 'Уникальный маркетинг разработан лучшими интернет - маркетологами. Вам не придется проходить несколько матриц для того, чтобы выйти на вознаграждение, Вы начнёте получать прибыль сразу');?></p>
            </div>
          </div>
          <div class="unit unit-spacing-sm unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-left"><span class="icon icon-circle icon-md icon-pink-filled mdi mdi-play-box-outline"></span></div>
            <div class="unit-body">
              <h4 class="text-uppercase text-bold offset-md-top-20"><?php echo Yii::t('app', 'Выгодная партнерская программа');?></h4>
              <p>
                <?php echo Yii::t('app', 'Вы легко соберете доходный и сбалансированный инвестиционный портфель из криптовалют и выйдите на постоянный пассивный доход!');?>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="range range-condensed">
      <div style="background-image: url(images/intros/intro-07-1920x1078.jpg)" class="cell-md-6 bg-image"></div>
      <div class="cell-md-6">
        <div class="bg-lightest">
          <div class="inset-left-11p inset-right-11p section-98 section-sm-110">
            <div class="small text-uppercase text-spacing-60 text-primary"><?php echo Yii::t('app', 'Соображения высшего порядка');?></div>
            <h2 class="text-bold offset-top-14"><?php echo Yii::t('app', 'Соображения высшего порядка, а также реализация');?></h2>
            <div class="offset-top-41">
              <!-- Media Elements-->
              <!-- RD Video-->
              <div data-rd-video-path="video/intense/intense" data-rd-video-title="What can we say about Intense ?" data-rd-video-muted="true" data-rd-video-preview="video/intense/intense-preview.jpg" class="rd-video-player play-on-scroll">
                <div class="rd-video-wrap embed-responsive-16by9">
                  <div class="rd-video-preloader"></div>
                  <video preload="metadata"></video>
                  <div class="rd-video-preview"></div>
                  <div class="rd-video-top-controls">
                    <!-- Title--><span class="rd-video-title"></span><a href="#" class="rd-video-fullscreen mdi mdi-fullscreen rd-video-icon"></a>
                  </div>
                  <div class="rd-video-controls">
                    <div class="rd-video-controls-buttons">
                      <!-- Play\Pause button--><a href="#" class="rd-video-play-pause mdi mdi-play"></a>
                      <!-- Progress bar-->
                    </div>
                    <div class="rd-video-progress-bar"></div>
                    <div class="rd-video-time"><span class="rd-video-current-time"></span> <span class="rd-video-time-divider">:</span>  <span class="rd-video-duration"></span></div>
                    <div class="rd-video-volume-wrap">
                      <!-- Volume button--><a href="#" class="rd-video-volume mdi mdi-volume-high rd-video-icon"></a>
                      <div class="rd-video-volume-bar-wrap">
                        <!-- Volume bar-->
                        <div class="rd-video-volume-bar"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>