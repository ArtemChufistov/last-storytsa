<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = 'Восстановление пароля';
?>

<div class="panel section-34 section-sm-41 inset-left-20 inset-right-20 inset-sm-left-20 inset-sm-right-20 inset-lg-left-30 inset-lg-right-30 bg-white shadow-drop-md">
  <span class="icon icon-circle icon-bordered icon-lg icon-default mdi mdi-account-multiple-outline"></span>
  <div>
    <div class="offset-top-24 text-darker big text-bold"><?= Html::encode($this->title) ?></div>
  </div>
  
<?php if (Yii::$app->session->hasFlash('reset-success')):?>
    <div class='text-center'><?php echo Yii::$app->session->getFlash('reset-success');?></div>
<?php else: ?>

  <?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}",
    ],
    'options' => [
        'data-form-output' => 'form-output-global',
        'data-form-type' => 'contact',
        'class' => 'text-left offset-top-30'
    ]
  ]); ?>

    <div class="form-group">

        <?= $form->field($forget, 'email',
          ['template'=>'<div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-outline"></span></span>{input}</div>{error}']
          )->textInput([
            'maxlength' => true,
            'placeholder' => $forget->getAttributeLabel('email'),
            'options' => [ 'class' => 'form-control']
        ]); ?>
      
    </div>
    <div class="form-group offset-top-20">
        <?= $form->field($forget, 'password',
          ['template'=>'<div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-outline"></span></span>{input}</div>{error}']
          )->textInput([
            'maxlength' => true,
            'placeholder' => $forget->getAttributeLabel('password'),
            'options' => [ 'class' => 'form-control']
        ]); ?>
    </div>

    <div class="form-group offset-top-20">
        <?= $form->field($forget, 'captcha')->widget(Captcha::className(), [
            'captchaAction' => '/profile/profile/captcha',
            'options' => [
                'class' => 'form-control',
                'placeholder' => $forget->getAttributeLabel('captcha')
            ],
            'template' => '<div class="row">
                    <div class="col-lg-8">{input}</div>
                    <div class="col-lg-4">{image}</div>
                    </div>',
        ]);
        ?>
    </div>


    <?= Html::submitButton(Yii::t('user', 'Сменить пароль'), [
          'class' => 'btn btn-sm btn-icon btn-block btn-primary offset-top-20 offset-bottom-10',
          'name' => 'login-button']) ?>


	<?php ActiveForm::end();?>
<?php endif;?>


  <div class="offset-top-30 text-sm-left text-dark text-extra-small">
  <?=Html::a(Yii::t('user', 'Личный кабинет'), ['/login'], ['class' => 'text-picton-blue'])?>
  <br/>
  <?=Html::a(Yii::t('user', 'Регистрация'), ['/signup'], ['class' => 'text-picton-blue'])?>
  </div>
</div>