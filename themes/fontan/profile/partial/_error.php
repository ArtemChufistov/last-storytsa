<?php

use yii\bootstrap\Modal;
use yii\web\View;

Modal::begin([
	'header' => '<div style = "width: 90%;">' . Yii::t('app', 'Ошибка при регистрации') . '</div>',
	'toggleButton' => false,
	'id' => 'error',
]);
?>
    <p class="hint-block">
        <?php echo $error; ?>
    </p>
<?php Modal::end();?>

<script>
<?php $this->registerJs("
  $('#error').modal('show');
", View::POS_END);?>
</script>