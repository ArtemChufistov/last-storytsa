<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use app\modules\profile\components\AuthChoice;

$this->title = Yii::t('user', 'Вход на сайт');
$this->params['breadcrumbs'][] = $this->title;
UserAsset::register($this);
?>

<div class="panel section-34 section-sm-41 inset-left-20 inset-right-20 inset-sm-left-20 inset-sm-right-20 inset-lg-left-30 inset-lg-right-30 bg-white shadow-drop-md">
  <span class="icon icon-circle icon-bordered icon-lg icon-default mdi mdi-account-multiple-outline"></span>
  <div>
    <div class="offset-top-24 text-darker big text-bold"><?= Html::encode($this->title) ?></div>
    <p class="text-extra-small text-dark offset-top-4"><?= Yii::t('app', 'Пожалуйста введите свой Логин и пароль');?></p>
  </div>
  
  <?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}",
    ],
    'options' => [
        'data-form-output' => 'form-output-global',
        'data-form-type' => 'contact',
        'class' => 'text-left offset-top-30'
    ]
  ]); ?>

    <div class="form-group">

        <?= $form->field($model, 'loginOrEmail',
          ['template'=>'<div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-outline"></span></span>{input}</div>{error}']
          )->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('loginOrEmail'),
            'options' => [ 'class' => 'form-control']
        ]); ?>
      
    </div>
    <div class="form-group offset-top-20">
        <?= $form->field($model, 'password',
          ['template'=>'<div class="input-group input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-lock-open-outline"></span></span>{input}</div>{error}']
          )->passwordInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('password'),
            'options' => [ 'class' => 'form-control']
        ]); ?>
    </div>
    
    <?= Html::submitButton('<i class="glyphicon glyphicon-log-in"></i> '.Yii::t('user', 'Войти'), [
          'class' => 'btn btn-sm btn-icon btn-block btn-primary offset-top-20 offset-bottom-10',
          'name' => 'login-button']) ?>


  <?php ActiveForm::end(); ?>

<?php /*
  <p class="text-extra-small text-dark offset-top-4"><?=Yii::t('user', 'Войти с помощью социальных сетей')?>:</p>
  <div class="text-center" style="text-align: center">
              <?=AuthChoice::widget([
                'baseAuthUrl' => ['/profile/auth/index'],
                'clientCssClass' => 'col-xs-3',
              ])?>
  </div>
  */
  ?>

  <div class="offset-top-30 text-sm-left text-dark text-extra-small">
  <?=Yii::t('user', 'Если')?> <?=Html::a(Yii::t('user', 'регистрировались'), ['/signup'], ['class' => 'text-picton-blue'])?> <?=Yii::t('user', 'ранее, но забыли пароль, нажмите')?>
  <?=Html::a(Yii::t('user', 'восстановить пароль'), ['/resetpasswd'], [
      'class' => 'text-picton-blue'
  ])?>.
  </div>
</div>