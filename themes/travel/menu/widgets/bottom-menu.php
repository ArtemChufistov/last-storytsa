<?php
use yii\web\View;
use yii\helpers\Url;
?>

<div class="cell-xs-5 offset-top-41 offset-xs-top-0 text-xs-left cell-md-3 cell-lg-2 cell-lg-push-3">
  <h6 class="text-uppercase text-spacing-60"><?php echo Yii::t('app', 'Полезные ссылки');?></h6>
  <div class="reveal-block">
    <div class="reveal-inline-block">
      <ul class="list list-marked">

        <?php foreach($menu->children($menu->depth + 1)->all() as $num => $child):?>
          <li><a href="<?= $child->link;?>"><?= $child->title;?></a></li>
        <?php endforeach;?>

      </ul>
    </div>
  </div>
</div>