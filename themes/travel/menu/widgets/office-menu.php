<ul class="sidebar-menu">
  <li class="header"><?= Yii::t('app', 'Личный кабинет');?></li>
  <?php foreach($menu->children($menu->depth + 1)->all() as $num => $child):?>
    <?php $children = $child->children()->all();?>
    <li class="<?php if($requestUrl == $child->link):?>active<?php endif;?> treeview">
      <a href="<?= $child->link;?>">
        <?= $child->icon;?> <span><?= $child->title;?></span>
        <?php if (!empty($children)):?>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        <?php endif;?>
      </a>
      <?php if (!empty($children)):?>
        <ul class="treeview-menu">
          <?php foreach($children as $numChild => $childChildren):?>
            <li><a href="<?= $childChildren->link;?>"><?= $childChildren->icon;?> <?= $childChildren->title;?></a></li>
          <?php endforeach;?>
        </ul>
      <?php endif;?>
    </li>
  <?php endforeach;?>

  <?php $user = Yii::$app->user->identity;?>

</ul>