<div class="page-loader page-loader-variant-1">
  <div><img class='img-responsive' style='margin-top: -20px;margin-left: -18px;' width='330' height='67' src='images/childs/green-space-logo-big-330x67.png' alt=''/>
    <div class="offset-top-41 text-center">
      <div class="spinner"></div>
    </div>
  </div>
</div>
<!-- Page Head-->
<header class="page-head slider-menu-position">
  <!-- RD Navbar Transparent-->
  <div class="rd-navbar-wrap">
    <nav data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" class="rd-navbar rd-navbar-default rd-navbar-transparent" data-lg-auto-height="true" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
      <div class="rd-navbar-inner">
        <!-- RD Navbar Panel-->
        <div class="rd-navbar-panel">
          <!-- RD Navbar Toggle-->
          <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
          <!--Navbar Brand-->
          <div class="rd-navbar-brand"><a href="/"><img class='img-responsive' width='246' height='32' src='images/childs/green-space-logo-light-246x32.png' alt=''/></a></div>
        </div>
        <div class="rd-navbar-menu-wrap">
          <div class="rd-navbar-nav-wrap">
            <div class="rd-navbar-mobile-scroll">
              <!--Navbar Brand Mobile-->
              <div class="rd-navbar-mobile-brand"><a href="/"><img class='img-responsive' width='246' height='32' src='images/childs/green-space-logo-light-246x32.png' alt=''/></a></div>

              <ul class="rd-navbar-nav">
                <?php foreach($menu->children()->all() as $num => $children):?>
                  <li class="<?php if($requestUrl == $children->link):?>active<?php endif;?>" ><a href="<?= $children->link;?>"><span><?= $children->title;?></span></a>
                    <?php if (!empty($children->children()->all())):?>
                      <ul class="rd-navbar-dropdown">
                        <?php foreach($children->children()->all() as $numChild => $childChildren):?>
                          <li><a href="<?= $childChildren->link;?>"><span class="text-middle"><?= $childChildren->title;?></span></a>
                            <ul class="rd-navbar-dropdown">
                              <li><a href="<?= $childChildren->link;?>"><span class="text-middle"><?= $childChildren->title;?></span></a>
                              </li>
                            </ul>
                          </li>
                        <?php endforeach;?>
                      </ul>
                    <?php endif;?>
                  </li>
                <?php endforeach;?>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
</header>