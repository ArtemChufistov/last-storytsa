<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\web\View;

$this->title = Yii::t('app', 'Контакты');

$this->params['breadcrumbs']= [ [
    'label' => $this->title,
    'url' => '/contact'
  ],
]

?>

<?php if(Yii::$app->session->hasFlash('success')): ?>

	<div class="successModal modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><?= Yii::t('app', 'Ваше сообщение принято');?></h4>
	      </div>
	      <div class="modal-body">
	        <p><?= Yii::t('app', 'Спасибо, что оставили нам сообщение, мы обязательно свяжемся с вам в ближайшее время');?></p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Закрыть');?></button>
	        <button type="button" class="btn btn-primary" data-dismiss="modal"><?= Yii::t('app', 'Ok');?></button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<?php echo $this->registerJs("$('.successModal').modal('show');", View::POS_END);?>

<?php endif;?>

<!-- Classic Breadcrumbs-->
<section class="breadcrumb-classic headmenusection">
<div class="shell section-34 section-sm-50">
  <div class="range range-lg-middle">
    <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-map-marker-circle icon icon-white"></span></div>
    <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
      <h2><span class="big" style = "color: white;"><?= Yii::t('app', 'Контакты');?></span></h2>
    </div>
    <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">

      <?= Breadcrumbs::widget([
        'options' => ['class' => 'list-inline list-inline-dashed p'],
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
      ]) ?>

    </div>
  </div>
</div>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
  <defs>
    <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
      <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
      <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
    </lineargradient>
  </defs>
  <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
</svg>
</section>

<main class="page-content">
  <!-- Get in Touch-->
  <section class="section-98 section-sm-110">
    <div class="shell">
      <h1><?php echo Yii::t('app', 'Техническая 24/7 Поддержка');?></h1>
      <hr class="divider bg-mantis">
      <div class="range range-sm-center offset-top-66">
        <div class="cell-sm-6"><img src="/images/users/user-milana-stark-140x140.jpg" width="140" height="140" alt="" class="img-circle">
          <p class="offs offset-top-30"><?php echo Yii::t('app', 'Наша техническая служба доступна 24 часа в день, 7 дней в неделю для того, чтобы помочь вам построить ваш собственный бизнес.');?></p>
          <h4 class="text-bold text-picton-blue"><span class="p"><a href="mailto:support@volcano-travel.com">support@volcano-travel.com</a></span></h4>
        </div>
      </div>
    </div>
  </section>
  <section class="bg-gray-darkest">
    <!-- Contact Me-->
    <section class="context-dark">
          <!-- RD Parallax-->
          <div data-on="false" data-md-on="true" class="rd-parallax">
            <div data-speed="0.35" data-type="media" data-url="images/backgrounds/435617.jpg" class="rd-parallax-layer"></div>
            <div data-speed="0" data-type="html" class="rd-parallax-layer">
              <div class="shell section-98 section-sm-110">
                <h1><?php echo Yii::t('app', 'Напишите нам');?></h1>
                <hr class="divider bg-mantis">
                <div class="range range-sm-center offset-top-0">
                  <div class="cell-sm-8">

                    <?php $form = ActiveForm::begin(); ?>

                        <div class="range">
                          <div class="cell-lg-6">
                            <div class="form-group">
                              <?= $form->field($model, 'name') ?>
                            </div>
                          </div>
                          <div class="cell-lg-6 offset-top-20 offset-lg-top-0">
                            <div class="form-group">
                              <?= $form->field($model, 'email') ?>
                            </div>
                          </div>
                          <div class="cell-lg-12 offset-top-20">
                            <div class="form-group">
                              <?= $form->field($model, 'message')->textArea(['rows' => '6']); ?>
                            </div>
                          </div>
                        </div>
                        <div class="group-sm text-center text-lg-left offset-top-30">

                          <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary']) ?>
                        </div>

                  <?php ActiveForm::end(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
    </section>
  </section>
</main>