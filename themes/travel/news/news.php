<?php
use yii\web\View;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Новости');
?>

<section class="breadcrumb-classic headmenusection">
  <div class="shell section-34 section-sm-50">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="mdi mdi-delta icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h3><span class="big" style = "color: white;"><?= Yii::t('app', 'Новости');?></span></h3>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
        <ul class="list-inline list-inline-dashed p">
          <li><a href="/"><?= Yii::t('app', 'Главная');?></a></li>
          <li><?= Yii::t('app', 'Новости');?>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
  </svg>
</section>
<!-- Page Content-->
<main class="page-content section-41 section-sm-41">
  <div class="shell">
    <div class="range range-xs-center">
      <div class="cell-md-12 cell-lg-12">

        <section>

          <?php foreach($dataProvider->getModels() as $model):?>
            <hr class="hr bg-lighter offset-top-41 offset-sm-top-66">
            <div class="offset-top-41 offset-sm-top-66">
                <article class="post post-classic">
                  <!-- Post media-->
                  <header class="post-media">
                    <img src="<?= $model->image;?>" style = "width: 100%;">
                  </header>
                  <!-- Post content-->
                  <section class="post-content text-left offset-top-41">
                    <div class="unit unit-sm unit-sm-horizontal unit-sm-inverse" style = "float: left;">
                      <div class="unit-body" >
                        <!-- Post Title-->
                        <h3 class="offset-top-10"><a href="<?php echo Url::to(['/news/frontnews/show', 'slug' => $model->slug]);?>"><?= $model->title;?></a></h3>
                        <!-- Post Body-->
                        <div class="post-body">
                          <p><?= $model->smallDescription;?></p>
                        </div>

                      </div>
                    </div>
                  </section>
                </article>
            </div>
            </br>
            </br>
            </br>
          <?php endforeach;?>

        </section>
      </div>
    </div>
  </div>
</main>