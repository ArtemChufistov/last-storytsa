<?php
use yii\web\View;
use yii\helpers\Url;

$this->title = $model->title;
?>

  <section class="breadcrumb-classic headmenusection">
    <div class="shell section-34 section-sm-50">
      <div class="range range-lg-middle">
        <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="mdi mdi-arrange-send-to-back icon icon-white"></span></div>
        <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
          <h2><span class="big" style = "color: white;"><?= $model->title;?></span></h2>
        </div>
        <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
          <ul class="list-inline list-inline-dashed p">
            <li><a href="/"><?= Yii::t('app', 'Главная');?></a></li>
            <li><a href="/news"><?= Yii::t('app', 'Новости');?></a></li>
            <li><?= $model->title;?>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
      <defs>
        <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
          <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
          <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
        </lineargradient>
      </defs>
      <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
    </svg>
  </section>
  <!-- Page Content-->
  <main class="page-content">
    <!-- Blog Wide Single post-->
    <section style="background-image: url(/<?php echo $model->image;?>); background-position: right center;" class="bg-fixed">
      <div class="section-split bg-white">
        <div class="inset-left-11p inset-right-11p section-98 section-sm-110">
          <article>
            <h3 class="offset-top-14"><?= $model->title;?></h3>
            <p class="text-left offset-top-24"><?= $model->fullDescription;?></p>
          </article>
          <hr class="offset-top-66">
        </div>
      </div>
    </section>
  </main>