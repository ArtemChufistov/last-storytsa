<?php
use yii\web\View;
use yii\helpers\Url;

$this->title = $model->title;
?>
<section>
  <div data-on="false" data-md-on="true" class="bg-gray-base context-dark rd-parallax">
    <div data-speed="0.45" data-type="media" data-url="images/backgrounds/background-42-1920x1024.jpg" class="rd-parallax-layer"></div>
    <div data-speed="0.3" data-type="html" data-md-fade="true" class="rd-parallax-layer">
      <div class="shell">
        <div class="range">
          <div class="range range-xs-middle range-xs-center section-cover section-98 section-sm-110 text-md-left context-dark">
            <div class="cell-xs-12">
              <div>
                <h2 class="font-default text-italic text-regular"><?= Yii::t('app', 'Добро пожаловать');?></h2>
              </div>
              <div class="offset-top-4 offset-lg-top-0">
                <h1 class="text-capitalize"><span class="big"><?= Yii::t('app', 'Volcano Travel');?></span></h1>
              </div>
              <div class="range offset-top-4 offset-lg-top-30">
                <div class="cell-lg-6">
                  <h6 class="font-default text-regular"><?= Yii::t('app', 'Вас интересует возможность побывать в лучших уголках нашей планеты, и при этом заработать денег?');?></h6>
                </div>
              </div>
              <div class="group group-xl offset-top-30">
                <a href="<?php echo Url::to(['/profile/profile/signup']);?>" class="btn btn-lg btn-warning"><?= Yii::t('app', 'Присоединиться');?></a>
                <a href="<?php echo Url::to(['/profile/profile/login']);?>"  class="btn btn-lg btn-default"><?= Yii::t('app', 'Личный кабинет');?></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  <!-- Page Contents-->
  <main class="page-content">
    <!-- Welcome-->
    <section id="welcome" class="section-top-98 section-sm-top-110">
      <div class="shell">
        <h1><?= Yii::t('app', 'Вы желаете отдохнуть так, чтобы Ваши друзья и знакомые умерли от зависти?');?></h1>
        <hr class="divider divider-lg bg-primary">
        <div class="range range-xs-center">
          <div class="cell-sm-9 cell-lg-8">
            <p><?= Yii::t('app', 'В этом проекте Вы не только осуществите свои самые смелые мечты и побываете там, где всегда Вам хотелось отдохнуть. Для Вас - уникальные предложения от Компании по отдыху в частных апартаментах на лучших мировых курортах.');?></p>
          </div>
        </div>
      </div>
      <!-- Mock Up-->
      <div style="overflow: hidden;" class="offset-top-50">
        <div class="mock-up-wrapper veil reveal-sm-inline-block">
          <div data-wow-duration="1s" data-wow-delay="0s" class="mock-up-desktop-wrapper wow fadeInUpBig"><img src="images/moqups/moqup-home-desktop.png" width="615" height="508" alt="" class="mock-up-desktop img-responsive"></div>
          <div data-wow-delay="1.6s" class="mock-up-circle veil reveal-md-block wow zoomIn"><img src="images/moqups/moqup-home-ellipse.png" width="309" height="315" alt=""></div>
          <div data-wow-delay="0.9s" class="mock-up-mobile-wrapper wow fadeInUp"><img src="images/moqups/moqup-home-mobile.png" width="247" height="470" alt="" class="mock-up-mobile"></div>
        </div>
      </div>
    </section>
    <!-- Take Full Control of Your Site-->
    <section class="section-98 section-sm-110 bg-white-lilac">
      <div class="shell">
        <h1><?= Yii::t('app', 'Начните жить страстно и красиво!');?></h1>
        <hr class="divider divider-lg bg-mantis">
        <div class="range range-xs-center">
          <div class="cell-sm-9 cell-lg-8">
            <p><?= Yii::t('app', 'Проекты под брендом «Volcano» - это не только прекрасный инструмент заработка, это новый стиль жизни. О такой жизни многие только мечтают.');?></p>
          </div>
        </div>
        <div class="range range-xs-center offset-top-66">
          <div class="cell-sm-8 cell-md-4">
            <!-- Icon Box Type 2-->
            <div class="unit unit-sm unit-sm-horizontal text-sm-left">
              <div class="unit-left"><span class="icon icon mdi mdi-crosshairs-gps icon-mantis-filled icon-circle"></span></div>
              <div class="unit-body">
                <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Маркетинг проекта');?></h4>
                <p><?= Yii::t('app', 'имеет продуманную бонусную программу поощрений, разработан группой сетевиков и учитывает интересы как опытных, так и начинающих дистрибьюторов. Заработать и осуществить свою мечту смогут все, без исключения, кто будет рассказывать об этой уникальной возможности другим людям.');?></p>
              </div>
            </div>
          </div>
          <div class="cell-sm-8 cell-md-4 offset-top-66 offset-md-top-0">
            <!-- Icon Box Type 2-->
            <div class="unit unit-sm unit-sm-horizontal text-sm-left">
              <div class="unit-left"><span class="icon icon mdi mdi-account-plus icon-mantis-filled icon-circle"></span></div>
              <div class="unit-body">
                <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Становитесь участником');?></h4>
                <p><?= Yii::t('app', '«Volcano Travel» и Вас ожидают много приключений и невероятных открытий. Начните жить легко и непринужденно. Возьмите под контроль свою жизнь, и у Вас все получится!
');?></p>
              </div>
            </div>
          </div>
          <div class="cell-sm-8 cell-md-4 offset-top-66 offset-md-top-0">
            <!-- Icon Box Type 2-->
            <div class="unit unit-sm unit-sm-horizontal text-sm-left">
              <div class="unit-left"><span class="icon icon mdi mdi-airplane icon-mantis-filled icon-circle"></span></div>
              <div class="unit-body">
                <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Вы можете, например');?></h4>
                <p><?= Yii::t('app', 'поучаствовать в кругосветном круизе на шикарном лайнере, или сделать автопробег по странам Латинской Америки, или поучаствовать в дайвинг - туре на специально оборудованном корабле с подводной охотой и регулярными погружениями.');?></p>
              </div>
            </div>
          </div>
          <div class="cell-sm-8 cell-md-4 offset-top-66">
            <!-- Icon Box Type 2-->
            <div class="unit unit-sm unit-sm-horizontal text-sm-left">
              <div class="unit-left"><span class="icon icon mdi mdi-sunglasses icon-mantis-filled icon-circle"></span></div>
              <div class="unit-body">
                <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Или вам ближе');?></h4>
                <p><?= Yii::t('app', 'пассивный отдых в хорошем отеле или на вилле? У Вас всегда будет огромный выбор маршрутов и предложений. А когда Ваши друзья узнают, сколько Вы за такой роскошный отдых заплатили, они тоже захотят так жить и отдыхать.');?></p>
              </div>
            </div>
          </div>
          <div class="cell-sm-8 cell-md-4 offset-top-66">
            <!-- Icon Box Type 2-->
            <div class="unit unit-sm unit-sm-horizontal text-sm-left">
              <div class="unit-left"><span class="icon icon mdi mdi-crown icon-mantis-filled icon-circle"></span></div>
              <div class="unit-body">
                <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'В этом проекте');?></h4>
                <p><?= Yii::t('app', 'Вы не только осуществите свои самые смелые мечты и побываете там, где всегда Вам хотелось отдохнуть. Для Вас - уникальные предложения от Компании по отдыху в частных апартаментах на лучших мировых курортах.');?></p>
              </div>
            </div>
          </div>
          <div class="cell-sm-8 cell-md-4 offset-top-66">
            <!-- Icon Box Type 2-->
            <div class="unit unit-sm unit-sm-horizontal text-sm-left">
              <div class="unit-left"><span class="icon icon mdi mdi-emoticon-cool icon-mantis-filled icon-circle"></span></div>
              <div class="unit-body">
                <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Мы не будем тратить');?></h4>
                <p><?= Yii::t('app', 'свою жизнь на негатив, не будем  жить навязанными ложными идеалами и социальными установками. Мы хотим сами выбирать свой путь и жить так, как хочется, а не так, как получается.');?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="range range-xs-center offset-top-66">
          <div class="cell-lg-4">
            <div class="inset-lg-left-50 inset-lg-right-50"><a href="ui-kit.html" class="btn btn-primary btn-lg reveal-lg-block"><?= Yii::t('app', 'Стать участником');?></a></div>
          </div>
        </div>
      </div>
    </section>
    <!-- About-->
    <section class="section-98 section-md-110">
      <div class="shell">
        <div class="range text-lg-left">
          <div class="cell-sm-preffix-2 cell-sm-8 cell-lg-preffix-0 cell-lg-6">
            <div class="shadow-drop-xl">
              <!-- Media Elements-->
              <!-- RD Video-->
              <div data-rd-video-path="video/intense/intense" data-rd-video-title="What can we say about Intense ?" data-rd-video-muted="true" data-rd-video-preview="video/intense/intense-preview.jpg" data-rd-video-preload="false" class="rd-video-player play-on-scroll">
                <div class="rd-video-wrap embed-responsive-16by9">
                  <div class="rd-video-preloader"></div>
                  <video preload="metadata"></video>
                  <div class="rd-video-preview"></div>
                  <div class="rd-video-top-controls">
                    <!-- Title--><span class="rd-video-title"></span><a href="#" class="rd-video-fullscreen mdi mdi-fullscreen rd-video-icon"></a>
                  </div>
                  <div class="rd-video-controls">
                    <div class="rd-video-controls-buttons">
                      <!-- Play\Pause button--><a href="#" class="rd-video-play-pause mdi mdi-play"></a>
                      <!-- Progress bar-->
                    </div>
                    <div class="rd-video-progress-bar"></div>
                    <div class="rd-video-time"><span class="rd-video-current-time"></span> <span class="rd-video-time-divider">:</span>  <span class="rd-video-duration"></span></div>
                    <div class="rd-video-volume-wrap">
                      <!-- Volume button--><a href="#" class="rd-video-volume mdi mdi-volume-high rd-video-icon"></a>
                      <div class="rd-video-volume-bar-wrap">
                        <!-- Volume bar-->
                        <div class="rd-video-volume-bar"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="cell-sm-preffix-2 cell-sm-8 cell-lg-preffix-0 cell-lg-6 offset-top-30 offset-lg-top-0">
            <div class="inset-lg-left-50">
              <h1><?= Yii::t('app', 'О нас');?></h1>
              <hr class="divider divider-lg hr-lg-left-0 bg-mantis offset-top-0">
              <p class="offset-top-20 offset-lg-top-50">
                <?= Yii::t('app', 'Жизнь  так коротка и быстротечна, поэтому важно стремиться к самореализации и самовыражению, максимально  наполнить нашу жизнь яркими впечатлениями, позитивными эмоциями, познаниями всех  красот мира. Для этого и создан «Volcano Travel». Не проживайте жизнь как сторонний наблюдатель, не засиживайтесь дома, не упускайте моменты, не бойтесь  и не стесняйтесь жить ярко. Пробовать, ошибаться, стремиться, знакомиться и развиваться! Жизнь прекрасна и каждый наш партнер должен найти в ней  свое место!');?>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Skills-->
    <section>
      <!-- RD Parallax-->
      <div data-on="false" data-md-on="true" class="rd-parallax">
        <div data-speed="0.35" data-type="media" data-url="images/backgrounds/background-45-1920-950.jpg" class="rd-parallax-layer"></div>
        <div data-speed="0" data-type="html" class="rd-parallax-layer">
          <div class="shell section-98">
            <div class="range">
              <div class="cell-sm-8 cell-sm-preffix-2 cell-md-12 cell-md-preffix-0">
                <div class="range">
                  <div class="cell-sm-6 cell-md-3 cell-md-preffix-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div class="h2"><span data-step="3000" data-from="0" data-to="58249" class="big counter text-bold text-white"></span>
                        <hr class="divider bg-warning"/>
                      </div>
                      <h6 class="text-uppercase font-default text-white text-regular text-spacing-60 offset-top-20"><?= Yii::t('app', 'Циферка 1');?></h6>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-md-3 offset-top-66 offset-sm-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div class="h2"><span data-speed="2500" data-from="0" data-to="246" class="big counter text-bold text-white"></span><span class="big text-bold text-white"></span>
                        <hr class="divider bg-warning"/>
                      </div>
                      <h6 class="text-uppercase font-default text-white text-regular text-spacing-60 offset-top-20"><?= Yii::t('app', 'Циферка 2');?></h6>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-md-3 offset-top-66 offset-md-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div class="h2"><span data-step="1500" data-from="0" data-to="1200" class="big counter text-bold text-white"></span>
                        <hr class="divider bg-warning"/>
                      </div>
                      <h6 class="text-uppercase font-default text-white text-regular text-spacing-60 offset-top-20"><?= Yii::t('app', 'Циферка 3');?></h6>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-md-3 offset-top-66 offset-md-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div class="h2"><span data-speed="1300" data-from="0" data-to="834" class="big counter text-bold text-white"></span><span class="big text-bold text-white"></span>
                        <hr class="divider bg-warning"/>
                      </div>
                      <h6 class="text-uppercase font-default text-white text-regular text-spacing-60 offset-top-20"><?= Yii::t('app', 'Циферка 4');?></h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Meet Our Team-->
    <section class="section-66 section-md-top-110 section-md-bottom-0 bg-white-lilac section-team-custom-effects section-custom-effects">
      <div class="shell section-relative">
        <h1><?= Yii::t('app', 'Volcano Travel');?></h1>
        <div class="offset-top-0">
          <hr class="divider divider-lg bg-mantis">
        </div>
        <div class="range range-xs-center">
          <div class="cell-lg-8">
            <p><?= Yii::t('app', 'Мы не будем тратить  свою жизнь на негатив, не будем  жить навязанными ложными идеалами и социальными установками. Мы хотим сами выбирать свой путь и жить так, как хочется, а не так, как получается.');?></p>
          </div>
        </div>
        <!-- Simple quote Slider-->
        <div data-items="1" data-nav="false" data-dots="false" data-nav-custom=".owl-custom-navigation" class="owl-carousel owl-carousel-classic owl-carousel-class-light offset-top-41 offset-md-top-0">
          <div>
            <div class="range range-md-reverse range-xs-center range-xs-middle text-md-left">
              <div class="cell-sm-10 cell-md-6">
                <div>
                  <h2 class="text-primary font-default"><?= Yii::t('app', '«Volcano Travel»');?></h2>
                </div>
                <div class="offset-top-4">
                  <h6 class="font-default text-regular"><?= Yii::t('app', '');?></h6>
                </div>
                <div class="offset-top-30">
                  <p><?= Yii::t('app', '- это не только любовь к миру и жизни, а наше сообщество - не простой клуб путешественников.');?></p>
                </div>
                <blockquote class="quote quote-variant-2 offset-top-20">
                  <div class="h3 text-italic font-default">
                    <div class="inset-md-left-30 inset-lg-left-0">
                      <q><?= Yii::t('app', 'Слишком много хорошего – это прекрасно!');?></q>
                    </div>
                  </div>
                </blockquote>
              </div>
              <div class="cell-md-6"><img src="images/users/user-john-doe-598x624.png" width="598" height="624" alt="" class="img-responsive veil reveal-md-inline-block"></div>
            </div>
          </div>
          <div>
            <div class="range range-md-reverse range-xs-center range-xs-middle text-md-left">
              <div class="cell-md-6">
                <div>
                  <h2 class="text-primary font-default"><?= Yii::t('app', '«Volcano Travel»');?></h2>
                </div>
                <div class="offset-top-4">
                  <h6 class="font-default text-regular"><?= Yii::t('app', '');?></h6>
                </div>
                <div class="offset-top-30">
                  <p><?= Yii::t('app', '- это философия позитивного мировоззрения и активной жизни.');?></p>
                </div>
                <blockquote class="quote quote-variant-2 offset-top-20">
                  <div class="h3 text-italic font-default">
                    <div class="inset-md-left-30 inset-lg-left-0">
                      <q><?= Yii::t('app', 'Каждое свершение начинается с решения попытаться.');?></q>
                    </div>
                  </div>
                </blockquote>
              </div>
              <div class="cell-md-6"><img src="images/users/user-paul-howell-598x624.png" width="598" height="624" alt="" class="img-responsive veil reveal-md-inline-block"></div>
            </div>
          </div>
          <div>
            <div class="range range-md-reverse range-xs-center range-xs-middle text-md-left">
              <div class="cell-md-6">
                <div>
                  <h2 class="text-primary font-default"><?= Yii::t('app', '«Volcano Travel»');?></h2>
                </div>
                <div class="offset-top-4">
                  <h6 class="font-default text-regular"><?= Yii::t('app', '');?></h6>
                </div>
                <div class="offset-top-30">
                  <p><?= Yii::t('app', '- это объединение активных, любознательных, открытых и просто хороших людей, настоящих личностей.');?>.</p>
                </div>
                <blockquote class="quote quote-variant-2 offset-top-20">
                  <div class="h3 text-italic font-default">
                    <div class="inset-md-left-30 inset-lg-left-0">
                      <q><?= Yii::t('app', 'Жизнь приобретает смысл, когда Вы мотивированы, ставите цели и непрерывно достигаете их.');?></q>
                    </div>
                  </div>
                </blockquote>
              </div>
              <div class="cell-md-6"><img src="images/users/user-gregory-tucker-598x624.png" width="598" height="624" alt="" class="img-responsive veil reveal-md-inline-block"></div>
            </div>
          </div>
        </div>
        <div class="owl-custom-navigation owl-customer-navigation text-md-left">
          <div class="owl-nav">
            <div data-owl-prev class="owl-prev mdi mdi-chevron-left"></div>
            <div data-owl-next class="owl-next mdi mdi-chevron-right"></div>
          </div>
        </div>
      </div>
    </section>
    <!-- Testimonials-->
    <section class="section-98 section-sm-110">
      <div class="shell">
        <h1><?= Yii::t('app', 'Отзывы');?></h1>
        <hr class="divider divider-lg bg-mantis">
        <div class="range range-xs-center offset-top-41 offset-md-top-66">
          <div class="cell-sm-8 cell-md-4">
            <!-- Boxed Testimonials v2-->
            <blockquote class="quote quote-classic quote-classic-boxed-2">
              <div class="quote-meta"><img width="80" height="80" src="images/users/user-steven-carpenter-80x80.jpg" alt="" class="img-circle quote-img"/></div>
              <div class="quote-body">
                <p>
                  <q><?= Yii::t('app', 'В этом проекте я заработал 15 000 долларов, смог приобрести себе новую машину и съездить на отдых с семьей и это только за месяц моей работы в этом проекте, для меня это только начало');?></q>
                </p>
                <div class="quote-author h6 font-default text-primary">
                  <cite class="text-normal"><?= Yii::t('app', 'Виктор Михайлович');?></cite>
                </div>
                <p class="quote-desc font-default text-italic"><?= Yii::t('app', 'Участник проекта');?></p>
              </div>
            </blockquote>
          </div>
          <div class="cell-sm-8 cell-md-4 offset-top-41 offset-md-top-0">
            <!-- Boxed Testimonials v2-->
            <blockquote class="quote quote-classic quote-classic-boxed-2">
              <div class="quote-meta"><img width="80" height="80" src="images/users/user-carol-harper-80x80.jpg" alt="" class="img-circle quote-img"/></div>
              <div class="quote-body">
                <p>
                  <q><?= Yii::t('app', 'С момента начала участия в этом проекте я заработала 12 345 долларов, эти деньги пошли на улучшение моих жилишных условий, я довольна');?></q>
                </p>
                <div class="quote-author h6 font-default text-primary">
                  <cite class="text-normal"><?= Yii::t('app', 'Светлана Ким');?></cite>
                </div>
                <p class="quote-desc font-default text-italic"><?= Yii::t('app', 'Участник проекта');?></p>
              </div>
            </blockquote>
          </div>
          <div class="cell-sm-8 cell-md-4 offset-top-41 offset-md-top-0">
            <!-- Boxed Testimonials v2-->
            <blockquote class="quote quote-classic quote-classic-boxed-2">
              <div class="quote-meta"><img width="80" height="80" src="images/users/user-andrew-wilson-80x80.jpg" alt="" class="img-circle quote-img"/></div>
              <div class="quote-body">
                <p>
                  <q><?= Yii::t('app', 'Даже и не думал раньше, что заработать деньги в интернете так легко, каждый день наблюдаю как пополняется мой личный счёт, это доставляет мне огромное удовольствие');?></q>
                </p>
                <div class="quote-author h6 font-default text-primary">
                  <cite class="text-normal"><?= Yii::t('app', 'Евгений Романов');?></cite>
                </div>
                <p class="quote-desc font-default text-italic"><?= Yii::t('app', 'Участник проекта');?></p>
              </div>
            </blockquote>
          </div>
        </div>
      </div>
    </section>
    <!-- Our Partners-->
    <section class="context-dark bg-gray-base">
      <!-- RD Parallax-->
      <div data-on="false" data-md-on="true" class="rd-parallax">
        <div data-speed="0.35" data-type="media" data-url="images/backgrounds/background-46-1920-950.jpg" class="rd-parallax-layer"></div>
        <div data-speed="0" data-type="html" class="rd-parallax-layer">
          <div class="shell section-98 section-sm-110">
            <div>
              <h2><span class="big"><?= Yii::t('app', '«Volcano Travel» - доступен и открыт для всех желающих без исключений.');?></span></h2>
            </div>
            <hr class="divider divider-lg bg-warning">
            <div class="range range-xs-center offset-top-24">
              <div class="cell-xs-10 cell-lg-8">
                <p><?= Yii::t('app', 'Доброжелательная улыбка и сияющие глаза -  это визитная карточка партнера «Volcano Travel», помните об этом и перед Вами раскроются многие тайны и появятся новые  возможности, о которых Вы даже и не мечтали. Мы ценим искренность, честность и порядочность');?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>