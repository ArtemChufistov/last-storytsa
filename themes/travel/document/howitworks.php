<?php
use yii\web\View;
use yii\helpers\Url;

$this->title = $model->title;

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js',  ['position' => yii\web\View::POS_END]);
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />

<section class="breadcrumb-classic headmenusection">
  <div class="shell section-34 section-sm-50">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="mdi mdi-math-compass icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h3><span class="big" style = "color: white;"><?= Yii::t('app', 'Как это работает');?></span></h3>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
        <ul class="list-inline list-inline-dashed p">
          <li><a href="/"><?= Yii::t('app', 'Главная');?></a></li>
          <li><?= Yii::t('app', 'Как это работает');?>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
  </svg>
</section>

<!-- Page Content-->
<main class="page-content">
  <!-- What we do-->
  <section class="section-98 section-sm-110">
    <div class="shell">
      <h1><?= Yii::t('app', 'Как это работает');?></h1>
      <hr class="divider bg-mantis">
      <p class="inset-left-11p inset-right-11p"><?= Yii::t('app', 'Все очень просто. Пройдите маркетинговый путь и получите возможность путешествовать в любые уголки планеты');?></p>
      <div class="range offset-top-66">
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0">
                  <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-account-circle"></span>
                  <div>
                    <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Шаг 1');?></h4>
                    <p>
                      <?= Yii::t('app', 'Пройдите простую регистрацию и станьте участником клуба «Volcano Travel»');?>
                      
                    </p>
                  </div>
        </div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                  <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-cash-multiple"></span>
                  <div>
                    <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Шаг 2');?></h4>
                    <p>
                      <?= Yii::t('app', 'Пополните ваш внутренний баланс в разделе финансов на сумму 0.5 биткоинов');?>
                    </p>
                  </div>
        </div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                  <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-briefcase-check"></span>
                  <div>
                    <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Шаг 3');?></h4>
                    <p>
                      <?= Yii::t('app', 'Оплатите участие, станте участником программы и начните свою карьеру в проекте');?>
                    </p>
                  </div>
        </div>
        <div class="range-spacer veil reveal-md-inline-block offset-top-41 offset-md-top-66"></div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                  <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-share-variant"></span>
                  <div>
                    <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Шаг 4');?></h4>
                    <p>
                      <?= Yii::t('app', 'Рекомендуйте сайт друзьям и првлекайте новых партнеров');?>
                    </p>
                  </div>
        </div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                  <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-trending-up"></span>
                  <div>
                    <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Шаг 5');?></h4>
                    <p>
                      <?= Yii::t('app', 'Начните получать поэтапную прибыль с маркетинга проекта');?>
                    </p>
                  </div>
        </div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                  <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-professional-hexagon"></span>
                  <div>
                    <h4 class="text-bold offset-top-20"><?= Yii::t('app', 'Шаг 6');?></h4>
                    <p>
                      <?= Yii::t('app', 'Пройдите квалификационные этапы и получите клубную карту, а вместе с ней и 10.5 биткоинов');?>
                    </p>
                  </div>
        </div>
      </div>
      <hr class="bg-lightest offset-top-66">
      <div class="range range-xs-middle text-md-left">
        <div class="cell-md-6"><img src="/images/moqups/moqup-home-both.jpg" width="570" height="368" alt="" class="img-responsive center-block"></div>
        <div class="cell-md-6">
          <h1><?= Yii::t('app', 'Видеопрезентация');?></h1>
          <hr class="divider bg-mantis hr-md-left-0">
          <p><?= Yii::t('app', '');?></p>
          <p><?= Yii::t('app', 'Посмотрете маркетинг, чтобы понять как все работает, и как вы сможете заработать 10.5 биткоинов + клубную карту «Volcano Travel» для путешествий по всему миру.');?></p><a href="<?php echo Url::to(['/profile/profile/signup']);?>" class="btn btn-primary offset-top-14"><?= Yii::t('app', 'Присоединиться');?></a>
        </div>
      </div>
    </div>
  </section>
  <section class="shadow-drop-ambient">
    <!-- RD Parallax-->
    <div data-on="false" data-md-on="true" class="rd-parallax">
      <div data-speed="0.35" data-type="media" data-url="images/blog-single-post-classic-03.jpg" class="rd-parallax-layer"></div>
      <div data-speed="0" data-type="html" class="rd-parallax-layer">
        <div class="bg-overlay-white">
          <div class="shell section-66">
            <div class="range range-xs-middle range-xs-center offset-top-20">
              <div class="cell-lg-4 text-lg-left">
              <h3><?= Yii::t('app', 'Фотопрезентация');?></h3>
                </br>
                </br>
                <p><?= Yii::t('app', 'Посмотрите маркетинг в схемах и картинках, для того чтобы понять как вы сможете заработать 10.5 биткоинов + клубную карту «Volcano Travel» для путешествий по всему миру.');?></p>
                <!-- Custom Pagination-->
              </div>
              <div class="cell-lg-8 offset-top-41 offset-lg-top-0">
                <a data-fancybox="gallery" href="/images/marketing_slide.png"><img src = "/images/marketing_slide.png" style = "height: 300px;"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
            <!-- Call to action type 2-->
            <section class="section-66 bg-blue-gray context-dark">
              <div class="shell">
                <div class="range range-xs-middle range-condensed">
                  <div class="cell-md-8 cell-lg-9 text-center text-md-left">
                    <h2><span class="big"><?= Yii::t('app', 'Начните жить красиво!');?></span></h2>
                  </div>
                  <div class="cell-md-4 cell-lg-3 offset-top-41 offset-md-top-0">
                    <a href="<?php echo Url::to(['/profile/profile/signup']);?>" class="btn btn-icon btn-lg btn-default btn-anis-effect btn-icon-left">
                      <?= Yii::t('app', 'Присоединиться');?>
                    </a>
                  </div>
                </div>
              </div>
            </section>
  </section>
</main>