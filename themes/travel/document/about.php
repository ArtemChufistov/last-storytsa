<?php
use yii\web\View;
use yii\helpers\Url;

$this->title = $model->title;
?>

<section class="breadcrumb-classic headmenusection">
  <div class="shell section-34 section-sm-50">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="mdi mdi-airplay icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h3><span class="big" style = "color: white;"><?= Yii::t('app', 'О Нас');?></span></h3>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
        <ul class="list-inline list-inline-dashed p">
          <li><a href="/"><?= Yii::t('app', 'Главная');?></a></li>
          <li><?= Yii::t('app', 'О Нас');?>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
  </svg>
</section>

<main class="page-content">
  <!-- section join our team-->
  <section class="section-124">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-xs-8">
          <h1><?= Yii::t('app', 'О Нас');?></h1>
          <div>
            <hr class="divider bg-mantis">
          </div>
          <div class="offset-top-20">
            <p><?= Yii::t('app', 'Жизнь  так коротка и быстротечна, поэтому важно стремиться к самореализации и самовыражению, максимально  наполнить нашу жизнь яркими впечатлениями, позитивными эмоциями, познаниями всех  красот мира. Для этого и создан «Volcano Travel». ');?></p>
          </div>
          <div class="offset-top-34"><a href="<?php echo Url::to(['/profile/profile/signup']);?>" data-custom-scroll-to="careers-start" class="btn btn-primary btn-icon btn-icon-left">
            <span class="icon mdi mdi-check-all"></span><?= Yii::t('app', 'Присоединиться');?></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- section reasons to get-->
  <section class="section-66 section-top-50 bg-mantis section-triangle section-triangle-bottom context-dark">
    <div class="shell">
      <div class="range range-sm-center">
        <h2><span class="big"><?= Yii::t('app', 'Дружба, Порядочность, Честность – это ключевые ценности клуба «Volcano Travel»');?></span></h2>
        <div class="cell-md-8">
          <p><?= Yii::t('app', 'Доброжелательная улыбка и сияющие глаза -  это визитная карточка партнера «Volcano Travel», помните об этом и перед Вами раскроются многие тайны и появятся новые  возможности, о которых Вы даже и не мечтали. Мы ценим искренность, честность и порядочность.');?></p>
        </div>
      </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
      <defs>
        <lineargradient id="grad2" x1="0%" y1="0%" x2="100%" y2="0%">
          <stop offset="0%" style="stop-color:rgb(99,189,98);stop-opacity:1"></stop>
          <stop offset="100%" style="stop-color:rgb(99,189,98);stop-opacity:1"></stop>
        </lineargradient>
      </defs>
      <polyline points="0,0 60,0 29,29" fill="url(#grad2)"></polyline>
    </svg>
  </section>
  <section class="section-66 section-sm-0">
    <div class="shell">
      <div class="range range-xs-center range-sm-left">
        <div class="cell-xs-10 cell-sm-6 section-image-aside section-image-aside-right text-left">
          <div style="background-image: url(images/pages/careers-01-960x540.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
          <div class="section-image-aside-body section-sm-66 inset-sm-right-30">
            <div>
              <h3 class="text-picton-blue">01</h3>
            </div>
            <div class="offset-top-10">
              <h2> <?= Yii::t('app', '«Любовь к Миру»');?></h2>
            </div>
            <div class="offset-top-20">
              <p><?= Yii::t('app', '«Volcano Travel» – это не только любовь к миру и жизни, а наше сообщество не простой клуб путешественников.');?></p>
            </div>
          </div>
        </div>
      </div>
      <div class="range range-xs-center range-sm-right offset-top-0">
        <div class="cell-xs-10 cell-sm-6 section-image-aside section-image-aside-left text-left">
          <div style="background-image: url(images/pages/careers-02-960x540.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
          <div class="section-image-aside-body offset-top-41 offset-sm-top-0 section-sm-66 inset-sm-left-50">
            <div>
              <h3 class="text-picton-blue">02</h3>
            </div>
            <div class="offset-top-10">
              <h2> <?= Yii::t('app', 'Позитивное мировозрение');?></h2>
            </div>
            <div class="offset-top-20">
              <p><?= Yii::t('app', '«Volcano Travel» - это философия позитивного мировоззрения и активной жизни.');?></p>
            </div>
          </div>
        </div>
      </div>
      <div class="range range-xs-center range-sm-left offset-top-0">
        <div class="cell-xs-10 cell-sm-6 section-image-aside section-image-aside-right text-left">
          <div style="background-image: url(images/pages/careers-03-960x540.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
          <div class="section-image-aside-body offset-top-41 offset-sm-top-0 section-sm-66 inset-sm-right-30">
            <div>
              <h3 class="text-picton-blue">03</h3>
            </div>
            <div class="offset-top-10">
              <h2> <?= Yii::t('app', 'Нет границ');?></h2>
            </div>
            <div class="offset-top-20">
              <p><?= Yii::t('app', ' «Volcano Travel» - доступен и открыт для всех желающих без исключений.');?></p>
            </div>
          </div>
        </div>
      </div>
      <div class="range range-xs-center range-sm-right offset-top-0">
        <div class="cell-xs-10 cell-sm-6 section-image-aside section-image-aside-left text-left">
          <div style="background-image: url(images/pages/careers-04-960x540.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
          <div class="section-image-aside-body offset-top-41 offset-sm-top-0 section-sm-66 inset-sm-left-50">
            <div>
              <h3 class="text-picton-blue">04</h3>
            </div>
            <div class="offset-top-10">
              <h2> <?= Yii::t('app', 'Хорошие люди');?></h2>
            </div>
            <div class="offset-top-20">
              <p><?= Yii::t('app', '«Volcano Travel» - это объединение активных, любознательных, открытых и просто хороших людей, настоящих личностей.
');?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- section career start here-->
  <section id="careers-start" class="section-66 section-top-50 bg-blue-gray section-triangle section-triangle-bottom context-dark">
    <div class="shell">
      <h2><span class="big"><?= Yii::t('app', 'Начните жить страстно и красиво!');?></span></h2>
      <p><?= Yii::t('app', 'Становитесь участником «Volcano Travel» и Вас ожидают много приключений и невероятных открытий. Начните жить легко и непринужденно. Танцуйте свою жизнь, и у Вас все получится!');?></p>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
      <defs>
        <lineargradient id="grad3" x1="0%" y1="0%" x2="100%" y2="0%">
          <stop offset="0%" style="stop-color:rgb(109,146,204);stop-opacity:1"></stop>
          <stop offset="100%" style="stop-color:rgb(109,146,204);stop-opacity:1"></stop>
        </lineargradient>
      </defs>
      <polyline points="0,0 60,0 29,29" fill="url(#grad3)"></polyline>
    </svg>
  </section>
</main>