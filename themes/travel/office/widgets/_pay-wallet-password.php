<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="box box-warning">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-key" aria-hidden="true"></i><?php echo Yii::t('app', 'Платёжный пароль');?></h3>
  </div>
  <div class="box-body form-group text-trancpancy">
  		<div class = "col-md-12">
	    <?php echo Yii::t('app', '<p>Для добавления/изменения кошелька BitCoin Вам необходимо ввести платёжный пароль.</p> <p>Если вы ранее запрашивали платёжный пароль, то он отправлялся Вам на почту. </p><p>Если Вы не смогли найти свой платёжный пароль или не запрашивали его, то можете сделать это заного</p>');?>

        <?php $form = ActiveForm::begin(); ?>
      		<?= Html::submitButton('<i class="glyphicon glyphicon-send"></i> '.Yii::t('app', 'Получить платёжный пароль'), [
          'class' => 'btn btn-warning pull-left',
          'value' => 1,
          'name' => 'send-pay-wallet']) ?>
        <?php ActiveForm::end();?>
        </div>
        <?php if (!empty($message)):?>
        	</br>
        	<strong class="help-block col-md-12"><?php echo $message;?></strong>
        <?php endif;?>
  </div>
</div>