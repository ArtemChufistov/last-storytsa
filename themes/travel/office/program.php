<?php

use app\modules\profile\widgets\UserTreeElementWidget;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Принять участие в программе');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<?php if(Yii::$app->session->getFlash('message')): ?>
  <div class="modalMessage modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('message');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalMessage').modal('show');
", View::POS_END);?>

<?php endif;?>

<div class="row">
	<div class="col-md-8">

		<div class="box box-danger">
			<div class="box-header with-border">
			  <h3 class="box-title"><i class="fa fa-fire" aria-hidden="true"></i></i><?php echo Yii::t('app', 'Информация');?></h3>
			</div>
			<div class="box-body gold-background">
				<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_payment-buy-place', ['user' => $user, 'matrixCategory' => $matrixCategory, 'rootMatrixes' => $rootMatrixes]);?>
			</div>
		</div>

		<div class="nav-tabs-custom">
		    <!-- Tabs within a box -->
		    <ul class="nav nav-tabs pull-right">
		      <?php foreach(array_reverse($rootMatrixes) as $num => $rootMatrixItem):?>
		      	<li <?php if ($rootMatrix->slug == $rootMatrixItem->slug):?>class="active"<?php endif;?>>
		      		<a href="<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrixItem->slug]);?>">
		      			<?php echo $rootMatrixItem->name;?>
		      		</a>
		      	</li>
		      <?php endforeach;?>

		      <li class="pull-left header"><i class="fa fa-users" aria-hidden="true"></i></li>
		    </ul>
		    <div class="tab-content no-padding">
		      <?php foreach(array_reverse($rootMatrixes) as $num => $rootMatrixItem):?>
		      	<div class="chart tab-pane <?php if ($rootMatrixItem->slug == $rootMatrix->slug):?>active<?php endif;?>" id="<?php echo $rootMatrix->slug;?>">
					<?php if(!empty($currentPlace)):?>
						<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_matrix', [
							'matrixCategory' => $matrixCategory,
							'currentPlace' => $currentPlace,
							'currentLevel' => $currentLevel,
							'parentPlace' => $parentPlace,
							'rootMatrix' => $rootMatrix,
							'user' => $user,
						]);?>
					<?php else:?>
						<h4 style = "text-align:center;margin-top:30px; margin-bottom: 30px;"><?php echo Yii::t('app', 'У вас еще нет места в программе');?></h4>
					<?php endif;?>
		      	</div>
		      <?php endforeach;?>
		    </div>
		</div>
	</div>
	<div class = "col-md-4">
		<?php if (!empty($parentUser)):?>
			<div class="box box-widget widget-user">
				<div class="widget-user-header bg-aqua-active">
					<h3 class="widget-user-username"><?php echo Yii::t('app', 'Ваш пригласитель')?></h3>
					<h5 class="widget-user-desc">
						<?php if (empty($parentUser->first_name) || empty($parentUser->last_name)):?>
							<?php echo $parentUser->login;?>
						<?php else:?>
							<?php echo $parentUser->first_name;?> <?php echo $parentUser->last_name;?>
						<?php endif;?>
					</h5>
				</div>
				<div class="widget-user-image">
					<img class="img-circle" src="<?php echo $parentUser->getImage();?>" alt="User Avatar">
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-sm-4 border-right">
							<div class="description-block">
								<h5 class="description-header"><?php echo $parentUser->email;?></h5>
								<span class="description-text">E-mail</span>
							</div>
						</div>
					<div class="col-sm-4 border-right">
						<div class="description-block">
							<h5 class="description-header"><?php echo $parentUser->skype;?></h5>
							<span class="description-text">Skype</span>
						</div>
					</div>
						<div class="col-sm-4">
							<div class="description-block">
								<h5 class="description-header"><?php echo $parentUser->phone;?></h5>
								<span class="description-text"><?php echo Yii::t('app', 'Телефон')?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif;?>

		<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_matrix-places', [
			'matrixCategory' => $matrixCategory,
			'rootMatrixes' => $rootMatrixes,
			'user' => $user,
		]);?>

	</div>
</div>