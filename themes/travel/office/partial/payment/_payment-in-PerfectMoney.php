<?php
use app\modules\mainpage\models\Preference;
use app\modules\payment\models\Currency;
use yii\helpers\Url;

$preferenceSecret = Preference::find()->where(['key' => Preference::KEY_PERF_MON_SECRET])->one();

if ($paymentForm->getCurrency()->one()->key == Currency::KEY_USD){
    $preferencePayeeAcc = Preference::find()->where(['key' => Preference::KEY_PERF_MON_PAYEE_ACCOUNT_USD])->one();
}elseif ($paymentForm->getCurrency()->one()->key == Currency::KEY_EUR){
    $preferencePayeeAcc = Preference::find()->where(['key' => Preference::KEY_PERF_MON_PAYEE_ACCOUNT_EUR])->one();
}

$preferencePayeeName = Preference::find()->where(['key' => Preference::KEY_PERF_MON_PAYEE_NAME])->one();
$preferenceStatusUrl = Preference::find()->where(['key' => Preference::KEY_PERF_MON_STATUS_URL])->one();
$preferencePaymentUrl = Preference::find()->where(['key' => Preference::KEY_PERF_MON_PAYMENT_URL])->one();

if (empty($preferencePaymentUrl) || $preferencePaymentUrl->value == 0){
    $paymentUrl = rtrim(Url::home(true), '/\\') . Url::to(['/profile/office/finance']);
}else{
    $paymentUrl = $preferencePaymentUrl->value;
}

$preferenceNoPaymentUrl = Preference::find()->where(['key' => Preference::KEY_PERF_MON_NO_PAYMENT_URL])->one();

if (empty($preferenceNoPaymentUrl) || $preferenceNoPaymentUrl->value == 0){
    $noPaymentUrl = rtrim(Url::home(true), '/\\') . Url::to(['/profile/office/finance']);
}else{
    $noPaymentUrl = $preferenceNoPaymentUrl->value;
}
?>

<form style = "display: none;" id = "toPm" action='https://perfectmoney.is/api/step1.asp' method='POST'>
    <input type='hidden' name='PAYEE_ACCOUNT' value='<?php echo $preferencePayeeAcc->value; ?>'/>
    <input type='hidden' name='PAYEE_NAME' value='<?php echo $preferencePayeeName->value; ?>'/>
    <input type='hidden' name='PAYMENT_ID' value='<?php echo $paymentForm->id; ?>'/>
    <input type='hidden' name='PAYMENT_AMOUNT' value='<?php echo $paymentForm->sum; ?>'/>
    <input type='hidden' name='PAYMENT_UNITS' value='<?php echo $paymentForm->getCurrency()->one()->key;?>'/>
    <input type='hidden' name='STATUS_URL' value='<?php echo $preferenceStatusUrl->value; ?>'/>
    <input type='hidden' name='PAYMENT_URL' value='<?php echo $paymentUrl; ?>'/>
    <input type='hidden' name='PAYMENT_URL_METHOD' value='LINK'/>
    <input type='hidden' name='NOPAYMENT_URL' value='<?php echo $noPaymentUrl; ?>'/>
    <input type='hidden' name='NOPAYMENT_URL_METHOD' value='LINK'/>
    <input type='hidden' name='SUGGESTED_MEMO' value='<?php echo Yii::t('app', 'Пополнение баланса на сумму {sum} {currency} . Оплата № {hash} Участник {login}', [
        'currency' => $paymentForm->getCurrency()->one()->key,
        'login' => $paymentForm->getFromUser()->one()->login,
        'hash' => $paymentForm->hash,
        'sum' => $paymentForm->sum,
    ]);?>'/>
</form>
<script type="text/javascript">
<?php
$this->registerJs("
	$('#toPm').submit();
");?>
</script>