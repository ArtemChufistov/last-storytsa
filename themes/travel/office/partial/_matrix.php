<?php
use app\modules\profile\widgets\UserTreeElementWidget;
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\matrix\models\MatrixUserSettingForm;
use app\modules\matrix\models\MatrixUserSetting;
use app\modules\matrix\models\MatrixPref;
use app\modules\matrix\models\Matrix;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;


$childs = $currentPlace->children()->orderBy(['sort' => 'asc'])->all();

foreach($childs as $childItem){

    if ($childItem->sort == 0){
        $child1  = $childItem;
        $childs1 = $child1->children()->orderBy(['sort' => 'asc'])->all();
        foreach($childs1 as $childItem1){
            if ($childItem1->sort == 0){
                $child11 = $childItem1;
            }elseif($childItem1->sort == 1){
                $child12 = $childItem1;
            }
        }
    }elseif($childItem->sort == 1){
        $child2  = $childItem;
        $childs2 = $child2->children()->orderBy(['sort' => 'asc'])->all();
        foreach($childs2 as $childItem2){
            if ($childItem2->sort == 0){
                $child21 = $childItem2;
            }elseif($childItem2->sort == 1){
                $child22 = $childItem2;
            }
        }
    }
}

$depth = MatrixPref::find()->where(['key' => MatrixPref::KEY_DEPTH, 'matrix_id' => $rootMatrix->id])->one();

$countChildren = MatrixPref::find()->where(['key' => MatrixPref::KEY_COUNT_CHILDREN, 'matrix_id' => $rootMatrix->id])->one();
?>
<section class="management-hierarchy">

    <div class="hv-container">

        <div class="hv-wrapper treeViewWraper simpleView">

            <ul class="timeline">

                <?php foreach($currentPlace->path()->having('depth <=' . $currentLevel)->all() as $placeItem):?>
                <li>
                    <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $placeItem->slug, 'parentPlaceSlug' => $parentPlace->slug]);?>">
                        <img class="fa" src = "<?php echo $placeItem->getUser()->one()->getImage();?>">
                    </a>
                        <div class="timeline-item"></div>
                </li>
                <?php endforeach;?>
            </ul>

            <div class="hv-item">
                <div class="hv-item-parent parent1">
                    <?php echo UserTreeElementWidget::widget([
                        'matrixCategory' => $matrixCategory,
                        'currentPlace' => $currentPlace,
                        'parentPlace' => $parentPlace,
                        'currentUser' => $user,
                        'rootMatrix' => $rootMatrix,
                        ]);?>
                </div>

                <div class="hv-item-children">

                    <div class="hv-item-child">
                        <!-- Key component -->
                        <div class="hv-item">

                            <div class="hv-item-parent <?php if(!empty($depth) && $depth->value == 2):?>parent11<?php endif;?>">
                                <?php echo UserTreeElementWidget::widget([
                                    'matrixCategory' => $matrixCategory,
                                    'ancestorPlace' => $currentPlace,
                                    'currentPlace' => empty($child1) ? '' : $child1,
                                    'parentPlace' => $parentPlace,
                                    'currentUser' => $user,
                                    'rootMatrix' => $rootMatrix,
                                    'sort' => 0,
                                ]);?>
                            </div>

                            <?php if(!empty($depth) && $depth->value == 2):?>
                                <div class="hv-item-children">
                                    <div class="hv-item-child child1">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child1,
                                            'currentPlace' => $child11,
                                            'parentPlace' => $parentPlace,
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 0,
                                        ]);?>
                                    </div>

                                    <div class="hv-item-child child2">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child1,
                                            'currentPlace' => $child12,
                                            'parentPlace' => $parentPlace,
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 1,
                                        ]);?>
                                    </div>

                                </div>
                            <?php endif;?>
                        </div>
                    </div>

                    <div class="hv-item-child">
                        <!-- Key component -->
                        <div class="hv-item">

                            <div class="hv-item-parent <?php if(!empty($depth) && $depth->value == 2):?>parent12<?php endif;?>">
                                <?php echo UserTreeElementWidget::widget([
                                    'matrixCategory' => $matrixCategory,
                                    'ancestorPlace' => $currentPlace,
                                    'currentPlace' => empty($child2) ? '' : $child2,
                                    'parentPlace' => $parentPlace, 
                                    'currentUser' => $user,
                                    'rootMatrix' => $rootMatrix,
                                    'sort' => 1,
                                ]);?>
                            </div>

                            <?php if(!empty($depth) && $depth->value == 2):?>
                                <div class="hv-item-children">

                                    <div class="hv-item-child child3">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child2,
                                            'currentPlace' => $child21, 
                                            'parentPlace' => $parentPlace, 
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 0,
                                        ]);?>
                                    </div>


                                    <div class="hv-item-child child4">
                                        <?php echo UserTreeElementWidget::widget([
                                            'matrixCategory' => $matrixCategory,
                                            'ancestorPlace' => $child2,
                                            'currentPlace' => $child22, 
                                            'parentPlace' => $parentPlace, 
                                            'currentUser' => $user,
                                            'rootMatrix' => $rootMatrix,
                                            'sort' => 1,
                                        ]);?>
                                    </div>

                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="hv-wrapper treeViewWraper detailView" style = "display: none;">
            <div class="hv-item">

                <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_detail-struct', [
                    'user' => $user,
                    'currentPlace' => $currentPlace,
                    'countChildren'=> $countChildren
                ]);?>

            </div>
        </div>
    </div>
</section>

<?php $this->registerJs("

$(document).ready(function()
{
    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };
    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);
    // activate Nestable for list 2
    $('#nestable2').nestable({
        group: 1
    })
    .on('change', updateOutput);
    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));
    updateOutput($('#nestable2').data('output', $('#nestable2-output')));
    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });
    $('#nestable3').nestable();
});

$('.viewButton').on('click', function(){
    $('.viewButton').show();
    $(this).hide();
    
    $('.treeViewWraper').hide();
    $('.' + $(this).attr('hdWrapper')).show();

    console.log($(this).attr('hdWrapper'));
})
$('.checkBoxPlace').on('ifChecked', function(event){
  $(this).closest('form').submit();
});
$('.checkBoxPlace').on('ifUnchecked', function(event){
  $(this).closest('form').submit();
});
$('.imgAvatar').one('click', function(){
        location.href = $(this).attr('href');
})
")?>