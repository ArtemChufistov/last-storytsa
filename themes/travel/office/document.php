
<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\payment\models\Transaction;
use app\modules\payment\models\Payment;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = $model->title;
$this->params['breadcrumbs'][] = $model->title;
$assets = UserAsset::register($this);
?>

<div class="row">
  <div class="col-md-12">

    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true"></i><?php echo $model->title;?></h3>
      </div>
      <div class="box-body">
		<?php echo $model->content;?>
      </div>
    </div>
  </div>
</div>