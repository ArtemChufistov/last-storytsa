<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use yii\grid\GridView;
use moonland\tinymce\TinyMCE;

$this->title = Yii::t('user', 'Оставить отзыв');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<?php $form = ActiveForm::begin([
  'id' => 'form-profile',
  'options' => [
      'class'=>'form',
      'enctype'=>'multipart/form-data'
  ],
  ]); ?>

  <div class="row">

    <div class="col-md-6">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo Yii::t('app', 'Добавить отзыв');?></h3>
        </div>
        <div class="box-body">
          <?= $form->field($response, 'name')->textInput([
              'maxlength' => true,
              'placeholder' => $response->getAttributeLabel('name')
          ]) ?>

          <?= $form->field($response, 'text')->textArea(['rows' => '9', 'class' => 'textarea form-control']); ?>

          <div class="form-group">
              <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                  'class' => 'btn btn-success pull-right',
                  'name' => 'signup-button']) ?>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-fire" aria-hidden="true"></i><?php echo Yii::t('app', 'Обратите внимание');?></h3>
        </div>
        <div class="box-body">
          <?php echo Yii::t('app', '<p>Расскажите о Ваших впечатлениях, о проекте, о полученных выплатах, поделитесь своей историей успеха ! Постарайтесь написать более развернутый текст Вашего отзыва - новым участникам интересно знать Ваше мнение. Отзывы в одно предложение в стиле \'Все хорошо, спасибо.\' \'Отличный проект спасибо всем.\' - публиковаться не будут.</p></br><strong>Важно</strong><p>Отзывы размещаются на главной странице проекта. В них запрещена какая либо реклама своих ресурсов, личных контактов или реф ссылок. Главную страницу посещают участники разных команд и нужно уважать партнёров, которые с ними провели работу и пригласили в проект.</p>');?>
        </div>
      </div>

      <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_response-list', ['responseArray' => $responseArray]);?>

    </div>

  </div>

<?php ActiveForm::end();?>

<?php if(Yii::$app->session->getFlash('success')): ?>
  <div class="modalError modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('success');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalError').modal('show');
", View::POS_END);?>

<?php endif;?>