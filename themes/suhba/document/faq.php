<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'Faq',
    'url' => '/faq'
  ],
];
$this->title = $model->title;
?>

</header>
<!-- Classic Breadcrumbs-->
<section class="breadcrumb-classic">
  <div class="shell section-34 section-sm-50">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-help-circle icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h2><span class="big">Faq</span></h2>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">

          <?= Breadcrumbs::widget([
            'options' => ['class' => 'list-inline list-inline-dashed p'],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]) ?>

      </div>
    </div>
  </div>
</section>
<!-- Page Content-->
<main class="page-content">
  <!-- Faq variant 4-->
  <section class="section-66 section-sm-top-110 section-lg-bottom-0">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-sm-9 cell-lg-6">
          <h1 class="text-darker text-lg-left"><?= Yii::t('app', 'Основные вопросы');?></h1>
          <hr class="divider bg-mantis hr-lg-left-0">
          <div class="offset-top-41 offset-lg-top-66 text-left">
                    <!-- Bootstrap Accordion-->
                    <div role="tablist" aria-multiselectable="true" id="accordion-1" class="panel-group accordion offset-top-0">
                      <div class="panel panel-default">
                        <div role="tab" id="headingOne" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'КАК СТАТЬ УЧАСТНИКОМ СИСТЕМЫ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingOne" id="collapseOne" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Для того, чтобы стать участником, Вам достаточно пройти простую форму регистрации на нашем сайте, перейдя по реферальной ссылке Вашего пригласителя. Обращаем Ваше внимание на то, что данные указываемые при регистрации должны быть достоверны.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingTwo" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'МОГУ ЛИ Я ИЗМЕНИТЬ СВОИ ДАННЫЕ ПОСЛЕ РЕГИСТРАЦИИ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingTwo" id="collapseTwo" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Вам доступно любое изменение данных своего кабинета, кроме изменения Вашего логина и Вашего спонсора.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingThree" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'КАК ВЕРИФИЦИРОВАТЬ E-MAIL?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingThree" id="collapseThree" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Для того чтобы верифицировать e-mail вам необходимо в разделе профиль указать действующую почту и получить активационную ссылку. После того, как вы это сделаете, ваш e-mail будет верифицирован.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'КАК УСТАНОВИТЬ ИЛИ ИЗМЕНИТЬ СВОИ РЕКВИЗИТЫ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseFour" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Прежде всего необходимо получить платежный пароль. Это можно сделать в разделе реквизиты. После этого, вы сможете добавить/изменить реквизиты. Получить платежный пароль возможно только на верифицированную почту.');?>
                          </div>
                        </div>
                      </div>
                       <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'КАКИЕ ВАРИАНТЫ ОПЛАТ ПРИСУТСТВУЮТ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseFive" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Проект работает с криптовалютой Bitcoin');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSix" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'СКОЛЬКО ВРЕМЕНИ ДАЕТСЯ НА АКТИВАЦИЮ АККАУНТА?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseSix" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Вы можете произвести активацию в любой момент времени после того как пройдете регистрацию. Ваша регистрация будет действительна постоянно. Но настоятельно рекомендуем Вам не тянуть с активацией, так как Вы будете терять выплаты от переливов общего распределения участников в структуре.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSeven" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'НУЖНО ЛИ ЖДАТЬ ПОДТВЕРЖДЕНИЕ ОПЛАТЫ ИЛИ ПОДТВЕРЖДАТЬ ПОЛУЧЕНИЕ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseSeven" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'В проекте все переводы между участниками полностью автоматизированы и не требуют подтверждения от их получателя. Транзакция считается успешной при подтверждении ее сетью.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseEight" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'ГДЕ МНЕ УВИДЕТЬ МОЮ РЕФЕРАЛЬНУЮ ССЫЛКУ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseEight" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Реферальная ссылка находится в разделе профиль вашего личного кабинета. Она становится доступной вам, только после активации аккаунта и получения места в программе.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseNine" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'КАК Я МОГУ ИСПОЛЬЗОВАТЬ СВОЮ РЕФЕРАЛЬНУЮ ССЫЛКУ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseNine" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Реферальная ссылка для каждого участника генерируется индивидуально и дает Вам возможность привлекать людей именно в Вашу структуру. Вы можете размещать ее в социальных сетях, блогах, тематических форумах и прикреплять к письмам в E-mail рассылках. Это обеспечит Вас постоянным притоком новых участников в Вашу структуру, а значит, Ваш доход будет расти.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTen" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'КАК ДОЛГО ЖДАТЬ ПОДТВЕРЖДЕНИЯ ТРАНЗАКЦИИ В СЕТИ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseTen" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Все переводы проходят в сети биткоин и требуется время, для того чтобы транзакция принялась сетью и подтвердилась. С учетом особенности сети ожидание подтверждения может занимать несколько часов. Статус вашей транзакции можно увидеть в деталях заявки.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseEleven" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'СКОЛЬКО ЧЕЛОВЕК Я МОГУ ПРИГЛАСИТЬ В СИСТЕМУ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseEleven" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Вы можете приглашать сколько угодно новых участников. Все они будут занимать свободные места в вашей структуре');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwelve" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'СКОЛЬКО Я СМОГУ ЗАРАБОТАТЬ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseTwelve" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Доход за полный цикл составляет: 84.3 BTC. С учетом реинвестов и повторных закрытий вы сможете получать пассивный доход бесконечное количество раз');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThirteen" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'ЧТО ДЕЛАТЬ ЕСЛИ ЗАБЫЛИ ПАРОЛЬ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseThirteen" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Воспользуйтесь функцией восстановления пароля.');?>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div role="tab" id="headingFour" class="panel-heading">
                          <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFourteen" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'ОСТАЛИСЬ ВОПРОСЫ?');?></a></div>
                        </div>
                        <div role="tabpanel" aria-labelledby="headingFour" id="collapseFourteen" class="panel-collapse collapse">
                          <div class="panel-body"><?= Yii::t('app', 'Задать вопрос вы можете на странице <a href="http://storytsa.com/contact">обратной связи</a> или в личном кабинете в разделе "Техническая поддержка"');?>
                          </div>
                        </div>
                      </div>
                    </div>
          </div></br>
        </div>
        <div class="cell-lg-6 offset-top-0"><img src="images/pages/faq-01-531x699.png" width="531" height="699" alt="" class="veil reveal-lg-inline-block"></div>
      </div>
    </div>
  </section>
</main>
