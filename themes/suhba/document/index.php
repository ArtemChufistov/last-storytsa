<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use lowbase\document\DocumentAsset;
use app\modules\profile\models\User;
use app\modules\finance\models\Payment;
use app\modules\menu\widgets\MenuWidget;
use app\modules\profile\components\AuthChoice;
use app\modules\profile\models\forms\LoginForm;

$this->title = $model->title;

$model = new LoginForm();
?>

<div class="center-vertical bg-man-laptop">
    <div id="particles-js"></div>
    <div class="center-content row">
        <div class = "col-lg-10 center-margin">
    <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']);?>
            <?php $form = ActiveForm::begin([
                'action' => ['/login'],
                'id' => 'loginform',
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{hint}\n{error}",
                ],
                'options' => [
                    'class' => 'col-lg-5',
                ]
            ]); ?>

            <div id="login-form" class="content-box" style = "height: 340px;">
                <div class="content-box-wrapper pad20A">
                    <?= $form->field($model, 'loginOrEmail', [
                        'template' => "{label}\n<div class='input-group input-group-lg'>\n<span class='input-group-addon addon-inside bg-white font-primary'><i class='glyph-icon icon-envelope-o'></i></span>\n{input}\n{hint}\n{error}</div>"
                    ])->textInput([
                        'maxlength' => true,
                        'placeholder' => $model->getAttributeLabel('loginOrEmail'),
                        'options' => ['class' => 'form-control']
                    ]); ?>

                    <?= $form->field($model, 'password', [
                        'template' => "{label}\n<div class='input-group input-group-lg'>\n<span class='input-group-addon addon-inside bg-white font-primary'><i class='glyph-icon icon-unlock-alt'></i></span>\n{input}\n{hint}\n{error}</div>"
                    ])->passwordInput([
                        'maxlength' => true,
                        'placeholder' => $model->getAttributeLabel('password'),
                        'options' => ['class' => 'form-control']
                    ]); ?>
                </div>
                <div class="button-pane">
                    <?= Html::submitButton('<i class="glyphicon glyphicon-log-in"></i> ' . Yii::t('user', 'Войти'), [
                        'class' => 'btn btn-block btn-primary',
                        'name' => 'login-button']) ?>
                </div>


                <div class="col-md-6">
                    <?= Html::a(Yii::t('user', 'Восстановить пароль'), '/resetpasswd') ?>
                </div>

                <div class="text-right col-md-6">
                    <?= Html::a(Yii::t('user', 'Регистрация'), ['/signup'], ['class' => 'text-picton-blue']) ?>
                </div>


            </div>

            <?php ActiveForm::end(); ?>

            <div class="videox col-lg-7">
                <div class = "videoWrap">
                    <span class="fa fa-youtube-play" aria-hidden="true" onClick="addVideo();"></span>
                </div>
            </div> 

        </div>
    </div>
</div>

<script>

function addVideo(){
    var yout = '<iframe style="width: 100%; height: 340px" data-title="https://www.youtube.com/embed/amBEJh453CY?autoplay=1" src="http://suhba.net/video/suhba_ru.mp4" frameborder="0" allowfullscreen></iframe>';
    $('.videox').html(yout);
    $('.videox').css('background-color','#000');
}

</script>