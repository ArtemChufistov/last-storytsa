<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = 'Восстановление пароля';
?>

<style type="text/css">

    html, body {
        height: 100%;
    }

</style>

<div class="center-vertical bg-man-laptop">
    <div id="particles-js"></div>
    <div class="center-content row">
        <h3 class="text-center pad25B font-gray font-size-23"><?= Html::encode($this->title) ?></h3>
        <?php if (Yii::$app->session->hasFlash('reset-success')): ?>
            <div class='text-center'><?php echo Yii::$app->session->getFlash('reset-success'); ?></div>
        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{hint}\n{error}",
                ],
                'options' => [
                    'data-form-output' => 'form-output-global',
                    'data-form-type' => 'contact',
                    'class' => 'center-margin col-xs-11 col-sm-4'
                ]
            ]); ?>
            <div id="login-form" class="content-box">
                <div class="content-box-wrapper pad20A">
                    <?= $form->field($forget, 'email', [
                        'template' => "{label}\n<div class='input-group input-group-lg'>\n<span class='input-group-addon addon-inside bg-white font-primary'><i class='glyph-icon icon-envelope-o'></i></span>\n{input}\n{hint}\n{error}"
                    ])->textInput([
                        'maxlength' => true,
                        'placeholder' => $forget->getAttributeLabel('email'),
                    ]); ?>

                    <?= $form->field($forget, 'password', [
                        'template' => "{label}\n<div class='input-group input-group-lg'>\n<span class='input-group-addon addon-inside bg-white font-primary'><i class='glyph-icon icon-unlock-alt'></i></span>\n{input}\n{hint}\n{error}"
                    ])->textInput([
                        'maxlength' => true,
                        'placeholder' => $forget->getAttributeLabel('password'),
                    ]); ?>

                    <?= $form->field($forget, 'captcha')->widget(Captcha::className(), [
                        'captchaAction' => '/profile/profile/captcha',
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => $forget->getAttributeLabel('captcha')
                        ]
                    ])->label('');
                    ?>
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <?= Html::submitButton(Yii::t('user', 'Сменить пароль'), [
                        'class' => 'btn btn-primary btn-block text-uppercase waves-effect waves-light',
                        'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        <?php endif; ?>

        <div class="row">
            <div class="col-md-6">
                <?= Html::a(Yii::t('user', 'Личный кабинет'), ['/login'], ['class' => 'text-picton-blue']) ?>
            </div>

            <div class="text-right col-md-6">
                <?= Html::a(Yii::t('user', 'Регистрация'), ['/signup'], ['class' => 'text-picton-blue']) ?>
            </div>
        </div>
    </div>
</div>