<?php
use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use lowbase\user\components\AuthChoice;
use app\modules\mainpage\models\Preference;

$this->title = Yii::t('app', 'Регистрация');
?>

<style type="text/css">

    html, body {
        height: 100%;
    }

</style>

<div class="center-vertical bg-man-laptop">
    <div id="particles-js"></div>
    <div class="center-content row">
        <?php
        $form = ActiveForm::begin([
            'id' => 'loginform',
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{hint}\n{error}",
            ],
            'options' => [
                'class' => 'center-margin col-xs-11 col-sm-4'
            ]
        ]);
        ?>
        <h3 class="text-center pad25B font-gray font-size-23"><?= Html::encode($this->title) ?></h3>
        <div id="login-form" class="content-box">
            <div class="content-box-wrapper pad20A">
                <?php $readonlyPref = Preference::find()->where(['key' => Preference::KEY_USER_REG_CAN_PARENT_EDIT])->one()->value == 1 ? false : true; ?>

                <?= $form->field($model, 'ref')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('ref'),
                    'readonly' => $readonlyPref,
                    'id' => 'ref-user-name',
                ]); ?>

                <?= $form->field($model, 'email')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('email'),
                    'id' => 'email-user-name',
                ]); ?>

                <?= $form->field($model, 'password')->passwordInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('password'),
                ]); ?>

                <?= $form->field($model, 'passwordRepeat')->passwordInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('passwordRepeat'),
                ]); ?>

                <div class="checkbox checkbox-primary p-t-0">
                    <?php $labelLicense = Yii::t('app', 'Я согласен с условиями') . ' ' . Html::a(Yii::t('app', 'лицензионного соглашения'), ['/licenseagreement'], ['target' => '_blank']); ?>

                    <?= $form->field($model, 'licenseAgree', ['template' => '{input}{label}{error}', 'options' => ['tag' => false]])->checkBox([], false)->label($labelLicense, ['class' => 'label-text']); ?>
                </div>

                <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
                    'imageOptions' => [
                        'id' => 'my-captcha-image',
                    ],
                    'captchaAction' => '/profile/profile/captcha',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => $model->getAttributeLabel('captcha')
                    ],
                    'template' => '<div class="row"><div class="col-lg-8">{input}</div><div class="col-lg-4">{image}</div></div>',
                ]);
                ?>

                <div class="button-pane">
                    <?= Html::submitButton('<i class="glyphicon glyphicon-user"></i> ' . Yii::t('user', 'Зарегистрироваться'), [
                        'class' => 'btn btn-block btn-primary',
                        'name' => 'login-button']) ?>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <?= Html::a(Yii::t('user', 'Личный кабинет'), ['/login'], ['class' => 'text-picton-blue']) ?>
                    </div>

                    <div class="text-right col-md-6">
                        <?= Html::a(Yii::t('user', 'Восстановить пароль'), '/resetpasswd') ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


<style>

    #my-captcha-image {
        cursor: pointer;
    }
</style>