<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Для СМИ');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<style>
    .mainbrand {
        width: 960px;
        float: left;
        height: 300px;
        background-color: #64aa46;
        border-radius: 4px;
        background-image: url(http://suhba.net/images/1.png);
        background-position: center;
        background-repeat: no-repeat;
    }
    .mainbrand1 {
        width: 960px;
        float: left;
        height: 300px;
        border-radius: 4px;
        background-position: center;
        background-repeat: no-repeat;
    }
    .x25_1 {
        width: 228px;
        height: 262px;
        float: left;
        border-radius: 4px;
        margin: 15px 15px 0px 0;
        background-color: #49952A;
        background-size: cover;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -ms-transition: all 0.3s ease;
        background-image: url(http://suhba.net/images/2.png);
    }
    .x25_1 .name1 {
        float: left;
        width: 100%;
        text-align: center;
        color: rgba(255, 255, 255, 0.85);
        font-weight: bold;
        font-size: 20px;
        margin-top: 170px;
    }
    .x25_1 .name2 {
        float: left;
        width: 100%;
        text-align: center;
        color: rgba(255, 255, 255, 0.85);
    }
    .x25_1:hover {
        background-color: #64aa46;
        cursor: pointer;
    }
    .downoladbrandbook {
        float: left;
        padding: 7px 15px;
        margin: 236px 300px 0;
        background-color: rgba(255, 255, 255, 0.9);
        color: #40463D;
        border-radius: 4px;
        cursor: pointer;
        width: 360px;
        text-align: center
    }
    .colclass {
        float: left;
        width: 100%;
        color: #64aa46;
        font-weight: bold;
        font-size: 20px;
    }
    .lsps {
        width: 310px;
        float: left;
        height: 50px;
        background-color: #64aa46;
        border-radius: 4px;
        margin-bottom: 15px;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
        -ms-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        cursor: pointer;
    }
    .lsps:hover {
        background-color: #90C063;
    }
    .lsps font {
        width: 100%;
        text-align: center;
        color: rgba(255, 255, 255, 0.85);
        font-weight: bold;
        margin-top: 17px;
        float: left
    }
</style>

        <div class="panel">
            <div class="panel-body">
                <div class="example-box-wrapper">
                    <ul class="list-group list-group-separator row list-group-icons">
                        <li class="col-md-3 active">
                            <a href="#tab-example-1" data-toggle="tab" class="list-group-item">
                                <i class="glyph-icon font-red icon-bullhorn"></i>
                                <?php echo Yii::t('app', 'Документы на Русском');?>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab-example-1">

                                <div id="russianb" style=" height: 960px;">
                                    <div style="width:100%;float:left;height:15px;"></div>
                                    <div class="colclass">Сеть</div>
                                    <div style="width:100%;float:left;height:15px;"></div>



                                    <div class="mainbrand1" style = "margin-top: 30px;">

                                       <a href="/suhba/ocenka_suhba_titul.pdf" target="new">
                                            <div class="x25_1">
                                                <div class="name1">Оценка стоимости активов</div>
                                                <div class="name2">Скачать</div>
                                            </div>
                                        </a>

                                       <a href="/suhba/171222-10.pdf" target="new">
                                            <div class="x25_1">
                                                <div class="name1">Проверка контрагента</div>
                                                <div class="name2">Скачать</div>
                                            </div>
                                        </a>

                                       <a href="/suhba/agent_dogovor.pdf" target="new">
                                            <div class="x25_1">
                                                <div class="name1">Агентский Договор</div>
                                                <div class="name2">Скачать</div>
                                            </div>
                                        </a>

                                       <a href="/suhba/priem_peredacha.pdf" target="new">
                                            <div class="x25_1">
                                                <div class="name1">Акт приема-передачи, Приложение №2</div>
                                                <div class="name2">Скачать</div>
                                            </div>
                                        </a>

                                       <a href="/suhba/dogovor_investirovania.pdf" target="new">
                                            <div class="x25_1">
                                                <div class="name1">Договор инвестирования</div>
                                                <div class="name2">Скачать</div>
                                            </div>
                                        </a>
  

                                    </div>


                                </div>

                        </div>
                        <div class="tab-pane fade" id="tab-example-2">

                                    <div style="width:100%;float:left;height:15px;"></div>
                                    <div class="colclass">Network</div>
                                    <div style="width:100%;float:left;height:15px;"></div>


                                    <div class="mainbrand">

                                        <a href="http://suhba.net/forsmi/brand/brandbook_en.pdf" target="new">
                                            <div class="downoladbrandbook"><i class="icon-floppy"></i> Download Brand Book</div>
                                        </a>

                                    </div>

                                    <div class="mainbrand1" style = "margin-top: 30px;">
                                        <a href="http://suhba.net/forsmi/logos/logos.zip" target="new">
                                            <div class="x25_1">
                                                <div class="name1">Logotypes</div>
                                                <div class="name2">Download</div>
                                            </div>
                                        </a>

                                        <a href="http://suhba.net/forsmi/presentation/presentation_suhba_en.pdf" target="new">
                                            <div class="x25_1">
                                                <div class="name1">Presentation</div>
                                                <div class="name2">Download in PDF</div>
                                            </div>
                                        </a>

                                        <a href="http://suhba.net/forsmi/teaser/teaser_suhba_en.pdf" target="new">
                                            <div class="x25_1">
                                                <div class="name1">Teaser</div>
                                                <div class="name2">Download in PDF</div>
                                            </div>
                                        </a>

                                        <a href="http://suhba.net/forsmi/press/press_kit_suhba_en.pdf" target="new">
                                            <div class="x25_1" style="margin-right:0;">
                                                <div class="name1">Press Kit</div>
                                                <div class="name2">Download in PDF</div>
                                            </div>
                                        </a>

       
                                    </div>

                                    <div style="width:100%;float:left;height:15px;"></div>
                                    <div class="colclass">Suhba-Connect</div>

                                    <div class="mainbrand1" style = "margin-top: 30px;">
                                        <a href="http://suhba.net/forsmi/press/press_kit_connect_en.pdf" target="new">
                                            <div class="x25_1" style="background-image:url(http://suhba.net/images/4b.png);width:310px;background-position: center;">
                                                <div class="name1">Press kit</div>
                                                <div class="name2">Download in PDF</div>
                                            </div>
                                        </a>

                                        <a href="http://suhba.net/forsmi/teaser/teaser_connect_en.pdf" target="new">
                                            <div class="x25_1" style="background-image:url(http://suhba.net/images/4b.png);width:310px;background-position: center;">
                                                <div class="name1">Teaser</div>
                                                <div class="name2">Download in PDF</div>
                                            </div>
                                        </a>

                                        <a href="http://suhba.net/forsmi/presentation/presentation_connect_en.pdf" target="new">
                                            <div class="x25_1" style="background-image:url(http://suhba.net/images/4b.png);width:310px;background-position: center;margin-right:0;">
                                                <div class="name1">Presentation</div>
                                                <div class="name2">Download in PDF</div>
                                            </div>
                                        </a>
                                    </div>

                                    <div style="width:100%;float:left;height:15px;"></div>
                                    <div class="colclass">Adversting</div>
                                    <div class="mainbrand1" style = "margin-top: 30px;">
                                        <a href="http://suhba.net/video/promo_suhba_en.mp4" target="new">
                                            <div class="x25_1" style="background-image:url(http://suhba.net/images/4.png) !important;width: 960px;background-position: center;background-size:auto;background-repeat: no-repeat;">
                                                <div class="name1">Advertising clip</div>
                                                <div class="name2">Download video</div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="mainbrand1" style = "margin-top: 30px;">
                                        <a href="http://suhba.net/forsmi/reklama/promo_suhba_en.png" target="new">
                                            <div class="x25_1" style="background-image:url(http://suhba.net/images/2.png) !important;width: 960px;background-position: center;background-size:auto;background-repeat: no-repeat;">
                                                <div class="name1">Advertising banner</div>
                                                <div class="name2">Download PNG</div>
                                            </div>
                                        </a>
                                    </div>

                        </div>
                        <div class="tab-pane fade" id="tab-example-3">

                                    <div class="mainbrand">

                                        <a href="http://suhba.net/forsmi/brand/brandbook_ar.pdf" target="new">
                                            <div class="downoladbrandbook"><i class="icon-floppy"></i> تحميل كتاب العلامة التجارية</div>
                                        </a>

                                    </div>

                                    <div class="mainbrand1" style = "margin-top: 30px;">

                                        <a href="http://suhba.net/forsmi/logos/logos.zip" target="new">
                                            <div class="x25_1">
                                                <div class="name1">الشعار</div>
                                                <div class="name2">تحميل</div>
                                            </div>
                                        </a>

                                        <a href="http://suhba.net/forsmi/press/press_kit_suhba_ar.pdf" target="new">
                                            <div class="x25_1">
                                                <div class="name1">صحافة صحبة</div>
                                                <div class="name2">تحميل PDF</div>
                                            </div>
                                        </a>

                                        <a href="http://suhba.net/forsmi/presentation/presentation_suhba_ar.pdf" target="new">
                                            <div class="x25_1">
                                                <div class="name1">عرض صحبة</div>
                                                <div class="name2">تحميل PDF</div>
                                            </div>
                                        </a>

                                        <a href="http://suhba.net/forsmi/presentation/presentation_connect_ar.pdf" target="new">
                                            <div class="x25_1" style="margin-right:0;">
                                                <div class="name1">عرض التواصل</div>
                                                <div class="name2">تحميل PDF</div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="mainbrand1" style = "margin-top: 30px;">

                                        <a href="http://suhba.net/video/suhba_en.mp4" target="new">
                                            <div class="x25_1" style="background-image:url(http://suhba.net/images/4.png) !important;width: 960px;background-position: center;background-size:auto;background-repeat: no-repeat;">
                                                <div class="name1">العرض</div>
                                                <div class="name2">تحميل الفيديو</div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="mainbrand1" style = "margin-top: 30px;">

                                        <a href="http://suhba.net/forsmi/reklama/promo_suhba_ar.png" target="new">
                                            <div class="x25_1" style="background-image:url(http://suhba.net/images/2.png) !important;width: 960px;background-position: center;background-size:auto;background-repeat: no-repeat;">
                                                <div class="name1">صورة الأ‘علان</div>
                                                <div class="name2">تحميل PNG</div>
                                            </div>
                                        </a>
                                    </div>

                                </div>  


                    </div>
                </div>
            </div>
        </div>
