<?php

use app\modules\profile\components\AuthKeysManager;
use borales\extensions\phoneInput\PhoneInput;
use yii\authclient\widgets\AuthChoice;
use lowbase\user\models\Country;
use app\modules\profile\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Мой профиль');
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    .thumb-lg {
        height: 88px;
        width: 88px;
    }

    /*User-box*/
    .user-bg {
        margin-top: -15px;
        margin-left: -20px;
        margin-right: -20px;
        height: 330px;
        overflow: hidden;
        position: relative;
    }

    .user-bg .overlay-box {
        background: #707cd2;
        opacity: 0.9;
        position: absolute;
        top: 0px;
        left: 0px;
        right: 0px;
        height: 100%;
        text-align: center;
    }

    .user-bg .overlay-box .user-content {
        padding: 15px;
        margin-top: 30px;
    }

    .user-btm-box {
        padding: 40px 0 10px;
        clear: both;
        overflow: hidden;
    }

    .text-white {
        color: #ffffff;
    }

    .auth-clients li {
        display: inline-table;
        float: unset;
    }

    .auth-clients {
        text-align: center;
    }
</style>

<?php $form = ActiveForm::begin([
    'id' => 'form-profile',
    'options' => [
        'class' => 'form-horizontal bordered-row',
        'enctype' => 'multipart/form-data'
    ],
]); ?>

<h2 style = "margin-bottom: 15px; font-size: 20px;"><?= $this->title;?></h2>

<div class="row">
    <div class="col-md-4 col-xs-12">
        <div class="panel">
            <div class="panel-body mrg10B">
                <div class="example-box-wrapper">
                    <div class="user-bg" style = "min-height: 400px;"><img style = "height: 400px;" alt="<?php echo $user->login; ?>"
                                              src="/images/money_back.jpg">
                        <div class="overlay-box">
                            <div class="user-content" >
                                <img src="<?php echo $user->getImage(); ?>" class="thumb-lg img-circle"
                                     alt="<?php echo $user->login; ?>">
                                <br/>
                                <br/>
                                <?php if ($user->image): ?>
                                    <?php echo Html::a(Yii::t('app', 'Удалить фото'), ['/profile/office/remove'], ['class' => 'btn btn-default btn-file']); ?>
                                    <br/>
                                <?php else: ?>
                                    <?= $form->field($user, 'photo', ['options' => ['class' => 'btn btn-default btn-file'], 'template' => '<i class="fa fa-paperclip"></i> ' . Yii::t('app', 'Ваше изображение') . '{input}'])->fileInput() ?>
                                    <br/>
                                    <br/>
                                        <span style="color: white;"><?php echo Yii::t('app', 'в формате (PNG, JPG), не должно превышать 2 МБ');?></span>
                                    <br/>
                                    <br/>
                                    <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Сохранить'), [
                                        'class' => 'btn btn-success',
                                        'name' => 'signup-button']) ?>
                                <?php endif; ?>

                                <h4 class="text-white mrg10T"><?php echo $user->login; ?></h4>
                                <h5 class="text-white mrg10T"><?php echo $user->email; ?></h5></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    <?php echo Yii::t('app', 'Смена пароля'); ?>
                </h3>
                <div class="example-box-wrapper">

                    <div class="lb-user-user-profile-password">
                        <?= $form->field($user, 'old_password', [
                            'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                            'labelOptions' => ['class' => 'col-sm-3 control-label']
                        ])->passwordInput([
                            'maxlength' => true,
                            'value' => '',
                            'placeholder' => $user->getAttributeLabel('old_password'),
                            'class' => 'form-control password'
                        ]) ?>
                    </div>

                    <div class="lb-user-user-profile-password">
                        <?= $form->field($user, 'password', [
                            'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                            'labelOptions' => ['class' => 'col-sm-3 control-label']
                        ])->passwordInput([
                            'maxlength' => true,
                            'value' => '',
                            'placeholder' => $user->getAttributeLabel('password'),
                            'class' => 'form-control password'
                        ]) ?>
                    </div>

                    <div class="lb-user-user-profile-password">
                        <?= $form->field($user, 'repeat_password', [
                            'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                            'labelOptions' => ['class' => 'col-sm-3 control-label']
                        ])->passwordInput([
                            'maxlength' => true,
                            'value' => '',
                            'placeholder' => $user->getAttributeLabel('repeat_password'),
                            'class' => 'form-control repeat_password'
                        ]) ?>
                    </div>

                    <div class="form-group" style="padding-left: 15px;">
                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Сохранить'), [
                            'class' => 'btn btn-success pull-left',
                            'value' => 1,
                            'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<style type="text/css">
    .field-profileform-phone{
        width: 100%;
    }
</style>

    <div class="col-md-4 col-xs-12">
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    <?php echo Yii::t('app', 'Личные данные'); ?>
                </h3>
                <div class="example-box-wrapper">

                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($user, 'first_name', [
                                'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => ['class' => 'col-sm-3 control-label']
                            ])->textInput([
                                'maxlength' => true,
                                'placeholder' => $user->getAttributeLabel('first_name')
                            ])->label(Yii::t('app', 'ФИО')) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($user, 'address', [
                                'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => ['class' => 'col-sm-3 control-label']
                            ])->textInput([
                                'maxlength' => true,
                                'placeholder' => $user->getAttributeLabel('address')
                            ])->label('Адрес проживания') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($user, 'nationality', [
                                'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => ['class' => 'col-sm-3 control-label']
                            ])->textInput([
                                'maxlength' => true,
                                'placeholder' => $user->getAttributeLabel('nationality')
                            ])->label('Гражданство') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($user, 'passport', [
                                'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => ['class' => 'col-sm-3 control-label']
                            ])->textInput([
                                'maxlength' => true,
                                'placeholder' => $user->getAttributeLabel('passport')
                            ])->label('Номер удостоверения личности') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($user, 'skype', [
                                'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => ['class' => 'col-sm-3 control-label']
                            ])->textInput([
                                'maxlength' => true,
                                'placeholder' => $user->getAttributeLabel('skype')
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($user, 'birthday', [
                                'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => ['class' => 'col-sm-3 control-label']
                            ])
                                ->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => $user->getAttributeLabel('birthday')],
                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd.mm.yyyy'
                                    ]
                                ]); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="col-sm-3 control-label" for="profileform-last_name" ><?php echo Yii::t('app', 'Телефон');?></label>
                            <div class="col-sm-8">
                                <?php echo $form->field($user, 'phone')->widget(PhoneInput::className(), [
                                    'jsOptions' => [
                                        'preferredCountries' => ['ru'],
                                    ]
                                ])->label(''); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php $sexArray = User::getSexArray(); unset($sexArray[User::SEX_OTHER]);?>
                            <?= $form->field($user, 'sex', [
                                'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => ['class' => 'col-sm-3 control-label']
                            ])
                                ->radioList(
                                    $sexArray,
                                    [
                                        'item' => function ($index, $label, $name, $checked, $value) {

                                            $checked = $checked == true ? 'checked="checked"' : '';
                                            $return = '<label class="radio-inline">';
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"  ' . $checked . ' >';
                                            $return .= '<i></i>';
                                            $return .= '<span>' . ucwords($label) . '</span>';
                                            $return .= '</label>';

                                            return $return;
                                        }
                                    ]
                                ); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Сохранить'), [
                                'class' => 'btn btn-success pull-left',
                                'name' => 'signup-button']) ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/_parent-wrap', ['user' => $user]); ?>
        <div class="panel">
            <div class="panel-body">
                <h3 class="title-hero">
                    <?php echo Yii::t('app', 'Сменить E-mail'); ?>
                </h3>
                <div class="example-box-wrapper">

                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($user, 'email', [
                                'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => ['class' => 'col-sm-3 control-label']
                            ])->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                                'placeholder' => $user->getAttributeLabel('email')
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?= $form->field($user, 'email_confirm_token', [
                                'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                                'labelOptions' => ['class' => 'col-sm-3 control-label'],
                                'options' => [
                                    'style' => $oldUser->email_confirm_token == '' ? 'display:none;' : 'display:block;']])->textInput([
                                'maxlength' => true,
                                'value' => '',
                                'placeholder' => $user->getAttributeLabel('email_confirm_token')
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?php if ($oldUser->email_confirm_token == ''): ?>
                                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Отвязать'), [
                                    'class' => 'btn btn-success pull-left',
                                    'value' => 1,
                                    'name' => (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                            <?php else: ?>
                                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Отвязать'), [
                                    'class' => 'btn btn-success pull-left',
                                    'value' => 1,
                                    'name' => (new \ReflectionClass($user))->getShortName() . '[confirm_token_submit]']) ?>

                                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Отправить повторно код подтверждения'), [
                                    'class' => 'btn btn-success pull-right',
                                    'value' => 1,
                                    'name' => (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php $successMessage = Yii::$app->session->getFlash('success');?>

<?php if (!empty($successMessage)):?>

  <div class="modal fade bs-example-modal-lg hotmessage" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="myLargeModalLabel"><?php echo Yii::t('app', 'Внимание !');?></h4> </div>
        <div class="modal-body" style="padding: 0px;">
          <div style= "margin-top: 20px;margin-bottom:20px; text-align: center;"><?php echo $successMessage;?></div>
        </div>
        <div class="modal-footer" style="height: 55px;">
          <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("

jQuery(document).ready(function($) {
  $('.hotmessage').modal('show');
});
");?>

<?php endif;?>