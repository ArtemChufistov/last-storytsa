<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = Yii::t('user', 'События');

?>

<div id="page-title">
    <h2><?php echo $this->title;?></h2>
    <p><?php echo Yii::t('app', 'Информация о событиях');?></p>
</div>

<div class="row">
    <?php echo ListView::widget([
          'dataProvider' => $dataProvider,
          'itemView' => '_list_news',
          'layout' => "{items}\n{pager}",
      ]);

      ?>
</div>