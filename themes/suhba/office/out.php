<?php
use app\modules\finance\models\Payment;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>

<style type="text/css">
.form-wizard .acti .wizard-step {
  background: #00bca4 none repeat scroll 0 0;
  color: #fff;
}
.form-wizard li a:hover{
  text-decoration: none;
}
</style>
<?php 
	$dayDate = intval(date('d'));
?>

<?php if ($dayDate >= 1 && $dayDate <= 10): ?>

	<div class="row">
	  <div class="col-md-12">
	    <div class="panel">
	      <div class="panel-body">
	        <div class="example-box-wrapper">
				<?php echo Yii::t('app', '<h3>В данный момент мы производим все расчеты по начислением. Вывод средств станет доступен <strong>6</strong>-го числа в <strong>00-00</strong> по МСК</h3>');?>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

<?php else:?>
	<?php echo Yii::$app->controller->renderPartial(
	  '@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_' . strtolower(\yii\helpers\StringHelper::basename(get_class($paymentForm))),
	          ['paymentForm' => $paymentForm, 'user' => $user, 'currencyArray' => $currencyArray]); ?>
<?php endif;?>