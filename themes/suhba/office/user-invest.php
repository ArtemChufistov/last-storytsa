<div class="content-box tabs">
    <h3 class="content-box-header bg-green">
        <span><?php echo Yii::t('app', 'Ваши инвестиционные пакеты');?></span>
        <ul>
            <li>
                <a href="#tabs-example-1" title="Tab 1">
                    <?php echo Yii::t('app', 'Приобретённые пакеты');?>
                </a>
            </li>
            <li>
                <a href="#tabs-example-2" title="Tab 2">
                    <?php echo Yii::t('app', 'Сертификаты');?>
                </a>
            </li>

        </ul>
    </h3>
    <div id="tabs-example-1">
        <table class="table table-hover manage-u-table">
            <thead>
                <tr>
                    <th class="text-center" width="70">№</th>
                    <th class="text-center"><?php echo Yii::t('app', 'Пакет');?></th>
                    <th class="text-center"><?php echo Yii::t('app', 'Цена');?></th>
                    <th class="text-center"><?php echo Yii::t('app', 'Дата покупки:');?></th>
                </tr>
            </thead>
            <tbody>
            <?php $inSum = 0; $outSum = 0;?>
            <?php foreach($userInvestArray as $num => $userInvest ):?>
                <tr>
                    <td class="text-center"><?php echo $num + 1;?></td>
                    <td class="text-center"><?php echo $userInvest->getInvestType()->one()->name;?></td>
                    <td class="text-center"><?php echo number_format($userInvest->sum, 2, '.', ' ');?></td>
                    <td class="text-center"><?php echo date('d-m-Y', strtotime($userInvest->date_add));?></td>
                    <?php $outSum += $userInvest->sum;?>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <p>
            <strong><?php echo Yii::t('app', 'Всего вложено:');?></strong> <?php echo number_format($outSum, 2);?> <?php echo Yii::t('app', 'RUB');?><br/>
        </p>
    </div>
    <div id="tabs-example-2">
        <?php echo Yii::t('app', 'В разработке');?>
    </div>
</div>


</div>
<?php $this->registerJs("
    $(function() { 'use strict';
        $('.tabs').tabs();
    });

    $(function() { 'use strict';
        $('.tabs-hover').tabs({
            event: 'mouseover'
        });
    });
");?>