<?php
use app\modules\profile\models\User;
use app\modules\finance\models\Transaction;
use app\modules\invest\models\UserInvest;
use app\modules\invest\models\InvestType;

?>
<?php if (in_array($model->type,[Transaction::TYPE_PARTNER_BUY_PLACE, Transaction::TYPE_BUY_MATRIX_PLACE, Transaction::TYPE_REINVEST_MATRIX_PLACE, Transaction::TYPE_WITHDRAW_MATRIX_PLACE])):?>
	<?php $matrixQueue = $model->getMatrixQueue()->one(); if (!empty($matrixQueue)){ echo $matrixQueue->getRootMatrix()->one()->name;}?>
<?php elseif (in_array($model->type, [Transaction::TYPE_PARTNER_BUY_PLACE_TO_DEPOSIT]) && !empty($model->payment_id)):?>
	<?php echo Yii::t('app', 'Логин:');?>
	<?php $userInvest = UserInvest::find()->where(['id' => $model->payment_id])->one();?>
	<?php if (!empty($userInvest)):?>
		<?php $user = $userInvest->getUser()->one();?>
		<?php if (!empty($user)):?>
			<?php echo $user->login;?>
		<?php endif;?>
	<?php endif;?>
<?php elseif (in_array($model->type, [Transaction::TYPE_PARTNER_BUY_AKCII])):?>
	<?php $investType = InvestType::find()->where(['id' => $model->invest_type_id])->one(); echo empty($investType) ? '' : $investType->name; ?>
<?php elseif (in_array($model->type, [Transaction::TYPE_INNER_PAY])):?>
	<?php $user = User::find()->where(['id' => $model->data])->one(); if (!empty($user)){echo $user->login;}?>
<?php elseif (in_array($model->type, [Transaction::TYPE_PROMO1])):?>
	<?php echo Yii::t('app', '{num} долей', ['num' => $model->data]);?>
<?php endif;?>