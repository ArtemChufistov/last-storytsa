<?php if ($model->to_user_id == \Yii::$app->user->identity->id):?>
	<span class="badge bg-green">+<?php echo number_format($model->sum, 2, '.', ' ');?> <?php echo $model->getCurrency()->one()->title;?></span>
<?php else:?>
	<span class="badge bg-red">-<?php echo number_format($model->sum, 2, '.', ' ');?> <?php echo $model->getCurrency()->one()->title;?></span>
<?php endif;?>