<?php
use app\modules\finance\models\Payment;
?>
<?php if ($model->status == Payment::STATUS_WAIT_SYSTEM):?>
	<div class="tl-label bs-label label-info"><?php echo $model->getStatusTitle();?></div>
<?php elseif($model->status == Payment::PROCESS_SYSTEM):?>
	<span class="bs-label label-primary"><?php echo $model->getStatusTitle();?></span>
<?php elseif($model->status == Payment::STATUS_OK):?>
	<span class="bs-label bg-green" title=""><?php echo $model->getStatusTitle();?></span>
<?php elseif($model->status == Payment::STATUS_CANCEL):?>
	<div class="tl-label bs-label label-danger"><?php echo $model->getStatusTitle();?></div>
<?php endif;?>