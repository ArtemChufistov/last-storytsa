<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\invest\components\marketing\SuhbaMarketing;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Payment;
use app\modules\finance\models\Currency;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = Yii::t('user', 'Детализация маркетинговых начислений');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$currencyRur = Currency::find()->where(['key' => Currency::KEY_RUR])->one();

$periodArray = array_reverse(SuhbaMarketing::getPeriodArray());

?>

<?php foreach($periodArray as $period):?>

<?php
$detailMarketing->to_user_id = $user->id;
$detailMarketing->dateFrom = $period['dateFrom'];
$detailMarketing->dateTo = $period['dateTo'];

$detailProvider = $detailMarketing->search(Yii::$app->request->queryParams);
$detailProvider->pagination->pageSize = 10;
?>
  <?php if (!empty($detailProvider->getModels())):?>
    <?= GridView::widget([
        'dataProvider' => $detailProvider,
        'filterModel' => $detailMarketing,
        'id' => 'detail-grid',
        'layout' => '
          <div class="panel">
            <h3 class="content-box-header bg-default">' . Yii::t('app', 'маркетинговые начисления c {dateFrom} по {dateTo}', [
              'dateFrom' => date('d-m-y', strtotime($detailMarketing->dateFrom)), 
              'dateTo' => date('d-m-y', strtotime($detailMarketing->dateTo))]) . ' ' .
              '<span style = "float:right;">' . Yii::t('app', 'итого:') . ' ' . number_format($detailMarketing->sumForPeriod(Yii::$app->request->queryParams), 2, '.', ' ') . ' РУБ</span>' .
            '</h3>
                <div class="table-responsive" style = "border-top: 1px solid #dfe8f1; border-bottom: 1px solid #dfe8f1;">
                  {items}
                </div>
                <div style = "padding-left: 25px;">{pager}</div>
          </div>
          ',
        'columns' => [
            [
              'header' => Yii::t('app', 'Партнёр'),
              'attribute' => 'from_user_id',
              'format' => 'raw',
              'filter' => false,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_marketing-from-user-id', ['model' => $model]);},
            ],[
              'header' => Yii::t('app', 'Сумма'),
              'attribute' => 'sum',
              'format' => 'raw',
              'filter' => false,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_marketing-sum', ['model' => $model]);},
            ],[
              'header' => Yii::t('app', 'Процент'),
              'attribute' => 'percent',
              'format' => 'raw',
              'filter' => false,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_marketing-percent', ['model' => $model]);},
            ],[
              'header' => Yii::t('app', 'Уровень'),
              'attribute' => 'level',
              'format' => 'raw',
              'filter' => false,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_marketing-level', ['model' => $model]);},
            ],[
              'attribute' => 'date',
              'header' => Yii::t('app', 'Дата'),
              'format' => 'raw',
              'filter' => false,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_marketing-date', ['model' => $model]);},
            ]
        ],
    ]); ?>
  <?php endif;?>

<?php endforeach;?>

<style type="text/css">
  .content-box-header{
    font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  }
</style>