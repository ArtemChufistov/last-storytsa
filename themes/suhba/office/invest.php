<?php
use app\modules\invest\models\UserInvest;
use app\modules\invest\models\InvestPref;
use app\modules\event\models\Event;
use lowbase\user\UserAsset;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('app', 'Выберите инвестиционный пакет');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<script src="/js/jquery-ui.min.js"></script>
<script src="/js/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" href="/css/styles-price.css">
<style type="text/css">
.pricing-box .pricing-specs span{
  font-size: 32px;
}
.pricing-box .pricing-specs sup{
  top: 0px;
  margin-left: 0px !important;
  margin-right: 0px;
}
.descr-link{
    margin-top: 30px;
    font-size: 16px;
    text-decoration: underline;
    display: block;
}
</style>

<?php if(Yii::$app->session->getFlash('message')): ?>
  <div class="modalMessage modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('message');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalMessage').modal('show');
", View::POS_END);?>

<?php endif;?>

<h2 style = "margin-bottom: 15px; font-size: 20px;"><?= $this->title;?></h2>

<div class="row">
    <div class="col-xs-6 col-sm-3">
        <div class="pricing-box content-box">
            <h3 class="bg-black pricing-title"><?php echo Yii::t('app', 'МИНИМАЛЬНЫЙ');?></h3>
            <?php $investPrefOpc = InvestPref::find()->where(['invest_type_id' => 1])->andWhere(['key' => InvestPref::KET_COUNT_AKCII])->one()?>
            <div class="bg-blue pricing-specs">
                <span>10 000<sup>p</sup></span>
                <i><?php echo Yii::t('app', '{count} опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?></i>
            </div>
            <a href = "#"  class="descr-link" data-toggle="modal" data-target="#investDescr">
              <?php echo Yii::t('app', 'Подробнее о бонусах');?>
            </a>
            <div class="pad25A">
                <button type="button" class="btn btn-lg text-transform-upr btn-black font-size-12" id = "modalInvest1">
                    <?php echo Yii::t('app', 'Приобрести');?>
                </button>
            </div>
        </div>
    </div>

      <div class="modalInvest1 modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title"><?php echo Yii::t('app', 'МИНИМАЛЬНЫЙ');?></h4>
            </div>
            <div class="modal-body" style = "text-align: center;">
                <p style = "font-size: 16px;">
                    <?php echo Yii::t('app', 'Вы приобретаете <strong>{count}</strong> опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?>
                </p>
                <p style = "font-size: 16px; margin-bottom: 20px;">
                    <?php echo Yii::t('app', 'С Вашего баланса спишется <strong>10 000</strong> руб.');?>
                </p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Приобрести'), [
                      'class' => 'btn btn-lg text-transform-upr btn-black font-size-12',
                      'value' => 1,
                      'name' => 'invest-type']) ?>
                <?php ActiveForm::end();?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
            </div>
          </div>
        </div>
      </div>

    <div class="col-xs-6 col-sm-3">
        <div class="pricing-box content-box">
            <h3 class="bg-black pricing-title"><?php echo Yii::t('app', 'БАЗОВЫЙ');?></h3>
            <?php $investPrefOpc = InvestPref::find()->where(['invest_type_id' => 2])->andWhere(['key' => InvestPref::KET_COUNT_AKCII])->one()?>
            <div class="bg-green pricing-specs">
                <span>25 000<sup>p</sup></span>
                <i><?php echo Yii::t('app', '{count} опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?></i>
            </div>
            <a href = "#"  class="descr-link" data-toggle="modal" data-target="#investDescr">
              <?php echo Yii::t('app', 'Подробнее о бонусах');?>
            </a>
            <div class="pad25A">
                <button type="button" class="btn btn-lg text-transform-upr btn-black font-size-12" id = "modalInvest2">
                    <?php echo Yii::t('app', 'Приобрести');?>
                </button>
            </div>
        </div>
    </div>

      <div class="modalInvest2 modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title"><?php echo Yii::t('app', 'БАЗОВЫЙ');?></h4>
            </div>
            <div class="modal-body" style = "text-align: center;">
                <p style = "font-size: 16px;">
                    <?php echo Yii::t('app', 'Вы приобретаете <strong>{count}</strong> опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?>
                </p>
                <p style = "font-size: 16px; margin-bottom: 20px;">
                    <?php echo Yii::t('app', 'С Вашего баланса спишется <strong>25 000</strong> руб.');?>
                </p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Приобрести'), [
                      'class' => 'btn btn-lg text-transform-upr btn-black font-size-12',
                      'value' => 2,
                      'name' => 'invest-type']) ?>
                <?php ActiveForm::end();?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
            </div>
          </div>
        </div>
      </div>
    <div class="col-xs-6 col-sm-3">
        <div class="pricing-box content-box">

                    <div class="ribbon">
                        <div class="bg-primary"><?php echo Yii::t('app', 'Акция');?></div>
                    </div>
        
            <h3 class="bg-black pricing-title"><?php echo Yii::t('app', 'СТАНДАРТ');?></h3>
            <?php $investPrefOpc = InvestPref::find()->where(['invest_type_id' => 3])->andWhere(['key' => InvestPref::KET_COUNT_AKCII])->one()?>
            <div class="bg-green pricing-specs">
                <span>50 000<sup>p</sup></span>
                <i><?php echo Yii::t('app', '{count} опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?></i>
            </div>
            <a href = "#"  class="descr-link" data-toggle="modal" data-target="#investDescr">
              <?php echo Yii::t('app', 'Подробнее о бонусах');?>
            </a>
            <div class="pad25A">
                <button type="button" class="btn btn-lg text-transform-upr btn-black font-size-12" id = "modalInvest3">
                    <?php echo Yii::t('app', 'Приобрести');?>
                </button>
            </div>
        </div>
    </div>

      <div class="modalInvest3 modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title"><?php echo Yii::t('app', 'СТАНДАРТ');?></h4>
            </div>
            <div class="modal-body" style = "text-align: center;">
                <p style = "font-size: 16px;">
                    <?php echo Yii::t('app', 'Вы приобретаете <strong>{count}</strong> опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?>
                </p>
                <p style = "font-size: 16px; margin-bottom: 20px;">
                    <?php echo Yii::t('app', 'С Вашего баланса спишется <strong>50 000</strong> руб.');?>
                </p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Приобрести'), [
                      'class' => 'btn btn-lg text-transform-upr btn-black font-size-12',
                      'value' => 3,
                      'name' => 'invest-type']) ?>
                <?php ActiveForm::end();?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
            </div>
          </div>
        </div>
      </div>
    <div class="col-xs-6 col-sm-3">
        <div class="pricing-box content-box">

                    <div class="ribbon">
                        <div class="bg-primary"><?php echo Yii::t('app', 'Акция');?></div>
                    </div>

            <h3 class="bg-black pricing-title"><?php echo Yii::t('app', 'ПРЕМИУМ');?></h3>
            <?php $investPrefOpc = InvestPref::find()->where(['invest_type_id' => 4])->andWhere(['key' => InvestPref::KET_COUNT_AKCII])->one()?>
            <div class="bg-blue pricing-specs">
                <span>150 000<sup>p</sup></span>
                <i><?php echo Yii::t('app', '{count} опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?></i>
            </div>
            <a href = "#"  class="descr-link" data-toggle="modal" data-target="#investDescr">
              <?php echo Yii::t('app', 'Подробнее о бонусах');?>
            </a>
            <div class="pad25A">
                <button type="button" class="btn btn-lg text-transform-upr btn-black font-size-12" id = "modalInvest4">
                    <?php echo Yii::t('app', 'Приобрести');?>
                </button>
            </div>
        </div>
    </div>

      <div class="modalInvest4 modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title"><?php echo Yii::t('app', 'ПРЕМИУМ');?></h4>
            </div>
            <div class="modal-body" style = "text-align: center;">
                <p style = "font-size: 16px;">
                    <?php echo Yii::t('app', 'Вы приобретаете <strong>{count}</strong> опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?>
                </p>
                <p style = "font-size: 16px; margin-bottom: 20px;">
                    <?php echo Yii::t('app', 'С Вашего баланса спишется <strong>150 000</strong> руб.');?>
                </p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Приобрести'), [
                      'class' => 'btn btn-lg text-transform-upr btn-black font-size-12',
                      'value' => 4,
                      'name' => 'invest-type']) ?>
                <?php ActiveForm::end();?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
            </div>
          </div>
        </div>
      </div>

</div>
<div class="row">
    <div class="col-xs-6 col-sm-4">
        <div class="pricing-box content-box">

                    <div class="ribbon">
                        <div class="bg-primary"><?php echo Yii::t('app', 'Акция');?></div>
                    </div>

            <h3 class="bg-black pricing-title"><?php echo Yii::t('app', 'VIP');?></h3>
            <?php $investPrefOpc = InvestPref::find()->where(['invest_type_id' => 5])->andWhere(['key' => InvestPref::KET_COUNT_AKCII])->one()?>
            <div class="bg-blue pricing-specs">
                <span>300 000<sup>p</sup></span>
                <i><?php echo Yii::t('app', '{count} опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?></i>
            </div>
            <a href = "#"  class="descr-link" data-toggle="modal" data-target="#investDescr">
              <?php echo Yii::t('app', 'Подробнее о бонусах');?>
            </a>
            <div class="pad25A">
                <button type="button" class="btn btn-lg text-transform-upr btn-black font-size-12" id = "modalInvest5">
                    <?php echo Yii::t('app', 'Приобрести');?>
                </button>
            </div>
        </div>
    </div>

      <div class="modalInvest5 modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title"><?php echo Yii::t('app', 'VIP');?></h4>
            </div>
            <div class="modal-body" style = "text-align: center;">
                <p style = "font-size: 16px;">
                    <?php echo Yii::t('app', 'Вы приобретаете <strong>{count}</strong> опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?>
                </p>
                <p style = "font-size: 16px; margin-bottom: 20px;">
                    <?php echo Yii::t('app', 'С Вашего баланса спишется <strong>300 000</strong> руб.');?>
                </p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Приобрести'), [
                      'class' => 'btn btn-lg text-transform-upr btn-black font-size-12',
                      'value' => 5,
                      'name' => 'invest-type']) ?>
                <?php ActiveForm::end();?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
            </div>
          </div>
        </div>
      </div>
    <div class="col-xs-6 col-sm-4">
        <div class="pricing-box content-box">
            <h3 class="bg-black pricing-title"><?php echo Yii::t('app', 'SUPER VIP');?></h3>
            <?php $investPrefOpc = InvestPref::find()->where(['invest_type_id' => 6])->andWhere(['key' => InvestPref::KET_COUNT_AKCII])->one()?>
            <div class="bg-green pricing-specs">
                <span>600 000<sup>p</sup></span>
                <i><?php echo Yii::t('app', '{count} опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?></i>
            </div>
            <a href = "#"  class="descr-link" data-toggle="modal" data-target="#investDescr">
              <?php echo Yii::t('app', 'Подробнее о бонусах');?>
            </a>
            <div class="pad25A">
                <button type="button" class="btn btn-lg text-transform-upr btn-black font-size-12" id = "modalInvest6">
                    <?php echo Yii::t('app', 'Приобрести');?>
                </button>
            </div>
        </div>
    </div>

      <div class="modalInvest6 modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title"><?php echo Yii::t('app', 'SUPER VIP');?></h4>
            </div>
            <div class="modal-body" style = "text-align: center;">
                <p style = "font-size: 16px;">
                    <?php echo Yii::t('app', 'Вы приобретаете <strong>{count}</strong> опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?>
                </p>
                <p style = "font-size: 16px; margin-bottom: 20px;">
                    <?php echo Yii::t('app', 'С Вашего баланса спишется <strong>600 000</strong> руб.');?>
                </p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Приобрести'), [
                      'class' => 'btn btn-lg text-transform-upr btn-black font-size-12',
                      'value' => 6,
                      'name' => 'invest-type']) ?>
                <?php ActiveForm::end();?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
            </div>
          </div>
        </div>
      </div>
    <div class="col-xs-6 col-sm-4">
        <div class="pricing-box content-box">
            <h3 class="bg-black pricing-title"><?php echo Yii::t('app', 'ЛЮКС');?></h3>
            <?php $investPrefOpc = InvestPref::find()->where(['invest_type_id' => 7])->andWhere(['key' => InvestPref::KET_COUNT_AKCII])->one()?>
            <div class="bg-blue pricing-specs">
                <span>1 200 000<sup>p</sup></span>
                <i><?php echo Yii::t('app', '{count} опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?></i>
            </div>
            <a href = "#"  class="descr-link" data-toggle="modal" data-target="#investDescr">
              <?php echo Yii::t('app', 'Подробнее о бонусах');?>
            </a>
            <div class="pad25A">
                <button type="button" class="btn btn-lg text-transform-upr btn-black font-size-12" id = "modalInvest7">
                    <?php echo Yii::t('app', 'Приобрести');?>
                </button>
            </div>
        </div>
    </div>

      <div class="modalInvest7 modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title"><?php echo Yii::t('app', 'PREMIUM');?></h4>
            </div>
            <div class="modal-body" style = "text-align: center;">
                <p style = "font-size: 16px;">
                    <?php echo Yii::t('app', 'Вы приобретаете <strong>{count}</strong> опционов', ['count' => number_format($investPrefOpc->value, 0, '', ' ')]);?>
                </p>
                <p style = "font-size: 16px; margin-bottom: 20px;">
                    <?php echo Yii::t('app', 'С Вашего баланса спишется <strong>1 200 000</strong> руб.');?>
                </p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Приобрести'), [
                      'class' => 'btn btn-lg text-transform-upr btn-black font-size-12',
                      'value' => 7,
                      'name' => 'invest-type']) ?>
                <?php ActiveForm::end();?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
            </div>
          </div>
        </div>
      </div>
</div>
<div class ="row">
    <div class="col-xs-12">
        <div class="bg-default content-box text-center pad20A mrg25T">
            <a href = "/office/onuserinvest" class="btn btn-lg btn-primary"><?php echo Yii::t('app', 'Посмотреть мои инвестиции');?></a>
        </div>
    </div>
</div>

<div id = "investDescr" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h4 class="modal-title" id="myLargeModalLabel"><?php echo Yii::t('app', 'Структурный бонус');?></h4> </div>
        <div class="modal-body">
            <div class="example-box-wrapper">
                <div class="scroll-columns">
                <table class="table table-bordered table-striped table-condensed cf">
                    <thead class="cf">
                    <tr>
                        <th><?php echo Yii::t('app', 'Уровень');?></th>
                        <th><?php echo Yii::t('app', 'Структурный бонус');?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>6</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>6-10</td>
                        <td>1.5</td>
                    </tr>
                    <tr>
                        <td>11-15</td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>16-21</td>
                        <td>0.5</td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    <div class="modal-footer" style="height: 55px;">
      <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
    </div>
  </div>
</div>
</div>

<?php $this->registerJs("
jQuery('.invest1').on('click', function(){
    jQuery('#invest1').modal('show');
})
jQuery('.invest2').on('click', function(){
    jQuery('#invest2').modal('show');
})
jQuery('.invest3').on('click', function(){
    jQuery('#invest3').modal('show');
})

", View::POS_END);?>

<div class="needFeelUserInfo modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title"><?php echo Yii::t('app', 'ВНИМАНИЕ !');?></h4>
      </div>
      <div class="modal-body" style = "text-align: center;">
          <p style = "font-size: 16px;">
              <?php echo Yii::t('app', 'Перед покупкой опционов вы должны полностью заполнить профиль, включая дату рождения <br/><a href = "/office/user">Перейти в профиль</a>');?>
          </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

<?php if (empty($user->first_name) || empty($user->phone) || empty($user->address) || empty($user->nationality) || empty($user->passport)):?>
  <?php $this->registerJs("

  $('#modalInvest1').click( function(){
    $('.needFeelUserInfo').modal('show');
  })

  $('#modalInvest2').click( function(){
    $('.needFeelUserInfo').modal('show');
  })

  $('#modalInvest3').click( function(){
    $('.needFeelUserInfo').modal('show');
  })

  $('#modalInvest4').click( function(){
    $('.needFeelUserInfo').modal('show');
  })

  $('#modalInvest5').click( function(){
    $('.needFeelUserInfo').modal('show');
  })

  $('#modalInvest6').click( function(){
    $('.needFeelUserInfo').modal('show');
  })

  $('#modalInvest7').click( function(){
    $('.needFeelUserInfo').modal('show');
  })


  ", View::POS_END);?>
<?php else:?>
  <?php $this->registerJs("

  $('#modalInvest1').click( function(){
    $('.modalInvest1').modal('show');
  })

  $('#modalInvest2').click( function(){
    $('.modalInvest2').modal('show');
  })

  $('#modalInvest3').click( function(){
    $('.modalInvest3').modal('show');
  })

  $('#modalInvest4').click( function(){
    $('.modalInvest4').modal('show');
  })

  $('#modalInvest5').click( function(){
    $('.modalInvest5').modal('show');
  })

  $('#modalInvest6').click( function(){
    $('.modalInvest6').modal('show');
  })

  $('#modalInvest7').click( function(){
    $('.modalInvest7').modal('show');
  })
  ", View::POS_END);?>
<?php endif;?>
