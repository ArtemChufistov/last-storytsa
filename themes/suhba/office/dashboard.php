<?php
use app\modules\finance\models\CryptoWallet;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyGraph;
use app\modules\finance\models\Payment;
use app\modules\matrix\models\Matrix;
use app\modules\finance\models\Transaction;
use app\modules\event\models\Event;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Панель управления');
$this->params['breadcrumbs'][] = $this->title;

$currencyVou = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();
$currencyFreez = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();

$vouBalance = $user->getBalances([$currencyVou->id])[$currencyVou->id];
$freezBalance = $user->getBalances([$currencyFreez->id])[$currencyFreez->id];

$userInfo = $user->initUserInfo();
?>


<div id="page-title">
    <h2><?php echo $this->title;?></h2>
    <p><?php echo Yii::t('user', 'Сводная информация по вашему аккаунту');?></p>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="tile-box tile-box-alt mrg20B bg-blue-alt">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Ваш баланс:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-suitcase"></i>
                <div class="tile-content">

                    <?php foreach ($user->getBalances() as $balance): ?>
                        <span>$</span>
                        <?php echo number_format($balance->value, 2, '.', ' '); ?> <?php echo $balance->getCurrency()->one()->title; ?>
                    <?php endforeach;?>

                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Ваш личный финансовый счёт');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/finance']);?>" class="tile-footer tooltip-button" data-placement="bottom" title="<?php echo Yii::t('app', 'Перейти в раздел финансы');?>">
                <?php echo Yii::t('app', 'Перейти в раздел финансы');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="tile-box tile-box-alt mrg20B bg-blue-alt">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Ваши опционы:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-money"></i>
                <div class="tile-content">
                    <?php echo empty($vouBalance) ? 0 : number_format($vouBalance->value, 2, '.', ' ');?>
                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Приобретённыее опционы');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/onuserinvest']);?>" class="tile-footer tooltip-button" data-placement="bottom" title="<?php echo Yii::t('app', 'Перейти в раздел опционов');?>">
                <?php echo Yii::t('app', 'Перейти в раздел опционов');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="tile-box tile-box-alt mrg20B bg-blue-alt">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Маркетинговый доход:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-heart"></i>
                <div class="tile-content">
                    <?php echo empty($userInfo->struct_sum) ? 0 : number_format($userInfo->struct_sum, 2, '.', ' ');?>
                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Доход со структуры');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/detailmarketing']);?>" class="tile-footer tooltip-button" data-placement="bottom" title="<?php echo Yii::t('app', 'Детализация расчётов');?>">
                <?php echo Yii::t('app', 'Просмотр детального расчёта');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
</div>
<div class = "row">
    <div class="col-md-4">
        <div class="tile-box tile-box-alt mrg20B bg-green">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Ваши партнёры:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-users"></i>
                <div class="tile-content">
                    <?php $descendants = $user->descendants()->all(); echo count($descendants);?>
                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Общее количество партнёров');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/struct']);?>" class="tile-footer tooltip-button" data-placement="bottom" title="<?php echo Yii::t('app', 'Перейти в раздел вашей структуры');?>">
                <?php echo Yii::t('app', 'Перейти в раздел вашей структуры');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="tile-box tile-box-alt mrg20B bg-green">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Промо материалы:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-bullhorn"></i>
                <div class="tile-content">
                    6
                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Рекламная информация для сайта');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/promo']);?>" class="tile-footer tooltip-button" data-placement="bottom" title="<?php echo Yii::t('app', 'Перейти в раздел промо материалов');?>">
                <?php echo Yii::t('app', 'Перейти в раздел промо материалов');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="tile-box tile-box-alt mrg20B bg-green">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Лидерские доли:');?>
            </div>
            <div class="tile-content-wrapper">
                <i class="glyph-icon icon-star"></i>
                <div class="tile-content">
                    <?php echo empty($userInfo->count_parts) ? 0 : $userInfo->count_parts;?>
                </div>
                <small>
                    <i class="glyph-icon icon-caret-up"></i>
                    <?php echo Yii::t('app', 'Ваши лидерские доли');?>
                </small>
            </div>
            <a href="<?php echo Url::to(['/profile/office/dashboard']);?>" class="tile-footer">
                <?php echo Yii::t('app', 'Доли компании от распределения 5% МТО');?>
                <i class="glyph-icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <i class="glyph-icon icon-cog"></i>
                <?php echo Yii::t('app', 'Обращение президента компании к акционерам');?>
            </h3>
            <div class="content-box-wrapper">
                <div id="data-example-3" style="width: 100%">
                    
<video style = "width: 100%;" controls>
<source src="/suhba/vypusk_001_dubl_2.mp4" type="video/mp4">
</video>

                    <br/>
                    <br/>
                    <div class="example-box-wrapper">
                        <a href="/office/invest" class="btn btn-lg btn-primary" title=""><?php echo Yii::t('app', 'Перейти к инвестированию');?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">

<div class="panel">
<div class="panel-body">
<h3 class="title-hero">
    <?php echo Yii::t('app', 'Последние 10 операций');?>
</h3>
<div class="example-box-wrapper">
<table id="datatable-tabletools" class="table table-striped table-bordered" cellspacing="0" width="100%">
<thead>
    <thead>
        <tr>
            <th><?php echo Yii::t('app', 'Тип');?></th>
            <th><?php echo Yii::t('app', 'Сумма');?></th>
            <th><?php echo Yii::t('app', 'Дата');?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach(Transaction::find()->where(['from_user_id' => $user->id])->orWHere(['to_user_id' => $user->id])->limit(10)->orderBy(['id' => SORT_DESC])->all() as $transaction):?>
            <?php $toUser = $transaction->getToUser()->one();?>
            <?php $fromUser = $transaction->getFromUser()->one();?>
            <tr>
                <td><?php echo $transaction->getTypeTitle();?></td>
                <td>
                    <?php if (!empty($toUser) && $toUser->id == $user->id):?>
                        <span class="badge bg-green">+<?php echo number_format($transaction->sum, 2, '.', ' ');?> <?php echo !empty($transaction->getCurrency()->one()) ? $transaction->getCurrency()->one()->title : '';?></span>
                    <?php else:?>
                        <span class="badge bg-red">-<?php echo number_format($transaction->sum, 2, '.', ' ');?>  <?php echo !empty($transaction->getCurrency()->one()) ? $transaction->getCurrency()->one()->title : '';?></span>
                    <?php endif;?>
                </td>
                <td><?php echo date("H-i-s d-m-y", strtotime($transaction->date_add));?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
</div>
</div>

</div>


    </div>
    <div class="col-md-4">

<div class="panel">
<div class="panel-body">
<h3 class="title-hero">
    <?php echo Yii::t('app', 'Посление <strong>10</strong> событий');?>
</h3>
<div class="example-box-wrapper">
<table id="datatable-tabletools" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th><?php echo Yii::t('app', 'Событие');?></th>
            <th><?php echo Yii::t('app', 'Логин');?></th>
            <th><?php echo Yii::t('app', 'Дата');?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach(Event::find()->where(['user_id' => $user->id])->limit(10)->orderBy(['date_add' => SORT_DESC])->all() as $event):?>
            <?php $toUser = $event->getToUser()->one();?>
            <?php if (!empty($toUser)):?>
                <tr>
                    <td><?php echo $event->getTypeTitle();?></td>
                    <td><?php echo $toUser->login;?></td>
                    <td><?php echo date("H-i-s d-m-y", strtotime($event->date_add));?></td>
                </tr>
            <?php endif;?>
        <?php endforeach;?>
    </tbody>
</table>
</div>
</div>
</div>


    </div>
</div>