<?php

use app\modules\profile\widgets\InviteFriendsByEmailWidget;
use app\modules\invest\models\UserInvest;
use app\modules\finance\models\Transaction;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Рабочая структура');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$userInvest = UserInvest::find()->where(['user_id' => $user->id])->one();

$selfInvestSum = UserInvest::find()->select('SUM(sum) as sum')->where(['user_id' => $user->id])->one();

$fulChildInvest = 0;
$mightLegSum = 0;
$children = [];
foreach($user->children()->all() as $child){
  $children[] = $child->id;
  $childInfo = $child->initUserInfo();

  if ($mightLegSum < $childInfo->struct_invest){
    $mightLegSum = $childInfo->struct_invest;
  }

  //$fulChildInvest += $childInfo->self_invest;
  $fulChildInvest += $childInfo->struct_invest;

}

$userInfo = $user->initUserInfo();

?>

<style type="text/css">
.stat-small-wrap .tile-box{
  margin-bottom: 15px;
}
</style>

<h2 style = "margin-bottom: 15px; font-size: 20px;"><?= $this->title;?></h2>

<?php if (empty($userInvest)):?>
	<div class="row">
	  <div class="col-lg-12">
	    <div class="panel">
	      <div class="panel-body">
	        <h3 class="title-hero" style = "height: 60px;">
	            <?php echo Yii::t('app', 'Структура активных участников'); ?>
	        </h3>
	        <div class="example-box-wrapper">
	          <?php echo Yii::t('app', 'Чтобы принимать участие в распределении начислений с рабочей структуры, Вам необходимо приобрести инвестиционный пакет'); ?><br/><br/><br/>
			    <a href="/office/userinvest" class="btn btn-lg btn-primary"><?php echo Yii::t('app', 'Приобрести инвестиционный пакет'); ?></a><br/>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
<?php else:?>

<div class="row">
  <div class="col-lg-6">
    <div class="panel">
      <div class="panel-body">
        <h3 class="title-hero" style = "height: 60px;">
            <?php echo Yii::t('app', 'Поиск партнёра по структуре'); ?>
        </h3>
        <div class="example-box-wrapper" style = "height: 83px;">
          <div class="form-group">
            <input type="input" class="form-control" id="userSearch" placeholder="<?php echo Yii::t('app', 'Введите для поиска');?>" value="">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/office/partial/_ref', ['user' => $user]); ?>
  </div>
</div>

<div class="row">
  <div class="col-lg-9">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div class="form-group">
            <div id="userTreeView" class=""></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 stat-small-wrap">
    <div class="tile-box bg-blue">
        <div class="tile-header">
          <?php echo Yii::t('app', 'Личные инвестиции');?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-user"></i>
            <div class="tile-content">
                <?php echo empty($selfInvestSum->sum) ? 0 : number_format($selfInvestSum->sum, 2, '.', ' ');?>
            </div>
        </div>
    </div>
    <div class="tile-box bg-green">
        <div class="tile-header">
          <?php echo Yii::t('app', 'Оборот первой линии');?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-heart"></i>
            <div class="tile-content">
                <?php echo empty($fulChildInvest) ? 0 : number_format($fulChildInvest, 2, '.', ' ');?>
            </div>
        </div>
    </div>
    <div class="tile-box bg-blue-alt">
        <div class="tile-header">
          <?php echo Yii::t('app', 'Оборот сильной ветки');?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-pie-chart"></i>
            <div class="tile-content">
                <?php echo number_format($mightLegSum, 2, '.', ' ');?>
            </div>
        </div>
    </div>
    <div class="tile-box bg-purple">
        <div class="tile-header">
          <?php echo Yii::t('app', 'Оборот всей структуры');?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-group"></i>
            <div class="tile-content">
                <?php echo empty($userInfo->struct_invest) ? 0 : number_format($userInfo->struct_invest, 2, '.', ' ');?>
            </div>
        </div>
    </div>
    <div class="tile-box bg-green">
        <div class="tile-header">
          <?php echo Yii::t('app', 'Всего лично приглашенных');?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-home"></i>
            <div class="tile-content">
                <?php echo count($children);?>
            </div>
        </div>
    </div>
    <div class="tile-box bg-blue">
        <div class="tile-header">
          <?php echo Yii::t('app', 'Всего человек в структуре');?>
        </div>
        <div class="tile-content-wrapper">
            <i class="glyph-icon icon-star"></i>
            <div class="tile-content">
                <?php echo count($user->descendants()->all());?>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg userInfoModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myLargeModalLabel"><?php echo Yii::t('app', 'Инвестиции пользователя');?></h4> </div>
      <div class="modal-body userInvestInfo">
        
      </div>
      <div class="modal-footer" style="height: 55px;">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs("

  function userInfo(userLogin){

    $.ajax({url: '/office/userinfo/' + userLogin, success: function(result){
      $('.userInvestInfo').html(result);
      $('.userInfoModal').modal('show');
    }});

  }

  var defaultData = " . json_encode($userTree) . "; 
  $(function() {
    var userTree = $('#userTreeView').treeview({
      data: defaultData,
      levels: 1,
      showTags: true,
      selectedColor: 'white',
      selectedBackColor: '#2ecc71',
      onNodeCollapsed: function(event, node) {
        console.log(111);
      },
      onNodeExpanded: function (event, node) {
        console.log(222);
      }
    });

    var findExpandibleNodess = function() {
      return expTree =  userTree.treeview('search', [ $('#userSearch').val(), { ignoreCase: false, exactMatch: false } ]);
    };

    var expandibleNodes = findExpandibleNodess();

    $('#userSearch').on('keyup', function (e) {
      userTree.treeview('collapseAll');
      expandibleNodes = findExpandibleNodess();
      $('.expand-node').prop('disabled', !(expandibleNodes.length >= 1));
    });
  });
", View::POS_END); ?>

<style type="text/css">
  .node-userTreeView .userInfo{
    display: inline-block;
  }
  .search-result{
    background-color: #f9f9f9;
  }
</style>

<?php endif;?>