
<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Payment;
use app\modules\finance\models\Currency;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = Yii::t('user', 'Финансы');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$currencyVou = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();
?>

<h2 style = "margin-bottom: 15px; font-size: 20px;"><?= $this->title;?></h2>

<div class="row">
  <div class="col-lg-6">
    <div class="panel">
      <div class="panel-body">
        <div class="content-box-wrapper text-center clearfix">
          <p><?php echo Yii::t('app', 'Для <strong>Пополнения/Снятия</strong> средств личного кабинета выберите соответствующее действие');?></p>
          <br/>
          <a href= "<?php echo Url::to(['/profile/office/in']);?>" class="btn btn-success pull-left inPayment" >
            <i class="fa fa-level-down"></i> <?php echo Yii::t('app', 'Пополнить личный счёт');?>
          </a>
          <a href= "<?php echo Url::to(['/profile/office/out']);?>" class="btn btn-success pull-right outPayment" >
            <i class="fa fa-level-up"></i> <?php echo Yii::t('app', 'Снять с личного счёта');?>
          </a>
        </div>
      </div>
    </div>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $paymentProvider,
        'filterModel' => $payment,
        'id' => 'payment-grid',
        'layout' => '
          <div class="panel">
            <h3 class="content-box-header bg-default">' . Yii::t('app', 'Ваши Пополнения/Снятия') . '</h3>
                <div class="table-responsive" style = "border-top: 1px solid #dfe8f1; border-bottom: 1px solid #dfe8f1;">
                  {items}
                </div>
                <div style = "padding-left: 25px;">{pager}</div>
          </div>
          ',
        'columns' => [
            [
              'attribute' => 'type',
              'format' => 'raw',
              'filter' => Select2::widget([
                  'name' => 'PaymentFinanceSearch[type]',
                  'data' => Payment::getFinanceTypeArray(),
                  'theme' => Select2::THEME_BOOTSTRAP,
                  'hideSearch' => true,
                  'options' => [
                      'placeholder' => Yii::t('app', 'Выберите тип'),
                      'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                  ]
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-type', ['model' => $model]);},
            ],[
              'attribute' => 'realSum',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payments-sum', ['model' => $model]);},
            ],[
              'attribute' => 'status',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-status', ['model' => $model]);},
              'filter' => Select2::widget([
                  'name' => 'PaymentFinanceSearch[status]',
                  'data' => Payment::getStatusArray(),
                  'theme' => Select2::THEME_BOOTSTRAP,
                  'hideSearch' => true,
                  'options' => [
                      'placeholder' => Yii::t('app', 'Выберите статус'),
                      'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                  ]
              ]),
            ],[
              'attribute' => 'date_add',
              'format' => 'raw',
              'filter' => true,
              'filter' => \yii\jui\DatePicker::widget([
                  'model'=>$payment,
                  'attribute'=>'date_add',
                  'options' => ['class' => 'form-control'],
                  'language' => 'ru',
                  'dateFormat' => 'dd-MM-yyyy',
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-date-add', ['model' => $model]);},
            ]
        ],
    ]); ?>
  <?php Pjax::end(); ?>

  </div>
  <div class="col-lg-6">
<?php if ( 1 != 1):?>
    <div class="content-box">
        <h3 class="content-box-header bg-default">
            <?php echo Yii::t('app', 'Перевод участнику');?>
        </h3>
        <div class="content-box-wrapper">
          <?php $form = ActiveForm::begin(['options' => ['class' => 'innerTransactionForm']]); ?>

          <div class ="form-group">
            <?= $form->field($usertransactionForm, 'to_login')->widget(Select2::classname(), [
                'initValueText' => $usertransactionForm->to_login,

                'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                'theme' => Select2::THEME_BOOTSTRAP,
                'pluginOptions' => [
                    'minimumInputLength' => 2,
                    'ajax' => [
                        'url' => Url::to(['/office/searhbylogin']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(user) { return user.login; }'),
                    'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                ],
            ])->label(Yii::t('app', 'Логин')); ?>
          </div>

          <div class ="form-group">
            <?= $form->field($usertransactionForm, 'sum')->textInput(['value' => '']); ?>
          </div>

          <div class ="form-group">
          <?php echo $form->field($usertransactionForm, 'currency_id')->widget(Select2::classname(), [
              'data' =>  ArrayHelper::map(array_merge($userCurrencyArray, [$currencyVou]), 'id', 'title'),
              'options' => ['placeholder' => Yii::t('app', 'Выберите валюту...')],
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ]);?>
          </div>

          <div class="form-group">
              <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-success']) ?>
          </div>

          <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php endif;?>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $transactionProvider,
            'filterModel' => $transaction,
            'id' => 'transaction-grid',
            'layout' => '
              <div class="panel">
                <h3 class="content-box-header bg-default">' . Yii::t('app', 'Движения по счёту') . '</h3>
                <a href = "/office/finance" class="btn btn-success" style = "margin: 25px;">' . Yii::t('app', 'Сбросить фильтр') . '</a>
                <div class="table-responsive" style = "border-top: 1px solid #dfe8f1; border-bottom: 1px solid #dfe8f1;">
                  {items}
                </div>
                <div style = "padding-left: 25px;">{pager}</div>
              </div>
              ',
            'columns' => [
                [
                  'attribute' => 'type',
                  'format' => 'raw',
                  'filter' => Select2::widget([
                      'name' => 'TransactionFinanceSearch[type]',
                      'data' => Transaction::getOfficeTypeArray(),
                      'theme' => Select2::THEME_BOOTSTRAP,
                      'hideSearch' => true,
                      'options' => [
                          'placeholder' => Yii::t('app', 'Выберите тип'),
                          'value' => isset($_GET['TransactionFinanceSearch[type]']) ? $_GET['TransactionFinanceSearch[type]'] : null
                      ]
                  ]),
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-type', ['model' => $model]);},
                ],[
                  'attribute' => 'data',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-data', ['model' => $model]);},
                ],[
                  'attribute' => 'sum',
                  'format' => 'raw',
                  'filter' => true,
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-sum', ['model' => $model]);},
                ],[
                  'attribute' => 'date_add',
                  'format' => 'raw',
                  'filter' => true,
                  'filter' => \yii\jui\DatePicker::widget([
                      'model'=>$transaction,
                      'attribute'=>'date_add',
                      'options' => ['class' => 'form-control'],
                      'language' => 'ru',
                      'dateFormat' => 'dd-MM-yyyy',
                  ]),
                  'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-date-add', ['model' => $model]);},
                ]
            ],
        ]); ?>
      <?php Pjax::end(); ?>

  </div>
</div>

<div class="modal fade bs-example-modal-lg submitTransactionModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myLargeModalLabel"><?php echo Yii::t('app', 'Внимание !');?></h4> </div>
      <div class="modal-body" style="padding: 0px;">
        <div style= "margin-top: 20px;margin-bottom:20px; text-align: center;">
          <?php echo Yii::t('app', 'Вы подтверждаете  отправку<strong class = "innerTransactionSum"></strong> <strong class = "innerTransactionCurrency">RUR</strong> пользователю <strong class = "innerTransactionLogin"></strong> ?<br/><br/> С вашего баланса спишется <strong class = "innerTransactionSum"></strong> <strong class = "innerTransactionCurrency">RUR</strong>');?>
          <br/>
          <br/>
          <button type="submit" class="btn btn-success iSubmitInnerTransaction"><?php echo Yii::t('app', 'Подтверждаю')?></button>
        </div>
      </div>
      <div class="modal-footer" style="height: 55px;">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs("

jQuery('.showPaymentInfo').on('click', function(){
  location.href = jQuery(this).attr('paymeninfourl');
})

var innerTrSubmit = false;

jQuery('.innerTransactionForm').on( 'beforeSubmit', function(event){
  console.log(innerTrSubmit);
  if (innerTrSubmit == true){
    return true;
  }else{
    jQuery('.innerTransactionSum').html($('#usertransactionform-sum').val());
    jQuery('.innerTransactionCurrency').html($('#select2-usertransactionform-currency_id-container').attr('title'));
    jQuery('.innerTransactionLogin').html($('#usertransactionform-to_login').val());
    jQuery('.submitTransactionModal').modal('show');
    return false;
  }
})

jQuery('.iSubmitInnerTransaction').on( 'click', function(){
  innerTrSubmit = true;
  jQuery('.innerTransactionForm').submit();
});

", View::POS_END);?>

<div class="needFeelUserInfo modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title"><?php echo Yii::t('app', 'ВНИМАНИЕ !');?></h4>
      </div>
      <div class="modal-body" style = "text-align: center;">
          <p style = "font-size: 16px;">
              <?php echo Yii::t('app', 'Перед Пополнением/Снятием вы должны полностью заполнить профиль, включая дату рождения <br/><a href = "/office/user">Перейти в профиль</a>');?>
          </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

<?php if (empty($user->first_name) || empty($user->phone) || empty($user->address) || empty($user->nationality) || empty($user->passport)):?>
  <?php $this->registerJs("

  $('.inPayment').click( function(){
    $('.needFeelUserInfo').modal('show');
    return false;
  })

  $('.outPayment').click( function(){
    $('.needFeelUserInfo').modal('show');
    return false;
  })
  ", View::POS_END);?>

<?php endif;?>