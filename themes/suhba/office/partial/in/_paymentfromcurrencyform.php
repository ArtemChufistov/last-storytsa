<?php
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Финансы - Пополнение личного счёта');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$btnColors = ['success', 'info', 'blue-alt', 'primary', 'purple', 'azure'];

?>

<div id="page-title">
  <h2><?php echo $this->title;?></h2>
  <p><?php echo Yii::t('app', 'Пополнение личного кабинета'); ?></p>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div id="form-wizard-3" class="form-wizard">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_tabs', [
	'active' => 2,
]); ?>
            <div class="tab-content">
              <div class="tab-pane active" id="step-2">
                <div class="content-box">
                  <h3 class="content-box-header bg-blue">
                      <?php echo Yii::t('app', 'Выберите валюту'); ?>
                  </h3>
                  <div class="content-box-wrapper" style = "text-align: center;">
                    <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

                      <?php foreach ($currencyArray as $currency): ?>
                        <?=Html::submitButton('<span>' . $currency->key . '</span><i class="glyph-icon icon-arrow-right"></i>', [
	'style' => 'margin: 15px;',
	'value' => $currency->id,
	'name' => StringHelper::basename(get_class($paymentForm)) . '[from_currency_id]',
	'class' => 'currencyButton btn btn-alt btn-hover btn-' . array_shift($btnColors)])?>
                      <?php endforeach;?>

                      <?=$form->errorSummary($paymentForm);?>

                    <?php ActiveForm::end();?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>