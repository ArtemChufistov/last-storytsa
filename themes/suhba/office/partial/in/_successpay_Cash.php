<div class="row">
    <div class="col-lg-3">
        <div class="dummy-logo">
            <img src = "/suhba/big_logo.png" style = "width: 170px;">
            <p style = "font-size: 14px;">SUHBA</p>
        </div>
    </div>
    <div class="col-lg-9 float-right text-right">
        <h4 class="invoice-title"><?php echo Yii::t('app', 'Оплата'); ?></h4>
        <br/>
        <b>№ <?php echo $paymentForm->hash; ?></b>

        <div class="divider"></div>
        <div class="invoice-date mrg20B"><?php echo Yii::t('app', 'от: '); ?><?php echo date('H:i d-m-Y', strtotime($paymentForm->date_add)); ?></div>
    </div>
</div>
<div class="row">
	<div class="col-lg-4">
	</div>
	<div class="col-lg-8">
		<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Информация'); ?>:</h2>
		<ul class="reset-ul">
			<li>
				<b><?php echo Yii::t('app', 'Сумма'); ?>:</b> <strong style = "font-size: 22px;"> <?php echo number_format($paymentForm->to_sum, 2, '.', ' '); ?> <?php echo $paymentForm->getToCurrency()->one()->title; ?></strong>
			</li>
		</ul>
		<br/>
		<br/>
		<strong style = "font-size: 20px;"><b><?php echo Yii::t('app', 'Ожидайте, в ближайшее время с Вами свяжется оператор'); ?></strong>
	</div>
</div>