<?php
use app\modules\finance\models\CurrencyCourse;
use app\modules\mainpage\models\Preference;

$prefSciEmail = Preference::find()->where(['key' => Preference::KEY_SCI_EMAIL])->one();
$prefSciName = Preference::find()->where(['key' => Preference::KEY_SCI_NAME])->one();
$prefSciSercet = Preference::find()->where(['key' => Preference::KEY_SCI_SECRET])->one();

$acSign =  
	$prefSciEmail->value . ':' . 
	$prefSciName->value . ':' . 
	($paymentForm->from_sum + $paymentForm->comission_from_sum) . ':' . 
	$paymentForm->getFromCurrency()->one()->title . ':' .
	$prefSciSercet->value . ':' .
	$paymentForm->hash;

$acSign = hash('sha256', $acSign);
?>
<div class="row">
    <div class="col-lg-3">
        <div class="dummy-logo">
            <img src = "/suhba/big_logo.png" style = "width: 170px;">
            <p style = "font-size: 14px;">SUHBA</p>
        </div>
    </div>
    <div class="col-lg-9 float-right text-right">
		<form method="POST" action="https://wallet.advcash.com/sci/">
	        <h4 class="invoice-title"><?php echo Yii::t('app', 'Оплата'); ?></h4>
	        <b>№ <?php echo $paymentForm->hash; ?></b>

	        <div class="divider"></div>
	        <div class="invoice-date mrg20B"><?php echo date('H:i d-m-Y', strtotime($paymentForm->date_add)); ?></div>

			<input type="hidden" name="ac_account_email" value="<?php echo $prefSciEmail->value;?>">
			<input type="hidden" name="ac_sci_name" value="<?php echo $prefSciName->value;?>">
			<input type="hidden" name="ac_amount" value="<?php echo $paymentForm->from_sum + $paymentForm->comission_from_sum; ?>">
			<input type="hidden" name="ac_currency" value="<?php echo $paymentForm->getFromCurrency()->one()->title; ?>">
			<input type="hidden" name="ac_order_id" value="<?php echo $paymentForm->hash; ?>">
			<input type="hidden" name="ac_sign" value="<?php echo $acSign; ?>">
			<input type="hidden" name="ac_comments" value="<?php echo Yii::t('app', 'Пополнение личного кабинета участник: {login}', ['login' => $user->login]) ?>">
			<input type="hidden" name="operation_id" value="<?php echo $paymentForm->hash; ?>">
			<input type="hidden" name="login" value="<?php echo $user->login;?>">

	        <button class="btn btn-alt btn-hover btn-info" type="submit">
	            <span><?php echo Yii::t('app', 'Перейти к оплате'); ?></span>
	            <i class="glyph-icon icon-play"></i>
	        </button>
	        <button class="btn btn-alt btn-hover btn-danger">
	            <span><?php echo Yii::t('app', 'Отменить'); ?></span>
	            <i class="glyph-icon icon-trash"></i>
	        </button>
		</form>
    </div>
</div>
<div class="row">
	<div class="col-lg-4">
	</div>
	<div class="col-lg-4">
		<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Информация'); ?>:</h2>
		<ul class="reset-ul">
			<li>
				<b><?php echo Yii::t('app', 'Сумма'); ?>:</b> <strong style = "font-size: 22px;"> <?php echo number_format($paymentForm->to_sum, 2, '.', ' '); ?> <?php echo $paymentForm->getToCurrency()->one()->title; ?></strong>
			</li>
			<li>
				<b><?php echo Yii::t('app', 'Платёжная система'); ?>:</b> <strong style = "font-size: 22px;"> <?php echo Yii::t('app', 'Advanced Cash'); ?></strong>
			</li>
		</ul>
	</div>
	<div class="col-lg-4">
	</div>
</div>
