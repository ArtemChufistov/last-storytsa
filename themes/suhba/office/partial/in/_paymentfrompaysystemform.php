<?php
use app\modules\finance\components\paysystem\FchangeComponent;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\Currency;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Финансы - Пополнение личного счёта');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$paysystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
$paysystemCash = PaySystem::find()->where(['key' => PaySystem::KEY_CASH])->one();
$paysystemAdvCash = PaySystem::find()->where(['key' => PaySystem::KEY_ADVCASH])->one();
$paysystemFchange = PaySystem::find()->where(['key' => PaySystem::KEY_FCHANGE])->one();

$fchangeCurrencyArray = FchangeComponent::getCurrencyCourseList();


?>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div id="form-wizard-3" class="form-wizard">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_tabs', [
            	'active' => 3,
            	'paymentForm' => $paymentForm,
            ]); ?>
            <div class="tab-content">
              <div class="tab-pane active" id="step-3">
                <div class="content-box">
                    <h3 class="content-box-header bg-yellow">
                      <?php echo Yii::t('app', 'Выбор платёжной системы'); ?>
                    </h3>
                    <div class="content-box-wrapper" style="min-height: 180px;">
                      <div class="form-group">
                        <div class="col-sm-12" style = "text-align: center;">

                          <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

                            <?php if (!empty(CurrencyCourse::find()
                                ->where(['from_pay_system_id' => $paysystemCash->id])
                                ->andWhere(['to_pay_system_id' => $paysystemLk->id])
                                ->andWhere(['from_currency' => $paymentForm->from_currency_id])
                                ->andWhere(['to_currency' => $paymentForm->to_currency_id])
                                ->andWhere(['active' => CurrencyCourse::ACTIVE_TRUE])
                                ->one())): ?>

                              <span style = "display: inline-table;" class="btn btn-lg btn-default paySysWrap" name="PaymentFromPaySystemForm[sub_pay_system]" data-toggle="modal" data-target="#modalCashInfo">
                                <img class="paySysIcon" src="/suhba/cash-in-hand.png" style = "width: 45px;">
                                <p><?php echo Yii::t('app', 'Наличные');?></p>
                              </span>
                            <?php endif;?>

                            <?php if (!empty(CurrencyCourse::find()
                                ->where(['from_pay_system_id' => $paysystemAdvCash->id])
                                ->andWhere(['to_pay_system_id' => $paysystemLk->id])
                                ->andWhere(['from_currency' => $paymentForm->from_currency_id])
                                ->andWhere(['to_currency' => $paymentForm->to_currency_id])
                                ->andWhere(['active' => CurrencyCourse::ACTIVE_TRUE])
                                ->one())): ?>

                                <?=Html::submitButton('
                                  <img class = "paySysIcon" src = "/suhba/advcash-app.png" style = "width: 32px;">
                                  <p>' . Yii::t('app', 'Advanced Cash') . '</p>
                                    ', ['value' => $paysystemAdvCash->id,
                                    'name' => StringHelper::basename(get_class($paymentForm)) . '[from_pay_system_id]',
                                    'style' => 'display: inline-table;',
                                    'class' => 'btn btn-lg btn-default paySysWrap'])?>
                            <?php endif;?>

                            <?php if (!empty(CurrencyCourse::find()
                                ->where(['from_pay_system_id' => $paysystemFchange->id])
                                ->andWhere(['to_pay_system_id' => $paysystemLk->id])
                                ->andWhere(['from_currency' => $paymentForm->from_currency_id])
                                ->andWhere(['to_currency' => $paymentForm->to_currency_id])
                                ->andWhere(['active' => CurrencyCourse::ACTIVE_TRUE])
                                ->one())): ?>

                              <?php $paySystemFinded = false;?>
                              <?php foreach ($fchangeCurrencyArray as $fchangeCurrency): ?>

                                <?php if($fchangeCurrency['send_paysys_identificator'] == 'ADVCUSD'){continue;};?>

                                <?php if (strtoupper($fchangeCurrency['send_paysys_valute']) == $paymentForm->getFromCurrency()->one()->key): ?>

                                  <?php $resultSum = $paymentForm->to_sum;?>

                                  <?php $paySystemFinded = true;?>

                                  <?php if ($resultSum > $fchangeCurrency['max_summ']): ?>

                                    <button class="btn btn-lg btn-default paySysWrap">
                                      <img class = "paySysIcon" src = "<?php echo $fchangeCurrency['send_paysys_icon']; ?>" style = "margin-bottom: -10px;">

                                      <p style = "color: red; font-size: 12px; line-height: 1.3;">
                                          <?php echo Yii::t('app', 'Макс. сумма<br/> для пополнения:<br/>'); ?> <?php echo $fchangeCurrency['max_summ']; ?>
                                          <?php echo strtoupper($fchangeCurrency['recive_paysys_valute']); ?>
                                      </p>
                                    </button>

                                  <?php elseif ($resultSum < $fchangeCurrency['min_summ']): ?>

                                    <button class="btn btn-lg btn-default paySysWrap">
                                      <img class = "paySysIcon" src = "<?php echo $fchangeCurrency['send_paysys_icon']; ?>" style = "margin-bottom: -10px;">

                                      <p style = "color: red; font-size: 12px; line-height: 1.3;">
                                          <?php echo Yii::t('app', 'Мин. сумма<br/> для пополнения:<br/>'); ?> <?php echo $fchangeCurrency['min_summ']; ?>
                                          <?php echo strtoupper($fchangeCurrency['recive_paysys_valute']); ?>
                                      </p>
                                    </button>

                                  <?php else: ?>

                                    <?=Html::submitButton('
                                      <img class = "paySysIcon" src = "' . $fchangeCurrency['send_paysys_icon'] . '">
                                      <p>' . $fchangeCurrency['send_paysys_title'] . '</p>
                                        ', ['value' => $fchangeCurrency['send_paysys_identificator'],
                                        'name' => StringHelper::basename(get_class($paymentForm)) . '[sub_pay_system]',
                                        'class' => 'btn btn-lg btn-default paySysWrap'])?>

                                  <?php endif;?>
                                <?php endif;?>
                              <?php endforeach;?>

                              <?php if ($paySystemFinded == false): ?>
                                <span class = "btn btn-lg btn-default paySysWrap">
                                  <p>
                                    <?php echo Yii::t('app', '<strong>Приносим извинения</strong></br>В данный момент расчёты в этой валюте закрыты.</br><a href = "{link}"> Выберите другую валюту<a>', ['link' => Url::to(['office/in', StringHelper::basename(get_class($paymentForm)) . '[to_sum]' => $paymentForm->to_sum])]); ?>
                                  </p>
                                </span>
                              <?php endif;?>

                            <?php endif;?>

                            <?= $form->field($paymentForm, 'from_pay_system_id')->hiddenInput(['value' => $paysystemFchange->id])->label('');?>

                          <?php ActiveForm::end();?>

                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modalCashInfo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo Yii::t('app', 'Укажите информацию о себе');?></h4>
      </div>
      <div class="modal-body">
        <?php $form = ActiveForm::begin(['method' => 'get']); ?>
          <div class="example-box-wrapper">
            <div class="row">
              <div class="col-md-12">
                  <?= $form->field($paymentForm, 'additional_info[name]', [
                      'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                      'labelOptions' => ['class' => 'col-sm-3 control-label']
                  ])->textInput([
                      'maxlength' => true,
                      'value' => $user->first_name,
                      'placeholder' => Yii::t('app', 'ФИО')
                  ])->label(Yii::t('app', 'ФИО')); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                  <?= $form->field($paymentForm, 'additional_info[address]', [
                      'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                      'labelOptions' => ['class' => 'col-sm-3 control-label']
                  ])->textInput([
                      'maxlength' => true,
                      'placeholder' => Yii::t('app', 'Адрес')
                  ])->label(Yii::t('app', 'Адрес')); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                  <?= $form->field($paymentForm, 'additional_info[phone]', [
                      'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                      'labelOptions' => ['class' => 'col-sm-3 control-label']
                  ])->textInput([
                      'maxlength' => true,
                      'value' => $user->phone,
                      'placeholder' => Yii::t('app', 'Phone')
                  ])->label(Yii::t('app', 'Телефон')); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                  <?= $form->field($paymentForm, 'additional_info[email]', [
                      'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                      'labelOptions' => ['class' => 'col-sm-3 control-label']
                  ])->textInput([
                      'maxlength' => true,
                      'value' => $user->email,
                      'placeholder' => Yii::t('app', 'E-mail')
                  ])->label(Yii::t('app', 'E-mail')); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                  <?= $form->field($paymentForm, 'additional_info[skype]', [
                      'template' => "{label}\n<div class='col-sm-8'>{input}</div>\n{hint}\n{error}",
                      'labelOptions' => ['class' => 'col-sm-3 control-label']
                  ])->textInput([
                      'maxlength' => true,
                      'value' => $user->skype,
                      'placeholder' => Yii::t('app', 'Skype')
                  ])->label(Yii::t('app', 'Skype')); ?>
              </div>

              <?= $form->field($paymentForm, 'from_pay_system_id')->hiddenInput(['value' => $paysystemCash->id])->label('');?>

            </div>
            <div class="row" style = "margin-top: 20px;">
              <div class="col-md-12">
                <?= Html::submitButton(Yii::t('app', 'Подтвердить'), [
                        'class' => 'btn btn-block btn-primary',
                        'name' => 'login-button']) ?>
              </div>
            </div>
            <div class="row" style = "margin-top: 20px;">
              <div class="col-md-12">
                <?php echo Yii::t('app', '<strong style = "color: red">Внимание!</strong> Будьте внимательны, указывая свою контактную информацию, для выдачи денежных средств с Вами свяжется оператор.');?>
              </div>
            </div>
          </div>
        <?php ActiveForm::end(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>

  </div>
</div>

<?php if ($paymentForm->from_pay_system_id == $paysystemCash->id && $paymentForm->hasErrors()):?>

<?php $this->registerJs("

jQuery(document).ready(function($) {
  $('#modalCashInfo').modal('show');
");?>

<?php endif;?>


<style type="text/css">
  .paySysIcon{
    margin: 10px;
  }
  .has-error .help-block{
    margin-top: 30px;
    text-align: center;
  }
  .paySysWrap{
    margin: 15px;
    height: 125px;
  }
</style>