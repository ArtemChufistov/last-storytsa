<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>

<div class="white-box">
  <div class="tab-content">
    <h3 class="box-title"><?php echo Yii::t('app', 'Ваш тикет');?></h3>
    <div class="tab-content">
      <div class="form-group">
        <label class="control-label" ><?php echo Yii::t('app', 'Отдел');?></label>
        <pre class="form-control" ><?php echo $currentTicket->getDepartment()->one()->name;?></pre>
      </div>
      <div class="form-group">
        <label class="control-label" ><?php echo Yii::t('app', 'Текст');?></label>
        <pre class="form-control" style = "height: 80px;"><?php echo $currentTicket->text;?></pre>
      </div>
    </div>
  </div>
</div>

<div class="white-box">
  <div class="tab-content">
    <h3 class="box-title"><?php echo Yii::t('app', 'Чат с поддержкой');?></h3>
    <div class="tab-content">
      <?php foreach($ticketMessages as $ticketMessageItem): ?>
        <div class="item">
          <img src="/<?php echo $ticketMessageItem->getUser()->one()->image;?>" alt="user image" class="offline">

          <p class="message">
            <a href="#" class="name">
              <?php echo $ticketMessageItem->getUser()->one()->login;?>
            </a>
            <?php echo $ticketMessageItem->text;?>
          </p>
        </div>
      <?php endforeach;?>

    <?php $form = ActiveForm::begin([
      'options' => [
          'class'=>'input-group support-message',
      ],
    ]); ?>

        <?= $form->field($ticketMessage, 'text')->textInput([
            'maxlength' => true,
            'placeholder' => $ticketMessage->getAttributeLabel('text')
        ]) ?>

      <div class="input-group-btn">
          <?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-success']) ?>
      </div>
      <?php ActiveForm::end();?>
      
    </div>
  </div>
</div>
