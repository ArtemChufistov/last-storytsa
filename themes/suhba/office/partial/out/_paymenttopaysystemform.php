<?php
use app\modules\finance\components\paysystem\FchangeComponent;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\Currency;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use yii\helpers\Html;

$this->title = Yii::t('user', 'Выбор платёжной системы');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$paysystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
$paysystemCash = PaySystem::find()->where(['key' => PaySystem::KEY_CASH])->one();
$paysystemAdvCash = PaySystem::find()->where(['key' => PaySystem::KEY_ADVCASH])->one();
$paysystemFchange = PaySystem::find()->where(['key' => PaySystem::KEY_FCHANGE])->one();
?>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div id="form-wizard-3" class="form-wizard">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_tabs', [
	'active' => 3,
	'paymentForm' => $paymentForm,
]); ?>
            <div class="tab-content">
              <div class="tab-pane active" id="step-3">
                <div class="content-box">
                    <h3 class="content-box-header bg-yellow">
                      <?php echo Yii::t('app', 'Выбор платёжной системы'); ?>
                    </h3>
                    <div class="content-box-wrapper">
                      <div class="form-group">
                        <div class="col-sm-12" style = "text-align: center;">
                          <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

                            <?php if (!empty(CurrencyCourse::find()
                                ->where(['from_pay_system_id' => $paysystemLk->id])
                                ->andWhere(['to_pay_system_id' => $paysystemCash->id])
                                ->andWhere(['from_currency' => $paymentForm->from_currency_id])
                                ->andWhere(['to_currency' => $paymentForm->to_currency_id])
                                ->andWhere(['active' => CurrencyCourse::ACTIVE_TRUE])
                                ->one())): ?>

                                    <?=Html::submitButton('
                                      <img class = "paySysIcon" src = "/suhba/cash-in-hand.png" style = "width: 32px;">
                                      <p>' . Yii::t('app', 'Наличные') . '</p>
                                        ', ['value' => $paysystemCash->id,
                                        'name' => StringHelper::basename(get_class($paymentForm)) . '[to_pay_system_id]',
                                        'class' => 'btn btn-lg btn-default paySysWrap'])?>
                            <?php endif;?>

                            <?php if (!empty(CurrencyCourse::find()
                                ->where(['from_pay_system_id' => $paysystemLk->id])
                                ->andWhere(['to_pay_system_id' => $paysystemAdvCash->id])
                                ->andWhere(['from_currency' => $paymentForm->from_currency_id])
                                ->andWhere(['to_currency' => $paymentForm->to_currency_id])
                                ->andWhere(['active' => CurrencyCourse::ACTIVE_TRUE])
                                ->one())): ?>

                                    <?=Html::submitButton('
                                      <img class = "paySysIcon" src = "/suhba/advcash-app.png" style = "width: 32px;">
                                      <p>' . Yii::t('app', 'Advanced Cash') . '</p>
                                        ', ['value' => $paysystemAdvCash->id,
                                        'name' => StringHelper::basename(get_class($paymentForm)) . '[to_pay_system_id]',
                                        'class' => 'btn btn-lg btn-default paySysWrap'])?>
                            <?php endif;?>


                          <?php ActiveForm::end();?>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  .paySysIcon{
    margin: 10px;
  }
  .paySysWrap{
    margin: 15px;
    height: 120px;
  }
</style>