<?php
use app\modules\finance\models\Currency;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Финансы - Снятие с личного счёта');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$defaultBuy = 0;
?>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div  class="form-wizard">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_tabs', [
	'active' => 1,
]); ?>
            <div class="tab-content">
              <div class="tab-pane active" id="step-1">
                <div class="content-box">
                  <h3 class="content-box-header bg-green">
                    <?php echo Yii::t('app', 'Укажите сумму'); ?>
                  </h3>
                  <div class="content-box-wrapper">

                    <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

                      <?=$form->field($paymentForm, 'from_sum', ['template' => '<label class="col-sm-3 control-label">' . Yii::t('app', 'Снимаемая сумма:') . '</label><div class="col-sm-4">{input}{error}</div>'])->textInput(['value' => $defaultBuy, 'class' => 'form-control summBuySitt'])?>

                      <div class="form-group">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-4">
                          <?=Html::submitButton(Yii::t('app', 'Далее'), ['class' => 'btn btn-lg  bg-green'])?>
                        </div>
                      </div>

                    <?php ActiveForm::end();?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs("
jQuery(function () {

  $('.summBuySitt').keyup(function(){
    reculcResUsd();
  })

  $('.bootstrap-touchspin-up').on('click', function(){
    reculcResUsd();
  })

  $('.bootstrap-touchspin-down').on('click', function(){
    reculcResUsd();
  })

  function reculcResUsd()
  {
    var sum = parseFloat($('.courseInUsd').attr('value') * $('.summBuySitt').val());
    $('.resultInUsd').html(sum);
  }
});
", View::POS_END);?>