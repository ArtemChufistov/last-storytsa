<?php
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;

$paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
$currencyCourse = CurrencyCourse::find()
	->where(['to_pay_system_id' => $paymentForm->to_pay_system_id])
	->andWhere(['from_pay_system_id' => $paySystem->id])
	->andWhere(['to_currency' => $paymentForm->to_currency_id])
	->one();
?>
<div class="row">
    <div class="col-lg-3">
        <div class="dummy-logo">
            <img src = "/suhba/big_logo.png" style = "width: 170px;">
            <p style = "font-size: 14px;">SUHBA</p>
        </div>
    </div>
    <div class="col-lg-9 float-left text-left">
		<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Информация'); ?>:</h2>
		<table>
			<tr>
				<td style = "width: 200px;"><b><?php echo Yii::t('app', 'Статус'); ?>:</b></td>
				<td><strong > <?php echo $paymentForm->getStatusTitle(); ?></strong></td>
			</tr>
			<tr>
				<td><b><?php echo Yii::t('app', 'Сумма'); ?>:</b></td>
				<td><strong> <?php echo number_format($paymentForm->from_sum, 2); ?> <?php echo $paymentForm->getFromCurrency()->one()->key;?></strong></td>
			</tr>
			<tr>
				<td colspan="2" style = "padding-top: 25px; font-size: 24px;"><?php echo Yii::t('app', 'Ожидайте получения средств на ваш кошелёк в ближайшее время'); ?></td>
			</tr>
		</table>
    </div>

</div>
