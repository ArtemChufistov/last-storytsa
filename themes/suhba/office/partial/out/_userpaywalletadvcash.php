<?php
use app\modules\finance\widgets\PayWalletPasswordWidget;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Финансы - Снятие с личного счёта');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

?>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div id="form-wizard-3" class="form-wizard">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_tabs', [
	'active' => 4,
	'paymentForm' => $paymentForm,
]); ?>
            <div class="tab-content">
              <div class="tab-pane active" id="step-4">

                <h3 style="text-align: center;"><?php echo Yii::t('app', 'Укажите ваш кошелёк <strong>Advanced Cash</strong>'); ?> RUB</h3><br/>

						<?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

						  <?=$form->field($paymentForm, 'address', ['template' => '<label class="col-sm-3 control-label">' . Yii::t('app', 'Кошелёк Advanced Cash:') . '</label><div class="col-sm-4">{input}{error}</div>'])->textInput(['value' => $paymentForm->address, 'class' => 'form-control', 'placeholder' => 'R123456789123'])?>

						  <?=$form->field($paymentForm, 'pay_password', ['template' =>
	'<label class="col-sm-3 control-label">' . Yii::t('app', 'Платёжный пароль:') . '</label>
						  <div class="col-sm-4">{input}{error}</div>', ])->textInput(['value' => $paymentForm->pay_password, 'class' => 'form-control'])?>

						  <div class="form-group">
						  	<div class="col-sm-3">
						  	</div>
						    <div class="col-sm-4">
						      <?=Html::submitButton(Yii::t('app', 'Далее'), ['class' => 'btn btn-info'])?>
						    </div>
						  </div>

						<?php ActiveForm::end();?>

						<?php echo PayWalletPasswordWidget::widget(['user' => $user]); ?>s


                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>