<?php
use app\modules\invest\models\UserInvest;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

$defaultImage = '/images/money_hand.png';

$userInvestArray = UserInvest::find()->where(['user_id' => $user->id])->all();
?>
<div class="example-box-wrapper">
    <div class="scroll-columns">
        <table class="table table-bordered table-striped table-condensed cf">
            <thead class="cf">
            <tr>
                <th><?php echo Yii::t('app', 'Дата');?></th>
                <th><?php echo Yii::t('app', 'Сумма');?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($userInvestArray as $userInvest): ?>
	            <tr>
	                <td><?php echo date('d-m-Y', strtotime($userInvest->date_add));?></td>
	                <td><?php echo number_format($userInvest->sum, 2, '.', ' ');?> <?php echo Yii::t('app', 'РУБ.');?></td>
	            </tr>
	        <?php endforeach;?>
           	</tbody>
        </table>
    </div>
</div>