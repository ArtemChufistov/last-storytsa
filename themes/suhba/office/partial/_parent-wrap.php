<?php $parentUser = $user->parent()->one(); ?>

<?php if (!empty($parentUser)): ?>
    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                <?php echo Yii::t('app', 'Ваш пригласитель'); ?>
            </h3>
            <div class="example-box-wrapper">
                <div class="row">
                    <div class="col-xs-4"><img src="<?php echo $parentUser->getImage(); ?>" alt="varun"
                                               class="img-circle img-responsive" style="width: 100px;"></div>
                    <div class="col-xs-8">
                        <h2 class="m-b-0">
                            <?php echo $parentUser->first_name; ?>
                        </h2>
                        <h4><?php echo $parentUser->login; ?></h4>
                    </div>
                </div>
            </div>
            <?php if (!empty($parentUser->phone)): ?>
                <hr class="m-t-10"/>
                <div class="text-center">
                    <h4 class="font-medium"><?php echo Yii::t('app', 'Телефон'); ?></h4>
                    <div class="row m-t-30"><?php echo $parentUser->phone; ?></div>
                </div>
            <?php endif; ?>
            <?php if (!empty($parentUser->phone)): ?>
                <hr class="m-t-10"/>
                <div class="text-center">
                    <h4 class="font-medium"><?php echo Yii::t('app', 'Skype'); ?></h4>
                    <div class="row m-t-30"><?php echo $parentUser->skype; ?></div>
                </div>
            <?php endif; ?>
        </div>
    </div>

<?php endif; ?>