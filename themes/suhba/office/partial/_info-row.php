<div class = "userInfo" >
	<img class="img-circle" width="50" src = "<?php echo $user->getImage();?>" style = "display: block; float: left;">
	<div style = "float: left; padding-left: 10px; margin-left: 10px; color: #333; font-size: 12px; border-left: 1px solid #ccc; height: 50px;">
		<strong><?php echo Yii::t('app','Логин:');?></strong> <?php echo $user->login;?><br/>
		<strong><?php echo Yii::t('app','Имя:');?></strong> <?php echo empty($user->first_name) ? Yii::t('app', 'Имя не указано') : $user->first_name;?>
	</div>
	<div style = "float: left; padding-left: 10px; margin-left: 10px; color: #333; font-size: 12px; border-left: 1px solid #ccc; height: 50px;">
		<strong><?php echo Yii::t('app','E-mail:');?></strong><?php echo $user->email;?><br/>
		<strong><?php echo Yii::t('app','Skype:');?></strong><?php echo empty($user->skype) ? Yii::t('app', 'Не указан') : $user->skype;?>
	</div>
	<div style = "float: left; padding-left: 10px; margin-left: 10px; color: #333; font-size: 12px; border-left: 1px solid #ccc; height: 50px;">
		<strong><?php echo Yii::t('app','Зарегистрирован:');?></strong> <?php echo date('d-m-Y', strtotime($user->created_at));?><br/>
		<strong><?php echo Yii::t('app','Последний раз заходил:');?></strong> <?php echo date('d-m-Y', strtotime($userInfo->online_date));?><br/>
	</div>
	<div style = "float: left; padding-left: 10px; margin-left: 10px; color: #333; font-size: 12px; border-left: 1px solid #ccc; height: 50px;">
		<strong><?php echo Yii::t('app','Личные инвестиции:');?></strong> <?php echo empty($userInfo->self_invest) ? 0 : number_format($userInfo->self_invest, 2, '.', ' ');?><br/>
		<strong><?php echo Yii::t('app','Инвестиции структуры:');?></strong> <?php echo empty($userInfo->struct_invest) ? 0 : number_format($userInfo->struct_invest, 2, '.', ' ');?><br/>
	</div>
</div>
