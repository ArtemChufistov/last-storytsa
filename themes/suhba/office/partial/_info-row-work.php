<div class = "userInfo" onclick = "userInfo('<?php echo $user->login;?>')">
	<img class="img-circle" width="50" src = "<?php echo $user->getImage();?>" style = "display: block; float: left;">
	<div style = "float: left; padding-left: 10px; margin-left: 10px; color: #333; font-size: 12px; border-left: 1px solid #ccc; height: 50px;">
		<strong><?php echo Yii::t('app','Логин:');?></strong> <?php echo $user->login;?><br/>
		<strong><?php echo Yii::t('app','Пригласитель:');?></strong> <?php echo $parent->login;?><br/>
		<strong><?php echo Yii::t('app','Адрес:');?></strong> <?php echo empty($user->address) ? Yii::t('app', 'Адрес не указан') : $user->address;?><br/>
	</div>
	<div style = "float: left; padding-left: 10px; margin-left: 10px; color: #333; font-size: 12px; border-left: 1px solid #ccc; height: 50px;">
		<strong><?php echo Yii::t('app','E-mail:');?></strong> <?php echo $user->email;?><br/>
		<strong><?php echo Yii::t('app','Skype:');?></strong> <?php echo empty($user->skype) ? Yii::t('app', 'Не указан') : $user->skype;?><br/>
		<strong><?php echo Yii::t('app','Телефон:');?></strong> <?php echo empty($user->phone) ? Yii::t('app', 'Не указан') : $user->phone;?>
	</div>
	<div style = "float: left; padding-left: 10px; margin-left: 10px; color: #333; font-size: 12px; border-left: 1px solid #ccc; height: 50px;">
		<strong><?php echo Yii::t('app','Пригласитель:');?></strong> <?php echo $parent->login;?><br/>
		<strong><?php echo Yii::t('app','ФИО:');?></strong> <?php echo $parent->first_name;?><br/>
		<strong><?php echo Yii::t('app','E-mail:');?></strong> <?php echo $parent->email;?><br/>
	</div>
	<div style = "float: left; padding-left: 10px; margin-left: 10px; color: #333; font-size: 12px; border-left: 1px solid #ccc; height: 50px;">
		<strong><?php echo Yii::t('app','Личный оборот (ЛО):');?></strong> <?php echo empty($userInfo->self_invest) ? 0 : number_format($userInfo->self_invest, 2, '.', ' ');?><br/>
		<strong><?php echo Yii::t('app','Групповой оборот (ГО):');?></strong> <?php echo empty($userInfo->struct_invest) ? 0 : number_format($userInfo->struct_invest, 2, '.', ' ');?><br/>
		<strong><?php echo Yii::t('app','Долей:');?></strong> <?php echo empty($userInfo->count_parts) ? 0 : $userInfo->count_parts;?>
	</div>
</div>
