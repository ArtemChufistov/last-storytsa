<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
?>
<div class="panel">
  <div class="panel-body">
    <h3 class="title-hero" style = "height: 40px;"><?php echo Yii::t('app', 'Вы можете пригласить своих друзей, указав их <strong>E-mail</strong> через запятую');?></h3>
    <div class="example-box-wrapper">

      <?php $form = ActiveForm::begin([
        'options' => [
          'style' => 'margin-bottom: 2px;'
        ]
      ]); ?>

      <div class = "form-group">
        <?= $form->field($inviteForm, 'separate_emails')->textInput(['class' => 'form-control', 'value' => '', 'placeholder' => Yii::t('app', 'Список E-mail через запятую')])->label(''); ?>
      </div>

      <?= Html::submitButton('<i class="glyphicon glyphicon-envelope"></i> '.Yii::t('app', 'Пригласить'), ['class' => 'btn btn-success waves-effect waves-light m-r-10']) ?>

      <?php ActiveForm::end();?>
    </div>

    <?php $successMessage = Yii::$app->session->getFlash('successInviteByEmail');?>

    <?php if (!empty($successMessage)):?>

    <div class="modal fade successModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div style = "width: 90%; font-weight: bold;" class="modal-title"><?php echo Yii::t('app', 'Внимание !');?></div>
            </div>
            <div class="modal-body">
            <?php echo $successMessage;?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
          </div>
        </div>
      </div>
    </div>

    <?php $this->registerJs("
        jQuery('.successModal').modal('show');
    ", View::POS_END);?>
  <?php endif;?>
  </div>
</div>