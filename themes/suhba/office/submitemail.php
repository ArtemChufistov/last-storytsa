<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'E-mail подтверждён');
?>

<style type="text/css">

    html, body {
        height: 100%;
    }

</style>

<div class="center-vertical bg-man-laptop">
    <div id="particles-js"></div>
    <div class="center-content row">
        <?php
        $form = ActiveForm::begin([
            'id' => 'loginform',
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{hint}\n{error}",
            ],
            'options' => [
                'class' => 'center-margin col-xs-11 col-sm-4'
            ]
        ]);
        ?>
        <h3 class="text-center pad25B font-gray font-size-23"><?= Html::encode($this->title) ?></h3>
        <div id="login-form" class="content-box">
            <div class="content-box-wrapper pad20A" style = "color:#8da0aa; text-align: center; font-size: 17px;">
              <?php echo Yii::t('app', 'Поздравляем вы успешно подтвердили свой <br/><strong>E-mail</strong><br/>');?>
              <p><?php echo Yii::t('app', 'Теперь Вам доступен полный функционал личного кабинета');?></p>
              </br>
              <a href = "<?php echo Url::to(['/profile/office/dashboard']);?>" style = "font-weight:bold; text-decoration: underline;">
                <?php echo Yii::t('app', 'Перейти в личный кабинет');?>
              </a>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
