<?php
use app\modules\event\models\Event;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Статистика');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<style type="text/css">
  .counter{
    font-size: 20px;
  }
</style>

<h2 style = "margin-bottom: 15px; font-size: 20px;"><?= $this->title;?></h2>

<div class="row">
    <div class="col-md-4 col-xs-4">
        <a href="#" title="Example tile shortcut" class="tile-box tile-box-alt btn-success" style = "margin-bottom: 20px;">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Участников в структуре'); ?>
            </div>
            <div class="tile-content-wrapper">
                <div class="chart-alt-10" data-percent="<?php echo $user->descendants()->count(); ?>"><span><?php echo $user->descendants()->count(); ?></span></div>
            </div>
        </a>
        <a href="#" title="Example tile shortcut" class="tile-box tile-box-alt btn-info" style = "margin-bottom: 20px;">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Личных партнёров'); ?>
            </div>
            <div class="tile-content-wrapper">
                <div class="chart-alt-10" data-percent="<?php echo $user->children()->count(); ?>"><span><?php echo $user->children()->count(); ?></span></div>
            </div>
        </a>
        <a href="#" title="Example tile shortcut" class="tile-box tile-box-alt btn-warning" style = "margin-bottom: 20px;">
            <div class="tile-header">
                <?php echo Yii::t('app', 'Переходов по <strong>REF</strong> ссылке'); ?>
            </div>
            <div class="tile-content-wrapper">
                <div class="chart-alt-10" data-percent="<?php echo $user->getEvents()->where(['type' => Event::TYPE_REF_LINK])->count(); ?>"><span><?php echo $user->getEvents()->where(['type' => Event::TYPE_REF_LINK])->count(); ?></span></div>
            </div>
        </a>
    </div>
    <div class="col-md-8 col-xs-8">
        <div class="panel">
            <div class="panel-body">
            <h3 class="title-hero"><?php echo Yii::t('app', 'События'); ?></h3>
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $eventProvider,
                'filterModel' => $eventModel,
                'layout' => '
                          <div class="box-header">
                            <div class="box-tools"><div class="pagination page-success pagination-sm no-margin pull-right">{pager}</div></div>
                          </div>
                         </div><div class="box-body no-padding eventTable">{items}</div>',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'type',
                        'format' => 'html',
                        'filter' => false,
                        'value' => function ($model) {
                            return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_event-type', ['model' => $model]);
                        },
                    ],
                    [
                        'attribute' => 'date_add',
                        'format' => 'html',
                        'filter' => false,
                        'value' => function ($model) {
                            return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_event-date', ['model' => $model]);
                        },
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
        </div>
    </div>
</div>