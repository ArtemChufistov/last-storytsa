<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\suhba\FrontAppAssetTop;
use app\assets\suhba\FrontAssetBottom;
use app\modules\menu\widgets\MenuWidget;

FrontAppAssetTop::register($this);
FrontAssetBottom::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>  
<html lang="en">
<head>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>

  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>

  <?php $this->head() ?>
    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>
    <script src="/js/particles.js-master/particles.js"></script>
</head>
<body>
<div id="loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
  <?php $this->beginBody() ?>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_dop_styles');?>

    <?php echo $content;?>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_particles');?>

  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>