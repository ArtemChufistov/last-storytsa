<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'] = [[
	'label' => $message,
	'url' => '/404',
],
];

$this->title = $message;
?>

<img src="/suhba/assets/image-resources/blurred-bg/blurred-bg-7.jpg" class="login-img wow fadeIn" alt="">

<div class="center-vertical">
    <div class="center-content row">

        <div class="col-md-6 center-margin">
            <div class="server-message wow bounceInDown inverse">
                <h1>Error 404</h1>
                <p><?=$message;?></p>

                <a href="/" class="btn btn-lg btn-success"><?php echo Yii::t('app', 'Вернуться на главную');?></a>

            </div>
        </div>

    </div>
</div>