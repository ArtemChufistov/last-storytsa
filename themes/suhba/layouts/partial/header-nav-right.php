<?php
use yii\helpers\Url;
?>
<div id="header-nav-right">
    <a href="#" class="hdr-btn" id="fullscreen-btn" title="Fullscreen">
        <i class="glyph-icon icon-arrows-alt"></i>
    </a>
    <div class="dropdown" id="notifications-btn">
        <a data-toggle="dropdown" href="#" title="">
            <span class="small-badge bg-yellow"></span>
            <i class="glyph-icon icon-linecons-params"></i>
        </a>
        <div class="dropdown-menu box-md float-right">
            <div class="popover-title display-block clearfix pad10A">
                <?php echo Yii::t('app', 'Быстрая навигация');?>
            </div>
            <div class="scrollable-content scrollable-slim-box">
                <ul class="no-border notifications-box">
                    <li>
                        <span class="bg-danger icon-notification glyph-icon icon-user"></span>
                        <span class="notification-text"><a href ="<?php echo Url::to(['/profile/office/user']);?>"><?php echo Yii::t('app', 'Профиль');?></a></span>
                    </li>
                    <li>
                        <span class="bg-info icon-notification glyph-icon icon-money"></span>
                        <span class="notification-text"><a href ="<?php echo Url::to(['/profile/office/finance']);?>"><?php echo Yii::t('app', 'Финансы');?></a></span>
                    </li>
                    <li>
                        <span class="bg-green icon-notification glyph-icon icon-dashboard"></span>
                        <span class="notification-text"><a href ="<?php echo Url::to(['/profile/office/dashboard']);?>"><?php echo Yii::t('app', 'Панель управления');?></a></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <a class="header-btn" id="logout-btn" href="/logout" title="<?php echo Yii::t('app', 'Выход');?>">
        <i class="glyph-icon icon-linecons-lock"></i>
    </a>
</div>