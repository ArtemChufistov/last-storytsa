<style type="text/css">

html, body {
    height: 100%;
}

.fa-youtube-play{
    cursor: pointer;
    color: black;
    margin: 0 auto;
    display: block;
    font-size: 65px;
    line-height: 340px;
    color: #64aa46;
}
.fa-youtube-play:hover{
    color: #76cb50;
}
.videoWrap{
    height: 340px;
    background-color: white;
    text-align: center;

}
#loginform, .videox{
    opacity: 0.95;
}
canvas {
  display: block;
  vertical-align: bottom;
}

#particles-js {
  position: absolute;
  width: 100%;
  height: 100%;

  background-size: cover;
  background-position: 50% 50%;
}
.input-group-addon{
    background-color: white;
}
.glyph-icon{
    color: #64aa46;
}
.btn{
    background-color: #64aa46;
}
.btn:hover{
    background-color: #76cb50;
}
.header-nav{
  display: block;
}
.sf-mega a{
  color: white;
}
.bg-man-laptop .main-header .container{
  max-width: 98%;
}
.input-group input{
  font-size: 14px;
}
</style>