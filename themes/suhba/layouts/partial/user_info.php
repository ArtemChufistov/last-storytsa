<?php
use yii\helpers\Html;
use yii\helpers\Url;

$user = $this->params['user']->identity;
?>
<style type="text/css">
.fioTitle{
    word-wrap: break-word;
    display: unset;
}

@media only screen and (min-width: 100px) and (max-width: 800px){
    .fioTitle{
        display: none !important;
    }
}
</style>
<div id="header-nav-left">
    <div class="user-account-btn dropdown">
        <a href="javascript:void(0)" title="My Account" class="user-profile clearfix" data-toggle="dropdown" >
            <img src="<?php echo $this->params['user']->identity->getImage(); ?>" alt="Profile image" width="28" class="img-circle">
            <span style = "width: auto;">
                <?= $this->params['user']->identity->login; ?>
            </span>
            <i class="glyph-icon icon-angle-down" style = "margin-left: 7px;"></i>
            <?php if (!empty($user->first_name)):?>
                <div class = "fioTitle">( <?php echo trim($user->first_name);?> )</div>
            <?php endif;?>
        </a>
        <div class="dropdown-menu float-left">
            <div class="box-sm">
                <div class="login-box clearfix">
                    <div class="user-img">

                        <img src="<?= $this->params['user']->identity->getImage(); ?>" alt="">
                    </div>
                    <div class="user-info">
                            <span>
                                <?= $this->params['user']->identity->login; ?>
                                <i>(<?php echo Yii::t('app', 'Баланс:');?>
                                    <?php foreach($user->getBalances() as $balance):?>
                                        <?php echo $balance->value;?> <?php echo $balance->getCurrency()->one()->key;?>
                                    <?php endforeach;?>
                                    )</i>
                            </span>
                        <a href="<?= Url::to(['/office/user']);?>" title="Edit profile"><?= Yii::t('app', 'Редактировать профиль');?></a>
                    </div>
                </div>
                <div class="divider"></div>
                <ul class="reset-ul mrg5B">
                    <li>
                        <a href="<?= Url::to(['/office/finance']);?>"><?php echo Yii::t('app', 'Финансы');?></a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['/office/stat']);?>">
                            <i class="glyph-icon float-right icon-caret-right"></i>
                            <?php echo Yii::t('app', 'Статистика');?>
                        </a>
                    </li>
                </ul>
                <div class="pad5A button-pane button-pane-alt text-center">
                    <a href="<?= Url::to(['logout']);?>" class="btn display-block font-normal btn-danger">
                        <i class="glyph-icon icon-power-off"></i>
                        <?= Yii::t('app', 'Выход');?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>