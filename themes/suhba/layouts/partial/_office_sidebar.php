<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div id="page-header" class="bg-black font-inverse">
    <?= Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_body_logo');?>
    <?= Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/user_info');?>
    <?= Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/left_sidebar');?>
</div>