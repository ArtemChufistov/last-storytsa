<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div id="mobile-navigation">
    <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
    <a href="<?php echo Url::to(['/office/dashboard']);?>" class="logo-content-small" title="Suhba"></a>
</div>
<div id="header-logo" style="background-color: white;">
    <a href="<?php echo Url::to(['/office/dashboard']);?>" class="logo-content-big" title="Suhba">
        Suhba <i>S</i>
        <span>The perfect invest</span>
    </a>
    <a href="<?php echo Url::to(['/office/dashboard']);?>" class="logo-content-small" title="Suhba">
        Suhba <i>S</i>
        <span>The perfect invest</span>
    </a>
    <a id="close-sidebar" href="#" title="Close sidebar">
        <i class="glyph-icon icon-angle-left"></i>
    </a>
</div>

<!--<div class="top-left-part">-->
<!--    <a class="logo" href="--><?php //echo Url::to(['/office/dashboard']);?><!--">-->
<!--        <b>-->
<!--            <img src="/family/images/admin-logo.png" alt="home" class="dark-logo" />-->
<!--            <img src="/family/images/admin-logo-dark.png" alt="home" class="light-logo" />-->
<!--        </b>-->
<!--        <span class="hidden-xs">-->
<!--            <img src="/family/images/admin-text.png" alt="home" class="dark-logo" />-->
<!--            <strong style = "color: black; font-size: 20px;">Family</strong>-->
<!--        </span> -->
<!--     </a>-->
<!--</div>-->