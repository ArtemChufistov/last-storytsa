<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\suhba\FrontAppAssetTop;
use app\assets\suhba\FrontAssetBottom;

FrontAppAssetTop::register($this);
FrontAssetBottom::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>  
<html lang="en">
<head>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>

  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>

  <?php $this->head() ?>
  <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>

</head>
<body>
  <?php $this->beginBody() ?>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_dop_styles');?>

    <?php echo $content;?>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_particles');?>

  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>