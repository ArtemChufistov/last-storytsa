<div class="main-header bg-header wow fadeInDown animated animated" style="visibility: visible;">
  <div class="container">
    <a href="/" class="header-logo"></a>
    <ul class="header-nav collapse" style = "border-width: 0px;">
      <?php foreach($menu->children()->all() as $num => $children):?>
        <li class="sf-mega">
            <a href="<?= $children->link;?>" title="<?= $children->title;?>" target = "_blank">
                <?= $children->title;?>
            </a>
        </li>
      <?php endforeach;?>
    </ul>
  </div>
</div>

