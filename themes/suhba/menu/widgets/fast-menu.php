<?php
use app\modules\lang\widgets\WLang;
use yii\helpers\Url;
$colorMenuArray = ['info', 'danger', 'purple', 'azure', 'yellow', 'warning'];
?>
<a href="#" class="hdr-btn" id="fullscreen-btn" title="<?php echo Yii::t('app', 'Во весь экран'); ?>">
    <i class="glyph-icon icon-arrows-alt"></i>
</a>

<a class="header-btn" href="<?php echo Url::to(['office/dashboard']); ?>" title="<?php echo Yii::t('app', 'Статистика CITT'); ?>">
    <i class="glyph-icon icon-linecons-star"></i>
</a>

<?=WLang::widget(['view' => '_lang-office-choser']);?>

<div class="dropdown" id="dashnav-btn">
    <a href="#" data-toggle="dropdown" data-placement="bottom" class="popover-button-header tooltip-button" title="<?php echo Yii::t('app', 'Навигация'); ?>">
        <i class="glyph-icon icon-linecons-cog"></i>
    </a>
    <div class="dropdown-menu float-right">
        <div class="box-sm">
            <div class="pad5T pad5B pad10L pad10R dashboard-buttons clearfix">
                <?php foreach ($menu->children($menu->depth + 1)->all() as $num => $child): ?>
                    <?php $randColorNum = array_rand($colorMenuArray, 1);?>
                    <a href="<?=Url::to([$child->link]);?>" class="btn vertical-button remove-border btn-<?php echo $colorMenuArray[$randColorNum]; ?>" title="">
                        <?=$child->icon;?>
                        <?=str_replace(' ', '</br>', $child->title);?>
                    </a>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
