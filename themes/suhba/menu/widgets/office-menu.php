<?php
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;

$matrix   = Matrix::find()->where(['user_id' => $this->params['user']->id])->one();

$user = $this->params['user'];

?>
<style type="text/css">
#sidebar-menu li a{
    color: #3c3c3c;
}
#page-sidebar ul li a .glyph-icon{
    color: #3c3c3c !important;
}
#sidebar-menu li a:hover{
    color: #999999;
}
#sidebar-menu .active{
    border: 1px solid #dfe8f1;
}
</style>

<div id="page-sidebar">
    <div class="scroll-sidebar">
        <ul id="sidebar-menu">
            <li class="header"><span>
                        <span class="fa-fw open-close">
                            <i class="ti-menu hidden-xs"></i>
                            <i class="ti-close visible-xs"></i>
                        </span>
                        <span class="hide-menu"><?php echo Yii::t('app', 'Личный кабинет');?></span>
                </span></li>

            <?php foreach ($menu->children($menu->depth + 1)->all() as $num => $child): ?>
                <li class="divider"></li>
                <li>

                    <a href="<?=Url::to([$child->link]);?>" class="waves-effect <?php if (Url::to([$requestUrl]) == Url::to([$child->link])):?>active<?php endif;?>">
                        <?php echo $child->icon; ?>
                        <span><?php echo $child->title?> <span class="fa arrow"></span></span>
                    </a>
                </li>
            <?php endforeach;?>

        </ul><!-- #sidebar-menu -->
    </div>
</div>
