<div class=" container-fluid bg-white">
	<div class="card card-transparent">
	  <div class="card-block">
	    <div class="table-responsive">
	      <table class="table table-hover" id="basicTable">
	        <tbody>
	          <tr>
	            <td class="v-align-middle ">
	              <?php echo Yii::t('app', 'Сумма:');?>
	            </td>
	            <td>
	              <?php echo $payment->to_sum; ?> <?php echo $payment->getToCurrency()->one()->key; ?>
	            </td>
	          </tr>
	          <tr>
	            <td class="v-align-middle ">
	              <?php echo Yii::t('app', 'Кошелёк Bitcoin');?>
	            </td>
	            <td>
	              <?php echo $payment->getToUserWallet()->in_wallet;?>
	            </td>
	          </tr>
	        </tbody>
	      </table>
	    </div>
	  </div>
	</div>
</div>