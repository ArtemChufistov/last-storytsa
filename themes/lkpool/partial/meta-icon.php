<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="apple-touch-icon" href="/lkpool/pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="/lkpool/pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="/lkpool/pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/lkpool/pages/ico/152.png">
<link rel="icon" type="image/x-icon" href="/lkpool/img/favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">