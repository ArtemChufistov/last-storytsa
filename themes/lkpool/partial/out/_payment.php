<?php
use app\modules\finance\models\Currency;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/partial/out/_tabs', ['active' => 4]); ?>

<div class="tab-content">
    <div class="tab-pane padding-20 sm-no-padding active slide-left" id="tab1">
      <div class="row row-same-height">

        <div class=" container-fluid   container-fixed-lg">
                    <!-- START card -->
          <div class="card card-default m-t-20">
            <div class="card-block">
              <div class="invoice padding-50 sm-padding-10">
                <div>
                  <div class="pull-left">
                    <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="/lkpool/img/invoice/squarespace2x.png" data-src="/lkpool/img/invoice/squarespace2x.png" src="/lkpool/img/invoice/squarespace2x.png">
                  </div>
                  <div class="pull-right sm-m-t-20">
                    <h2 class="font-montserrat all-caps hint-text"><?php echo Yii::t('app', 'Выплата для вас');?></h2>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <br>
                <br>
                <div class="col-12">
                  <div class="row">
                    <div class="col-lg-12 col-sm-height sm-no-padding">
                      <address>
                          <strong><?php echo Yii::t('app', 'Информация');?></strong><br/>
                          <?php echo Yii::t('app', 'Для пополнения личного счёта, Вам необходимо перести указанную сумму на соответствующий кошелёк');?>
                      </address>
                    </div>
                  </div>
                </div>
                <br>
                <br>
                <div class="p-l-15 p-r-15">
                  <div class="row b-a b-grey">
                    <div class="col-md-6 p-l-15 sm-p-t-15 clearfix sm-p-b-15 d-flex flex-column justify-content-center">
                      <h5 class="font-montserrat all-caps small no-margin hint-text bold"><?php echo Yii::t('app', 'Сумма');?></h5>
                      <h3 class="no-margin"><?php echo $paymentForm->to_sum;?> <?php echo $paymentForm->getToCurrency()->one()->key; ?></h3>
                    </div>

                    <div class="col-md-6 text-right bg-master-darker col-sm-height padding-15 d-flex flex-column justify-content-center align-items-end">
                      <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold"><?php echo Yii::t('app', 'Кошелёк Bitcoin');?></h5>
                      <h3 class="no-margin" style = "color: white;"><?php echo $paymentForm->wallet;?></h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>

    <div class="padding-20 sm-padding-5 sm-m-b-20 sm-m-t-20 bg-white clearfix">
      <ul class="pager wizard no-style">
        <li class="next">
          <a class="btn btn-primary btn-cons btn-animated from-left fa fa-btc pull-right" href = "/office/finance">
            <span><?php echo Yii::t('app', 'В раздел Финансы');?></span>
          </a>
        </li>
        <li class="previous">
          <a class="btn btn-default btn-cons pull-right" href = "/office/out">
            <span><?php echo Yii::t('app', 'Назад');?></span>
          </a>
        </li>
      </ul>
    </div>
</div>