<?php
use yii\web\View;
use yii\helpers\Url;
?>

<div class="header ">
  <!-- START MOBILE SIDEBAR TOGGLE -->
  <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
  </a>
  <!-- END MOBILE SIDEBAR TOGGLE -->
  <div class="">
    <div class="brand inline   ">
      <span style = "color: black;">MANO</span> <span style = "color: #584b8d;">FUND</span>
    </div>
  </div>
  <div class="d-flex align-items-center">
    <!-- START User Info-->
    <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down">
      <span class="semi-bold"><?php echo $this->params['user']->identity->login; ?></span>
    </div>
    <div class="dropdown pull-right hidden-md-down">
      <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="thumbnail-wrapper d32 circular inline">
        <img src="<?php echo $this->params['user']->identity->getImage();?>" alt="" data-src="<?php echo $this->params['user']->identity->getImage();?>" data-src-retina="<?php echo $this->params['user']->identity->getImage();?>" width="32" height="32">
        </span>
      </button>
      <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
        <a href="<?php echo Url::to(['/office/user']);?>" class="dropdown-item"><i class="pg-settings_small"></i><?php echo Yii::t('app', 'Личные данные');?></a>
        <a href="<?php echo Url::to(['/office/finance']);?>" class="dropdown-item"><i class="fa fa-usd"></i> <?php echo Yii::t('app', 'Финансы');?></a>
        <a href="<?php echo Url::to(['/office/invest']);?>" class="dropdown-item"><i class="pg-outdent"></i> <?php echo Yii::t('app', 'Мастер ноды');?></a>
        <a href="<?php echo Url::to(['/logout']);?>" class="clearfix bg-master-lighter dropdown-item">
          <span class="pull-left"><?php echo Yii::t('app', 'Выход');?></span>
          <span class="pull-right"><i class="pg-power"></i></span>
        </a>
      </div>
    </div>
    <!-- END User Info-->
    <a href="#" class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a>
  </div>
</div>