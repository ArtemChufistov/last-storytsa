  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/office/dashboard" ><?php echo Yii::t('app', 'Панель управления');?></a></li>
    <?php foreach($breadcrumbs as $numBreadcrumb => $breadcrumb):?>
    	<?php if ($numBreadcrumb < count($breadcrumbs) - 1):?>
    		<li class="breadcrumb-item">
    			<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['title'];?></a>
    		</li>
    	<?php else: ?>
    		<li class="breadcrumb-item active"><?php echo $breadcrumb['title'];?></li>
    	<?php endif;?>
	<?php endforeach;?>
  </ol>