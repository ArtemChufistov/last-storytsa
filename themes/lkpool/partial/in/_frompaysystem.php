<?php
use app\modules\finance\models\Currency;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/partial/in/_tabs', ['active' => 2,]); ?>

<div class="tab-content">
  <?php $form = ActiveForm::begin(['method' => 'get']);?>
    <div class="tab-pane padding-20 sm-no-padding active slide-left" id="tab1">
      <div class="row row-same-height">



      </div>
    </div>

    <div class="padding-20 sm-padding-5 sm-m-b-20 sm-m-t-20 bg-white clearfix">
      <ul class="pager wizard no-style">
        <li class="next">
          <?=Html::submitButton('<span>' . Yii::t('app', 'Далее') . '</span>', ['class' => 'btn btn-primary btn-cons btn-animated from-left fa fa-btc pull-right'])?>
        </li>
        <li class="previous">
          <a class="btn btn-default btn-cons pull-right" href = "/office/in">
            <span><?php echo Yii::t('app', 'Назад');?></span>
          </a>
        </li>
      </ul>
    </div>
  <?php ActiveForm::end();?>
</div>