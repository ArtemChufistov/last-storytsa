<ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm" role="tablist" data-init-reponsive-tabs="dropdownfx">
  <li class="nav-item">
    <a class="<?php if (in_array($active, [1])): ?>active<?php endif;?>"  href="/office/in" role="tab">
      <i class="fa fa-balance-scale tab-icon"></i>
      <span><?php echo Yii::t('app', 'Ввод суммы'); ?></span>
    </a>
  </li>
<?php /*
  <li class="nav-item">
    <a class="<?php if (in_array($active, [2])): ?>active<?php endif;?>"  href="/office/in" role="tab">
      <i class="fa fa-btc tab-icon"></i>
      <span><?php echo Yii::t('app', 'Выбор валюты');?></span>
    </a>
  </li>
  <li class="nav-item">
    <a class="<?php if (in_array($active, [3])): ?>active<?php endif;?>"  href="/office/in" role="tab">
      <i class="fa fa-credit-card tab-icon"></i>
      <span><?php echo Yii::t('app', 'Выбор платёжной системы');?></span>
    </a>
  </li>
*/ ?>
  <li class="nav-item">
    <a class="<?php if (in_array($active, [4])): ?>active<?php endif;?>"  href="/office/in" role="tab">
      <i class="fa fa-check tab-icon"></i>
      <span><?php echo Yii::t('app', 'Оплата');?></span>
    </a>
  </li>
</ul>