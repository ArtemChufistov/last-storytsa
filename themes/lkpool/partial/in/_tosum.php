<?php
use app\modules\finance\models\Currency;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/partial/in/_tabs', ['active' => 1,]); ?>

<div class="tab-content">
  <?php $form = ActiveForm::begin(['method' => 'get']);?>
    <div class="tab-pane padding-20 sm-no-padding active slide-left" id="tab1">
      <div class="row row-same-height">
        <div class="col-lg-4">
          <div class="padding-30 sm-padding-5">
            <div class="form-group-attached">
              <div class="form-group form-group-default">
                <?=$form->field($paymentForm, 'to_sum')->textInput(['class' => 'form-control summBuySitt', 'value' => 0.1])?>
              </div>
            </div>
          </div>
        </div>
      </div>

    <div class="padding-20 sm-padding-5 sm-m-b-20 sm-m-t-20 bg-white clearfix">
      <ul class="pager wizard no-style">
        <li class="previous">
          <a href = "/office/finance" class="btn btn-default btn-cons pull-left" type="button">
            <span><?php echo Yii::t('app', 'Назад');?></span>
          </a>
        </li>
        <li class="next">
          <?=Html::submitButton('<span>' . Yii::t('app', 'Далее') . '</span>', ['class' => 'btn btn-primary btn-cons btn-animated from-left fa fa-btc pull-left'])?>
        </li>
      </ul>
    </div>
  </div>
  <?php ActiveForm::end();?>
</div>