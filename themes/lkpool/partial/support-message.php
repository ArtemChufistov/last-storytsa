<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>


<style type="text/css">
  .view-port.push-parrallax > .view:last-child{
    transform: translate3d(0%, 0, 0);
  }
</style>

  <div class="card card-transparent">
    <div class="card-block no-padding">
      <div id="card-advance" class="card card-default">
        <div class="card-header  separator">
          <div class="card-title"><?php echo Yii::t('app', 'Тикет №{id}', ['id' => $currentTicket->id]);?> - 
            <?php echo Yii::t('app', 'Чат с');?>
            <?php if ($currentTicket->getDepartment()->one()->id == 1):?>
              <?php echo Yii::t('app', 'поддержкой');?>
            <?php elseif($currentTicket->getDepartment()->one()->id == 2):?>
              <?php echo Yii::t('app', 'администрацией');?>
            <?php endif;?>
          </div>
          <div class="card-controls">
            <ul>
              <li><a href="#" class="card-close" data-toggle="close"><i class="fa fa-comment-o"></i></a>
              </li>
            </ul>
          </div>
        </div>
        <div class="card-block">
        <br/>
          <div class="form-group">
            <label class="control-label" ><?php echo Yii::t('app', 'Текст');?></label>
            <pre class="form-control" style = "height: 80px;"><?php echo $currentTicket->text;?></pre>
          </div>
          <div class="tab-pane active no-padding" id="quickview-chat">
            <div class="view-port clearfix push-parrallax" id="chat">
              <div class="view chat-view bg-white clearfix">
                <div class="chat-inner" id="my-conversation">
                  <?php foreach(array_reverse($ticketMessages) as $ticketMessageItem): ?>
                    <?php if ($ticketMessageItem->getUser()->one()->id != $user->id):?>
                      <div class="message clearfix">
                        <div class="profile-img-wrapper m-t-5 inline">
                          <img class="col-top" width="30" height="30" src="<?php echo $ticketMessageItem->getUser()->one()->getImage();?>" alt="" data-src="<?php echo $ticketMessageItem->getUser()->one()->getImage();?>" data-src-retina="<?php echo $ticketMessageItem->getUser()->one()->getImage();?>">
                        </div>
                        <div class="chat-bubble from-them">
                          <?php echo $ticketMessageItem->text;?>
                        </div>
                      </div>
                    <?php else:?>
                      <div class="message clearfix">
                        <div class="chat-bubble from-me">
                          <?php echo $ticketMessageItem->text;?>
                        </div>
                      </div>
                    <?php endif;?>
                  <?php endforeach;?>
                </div>
              </div>
            </div>
          </div>

          <?php $form = ActiveForm::begin([

          ]); ?>

              <?= $form->field($ticketMessage, 'text')->textInput([
                  'maxlength' => true,
                  'value' => '',
                  'placeholder' => $ticketMessage->getAttributeLabel('text')
              ]) ?>

            <div class="input-group-btn" style = "padding-top:25px;">
                <?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end();?>

        </div>
      </div>

  </div>
  
</div>

<script type="text/javascript">

$( document ).ready(function() {
  $(".chat-list").animate({ scrollTop: $(".chat-list")[0].scrollHeight}, 1000);
});
</script>