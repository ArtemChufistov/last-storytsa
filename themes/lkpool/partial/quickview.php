<?php
use app\modules\finance\models\Currency;

$user = $this->params['user']->identity;
$currencyBTC = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
$balanceBTC  = $user->getBalances([$currencyBTC->id])[$currencyBTC->id];
?>

<div id="quickview" class="quickview-wrapper" data-pages="quickview">
  <ul class="nav nav-tabs">
    <li class="active" data-target="#quickview-alerts" data-toggle="tab">
      <a href="#"><?php echo Yii::t('app', 'Сводная статистика');?></a>
    </li>
  </ul>
  <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
  <div class="tab-content">
    <div class="tab-pane active no-padding" id="quickview-alerts">
      <div class="view-port clearfix" id="alerts">
        <div class="view bg-white">
          <div class="navbar navbar-default navbar-sm">
            <div class="navbar-inner">
              <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                <i class="pg-more"></i>
              </a>
              <div class="view-heading">
                <?php echo Yii::t('app', 'Баланс : {sum} BTC', ['sum' => number_format($balanceBTC->value, 8)]);?>
              </div>

            </div>
          </div>
          <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
            <div class="list-view-group-container">
              <div class="list-view-group-header text-uppercase">
                <?php echo Yii::t('app', 'Приобретённые ноды');?>
              </div>
              <ul>
                <li class="alert-list">
                  <a href="javascript:;" class="align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <p class="">
                      <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                    </p>
                    <p class="p-l-10 overflow-ellipsis fs-12">
                      <span class="text-master"><?php echo Yii::t('app', 'Megamaster node');?></span>
                    </p>
                    <p class="p-r-10 ml-auto fs-12 text-right">
                      <span class="text-warning"><?php echo Yii::t('app', '564 Мегахэш');?> <br></span>
                      <span class="text-master">10.10.2017</span>
                    </p>
                  </a>
                </li>
                <li class="alert-list">
                  <a href="#" class="align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <p class="">
                      <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                    </p>
                    <p class="p-l-10 overflow-ellipsis fs-12">
                      <span class="text-master"><?php echo Yii::t('app', 'MonsterHasher node');?></span>
                    </p>
                    <p class="p-r-10 ml-auto fs-12 text-right">
                      <span class="text-warning"><?php echo Yii::t('app', '124 Мегахэш');?> <br></span>
                      <span class="text-master">10.02.2018</span>
                    </p>
                  </a>
                </li>
              </ul>
            </div>
            <div class="list-view-group-container">
              <div class="list-view-group-header text-uppercase">
                <?php echo Yii::t('app', 'Топовые ноды в продаже');?>
              </div>
              <ul>
                <li class="alert-list">
                  <a href="javascript:;" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <p class="">
                      <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                    </p>
                    <p class="col overflow-ellipsis fs-12 p-l-10">
                      <span class="text-master"><?php echo Yii::t('app', 'HashNight node');?></span>
                      <span class="text-master"><?php echo Yii::t('app', '431 Мегахэш - 0.0034 BTC');?></span>
                    </p>
                  </a>
                </li>
                <li class="alert-list">
                  <a href="javascript:;" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <p class="">
                      <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                    </p>
                    <p class="col overflow-ellipsis fs-12 p-l-10">
                      <span class="text-master"><?php echo Yii::t('app', 'HashMiner node');?></span>
                      <span class="text-master"><?php echo Yii::t('app', '12 Мегахэш - 0.0034 BTC');?></span>
                    </p>
                  </a>
                </li>
              </ul>
            </div>
            <div class="list-view-group-container">
              <div class="list-view-group-header text-uppercase">
                <?php echo Yii::t('app', 'Действия');?>
              </div>
              <ul>
                <li class="alert-list">
                  <a href="/office/in" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <p class="">
                      <span class="text-success fs-10"><i class="fa fa-circle"></i></span>
                    </p>
                    <p class="col overflow-ellipsis fs-12 p-l-10">
                      <?php echo Yii::t('app', 'Пополнить баланс');?>
                    </p>
                  </a>
                </li>
                <li>
                  <a href="/office/out" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <p class="">
                      <span class="text-success fs-10"><i class="fa fa-circle"></i></span>
                    </p>
                    <p class="col overflow-ellipsis fs-12 p-l-10">
                      <?php echo Yii::t('app', 'Снять с баланса');?>
                    </p>
                  </a>
                </li>
                <li>
                  <a href="/invest" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <p class="">
                      <span class="text-success fs-10"><i class="fa fa-circle"></i></span>
                    </p>
                    <p class="col overflow-ellipsis fs-12 p-l-10">
                      <?php echo Yii::t('app', 'Приобрести ноду');?>
                    </p>
                  </a>
                </li>
                <li>
                  <a href="/support" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <p class="">
                      <span class="text-success fs-10"><i class="fa fa-circle"></i></span>
                    </p>
                    <p class="col overflow-ellipsis fs-12 p-l-10">
                      <?php echo Yii::t('app', 'Поддержка');?>
                    </p>
                  </a>
                </li>
                <li>
                  <a href="/logout" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <p class="">
                      <span class="text-success fs-10"><i class="fa fa-circle"></i></span>
                    </p>
                    <p class="col overflow-ellipsis fs-12 p-l-10">
                      <?php echo Yii::t('app', 'Выход');?>
                    </p>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>