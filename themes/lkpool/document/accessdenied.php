<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Доступ запрещён');
?>


<div class="login-wrapper ">
  <div class="bg-pic">
    <div id="particles-js">
    </div>
      <img src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src-retina="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" alt="" class="lazy" style = "width: 100%;">
  </div>
</div>

<div class="errorModal modal fade slide-up disable-scroll" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content-wrapper">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
        <div class="col-xs-12 text-center" style = "text-align: center;">
          <?= Yii::t('app', 'Вам необходимо подтвердить свой E-mail<br/> перейти в 
            <a style = "font-weight: bold;" href = "/office/dashboard">личный кабинет</a>');?>
          <br/>
          <br/>
          <a href="/" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40"><?php echo Yii::t('app', 'Вернуться на главную');?></a>
        </div>
      </div>

    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function() {
  $('.errorModal').modal('show');
});
</script>