<?php
use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use lowbase\user\components\AuthChoice;
use app\modules\mainpage\models\Preference;

$this->title = Yii::t('user', 'Вход на сайт');

?>

<style type="text/css">
  #signupform-licenseagree{
    position: absolute;
  }
</style>

<div class="login-wrapper ">
  <div class="bg-pic">
    <div id="particles-js">
    </div>
      <img src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src-retina="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" alt="" class="lazy">
    <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
      <h2 class="semi-bold text-white">
    <?php echo Yii::t('app', 'MANO FUND проффесиональный инвестиции');?>
    </h2>
      <p class="small">
        <?php echo Yii::t('app', 'Мы создаём революционные решения в сфере крипто инвестирования, гарантирующие постоянный стабильный доход');?>
      </p>
    </div>
  </div>

  <div class="login-container bg-white">
    <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
      <h2>MANO <span style = "color: #584b8d;">FUND</span></h2>
      <p><?php echo Yii::t('app', 'Регистрация');?></p>

      <?php $form = ActiveForm::begin([
          'id' => 'form-login',
          'fieldConfig' => [
              'template' => "{input}\n{hint}\n{error}",
          ],
          'options' => [
              'class' => 'p-t-15',
              'role' => 'form'
          ]
        ]); ?>

        <?php $readonlyPref = Preference::find()->where(['key' => Preference::KEY_USER_REG_CAN_PARENT_EDIT])->one()->value == 1 ? false : true;?>


        <div class="form-group form-group-default">
          <label><?php echo Yii::t('app', 'Логин');?></label>
          <div class="controls">
            <?= $form->field($model, 'login')->textInput([
                'maxlength' => true,
                'required' => "required",
                'placeholder' => Yii::t('app', 'Введите ваш логин'),
                'id' => 'login-user-name',
            ]); ?>
          </div>
        </div>

        <div class="form-group form-group-default">
          <label><?php echo Yii::t('app', 'E-mail');?></label>
          <div class="controls">
            <?= $form->field($model, 'email')->textInput([
                'maxlength' => true,
                'required' => "required",
                'placeholder' => Yii::t('app', 'Введите ваш E-mail'),
                'id' => 'email-user-name',
            ]); ?>
          </div>
        </div>

        <div class="form-group form-group-default">
          <label><?php echo Yii::t('app', 'Пароль');?></label>
          <div class="controls">
            <?= $form->field($model, 'password')->textInput([
                'maxlength' => true,
                'required' => "required",
                'type' => 'password',
                'placeholder' => Yii::t('app', 'Введите ваш пароль'),
            ]); ?>
          </div>
        </div>

        <div class="form-group form-group-default">
          <label><?php echo Yii::t('app', 'Повтор пароля');?></label>
          <div class="controls">
            <?= $form->field($model, 'passwordRepeat')->textInput([
                'maxlength' => true,
                'required' => "required",
                'type' => 'password',
                'placeholder' => Yii::t('app', 'Повторите пароль'),
            ]); ?>
          </div>
        </div>

        <div class="form-group form-group-default">
            <div class="col-xs-12">
                <div class="checkbox checkbox-primary pull-left p-t-0" style = "margin-top: 0px; margin-bottom: 0px;">
                    <?php $labelLicense = Yii::t('app', 'Я согласен с условиями') . ' ' . Html::a(Yii::t('app', 'лицензионного соглашения'), ['/licenseagreement'], ['target' => '_blank']);?>

                    <?= $form->field($model, 'licenseAgree', ['template' => '{input}{label}{error}', 'options' => ['tag' => false, 'required' => "required",]])->checkBox([], false)->label($labelLicense,['class'=>'label-text']);?>
                </div>
            </div>
        </div>

        <div class="form-group form-group-default">
          <div class = "col-xs-12">
              <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
                  'imageOptions' => [
                      'id' => 'my-captcha-image',
                  ],
                  'captchaAction' => '/profile/profile/captcha',
                  'options' => [
                      'class' => 'form-control',
                      'placeholder' => $model->getAttributeLabel('captcha'),
                      'required' => "required",
                  ],
                  'template' => '
                  <div class="row">
                    <div class="col-lg-8">{input}</div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">{image}</div>
                  </div>',
              ]);
              ?>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12 no-padding sm-p-l-10">
            <a href="/contact" class="text-info small"><?php echo Yii::t('app', 'Нужна помощь? свяжитесь с нами');?></a>
          </div>
        </div>

        <button class="btn btn-primary btn-cons m-t-10" type="submit"><?php echo Yii::t('app', 'Зарегистрироваться');?></button>
        <a href = "/login" class="btn btn-primary btn-cons m-t-10"><?php echo Yii::t('app', 'Вход в ЛК');?></a>

      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>