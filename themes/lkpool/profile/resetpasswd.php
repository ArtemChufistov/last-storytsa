<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Вход на сайт');
?>

<div class="login-wrapper ">
	<div class="bg-pic">
    <div id="particles-js">
    </div>
    	<img src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src-retina="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" alt="" class="lazy">
    <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
      <h2 class="semi-bold text-white">
		<?php echo Yii::t('app', 'MANO FUND проффесиональный инвестиции');?>
	  </h2>
      <p class="small">
        <?php echo Yii::t('app', 'Мы создаём революционные решения в сфере крипто инвестирования, гарантирующие постоянный стабильный доход');?>
      </p>
    </div>
	</div>

  <div class="login-container bg-white">
    <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
      <h2>MANO <span style = "color: #584b8d;">FUND</span></h2>
      <p class="p-t-35"><?php echo Yii::t('app', 'Восстановление пароля');?></p>

      <?php $form = ActiveForm::begin([
          'id' => 'form-login',
          'fieldConfig' => [
              'template' => "{input}\n{hint}\n{error}",
          ],
          'options' => [
              'class' => 'p-t-15',
              'role' => 'form'
          ]
        ]); ?>

        <div class="form-group form-group-default">
          <label><?php echo Yii::t('app', 'Логин');?></label>
          <div class="controls">
            <?= $form->field($forget, 'email')->textInput([
              'maxlength' => true,
              'required' => "required",
              'placeholder' => $forget->getAttributeLabel('email'),
              'class' => 'form-control'
            ]); ?>
          </div>
        </div>

        <div class="form-group form-group-default">
          <label><?php echo Yii::t('app', 'Пароль');?></label>
          <div class="controls">
            <?= $form->field($forget, 'password')->textInput([
              'maxlength' => true,
              'required' => "required",
              'placeholder' => $forget->getAttributeLabel('password'),
              'class' => 'form-control'
            ]); ?>
          </div>
        </div>

        <div class="form-group">
          <div class="col-xs-12">
            <?= $form->field($forget, 'captcha')->widget(Captcha::className(), [
                'captchaAction' => '/profile/profile/captcha',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => $forget->getAttributeLabel('captcha')
                ]
            ]);
            ?>
          </div>
        </div>

        <button class="btn btn-primary btn-cons m-t-10" type="submit"><?php echo Yii::t('app', 'Восстановить доступ');?></button>
        <a href = "/" class="btn btn-primary btn-cons m-t-10"><?php echo Yii::t('app', 'Перейти на главную');?></a>

      <?php ActiveForm::end(); ?>

      <div class="pull-bottom sm-pull-bottom">
        <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
          <div class="col-sm-3 col-md-2 no-padding">
            <img alt="" class="m-t-5" data-src="assets/img/demo/pages_icon.png" data-src-retina="/lkpool/img/demo/pages_icon_2x.png" height="60" src="assets/img/demo/pages_icon.png" width="60">
          </div>
          <div class="col-sm-9 no-padding m-t-10">
            <p>
              <small>
      				  <?php echo Yii::t('app', 'Создавая аккаунт вы соглашаетесь с');?> <a href="" class="text-info"><?php echo Yii::t('app', 'договором офферты');?></a>
      			  </small>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>