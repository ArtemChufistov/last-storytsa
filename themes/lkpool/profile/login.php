<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Вход на сайт');
?>

<div class="login-wrapper ">
	<div class="bg-pic">
    <div id="particles-js">
    </div>
    	<img src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src-retina="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" alt="" class="lazy">
    <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
      <h2 class="semi-bold text-white">
		<?php echo Yii::t('app', 'MANO FUND проффесиональный инвестиции');?>
	  </h2>
      <p class="small">
        <?php echo Yii::t('app', 'Мы создаём революционные решения в сфере крипто инвестирования, гарантирующие постоянный стабильный доход');?>
      </p>
    </div>
	</div>

  <div class="login-container bg-white">
    <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
      <h2>MANO <span style = "color: #584b8d;">FUND</span></h2>
      <p class="p-t-35"><?php echo Yii::t('app', 'Вход в личный кабинет');?></p>

      <?php $form = ActiveForm::begin([
          'id' => 'form-login',
          'fieldConfig' => [
              'template' => "{input}\n{hint}\n{error}",
          ],
          'options' => [
              'class' => 'p-t-15',
              'role' => 'form'
          ]
        ]); ?>

        <div class="form-group form-group-default">
          <label><?php echo Yii::t('app', 'Логин');?></label>
          <div class="controls">
            <?= $form->field($model, 'loginOrEmail')->textInput([
              'maxlength' => true,
              'required' => "required",
              'placeholder' => Yii::t('app', 'Укажите ваш Логин или E-mail'),
              'class' => 'form-control'
            ]); ?>
          </div>
        </div>

        <div class="form-group form-group-default">
          <label><?php echo Yii::t('app', 'Пароль');?></label>
          <div class="controls">
            <?= $form->field($model, 'password')->textInput([
              'maxlength' => true,
              'required' => "required",
              'placeholder' => Yii::t('app', 'Укажите ваш пароль'),
              'class' => 'form-control'
            ]); ?>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 no-padding sm-p-l-10">
            <div class="checkbox ">
              <input type="checkbox" value="1" id="checkbox1">
              <label for="checkbox1"><?php echo Yii::t('app', 'Я гость');?></label>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-center justify-content-end">
            <a href="/resetpasswd" class="text-info small"><?php echo Yii::t('app', 'Забыли пароль?');?></a>
          </div>
        </div>

        <button class="btn btn-primary btn-cons m-t-10" type="submit"><?php echo Yii::t('app', 'Вход');?></button>
        <a href = "/signup" class="btn btn-primary btn-cons m-t-10"><?php echo Yii::t('app', 'Регистрация');?></a>

      <?php ActiveForm::end(); ?>

      <div class="pull-bottom sm-pull-bottom">
        <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
          <div class="col-sm-9 no-padding m-t-10">
            <p>
              <small>
      				  <?php echo Yii::t('app', 'Создавая аккаунт вы соглашаетесь с');?> <a href="" class="text-info"><?php echo Yii::t('app', 'договором офферты');?></a>
      			  </small>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>