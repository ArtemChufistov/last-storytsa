<?php
use yii\web\View;
use yii\helpers\Url;
?>

<nav class="page-sidebar" data-pages="sidebar">
  <div class="sidebar-header">
    <h2><span style = "color: white;">MANO</span> <span style = "color: #584b8d;">FUND</span></h2>
    <div class="sidebar-header-controls">
      <button type="button" class="btn btn-link hidden-md-down" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
      </button>
    </div>
  </div>
  <div class="sidebar-menu">
    <ul class="menu-items">
      <li class="m-t-30 ">
      </li>
      <?php foreach ($menu->children(1)->all() as $num => $child): ?>
        <?php $children = $child->children(2)->all();?>
        <li class="<?php if (Url::to([$requestUrl]) == Url::to([$child->link])):?>active<?php endif;?>">
          <a href="<?php if (empty($children)){ echo Url::to([$child->link]); }else{ echo 'javascript:;'; };?>">
            <span class="title"><?php echo $child->title?></span>
            <?php if (!empty($children)):?>
                <span class=" arrow"></span><?php echo $child->icon; ?>
                <ul class="sub-menu">
                  <?php foreach ($children as $num => $childChild): ?>
                    <li class="">
                      <a href="calendar.html"><?php echo $childChild->title; ?></a>
                      <?php echo $childChild->icon; ?>
                    </li>
                  <?php endforeach;?>
                </ul>
            <?php endif;?>
          </a>
          <?php if (empty($children)):?><span class="icon-thumbnail"><?php echo $child->icon; ?></span><?php endif;?>
        </li>
      <?php endforeach;?>
    </ul>
    <div class="clearfix"></div>
  </div>
</nav>