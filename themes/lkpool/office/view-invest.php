<?php
use app\modules\mainpage\models\Preference;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Payment;
use app\modules\finance\models\Currency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;
use app\modules\invest\models\search\InvestTypeNodeSearch;
use app\modules\invest\components\marketing\LkpoolMarketing;
use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;

$this->title = Yii::t('user', 'Мои мастер ноды');
$this->params['breadcrumbs'][] = $this->title;

$investTypeNodeSearch = new InvestTypeNodeSearch;
$investTypeNodeProvider = $investTypeNodeSearch->search(Yii::$app->request->queryParams);
?>

<div class="page-content-wrapper ">
  <div class="content">
    <div class="container-fluid  container-fixed-lg" >

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/office-breadcrumbs', [
          'breadcrumbs' => [
          	  ['link' => Url::to('/office/invest'), 'title' => Yii::t('app', 'Покупка мастер ноды')],
              ['link' => Url::to('/office/viewinvest'), 'title' => $this->title]
          ]
      ]);?>

		<div class=" container-fluid   container-fixed-lg bg-white">
            <!-- START card -->
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title"><?php echo Yii::t('app', 'Купленные мастер ноды');?>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-block">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                  <thead>
                    <tr>
                      <th><?php echo Yii::t('app', 'Название ноды');?></th>
                      <th><?php echo Yii::t('app', 'Оплаченая сумма');?></th>
                      <th><?php echo Yii::t('app', 'Долей');?></th>
                      <th><?php echo Yii::t('app', 'Статус');?></th>
                      <th><?php echo Yii::t('app', 'Цена ноды');?></th>
                      <th><?php echo Yii::t('app', 'Дата покупки');?></th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php foreach($userInvestArray as $userInvest): ?>
	                    <tr>
	                      <td class="v-align-middle semi-bold">
	                        <p><?php echo $userInvest->getInvestType()->one()->key;?> (<?php echo $userInvest->getInvestType()->one()->name;?>)</p>
	                      </td>
	                      <td class="v-align-middle">
	                      	<?php echo $userInvest->paid_sum;?>
	                      </td>
	                      <td class="v-align-middle">
	                        <?php echo $userInvest->closed_percent;?>
	                      </td>
	                      <td class="v-align-middle">
	                        <?php 
	                        $currentInvestData = LkpoolMarketing::getCurrentInvestData($userInvest->invest_type_id);
							$investNum = $currentInvestData['invest_num'];
							?>
							<?php if ($investNum > $userInvest->invest_num):?>
                <?php $userInvest = UserInvest::find()
                  ->where(['invest_type_id' => $userInvest->invest_type_id])
                  ->andWhere(['invest_num' => $userInvest->invest_num])
                  ->orderBy(['date_add' => SORT_DESC])
                  ->one();?>
                <?php if ((time() - strtotime($userInvest->date_add)) > (60*60*24)):?>
                  <span class=" label label-success p-t-5 m-l-5 p-b-5 inline fs-12"><?php echo Yii::t('app', 'Настройка ноды');?></span>
                <?php else:?>
                  <span class=" label label-success p-t-5 m-l-5 p-b-5 inline fs-12"><?php echo Yii::t('app', 'Активна');?></span>
                <?php endif;?>
              <?php else:?>
                <span class=" label label-info p-t-5 m-l-5 p-b-5 inline fs-12"><?php echo Yii::t('app', 'Формируется');?></span>
							<?php endif;?>
	                      </td>
	                      <td class="v-align-middle semi-bold">
	                        <p><?php echo $userInvest->sum;?></p>
	                      </td>
	                      <td class="v-align-middle">
	                        <?php echo date('H:i d/m/Y',strtotime($userInvest->date_add));?>
	                      </td>
	                    </tr>
                  	<?php endforeach;?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
  </div>
</div>