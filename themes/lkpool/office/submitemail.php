<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Доступ запрещён');
?>


<div class="login-wrapper ">
  <div class="bg-pic">
    <div id="particles-js">
    </div>
      <img src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" data-src-retina="/lkpool/img/dashboard/bitcoin-worldwide-connections.jpg" alt="" class="lazy" style = "width: 100%;">
  </div>
</div>

<div class="errorModal modal fade slide-up disable-scroll" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content-wrapper">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
        <div class="col-xs-12 text-center" style = "text-align: center;">
          <?php echo Yii::t('app', 'Поздравляем вы успешно подтвердили свой <br/><strong>E-mail</strong><br/>');?>
          <p><?php echo Yii::t('app', 'Теперь Вам доступен полный функционал личного кабинета');?></p>
          </br>
          <a href = "<?php echo Url::to(['/profile/office/dashboard']);?>" style = "font-weight:bold; text-decoration: underline;">
            <?php echo Yii::t('app', 'Перейти в личный кабинет');?>
          </a>
        </div>
      </div>

    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function() {
  $('.errorModal').modal('show');
});
</script>