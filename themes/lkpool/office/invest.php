<?php
use app\modules\mainpage\models\Preference;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Payment;
use app\modules\finance\models\Currency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;
use app\modules\invest\models\search\InvestTypeNodeSearch;
use app\modules\invest\models\InvestPref;

$this->title = Yii::t('user', 'Покупка мастер ноды');
$this->params['breadcrumbs'][] = $this->title;

$investTypeNodeSearch = new InvestTypeNodeSearch;
$investTypeNodeProvider = $investTypeNodeSearch->search(Yii::$app->request->queryParams);
?>

<div class="page-content-wrapper ">
  <div class="content">
    <div class="container-fluid  container-fixed-lg" >

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/office-breadcrumbs', [
          'breadcrumbs' => [
              ['link' => Url::to('/office/user'), 'title' => $this->title]
          ]
      ]);?>

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/invest-script'); ?>

      <?php Pjax::begin();?>

        <?=GridView::widget([
        'dataProvider' => $investTypeNodeProvider,
        'filterModel' => $investTypeNodeSearch,
        'id' => 'payment-grid',
        'tableOptions' => [
          'class' => 'table table-hover demo-table-search table-responsive-block no-footer invest-table',
        ],
        'layout' => '
          <div class=" container-fluid   container-fixed-lg bg-white">
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">' . Yii::t('app', 'Список мастер нод, доступных к покупке') . '
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                    <a href = "/office/viewinvest" class="btn btn-primary btn-cons"><i class="fa fa-inbox"></i> ' . Yii::t('app', 'Мои мастер ноды') . '
                    </a>
                  </div>
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                    {pager}
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-block">
                {items}
              </div>
            </div>
          </div>',
        'columns' => [
          [
            'attribute' => 'name',
            'label' => Yii::t('app', 'Coin / detail'),
            'format' => 'raw',
            'contentOptions' => ['class' => 'node-info'],
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_coin_detail', ['model' => $model]);},
          ],[
            'attribute' => 'sum',
            'label' => Yii::t('app', 'Price'),
            'format' => 'raw',
            'contentOptions' => ['class' => 'node-info'],
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_price', ['model' => $model]);},
          ],[
            'attribute' => 'change',
            'label' => Yii::t('app', 'Change'),
            'format' => 'raw',
            'contentOptions' => ['class' => 'node-info'],
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_change', ['model' => $model]);},
          ],[
            'attribute' => 'volume',
            'label' => Yii::t('app', 'Volume'),
            'format' => 'raw',
            'contentOptions' => ['class' => 'node-info'],
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_volume', ['model' => $model]);},
          ],[
            'attribute' => 'marketcap',
            'label' => Yii::t('app', 'Marketcap'),
            'format' => 'raw',
            'contentOptions' => ['class' => 'node-info'],
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_marketcap', ['model' => $model]);},
          ],[
            'attribute' => 'roi',
            'label' => Yii::t('app', 'ROI'),
            'format' => 'raw',
            'contentOptions' => ['class' => 'node-info'],
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_roi', ['model' => $model]);},
          ],[
            'attribute' => 'nodes',
            'label' => Yii::t('app', 'Nodes'),
            'format' => 'raw',
            'contentOptions' => ['class' => 'node-info'],
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_nodes', ['model' => $model]);},
          ],[
            'attribute' => 'required',
            'label' => Yii::t('app', 'Required'),
            'format' => 'raw',
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_required', ['model' => $model]);},
          ],[
            'attribute' => 'mn_worth',
            'label' => Yii::t('app', 'Mn worth'),
            'format' => 'raw',
            'contentOptions' => ['class' => 'node-info'],
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_mn_worth', ['model' => $model]);},
          ],[
            'attribute' => 'coins_for_launch',
            'label' => Yii::t('app', 'Coins for launch'),
            'format' => 'raw',
            'contentOptions' => ['class' => 'node-info'],
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_coins_for_launch', ['model' => $model]);},
          ],[
            'attribute' => 'coins_for_launch',
            'label' => Yii::t('app', 'Действие'),
            'format' => 'raw',
            'filter' => true,
            'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_node_action', ['model' => $model]);},
          ]
        ],
      ]);?>
      <?php Pjax::end();?>
    </div>
  </div>
</div>

<?php $this->registerJs('
$(".node-info").click(function(){
  var nodeId = $(this).parent().find(".nodeId").attr("nodeid");
  location.href = "/office/investdetail/" + nodeId;
})
');?>

<?php if(Yii::$app->session->getFlash('message')): ?>
  <div class="modalMessage modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('message');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalMessage').modal('show');
", View::POS_END);?>

<?php endif;?>