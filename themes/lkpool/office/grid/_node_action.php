<?php
use app\modules\invest\components\marketing\LkpoolMarketing;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$currentInvestData = LkpoolMarketing::getCurrentInvestData($model->id);
$currentPercent = $currentInvestData['closed_percent'];
?>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-cons btn-animated from-left pg pg-credit_card" data-toggle="modal" data-target="#buyNodeModal<?php echo $model->id;?>">
  <span><?php echo Yii::t('app', 'Приобрести');?></span>
</button>

<!-- Modal -->
<div class="modal fade" id="buyNodeModal<?php echo $model->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style = "text-align: center;display: block; width: 100%;"><?php echo Yii::t('app', 'Мастер нода {name}({key})', ['name' => $model->name, 'key' => $model->key]);?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <?php $form = ActiveForm::begin(); ?>
      <div class="modal-body">
      	  <hr>
      	  <div class="chartContainer" id ="nodeContainer<?php echo $model->id; ?>"></div>
      	  <hr>
      	  <h5 style="text-align: center; margin-top: 30px;"><?php echo Yii::t('app', 'Выберите количество долей');?></h5>
	      <div class="slider-wrapper">
	        <input type="text" value="<?php echo 100 - $currentPercent; ?>" class="js-check-change<?php echo $model->id; ?>" name = "marketingData[closed_percent]"/>
	        <div style="margin-top: 20px;">
	        	<h5 style = "text-align: center; display: block; margin-bottom: 0px;" class="display-box-label"><?php echo Yii::t('app', 'Итого');?></h5>
	        </div>
	        <div>
	        	<div style = "text-align: center;" id="js-display-change<?php echo $model->id; ?>" class="display-box"></div>
	        </div>
	      </div>
      </div>
      <div class="modal-footer">
		<?= Html::submitButton(Yii::t('app', '<span>' . 'Приобрести' . '</span>'), [
          'class' => 'btn btn-primary btn-cons btn-animated from-left pg pg-credit_card',
          'value' => $model->id,
          'name' => 'marketingData[invest_type_id]']) ?>

        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
	  <?php ActiveForm::end();?>
    </div>
  </div>
</div>

<script type="text/javascript">
var data = [
    {
        y: <?php echo $currentPercent;?>,
        color: "#6d5cae",
        name: "<?php echo Yii::t('app', 'Куплено');?>"
    },
    {
        y: <?php echo (100 - $currentPercent);?>,
        color:"#d0d3d8",
        name: "<?php echo Yii::t('app', 'Осталось');?>"
    }
]

var initChart = function(_data) {
    $('#nodeContainer<?php echo $model->id; ?>').highcharts({
        chart: {
            animation: true,
            type: 'pie',
            backgroundColor: null
        },
		title: {
	      verticalAlign: 'bottom',
	      floating: true,
	      text: '<?php echo Yii::t('app', 'Куплено: {buySum} BTC Осталось: {lastSum} BTC', ['buySum' => ($model->sum / 100) * $currentPercent, 'lastSum' => ($model->sum / 100) * (100 - $currentPercent)]);?>'
	    },
        tooltip: {
            valueSuffix: '%',
            enabled: true
        },
        plotOptions: {
            pie: {
                animation: {
                    duration: 750,
                    easing: 'easeOutQuad'
                },
                shadow: false,
                center: ['50%', '50%'],
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                point:{
                  events:{
                    click: function(){
                        moveToPoint(this);
                    }
                  }
                }
            },
            series: {
                animation: {
                    duration: 750,
                    easing: 'easeOutQuad'
                }
            }
        },
        series: [{
            animation: {
                duration: 750,
                easing: 'easeOutQuad'
            },
            name: '<?php echo Yii::t('app', 'Всего');?>',
            data: data,
            size: '90%',
            innerSize: '0%',
            dataLabels: {
                formatter: function () {
                    return this.y > 5 ? this.point.name : null;
                },
                color: '#ffffff',
                distance: -30
            }
        }]
    });
};

var lastAngle = 0;
var moveToPoint = function (clickPoint) {
    var points = clickPoint.series.points;
    var startAngle = 0;
    for (var i = 0; i < points.length; i++){
        var p = points[i];
        if (p === clickPoint){
            break;
        }
        startAngle += (p.percentage/100.0 * 360.0);
    }

    var newAngle = -startAngle + 90 - ((clickPoint.percentage/100.0 * 360.0)/2);

    $({
        angle: 0,
        target: newAngle
    }).animate({
        angle: newAngle-lastAngle
    }, {
        duration: 750,
        easing: 'easeOutQuad',
        step: function() {
            $('.chart').css({
                transform: 'rotateZ('+this.angle+'deg)'
            });
        },
        complete: function() {
            $('.chart').css({
                transform: 'rotateZ(0deg)'
            });
            clickPoint.series.update({
                startAngle: newAngle // center at 90
            });
            lastAngle = newAngle;
        }
    });
};

initChart();

var changeInput<?php echo $model->id; ?> = document.querySelector('.js-check-change<?php echo $model->id; ?>');
initChangeInput = new Powerange(changeInput<?php echo $model->id; ?>, { min: 0, max: <?php echo 100 - $currentPercent; ?>, start: <?php echo 100 - $currentPercent; ?> });

changeInput<?php echo $model->id; ?>.onchange = function() {
  document.getElementById('js-display-change<?php echo $model->id; ?>').innerHTML = changeInput<?php echo $model->id; ?>.value * <?php echo ($model->sum / 100); ?>;
  document.getElementById('js-display-change<?php echo $model->id; ?>').innerHTML = document.getElementById('js-display-change<?php echo $model->id; ?>').innerHTML + ' BTC';
};

</script>