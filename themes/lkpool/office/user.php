<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use app\modules\profile\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Профиль');
$this->params['breadcrumbs'][] = $this->title;

?>
<style type="text/css">
  .kv-date-calendar{
    width: 40px;
  }
</style>

<div class="page-content-wrapper ">
  <div class="content ">
    <div class=" container-fluid   container-fixed-lg">

      <?php $form = ActiveForm::begin([
        'id' => 'form-profile',
        'options' => [
            'class'=>'form',
            'enctype'=>'multipart/form-data'
        ],]); ?>

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/office-breadcrumbs', [
          'breadcrumbs' => [
              ['link' => Url::to('/office/user'), 'title' => $this->title]
          ]
      ]);?>

      <div class="row">

          <div class="col-lg-4 sm-no-padding">
            <div class="card card-transparent">
              <div class="card-block no-padding">
                <div id="card-advance" class="card card-default">
                  <div class="card-header  separator">
                    <div class="card-title"><?php echo Yii::t('app', 'Дополнительная информация');?>
                    </div>
                    <div class="card-controls">
                      <ul>
                        <li><a href="#" class="card-close" data-toggle="close"><i class="fa fa-user"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-block">
                    <div class="lb-user-module-profile-image">
                    <br/>
                        <?php
                        if ($user->image) {
                            echo "<img src='/".$user->image."' class='thumbnail'>";
                            echo "<p>" . Html::a(Yii::t('app', 'Удалить фото'), ['/profile/office/remove']) . "</p>";
                        } else {
                                echo "<img src='/lkpool/img/profiles/8x.jpg' class='thumbnail'>";
                        }
                        ?>
                          <?= $form->field($user, 'photo', ['options' => [
                            'class' => 'btn btn-default'], 'template' => '<i class="fa fa-paperclip"></i> ' . Yii::t('app', 'Ваше изображение'). '{input}'])->fileInput() ?>

                    </div>

                    <div class="form-group">
                      <br/>
                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                            'class' => 'btn btn-success pull-left',
                            'name' => 'signup-button']) ?>
                    </div>
                  </div>
                </div>
              </div>
          </div>

            <div class="card card-transparent">
              <div class="card-block no-padding">
                <div id="card-advance" class="card card-default">
                  <div class="card-header  separator">
                    <div class="card-title"><?php echo Yii::t('app', 'Смена пароля');?>
                    </div>
                    <div class="card-controls">
                      <ul>
                        <li><a href="#" class="card-close" data-toggle="close"><i class="fa fa-unlock"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-block">
                    <br/>
                    <div class="lb-user-user-profile-password">
                        <?= $form->field($user, 'old_password')->passwordInput([
                            'maxlength' => true,
                            'value' => '',
                            'placeholder' => $user->getAttributeLabel('old_password'),
                            'class' => 'form-control password'
                        ]) ?>
                    </div>

                    <div class="lb-user-user-profile-password">
                        <?= $form->field($user, 'password')->passwordInput([
                            'maxlength' => true,
                            'placeholder' => $user->getAttributeLabel('password'),
                            'class' => 'form-control password'
                        ]) ?>
                    </div>

                    <div class="lb-user-user-profile-password">
                        <?= $form->field($user, 'repeat_password')->passwordInput([
                            'maxlength' => true,
                            'placeholder' => $user->getAttributeLabel('repeat_password'),
                            'class' => 'form-control repeat_password',
                            'value' => ''
                        ]) ?>
                    </div>

                    <div class="form-group">
                      <br/>
                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                            'class' => 'btn btn-success pull-left',
                            'value' => 1,
                            'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
                    </div>
                  </div>
                </div>
              </div>
          </div>

        </div>

          <div class="col-lg-4 sm-no-padding">
            <div class="card card-transparent">
              <div class="card-block no-padding">
                <div id="card-advance" class="card card-default">
                  <div class="card-header  separator">
                    <div class="card-title"><?php echo Yii::t('app', 'Личные данные');?>
                    </div>
                    <div class="card-controls">
                      <ul>

                        <li><a href="#" class="card-close" data-toggle="close"><i class="fa fa-user-circle"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-block">
                    <br/>
                    <?= $form->field($user, 'first_name')->textInput([
                        'maxlength' => true,
                        'placeholder' => $user->getAttributeLabel('first_name')
                    ]) ?>

                    <?= $form->field($user, 'last_name')->textInput([
                        'maxlength' => true,
                        'placeholder' => $user->getAttributeLabel('last_name')
                    ]) ?>

                    <?= $form->field($user, 'phone')->textInput([
                        'maxlength' => true,
                        'placeholder' => $user->getAttributeLabel('phone')
                    ]) ?>

                    <?= $form->field($user, 'skype')->textInput([
                        'maxlength' => true,
                        'placeholder' => $user->getAttributeLabel('skype')
                    ]) ?>

                    <?= $form->field($user, 'birthday')
                        ->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => $user->getAttributeLabel('birthday')],
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd.mm.yyyy'
                            ]
                            ]); ?>

                    <div class="input-wrap">
                        <div class="clearfix" id="UserLogin-gender">
                            <label class="radio-head"><?php echo Yii::t('app', 'Пол');?></label>
                            <?=
                            $form->field($user, 'sex')
                                ->radioList(
                                    User::getSexArray(),
                                    [
                                        'item' => function($index, $label, $name, $checked, $value) {

                                            $checked = $checked == true ? 'checked="checked"' : '';
                                            $return = '<label class="modal-radio">';
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" class="flat-red" ' . $checked . ' >';
                                            $return .= '<i></i>';
                                            $return .= '<span>' . ucwords($label) . '</span>';
                                            $return .= '</label>';

                                            return $return;
                                        }
                                    ]
                                )
                            ->label(false);
                            ?>
                        </div>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group">
                      <br/>
                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                            'class' => 'btn btn-success pull-left',
                            'value' => 1,
                            'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
                    </div>

                  </div>
                </div>
              </div>
          </div>
        </div>

          <div class="col-lg-4 sm-no-padding">
            <div class="card card-transparent">
              <div class="card-block no-padding">
                <div id="card-advance" class="card card-default">
                  <div class="card-header  separator">
                    <div class="card-title"><?php echo Yii::t('app', 'Сменить E-mail');?>
                    </div>
                    <div class="card-controls">
                      <ul>
                        <li><a href="#" class="card-close" data-toggle="close"><i class="fa fa-envelope-open"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-block">
                    <br/>
                    <?= $form->field($user, 'email')->textInput([
                        'maxlength' => true,
                        'readonly' => true,
                        'placeholder' => $user->getAttributeLabel('email')
                    ]) ?>

                    <?= $form->field($user, 'email_confirm_token',['options' => [
                        'style' => $oldUser->email_confirm_token == '' ? 'display:none;' : 'display:block;']])->textInput([
                        'maxlength' => true,
                        'value' => '',
                        'placeholder' => $user->getAttributeLabel('email_confirm_token')
                    ]) ?>

                    <div class="form-group">
                      <br/>
                        <?php if ($oldUser->email_confirm_token == ''):?>
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отвязать'), [
                                'class' => 'btn btn-success pull-left',
                                'value' => 1,
                                'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                        <?php else:?>
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отвязать'), [
                                'class' => 'btn btn-success pull-left',
                                'value' => 1,
                                'name' =>  (new \ReflectionClass($user))->getShortName() . '[confirm_token_submit]']) ?>

                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить повторно код подтверждения'), [
                                'class' => 'btn btn-success pull-right',
                                'value' => 1,
                                'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                        <?php endif;?>

                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
      <?php ActiveForm::end();?>
    </div>
  </div>
</div>

<?php $successMessage = Yii::$app->session->getFlash('success');?>

<?php if (!empty($successMessage)):?>

<div class="successModal modal fade slide-up disable-scroll" tabindex="-1" role="dialog">

  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
        <?php echo $successMessage;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$(function() {
  $('.successModal').modal('show');
});
</script>
<?php endif;?>