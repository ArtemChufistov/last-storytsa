<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
?>

<div class="modal modalVerifiedUser fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Поздралвяем');?></h4>
        </div>
        <div class="modal-body">
        <p><?php echo Yii::t('app', 'Вы успешно подтвердили свой E-mail, теперь Вам доступен весь функционал личного кабинета');?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs("
jQuery(function() {
  jQuery('.modalVerifiedUser').modal('show');
});
", View::POS_END);?>