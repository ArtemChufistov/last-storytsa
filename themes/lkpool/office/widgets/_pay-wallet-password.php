<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(['options' => ['class' => ['form-horizontal bordered-row']]]);?>
  <div class="row row-same-height">
    <div class="col-lg-6">
      <div class="padding-30 sm-padding-5">

        <?=Html::submitButton('<i class="glyphicon glyphicon-send"></i> ' . Yii::t('app', 'Получить платёжный пароль'), [
          'class' => 'btn btn-md btn-post float-left btn-success',
          'value' => 1,
          'name' => 'send-pay-wallet'])?>
      <?php if (!empty($message)): ?>
        </br>
        </br>
        <strong class="help-block"><?php echo $message; ?></strong>
      <?php endif;?>

      </div>
    </div>
  </div>
<?php ActiveForm::end();?>
