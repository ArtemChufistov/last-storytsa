<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\web\View;
use app\modules\support\models\TicketDepartment;

$this->title = Yii::t('user', 'Поддержка');
$this->params['breadcrumbs'][] = $this->title;

$departments = TicketDepartment::find()->all();

$departmentArray = [];
foreach($departments as $department){
  $departmentArray[$department->id] = Yii::t('app', $department->name);
}

?>

<style type="text/css">
  .select2-selection__clear{
    top: 0px !important;
  }
</style>

<div class="page-content-wrapper ">
  <!-- START PAGE CONTENT -->
  <div class="content ">
    <!-- START CONTAINER FLUID -->
    <div class=" container-fluid   container-fixed-lg">

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/office-breadcrumbs', [
          'breadcrumbs' => [
              ['link' => Url::to('/office/support'), 'title' => $this->title]
          ]
      ]);?>

      <div class="row">
        <div class="col-lg-6 sm-no-padding">
          <div class="card card-transparent">
            <div class="card-block no-padding">
              <div id="card-advance" class="card card-default">
                <div class="card-header  separator">
                  <div class="card-title"><?php echo Yii::t('app', 'Служба технической поддержки');?>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-close" data-toggle="close"><i class="fa fa-bars"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-block">
                  <div class="content-box-wrapper text-center clearfix" style = "text-align: justify;">
                  <br/>
                    <?php echo Yii::t('app', '<p style = "text-align: left;"><strong style = "text-align: left;">Внимание! ответ может занять до 24 часов.</strong></p><p style = "text-align: left;">Здесь вы можете задавать вопросы администрации проекта, или внести своё предложение по улучшению сервиса, а так-же дополнительным разделам личного кабинета.</p><p style = "text-align: left;"> Возможно у Вас возникли трудности технического характера, если так, то прежде чем их задавать попробуйте найти решение в разделе FAQ.</p><p style = "text-align: left;"> Если-же вы всеравно не нашли решение своей проблемы, то обратитесь к нам и мы постараемся в кратчайшие сроки помочь. Просьба относится с внимательностью к выбору отдела, так как это упростит сотрудничество</p>');?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card card-transparent">
            <div class="card-block no-padding">
              <div id="card-advance" class="card card-default">
                <div class="card-header  separator">
                  <div class="card-title"><?php echo Yii::t('app', 'Создать тикет');?>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-close" data-toggle="close"><i class="fa fa-bookmark-o"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-block">
                  <div class="content-box-wrapper text-center clearfix" style = "text-align: left;">
                  <br/>
                    <?php $form = ActiveForm::begin([
                      'options' => [
                          'class'=>'form',
                          'enctype'=>'multipart/form-data'
                      ],
                      ]); ?>
                    <div class="form-group">
                        <?php echo $form->field($ticket, 'department')->widget(Select2::classname(), [
                            'data' =>  $departmentArray,
                            'options' => ['placeholder' => Yii::t('app', 'Выберите отдел')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);?>
                    </div>
                    <div class="form-group">
                      <?= $form->field($ticket, 'text')->textArea(['rows' => '4', 'value' => '', 'class' => 'textarea form-control']); ?>
                    </div>
                    <div class="form-group">
                      <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить'), [
                          'class' => 'btn btn-info pull-left',
                          'name' => 'signup-button']) ?>
                    </div>
                    <?php ActiveForm::end();?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-xs-12">
          <div class="card card-transparent">
            <div class="card-block no-padding">
              <div id="card-advance" class="card card-default">
                <div class="card-header  separator">
                  <div class="card-title"><?php echo Yii::t('app', 'Ваши тикеты');?>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-close" data-toggle="close"><i class="fa fa-comments-o"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-block">

                 <table class="table ticketTable">
                    <tr>
                      <td style="width: 10px">№</th>
                      <td><?php echo Yii::t('app', 'Дата');?></td>
                      <td><?php echo Yii::t('app', 'Статус');?></td>
                      <td><?php echo Yii::t('app', 'Отдел');?></td>
                      <td><?php echo Yii::t('app', 'Новых (Всего)');?></td>
                    </tr>
                    <?php foreach(array_reverse($ticketArray) as $ticket):?>
                      <tr ticketId="<?php echo $ticket->id;?>">
                        <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo $ticket->id;?>.</a></td>
                        <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo date('d-m-Y', strtotime($ticket->dateAdd));?></a></td>
                        <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo Yii::t('app', $ticket->getStatus()->one()->titleName());?></a></td>
                        <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo Yii::t('app', $ticket->getDepartment()->one()->name);?></a></td>
                        <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>">
                          <?php if (count($ticket->getNotReadMessages($user->id)) > 0):?>
                            <span class="label label-success label-rouded"><?php echo count($ticket->getNotReadMessages($user->id));?> (<?php echo $ticket->getMessages()->count();?>)</span></a>
                          <?php else:?>
                            <span class="label label-warning label-rounded"><?php echo count($ticket->getNotReadMessages($user->id));?> (<?php echo $ticket->getMessages()->count();?>)</span></a>
                          <?php endif;?>
                        </td>
                      </tr>
                    <?php endforeach;?>
                  </table>
                </div>
              </div>
            </div>
          </div>
        
        <?php if (!empty($currentTicket)):?>

        <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/partial/support-message', [
          'ticketMessages' => $ticketMessages,
          'ticketMessage' => $ticketMessage,
          'currentTicket' => $currentTicket,
          'user' => $user
          ]);?>

        <?php endif;?>
        </div>
      </div>
    </div>
  </div>
</div>