<?php
use yii\helpers\Url;

$this->title = Yii::t('user', 'Информация по ноде: {name}', ['name' => $investType->name]);
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-content-wrapper ">
  <div class="content">
    <div class="container-fluid  container-fixed-lg" >

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/office-breadcrumbs', [
          'breadcrumbs' => [
              ['link' => Url::to('/office/invest'), 'title' => Yii::t('app', 'Покупка мастер ноды')],
              ['link' => Url::to('/office/investdetail/' . $investType->id), 'title' => $this->title],
          ]
      ]);?>

      <?php echo $investType->text;?>

    </div>
  </div>
</div>