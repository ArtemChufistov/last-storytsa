<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use app\modules\profile\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Снятие с личного кабинета');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="page-content-wrapper ">
  <div class="content ">
    <div class=" container-fluid   container-fixed-lg">

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/office-breadcrumbs', [
          'breadcrumbs' => [
              ['link' => Url::to('/office/finance'), 'title' => Yii::t('app', 'Финансы')],
              ['link' => Url::to('/office/in'), 'title' => $this->title],
          ]
      ]);?>

      <div class=" container-fluid   container-fixed-lg">
        <div id="rootwizard" class="m-t-50">
          <div id="rootwizard" class="m-t-50">
          <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/partial/out/_' . strtolower(\yii\helpers\StringHelper::basename(get_class($paymentForm))), ['paymentForm' => $paymentForm, 'user' => $user, 'currencyArray' => $currencyArray]); ?>
        </div>
      </div>
    </div>
  </div>
</div>