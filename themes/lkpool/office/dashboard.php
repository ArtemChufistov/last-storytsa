<?php
use app\assets\lkpool\DashboardBottomAsset;
use app\modules\finance\models\Currency;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;

DashboardBottomAsset::register($this);

$this->title = Yii::t('user', 'Панель управления');

$currencyBTC = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
$balanceBTC  = $user->getBalances([$currencyBTC->id])[$currencyBTC->id];
?>

<div class="page-content-wrapper ">
  <div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
      <div class="row">
        <div class="col-lg-6 col-xlg-5">
          <div class="row">
            <div class="col-md-12 m-b-10">
              <div class="ar-3-2 widget-1-wrapper">
                <!-- START WIDGET widget_imageWidget-->
                <div class="widget-1 card no-border bg-complete no-margin widget-loader-circle-lg">
                  <div class="card-header  top-right ">
                    <div class="card-controls">
                      <ul>
                        <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i class="fa fa-btc" style = "color: white;"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-block">
                    <div class="pull-bottom bottom-left bottom-right ">
                      <span class="label font-montserrat fs-11"><?php echo Yii::t('app', 'Bitcoin');?></span>
                      <br>
                      <h2 class="text-white"><?php echo Yii::t('app', 'Текущий курс');?></h2>
                      <p class="text-white hint-text"><?php echo Yii::t('app', 'Актуальнные данные от биржи Poloniex');?></p>
                      <div class="row stock-rates m-t-15">
                        <div class="company col-4">
                          <div>
                            <p class="font-montserrat text-success no-margin fs-16">
                              <i class="fa fa-caret-up"></i> +0.95%
                              <span class="font-arial text-white fs-12 hint-text m-l-5">546.45</span>
                            </p>
                            <p class="bold text-white no-margin fs-11 font-montserrat lh-normal">
                              <?php echo Yii::t('app', 'За 7 часов');?>
                            </p>
                          </div>
                        </div>
                        <div class="company col-4">
                          <div>
                            <p class="font-montserrat text-danger no-margin fs-16">
                              <i class="fa fa-caret-up"></i> -0.34%
                              <span class="font-arial text-white fs-12 hint-text m-l-5">345.34</span>
                            </p>
                            <p class="bold text-white no-margin fs-11 font-montserrat lh-normal">
                              <?php echo Yii::t('app', 'За 24 часа');?>
                            </p>
                          </div>
                        </div>
                        <div class="company col-4">
                          <div class="pull-right">
                            <p class="font-montserrat text-success no-margin fs-16">
                              <i class="fa fa-caret-up"></i> +0.95%
                              <span class="font-arial text-white fs-12 hint-text m-l-5">278.87</span>
                            </p>
                            <p class="bold text-white no-margin fs-11 font-montserrat lh-normal">
                              <?php echo Yii::t('app', 'За неделю');?>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6 m-b-10">
              <div class="ar-2-1">
                <!-- START WIDGET widget_graphTile-->
                <div class="widget-4 card no-border  no-margin widget-loader-bar">
                  <div class="container-sm-height full-height d-flex flex-column">
                    <div class="card-header  ">
                      <div class="card-title text-black hint-text">
                        <span class="font-montserrat fs-11 all-caps">
                        <?php echo Yii::t('app', 'Производительность серверов');?> 
                    </span>
                      </div>
                      <div class="card-controls">
                        <ul>
                          <li>
                            <a href="#" class="card-refresh text-black" data-toggle="refresh">
                              <i class="fa fa-line-chart"></i>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="p-l-20 p-r-20">
                      <h5 class="no-margin p-b-5 pull-left hint-text"><?php echo Yii::t('app', 'Мега хэш');?></h5>
                      <p class="pull-right no-margin bold hint-text">2.563</p>
                      <div class="clearfix"></div>
                    </div>
                    <div class="widget-4-chart line-chart mt-auto" data-line-color="success" data-area-color="success-light" data-y-grid="false" data-points="false" data-stroke-width="2">
                      <svg></svg>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 m-b-10">
              <div class="ar-2-1">
                <div class="widget-5 card no-border  widget-loader-bar">
                  <div class="card-header  pull-top top-right">
                    <div class="card-controls">
                      <ul>
                        <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
				class="fa fa-bar-chart"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="container-xs-height full-height">
                    <div class="row row-xs-height">
                      <div class="col-xs-5 col-xs-height col-middle relative">
                        <div class="padding-15 top-left bottom-left">
                          <h5 class="hint-text no-margin p-l-10"><?php echo Yii::t('app', 'Топовые ноды');?></h5>
                          <p class=" bold font-montserrat p-l-10">2.345.789
                            <br><?php echo Yii::t('app', ' мегахэш');?></p>
                          <p class=" hint-text visible-xlg p-l-10"><?php echo Yii::t('app', 'Посмотреть');?></p>
                        </div>
                      </div>
                      <div class="col-xs-7 col-xs-height col-bottom relative widget-5-chart-container">
                        <div class="widget-5-chart"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-xlg-4">
          <div class="row">
            <div class="col-sm-6 m-b-10">
              <div class="ar-1-1">
                <div class="widget-2 card no-border bg-primary widget widget-loader-circle-lg no-margin">
                  <div class="card-header ">
                    <div class="card-controls">
                      <ul>
                        <li><a href="#" class="card-refresh" data-toggle="refresh"><i
				class="fa fa-newspaper-o" style = "color: white;"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-block">
                    <div class="pull-bottom bottom-left bottom-right padding-25">
                      <span class="label font-montserrat fs-11"><?php echo Yii::t('app', 'Новости');?></span>
                      <br>
                      <h3 class="text-white"><?php echo Yii::t('app', 'Свежая информация о Bitcoin');?></h3>
                      <p class="text-white hint-text hidden-lg-down"><?php echo Yii::t('app', 'Читать далее');?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 m-b-10">
              <div class="ar-1-1">
                <div class="card no-border bg-master widget widget-31 widget-loader-circle-lg no-margin">
                  <div class="card-header ">
                    <span class="label font-montserrat fs-11" style="font-size: 20px !important; text-align: center; margin: 0 auto; display: block;padding-top: 15px; padding-bottom: 10px;">
                      <?php echo Yii::t('app', 'Ваш баланс');?>
                    </span>
                  </div>
                  <div class="card-block">
                      <h3 class="text-white semi-bold" style="text-align: center; margin-top: 60px; margin-bottom: 30px;"><?php echo number_format($balanceBTC->value, 8);?> BTC</h3>
                    <div class="pull-top padding-25 bottom-left bottom-right">
                      <span class="label font-montserrat fs-11" style="font-size: 20px !important; display: block; float: left;"><?php echo Yii::t('app', 'Пополнить');?></span>
                      <span class="label font-montserrat fs-11" style="font-size: 20px !important; display: block; float: right;"><?php echo Yii::t('app', 'Снять');?></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6 m-b-10">
              <div class="ar-1-1">
                <div class="card no-border bg-master widget widget-6 widget-loader-circle-lg no-margin">
                  <div class="card-header ">
                    <div class="card-controls">
                      <ul>
                        <li><a data-toggle="refresh" class="card-refresh" href="#"><i class="fa fa-usd" style = "color: white;"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-block">
                    <div class="pull-bottom bottom-left bottom-right padding-25">
                      <h1 class="text-white semi-bold">20 445</h1>
                      <span class="label font-montserrat fs-11"><?php echo Yii::t('app', 'USD');?></span>
                      <p class="text-white m-t-20"><?php echo Yii::t('app', 'Добыто за последние 24 часа');?></p>
                      <p class="text-white hint-text m-t-30"><?php echo date('d/m/Y');?> </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 m-b-10">
              <div class="ar-1-1">
                <div class="widget-7 card no-border bg-32 no-margin">
                  <div class="card-block no-padding full-height">
                    <div class="metro live-tile " data-delay="3500" data-mode="carousel">
                      <div class="slide-front tiles slide active">
                        <div class="padding-30">
                          <div class="pull-bottom p-b-30 bottom-right bottom-left p-l-30 p-r-30">
                            <h5 class="no-margin semi-bold p-b-5" style = "color: white;"><?php echo Yii::t('app', 'Последние');?></h5>
                            <p class="no-margin text-white hint-text"><?php echo Yii::t('app', 'купленные мастер ноды');?></p>
                            <h3 class="semi-bold text-white">243.7 <?php echo Yii::t('app', 'Мегахэш')?></h3>
                            <p><span class="text-white"><?php echo Yii::t('app', 'Megamonstr')?></span> <span class="m-l-20 hint-text">1.4538 BTC</span>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="slide-back tiles">
                        <div class="container-sm-height full-height">
                          <div class="row-sm-height">
                            <div class="col-sm-height padding-25">
                              <p class="hint-text text-white"><?php echo Yii::t('app', 'Последние 3 покупки')?>
                              </p>
                              <p class="p-t-10 text-white">243.7 <?php echo Yii::t('app', 'Мегахэш')?></p>
                              <div class="p-t-10">
                                <p class="hint-text inline"><?php echo Yii::t('app', 'Megamonstr 2')?> <span class="m-l-20">2.1751 BTC</span>
                                </p>
                              </div>
                              <p class="p-t-10 text-white" style = "color">243.7 <?php echo Yii::t('app', 'Мегахэш')?></p>
                              <div class="p-t-10">
                                <p class="hint-text inline"><?php echo Yii::t('app', 'Megamonstr 3')?><span class="m-l-20">0.1751 BTC</span>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="row-sm-height">
                            <div class="col-sm-height relative">
                              <div class='widget-7-chart line-chart' data-line-color="white" data-points="true" data-point-color="white" data-stroke-width="3">
                                <svg></svg>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="visible-xlg col-xlg-3">
          <div class="ar-2-3">
            <div class="widget-11 card no-border  no-margin widget-loader-bar">
              <div class="card-header  ">
                <div class="card-title"><?php echo Yii::t('app', 'Ваши мастер ноды');?>
                </div>
                <div class="card-controls">
                  <ul>
                    <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
				class="fa fa-cubes"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="p-l-25 p-r-25 p-b-20">
                <div class="pull-left">
                  <h2 class="text-success no-margin"><?php echo Yii::t('app', 'Доход');?></h2>
                </div>
                <h3 class="pull-right semi-bold"><sup>
          				<small class="semi-bold">$</small>
          			</sup> 102,967
          			</h3>
                <div class="clearfix"></div>
              </div>
              <div class="widget-11-table auto-overflow">
                <table class="table table-condensed table-hover">
                  <tbody>
                    <tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr><tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr><tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr><tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr><tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="padding-25">
                <p class="small no-margin">
                  <a href="#"><i class="fa fs-16 fa-arrow-circle-o-down text-success m-r-10"></i></a>
                  <span class="hint-text "><?php echo Yii::t('app', 'Приобрести мастер ноду');?></span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-xl-4 m-b-10 hidden-xlg">
          <div class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle full-height d-flex flex-column">
            <div class="card-header  top-right">
              <div class="card-controls">
                <ul>
                  <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
				class="card-icon card-icon-refresh"></i></a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="padding-25">
              <div class="pull-left">
                <h2 class="text-success no-margin"><?php echo Yii::t('app', 'Доход');?></h2>
              </div>
              <h3 class="pull-right semi-bold"><sup>
	<small class="semi-bold">$</small>
</sup> 102.967
</h3>
              <div class="clearfix"></div>
            </div>
            <div class="auto-overflow widget-11-2-table">
              <table class="table table-condensed table-hover">
                <tbody>
                                            <tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr><tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr><tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr><tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr><tr>
                      <td class="font-montserrat all-caps fs-12"><?php echo Yii::t('app', 'MegaHash Creator');?></td>
                      <td class="text-right">
                        <span class="hint-text small">243.65</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey">
                        <span class="hint-text small"><?php echo Yii::t('app', 'Мегахэш');?></span>
                      </td>
                      <td>
                        <span class="font-montserrat fs-18">0.543 BTC</span>
                      </td>
                    </tr>
                </tbody>
              </table>
            </div>
            <div class="padding-25 mt-auto">
              <p class="small no-margin">
                <a href="#"><i class="fa fs-16 fa-arrow-circle-o-down text-success m-r-10"></i></a>
                <span class="hint-text "><?php echo Yii::t('app', 'Приобрести мастер ноду');?></span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 visible-md visible-xlg col-xlg-4 m-b-10">
          <div class="widget-15 card card-condensed  no-margin no-border widget-loader-circle">
            <div class="card-header ">
              <div class="card-controls">
                <ul>
                  <li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i
				class="card-icon card-icon-refresh"></i></a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-block no-padding">
              <ul class="nav nav-tabs nav-tabs-simple">
                <li class="nav-item">
                  <a href="#" data-toggle="tab" class="p-t-5 active">
			APPL<br>
			22.23<br>
			<span class="text-success">+60.223%</span>
		</a>
                </li>
                <li class="nav-item"><a href="#" data-toggle="tab" class="p-t-5">
		FB<br>
		45.97<br>
		<span class="text-danger">-06.56%</span>
	</a>
                </li>
                <li class="nav-item"><a href="#" data-toggle="tab" class="p-t-5">
		GOOG<br>
		22.23<br>
		<span class="text-success">+60.223%</span>
	</a>
                </li>
              </ul>
              <div class="tab-content p-l-20 p-r-20">
                <div class="tab-pane no-padding active" id="widget-15-tab-1">
                  <div class="full-width">
                    <div class="full-width">
                      <div class="widget-15-chart rickshaw-chart"></div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane no-padding" id="widget-15-tab-2">
                </div>
                <div class="tab-pane" id="widget-15-tab-3">
                </div>
              </div>
              <div class="p-t-20 p-l-20 p-r-20 p-b-30">
                <div class="row">
                  <div class="col-md-9">
                    <p class="fs-16 text-black"><?php echo Yii::t('app', 'Немного текста');?>
                      <br><?php echo Yii::t('app', 'Какая-то информация, пока не знаю');?>
                    </p>
                    <p class="small hint-text p-b-10"><?php echo Yii::t('app', 'Тут должна быть какая-то информация');?>
                    </p>
                  </div>
                  <div class="col-md-3 text-right">
                    <p class="font-montserrat bold text-success m-r-20 fs-16">+0.94</p>
                    <p class="font-montserrat bold text-danger m-r-20 fs-16">-0.63</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 m-b-10">
          <div class="widget-14 card no-border  no-margin widget-loader-circle">
            <div class="container-xs-height full-height">
              <div class="row-xs-height">
                <div class="col-xs-height">
                  <div class="card-header ">
                    <div class="card-title"><?php echo Yii::t('app', 'Производительность');?>
                    </div>
                    <div class="card-controls">
                      <ul>
                        <li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i
              class="card-icon card-icon-refresh"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row-xs-height">
                <div class="col-xs-height">
                  <div class="p-l-20 p-r-20">
                    <p><?php echo Yii::t('app', 'Ваши серверы');?></p>
                    <div class="row">
                      <div class="col-lg-3 col-md-12">
                        <h4 class="bold no-margin">5.2GB</h4>
                        <p class="small no-margin">Total usage</p>
                      </div>
                      <div class="col-lg-3 col-md-6">
                        <h5 class=" no-margin p-t-5">227.34KB</h5>
                        <p class="small no-margin">Currently</p>
                      </div>
                      <div class="col-lg-3 col-md-6">
                        <h5 class=" no-margin p-t-5">117.65MB</h5>
                        <p class="small no-margin">Average</p>
                      </div>
                      <div class="col-lg-3 visible-xlg">
                        <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                  <div class="widget-14-chart_y_axis"></div>
                  <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 m-b-10">
          <div class="widget-18 card no-border  no-margin ">
            <div class="padding-15">
              <div class="item-header clearfix">
                <div class="thumbnail-wrapper d32 circular">
                  <img width="40" height="40" src="assets/img/profiles/3x.jpg" data-src="assets/img/profiles/3.jpg" data-src-retina="assets/img/profiles/3x.jpg" alt="">
                </div>
                <div class="inline m-l-10">
                  <p class="no-margin">
                    <strong><?php echo Yii::t('app', 'Новости проекта');?></strong>
                  </p>
                  <p class="no-margin hint-text"><?php echo Yii::t('app', 'Рады сообщить вам');?>
                  </p>
                </div>
              </div>
            </div>
            <div class="relative">
              <div class="widget-18-post bg-black no-overflow">
              </div>
            </div>
            <div class="padding-15">
              <div class="hint-text pull-left"><?php echo Yii::t('app', '10/10/2018');?></div>
              <ul class="list-inline pull-right no-margin">
                <li><a class="text-master hint-text" href="#">5,345 <i class="fa fa-comment-o"></i></a>
                </li>
                <li><a class="text-master hint-text" href="#">23K <i class="fa fa-heart-o"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>