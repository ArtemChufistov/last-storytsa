<?php
use app\modules\mainpage\models\Preference;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Payment;
use app\modules\finance\models\Currency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = Yii::t('user', 'Финансы');
$this->params['breadcrumbs'][] = $this->title;

$currencyBTC = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
$balanceBTC  = $user->getBalances([$currencyBTC->id])[$currencyBTC->id];
?>

<style type="text/css">
  .table-bordered > thead > tr > th, .table-bordered > thead > tr > td{
    border-bottom-width: 1px;
  }
</style>

<div class="page-content-wrapper ">
  <div class="content ">
    <div class=" container-fluid   container-fixed-lg">

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/office-breadcrumbs', [
          'breadcrumbs' => [
              ['link' => Url::to('/office/user'), 'title' => $this->title]
          ]
      ]);?>

      <div class="row">
        <div class="col-lg-4 sm-no-padding">
            <div class="widget-9 card no-border bg-success no-margin widget-loader-bar">
              <div class="full-height d-flex flex-column">
                <div class="card-header ">
                  <div class="card-title text-black">
                    <span class="font-montserrat fs-11 all-caps"><?php echo Yii::t('app', 'Ваш баланс');?> <i class="fa fa-chevron-right"></i>
                          </span>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i class="fa fa-btc"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="p-l-20">
                  <h3 class="no-margin p-b-5 text-white"><?php echo number_format($balanceBTC->value, 8);?> BTC</h3>
                  <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i></a>
                  <span class="small hint-text text-white"><?php echo Yii::t('app', 'Ваш основной баланс');?></span>
                </div>
                <div class="mt-auto">
                  <div class="progress progress-small m-b-20">
                    <div class="progress-bar progress-bar-white" style="width:100%"></div>
                  </div>
                </div>
              </div>
            </div>

        </div>
        <div class="col-lg-4 sm-no-padding">

            <div class="widget-9 card no-border bg-complete no-margin widget-loader-bar">
              <div class="full-height d-flex flex-column">
                <div class="card-header ">
                  <div class="card-title text-black">
                    <span class="font-montserrat fs-11 all-caps"><?php echo Yii::t('app', 'Ожидает пополнения');?> <i class="fa fa-chevron-right"></i>
                          </span>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i class="fa fa-btc"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="p-l-20">

                <?php
                $waitToSum = Payment::find()
                  ->where(['to_user_id' => $user->id])
                  ->andWhere(['in', 'status', [Payment::STATUS_WAIT, Payment::STATUS_WAIT_SYSTEM, Payment::PROCESS_SYSTEM]])
                  ->sum('to_sum');
                ?>

                  <h3 class="no-margin p-b-5 text-white"><?php echo number_format($waitToSum, 8);?> BTC</h3>
                  <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i></a>
                  <span class="small hint-text text-white"><?php echo Yii::t('app', 'В скором времени будут начислены');?></span>
                </div>
                <div class="mt-auto">
                  <div class="progress progress-small m-b-20">
                    <div class="progress-bar progress-bar-white" style="width:100%"></div>
                  </div>
                </div>
              </div>
            </div>

        </div>
        <div class="col-lg-4 sm-no-padding">

            <div class="widget-9 card no-border bg-primary no-margin widget-loader-bar">
              <div class="full-height d-flex flex-column">
                <div class="card-header ">
                  <div class="card-title text-black">
                    <span class="font-montserrat fs-11 all-caps"><?php echo Yii::t('app', 'Ожидает снятия');?> <i
              class="fa fa-chevron-right"></i>
                          </span>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i
        class="fa fa-btc"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="p-l-20">

                <?php
                $waitFromSum = Payment::find()
                  ->where(['from_user_id' => $user->id])
                  ->andWhere(['in', 'status', [Payment::STATUS_WAIT, Payment::STATUS_WAIT_SYSTEM, Payment::PROCESS_SYSTEM]])
                  ->sum('from_sum');
                ?>

                  <h3 class="no-margin p-b-5 text-white"><?php echo number_format($waitFromSum, 8);?> BTC</h3>
                  <a href="#" class="btn-circle-arrow text-white"><i class="pg-arrow_minimize"></i></a>
                  <span class="small hint-text text-white"><?php echo Yii::t('app', 'В скором времени будут списаны');?></span>
                </div>
                <div class="mt-auto">
                  <div class="progress progress-small m-b-20">
                    <div class="progress-bar progress-bar-white" style="width:100%"></div>
                  </div>
                </div>
              </div>
            </div>

        </div>
      </div>
      <div class="row" style="margin-top: 20px;">
        <div class="col-lg-6 sm-no-padding">
          <div class="card card-transparent">
            <div class="card-block no-padding">
              <div id="card-advance" class="card card-default">
                <div class="card-header  separator">
                  <div class="card-title"><?php echo Yii::t('app', 'Операции со счётом');?>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-close" data-toggle="close"><i class="fa fa-exchange"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-block">
                  <br/>
                  <div class="row">
                    <div class="col-md-4 col-xs-4">
                      <a href= "<?php echo Url::to(['/profile/office/in']);?>" class="btn btn-primary btn-cons btn-animated from-top fa fa-arrow-down pull-left">
                        <span><i class="fa fa-level-down"></i> <?php echo Yii::t('app', 'Пополнить личный счёт');?></span>
                      </a>
                    </div>
                    <div class="col-md-4 col-xs-4">
                    </div>
                    <div class="col-md-4 col-xs-4">
                      <a href= "<?php echo Url::to(['/profile/office/out']);?>" class="btn btn-primary btn-cons btn-animated from-top fa fa-arrow-up pull-right">
                        <span><i class="fa fa-level-up"></i> <?php echo Yii::t('app', 'Снять с личного счёта');?></span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card card-transparent">
            <div class="card-block no-padding">
              <div id="card-advance" class="card card-default">
                <div class="card-header  separator">
                  <div class="card-title"><?php echo Yii::t('app', 'Ваши пополнения/снятия');?>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-close" data-toggle="close"><i class=" fa fa-navicon"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-block">
                  <br/>

                  <?php Pjax::begin(); ?>
                  <?= GridView::widget([
                      'dataProvider' => $paymentProvider,
                      'rowOptions'=>function($model){
                          return ['paymentHash' => $model->hash, 'class' => 'showPaymentInfo'];
                      },
                      'filterModel' => $payment,
                      'id' => 'payment-grid',
                      'tableOptions' => [
                        'class' => 'table table-hover demo-table-search table-responsive-block no-footer invest-table',
                      ],
                      'layout' => '
                        <div class="panel">

                          <div class="table-responsive">
                            {items}
                            <div style = "text-align:center;">{pager}</div>
                          </div>
                        </div>
                        ',
                      'columns' => [
                          [
                            'attribute' => 'type',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'name' => 'PaymentFinanceSearch[type]',
                                'data' => Payment::getFinanceTypeArray(),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'hideSearch' => true,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Выберите тип'),
                                    'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                                ]
                            ]),
                            'contentOptions' => ['class' => 'payment-info'],
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-type', ['model' => $model]);},
                          ],[
                            'attribute' => 'realSum',
                            'format' => 'raw',
                            'filter' => true,
                            'contentOptions' => ['class' => 'payment-info'],
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payments-sum', ['model' => $model]);},
                          ],[
                            'attribute' => 'status',
                            'format' => 'raw',
                            'contentOptions' => ['class' => 'payment-info'],
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-status', ['model' => $model]);},
                            'filter' => Select2::widget([
                                'name' => 'PaymentFinanceSearch[status]',
                                'data' => Payment::getStatusArray(),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'hideSearch' => true,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Выберите статус'),
                                    'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                                ]
                            ]),
                          ],[
                            'attribute' => 'date_add',
                            'format' => 'raw',
                            'contentOptions' => ['class' => 'payment-info'],
                            'filter' => \yii\jui\DatePicker::widget([
                                'model'=>$payment,
                                'attribute'=>'date_add',
                                'options' => ['class' => 'form-control'],
                                'language' => \Yii::$app->language,
                                'dateFormat' => 'dd-MM-yyyy',
                            ]),
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-date-add', ['model' => $model]);},
                          ]
                      ],
                  ]); ?>
                <?php Pjax::end(); ?>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 sm-no-padding">
          <div class="card card-transparent">
            <div class="card-block no-padding">
              <div id="card-advance" class="card card-default">
                <div class="card-header  separator">
                  <div class="card-title"><?php echo Yii::t('app', 'Перевод участнику');?>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-close" data-toggle="close"><i class=" fa fa-street-view"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-block">
                  <br/>
                  <?php $form = ActiveForm::begin(); ?>

                  <div class ="form-group">
                    <?= $form->field($usertransactionForm, 'to_login')->textInput(); ?>
                  </div>

                  <div class ="form-group">
                    <?= $form->field($usertransactionForm, 'sum')->textInput(); ?>
                  </div>

                  <div class ="form-group">
                  <?php echo $form->field($usertransactionForm, 'currency_id')->widget(Select2::classname(), [
                      'data' =>  ArrayHelper::map($userCurrencyArray, 'id', 'title'),
                      'options' => ['placeholder' => Yii::t('app', 'Выберите валюту...')],
                      'pluginOptions' => [
                          'allowClear' => true
                      ],
                  ]);?>
                  </div>

                  <div class="form-group">
                      <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-success']) ?>
                  </div>

                  <?php ActiveForm::end(); ?>
                </div>
              </div>
            </div>
          </div>

          <div class="card card-transparent">
            <div class="card-block no-padding">
              <div id="card-advance" class="card card-default">
                <div class="card-header  separator">
                  <div class="card-title"><?php echo Yii::t('app', 'Движения по счёту');?>
                  </div>
                  <div class="card-controls">
                    <ul>
                      <li><a href="#" class="card-close" data-toggle="close"><i class=" fa fa-navicon"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-block">
                  <?php Pjax::begin(); ?>
                  <?= GridView::widget([
                      'dataProvider' => $transactionProvider,
                      'filterModel' => $transaction,
                      'id' => 'transaction-grid',
                      'tableOptions' => [
                        'class' => 'table table-hover demo-table-search table-responsive-block no-footer invest-table',
                      ],
                      'layout' => '
                        <div class="panel">
                          <div class="table-responsive">
                            {items}
                            <div style = "text-align:center;">{pager}</div>
                          </div>
                        </div>',
                      'columns' => [
                          [
                            'attribute' => 'type',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'name' => 'TransactionFinanceSearch[type]',
                                'data' => Transaction::getOfficeTypeArray(),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'hideSearch' => true,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Выберите тип'),
                                    'value' => isset($_GET['TransactionFinanceSearch[type]']) ? $_GET['TransactionFinanceSearch[type]'] : null
                                ]
                            ]),
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-type', ['model' => $model]);},
                          ],[
                            'attribute' => 'sum',
                            'format' => 'raw',
                            'filter' => true,
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-sum', ['model' => $model]);},
                          ],[
                            'attribute' => 'date_add',
                            'format' => 'raw',
                            'filter' => true,
                            'filter' => \yii\jui\DatePicker::widget([
                                'model'=>$transaction,
                                'attribute'=>'date_add',
                                'options' => ['class' => 'form-control'],
                                'language' => \Yii::$app->language,
                                'dateFormat' => 'dd-MM-yyyy',
                            ]),
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-date-add', ['model' => $model]);},
                          ]
                      ],
                  ]); ?>
                <?php Pjax::end(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="paymentModal modal fade disable-scroll" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo Yii::t('app', 'Детальная информация');?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs("
  jQuery('.payment-info').on('click', function(){

    var paymentHash = $(this).parent().find('.paymentHash').attr('paymentHash');
    $.get( '/office/paymentinfo/' + paymentHash, function( data ) {
      $('.paymentModal').find('.modal-body').html(data);
      $('.paymentModal').modal('show');
    });

  });
", View::POS_END);?>
<style type="text/css">
  .payment-info{
    cursor: pointer;
  }
</style>
