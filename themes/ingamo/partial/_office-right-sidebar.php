<?php
use app\modules\menu\widgets\MenuWidget;
use app\modules\finance\models\Transaction;
use app\modules\event\models\EventSearch;
use app\modules\event\models\Event;
use app\modules\lang\widgets\WLang;
use yii\helpers\Html;
use yii\helpers\Url;

$user = $this->params['user'];

$eventModel = new EventSearch();
$eventModel->user_id = $user->id;
$eventProvider = $eventModel->search([]);

$events = $eventProvider->getModels();

$user = $this->params['user']->identity;
?>

<div class="has-sidebar-left">
    <div class="sticky">
        <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
            <div class="relative">
                <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                    <i></i>
                </a>
            </div>
            <!--Top Menu Start -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown custom-dropdown messages-menu">
                        <a href="#" class="nav-link" data-toggle="dropdown">
                            <?php echo Yii::t('app', 'Ваш баланс');?>:
                            <?php foreach($user->getBalances() as $balance): ?>
                                <strong><?php echo $balance->value;?> USD</strong>
                            <?php endforeach;?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <ul class="menu pl-2 pr-2">
                                    <?php /*
                                    <li>
                                        <a href="#">
                                            <div class="avatar float-left">
                                                <img src="/ingamo/img/dummy/u4.png" alt="">
                                                <span class="avatar-badge busy"></span>
                                            </div>
                                            <h4>
                                                Support Team
                                                <small><i class="icon icon-clock-o"></i> 5 mins</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    */ ?>
                                </ul>
                            </li>
                            <li class="footer s-12 p-2 text-center"><a href="/office/finance"><?php echo Yii::t('app', 'Перейти в финансы');?></a></li>
                        </ul>
                    </li>
                    <li class="dropdown custom-dropdown notifications-menu">
                        <a href="#" class=" nav-link" data-toggle="dropdown" aria-expanded="false">
                            <i class="icon-notifications "></i>
                            <?php /*
                            <?php if (count($events) > 0):?>
                                <span class="badge badge-danger badge-mini rounded-circle"><?php echo count($events);?></span>
                            <?php endif;?>
                            */ ?>
 </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="header"><?php echo Yii::t('app', 'События')?></li>
                            <li>
                                <ul class="menu">
                                    <?php foreach($events as $event):?>
                                        <?php $toUser = $event->getToUser()->one();?>
                                        <?php if (!empty($toUser)):?>
                                            <li>
                                                <a href="#">
                                                    <i class="icon icon-data_usage text-success"></i> <?php echo $event->getTypeTitle();?> <?php echo date("H:i:s d-m-y", strtotime($event->date_add));?>
                                                </a>
                                            </li>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </ul>
                            </li>
                            <li class="footer p-2 text-center"><a href="/office/stat"><?php echo Yii::t('app', 'Посмотреть все события')?></a></li>
                        </ul>
                    </li>
                    <li class="dropdown custom-dropdown messages-menu">
                        <?php echo WLang::widget(['view' => '_lang-office-choser']);?>
                    </li>
                    <li class="dropdown custom-dropdown user user-menu ">
                        <a href="#" class="nav-link" data-toggle="dropdown">
                            <img src="<?php echo $user->getImage(); ?>" class="user-image" alt="<?php echo $user->login; ?>">
                            <i class="icon-more_vert "></i>
                        </a>
                        <?php echo MenuWidget::widget(['menuName' => 'profileMenu', 'view' => 'office-right-menu']);?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>