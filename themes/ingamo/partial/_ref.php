<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;
use app\modules\invest\models\UserInvest;

$userInvest = UserInvest::find()->where(['user_id' => $user->id])->one();
?>

<div class="card" style="margin-bottom: 20px;">
   <div class="card-header white">
       <h6><?php echo Yii::t('app', 'Ваша реферальная ссылка');?> <i class="icon icon-share-alt s-18" style="float: right;"></i></h6>
   </div>
   <div class="card-body">
        <label class="control-label"></label>
         <?php echo Html::input('text', 'refLink', Url::home(true) . '?ref=' . $user->login, ['id' => 'refLink', 'class' => 'form-control' , 'readonly' => true, 'label' => false]);?>
        <br/>
        <?php /*
        <div class="form-group">
          <?php echo Html::submitButton('<i class="glyphicon glyphicon-share-alt"></i> ' . Yii::t('app', 'Скопировать в буфер обмена'), ['class' => 'btn-clipboard-ref btn btn-success pull-left', 'data-clipboard-target' => '#refLink']);?>
        </div>
*/?>
<?php /*$this->registerJs('
$(document).ready(function () {
  new Clipboard(".btn-clipboard-ref").on("success", function(e) {
     $.toast({
      heading: "' . Yii::t('app', 'Ссылка скопирована в буфер обмена') . '",
      text: "' . Yii::t('app', 'Information is in buffer') . '",
      position: "top-right",
      loaderBg:"#ff6849",
      icon: "info",
      hideAfter: 3000, 
      stack: 6
    });
  });
})
');*/?>

   </div>
</div>