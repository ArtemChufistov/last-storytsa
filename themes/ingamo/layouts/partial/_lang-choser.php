<?php
use app\modules\lang\widgets\WLang;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
?>


<li class="rd-nav-item rd-navbar--has-dropdown rd-navbar-submenu">
  <a class="rd-nav-link" href="#"><?php echo Yii::t('app', 'Сменить язык'); ?></a>
  <ul class="rd-menu rd-navbar-dropdown">
    <?php foreach ($langs as $lang): ?>
        <li class="rd-megamenu-list-item">
          <?=Html::a($lang->name, '/' . $lang->url . Yii::$app->getRequest()->getLangUrl())?>
        </li>
    <?php endforeach;?>
  </ul>
</li>