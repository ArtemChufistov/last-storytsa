<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\ingamo\FrontAppAssetTop;
use app\assets\ingamo\FrontAssetBottom;
use app\modules\menu\widgets\MenuWidget;

FrontAppAssetTop::register($this);
FrontAssetBottom::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>

  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>

  <?php $this->head() ?>

</head>
<body>
  <?php $this->beginBody() ?>

    <div class="preloader">
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p><?php echo Yii::t('app', 'Загрзука...');?></p>
      </div>
    </div>

    <div class="page">
      <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']);?>

      <?php echo $content; ?>

      <footer class="section footer-2 novi-background bg-cover">
        <div class="container">
          <div class="row row-40">
            <div class="col-md-6 col-lg-3"><a class="footer-logo" href="/"><img src="/ingamo/images/logo-white-216x53.png" alt="" width="216" height="53"/></a>
              <p><?php echo Yii::t('app', 'Ingamo Fund. – это объединение предпринимателей, инвесторов, созидателей, космополитов и гениев.');?></p>
            </div>
            <div class="col-md-6 col-lg-3">
              <h5 class="title">Contact Information</h5>
              <ul class="contact-box">
                <li>
                  <div class="unit unit-horizontal unit-spacing-xxs">
                    <div class="unit-left"><span class="icon novi-icon mdi mdi-email-outline"></span></div>
                    <div class="unit-body"><a href="mailto:#">support@ingamo.fund</a></div>
                  </div>
                </li>
              </ul>
              <div class="group-md group-middle social-items">
                <a class="icon novi-icon icon-md novi-icon mdi mdi-facebook" href="#"></a>
                <a class="icon novi-icon icon-md novi-icon mdi mdi-twitter" href="#"></a>
                <a class="icon novi-icon icon-md novi-icon mdi mdi-instagram" href="#"></a>
                <?php /*
                <a class="icon novi-icon icon-md novi-icon mdi mdi-facebook-messenger" href="#"></a>
                <a class="icon novi-icon icon-md novi-icon mdi mdi-linkedin" href="#"></a>
                <a class="icon novi-icon icon-md novi-icon mdi mdi-snapchat" href="#"></a>
                */ ?>
              </div>
            </div>
            <?php /*
            <div class="col-md-6 col-lg-3">
              <h5 class="title">Gallery</h5>
              <ul class="instafeed instagram-gallery" data-lightgallery="group">
                <li><a class="instagram-item" data-lightgallery="item" href="/ingamo/images/insta-gallery-1-original.jpg"><img src="/ingamo/images/insta-gallery-1-72x72.jpg" alt="" width="72" height="72"/></a>
                </li>
                <li><a class="instagram-item" data-lightgallery="item" href="/ingamo/images/insta-gallery-2-original.jpg"><img src="/ingamo/images/insta-gallery-2-72x72.jpg" alt="" width="72" height="72"/></a>
                </li>
                <li><a class="instagram-item" data-lightgallery="item" href="/ingamo/images/insta-gallery-3-original.jpg"><img src="/ingamo/images/insta-gallery-3-72x72.jpg" alt="" width="72" height="72"/></a>
                </li>
                <li><a class="instagram-item" data-lightgallery="item" href="/ingamo/images/insta-gallery-4-original.jpg"><img src="/ingamo/images/insta-gallery-4-72x72.jpg" alt="" width="72" height="72"/></a>
                </li>
                <li><a class="instagram-item" data-lightgallery="item" href="/ingamo/images/insta-gallery-5-original.jpg"><img src="/ingamo/images/insta-gallery-5-72x72.jpg" alt="" width="72" height="72"/></a>
                </li>
                <li><a class="instagram-item" data-lightgallery="item" href="/ingamo/images/insta-gallery-6-original.jpg"><img src="/ingamo/images/insta-gallery-6-72x72.jpg" alt="" width="72" height="72"/></a>
                </li>
              </ul>
            </div>
            */ ?>
           <?php /*
            <div class="col-md-6 col-lg-3">
              <h5 class="title">Newsletter</h5>
              <p>Keep up with our always upcoming news and updates. Enter your e-mail and subscribe to our newsletter.</p>
              <!-- RD Mailform-->
              <form class="rd-form form-sm rd-mailform" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                <div class="form-wrap">
                  <input class="form-input" id="newsletter-creative-email" type="email" name="email" data-constraints="@Email @Required">
                  <label class="form-label" for="newsletter-creative-email">Enter your e-mail</label>
                </div>
                <button class="button button-secondary" type="submit">Subscribe</button>
              </form>
            </div>
            */ ?>
          </div>
          <!-- Rights-->
          <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span>&nbsp;</span><span>Ingamo Fund</span><span>&nbsp;</span></p>
        </div>
      </footer>
    </div>
    <div class="snackbars" id="form-output-global"></div>

  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>