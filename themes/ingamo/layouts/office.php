<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\LayoutComponent;
use app\modules\menu\widgets\MenuWidget;
use app\assets\ingamo\OfficeAssetTop;
use app\assets\ingamo\OfficeAssetBottom;
use app\modules\profile\widgets\VerifyEmailWidget;

OfficeAssetTop::register($this);
OfficeAssetBottom::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta_profile');?>

    <script>(function(w,d,u){w.readyQ=[];w.bindReadyQ=[];function p(x,y){if(x=="ready"){w.bindReadyQ.push(y);}else{w.readyQ.push(x);}};var a={ready:p,bind:p};w.$=w.jQuery=function(f){if(f===d||f===u){return a}else{p(f)}}})(window,document)</script>
</head>
<body class="light">
    <?php $this->beginBody() ?>
    <!-- Pre loader -->
    <div id="loader" class="loader">
        <div class="plane-container">
            <div class="preloader-wrapper small active">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
                </div>

                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
                </div>

                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
                </div>

                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div id="app">
        <?php echo MenuWidget::widget(['menuName' => 'profileMenu', 'view' => 'office-menu']);?>
        <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/_office-right-sidebar')); ?>
        <div class="page has-sidebar-left height-full">
            <?php echo Yii::$app->controller->renderPartial(LayoutComponent::themePath('/partial/_office-title', ['title' => $this->title])); ?>
            <?php echo $content;?>
        </div>
    </div>

    <?= VerifyEmailWidget::widget(['user' => $this->params['user']->identity]); ?>

    <div class="control-sidebar-bg shadow white fixed"></div>

    <?php $this->endBody() ?>

    <script>(function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)</script>

</body>
</html>
<?php $this->endPage() ?>