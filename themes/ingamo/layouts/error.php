<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'] = [[
    'label' => $message,
    'url' => '/404',
]];
$this->title = $message;
?>

<section class="section one-screen-page bg-primary-gradient">
  <div class="one-screen-page-inner">
    <div class="page-header">
      <div class="container">
      </div>
    </div>
    <div class="page-content">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-9 col-lg-7 col-xl-6">
            <div class="one-screen-page-box">
              <div class="page-title"><?php echo $code;?></div>
              <h2 class="page-subtitle"><?php echo $name;?></h2>
              <p class="heading-6 page-description"><?=$message;?></p><a class="button button-lg button-secondary" href="/"><?php echo Yii::t('app', 'Вернуться на главную');?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="page-footer">
      <div class="container">
        <!-- Rights-->
        <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span>&nbsp;</span><span>PixLab</span><span>&nbsp;</span><a href="privacy-policy.html">Privacy Policy.</a>Design&nbsp;by&nbsp;<a href="https://zemez.io/">Zemez</a></p>
      </div>
    </div>
  </div>
</section>