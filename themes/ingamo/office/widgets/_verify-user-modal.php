<?php
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
?>

<style>
  .email_is_send .help-block{
    font-size: 17px;
    color: green;
  }
</style>

<div class="modal modalNotVerifiedUser fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <?php $form = ActiveForm::begin([
          'id' => 'form-profile',
          'options' => [
              'class' => 'form',
              'enctype' => 'multipart/form-data',
          ],

      ]);?>

        <div class="modal-header">
          <div style = "width: 90%;" class="modal-title"><?php echo Yii::t('app', 'Внимание'); ?></div>
        </div>
        <div class="modal-body">
        <?php if (empty($changeEmailForm->getErrors()['email_is_send'])):?>
          <p><?php echo Yii::t('app', 'Вам необходимо подтвердить свой <strong>E-mail</strong>. После подтверждения Вам будет доступна полная версия кабинета'); ?></p>

            <?=$form->field($changeEmailForm, 'changing_email')->textInput([
                'maxlength' => true,
                'value' => $user->changing_email == '' ? $user->email : $user->changing_email,
                'placeholder' => $changeEmailForm->getAttributeLabel('changing_email'),
            ])?>

        <?php endif;?>

        <?=$form->field($changeEmailForm, 'email_is_send', [
          'template' => '<div class = "email_is_send">{error}</div>'
        ])->hiddenInput()->label('');?>

      </div>
      <div class="modal-footer">

        <?php if (empty($changeEmailForm->getErrors()['email_is_send'])):?>

          <?=Html::submitButton('<i class="glyphicon glyphicon-send"></i> ' . Yii::t('app', 'Отправить код подтверждения'), [
            'class' => 'btn btn-primary',
            'value' => 1,
            'name' => (new \ReflectionClass($changeEmailForm))->getShortName() . '[send_mail_submit]'])?>

        <?php endif;?>

        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть'); ?></button>
      </div>
      <?php ActiveForm::end();?>
    </div>
  </div>
</div>

<?php $this->registerJs("
function showModalNotVerrified()
{
  jQuery('.modalNotVerifiedUser').modal('show');
}

jQuery(function() {
  showModalNotVerrified();
});
", View::POS_END);?>