<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
?>
<div class="card" style="margin-bottom: 20px;">
   <div class="card-header white">
       <h6><?php echo Yii::t('app', 'Вы можете пригласить своих друзей, указав их E-mail через запятую');?> <i class="icon icon-share-alt s-18" style="float: right;"></i></h6>
   </div>
   <div class="card-body">
      <?php $form = ActiveForm::begin(['enableClientScript' => false]); ?>

      <div class = "form-group">
        <?= $form->field($inviteForm, 'separate_emails')->textInput(['class' => 'form-control', 'value' => '', 'placeholder' => Yii::t('app', 'Список E-mail через запятую')])->label(''); ?>
      </div>

      <div class = "form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-envelope"></i> '.Yii::t('app', 'Пригласить'), ['class' => 'btn btn-success waves-effect waves-light m-r-10']) ?>
      </div>

      <?php ActiveForm::end();?>
   </div>
</div>

<?php $successMessage = Yii::$app->session->getFlash('successInviteByEmail');?>

<?php if (!empty($successMessage)):?>

<div class="modal fade successModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div style = "width: 90%; font-weight: bold;" class="modal-title"><?php echo Yii::t('app', 'Внимание !');?></div>
        </div>
        <div class="modal-body">
        <?php echo $successMessage;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

  <?php $this->registerJs("
      jQuery('.successModal').modal('show');
  ", View::POS_END);?>
<?php endif;?>