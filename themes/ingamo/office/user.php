<?php
use borales\extensions\phoneInput\PhoneInput;
use app\modules\profile\models\User;
use kartik\widgets\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::t('user', 'Профиль');
?>

<?php $form = ActiveForm::begin([
    'enableClientScript' => false,
    'options' => ['class' => 'form-material', 'enctype'=>'multipart/form-data',],
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ],
    ],
]); ?>

<div class="container-fluid relative animatedParent animateOnce">
    <div class="tab-content pb-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <div class="col-md-4">

                    <div class="card no-b shadow text-center p-5" style="margin-bottom: 20px;">
                        <div class="image" style="position: relative;">

                            <?php if ($user->image):?>
                                <img src="<?php echo $user->getImage();?>" class="user_avatar no-b no-p r-5" alt="<?php echo $user->login;?>">
                                <br/>
                                <?php echo Html::a(Yii::t('app', 'Удалить фото'), ['/profile/office/remove'], ['class' => 'btn btn-success btn-sm mt-3']);?>
                            <?php else: ?>
                                <?php echo  $form->field($user, 'photo', ['options' => ['class' => 'custom-file-input']])
                                  ->fileInput(['class' => 'custom-file-input'])
                                  ->label(Yii::t('app', 'Choose file...'), ['class' => 'custom-file-label']); ?>
                                <br/>
                                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                                'class' => 'btn btn-primary btn-sm pl-4 pr-4',
                                'name' => 'save-button']) ?>
                            <?php endif; ?>
                        </div>
                        <div>
                            <h6 class="p-t-10"><?php echo $user->login;?></h6>
                            <?php echo $user->email;?>
                        </div>
                    </div>

                    <div class="card">
                       <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Сменить пароль');?> <i class="icon icon-sync s-18" style="float: right;"></i></h6>
                       </div>
                       <div class="card-body">

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <?= $form->field($user, 'old_password')->passwordInput()->label($user->getAttributeLabel('old_password'),['class'=>'form-label']); ?>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <?= $form->field($user, 'password')->passwordInput()->label($user->getAttributeLabel('password'),['class'=>'form-label']); ?>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <?= $form->field($user, 'repeat_password')->passwordInput()->label($user->getAttributeLabel('repeat_password'),['class'=>'form-label']); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                                    'class' => 'btn btn-primary btn-sm pl-4 pr-4',
                                    'value' => 1,
                                    'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card">
                       <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Личные данные');?> <i class="icon icon-user s-18" style="float: right;"></i></h6>
                       </div>
                       <div class="card-body">

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <?= $form->field($user, 'first_name')->textInput()->label($user->getAttributeLabel('first_name'),['class'=>'form-label']); ?>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <?= $form->field($user, 'last_name')->textInput()->label($user->getAttributeLabel('last_name'),['class'=>'form-label']); ?>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <?= $form->field($user, 'skype')->textInput()->label($user->getAttributeLabel('skype'),['class'=>'form-label']); ?>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <?= $form->field($user, 'phone')->textInput()->label($user->getAttributeLabel('phone'),['class'=>'form-label']); ?>
                                </div>
                            </div>
<!--
                            <div class="form-group form-float">
                                <div class="form-line">
                                  <label class="radio-head"><?php echo Yii::t('app', 'Пол');?></label>
                                  <?= $form->field($user, 'sex')
                                      ->radioList(
                                          User::getSexArray(),
                                          [
                                              'item' => function($index, $label, $name, $checked, $value) {

                                                  $checked = $checked == true ? 'checked="checked"' : '';
                                                  $return = '<label class="radio-inline">';
                                                  $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"  ' . $checked . ' >';
                                                  $return .= '<i></i>';
                                                  $return .= '<span>' . ucwords($label) . '</span>';
                                                  $return .= '</label>';

                                                  return $return;
                                              }
                                          ]
                                      )
                                  ->label(false);?>
                                </div>
                            </div>
-->
                            <div class="form-group">
                                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                                    'class' => 'btn btn-primary btn-sm pl-4 pr-4',
                                    'value' => 1,
                                    'name' => (new \ReflectionClass($user))->getShortName() . '[save]']) ?>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card">
                       <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Сменить E-mail');?> <i class="icon icon-mail_outline s-18" style="float: right;"></i></h6>
                       </div>
                       <div class="card-body">

                        <div class="form-group form-float">
                          <div class="form-line">
                            <?= $form->field($user, 'email')->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                                'placeholder' => $user->getAttributeLabel('email')
                            ])->label($user->getAttributeLabel('email'),['class'=>'form-label']) ?>
                          </div>
                        </div>
                        <div class="form-group form-float">
                          <div class="form-line">
                            <?= $form->field($user, 'email_confirm_token', ['options' => [
                                'style' => $oldUser->email_confirm_token == '' ? 'display:none;' : 'display:block;']])->textInput([
                                'maxlength' => true,
                                'value' => '',
                            ])->label($user->getAttributeLabel('email_confirm_token'),['class'=>'form-label']) ?>
                          </div>
                        </div>

                        <?php if ($oldUser->email_confirm_token == ''):?>
                            <div class="form-group form-float">
                                  <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сменить E-mail'), [
                                      'class' => 'btn btn-primary btn-sm pl-4 pr-4',
                                      'value' => 1,
                                      'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                            </div>
                        <?php else:?>
                          <div class="form-group form-float">
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сменить E-mail'), [
                                'class' => 'btn btn-primary btn-sm pl-4 pr-4',
                                'value' => 1,
                                'name' =>  (new \ReflectionClass($user))->getShortName() . '[confirm_token_submit]']) ?>
                          </div>
                          <div class="form-group form-float">
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить повторно код подтверждения'), [
                              'class' => 'btn btn-primary btn-sm pl-4 pr-4',
                              'value' => 1,
                              'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                            </div>
                        <?php endif;?>
                       </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
