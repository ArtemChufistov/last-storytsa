<?php
use app\modules\support\models\TicketDepartment;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Техническая поддержка');

$departments = TicketDepartment::find()->all();

$departmentArray = [];
foreach($departments as $department){
  $departmentArray[$department->id] = Yii::t('app', $department->name);
}
?>

<div class="container-fluid relative animatedParent animateOnce">
    <div class="tab-content pb-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <div class="col-md-6">
                    <div class="card" style="margin-bottom: 20px;">
                       <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Информация');?> <i class="icon icon-help s-18" style="float: right;"></i></h6>
                       </div>
                       <div class="card-body">
                        <?php echo Yii::t('app', '<p style = "text-align: left;"><strong style = "text-align: left;">Внимание! ответ может занять до 24 часов.</strong></p><p style = "text-align: left;">Здесь вы можете задавать вопросы администрации проекта, или внести своё предложение по улучшению сервиса, а так-же дополнительным разделам личного кабинета.</p><p style = "text-align: left;"> Возможно у Вас возникли трудности технического характера, если так, то прежде чем их задавать попробуйте найти решение в разделе <strong>FAQ</strong>.</p><p style = "text-align: left;"> Если-же вы всеравно не нашли решение своей проблемы, то обратитесь к нам и мы постараемся в кратчайшие сроки помочь. Просьба относится с внимательностью к выбору отдела, так как это упростит сотрудничество</p>');?>
                       </div>
                    </div>
                    <div class="card">
                       <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Создать тикет');?> <i class="icon icon-cog s-18" style="float: right;"></i></h6>
                       </div>
                       <div class="card-body">
                            <?php $form = ActiveForm::begin([
                                'enableClientScript' => false,
                                'options' => ['class' => 'form-material', 'enctype'=>'multipart/form-data',],
                                'fieldConfig' => [
                                    'options' => [
                                        'tag' => false,
                                    ],
                                ]]); ?>

                                <?php echo $form->field($ticket, 'department')->dropDownList($departmentArray, ['class' => 'select2']);?>

                                <?php echo $form->field($ticket, 'text')->textArea(['rows' => '4', 'value' => '', 'class' => 'textarea form-control']); ?>

                                <div class="form-group" style="margin-top: 20px;">
                                    <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить'), [
                                        'class' => 'btn btn-primary btn-sm pl-4 pr-4',
                                        'value' => 1,
                                        'name' => (new \ReflectionClass($user))->getShortName() . '[save]']) ?>
                                </div>

                            <?php ActiveForm::end();?>
                       </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card" style="margin-bottom: 20px;">
                       <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Ваши тикеты');?> <i class="icon icon-th-list s-18" style="float: right;"></i></h6>
                       </div>
                       <div class="card-body">
                            <table class="table ticketTable">
                              <tr>
                                <th style="width: 10px">№</th>
                                <th><?php echo Yii::t('app', 'Дата');?></th>
                                <th><?php echo Yii::t('app', 'Статус');?></th>
                                <th><?php echo Yii::t('app', 'Отдел');?></th>
                                <th><?php echo Yii::t('app', 'Новых (Всего)');?></th>
                              </tr>
                              <?php foreach(array_reverse($ticketArray) as $ticket):?>
                                <tr ticketId="<?php echo $ticket->id;?>">
                                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo $ticket->id;?>.</a></td>
                                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo date('d-m-Y', strtotime($ticket->dateAdd));?></a></td>
                                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo Yii::t('app', $ticket->getStatus()->one()->titleName());?></a></td>
                                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>"><?php echo Yii::t('app', $ticket->getDepartment()->one()->name);?></a></td>
                                  <td><a href = "<?php echo Url::to('/office/support/' . $ticket->id);?>">
                                    <?php if (count($ticket->getNotReadMessages($user->id)) > 0):?>
                                      <span class="label label-success label-rouded"><?php echo count($ticket->getNotReadMessages($user->id));?> (<?php echo $ticket->getMessages()->count();?>)</span></a>
                                    <?php else:?>
                                      <span class="label label-warning label-rounded"><?php echo count($ticket->getNotReadMessages($user->id));?> (<?php echo $ticket->getMessages()->count();?>)</span></a>
                                    <?php endif;?>
                                  </td>
                                </tr>
                              <?php endforeach;?>
                            </table>
                       </div>
                    </div>
                  <?php if (!empty($currentTicket)):?>

                    <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/office/partial/_support-message', [
                      'ticketMessages' => $ticketMessages,
                      'ticketMessage' => $ticketMessage,
                      'currentTicket' => $currentTicket,
                      'user' => $user
                      ]);?>

                  <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
  .select2-container--default .select2-search--dropdown::before{
    content: "";
  }
</style>