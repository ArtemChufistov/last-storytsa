<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>

<div class="card" style="margin-bottom: 20px;">
  <div class="card-header white">
    <h6><?php echo Yii::t('app', 'Тикет №{id}', ['id' => $currentTicket->id]);?></h6>
  </div>
  <div class="card-body">
    <div class="form-group">
      <label class="control-label" ><?php echo Yii::t('app', 'Отдел');?></label>
      <pre class="form-control" ><?php echo Yii::t('app', $currentTicket->getDepartment()->one()->name);?></pre>
    </div>
    <div class="form-group">
      <label class="control-label" ><?php echo Yii::t('app', 'Текст');?></label>
      <pre class="form-control" style = "height: 80px;"><?php echo $currentTicket->text;?></pre>
    </div>
  </div>
</div>

<div class="card" style="margin-bottom: 20px;">
  <div class="card-header white">
    <h6>
      <?php echo Yii::t('app', 'Чат с');?>
      <?php if ($currentTicket->getDepartment()->one()->id == 1):?>
        <?php echo Yii::t('app', 'поддержкой');?>
      <?php elseif($currentTicket->getDepartment()->one()->id == 2):?>
        <?php echo Yii::t('app', 'администрацией');?>
      <?php endif;?>
    </h6>
    <div class="card-body">
      <div class="chat-box" style="height: 510px;border: 1px solid beige; overflow-y: scroll;">
          <div class="card-body  chat-widget p-3 slimScroll" data-height="600">
              <div class="w-body w-scroll ">
                  <ul class="list-unstyled">

                    <?php foreach(array_reverse($ticketMessages) as $ticketMessageItem): ?>
                      <li <?php if ($ticketMessageItem->getUser()->one()->id != $user->id):?>class="by-other"<?php else:?>class="by-me"<?php endif;?>>

                          <div class="avatar float-left">
                              <!-- Online or offline -->
                              <b class="c-idle"></b>
                              <img src="<?php echo $ticketMessageItem->getUser()->one()->getImage();?>" alt="" class="img-responsive">
                              <!-- Name -->
                              <span><?php echo $ticketMessageItem->getUser()->one()->login;?></span>
                          </div>
                          <div class="chat-content">
                              <!-- In meta area, first include "name" and then "time" -->
                              <div class="chat-meta"><?php echo Yii::t('app', 'Отправлено');?>
                                  <span class="float-right"><?php echo date('H:i d-m-Y', strtotime($ticketMessageItem->dateAdd));?></span>
                              </div>
                              <?php echo $ticketMessageItem->text;?>
                              <div class="clearfix"></div>
                          </div>
                      </li>
                    <?php endforeach;?>
                </ul>
          </div>
        </div>
      </div>

      <?php $form = ActiveForm::begin([
          'enableClientScript' => false,
          'options' => ['class' => 'form-material', 'enctype'=>'multipart/form-data',],
          'fieldConfig' => [
              'options' => [
                  'tag' => false,
              ],
          ]]); ?>

      <?= $form->field($ticketMessage, 'text')->textInput([
        'maxlength' => true,
        'value' => '',
        'placeholder' => $ticketMessage->getAttributeLabel('text')
      ]) ?>

      <div class="form-group" style="margin-top: 20px;">
          <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary btn-sm pl-4 pr-4']) ?>
      </div>

      <?php ActiveForm::end();?>

  </div>
  </div>
</div>
<script type="text/javascript">

$( document ).ready(function() {
  $(".chat-list").animate({ scrollTop: $(".chat-list")[0].scrollHeight}, 1000);
});
</script>