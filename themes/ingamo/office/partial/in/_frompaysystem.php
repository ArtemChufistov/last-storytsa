<?php
use app\modules\finance\components\paysystem\TopexchangeComponent;
use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Финансы - Пополнение личного счёта');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$paysystemTopExchange = PaySystem::find()->where(['key' => PaySystem::KEY_TOP_EXCHANGE])->one();
$exchangeCurrencyArray = TopexchangeComponent::getCurrencyCourseList();

$sumInUsd = $paymentForm->to_sum * 1;

//echo '<pre>';print_r($exchangeCurrencyArray);exit;
?>

<style>
.pay-sys-item{
  padding-top: 20px;
}
</style>

<div class="col-md-12">
  <div class="card">
    <div class="card-header white">
      <h6><?php echo Yii::t('app', 'Выберите платёжную систему, в которой будете производить расчёт');?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
    </div>
  <div class="card-body">
    <?php $form = ActiveForm::begin([
      'method' => 'get',
      'enableClientScript' => false,
      'options' => ['class' => 'form-material', 'enctype'=>'multipart/form-data',],
      'fieldConfig' => [
          'options' => [
              'tag' => false,
          ],
      ],
    ]); ?>

        <?php $paySystemFinded = false;?>
        <?php foreach ($exchangeCurrencyArray as $exchangeCurrency): ?>

            <?php $paySystemFinded = true;?>

            <?php if ($sumInUsd > $exchangeCurrency['max_summ']): ?>

              <span class = "btn btn-default pay-sys-item">
                <img class = "paySysIcon" src = "<?php echo $exchangeCurrency['send_paysys_icon']; ?>">
                <p><?php echo $exchangeCurrency['send_paysys_title']; ?></p>
                <p>
                  <strong>
                    <?php echo Yii::t('app', 'Макс. сумма:'); ?> <?php echo $exchangeCurrency['max_summ']; ?> <?php echo $paymentForm->getFromCurrency()->one()->key;?>
                    <?php echo strtoupper($exchangeCurrency['send_paysys_valute']); ?>
                  </strong>
                </p>
              </span>

            <?php elseif ($sumInUsd < $exchangeCurrency['min_summ']): ?>

              <span class = "btn btn-default pay-sys-item">
                <img class = "paySysIcon" src = "<?php echo $exchangeCurrency['send_paysys_icon']; ?>">
                <p><?php echo $exchangeCurrency['send_paysys_title']; ?></p>
                <p>
                  <strong>
                    <?php echo Yii::t('app', 'Мин. сумма для покупки:'); ?> <?php echo $exchangeCurrency['min_summ']; ?> <?php echo $paymentForm->getFromCurrency()->one()->key;?>
                  </strong>
                </p>
              </span>

            <?php else: ?>

              <?=Html::submitButton('
                <img class = "paySysIcon" src = "' . $exchangeCurrency['send_paysys_icon'] . '">
                <p>' . $exchangeCurrency['send_paysys_title'] . '</p>
                ', ['value' => $exchangeCurrency['send_paysys_identificator'],
                'name' => StringHelper::basename(get_class($paymentForm)) . '[sub_pay_system]',
                'class' => 'btn btn-default pay-sys-item'])?>

            <?php endif;?>
        <?php endforeach;?>

        <?php if (!empty($paymentForm->getErrors())):?>
          <?php foreach($paymentForm->getErrors() as $error):?>
            <?php echo $error[0];?>
          <?php endforeach;?>

        <?php elseif ($paySystemFinded == false): ?>
          <span class = "btn btn-default">
            <p>
              <?php echo Yii::t('app', '<strong>Приносим извинения</strong></br>В данный момент расчёты в этой валюте закрыты.</br><a href = "{link}"> Выберите другую валюту<a>', ['link' => Url::to(['office/in', StringHelper::basename(get_class($paymentForm)) . '[to_sum]' => $paymentForm->to_sum])]); ?>
            </p>
          </span>
        <?php endif;?>

        <?= $form->field($paymentForm, 'from_pay_system_id')->hiddenInput(['value' => $paysystemTopExchange->id])->label('');?>

      <?php ActiveForm::end();?>
      <a class="btn btn-info" href="/office/in" style="margin-top: 15px;"><?php echo Yii::t('app', 'Вернуться назад');?></a>
    </div>
  </div>
</div>

<style>
    .pay-sys-item {
        margin-left: 6px;
        padding-top: 20px;
        width: 186px;
        height: 135px;
        margin-bottom: 10px;
    }
</style>