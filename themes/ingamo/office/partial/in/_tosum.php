<?php
use app\modules\finance\models\Payment;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Пополнение личного кабинета');
$sum = 1000;
if (!empty($_GET['sum'])){
  $sum = $_GET['sum'];
}

?>

<div class="col-md-12">
    <div class="card">
       <div class="card-header white">
           <h6><?php echo Yii::t('app', 'Укажите сумму, на которую хотите пополнить личный кабинет');?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
       </div>
       <div class="card-body">
          <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

            <?=$form->field($paymentForm, 'to_sum', ['template' => '<label class="col-sm-3 control-label">' . Yii::t('app', 'Пополняемая сумма:') . '</label><div class="col-sm-4">{input}{error}</div>'])->textInput(['class' => 'form-control summBuySitt', 'value' => $sum])?>

            <div class="form-group">
              <div class="col-sm-3">
              </div>
              <div class="col-sm-4">
                <?php echo Html::submitButton(Yii::t('app', 'Далее'), ['class' => 'btn btn-success'])?>
                <a class="btn btn-info" href="/office/finance"><?php echo Yii::t('app', 'Вернуться назад');?></a>
              </div>
            </div>

          <?php ActiveForm::end();?>
       </div>
    </div>
</div>