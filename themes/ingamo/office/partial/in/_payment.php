<div class="col-md-12">
    <div class="card">
        <div class="card-header white">
           <h6><?php echo Yii::t('app', 'Ваш платёж создан, передите к оплате');?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
        </div>
        <div class="card-body">
            <?php echo Yii::$app->controller->renderPartial(
            '@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_successpay_' . $paymentForm->getFromPaySystem()->one()->key, [
                'paymentForm' => $paymentForm,
                'user' => $user,
            ]); ?>
        </div>
    </div>
</div>