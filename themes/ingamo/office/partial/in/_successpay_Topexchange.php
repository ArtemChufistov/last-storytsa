<?php
use app\modules\finance\models\PaySystem;
use app\modules\mainpage\models\Preference;

$prefExchangeName = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_NAME])->one();
$prefExchangeSuccessUrl = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_SUCCESS_URL])->one();
$prefExchangeErrorUrl = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_ERROR_URL])->one();

$exchangeInfo = json_decode($paymentForm->additional_info, true);

$paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();

?>
<div class="row">
    <div class="col-lg-3">
        <div class="dummy-logo">
            <img src = "/ingamo/img/basic/logo.png" style = "width: 100px;">
            <p style = "font-size: 14px;">INGAMO FUND</p>
        </div>
    </div>
    <div class="col-lg-4">
        <h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Информация'); ?></h2>
        <ul class="reset-ul">
            <li>
                <b><?php echo Yii::t('app', 'Сумма'); ?>:</b> <strong style = "font-size: 22px;"> <?php echo $paymentForm->to_sum; ?> <?php echo $paymentForm->getToCurrency()->one()->key; ?></strong>
            </li>
            <li>
                <b><?php echo Yii::t('app', 'Платёжная система'); ?>:</b>
                <strong style = "font-size: 22px;"> <?php echo $exchangeInfo['send_paysys_title']; ?> </strong>
            </li>
        </ul>
    </div>
    <div class="col-lg-5 float-right text-right">
        <form name="send_form" action="http://top-exchange.com/merchant_pay" method="post">
            <h4 class="invoice-title"><?php echo Yii::t('app', 'Оплата'); ?></h4>
            <b>№ <?php echo $paymentForm->hash; ?></b>

            <div class="divider"></div>
            <div class="invoice-date mrg20B"><?php echo date('H:i d-m-Y', strtotime($paymentForm->date_add)); ?></div>

            <input type="hidden" name="merchant_name" value="<?php echo $prefExchangeName->value; ?>" />
            <input type="hidden" name="merchant_title" value="ingamo.fund" />
            <input type="hidden" name="payed_paysys" value="<?php echo $exchangeInfo['send_paysys_identificator']; ?>" />
            <input type="hidden" name="amount" value="<?php echo $paymentForm->from_sum + $paymentForm->comission_from_sum; ?>" />
            <input type="hidden" name="payment_info" value="<?php echo Yii::t('app', 'Пополнение Лк участник: {login}', ['login' => $user->login]) ?>" />
            <input type="hidden" name="payment_num" value="<?php echo $paymentForm->hash; ?>" />
            <input type="hidden" name="sucess_url" value="<?php echo $prefExchangeSuccessUrl->value; ?>" />
            <input type="hidden" name="error_url" value="<?php echo $prefExchangeErrorUrl->value; ?>" />

            <button class="btn btn-alt btn-hover btn-info" type="submit">
                <span><?php echo Yii::t('app', 'Перейти к оплате'); ?></span>
                <i class="glyph-icon icon-play"></i>
            </button>
            <button class="btn btn-alt btn-hover btn-danger">
                <span><?php echo Yii::t('app', 'Отменить'); ?></span>
                <i class="glyph-icon icon-trash"></i>
            </button>
        </form>
    </div>
</div>
