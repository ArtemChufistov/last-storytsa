<div class="col-md-12">
    <div class="card">
        <div class="card-header white">
           <h6><?php echo Yii::t('app', 'Ваш вывод сформирован');?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
        </div>
        <div class="card-body">
            <?php echo Yii::$app->controller->renderPartial(
            '@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_successpay_' . $paymentForm->getToPaySystem()->one()->key, [
                'paymentForm' => $paymentForm,
                'user' => $user,
            ]); ?>
        </div>
    </div>
</div>