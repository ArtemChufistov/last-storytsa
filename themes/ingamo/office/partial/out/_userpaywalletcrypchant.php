<?php
use app\modules\finance\widgets\PayWalletPasswordWidget;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="col-md-12">
  <div class="card">
    <div class="card-header white">
      <h6><?php echo Yii::t('app', 'Укажите ваш кошелёк <strong>BitCoin</strong>'); ?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
    </div>
    <div class="card-body">
      <div class="content-box-wrapper">
        <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']], 'enableClientScript' => false]);?>

          <?=$form->field($paymentForm, 'wallet', ['template' => '<label class="col-sm-3 control-label">' . Yii::t('app', 'Платёжный кошелёк:') . '</label><div class="col-sm-4">{input}{error}</div>'])->textInput(['value' => $paymentForm->wallet, 'class' => 'form-control'])?>

          <?=$form->field($paymentForm, 'pay_password', ['template' =>'<label class="col-sm-3 control-label">' . Yii::t('app', 'Платёжный пароль:') . '</label>
          <div class="col-sm-4">{input}{error}</div>', ])->textInput(['value' => $paymentForm->pay_password, 'class' => 'form-control'])?>

          <div class="form-group">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-4">
              <?=Html::submitButton(Yii::t('app', 'Далее'), ['class' => 'btn btn-info'])?>
            </div>
          </div>

        <?php ActiveForm::end();?>

        <?php echo PayWalletPasswordWidget::widget(['user' => $user]); ?>
      </div>
    </div>
  </div>
</div>