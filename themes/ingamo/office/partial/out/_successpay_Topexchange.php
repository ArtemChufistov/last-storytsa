<h2 class="invoice-client mrg10T"><?php echo Yii::t('app', 'Информация'); ?>:</h2>
<ul class="reset-ul">
    <li>
        <b><?php echo Yii::t('app', 'Списываемая сумма'); ?>:</b> <strong style = "font-size: 22px;"> <?php echo number_format($paymentForm->fromSumWithComission(), 2); ?> <?php echo $paymentForm->getFromCurrency()->one()->title; ?></strong>
    </li>
    <li>
        <b><?php echo Yii::t('app', 'Получаемая сумма'); ?>:</b>
        <strong style = "font-size: 22px;"><?php echo number_format($paymentForm->to_sum - $paymentForm->comission_to_sum, 6); ?> <?php echo $paymentForm->getToCurrency()->one()->key; ?></strong>
    </li>
    <li>
        <b><?php echo Yii::t('app', 'Кошелёк для оплаты'); ?>:</b><strong style = "font-size: 22px;"> <?php echo $paymentForm->wallet; ?></strong>
    </li>
    <li>
        <b><?php echo Yii::t('app', 'Статус'); ?>:</b><strong style = "font-size: 22px;"> <?php echo $paymentForm->getStatusTitle(); ?></strong>
    </li>
</ul>

<a class="btn btn-info" href="/office/finance" style="margin-top: 15px;"><?php echo Yii::t('app', 'Перейти в финансы');?></a>