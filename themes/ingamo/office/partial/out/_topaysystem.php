<?php
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Снятие личного кабинета');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$btnColors = ['success', 'info', 'blue-alt', 'primary', 'purple', 'azure'];

?>

<div class="col-md-12">
  <div class="card">
    <div class="card-header white">
      <h6><?php echo Yii::t('app', 'Выберите валюту'); ?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
    </div>
    <div class="card-body">
      <div class="content-box-wrapper">
        <?php if (!empty($paymentForm->getErrors())):?>
          <?php foreach($paymentForm->getErrors() as $errors):?>
            <?php foreach($errors as $error):?>
              <?php echo $error;?>
            <?php endforeach;?>
          <?php endforeach;?>
        <?php endif ;?>
        <br/>
        <a class="btn btn-info" href="/office/out" style="margin-top: 15px;"><?php echo Yii::t('app', 'Вернуться назад');?></a>
      </div>
    </div>
  </div>
</div>