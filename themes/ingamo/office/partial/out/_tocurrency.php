<?php
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
use app\modules\finance\components\paysystem\TopexchangeComponent;

$this->title = Yii::t('user', 'Снятие личного кабинета - Выбор валюты');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$btnColors = ['success', 'info', 'blue-alt', 'primary', 'purple', 'azure'];

$accountBalances = TopexchangeComponent::getAccountBalances();
?>

<div class="col-md-12">
    <div class="card">
        <div class="card-header white">
            <h6><?php echo Yii::t('app', 'Выберите платёжную систему, в которую производите снятие');?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
        </div>
        <div class="card-body">
            <?php foreach($accountBalances as $balance):?>
                <?php foreach($balance['for_payout'] as $currency):?>
                    <span class = "btn btn-default pay-sys-item">
                        <img class = "paySysIcon" src = "<?php echo $currency['icon']; ?>">
                        <p><?php echo $currency['full_title']; ?></p>
                    </span>
                <?php endforeach;?>
            <?php endforeach;?>
        </div>
    </div>
</div>

<div class="col-md-12">
  <div class="card">
    <div class="card-header white">
      <h6><?php echo Yii::t('app', 'Выберите валюту'); ?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
    </div>
    <div class="card-body">
      <div class="content-box-wrapper">
          <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

            <?php foreach ($currencyArray as $currency): ?>
              <?=Html::submitButton($currency->key, [
                'style' => 'margin: 15px;',
                'value' => $currency->id,
                'name' => StringHelper::basename(get_class($paymentForm)) . '[to_currency_id]',
                'class' => 'currencyButton btn btn-alt btn-hover btn-' . array_shift($btnColors)])?>
              <?php endforeach;?>

          <?php ActiveForm::end();?>
      </div>
    </div>
  </div>
</div>
<style>
    .pay-sys-item {
        margin-left: 6px;
        padding-top: 20px;
        width: 126px;
        height: 135px;
        margin-bottom: 10px;
    }
</style>