<?php
use app\modules\finance\components\paysystem\TopexchangeComponent;
use app\modules\finance\widgets\PayWalletPasswordWidget;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$accountBalances = TopexchangeComponent::getAccountBalances();

$this->title = Yii::t('user', 'Финансы - Снятие с личного счёта');

?>

<?php if(empty($_REQUEST['Payment']['sub_pay_system'])):?>
    <div class="col-md-12">
        <div class="card currency-wrap">
            <div class="card-header white">
                <h6><?php echo Yii::t('app', 'Выберите платёжную систему, в которую производите снятие');?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
            </div>
            <div class="card-body">
                <div style="display: flex;flex-wrap:wrap;justify-content: center;">
                    <?php foreach($accountBalances as $balance):?>
                        <?php foreach($balance['for_payout'] as $currency):?>
                            <span class = "btn btn-default pay-sys-item" key="<?php echo $currency['recive_currency_id'];?>">
                                <img class = "paySysIcon" src = "<?php echo $currency['icon']; ?>">
                                <p><?php echo $currency['full_title']; ?></p>
                            </span>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
<?php else:?>
    <?php foreach($accountBalances as $balance):?>
        <?php foreach($balance['for_payout'] as $currency):?>
            <?php if($currency['recive_currency_id'] == $paymentForm->sub_pay_system):?>
                <div class="col-md-6 wallet-wrap">
                    <div class="card">
                        <div class="card-header white">
                            <h6><?php echo Yii::t('app', 'Укажите данные вашего кошелька'); ?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
                        </div>
                        <div class="card-body">
                            <div class="content-box-wrapper">

                                <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']], 'enableClientScript' => false]);?>

                                <?php foreach($currency['fields'] as $field):?>
                                    <?php $fieldName = $field['field_name'];?>
                                    <?=$form->field($paymentForm, $field['field_name'], ['template' => '<label class="col-sm-3 control-label">' . Yii::t('app', $field['title']) . '</label><div class="col-sm-12">{input}{error}</div>'])
                                        ->textInput(['value' => $paymentForm->$fieldName, 'class' => 'form-control', 'placeholder' => $field['placeholder']])?>
                                <?php endforeach;?>

                                <?=$form->field($paymentForm, 'pay_password', ['template' =>'<label class="col-sm-3 control-label">' . Yii::t('app', 'Платёжный пароль:') . '</label><div class="col-sm-12">{input}{error}</div>', ])->textInput(['value' => $paymentForm->pay_password, 'class' => 'form-control'])?>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <?=Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-info'])?>
                                        <a class="btn btn-info" href="/office/finance"><?php echo Yii::t('app', 'Вернуться назад');?></a>
                                    </div>
                                </div>

                                <?php ActiveForm::end();?>

                                <?php echo PayWalletPasswordWidget::widget(['user' => $user]); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (!empty($paymentForm->getErrors())):?>
                    <div class="col-md-6 wallet-wrap">
                        <div class="card">
                            <div class="card-header white">
                                <h6><?php echo Yii::t('app', 'Проверьте указанные данные'); ?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
                            </div>
                            <div class="card-body">
                                <div class="content-box-wrapper">
                                    <?php foreach($paymentForm->getErrors() as $errorNum): ?>
                                        <?php foreach($errorNum as $error): ?>
                                            <?php echo $error;?>
                                        <?php endforeach;?>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            <?php endif;?>
        <?php endforeach;?>
    <?php endforeach;?>
<?php endif;?>

<?php $this->registerJs("
    $('.pay-sys-item').click(function() {
        var key = $(this).attr('key');
        location.href = '/office/out?" . http_build_query($_REQUEST) . "&Payment[sub_pay_system]=' + key;
    });
", View::POS_END);?>
<style>
    .pay-sys-item {
        margin-left: 6px;
        padding-top: 20px;
        width: 126px;
        height: 105px;
        margin-bottom: 10px;
        cursor: pointer;
    }
    .form-control::placeholder {
        color: #e1e8ee;
    }
    .help-block{
        margin-top: 5px;
        color: red;
    }
</style>