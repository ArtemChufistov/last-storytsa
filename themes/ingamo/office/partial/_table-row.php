<?php
use app\modules\profile\models\UserInfo;
?>
<tr <?php if ($level > 1 ):?>style="display:none;"<?php endif;?> parentLogin = "<?php echo $parent->email;?>" currentLogin = "<?php echo $child->email;?>" class="user-struct-info">
    <td>
        <?php for($i=1; $i<=$level;$i++){echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';};?>
        <?php if (!empty($child->children()->all())):?>
            <i class="icon icon-plus show-hide-children"></i>
        <?php elseif($level == 1 && empty($child->children()->all())):?>
            <?php echo '&nbsp;&nbsp;&nbsp;&nbsp;'?>
        <?php endif;?>
        <?php echo $child->login;?>
    </td>
    <td><?php echo $level;?></td>
    <td>
        <?php $userInfo = UserInfo::find()->where(['user_id' => $child->id])->one();?>
        <?php if (!empty($userInfo) && !empty($userInfo->self_invest)):?>
            <?php echo $userInfo->self_invest;?> USD
        <?php else:?>
            0
        <?php endif;?>
    </td>
    <td>
        <?php if (!empty($userInfo) && !empty($userInfo->struct_invest)):?>
            <?php echo $userInfo->struct_invest;?> USD
        <?php else:?>
            0
        <?php endif;?>
    </td>
</tr>