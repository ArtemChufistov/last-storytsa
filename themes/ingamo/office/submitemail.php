<?php
$this->title = Yii::t('user', 'Активация учётной записи');
?>

<main>
    <div id="primary" class="p-t-b-100 height-full">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mx-md-auto paper-card">
                    <div class="text-center">
                        <img src="assets/img/dummy/u4.png" alt="">
                        <h3 class="mt-2"><?php echo Yii::t('app', 'Ваш аккаунт успешно активирован');?></h3>
                        <p class="p-t-b-20"><?php echo Yii::t('app', 'Теперь вы можете воспользоваться полным функционалом личного кабинета');?></p>
                    </div>
                    <a href="/office/dashboard" class="btn btn-lg btn-block btn-social twitter"><?php echo Yii::t('app', 'Перейти в личный кабинет');?></a>
                </div>
            </div>
        </div>
    </div>
    <!-- #primary -->
</main>