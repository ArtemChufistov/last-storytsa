<?php
$this->title = Yii::t('user', 'Рекламные материалы');
?>

<div class="container-fluid relative animatedParent animateOnce">
    <div class="tab-content pb-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <div class="col-md-12">
                     <div class="card">
                        <div class="card-header white">
                            <?php echo Yii::t('app', 'Рекламные материалы');?>
                        </div>
                        <div class="card-body text-center">
                            <?php echo Yii::t('app', 'В разработке');?>
                            <?php /*
                            <embed src="/ingamo/images/INGAMO-EN-Min.pdf" width="100%" height="800" type="application/pdf">
                            <a class = "btn btn-success" href="/ingamo/images/INGAMO-EN-Min.pdf"><?php echo Yii::t('app', 'Link to Presentation');?></a>
                            */?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>