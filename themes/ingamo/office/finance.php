<?php
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Payment;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Финансы');

?>
<div class="container-fluid relative animatedParent animateOnce">
    <div class="tab-content pb-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <div class="col-md-6">
                    <div class="card" style = "margin-bottom: 20px;">
                       <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Операции со счётом');?> <i class="icon icon-dollar s-18" style="float: right;"></i></h6>
                       </div>
                       <div class="card-body">
                            <div class="text-center">
                                <h3 class="my-3"><?php echo Yii::t('app', 'Ваш баланс');?></h3>
                                <?php foreach ($user->getBalances() as $balance): ?>
                                  <h3 class="my-3"><?php echo $balance->value; ?> USD</h3>
                                <?php endforeach;?>
                                <a href="<?php echo Url::to(['/profile/office/in']);?>" class="btn btn-success btn-lg btn-block"><?php echo Yii::t('app', 'Пополнить');?></a>
                                <a href="<?php echo Url::to(['/profile/office/out']);?>" class="btn btn-primary btn-lg btn-block"><?php echo Yii::t('app', 'Снять');?></a>
                            </div>
                       </div>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $transactionProvider,
                        'filterModel' => $transaction,
                        'id' => 'transaction-grid',
                        'layout' => '
                            <div class="card">
                             <div class="card-header white">
                                 <h6>' . Yii::t('app', 'Движения по счёту') .'<i class="icon icon-th-list s-18" style="float: right;"></i></h6>
                             </div>
                             <div class="card-body p-0">
                                  <div class="table-responsive">{items}</div>
                                  <div style = "text-align:center;">{pager}</div>
                              </div>
                            </div>',
                        'columns' => [
                            [
                              'attribute' => 'type',
                              'format' => 'raw',
                              'filter' => false,
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-type', ['model' => $model]);},
                            ],[
                              'attribute' => 'sum',
                              'format' => 'raw',
                              'filter' => false,
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-sum', ['model' => $model]);},
                            ],[
                              'attribute' => 'date_add',
                              'format' => 'raw',
                              'filter' => false,
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-date-add', ['model' => $model]);},
                            ]
                        ],
                    ]); ?>
                  </div>
                  <div class="col-md-6">
                    <?= GridView::widget([
                        'dataProvider' => $paymentProvider,
                        'rowOptions'=>function($model){
                            return ['paymentHash' => $model->hash, 'class' => 'showPaymentInfo'];
                        },
                        'filterModel' => $payment,
                        'layout' => '
                          <div class="card">
                           <div class="card-header white">
                               <h6>' . Yii::t('app', 'Ваши ордера на пополнение/снятие') .'<i class="icon icon-th-list s-18" style="float: right;"></i></h6>
                           </div>
                           <div class="card-body p-0">
                                <div class="table-responsive">{items}</div>
                                <div style = "text-align:center;">{pager}</div>
                            </div>
                          </div>
                          ',
                        'columns' => [
                            [
                              'attribute' => 'type',
                              'format' => 'raw',
                              'filter' => false,
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-type', ['model' => $model]);},
                            ],[
                              'attribute' => 'realSum',
                              'format' => 'raw',
                              'filter' => false,
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payments-sum', ['model' => $model]);},
                            ],[
                              'attribute' => 'status',
                              'format' => 'raw',
                              'filter' => false,
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-status', ['model' => $model]);},
                            ],[
                              'attribute' => 'date_add',
                              'format' => 'raw',
                              'filter' => false,
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-date-add', ['model' => $model]);},
                            ]
                        ],
                    ]); ?>

                    <?php if (in_array($user->id, [23780])): ?>
                      <div class="card" style="margin-top:20px;">
                         <div class="card-header white">
                             <h6><?php echo Yii::t('app', 'Перевод участнику');?> <i class="icon icon-sync s-18" style="float: right;"></i></h6>
                         </div>
                         <div class="card-body">
                          <?php $form = ActiveForm::begin([
                              'enableClientScript' => false,
                              'options' => ['class' => 'form-material', 'enctype'=>'multipart/form-data',],
                              'fieldConfig' => [
                                  'options' => [
                                      'tag' => false,
                                  ],
                              ],
                          ]); ?>

                          <div class="form-group form-float">
                            <div class="form-line">
                              <?= $form->field($usertransactionForm, 'to_login')->textInput()->label($usertransactionForm->getAttributeLabel('to_login'),['class'=>'form-label']); ?>
                            </div>
                          </div>


                          <div class="form-group form-float">
                            <div class="form-line">
                              <?= $form->field($usertransactionForm, 'sum')->textInput()->label($usertransactionForm->getAttributeLabel('sum'),['class'=>'form-label']); ?>
                            </div>
                          </div>

                          <div class="form-group form-float">
                            <div class="form-line">
                              <?php echo $form->field($usertransactionForm, 'currency_id')->dropDownList(ArrayHelper::map($userCurrencyArray, 'id', 'title'), ['class' => 'select2']);?>
                            </div>
                          </div>

                          <div class="form-group">
                              <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-success']) ?>
                          </div>

                          <?php ActiveForm::end(); ?>
                         </div>
                      </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>