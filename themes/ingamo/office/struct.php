<?php
use app\modules\profile\widgets\InviteFriendsByEmailWidget;
use \app\modules\profile\models\UserInfo;
use yii\web\View;
$this->title = Yii::t('user', 'Структура');
$tableRowView = '@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_table-row';

$userInfo = UserInfo::find()->where(['user_id' => $user->id])->one();
?>
<style>
.show-hide-children{
    margin-right: 3px;
    cursor: pointer;
}
</style>

<div class="container-fluid relative animatedParent animateOnce">
    <div class="tab-content" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <div class="col-md-6">
                    <div class="counter-box white r-5 p-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="icon icon-dollar text-green s-48"></span>
                            </div>
                            <div class="counter-title"><?php echo Yii::t('app', 'Личные инвестиции');?></div>
                            <h2 class="sc-counter mt-3"><?php echo (!empty($userInfo) ? $userInfo->self_invest : 0); ?></h2>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="128"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="counter-box white r-5 p-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="icon icon-dollar text-green s-48"></span>
                            </div>
                            <div class="counter-title"><?php echo Yii::t('app', 'Оборот структуры');?></div>
                            <h2 class="sc-counter mt-3"><?php echo (!empty($userInfo) ? $userInfo->struct_invest : 0); ?></h2>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="128"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content pb-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <div class="col-12">
                <section class="paper-card">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th><?php echo Yii::t('app', 'E-mail');?></th>
                                <th><?php echo Yii::t('app', 'Уровень');?></th>
                                <th><?php echo Yii::t('app', 'Личные инвестиции');?></th>
                                <th><?php echo Yii::t('app', 'Структурные инвестиции');?></th>
                            </tr>
                            <?php $level1 = 1;?>
                            <?php foreach($user->children(1)->all() as $child1):?>
                                <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $child1, 'level' => $level1, 'parent' => $user]);?>
                                <?php $level2 = 2;?>
                                <?php foreach($child1->children(1)->all() as $child2):?>
                                    <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $child2, 'level' => $level2, 'parent' => $child1]);?>
                                    <?php $level3 = 3;?>
                                    <?php foreach($child2->children(1)->all() as $child3):?>
                                        <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $child3, 'level' => $level3, 'parent' => $child2]);?>
                                        <?php $level4 = 4;?>
                                        <?php foreach($child3->children(1)->all() as $child4):?>
                                            <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $child4, 'level' => $level4, 'parent' => $child3]);?>
                                            <?php $level5 = 5;?>
                                            <?php foreach($child4->children(1)->all() as $child5):?>
                                                <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $child5, 'level' => $level5, 'parent' => $child4]);?>
                                                <?php $level6 = 6;?>
                                                <?php foreach($child5->children(1)->all() as $child6):?>
                                                    <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $child6, 'level' => $level6, 'parent' => $child5]);?>
                                                    <?php $level7 = 7;?>
                                                    <?php foreach($child6->children(1)->all() as $child7):?>
                                                        <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $child7, 'level' => $level7, 'parent' => $child6]);?>
                                                        <?php $level8 = 8;?>
                                                        <?php foreach($child7->children(1)->all() as $child8):?>
                                                            <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $child8, 'level' => $level8, 'parent' => $child7]);?>
                                                        <?php endforeach;?>
                                                    <?php endforeach;?>
                                                <?php endforeach;?>
                                            <?php endforeach;?>
                                        <?php endforeach;?>
                                    <?php endforeach;?>
                                <?php endforeach;?>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </section>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs("
$('.show-hide-children').click(function(){

    console.log($(this).attr('class'));

    if ($(this).hasClass('icon-plus')) {
        $(this).removeClass('icon-plus');
        $(this).addClass('icon-minus');
        var currenctLogin = $(this).parent().parent().attr('currentLogin');
        $('.user-struct-info[parentLogin=\"' + currenctLogin + '\"]').show();
    }else{
        $(this).removeClass('icon-minus');
        $(this).addClass('icon-plus');
        var currenctLogin = $(this).parent().parent().attr('currentLogin');
        $('.user-struct-info[parentLogin=\"' + currenctLogin + '\"]').hide();
    }
})
", View::POS_END);?>