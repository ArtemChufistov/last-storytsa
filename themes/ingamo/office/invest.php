<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::t('user', 'Инвестиции');

$investId = 1;
$investSum1 = 1000;
$investPercent1 = 8;
$investCountMonth1 = 3;

$investId = 2;
$investSum2 = 1500;
$investPercent2 = 17;
$investCountMonth2 = 12;

$investId = 3;
$investSum3 = 2000;
$investPercent3 = 20;
$investCountMonth3 = 12;

?>

<style>
.invest-sum-title{
    text-align: right;
    padding-right: 7px;
    padding-top:5px;
}
.total-in-month{
    font-size: 20px;
}
.invest-sum-calcer{
    margin-top: 5px;
}
.total-invest-sum, .total-invest-percent, .total-return-sum, .total-month, .total-invest-sum-title{
    font-size: 20px;
}
.p-r-30{
    padding-right: 30px;
}
.invest-step-2{
    font-weight: bold;
}
</style>

<div class="container-fluid relative animatedParent animateOnce">
    <div class="tab-content pb-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <?php /*
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header white text-center">
                            <strong><?php echo Yii::t('app', 'Инвест план <strong>"Light"</strong>');?></strong>
                        </div>
                        <div class="card-body pt-0">
                            <div class="my-3">
                                <small><?php echo Yii::t('app', 'Доходность');?><br/>6-8% <?php echo Yii::t('app', 'в месяц');?></small>
                                <div class="progress mb-4">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%"></div>
                                </div>
                            </div>
                            <div class="my-3">
                                <small><?php echo Yii::t('app', 'Депозит');?><br/>90 <?php echo Yii::t('app', 'дней');?></small>
                                <div class="progress mb-4">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%"></div>
                                </div>
                            </div>
                            <div class="my-3">
                                <div class="card-title"><?php echo Yii::t('app', 'Сумма инвестиции');?></div>
                                <input type="text" class="range-slider1 sum-invest form-control" />
                            </div>
                            <div class="my-3 row">
                                <div class="col-lg-6 counter-title invest-sum-title"><?php echo Yii::t('app', 'Начисленные проценты');?>:</div>
                                <div class="col-lg-6 invest-sum-calcer">
                                    <strong class="sc-counter total-percent-1 total-percent">
                                        <?php $sumPerc1 = ($investSum1 / 100) * $investPercent1 * $investCountMonth1; echo $sumPerc1;?>
                                    </strong> <span><?php echo Yii::t('app', 'USD');?></span></div>
                            </div>
                            <div class="my-3 row">
                                <div class="col-lg-6 counter-title invest-sum-title"><?php echo Yii::t('app', 'Всего к получению');?>:</div>
                                <div class="col-lg-6 invest-sum-calcer"><strong class="sc-counter total-counter-1 total-counter"><?php echo $sumPerc1 + $investSum1; ?></strong> <span><?php echo Yii::t('app', 'USD');?></span></div>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-primary invest-step-2" invest-id="1" invest-month="<?php echo $investCountMonth1;?>"><?php echo Yii::t('app', 'Инвестировать');?></button>
                            </div>
                        </div>
                    </div>
                </div>
                */ ?>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header white text-center">
                            <strong><?php echo Yii::t('app', 'Инвест план <strong>"Normal"</strong>');?></strong>
                        </div>
                        <div class="card-body pt-0">
                            <div class="my-3">
                                <small><?php echo Yii::t('app', 'Доходность');?><br/>17% <?php echo Yii::t('app', 'в месяц');?></small>
                                <div class="progress mb-4">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="my-3">
                                <small><?php echo Yii::t('app', 'Депозит');?><br/>360 <?php echo Yii::t('app', 'дней');?></small>
                                <div class="progress mb-4">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="my-3">
                                <div class="card-title"><?php echo Yii::t('app', 'Сумма инвестиции');?></div>
                                <input type="text" class="range-slider2 sum-invest form-control" />
                            </div>
                            <div class="my-3 row">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="sumNormalPlan" style="text-align: center;" name="amount" value="1500" aria-required="true">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="my-3 row">
                                <div class="col-lg-6 counter-title invest-sum-title"><?php echo Yii::t('app', 'Начисленные проценты');?>:</div>
                                <div class="col-lg-6 invest-sum-calcer">
                                    <strong class="sc-counter total-percent-2 total-percent">
                                        <?php $sumPerc2 = ($investSum2 / 100) * $investPercent2 * $investCountMonth2; echo $sumPerc2; ?>
                                    </strong> <span><?php echo Yii::t('app', 'USD');?></span></div>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-primary invest-step-2" invest-id="2" invest-month="<?php echo $investCountMonth2;?>"><?php echo Yii::t('app', 'Инвестировать');?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php /*
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header white text-center">
                            <strong><?php echo Yii::t('app', 'Инвест план <strong>"Max"</strong>');?></strong>
                        </div>
                        <div class="card-body pt-0">
                            <div class="my-3">
                                <small><?php echo Yii::t('app', 'Доходность');?><br/>12-18% <?php echo Yii::t('app', 'в месяц');?></small>
                                <div class="progress mb-4">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="my-3">
                                <small><?php echo Yii::t('app', 'Депозит');?><br/>360 <?php echo Yii::t('app', 'дней');?></small>
                                <div class="progress mb-4">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="my-3">
                                <div class="card-title"><?php echo Yii::t('app', 'Сумма инвестиции');?></div>
                                <input type="text" class="range-slider3 sum-invest form-control" />
                            </div>
                            <div class="my-3 row">
                                <div class="col-lg-6 counter-title invest-sum-title"><?php echo Yii::t('app', 'Начисленные проценты');?>:</div>
                                <div class="col-lg-6 invest-sum-calcer">
                                    <strong class="sc-counter total-percent-3 total-percent">
                                        <?php $sumPerc3 = ($investSum3 / 100) * $investPercent3 * $investCountMonth3; echo $sumPerc3; ?>
                                    </strong> <span><?php echo Yii::t('app', 'USD');?></span></div>
                            </div>
                            <div class="my-3 row">
                                <div class="col-lg-6 counter-title invest-sum-title"><?php echo Yii::t('app', 'Всего к получению');?>:</div>
                                <div class="col-lg-6 invest-sum-calcer"><strong class="sc-counter total-counter-3 total-counter"><?php echo $sumPerc3 + $investSum3; ?></strong> <span><?php echo Yii::t('app', 'USD');?></span></div>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-primary invest-step-2" invest-id="3" invest-month="<?php echo $investCountMonth3;?>"><?php echo Yii::t('app', 'Инвестировать');?></button>
                            </div>
                        </div>
                    </div>
                </div>
 */?>
            </div>
        </div>
    </div>
</div>

<div class="modal modalCheckInvest fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <?php $form = ActiveForm::begin(['enableClientScript' => false]); ?>
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo Yii::t('app', 'Оформление инвест пакета');?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="d-flex justify-content-between">
            <table>
                <tr>
                    <td class="p-r-30">
                        <i class="icon-circle-o text-green mr-2"></i><?php echo Yii::t('app', 'Выплата в течении');?>:
                    </td>
                    <td>
                        <i class="icon-angle-double-up text-green mr-2"></i><strong class="total-month">0</strong> <?php echo Yii::t('app', 'месяцев');?>
                    </td>
                </tr>
                <tr>
                    <td class="p-r-30">
                        <i class="icon-circle-o text-green mr-2"></i><?php echo Yii::t('app', 'Начисления в месяц');?>:
                    </td>
                    <td>
                        <i class="icon-angle-double-up text-green mr-2"></i><strong class="total-in-month">0</strong> <?php echo Yii::t('app', 'USD');?>
                    </td>
                </tr>
                <tr>
                    <td class="p-r-30">
                        <i class="icon-circle-o text-green mr-2"></i><?php echo Yii::t('app', 'Инвестируемая сумма');?>:
                    </td>
                    <td>
                        <i class="icon-angle-double-up text-green mr-2"></i><strong class="total-invest-sum-title">0</strong> USD
                    </td>
                </tr>
                <tr>
                    <td class="p-r-30">
                        <i class="icon-circle-o text-green mr-2"></i><?php echo Yii::t('app', 'Начисленный процент');?>:
                    </td>
                    <td>
                        <i class="icon-angle-double-up text-green mr-2"></i><strong class="total-invest-percent">0</strong> USD
                    </td>
                </tr>
                <?php /*
                <tr>
                    <td class="p-r-30">
                        <i class="icon-circle-o text-green mr-2"></i><?php echo Yii::t('app', 'Итого к получению');?>:
                    </td>
                    <td>
                        <i class="icon-angle-double-up text-green mr-2"></i><strong class="total-return-sum">0</strong> USD
                    </td>
                </tr>
 */?>
            </table>
        </div>
        <input type="hidden" class="form-control total-invest-sum" name = "marketingData[amount]" value="<?php echo $investSum2;?>">
      </div>
      <div class="modal-footer">
        <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Подтвердить'), [
          'class' => 'btn btn-block btn-success pull-left invest-id',
          'value' => 0,
          'name' => 'marketingData[type]']) ?>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть'); ?></button>
      </div>
        <?php ActiveForm::end();?>
    </div>
  </div>
</div>

<?php if(Yii::$app->session->getFlash('message')): ?>

<div class="modal modalMessageInvest fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="d-flex justify-content-between">
            <?php echo Yii::$app->session->getFlash('message');?>
        </div>
      </div>
      <div class="modal-footer">
        <a href="/office/in<?php if (!empty($_POST['marketingData']) && !empty($_POST['marketingData']['amount'])):?>?sum=<?php echo $_POST['marketingData']['amount'];?><?php endif;?>" class="btn btn-primary"><?php echo Yii::t('app', 'Пополнить личный кабинет'); ?></a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть'); ?></button>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs("
  $('.modalMessageInvest').modal('show');
", View::POS_END);?>

<?php endif;?>

<?php if(Yii::$app->session->getFlash('invest-success')): ?>

<div class="modal modalSuccessInvest fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo Yii::t('app', 'Инвестиция оформллена');?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="d-flex justify-content-between">
            <?php echo Yii::$app->session->getFlash('invest-success');?>
        </div>
      </div>
      <div class="modal-footer">
        <a href="/office/viewinvest" class="btn btn-primary"><?php echo Yii::t('app', 'Посмотреть мои инвестиции'); ?></a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть'); ?></button>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs("
  $('.modalSuccessInvest').modal('show');
", View::POS_END);?>

<?php endif;?>

<?php $this->registerJs("
$('.range-slider3').ionRangeSlider({
    skin: 'big',
    min: 100,
    max: 5000,
    from: " . $investSum3 . ",
    onChange: function (data) {
        var investSum = $('.range-slider3').val();
        var sumPerc = Math.round((investSum / 100) * " . $investPercent3 . "*" . $investCountMonth3 . ");
        var totalSum = Number(sumPerc) + Number(investSum);

        $('.total-percent-3').html(sumPerc);
        $('.total-counter-3').html(totalSum);
    }
});

var rangeSlider = $('.range-slider2');

rangeSlider.ionRangeSlider({
    skin: 'big',
    min: 100,
    max: 5000,
    from: " . $investSum2 . ",
    onChange: function (data) {
        var investSum = $('.range-slider2').val();
        var sumPerc = Math.round((investSum / 100) * " . $investPercent2 . "*" . $investCountMonth2 . ");
        var totalSum = Number(sumPerc);

        $('#sumNormalPlan').val(investSum);
        $('.total-invest-sum').val(investSum);
        $('.total-percent-2').html(sumPerc);
        $('.total-counter-2').html(totalSum);
    }
});

$('#sumNormalPlan').keyup(function() {
    var currentVal = $(this).val();
    
    if (currentVal < 100) {
        currentVal = 100;
    }
    
    if (currentVal > 5000) {
        currentVal = 5000;
    }
    
    var instance = rangeSlider.data('ionRangeSlider');
    
    instance.update({from: currentVal});
    $(this).val(currentVal);
    
    var investSum = currentVal;
    var sumPerc = Math.round((investSum / 100) * " . $investPercent2 . "*" . $investCountMonth2 . ");
    var totalSum = Number(sumPerc) + Number(investSum);

    $('#sumNormalPlan').val(investSum);
    $('.total-percent-2').html(sumPerc);
    $('.total-counter-2').html(totalSum);
    $('.total-invest-sum').val(investSum);
})

$('.range-slider1').ionRangeSlider({
    skin: 'big',
    min: 100,
    max: 5000,
    from: " . $investSum1 . ",
    onChange: function (data) {
        var investSum = $('.range-slider1').val();
        var sumPerc = Math.round((investSum / 100) * " . $investPercent1 . "*" . $investCountMonth1 . ");
        var totalSum = Number(sumPerc) + Number(investSum);

        $('.total-percent-1').html(sumPerc);
        $('.total-counter-1').html(totalSum);
    }
});

$('.invest-step-2').click(function(){

    var wrap = $(this).parent().parent();
    var totalCounter = Number(wrap.find('.sum-invest').val()) + Number(wrap.find('.total-percent').html());

    $('.total-month').html($(this).attr('invest-month'));
    $('.total-in-month').html(((wrap.find('.sum-invest').val() / 100 ) * 17).toFixed(2));
    $('.total-invest-sum-title').html(wrap.find('.sum-invest').val());
    $('.total-invest-percent').html(wrap.find('.total-percent').html());
    $('.total-return-sum').html(totalCounter);

    $('.total-invest-id').val(3);
    $('.invest-id').val($(this).attr('invest-id'));

    jQuery('.modalCheckInvest').modal('show');
})
", View::POS_END);?>