<?php
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Currency;
use app\modules\invest\models\UserInvest;
use app\modules\event\models\Event;

$this->title = Yii::t('user', 'Панель управления');

$userTransactions = Transaction::find()->where(['from_user_id' => $user->id])->orWHere(['to_user_id' => $user->id])->limit(10)->orderBy(['id' => SORT_DESC])->all();
$userInvestArray = UserInvest::find()->where(['user_id' => $user->id])->andWhere(['status' => UserInvest::STATUS_ACTIVE])->all();
$userEvents = Event::find()->where(['user_id' => $user->id])->limit(10)->orderBy(['date_add' => SORT_DESC])->all();

$children = $user->children()->all();

$childrenWithStruct = [];
foreach ($children as $child) {
    $countChildren = count($child->children()->all());
    if ($countChildren > 0){
        $childrenWithStruct[] = [
            'user' => $child,
            'countChildren' => $countChildren
        ];
    }
}

for ($j = 0; $j < count($childrenWithStruct) - 1; $j++){
    for ($i = 0; $i < count($childrenWithStruct) - $j - 1; $i++){
        // если текущий элемент больше следующего
        if ($childrenWithStruct[$i]['countChildren'] > $childrenWithStruct[$i + 1]['countChildren']){
            // меняем местами элементы
            $tmp_var = $childrenWithStruct[$i + 1];
            $childrenWithStruct[$i + 1] = $childrenWithStruct[$i];
            $childrenWithStruct[$i] = $tmp_var;
        }
    }
}

$adminUserId = 1;
$parent = $user->parent()->one();

$currencyFreez = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();

$userBalanceFreez = $user->getBalances([$currencyFreez->id]);
?>

<style type="text/css">
.counter-title{
    font-weight: bold;
}
</style>

<div class="container-fluid relative animatedParent animateOnce">
    <div class="tab-content pb-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <div class="col-md-3">
                    <div class="counter-box white r-5 p-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="icon icon-dollar text-green s-48"></span>
                            </div>
                            <div class="counter-title"><?php echo Yii::t('app', 'Ваш основной баланс');?></div>
                            <?php foreach ($user->getBalances() as $balance): ?>
                                <h2 class="sc-counter mt-3"><?php echo $balance->value; ?></h2>
                            <?php endforeach;?>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="128"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="counter-box white r-5 p-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="icon icon-storage text-purple s-48"></span>
                            </div>
                            <div class="counter-title "><?php echo Yii::t('app', 'С первой линии');?></div>
                            <h2 class="sc-counter mt-3">
                                <?php $sum = Transaction::find()
                                    ->where(['type' => Transaction::TYPE_PARTNER_INVEST_DIV_1])
                                    ->orWhere(['type' => Transaction::TYPE_REF_BONUS])
                                    ->andWhere(['to_user_id' => $user->id])
                                    ->sum('sum'); echo $sum;?>
                            </h2>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="128"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="counter-box white r-5 p-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="icon icon-bank text-blue s-48"></span>
                            </div>
                            <div class="counter-title"><?php echo Yii::t('app', 'Прибыль с инвестиций');?></div>
                            <h2 class="sc-counter mt-3">
                                <?php $sum = Transaction::find()
                                    ->where(['type' => Transaction::TYPE_CHARGE_TO_DEPOSIT])
                                    ->andWhere(['to_user_id' => $user->id])
                                    ->sum('sum'); echo $sum;?>
                            </h2>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="128"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="counter-box white r-5 p-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="icon icon-public text-red s-48"></span>
                            </div>
                            <div class="counter-title"><?php echo Yii::t('app', 'Оборот со структуры');?></div>
                            <h2 class="sc-counter mt-3">
                                <?php $sum = Transaction::find()
                                    ->orWhere(['type' => Transaction::TYPE_PARTNER_INVEST_DIV_1])
                                    ->orWhere(['type' => Transaction::TYPE_PARTNER_INVEST_DIV_2])
                                    ->orWhere(['type' => Transaction::TYPE_PARTNER_INVEST_DIV_3])
                                    ->orWhere(['type' => Transaction::TYPE_PARTNER_INVEST_DIV_4])
                                    ->orWhere(['type' => Transaction::TYPE_PARTNER_INVEST_DIV_5])
                                    ->orWhere(['type' => Transaction::TYPE_PARTNER_INVEST_DIV_6])
                                    ->orWhere(['type' => Transaction::TYPE_PARTNER_INVEST_DIV_7])
                                    ->orWhere(['type' => Transaction::TYPE_REF_BONUS])
                                    ->andWhere(['to_user_id' => $user->id])
                                    ->sum('sum'); echo $sum;?>
                            </h2>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="128"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-3">
                <div class="col-md-5">
                    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/_ref', ['user' => $user]);?>
                    <div class="card" style="margin-bottom: 20px;">
                        <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Последние <strong>10</strong> операций');?> <i class="icon icon-menu s-18" style="float: right;"></i></h6>
                        </div>
                        <div class="collapse show" id="salesCard">
                            <?php if (!empty($userTransactions)):?>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover earning-box">
                                            <thead class="bg-light">
                                                <tr>
                                                    <th><?php echo Yii::t('app', 'Тип');?></th>
                                                    <th><?php echo Yii::t('app', 'Сумма');?></th>
                                                    <th><?php echo Yii::t('app', 'Дата');?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($userTransactions as $transaction):?>
                                                <?php $toUser = $transaction->getToUser()->one();?>
                                                <?php $fromUser = $transaction->getFromUser()->one();?>
                                                <tr>
                                                    <td>
                                                        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/grid/_transaction-type', ['model' => $transaction]);?>
                                                    </td>
                                                    <td>
                                                        <?php if (!empty($toUser) && $toUser->id == $user->id):?>
                                                            <span class="label label-success label-rouded">+<?php echo number_format($transaction->sum, 2);?> <?php echo $transaction->getCurrency()->one()->title;?></span>
                                                        <?php else:?>
                                                            <span class="label label-danger label-rouded">-<?php echo number_format($transaction->sum, 2);?> <?php echo $transaction->getCurrency()->one()->title;?></span>
                                                        <?php endif;?>
                                                    </td>
                                                    <td><?php echo date("H:i:s d-m-y", strtotime($transaction->date_add));?></td>
                                                </tr>
                                            <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php else:?>
                                <div class="card-body text-center">
                                    <p><?php echo Yii::t('app', 'Операций по счёту не обнаружено, пополните личный кабинет');?></p>
                                    <a href="/office/in" class="btn btn-success" style="font-weight: bold; font-size: 14px;"><?php echo Yii::t('app', 'Пополнить личный кабинет');?></a>
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Последние <strong>10</strong> событий');?> <i class="icon icon-menu s-18" style="float: right;"></i></h6>
                        </div>
                        <div class="collapse show" id="salesCard2">
                            <?php if (!empty($userEvents)):?>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover earning-box">
                                            <thead class="bg-light">
                                                <tr>
                                                    <th><?php echo Yii::t('app', 'Событие');?></th>
                                                    <th><?php echo Yii::t('app', 'Логин');?></th>
                                                    <th><?php echo Yii::t('app', 'Дата');?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($userEvents as $event):?>
                                                    <?php $toUser = $event->getToUser()->one();?>
                                                    <?php if (!empty($toUser)):?>
                                                        <tr>
                                                            <td><?php echo $event->getTypeTitle();?></td>
                                                            <td><?php echo $toUser->login;?></td>
                                                            <td><?php echo date("H-i-s d-m-y", strtotime($event->date_add));?></td>
                                                        </tr>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php else:?>
                                <div class="card-body text-center">
                                    <p><?php echo Yii::t('app', 'Событий не обнаружено');?></p>
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="margin-bottom: 20px;">
                       <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Топ активных лидеров');?> <i class="icon icon-menu s-18" style="float: right;"></i></h6>
                       </div>
                       <div class="card-body text-center">
                        <p style="text-align: center;"><?php echo Yii::t('app', 'Приглашайте ваших знакомых для увеличения своей прибыли');?></p>
                        <a href="/office/struct" class="btn btn-primary" style="font-weight: bold; font-size: 14px;"><?php echo Yii::t('app', 'Посмотреть структуру');?></a>
                       </div>
                    </div>
                    <div class="card">
                       <div class="card-header white">
                           <h6><?php echo Yii::t('app', 'Ваши инвестиции');?> <i class="icon icon-menu s-18" style="float: right;"></i></h6>
                       </div>
                       <div class="card-body ">
                        <?php if (!empty($userInvestArray)):?>
                            <?php $investSum = 0; foreach($userInvestArray as $userInvest){$investSum += $userInvest->sum;};?>
                            <div class="circular-progressbar ">
                                <input type='text' value="0" data-thickness="0.2" data-width="100" data-bgColor="#eee" data-fgColor="#63b139">
                                <div class="mt-3 text-center">
                                    <h5 class="heading"><?php echo Yii::t('app', 'Выплачено');?></h5>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <p>
                                        <i class="icon-circle-o text-red mr-2"></i><?php echo Yii::t('app', 'Инвестировано');?></p>
                                </div>
                                <div>
                                    <p>
                                        <i class="icon-angle-double-up text-green mr-2"></i><?php echo $investSum;?> USD</p>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="/office/viewinvest" class="btn btn-success" style="font-weight: bold; font-size: 14px;"><?php echo Yii::t('app', 'Мои инвестиции');?></a>
                            </div>
                        <?php else:?>
                            <div class="mt-3 text-center">
                                <p style="text-align: center;"><?php echo Yii::t('app', 'Для получения пассивного дохода оформите инвестицию');?></p>
                                <a href="/office/invest" class="btn btn-success" style="font-weight: bold; font-size: 14px;"><?php echo Yii::t('app', 'Оформить инвестицию');?></a>
                            </div>
                        <?php endif;?>
                       </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <?php if (!empty($userBalanceFreez[$currencyFreez->id]) && $userBalanceFreez[$currencyFreez->id]->value > 0):?>
                        <div class="card" style="margin-bottom: 20px;">
                            <div class="card-header white">
                               <h6><?php echo Yii::t('app', 'Замороженные средства');?> <i class="icon icon-menu s-18" style="float: right;"></i></h6>
                            </div>
                            <div class="bg-primary text-white lighten-2">
                                <div class="pt-5 pb-2 pl-5 pr-5">
                                    <h5 class="font-weight-normal s-14"><?php echo Yii::t('app', 'Terofund USD');?></h5>
                                    <span class="s-48 font-weight-lighter text-primary">
                                        <small>$</small><?php echo $userBalanceFreez[$currencyFreez->id]->value;?></span>
                                    <div class="float-right">
                                        <span class="icon icon-chart s-48"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
                    <div class="paper-block counter-box text-center" style="background-color: white; ">
                        <div class="p-t-b-10">
                            <span class="icon icon-trophy7 s-48"></span>
                        </div>
                        <div class="sc-counter s-24">0</div>
                        <h6 class="mt-2"><?php echo Yii::t('app', 'Leader bonus');?></h6>
                    </div>
                    <div class="paper-block counter-box text-center" style="background-color: white; ">
                        <div class="p-t-b-10">
                            <span class="icon icon-wallet s-48"></span>
                        </div>
                        <div class="sc-counter s-24">0</div>
                        <h6 class="mt-2"><?php echo Yii::t('app', 'Matching bonus');?></h6>
                    </div>
                    <?php if (!empty($parent) && $parent->id != $adminUserId):?>
                        <div class="card" style="padding-bottom: 10px; margin-bottom: 20px;">
                           <div class="card-header white">
                               <h6><?php echo Yii::t('app', 'Ваш пригласитель');?></h6>
                           </div>
                            <div class="image mr-3 text-center">
                                <img class="user_avatar no-b no-p r-5" src="<?php echo $parent->getImage();?>" alt="User Image">
                            </div>
                            <div class="image mr-3 text-center">
                                <h6 class="p-t-10"><?php echo $parent->login;?></h6>
                                <?php echo $parent->email;?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>