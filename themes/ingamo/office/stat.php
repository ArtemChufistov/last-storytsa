<?php
use app\modules\event\models\Event;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Статистика');
?>

<div class="container-fluid relative animatedParent animateOnce">
    <div class="tab-content pb-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header white"><h6><?php echo Yii::t('app', 'Итоговая информация');?></h6></div>
                        <div class="card-body p-0">
                            <div class="lighten-2">
                                <div class="pt-5 pb-2 pl-5 pr-5">
                                    <h5 class="font-weight-normal s-14"><?php echo Yii::t('app', 'Участников в структуре');?></h5>
                                    <span class="s-48 font-weight-lighter text-primary">
                                        <small class="icon icon-users" style="margin-right: 20px;"></small>
                                        <span class="sc-counter"><?php echo $user->descendants()->count();?></span>
                                    </span>
                                    <div class="float-right">
                                        <span class="icon icon-arrow_upward s-48"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="margin-top: 20px;">
                        <div class="card-header white"><h6><?php echo Yii::t('app', 'Итоговая информация');?></h6></div>
                        <div class="card-body p-0">
                            <div class="lighten-2">
                                <div class="pt-5 pb-2 pl-5 pr-5">
                                    <h5 class="font-weight-normal s-14"><?php echo Yii::t('app', 'Личных партнёров');?></h5>
                                    <span class="s-48 font-weight-lighter text-primary">
                                        <small class="icon icon-users" style="margin-right: 20px;"></small>
                                        <span class="sc-counter"><?php echo $user->children()->count();?></span>
                                    </span>
                                    <div class="float-right">
                                        <span class="icon icon-arrow_upward s-48"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="margin-top: 20px;">
                        <div class="card-header white"><h6><?php echo Yii::t('app', 'Итоговая информация');?></h6></div>
                        <div class="card-body p-0">
                            <div class="lighten-2">
                                <div class="pt-5 pb-2 pl-5 pr-5">
                                    <h5 class="font-weight-normal s-14"><?php echo Yii::t('app', 'Переходов по REF ссылке');?></h5>
                                    <span class="s-48 font-weight-lighter text-primary">
                                        <small class="icon icon-users" style="margin-right: 20px;"></small>
                                        <span class="sc-counter"><?php echo $user->getEvents()->where(['type' => Event::TYPE_REF_LINK])->count();?></span>
                                    </span>
                                    <div class="float-right">
                                        <span class="icon icon-arrow_upward s-48"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header white">
                           <h6><i class="fa fa-bars" aria-hidden="true"></i> <?php echo Yii::t('app', 'События');?></h6>
                        </div>
                        <div class="collapse show">
                            <?= GridView::widget([
                                'dataProvider' => $eventProvider,
                                'filterModel' => $eventModel,
                                'layout' => '
                                    <div class="box-body no-padding eventTable">{items}</div><div style = "text-align: center;">{pager}</div>',
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        'attribute' => 'type',
                                        'format' => 'html',
                                        'filter' => false,
                                        'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-type', ['model' => $model]);},
                                    ],
                                    [
                                        'attribute' => 'date_add',
                                        'format' => 'html',
                                        'filter' => false,
                                        'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-date', ['model' => $model]);},
                                    ],
                                ],
                              ]); ?>
                        </div>
                   </div>
                </div>
                <div class="col-md-4">
                    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/_ref', ['user' => $user]);?>
                </div>
            </div>
        </div>
    </div>
</div>