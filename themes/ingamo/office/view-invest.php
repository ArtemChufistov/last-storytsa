<?php
$this->title = Yii::t('user', 'Мои инвестиции');

?>
<div class="container-fluid relative animatedParent animateOnce">
  <div class="tab-content pb-3" id="v-pills-tabContent">
    <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
      <div class="row my-3">
        <?php foreach($userInvestArray as $userInvest):?>
          <div class="col-md-3">
            <div class="card">
               <div class="card-header white">
                   <h6><?php echo Yii::t('app', 'Инвест план от {date}', ['date' => date('H:i d-m-Y', strtotime($userInvest->date_add))]);?></h6>
               </div>
               <div class="card-body ">
                    <div class="circular-progressbar ">
                        <input type='text' value="0" data-thickness="0.2" data-width="100" data-bgColor="#eee" data-fgColor="#63b139">
                        <div class="mt-3 text-center">
                            <h5 class="heading"><?php echo Yii::t('app', 'Выплачено');?></h5>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between">
                        <div>
                            <p>
                                <i class="icon-circle-o text-red mr-2"></i><?php echo Yii::t('app', 'Инвестировано');?></p>
                        </div>
                        <div>
                            <p>
                                <i class="icon-angle-double-up text-green mr-2"></i><?php echo $userInvest->sum;?> USD</p>
                        </div>
                    </div>
               </div>
            </div>
          </div>
        <?php endforeach;?>
      </div>
    </div>
  </div>
</div>