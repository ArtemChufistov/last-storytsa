<?php
use app\modules\support\models\TicketDepartment;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::t('user', 'Оставить отзыв');

$departments = TicketDepartment::find()->all();

$departmentArray = [];
foreach($departments as $department){
  $departmentArray[$department->id] = Yii::t('app', $department->name);
}
?>

<div class="container-fluid relative animatedParent animateOnce">
  <div class="tab-content pb-3" id="v-pills-tabContent">
    <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
      <div class="row my-3">
        <div class="col-md-6">
          <div class="card" style="margin-bottom: 20px;">
            <div class="card-header white">
              <?php $form = ActiveForm::begin([
                  'enableClientScript' => false,
                  'options' => ['class' => 'form-material', 'enctype'=>'multipart/form-data',],
                  'fieldConfig' => [
                      'options' => [
                          'tag' => false,
                      ],
                  ],
              ]); ?>

              <?= $form->field($response, 'name')->textInput([
                  'maxlength' => true,
                  'placeholder' => $response->getAttributeLabel('name')
              ]) ?>

              <?= $form->field($response, 'text')->textArea(['rows' => '9', 'class' => 'textarea form-control']); ?>

                <div class="col-sm-12" style = "margin-top: 20px;">
                  <?php echo  $form->field($response, 'photo1', ['options' => ['class' => 'custom-file-input']])
                    ->fileInput(['class' => 'custom-file-input'])
                    ->label(Yii::t('app', 'Choose file...'), ['class' => 'custom-file-label']); ?>
                  <p class="help-block"><?php echo Yii::t('app', 'Максимум');?> 2MB</p>
                </div>

                  <?= $form->field($response, 'video1')->textInput([
                      'maxlength' => true,
                      'placeholder' => $response->getAttributeLabel('video1')
                  ]) ?>

              <div class="form-group" style = "margin-top:20px;">
                  <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                      'class' => 'btn btn-success pull-right',
                      'name' => 'signup-button']) ?>
              </div>

              <?php ActiveForm::end();?>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card" style="margin-bottom: 20px;">
            <div class="card-header white">
              <h6><?php echo Yii::t('app', 'Обратите внимание');?> <i class="icon icon-check s-18" style="float: right;"></i></h6>
            </div>
            <div class="card-body">
              <?php echo Yii::t('app', '<p>Расскажите о Ваших впечатлениях, о проекте, о полученных выплатах, поделитесь своей историей успеха ! Постарайтесь написать более развернутый текст Вашего отзыва - новым участникам интересно знать Ваше мнение. Отзывы в одно предложение в стиле \'Все хорошо, спасибо.\' \'Отличный проект спасибо всем.\' - публиковаться не будут.</p></br><strong>Важно</strong><p>Отзывы размещаются на главной странице проекта. В них запрещена какая либо реклама своих ресурсов, личных контактов или реф ссылок. Главную страницу посещают участники разных команд и нужно уважать партнёров, которые с ними провели работу и пригласили в проект.</p>');?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php if(Yii::$app->session->getFlash('success')): ?>
  <div class="modalError modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('success');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalError').modal('show');
", View::POS_END);?>

<?php endif;?>