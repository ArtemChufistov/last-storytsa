<div class="container-fluid relative animatedParent animateOnce">
    <div class="tab-content pb-3" id="v-pills-tabContent">
        <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
            <div class="row my-3">
                <?php echo Yii::$app->controller->renderPartial(
                  '@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_' . strtolower(\yii\helpers\StringHelper::basename(get_class($paymentForm))),
                          ['paymentForm' => $paymentForm, 'user' => $user, 'currencyArray' => $currencyArray]); ?>
            </div>
        </div>
    </div>
</div>