<?php
$this->title = Yii::t('app', 'Главная');
?>
<section class="section swiper-container swiper-slider swiper-slider-2 swiper-bg" data-loop="true" data-autoplay="5000" data-simulate-touch="false" data-slide-effect="fade">
  <div class="swiper-img-block">
    <div class="swiper-img-block-inner">
      <div class="img-block-inner-item"><img src="/ingamo/images/phone-1.png" alt=""></div>
      <div class="img-block-inner-item"><img src="/ingamo/images/phone-2.png" alt=""></div>
    </div>
  </div>
  <div class="swiper-wrapper text-center text-md-left">
    <div class="swiper-slide context-dark" data-slide-bg="/ingamo/images/swiper-bg.jpg">
      <div class="swiper-slide-caption">
        <div class="container">
          <div class="row row-fix">
            <div class="col-md-9 col-lg-7">
              <h1 data-caption-animate="fadeInUp" data-caption-delay="100">Ingamo<br><?php echo Yii::t('app','Инвестиционные решения');?></h1>
              <div class="swiper-button-wrap" data-caption-animate="fadeInUp" data-caption-delay="150"><a class="button button-lg button-secondary" href="#whyChoseUs"><?php echo Yii::t('app','Узнать больше');?></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-slide context-dark" data-slide-bg="/ingamo/images/swiper-bg.jpg">
      <div class="swiper-slide-caption">
        <div class="container">
          <div class="row row-fix">
            <div class="col-md-9 col-lg-7">
              <h1 data-caption-animate="fadeInUp" data-caption-delay="100"><?php echo Yii::t('app','Диверсификация в отношении <br>инвестиционных и бизнес-рисков');?></h1>
              <div class="swiper-button-wrap" data-caption-animate="fadeInUp" data-caption-delay="150"><a class="button button-lg button-secondary" href="#whyChoseUs"><?php echo Yii::t('app','Узнать больше');?></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-slide context-dark" data-slide-bg="/ingamo/images/swiper-bg.jpg">
      <div class="swiper-slide-caption">
        <div class="container">
          <div class="row row-fix">
            <div class="col-md-9 col-lg-7">
              <h1 data-caption-animate="fadeInUp" data-caption-delay="100"><?php echo Yii::t('app','Доступ к стратегиям с высоким уровнем доходности');?></h1>
              <div class="swiper-button-wrap" data-caption-animate="fadeInUp" data-caption-delay="150"><a class="button button-lg button-secondary" href="#whyChoseUs"><?php echo Yii::t('app','Узнать больше');?></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php /*
  <div class="swiper-meta">
    <ul class="links">
      <li><a class="icon icon-meta mdi novi-icon mdi-facebook" href="#"></a></li>
      <li><a class="icon icon-meta mdi novi-icon mdi-twitter" href="#"></a></li>
      <li><a class="icon icon-meta mdi novi-icon mdi-instagram" href="#"></a></li>
      <li><a class="icon icon-meta mdi novi-icon mdi-facebook-messenger" href="#"></a></li>
      <li><a class="icon icon-meta mdi novi-icon mdi-linkedin" href="#"></a></li>
      <li><a class="icon icon-meta mdi novi-icon mdi-snapchat" href="#"></a></li>
    </ul>
    <div class="contacts">
      <div class="icon mdi mdi-cellphone-iphone novi-icon"></div>
      <div class="tel"><a href="tel:#">1-800-1234-567</a></div>
      <div class="request"><a href="#">Request a Call Back</a></div>
    </div>
    <!-- Swiper Pagination-->
    <div class="swiper-pagination"></div>
  </div>
  */?>
</section>

<!-- Advantages-->
<section class="section context-dark bounding-fix">
  <div class="row row-flex no-gutters">
    <div class="col-md-6 col-lg-3">
      <div class="blurb-boxed-2 novi-background">
        <div class="icon mdi mdi-responsive novi-icon"></div>
        <h6 class="title"><?php echo Yii::t('app','Миссия проекта:');?></h6>
        <p class="exeption"><?php echo Yii::t('app','Приумножить капитал инвесторов по отношению к биткоину на крипто рынке, посредством трейдинга, с помощью написанного алгоритма и команды профессиональных трейдеров.');?></p>
      </div>
    </div>
    <div class="col-md-6 col-lg-3">
      <div class="blurb-boxed-2 blurb-boxed-dark novi-background">
        <div class="icon mdi mdi-star-outline novi-icon"></div>
        <h6 class="title"><?php echo Yii::t('app','Видение:');?></h6>
        <p class="exeption"><?php echo Yii::t('app','Стать самой результативной командой трейдеров на мировой арене в сфере криптовалют.');?></p>
      </div>
    </div>
    <div class="col-md-6 col-lg-3">
      <div class="blurb-boxed-2 blurb-boxed-darker novi-background">
        <div class="icon mdi mdi-headset novi-icon"></div>
        <h6 class="title"><?php echo Yii::t('app','Ценности:');?></h6>
        <p class="exeption"><?php echo Yii::t('app','* Честность <br/> * Ответственность <br/> * Профессионализм');?>
</p>
      </div>
    </div>
    <div class="col-md-6 col-lg-3">
      <div class="blurb-boxed-2 blurb-boxed-darkest novi-background">
        <p class="exeption"></p>
        <h5 class="title"><?php echo Yii::t('app','Для того чтобы начать зарабатывать нажмите кнопку');?></h5><a class="button button-lg button-secondary" href="/signup"><?php echo Yii::t('app','Регистрация');?></a>
      </div>
    </div>
  </div>
</section>

<!-- The Best Banking Choise-->
<section class="section section-lg section-decorate novi-background bg-cover" id="whyChoseUs">
  <div class="container">
    <div class="block-lg text-center">
      <h2><?php echo Yii::t('app','Почему выбирают нас?');?></h2>
      <p><?php echo Yii::t('app','Наши клиенты выбирают Ingamo по ряду причин, включая надежность, стабильность, новейшие технологии и хорошие показатели ROI . Узнайте больше о других преимуществах ниже.');?></p>
    </div>
    <div class="row row-30 row-xxl-60">
      <div class="col-sm-6 col-md-4 wow fadeInLeft">
        <div class="blurb-image">
          <div class="icon mercury-icon-time"></div>
          <h6 class="title"><?php echo Yii::t('app','Быстрые результаты');?></h6>
          <p class="exeption"><?php echo Yii::t('app','Мы работаем быстро и эффективно, чтобы обеспечить наилучшие результаты.');?></p>
        </div>
      </div>
      <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="0.1s">
        <div class="blurb-image">
          <div class="icon mercury-icon-touch"></div>
          <h6 class="title"><?php echo Yii::t('app','Эффективные стратегии');?></h6>
          <p class="exeption"><?php echo Yii::t('app','Наша команда обладает эффективными стратегиями, которые подтверждаются результатами.');?></p>
        </div>
      </div>
      <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="0.2s">
        <div class="blurb-image">
          <div class="icon mercury-icon-money-3"></div>
          <h6 class="title"><?php echo Yii::t('app','Управление рисками');?></h6>
          <p class="exeption"><?php echo Yii::t('app','Наши аналитики умеют работать на снижение вероятности возникновения неблагоприятного результата и минимизацию возможных потерь.');?></p>
        </div>
      </div>
      <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="0.1s">
        <div class="blurb-image">
          <div class="icon mercury-icon-phone-24"></div>
          <h6 class="title"><?php echo Yii::t('app','Эффективная поддержка');?></h6>
          <p class="exeption"><?php echo Yii::t('app','Ingamo предлагает обширную поддержку своим клиентам по всему миру.');?></p>
        </div>
      </div>
      <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="0.2s">
        <div class="blurb-image">
          <div class="icon mercury-icon-lightbulb-gears"></div>
          <h6 class="title"><?php echo Yii::t('app','Инновационные Технологии');?></h6>
          <p class="exeption"><?php echo Yii::t('app','Наши разработчики используют новейшие технологии для предоставления лучших приложений.');?></p>
        </div>
      </div>
      <div class="col-sm-6 col-md-4 wow fadeInLeft" data-wow-delay="0.3s">
        <div class="blurb-image">
          <div class="icon mercury-icon-pointer"></div>
          <h6 class="title"><?php echo Yii::t('app','Фундаментальный подход');?></h6>
          <p class="exeption"><?php echo Yii::t('app','Фирма использует дисциплинированные, систематические инвестиционные процессы.');?></p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- A Few Words About Our Bank-->
<section class="section section-lg bg-gray-100 novi-background bg-cover">
  <div class="container">
    <div class="block-lg text-center">
      <h2><?php echo Yii::t('app','Несколько слов о нас');?></h2>
      <p><?php echo Yii::t('app','Мы постоянно инвестируем в талант, технологии и исследования, стремясь обеспечить наилучшие результаты для наших клиентов.');?></p>
    </div>
    <div class="row row-20 justify-content-center justify-content-lg-between">
      <div class="col-md-10 col-lg-6 wow fadeIn"><img src="/ingamo/images/index-1-657x400.png" alt="" width="657" height="400"/>
      </div>
      <div class="col-md-10 col-lg-6 col-xl-5">
        <div class="text-block-2">
          <p><?php echo Yii::t('app','Мы - активная фирма по управлению инвестициями, ориентированная на предоставление привлекательных решений и решений для клиентского портфеля, развертывание новейших технологий в нашем бизнесе, чтобы помочь нам оставаться в авангарде нашей развивающейся отрасли. Наши пять бизнесов по управлению инвестициями используют нашу инфраструктуру мирового класса для предоставления разнообразных стратегий по инвестиционным подходам, стилям и классам активов.');?></p>
          <div class="progress-linear-wrap">
            <!-- Linear progress bar-->
            <article class="progress-linear">
              <div class="progress-header">
                <p><?php echo Yii::t('app','Криптовалюты');?></p><span class="progress-value">40</span>
              </div>
              <div class="progress-bar-linear-wrap">
                <div class="progress-bar-linear"></div>
              </div>
            </article>
            <!-- Linear progress bar-->
            <article class="progress-linear">
              <div class="progress-header">
                <p><?php echo Yii::t('app','Фондовая биржа');?></p><span class="progress-value">15</span>
              </div>
              <div class="progress-bar-linear-wrap">
                <div class="progress-bar-linear"></div>
              </div>
            </article>
            <!-- Linear progress bar-->
            <article class="progress-linear">
              <div class="progress-header">
                <p><?php echo Yii::t('app','Форекс');?></p><span class="progress-value">20</span>
              </div>
              <div class="progress-bar-linear-wrap">
                <div class="progress-bar-linear"></div>
              </div>
            </article>
          </div><a class="button button-lg button-secondary" href="/about"><?php echo Yii::t('app','Подробнее');?></a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Financial Statistics-->
<section class="section section-lg bg-primary-dark novi-background bg-cover">
  <div class="container">
    <h2 class="text-center"><?php echo Yii::t('app','Статистика');?></h2>
    <div class="row row-20 justify-content-center justify-content-lg-between">
      <div class="col-md-12 col-lg-4 wow fadeIn">
        <blockquote class="quote quote-default">
          <div class="quote-icon mdi mdi-format-quote"></div>
          <div class="quote-body">
            <q class="heading-6"><?php echo Yii::t('app','Наш фонд опирается на разнообразные знания и специальные знания инвестиционных механизмов, чтобы создавать и реализовывать инновационные и индивидуальные инвестиционные программы «с несколькими двигателями».');?></q>
          </div>
            <?php /*
          <div class="quote-meta">
            <div class="author">
              <cite><?php echo Yii::t('app','Dmitry Timchenko');?></cite>
            </div>
            <div class="position"><?php echo Yii::t('app','CEO &amp; Founder of Ingamo');?></div>
          </div>
 */ ?>
        </blockquote>
      </div>
      <div class="col-md-8 col-lg-5 col-xxl-4 wow fadeIn">
        <!-- gradient blocks-->
        <!-- gradient primary-->
        <svg class="svg-hidden">
          <!-- gradient-->
          <lineargradient id="linear-gradient-primary" x1="50%" y1="30%" x2="50%" y2="100%">
            <stop offset="0%" stop-color="#a371f7"></stop>
            <stop offset="100%" stop-color="#fff"></stop>
          </lineargradient>
        </svg>
        <!-- gradient secondary-->
        <svg class="svg-hidden">
          <!-- gradient-->
          <lineargradient id="linear-gradient-secondary" x1="50%" y1="30%" x2="50%" y2="100%">
            <stop offset="0%" stop-color="#a371f7"></stop>
            <stop offset="100%" stop-color="#130c37"></stop>
          </lineargradient>
        </svg>
        <div class="d3-chart" id="spline-chart" style="width:100%; margin: 0 auto"></div>
      </div>
<?php /*
      <div class="col-md-4 col-lg-3 col-xxl-2">
        <div class="row row-fix row-40">
          <div class="col-6 col-md-12">
            <div class="progress-bar-circle-wrap text-center">
              <div class="progress-bar-circle" data-value="0.5" data-gradient="#fff" data-empty-fill="#21369d" data-size="100" data-thickness="2"><span></span></div>
              <p class="progress-bar-circle-title"><?php echo Yii::t('app','Innovations');?></p>
            </div>
          </div>
          <div class="col-6 col-md-12">
            <div class="progress-bar-circle-wrap text-center">
              <div class="progress-bar-circle" data-value="1" data-gradient="#fff" data-empty-fill="#21369d" data-size="100" data-thickness="2"><span></span></div>
              <p class="progress-bar-circle-title"><?php echo Yii::t('app','Results');?></p>
            </div>
          </div>
        </div>
      </div>
    */ ?>
    </div>
  </div>
</section>
<?php /*
<!-- Testimonials-->
<section class="section section-lg bg-gray-100 novi-background bg-cover">
  <div class="container text-center">
    <h2><?php echo Yii::t('app','О нас говорят');?></h2>
    <!-- Owl Carousel-->
    <div class="owl-carousel text-left" data-items="1" data-md-items="2" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false" data-autoplay="true">
      <blockquote class="quote quote-boxed">
        <q><?php echo Yii::t('app','Ингамо - это инновационная и профессиональная компания, которая предоставила нам ценные услуги по инвестициям и доверительному управлению. Мы очень рады нашему решению сотрудничать с ними.');?></q>
        <div class="quote-author">
          <div class="author-media"><img src="/ingamo/images/reviews/sao23ha_23hcsa.gif" alt="" width="64" height="64"/></div>
          <div class="author-body">
            <div class="author">
              <cite><?php echo Yii::t('app','Peter Jansen');?></cite>
            </div>
            <div class="position"><?php echo Yii::t('app','Sales manager');?></div>
          </div>
        </div>
      </blockquote>
      <blockquote class="quote quote-boxed">
        <q><?php echo Yii::t('app','Наша компания с большим уважением относится к техническим и управленческим возможностям Ингамо, чьи сотрудники очень компетентны, с ними легко работать и которые дали результаты, которые оказались полезными.');?></q>
        <div class="quote-author">
          <div class="author-media"><img src="/ingamo/images/reviews/sakdl3j2_dsah2.gif" alt="" width="64" height="64"/></div>
          <div class="author-body">
            <div class="author">
              <cite><?php echo Yii::t('app','Ayna Abeba');?></cite>
            </div>
            <div class="position"><?php echo Yii::t('app','Marketer');?></div>
          </div>
        </div>
      </blockquote>
      <blockquote class="quote quote-boxed">
        <q><?php echo Yii::t('app','Я выбрал компанию Ингамо потому, что ее опыт позволяет успешно торговать на бирже, преумножая деньги в долгосрочной перспективе. Я ставлю перед собой цель по увеличению имеющихся у меня денежных средств. В данной компании я могу выступать как в роли пассивного инвестора, так и активного.  Это программа, позволяющая получать дополнительный доход от рекомендаций. Это мне очень нравится. Я очень доволен выбором компании Ингамо и уверен в успехе.');?></q>
        <div class="quote-author">
          <div class="author-media"><img src="/ingamo/images/reviews/2139uj21sbd8sahi_sad2.png" alt="" width="64" height="64"/></div>
          <div class="author-body">
            <div class="author">
              <cite><?php echo Yii::t('app','Emilie Lindberg');?></cite>
            </div>
            <div class="position"><?php echo Yii::t('app','Teacher');?></div>
          </div>
        </div>
      </blockquote>
      <blockquote class="quote quote-boxed">
        <q><?php echo Yii::t('app','Мой предыдущий инвест проект развалился. Стал искать,  что то существенное.  Рассмотрев все имеющиеся сегодня предложения, а их было не мало – решил присоединиться к Ингамо. Поверьте, я досконально изучил эту компанию и не нашел ничего более достойного на сегодняшний день.  Сбалансировность заработка и накопления в сочетании с прозрачностью и открытостю руководства поставила проект Ингамо на первое место среди всех сравниваемых мною проектов.  Доступность входа, и достойный  заработка на финише в сочетании с маркетинговыми бонусами делают этот проект неповторимым.  Очень рад присоединиться в команду Ингамо.');?></q>
        <div class="quote-author">
          <div class="author-media"><img src="/ingamo/images/reviews/sa9021h_dh9832h.png" alt="" width="64" height="64"/></div>
          <div class="author-body">
            <div class="author">
              <cite><?php echo Yii::t('app','Han Je Byung');?></cite>
          </div>
          <div class="position"><?php echo Yii::t('app','Student');?></div>
        </div>
      </blockquote>
      <blockquote class="quote quote-boxed">
        <q><?php echo Yii::t('app','Отличный, грамотно рассчитаный проект. Не бойтесь инвестировать. Спасибо администрации проекта за поддержку. Наверное первый проект, из тех которые мне доводилось встречать, работает открыто. Я уверен в компании и уверен что команда аналитиков и трейдеров приведет меня и многих других к успеху! Проект замечательный.  Я очень доволен. Так держать!');?></q>
        <div class="quote-author">
          <div class="author-media"><img src="/ingamo/images/reviews/2013u213bdsuj_dsa.png" alt="" width="64" height="64"/></div>
          <div class="author-body">
            <div class="author">
              <cite><?php echo Yii::t('app','Lars Wallin');?></cite>
            </div>
            <div class="position"><?php echo Yii::t('app','Builder');?></div>
          </div>
        </div>
      </blockquote>
      <blockquote class="quote quote-boxed">
        <q><?php echo Yii::t('app','Решил поделиться с вами своим приятным впечатлением о компании Ингамо. Сюда можно смело приглашать даже близких друзей и не бояться. Здесь невозможно потерять. Можно не бегать по другим проектам в поисках заработка на жизнь.  А спокойно уделить все внимание и усилия на работу с командой для достижения поставленных целей. Рынок криптовалют набирает обороты, но чтобы не потерять – надо довериться профессионалам. Компания Мнгамо настоящие профессионалы своего дела!');?></q>
        <div class="quote-author">
          <div class="author-media"><img src="/ingamo/images/reviews/230rjf34vbvs_ds2d.png" alt="" width="64" height="64"/></div>
          <div class="author-body">
            <div class="author">
              <cite><?php echo Yii::t('app','Lee Haeng');?></cite>
            </div>
            <div class="position"><?php echo Yii::t('app','Translator');?></div>
          </div>
        </div>
      </blockquote>
    </div>
  </div>
</section>
*/?>
<!-- How to Order a New Card-->
<section class="section section-lg section-decorate section-decorate-1 novi-background bg-cover">
  <div class="container text-center">
    <h2><?php echo Yii::t('app','Наши ценности');?></h2>
    <div class="row row-40 justify-content-center number-counter">
      <div class="col-sm-6 col-lg-3 wow fadeInLeft">
        <div class="blurb-icon-fill">
          <div class="icon mercury-icon-user"><span class="index-counter"></span></div>
          <h5 class="title"><?php echo Yii::t('app','Кто мы:');?></h5>
          <p class="exeption"><?php echo Yii::t('app','Команда аналитиков, трейдеров и программистов. Всего в команде 7 человек. Мы настроены на честную и долгосрочную работу с нашими клиентами. Мы заинтересованы сначала показать результат, а потом получить процент от Вашего дохода.');?></p>
        </div>
      </div>
      <div class="col-sm-6 col-lg-3 wow fadeInLeft" data-wow-delay="0.1s">
        <div class="blurb-icon-fill">
          <div class="icon mercury-icon-gear"><span class="index-counter"></span></div>
          <h5 class="title"><?php echo Yii::t('app','Опыт:');?></h5>
          <p class="exeption"><?php echo Yii::t('app','Трейдить на крипто валютном рынке начали в 2016г. Все началось с майнинга. Майнить начали в 2016 году, все средства поступали на биржу, и хотелось заработать больше, именно так и пришли к плотному изучению деятельности трейдинга.');?></p>
        </div>
      </div>
      <div class="col-sm-6 col-lg-3 wow fadeInLeft" data-wow-delay="0.2s">
        <div class="blurb-icon-fill">
          <div class="icon mercury-icon-target-2"><span class="index-counter"></span></div>
          <h5 class="title"><?php echo Yii::t('app','Результат:');?></h5>
          <p class="exeption"><?php echo Yii::t('app','Наша команда работает 4 года с положительным результатом. Раз в неделю проводим прямые трансляции с командой разработчиков, на которых производится анализ результатов торгов, и даются ответы на все Ваши вопросы.');?></p>
        </div>
      </div>
      <div class="col-sm-6 col-lg-3 wow fadeInLeft" data-wow-delay="0.3s">
        <div class="blurb-icon-fill">
          <div class="icon mercury-icon-lightbulb-gears"><span class="index-counter"></span></div>
          <h5 class="title"><?php echo Yii::t('app','Иновации:');?></h5>
          <p class="exeption"><?php echo Yii::t('app','Наша команда профессионалов работает с использованием искусственного интеллекта , что позволяет получить сверхприбыль и стабильный доход.');?></p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-xs bg-primary novi-background bg-cover">
  <div class="container">
    <div class="box-cta">
      <div class="box-cta-inner">
        <h3><?php echo Yii::t('app','Ingamo Fund. – это объединение  предпринимателей, инвесторов, созидателей, космополитов и гениев');?></h3>
      </div>
      <div class="box-cta-inner"><a class="button button-lg button-primary" href="/login"><?php echo Yii::t('app','Пpиcoeдинитьcя');?></a></div>
    </div>
  </div>
</section>
<section class="section section-lg novi-background bg-cover">
  <div class="container text-center">
    <h2><?php echo Yii::t('app','Часто Задаваемые Вопросы');?></h2>
    <div class="row row-flex row-40 number-counter text-left">
      <div class="col-sm-12 col-lg-4 wow fadeIn">
        <div class="text-block-lined">
          <h5 class="title"><?php echo Yii::t('app','Каким образом компания получает прибыль?');?></h5>
          <p><?php echo Yii::t('app','Мы стремимся поддерживать сбалансированный портфель среднесрочных и долгосрочных вкладов, а также эффективно диверсифицируем стратегически важные активы, что в свою очередь дает гарантии стабильного результата. Сильная команда опытных трейдеров и аналитиков компании Ingamo сосредоточена на активной торговле на различных биржах, чтобы увеличить портфельный баланс Фонда для большей эффективности.');?></p>
          <h5 class="title"><?php echo Yii::t('app','Я не очень хорошо разбираюсь в инвестиционной деятельности, но хотел бы иметь пассивный доход, с чего мне начать?');?></h5>
          <p><?php echo Yii::t('app',' Для того чтобы получать доход вам необходимо купить тарифный план и начать зарабатывать 204% годовых с выплатой процентов ежемесячно.');?></p>
        </div>
      </div>
      <div class="col-sm-12 col-lg-4 wow fadeIn">
        <div class="text-block-lined">
          <h5 class="title"><?php echo Yii::t('app','Какой доход мне гарантирует компания?');?></h5>
          <p><?php echo Yii::t('app','Компания предполагает, что доход по предложенным тарифным планам может оказаться до 240%. Но вы также должны понимать, что эта сумма была высчетана по результатам прошлых лет. В будущем эта сумма может измениться как в большую, так и в меньшую сторону. Так же не забывайте, что мы занимаемся высокодоходной инвестиционной деятельностью связанной с рисками. Подробнее читайте пункт «Какие есть риски?»');?></p>
          <h5 class="title"><?php echo Yii::t('app','Какие существуют риски и есть ли они вообще?');?></h5>
          <p><?php echo Yii::t('app','Уведомление о рисках: Мы занимаемся высокодоходной инвестиционной деятельностью связанной с рисками.  Высокий уровень риска может повлечь потерю всего инвестиционного капитала. Поэтому наши условия могут не подходить для всех инвесторов. Вы не должны рисковать тем, что не готовы потерять. Перед началом инвестирования, пожалуйста, убедитесь, что понимаете все риски и учитываете свой уровень опыта.');?></p>
        </div>
      </div>
      <div class="col-sm-12 col-lg-4 wow fadeIn">
        <div class="text-block-lined">
          <h5 class="title"><?php echo Yii::t('app','Кто имеет доступ к моим личным данным и насколько надежно они защищены?');?></h5>
          <p><?php echo Yii::t('app','Ваши средства хранятся на аккаунтах бирж, букмейкерских контор и на персональных платежных системах верифицированных на компанию, которые отвечают за безопасность хранения средств и личных данных. Все учетные данные защищены максимальным уровнем защиты и 2-х факторной аутентификацией.');?></p>
          <h5 class="title"><?php echo Yii::t('app','то мне делать если я не нашел своего вопроса?');?></h5>
          <p><?php echo Yii::t('app','Если вы не нашли своего вопроса, вы можете задать его нам на почту: support@ingamo.fund. А также в разделе тикеты вашего личного кабинета инвестора. Мы постараемся в кротчайшие сроки ответить на него. Самые интересные и частые вопросы будут опубликованы на этой странице.');?></p>
        </div>
      </div>
    </div>
  </div>
</section>
<?php /*
<section class="section section-lg bg-gray-100 novi-background bg-cover">
  <div class="container text-center">
    <h2><?php echo Yii::t('app','Latest Blog Posts');?></h2>
    <div class="row row-flex row-40 justify-content-center number-counter text-left wow fadeInUp">
      <div class="col-sm-6 col-lg-3">
        <article class="post-classic-2"><a class="media-wrapper" href="single-post.html"><img src="/ingamo/images/masonry-blog-1-270x176.jpg" alt="" width="270" height="176"/></a>
          <div class="post-meta-main">
            <div class="post-meta-item">
              <ul class="list-tags">
                <li><a class="tag" href="single-post.html">News</a>
                </li>
              </ul>
            </div>
            <div class="post-meta-item">
              <div class="post-author"><span>by</span> <a href="single-post.html">Martha Ryan</a>
              </div>
            </div>
          </div>
          <h6 class="post-title"><a href="single-post.html">Undertaking Custom Software Development</a></h6>
          <p class="post-exeption">When your business needs custom software development, it may be tempting to turn the project over to in-house IT but you may want to...</p>
          <div class="post-date">2 days ago</div>
        </article>
      </div>
      <div class="col-sm-6 col-lg-3">
        <article class="post-classic-2"><a class="media-wrapper" href="single-post.html"><img src="/ingamo/images/masonry-blog-2-270x176.jpg" alt="" width="270" height="176"/></a>
          <div class="post-meta-main">
            <div class="post-meta-item">
              <ul class="list-tags">
                <li><a class="tag" href="single-post.html">News</a>
                </li>
              </ul>
            </div>
            <div class="post-meta-item">
              <div class="post-author"><span>by</span> <a href="single-post.html">Lawrence Kelly</a>
              </div>
            </div>
          </div>
          <h6 class="post-title"><a href="single-post.html">Can Mobile Software Give Your Business an Advantage Your Competitors Lack?</a></h6>
          <p class="post-exeption">If you haven't employed the benefits of mobile business intelligence, you are tying the hands of your managers and business users. No matter how...</p>
          <div class="post-date">2 days ago</div>
        </article>
      </div>
      <div class="col-sm-6 col-lg-3">
        <article class="post-classic-2"><a class="media-wrapper" href="single-post.html"><img src="/ingamo/images/masonry-blog-3-270x176.jpg" alt="" width="270" height="176"/></a>
          <div class="post-meta-main">
            <div class="post-meta-item">
              <ul class="list-tags">
                <li><a class="tag" href="single-post.html">News</a>
                </li>
              </ul>
            </div>
            <div class="post-meta-item">
              <div class="post-author"><span>by</span> <a href="single-post.html">Theresa Simpson</a>
              </div>
            </div>
          </div>
          <h6 class="post-title"><a href="single-post.html">Choosing a Software Development Company</a></h6>
          <p class="post-exeption">Choosing the right software development company for your product is like hiring a crew to build your new house. Triple check the contractor...</p>
          <div class="post-date">2 days ago</div>
        </article>
      </div>
      <div class="col-sm-6 col-lg-3">
        <article class="post-classic-2"><a class="media-wrapper" href="single-post.html"><img src="/ingamo/images/masonry-blog-4-270x176.jpg" alt="" width="270" height="176"/></a>
          <div class="post-meta-main">
            <div class="post-meta-item">
              <ul class="list-tags">
                <li><a class="tag" href="single-post.html">News</a>
                </li>
              </ul>
            </div>
            <div class="post-meta-item">
              <div class="post-author"><span>by</span> <a href="single-post.html">Lawrence Kelly</a>
              </div>
            </div>
          </div>
          <h6 class="post-title"><a href="single-post.html">What Skills Should You Test While Hiring a React Native Developer?</a></h6>
          <p class="post-exeption">React Native is obviously a great mobile app development framework. It’s good if you have chosen React Native for building mobile apps, but...</p>
          <div class="post-date">2 days ago</div>
        </article>
      </div>
    </div>
    <div class="button-wrap-lg"><a class="button button-lg button-secondary" href="classic-blog.html">View All Blog Posts</a></div>
  </div>
</section>
*/ ?>
<section class="section section-lg novi-background bg-cover">
  <div class="container text-center">
    <h2><?php echo Yii::t('app', 'Инвестиционные портфели');?></h2>
    <div class="row no-gutters justify-content-center">
        <?php /*
      <div class="col-md-4 bounding-fix">
        <div class="price-box-1 price-box-decor-top">
          <div class="title"><?php echo Yii::t('app', 'Инвест план "Light"');?></div>
          <p class="exeption"><?php echo Yii::t('app', 'Доходность в месяц');?>: <strong>6-8%</strong></p>
          <p class="exeption"><?php echo Yii::t('app', 'Срок депозита');?>: <strong>90 <?php echo Yii::t('app', 'дней');?></strong></p>
          <p class="exeption"><?php echo Yii::t('app', 'По окончанию действия депозита тело возвращается');?>:</p>
          <div class="heading-3 price"><?php echo Yii::t('app', '24%<br/> доходность');?></div>
          <a class="button button-lg button-secondary" href="/office/invest"><?php echo Yii::t('app', 'Оформить');?></a>
        </div>
      </div>
        */?>
      <div class="col-md-4 bounding-fix">
        <div class="price-box-1 price-box-1-primary">
          <div class="title"><?php echo Yii::t('app', 'Инвест план "Normal"');?></div>
          <p class="exeption"><?php echo Yii::t('app', 'Доходность в месяц');?>: <strong>17%</strong></p>
          <p class="exeption"><?php echo Yii::t('app', 'Срок депозита');?>: <strong>360 <?php echo Yii::t('app', 'дней');?></strong></p>
          <p class="exeption"><?php echo Yii::t('app', 'Выплата процентов: ежемесячно');?></p>
          <div class="heading-3 price"><?php echo Yii::t('app', '204%<br/> доходность');?></div>
          <a class="button button-lg button-secondary" href="/office/invest"><?php echo Yii::t('app', 'Оформить');?></a>
        </div>
      </div>
        <?php /*
      <div class="col-md-4 bounding-fix">
        <div class="price-box-1 price-box-decor-bottom">
          <div class="title"><?php echo Yii::t('app', 'Инвест план "Max"');?></div>
          <p class="exeption"><?php echo Yii::t('app', 'Доходность в месяц');?>: <strong>13-20%</strong></p>
          <p class="exeption"><?php echo Yii::t('app', 'Срок депозита');?>: <strong><?php echo Yii::t('app', '360 дней');?></strong></p>
          <p class="exeption"><?php echo Yii::t('app', 'По окончанию действия депозита тело возвращается');?>:</p>
          <div class="heading-3 price"><?php echo Yii::t('app', '220%<br/> доходность');?></div>
          <a class="button button-lg button-secondary" href="/office/invest"><?php echo Yii::t('app', 'Оформить');?></a>
        </div>
      </div>
 */ ?>
    </div>
  </div>
</section>