<?php
$this->title = Yii::t('app', 'Доступ запрещён');
?>

<section class="section-bredcrumbs custom-bg-image novi-background">
  <div class="container context-dark breadcrumb-wrapper">
    <h1><?php echo $this->title;?></h1>
    <ul class="breadcrumbs-custom">
      <li><a href="/"><?php echo Yii::t('app', 'Главная');?></a></li>
      <li class="active"><?php echo $this->title;?></li>
    </ul>
  </div>
</section>
<!-- Join Our Team-->
<section class="section section-lg custom-image-section novi-background bg-cover">
  <div class="container relative-container">
    <div class="row row-30 row-md-60 justify-content-between">
      <div class="col-md-12">
        <h2><?php echo Yii::t('app', 'Ошибка');?></h2>
      </div>
      <div class="col-md-12">
        <div class="heading-6"><?php echo Yii::t('app', 'Вам необходимо подтвердить свой E-mail');?></div>
        <a class="button button-lg button-secondary" href="/office/dashboard"><?php echo Yii::t('app', 'перейти в личный кабинет');?></a>
      </div>
    </div>
  </div>
</section>