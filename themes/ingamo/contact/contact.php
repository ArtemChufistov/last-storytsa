<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\captcha\Captcha;
use yii\web\View;

$this->title = Yii::t('app', 'Связатсья с нами');
?>

<?php if(Yii::$app->session->hasFlash('success')): ?>

    <div class="successModal modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?= Yii::t('app', 'Ваше сообщение принято');?></h4>
          </div>
          <div class="modal-body">
            <p><?= Yii::t('app', 'Спасибо, что оставили нам сообщение, мы обязательно свяжемся с вам в ближайшее время');?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Закрыть');?></button>
            <button type="button" class="btn btn-primary" data-dismiss="modal"><?= Yii::t('app', 'Ok');?></button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <?php echo $this->registerJs("$('.successModal').modal('show');", View::POS_END);?>

<?php endif;?>

<!-- Breadcrumbs-->
<section class="section-bredcrumbs custom-bg-image novi-background">
  <div class="container context-dark breadcrumb-wrapper">
    <h1>Contacts</h1>
    <ul class="breadcrumbs-custom">
      <li><a href="index.html">Home</a></li>
      <li class="active">Contacts</li>
    </ul>
  </div>
</section>
<!-- Mailform-->
<section class="section section-lg bg-default novi-background bg-cover">
<div class="container">
  <div class="row row-50 justify-content-between">
    <div class="col-md-6 col-lg-4 col-xl-3">
      <!-- Bootstrap tabs-->
      <div class="tabs-custom tabs-horizontal tabs-line" id="tabs-1">
        <div class="tab-content">
          <div class="tab-pane fade show active" id="tabs-1-1">
            <ul class="contact-box">
              <li>
                <div class="unit unit-horizontal unit-spacing-xxs">
                  <div class="unit-left"><span class="icon novi-icon mdi mdi-email-outline"></span></div>
                  <div class="unit-body"><a class="hover-text" href="mailto:#">support@ingamo.fund</a></div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="group group-middle social-items">
        <a class="icon novi-icon icon-md icon-gray-400 novi-icon mdi mdi-facebook" href="#"></a>
        <a class="icon novi-icon icon-md icon-gray-400 novi-icon mdi mdi-twitter" href="#"></a>
        <a class="icon novi-icon icon-md icon-gray-400 novi-icon mdi mdi-instagram" href="#"></a>
        <?php /*
        <a class="icon novi-icon icon-md icon-gray-400 novi-icon mdi mdi-facebook-messenger" href="#"></a>
        <a class="icon novi-icon icon-md icon-gray-400 novi-icon mdi mdi-linkedin" href="#"></a>
        <a class="icon novi-icon icon-md icon-gray-400 novi-icon mdi mdi-snapchat" href="#"></a>
        */ ?>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <h4><?php echo Yii::t('app', 'Связатсья с нами');?></h4>
      <!-- RD Mailform-->
      <?php $form = ActiveForm::begin(); ?>
        <div class="form-wrap">
          <?= $form->field($model, 'name') ?>
        </div>
        <div class="form-wrap">
          <?= $form->field($model, 'email') ?>
        </div>
        <div class="form-wrap">
          <?= $form->field($model, 'message')->textArea(['rows' => '6']); ?>
        </div>
        <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary']) ?>
      <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-12 col-lg-3">
    </div>
  </div>
</div>
</section>