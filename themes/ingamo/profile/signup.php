<?php
use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Регистрация');
?>

<div id="primary" class="p-t-b-100 height-full">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-md-auto">
                <div class="shadow r-10">
                <div class="row grid">
                    <div class="col-md-6 white p-5">

                        <div class="mb-5">
                           <img src="/ingamo/img/basic/logo.png" width="60" alt="">
                        </div>

                        <?php $form = ActiveForm::begin([
                            'enableClientScript' => false,
                            'options' => ['class' => 'form-material form-red-help'],
                            'fieldConfig' => [
                                'options' => [
                                    'tag' => false,
                                ],
                            ]]); ?>

                            <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <?= $form->field($model, 'ref')->textInput(['readonly'=> true])->label($model->getAttributeLabel('ref'),['class'=>'form-label']); ?>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <?= $form->field($model, 'login', ['template' => '<div class="form-line">{input}{label}</div>{error}'])
                                        ->textInput()->label($model->getAttributeLabel('login'),['class'=>'form-label']); ?>
                                </div>

                                <div class="form-group form-float">
                                    <?= $form->field($model, 'email', ['template' => '<div class="form-line">{input}{label}</div>{error}'])
                                        ->textInput()->label($model->getAttributeLabel('email'),['class'=>'form-label']); ?>
                                </div>

                                <div class="form-group form-float">
                                    <?= $form->field($model, 'password', ['template' => '<div class="form-line">{input}{label}</div>{error}'])
                                        ->passwordInput()->label($model->getAttributeLabel('password'),['class'=>'form-label']); ?>
                                </div>

                                <div class="form-group form-float">
                                    <?= $form->field($model, 'passwordRepeat', ['template' => '<div class="form-line">{input}{label}</div>{error}'])
                                        ->passwordInput()->label($model->getAttributeLabel('passwordRepeat'),['class'=>'form-label']); ?>
                                </div>

                                <?php echo $form->field($model, 'captcha', ['template' => '{input}{label}{error}'])->widget(Captcha::className(), [
                                    'captchaAction' => '/profile/profile/captcha',
                                    'options' => [
                                        'class' => 'form-control',
                                    ],
                                    'template' => '
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <label class="form-label" for="signupform-captcha">' . $model->getAttributeLabel('captcha') . '</label>
                                                {input}
                                            </div>
                                        </div>
                                        <div class="form-group form-float">{image}</div>
                                    '])->label(false);?>

                                <div class="row">
                                    <div class="card-body" style="padding: 1rem;">
                                        <ul class="list-group" >
                                            <li class="list-group-item">
                                                <?php echo Yii::t('app', 'Я согласен с условиями') . ' ' . Html::a(Yii::t('app', 'лицензионного соглашения'), ['/licenseagreement'], ['target' => '_blank']);?>
                                                <?= $form->field($model, 'licenseAgree', ['template' => '<div class="material-switch float-right">{input}{label}</div>{error}', 'options' => ['tag' => false]])->checkBox([], false)->label('',['class'=>'bg-success']);?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <?= Html::submitButton(Yii::t('app', 'Зарегистрироваться'), ['class' => 'btn btn-primary btn-sm pl-4 pr-4']) ?>

                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="col-md-6 blue p-5">
                        <i class="icon-circle-o s-36 text-white"></i>
                        <h1 class="mt-3 text-white"><?php echo Yii::t('app', 'Есть аккаунт?');?></h1>

                        <div class="pt-3 mb-5 text-white">
                            <p><?php echo Yii::t('app', 'Просто войдите в свой аккаунт');?></p>
                        </div>

                        <?php echo Html::a(Yii::t('app', 'Войти'), ['/login'], ['class' => 'btn btn-light s-14 pl-4 pr-4 text-primary'])?>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>