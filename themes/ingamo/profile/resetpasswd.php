<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Восстановление пароля');
?>

<main>
    <div id="primary" class="p-t-b-100 height-full">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mx-md-auto paper-card">
                    <div class="text-center">
                        <img src="/ingamo/img/dummy/u4.png" alt="">
                        <h3 class="mt-2"><?= Html::encode($this->title) ?></h3>
                    </div>
                    <?php if (Yii::$app->session->hasFlash('reset-success')):?>
                      <?php echo Yii::$app->session->getFlash('reset-success');?>
                    <?php else: ?>
                        <?php $form = ActiveForm::begin([
                          'fieldConfig' => [
                              'template' => "{input}\n{hint}\n{error}",
                          ],
                          'options' => [
                              'data-form-output' => 'form-output-global',
                              'data-form-type' => 'contact',
                              'class' => 'text-left offset-top-30'
                          ]
                        ]); ?>
                        <?= $form->field($forget, 'email')->textInput([
                              'maxlength' => true,
                              'placeholder' => $forget->getAttributeLabel('email'),
                        ]); ?>

                        <?= $form->field($forget, 'password')->textInput([
                            'maxlength' => true,
                            'placeholder' => $forget->getAttributeLabel('password'),
                        ]); ?>

                        <?= $form->field($forget, 'captcha')->widget(Captcha::className(), [
                            'captchaAction' => '/profile/profile/captcha',
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => $forget->getAttributeLabel('captcha')
                            ]
                        ]);?>

                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                              <?= Html::submitButton(Yii::t('user', 'Сменить пароль'), [
                                'class' => 'btn btn-primary btn-block text-uppercase waves-effect waves-light',
                                'name' => 'save-button']) ?>
                            </div>
                        </div>

                      <?php ActiveForm::end();?>

                    <?php endif;?>

                    <?php echo Html::a(Yii::t('user', 'Личный кабинет'), ['/login'], ['class' => 'btn btn-info btn-block text-uppercase waves-effect waves-light'])?>
                    <?php echo Html::a(Yii::t('user', 'Регистрация'), ['/signup'], ['class' => 'btn btn-info btn-block text-uppercase waves-effect waves-light'])?>
                </div>
            </div>
        </div>
    </div>
</main>