<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('user', 'Вход на сайт');
?>

<div id="primary" class="p-t-b-100 height-full">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-md-auto">
                <div class="shadow r-10">
                <div class="row grid">
                    <div class="col-md-5 white p-5">

                        <div class="mb-5">
                           <img src="/ingamo/img/basic/logo.png" width="60" alt="">
                        </div>

                        <?php $form = ActiveForm::begin([
                            'enableClientScript' => false,
                            'options' => ['class' => 'form-material'],
                            'fieldConfig' => [
                                'options' => [
                                    'tag' => false,
                                ],
                            ],
                        ]); ?>
                            <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <?= $form->field($model, 'loginOrEmail')->textInput()->label($model->getAttributeLabel('loginOrEmail'),['class'=>'form-label']); ?>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <?= $form->field($model, 'password')->passwordInput()->label($model->getAttributeLabel('password'),['class'=>'form-label']); ?>
                                    </div>
                                </div>

                                <?= Html::submitButton(Yii::t('app', 'Войти'), ['class' => 'btn btn-primary btn-sm pl-4 pr-4']) ?>

                                <div class="pt-5 pb-5">
                                    <?php echo Html::a(Yii::t('app', '<i class="icon-add"></i>Восстановить пароль'), ['/resetpasswd'], ['class' => 'grey-text'])?>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="col-md-7 blue p-5">
                        <i class="icon-circle-o s-36 text-white"></i>
                        <h1 class="mt-3 text-white"><?php echo Yii::t('app', 'Нет аккаунта?');?></h1>

                        <div class="pt-3 mb-5 text-white">
                            <p><?php echo Yii::t('app', 'Это не проблема, просто зарегистрируйтесь в системе!');?></p>
                        </div>

                        <?php echo Html::a(Yii::t('app', 'Регистрация'), ['/signup'], ['class' => 'btn btn-light s-14 pl-4 pr-4 text-primary'])?>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>