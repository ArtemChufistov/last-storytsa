<?php
use app\modules\lang\widgets\WLang;
use yii\web\View;
use yii\helpers\Url;
?>

<header class="section page-header">
  <!-- RD Navbar-->
  <div class="rd-navbar-wrap rd-navbar-absolute">
    <nav class="rd-navbar rd-navbar-creative" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-lg-stick-up-offset="20px" data-xl-stick-up-offset="20px" data-xxl-stick-up-offset="20px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
      <div class="rd-navbar-main-outer">
        <div class="rd-navbar-main">
          <!-- RD Navbar Panel-->
          <div class="rd-navbar-panel">
            <!-- RD Navbar Toggle-->
            <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
            <!-- RD Navbar Brand-->
            <div class="rd-navbar-brand"><a class="brand" href="/"><img src="/ingamo/images/logo-default-99x24.png" alt="" width="99" height="24"/></a>
            </div>
          </div>
          <div class="rd-navbar-main-element">
            <div class="rd-navbar-nav-wrap">
              <ul class="rd-navbar-nav">
              <?php foreach($menu->children()->all() as $num => $children):?>
                <li class="rd-nav-item <?php if($requestUrl == Url::to([$children->link])):?>active<?php endif;?>">
                  <a class="rd-nav-link" href="<?= Url::to([$children->link]);?>"><?= $children->title;?></a>
                </li>
                <?php if (!empty($children->children()->all())):?>
                  <ul class="rd-megamenu-list">
                    <?php foreach($children->children()->all() as $numChild => $childChildren):?>
                      <li class="rd-megamenu-list-item">
                        <a class="rd-megamenu-list-link" href="<?= Url::to([$childChildren->link]);?>"><?php echo $childChildren->title;?></a>
                      </li>
                    <?php endforeach;?>
                  </ul>
                <?php endif;?>
              <?php endforeach;?>
              <?=WLang::widget(['view' => '_lang-choser']);?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
</header>