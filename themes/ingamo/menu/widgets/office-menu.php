<?php
use app\modules\lang\widgets\WLang;
use yii\web\View;
use yii\helpers\Url;

$user = $this->params['user']->identity;
?>

<aside class="main-sidebar fixed offcanvas shadow" data-toggle='offcanvas'>
    <section class="sidebar">
        <div class="w-80px mt-3 mb-3 ml-3">
            <img src="/ingamo/img/basic/logo.png" alt="">
        </div>
        <div class="relative">
            <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"
               aria-controls="userSettingsCollapse" class="btn-fab btn-fab-sm absolute fab-right-bottom fab-top btn-primary shadow1 ">
                <i class="icon icon-cogs"></i>
            </a>
            <div class="user-panel p-3 light mb-2">
                <div>
                    <div class="float-left image">
                        <img class="user_avatar" src="<?php echo $user->getImage(); ?>" alt="User Image">
                    </div>
                    <div class="float-left info">
                        <h6 class="font-weight-light mt-2 mb-1"><?php echo $user->login; ?></h6>
                        <a href="/office/user"><i class="icon-circle text-primary blink"></i> <?php echo Yii::t('app', 'Активен');?></a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="collapse multi-collapse" id="userSettingsCollapse">
                    <div class="list-group mt-3 shadow">
                        <a href="/office/user" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-user text-green"></i><?php echo Yii::t('app', 'Профиль');?></a>
                        <a href="/office/finance" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-dollar text-green"></i><?php echo Yii::t('app', 'Пополнить баланс');?></a>
                        <a href="/logout" class="list-group-item list-group-item-action ">
                            <i class="mr-2 icon-power-off text-green"></i><?php echo Yii::t('app', 'Выход');?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header"><strong><?php echo Yii::t('app', 'Личный кабинет');?></strong></li>
            <?php foreach($menu->children()->all() as $num => $children):?>
                <li class="treeview <?php if($requestUrl == Url::to([$children->link])):?>active<?php endif;?>">
                    <a href="<?= Url::to([$children->link]);?>">
                        <?php echo $children->icon; ?>
                        <span><?= $children->title;?></span>
                    </a>
                    <?php if (!empty($children->children()->all())):?>
                      <ul class="treeview-menu">
                        <?php foreach($children->children()->all() as $numChild => $childChildren):?>
                          <li>
                            <a href="<?= Url::to([$childChildren->link]);?>"><i class="icon icon-circle-o"></i><?php echo $childChildren->title;?></a>
                          </li>
                        <?php endforeach;?>
                      </ul>
                    <?php endif;?>
                </li>
            <?php endforeach;?>
        </ul>
    </section>
</aside>