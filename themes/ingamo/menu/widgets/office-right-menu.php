<div class="dropdown-menu p-4 dropdown-menu-right">
    <div class="row box justify-content-between my-4">
        <?php foreach($menu->children()->all() as $num => $children):?>
        <div class="col-md-4 box justify-content-between" style="margin-bottom: 25px;">
            <a href="#">
                <?php echo $children->icon; ?>
                <div class="pt-1"><?= $children->title;?></div>
            </a>
        </div>
        <?php endforeach;?>
    </div>
</div>