<?php
use yii\helpers\Html;
use yii\helpers\Url;

$user = $this->params['user']->identity;
?>

<ul class="nav navbar-top-links navbar-right pull-right">
    <li class="dropdown">
        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="javascript:void(0)"> <img src="<?php echo $this->params['user']->identity->getImage(); ?>" alt="user-img" width="36" class="img-circle">
            <b class="hidden-xs">
                <?php echo $this->params['user']->identity->login; ?> (<?php echo Yii::t('app', 'Баланс:');?>
                <?php foreach($user->getBalances() as $balance):?>
                    <?php echo number_format($balance->value, 2);?> <?php echo $balance->getCurrency()->one()->key;?>
                <?php endforeach;?>
                )
            </b><span class="caret"></span> </a>
        <ul class="dropdown-menu dropdown-user animated flipInY">
            <li>
                <div class="dw-user-box">
                    <div class="u-img"><img src="<?php echo $this->params['user']->identity->getImage(); ?>" alt="user" /></div>
                    <div class="u-text"><h4><?php echo $this->params['user']->identity->login; ?></h4><p class="text-muted"><?php echo $this->params['user']->identity->email; ?></p><a href="<?= Url::to(['/office/user']);?>" class="btn btn-rounded btn-danger btn-sm"><?php echo Yii::t('app', 'Профиль');?></a></div>
                </div>
            </li>
            <li role="separator" class="divider"></li>
            <li><a href="<?= Url::to(['/office/user']);?>"><i class="ti-user"></i> <?php echo Yii::t('app', 'Редактировать профиль');?></a></li>
            <li><a href="<?= Url::to(['/office/in']);?>"><i class="ti-wallet"></i> <?php echo Yii::t('app', 'Пополнить Баланс');?></a></li>
            <li><a href="<?= Url::to(['/office/stat']);?>"><i class="ti-medall"></i> <?php echo Yii::t('app', 'Статистика');?></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?= Url::to(['logout']);?>"><i class="fa fa-power-off"></i> <?= Yii::t('app', 'Выход');?></a></li>
        </ul>
    </li>
</ul>