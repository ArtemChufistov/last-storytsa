<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_body_logo');?>
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/left_sidebar');?>
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/user_info');?>
    </div>
</nav> 