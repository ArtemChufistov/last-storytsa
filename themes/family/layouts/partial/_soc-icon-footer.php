<div class="cell-xs-12 offset-top-66 cell-lg-3 cell-lg-push-1 offset-lg-top-0">
  <!-- Footer brand-->
  <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_logo_light_1');?>
  <p class="text-darker offset-top-4"><?php echo \Yii::t('app', 'Мы в социальных сетях');?></p>
    <ul class="list-inline">
      <li><a href="https://www.facebook.com" class="icon fa fa-facebook icon-xxs icon-circle icon-darkest-filled"></a></li>
      <li><a href="https://twitter.com" class="icon fa fa-twitter icon-xxs icon-circle icon-darkest-filled"></a></li>
      <li><a href="https://plus.google.com" class="icon fa fa-google-plus icon-xxs icon-circle icon-darkest-filled"></a></li>
      <li><a href="https://ru.linkedin.com" class="icon fa fa-linkedin icon-xxs icon-circle icon-darkest-filled"></a></li>
    </ul>
</div>