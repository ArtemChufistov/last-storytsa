<?php
use yii\helpers\Url;
?>

<style type="text/css">
.dropdown-menu{
    z-index: 10000;
}
.tooltip-content5{
    z-index: 1000;
}
</style>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?= $title;?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <a href="<?php echo Url::to(['/office/in']);?>"  class="btn btn-success pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">
        	<?php echo Yii::t('app', 'Пополнить баланс');?>
        </a>
        <ol class="breadcrumb">
        	<?php foreach($breadcrumbs as $breadcrumb): ?>
            	<li><a href="<?php echo $breadcrumb['link'];?>" class="active"><?= $breadcrumb['title'];?></a></li>
            <?php endforeach;?>
            <li><a href="<?php echo Url::to(['/office/dashboard']);?>" class="active"><?= Yii::t('app', 'Личный кабинет');?></a></li>
        </ol>
    </div>
</div>