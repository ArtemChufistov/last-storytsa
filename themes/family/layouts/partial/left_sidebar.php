<?php
use app\modules\finance\models\Transaction;
use app\modules\event\models\EventSearch;
use app\modules\event\models\Event;
use yii\helpers\Html;
use yii\helpers\Url;

$user = $this->params['user'];

$eventModel = new EventSearch();
$eventModel->user_id = $user->id;
$eventProvider = $eventModel->search([]);
?>
<ul class="nav navbar-top-links navbar-left">
    <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
    <li class="dropdown">
        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="javascript:void(0)"> <i class="mdi  mdi-check-circle"></i>
            <?php /*<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div> */?>
        </a>
        <ul class="dropdown-menu mailbox animated bounceInDown">
            <li>
                <div class="drop-title"><?php echo Yii::t('app', 'События')?></div>
            </li>
            <li>
                <div class="message-center">
                    <?php foreach($eventProvider->getModels() as $event):?>
                        <?php $toUser = $event->getToUser()->one();?>
                        <?php if (!empty($toUser)):?>
                            <a href="javascript:void(0)">
                                <div class="user-img"> <img src="<?php echo $event->getToUser()->one()->getImage();?>" alt="user" class="img-circle">
                                    <span class="profile-status online pull-right"></span>
                                </div>
                                <div class="mail-contnet">
                                    <h5><?php echo $toUser->login;?></h5> <span class="mail-desc"><?php echo $event->getTypeTitle();?></span> <span class="time"><?php echo date("H:i:s d-m-y", strtotime($event->date_add));?></span> </div>
                            </a>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
            </li>
            <li>
                <a class="text-center" href="<?php echo Url::to(['/office/stat']);?>">
                    <strong><?php echo Yii::t('app', 'Посмотреть все события'); ?></strong> <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="javascript:void(0)"> <i class="mdi mdi-currency-usd"></i>
            <?php /*<div class="notify"><span class="heartbit"></span><span class="point"></span></div>*/?>
        </a>
        <ul class="dropdown-menu dropdown-tasks animated bounceInDown">
            <?php foreach(Transaction::find()->where(['from_user_id' => $user->id])->orWHere(['to_user_id' => $user->id])->limit(7)->orderBy(['id' => SORT_DESC])->all() as $transaction):?>
                <?php $toUser = $transaction->getToUser()->one();?>
                <?php $fromUser = $transaction->getFromUser()->one();?>

                <li>
                    <a href="javascript:void(0)">
                        <div>
                            <p> <strong><?php echo $transaction->getTypeTitle();?></strong>

                                <?php if (!empty($toUser) && $toUser->id == $user->id):?>
                                    <span class="label label-success label-rouded">+<?php echo number_format($transaction->sum, 2);?> <?php echo Yii::t('app', $transaction->getCurrency()->one()->title);?></span>
                                <?php else:?>
                                    <span class="label label-danger label-rouded">-<?php echo number_format($transaction->sum, 2);?> <?php echo Yii::t('app',  $transaction->getCurrency()->one()->title);?></span>
                                <?php endif;?>

                            </p>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
            <?php endforeach;?>
            <li>
                <a class="text-center" href="<?php echo Url::to(['/office/finance']);?>">
                    <strong><?php echo Yii::t('app', 'Мои финансы'); ?></strong> <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
    </li>
</ul>