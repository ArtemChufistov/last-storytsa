<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\family\OfficeHeadAssetTop;
use app\assets\family\OfficeHeadAssetBot;
use app\modules\menu\widgets\MenuWidget;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\widgets\ChangeEmailWidget;

OfficeHeadAssetTop::register($this);
OfficeHeadAssetBot::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta_office');?>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>

<body class="fix-header">
    <?php $this->beginBody() ?>
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
        </svg>
    </div>
    <div id="wrapper">
        
        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_office_sidebar');?>       

        <?php echo MenuWidget::widget(['menuName' => 'profileMenu', 'view' => 'office-menu']);?>

        <div id="page-wrapper">
            <div class="container-fluid">
                
                <?php echo $content;?>

                <?= ChangeEmailWidget::widget(['user' => $this->params['user']->identity]); ?>

                <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_right_sidebar');?> 

            </div>
            <footer class="footer text-center"> 2017 &copy; Dechmont </footer>
        </div>
    </div>
    <?php $this->endBody() ?>
</body>

</html>


<?php $this->endPage() ?>