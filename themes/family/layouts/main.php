<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\family\FrontAppAssetTop;
use app\assets\family\FrontAssetBottom;

FrontAppAssetTop::register($this);
FrontAssetBottom::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>  
<html lang="en">
<head>

  <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>

  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>

  <?php $this->head() ?>

</head>
<body>
  <?php $this->beginBody() ?>

    <?php echo $content;?>

  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>