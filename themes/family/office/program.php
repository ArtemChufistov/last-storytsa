<?php

use app\modules\profile\widgets\UserTreeElementWidget;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Принять участие в программе');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<?php if(Yii::$app->session->getFlash('message')): ?>
  <div class="modalMessage modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('message');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalMessage').modal('show');
", View::POS_END);?>

<?php endif;?>


<div class="row">
  <div class="col-lg-8">

	<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_payment-buy-place', ['user' => $user, 'matrixCategory' => $matrixCategory, 'rootMatrixes' => $rootMatrixes]);?>

	<div class="content-box">
        <h3 class="content-box-header bg-primary">
			<span class="icon-separator">
                <i class="glyph-icon icon-cog"></i>
            </span>
        	<?php echo Yii::t('app', 'Детальная информация по текущему месту');?>
        </h3>
		<div class="content-box-wrapper matrix-main-wrap">
	      <?php foreach(array_reverse($rootMatrixes) as $num => $rootMatrixItem):?>
				<?php if(!empty($currentPlace)):?>
					<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_matrix', [
						'matrixCategory' => $matrixCategory,
						'currentPlace' => $currentPlace,
						'currentLevel' => $currentLevel,
						'parentPlace' => $parentPlace,
						'rootMatrix' => $rootMatrix,
						'user' => $user,
					]);?>
				<?php else:?>
					<h4 style = "text-align:center;margin-top:30px; margin-bottom: 30px;"><?php echo Yii::t('app', 'У вас еще нет места в программе');?></h4>
				<?php endif;?>
	      <?php endforeach;?>
        </div>
    </div>
  </div>

	<div class="col-lg-4">
		<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_parent-wrap', [
			'parentUser' => $parentUser,
		]);?>

		<?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_matrix-places', [
			'matrixCategory' => $matrixCategory,
			'rootMatrixes' => $rootMatrixes,
			'user' => $user,
		]);?>

	</div>
</div>