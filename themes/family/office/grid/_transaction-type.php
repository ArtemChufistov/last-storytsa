<?php
use app\modules\profile\models\User;
use app\modules\invest\models\UserInvest;
use app\modules\finance\models\Transaction;
?>

<?php echo $model->getTypeTitle();?>

<?php if ($model->type == Transaction::TYPE_PARTNER_BUY_PLACE_TO_DEPOSIT || $model->type == Transaction::TYPE_REF_BONUS):?>
	<?php $userInvest = UserInvest::find()->where(['id' => $model->data])->one();?>

	<?php $user = $userInvest->getUser()->one();?>
	<?php if (!empty($user)):?>
		<?php echo Yii::t('app', ' от <strong>{login}</strong>', ['login' => $user->login]);?>
	<?php endif;?>
<?php elseif($model->type == Transaction::TYPE_INNER_PAY):?>
	<?php $user = User::find()->where(['id' => $model->data])->one();?>

	<?php if (!empty($user)):?>
		<?php if ($model->from_user_id != Yii::$app->user->identity->id):?>
			<?php echo Yii::t('app', ' от <strong>{login}</strong>', ['login' => $user->login]);?>
		<?php else:?>
			<?php echo Yii::t('app', ' to <strong>{login}</strong>', ['login' => $user->login]);?>
		<?php endif;?>
	<?php endif;?>
<?php endif;?>