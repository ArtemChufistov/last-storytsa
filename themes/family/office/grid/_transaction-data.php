
<?php
use app\modules\finance\models\Transaction;
?>
<?php if (in_array($model->type,[Transaction::TYPE_PARTNER_BUY_PLACE, Transaction::TYPE_BUY_MATRIX_PLACE, Transaction::TYPE_REINVEST_MATRIX_PLACE, Transaction::TYPE_WITHDRAW_MATRIX_PLACE])):?>
	<?php $matrixQueue = $model->getMatrixQueue()->one(); if (!empty($matrixQueue)){ echo $matrixQueue->getRootMatrix()->one()->name;}?>
<?php else:?>
	<?php echo $model->data;?>
<?php endif;?>