<?php if ($model->to_user_id == \Yii::$app->user->identity->id):?>
	<span class="label label-success label-rouded">+<?php echo  number_format($model->sum, 2);?> <?php echo $model->getCurrency()->one()->title;?></span>
<?php else:?>
	<span class="label label-danger label-rouded">-<?php echo  number_format($model->sum, 2);?> <?php echo $model->getCurrency()->one()->title;?></span>
<?php endif;?>