<?php

use app\modules\profile\components\AuthKeysManager;
use borales\extensions\phoneInput\PhoneInput;
use yii\authclient\widgets\AuthChoice;
use lowbase\user\models\Country;
use app\modules\profile\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;

$this->title = Yii::t('user', 'Мой профиль');
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
.form-group{
  padding-left: 9px;
  padding-right: 9px;
}
.lb-user-user-profile-password .form-group{
  padding-left: 16px;
  padding-right: 16px;
}
.auth-clients li{
  display: inline-table;
  float: unset;
}
.auth-clients{
  text-align: center;
}
.user-info .form-group{
  margin-bottom: 12px;
}
.intl-tel-input .country-list{
  z-index: 100;
}
</style>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/user'), 'title' => $this->title]
    ]
]);?>

<?php $form = ActiveForm::begin([
  'id' => 'form-profile',
  'options' => [
      'class'=>'form-horizontal form-material',
      'enctype'=>'multipart/form-data'
  ],
]); ?>

<style type="text/css">
  .intl-tel-input{
    width: 100%;
  }

</style>

<div class="row">
  <div class="col-md-4 col-xs-12">
      <div class="white-box">
          <div class="user-bg"> <img width="100%" alt="<?php echo $user->login;?>" src="<?php echo $user->getImage();?>">
              <div class="overlay-box">
                  <div class="user-content">
                      <img src="<?php echo $user->getImage();?>" class="thumb-lg img-circle" alt="<?php echo $user->login;?>">
                      <br/>
                      <br/>
                      <?php if ($user->image):?>
                          <?php echo Html::a(Yii::t('app', 'Удалить фото'), ['/profile/office/remove'], ['class' => 'btn btn-default btn-file']);?>
                      <br/>
                      <?php else: ?>
                        <?= $form->field($user, 'photo', ['options' => ['class' => 'btn btn-default btn-file'], 'template' => '<i class="fa fa-paperclip"></i> ' . Yii::t('app', 'Ваше изображение'). '{input}'])->fileInput() ?>
                      <br/>
                      <br/>
                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                        'class' => 'btn btn-info',
                        'name' => 'signup-button']) ?>
                      <?php endif; ?>

                      <h4 class="text-white"><?php echo $user->login;?></h4>
                      <h5 class="text-white"><?php echo $user->email;?></h5> </div>
              </div>
          </div>


      </div>
      <div class="white-box">
        <h3 class="box-title"><?php echo Yii::t('app', 'Смена пароля');?></h3>

        <div class="lb-user-user-profile-password">
            <?= $form->field($user, 'old_password')->passwordInput([
                'maxlength' => true,
                'value' => '',
                'placeholder' => $user->getAttributeLabel('old_password'),
                'class' => 'form-control password'
            ]) ?>
        </div>

        <div class="lb-user-user-profile-password">
            <?= $form->field($user, 'password')->passwordInput([
                'maxlength' => true,
                'placeholder' => $user->getAttributeLabel('password'),
                'class' => 'form-control password',
                'value' => ''
            ]) ?>
        </div>

        <div class="lb-user-user-profile-password">
            <?= $form->field($user, 'repeat_password')->passwordInput([
                'maxlength' => true,
                'placeholder' => $user->getAttributeLabel('repeat_password'),
                'class' => 'form-control repeat_password',
                'value' => ''
            ]) ?>
        </div>

        <div class="form-group" style = "padding-left: 15px; margin-bottom: 0px;">
            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                'class' => 'btn btn-info pull-left',
                'value' => 1,
                'name' => (new \ReflectionClass($user))->getShortName() . '[change_password_submit]']) ?>
        </div>
      </div>
  </div>
  <div class="col-md-4 col-xs-12">
      <div class="white-box">
        <h3 class="box-title"><?php echo Yii::t('app', 'Личные данные');?></h3>
          <div class="tab-content user-info">
            <div class="form-group">
                <div class="col-md-12">
                  <?= $form->field($user, 'first_name')->textInput([
                      'maxlength' => true,
                      'placeholder' => $user->getAttributeLabel('first_name')
                  ]) ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                  <?= $form->field($user, 'last_name')->textInput([
                      'maxlength' => true,
                      'placeholder' => $user->getAttributeLabel('last_name')
                  ]) ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="padding-left: 10px;">
                  <label class="control-label" for="profileform-last_name" ><?php echo Yii::t('app', 'Телефон');?></label>
                  <?php echo $form->field($user, 'phone')->widget(PhoneInput::className(), [
                        'jsOptions' => [
                            'preferredCountries' => ['no', 'pl', 'ua'],
                        ]
                    ])->label('');
                  ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                  <?= $form->field($user, 'skype')->textInput([
                      'maxlength' => true,
                      'placeholder' => $user->getAttributeLabel('skype')
                  ]) ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                  <?= $form->field($user, 'birthday')
                      ->widget(DatePicker::classname(), [
                          'options' => ['placeholder' => $user->getAttributeLabel('birthday')],
                          'language' => \Yii::$app->language,
                          'type' => DatePicker::TYPE_COMPONENT_APPEND,
                          'pluginOptions' => [
                              'autoclose'=>true,
                              'format' => 'dd.mm.yyyy'
                          ]
                  ]); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12" style="padding-left: 10px;">
                  <label class="radio-head"><?php echo Yii::t('app', 'Пол');?></label>
                  <?= $form->field($user, 'sex')
                      ->radioList(
                          User::getSexArray(),
                          [
                              'item' => function($index, $label, $name, $checked, $value) {

                                  $checked = $checked == true ? 'checked="checked"' : '';
                                  $return = '<label class="radio-inline">';
                                  $return .= '<input type="radio" name="' . $name . '" value="' . $value . '"  ' . $checked . ' >';
                                  $return .= '<i></i>';
                                  $return .= '<span>' . ucwords($label) . '</span>';
                                  $return .= '</label>';

                                  return $return;
                              }
                          ]
                      )
                  ->label(false);?>
                </div>
            </div>
            <div class="form-group" style = "margin-bottom: 0px;">
              <div class="col-sm-12" style = "padding-left: 5px; margin-bottom: 0px;">
                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                  'class' => 'btn btn-info pull-left',
                  'name' => 'signup-button']) ?>
              </div>
            </div>
          </div>
      </div>
  </div>
  <div class="col-md-4 col-xs-12" >
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/_parent-wrap', ['user' => $user]);?>
    <div class="white-box">
      <h3 class="box-title"><?php echo Yii::t('app', 'Сменить E-mail');?></h3>
      <div class="tab-content">
        <div class="form-group" style = "margin-bottom:0px;">
          <div class="col-md-12">
            <?= $form->field($user, 'email')->textInput([
                'maxlength' => true,
                'readonly' => true,
                'placeholder' => $user->getAttributeLabel('email')
            ]) ?>
          </div>
        </div>
        <div class="form-group" style = "padding-left: 0px; margin-bottom: 0px;">
          <div class="col-md-12">
            <?= $form->field($user, 'email_confirm_token',['options' => [
                'style' => $oldUser->email_confirm_token == '' ? 'display:none;' : 'display:block;']])->textInput([
                'maxlength' => true,
                'value' => '',
                'placeholder' => $user->getAttributeLabel('email_confirm_token')
            ]) ?>
          </div>
        </div>
        <div class="form-group" style = "margin-bottom: 0px;">
          <div class="col-md-lg" style = "padding-left: 5px;">
            <?php if ($oldUser->email_confirm_token == ''):?>
              <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сменить E-mail'), [
                  'class' => 'btn btn-warning pull-left',
                  'value' => 1,
                  'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
            <?php else:?>
              <div class="row">
                <div class = "col-lg-12">
                  <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сменить E-mail'), [
                      'class' => 'btn btn-danger pull-left',
                      'value' => 1,
                      'name' =>  (new \ReflectionClass($user))->getShortName() . '[confirm_token_submit]']) ?>
                </div>
              </div>
              <div class="row" style = "margin-top: 15px;">
                <div class = "col-lg-12">

                <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Отправить повторно код подтверждения'), [
                  'class' => 'btn btn-success pull-left',
                  'value' => 1,
                  'name' =>  (new \ReflectionClass($user))->getShortName() . '[send_mail_confirm_token_submit]']) ?>
                </div>
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php ActiveForm::end();?>


<?php $successMessage = Yii::$app->session->getFlash('success');?>

<?php if (!empty($successMessage)):?>

<div class="modal fade successModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div style = "width: 90%; font-weight: bold;" class="modal-title"><?php echo Yii::t('app', 'Внимание !');?></div>
        </div>
        <div class="modal-body">
        <?php echo $successMessage;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

  <?php $this->registerJs("
      jQuery('.successModal').modal('show');
  ", View::POS_END);?>
<?php endif;?>

<?php $this->registerJs("
  jQuery('#profileform-phone').keyup(function(){
    var value = jQuery(this).val();
   // value = value.replace(/[^-0-9]/,'')
    console.log(value);
   // jQuery(this).val(value);
  })
  ", View::POS_END);?>
