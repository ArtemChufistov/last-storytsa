<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class = "col-md-3">
  <div class="white-box userInfoPartial" login=<?php echo $user->login?>>
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo Yii::t('app', 'Информация об участнике');?> <?php echo $user->login;?></h3>
      <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
        <div class="btn-group" data-toggle="btn-toggle">
          <button class="btn btn-danger btn-sm closeUserInfo" type="button" data-widget="remove">
          <i class="fa fa-times"></i>
          </button>
        </div>
      </div>
    </div>
    <div class="box-body">
      <?php if (!empty($user->first_name)):?>
      <p><?php echo Yii::t('app', 'Имя:');?> <?php echo $user->first_name;?></p>
      <?php endif;?>
      <?php if (!empty($user->last_name)):?>
      <p><?php echo Yii::t('app', 'Фамилия:');?> <?php echo $user->last_name;?></p>
      <?php endif;?>
      <?php if (!empty($user->email)):?>
      <p><?php echo Yii::t('app', 'E-mail:');?> <?php echo $user->email;?></p>
      <?php endif;?>
      <?php if (!empty($user->skype)):?>
      <p><?php echo Yii::t('app', 'Skype:');?> <?php echo $user->skype;?></p>
      <?php endif;?>
      <?php if (!empty($user->phone)):?>
      <p><?php echo Yii::t('app', 'Телефон:');?> <?php echo $user->phone;?></p>
      <?php endif;?>
    </div>
  </div>
</div>