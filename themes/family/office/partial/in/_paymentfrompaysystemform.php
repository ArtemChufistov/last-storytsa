<?php
use app\modules\finance\components\paysystem\FchangeComponent;
use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use lowbase\user\UserAsset;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Финансы - Пополнение личного счёта');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$fchangeCurrencyArray = FchangeComponent::getCurrencyCourseList();
$fchangePaySystem = PaySystem::find()->where(['key' => PaySystem::KEY_FCHANGE])->one();

$sumInUsd = $paymentForm->to_sum * 1;
?>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="example-box-wrapper">
          <div id="form-wizard-3" class="form-wizard">
            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_tabs', [
	'active' => 3,
	'paymentForm' => $paymentForm,
]); ?>
            <div class="tab-content">
              <div class="tab-pane active" id="step-3">
                <div class="content-box">
                    <h3 class="content-box-header bg-yellow">
                      <?php echo Yii::t('app', 'Выбор платёжной системы'); ?>
                    </h3>
                    <div class="content-box-wrapper" style="min-height: 180px;">
                      <div class="form-group">
                        <div class="col-sm-12" style = "text-align: center;">
                          <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => ['form-horizontal bordered-row']]]);?>

                            <?php $paySystemFinded = false;?>
                            <?php foreach ($fchangeCurrencyArray as $fchangeCurrency): ?>
                              <?php if (strtoupper($fchangeCurrency['send_paysys_valute']) == $paymentForm->getFromCurrency()->one()->key): ?>

                                <?php $resultSum = round($sumInUsd * $fchangeCurrency['obmen_curs'], 2);?>

                                <?php $paySystemFinded = true;?>

                                <?php if ($resultSum > $fchangeCurrency['max_summ']): ?>

                                  <span class = "btn btn-lg btn-default paySysWrap">
                                    <img class = "paySysIcon" src = "<?php echo $fchangeCurrency['send_paysys_icon']; ?>">
                                    <p><?php echo $fchangeCurrency['send_paysys_title']; ?></p>
                                    <p>
                                      <strong>
                                        <?php echo Yii::t('app', 'Макс. сумма для покупки:'); ?> <?php echo $fchangeCurrency['max_summ']; ?>
                                        <?php echo strtoupper($fchangeCurrency['send_paysys_valute']); ?>
                                      </strong>
                                    </p>
                                  </span>

                                <?php elseif ($resultSum < $fchangeCurrency['min_summ']): ?>

                                  <span class = "btn btn-lg btn-default paySysWrap" >
                                    <img class = "paySysIcon" src = "<?php echo $fchangeCurrency['send_paysys_icon']; ?>">
                                    <p><?php echo $fchangeCurrency['send_paysys_title']; ?></p>
                                    <p>
                                      <strong>
                                        <?php echo Yii::t('app', 'Мин. сумма для покупки:'); ?> <?php echo $fchangeCurrency['max_summ']; ?>
                                        <?php echo strtoupper($fchangeCurrency['send_paysys_valute']); ?>
                                      </strong>
                                    </p>
                                  </span>

                                <?php else: ?>

                                  <?=Html::submitButton('
                                    <img class = "paySysIcon" src = "' . $fchangeCurrency['send_paysys_icon'] . '">
                                    <p>' . $fchangeCurrency['send_paysys_title'] . '</p>
                                    ', ['value' => $fchangeCurrency['send_paysys_identificator'],
                                	'name' => StringHelper::basename(get_class($paymentForm)) . '[sub_pay_system]',
                                	'class' => 'btn btn-lg btn-default paySysWrap'])?>

                                <?php endif;?>
                              <?php endif;?>
                            <?php endforeach;?>

                            <?php if (!empty($paymentForm->getErrors())):?>
                              <?php foreach($paymentForm->getErrors() as $error):?>
                                <?php echo $error[0];?>
                              <?php endforeach;?>
                            <?php elseif ($paySystemFinded == false): ?>
                              <span class = "btn btn-lg btn-default paySysWrap">
                                <p>
                                  <?php echo Yii::t('app', '<strong>Приносим извинения</strong></br>В данный момент расчёты в этой валюте закрыты.</br><a href = "{link}"> Выберите другую валюту<a>', ['link' => Url::to(['office/in', StringHelper::basename(get_class($paymentForm)) . '[to_sum]' => $paymentForm->to_sum])]); ?>
                                </p>
                              </span>
                            <?php endif;?>

                          <?php ActiveForm::end();?>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  .paySysIcon{
    margin: 10px;
  }
  .paySysWrap{
    display: none;
    margin: 15px;
    height: 120px;
  }
</style>