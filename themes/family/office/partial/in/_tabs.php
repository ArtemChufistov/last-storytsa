<?php
use yii\helpers\Url;
?>

<ul class="wizard-steps" role="tablist">
  <li class = "<?php if (in_array($active, [1])): ?>active current<?php endif;?>" role="tab">
    <h4><span><i class="ti-wallet"></i></span><?php echo Yii::t('app', 'Ввод суммы'); ?></h4>
  </li>
  <li class = "<?php if (in_array($active, [2])): ?>active current<?php endif;?>" role="tab">
    <h4><span><i class="ti-money"></i></span><?php echo Yii::t('app', 'Выбор валюты'); ?></h4>
  </li>
  <li class = "<?php if (in_array($active, [3,])): ?>active current<?php endif;?>" role="tab">
    <h4><span><i class="ti-credit-card"></i></span><?php echo Yii::t('app', 'Выбор платёжной системы'); ?></h4>
  </li>
  <li class = "<?php if ($active == 4): ?>active current<?php endif;?>" role="tab">
    <h4><span><i class="ti-check"></i></span><?php echo Yii::t('app', 'Оплата'); ?></h4>
  </li>
</ul>