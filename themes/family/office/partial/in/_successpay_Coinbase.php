<?php
use yii\helpers\Html;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;

$paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
$currencyCourse = CurrencyCourse::find()
	->where(['from_pay_system_id' => $paymentForm->from_pay_system_id])
	->andWhere(['to_pay_system_id' => $paySystem->id])
	->andWhere(['from_currency' => $paymentForm->from_currency_id])
	->one();

$this->title = Yii::t('user', 'Финансы');

$url = 'https://bitcoinfees.earn.com/api/v1/fees/list';
$currencyCourseList = json_decode(file_get_contents($url), true);


?>
<div class="row" style = "margin-bottom: 30px;">
    <div class="col-lg-12">
    	<a class="btn btn-success waves-effect waves-light m-r-10" href = "/office/finance"><?php echo Yii::t('app', 'Back to finance');?></a>
    </div>
</div>
<div class="row">
    <div class="col-lg-9">

	<div class="panel panel-info" style = "margin-bottom: 0px;">
	    <div class="panel-heading" style = "">
	    	<?php echo Yii::t('app', 'Information about your order');?>
	    	<span style = "float: right;" class = "timeToCloseWrap">
                    <a class="mytooltip" href="javascript:void(0)" style = "color: #FFF;">
                        <?php echo Yii::t('app', 'Time to cancel order');?>
                        <span class="tooltip-content5">
                            <span class="tooltip-text3">
                                <span class="tooltip-inner2"><?php echo Yii::t('app', 'If btc not send, this order has been canceled when time is over');?></span>
                            </span>
                        </span>
                    </a>
	    		: <span class = "timeToClose"></span>
	    	</span>
	    </div>
	</div>
	    <div class="panel-wrapper collapse in" aria-expanded="true">
	        <div class="panel-body" style = "border-left: 1px solid rgba(120, 130, 140, 0.13); border-right: 1px solid rgba(120, 130, 140, 0.13); border-bottom: 1px solid rgba(120, 130, 140, 0.13);">

				<div class="row" style = "margin-top: 18px; margin-bottom: 20px;">
					<div class = "col-md-3" style = "font-size: 18px;">
						<?php echo Yii::t('app', 'Количество USD'); ?>:
					</div>
					<div class = "col-md-6" style = "font-size: 18px;">
						&nbsp;&nbsp;<strong style = "font-size: 18px;"><?php echo $paymentForm->to_sum; ?></strong>
					</div>
					<div class = "col-md-3">
					</div>
				</div>
				<div class = "row" style = "margin-top: 20px; margin-bottom: 20px;">
					<div class = "col-md-3" style = "font-size: 20px;">
						<?php echo Yii::t('app', 'Сумма к оплате'); ?>:
					</div>
					<div class = "col-md-6">
						<?php echo Html::input('text', 'sumbtc2',  number_format($paymentForm->fromSumWithComission(), 6) . ' ' . $paymentForm->getFromCurrency()->one()->key, ['class' => 'form-control sumbtc2' , 'readonly' => true, 'style' => 'font-size:16px;']);?>
					</div>
					<div class = "col-md-3" style="text-align: center;">
					 <?php echo Html::input('button', '', Yii::t('app', 'Скопировать в буфер обмена'), ['style' => 'margin-left:20px', 'class' => 'btn btn-success pull-left subBtcButton', 'data-clipboard-target' => '#sumbtc']);?>
					</div>
				</div>
				<div class = "row" style = "margin-top: 20px; margin-bottom: 20px;">
					<div class = "col-md-3" style = "font-size: 18px;">
					<?php echo Yii::t('app', 'Кошелёк для оплаты'); ?>:
					</div>
					<div class = "col-md-6">
						<?php echo Html::input('text', 'walletbtc',  $paymentForm->wallet, ['id' => 'walletbtc', 'class' => 'form-control' , 'readonly' => true, 'style' => 'font-size:16px ;']);?>
					</div>
					<div class = "col-md-3" style="text-align: center;">
						<?php echo Html::input('button', '', Yii::t('app', 'Скопировать в буфер обмена'), ['style' => 'margin-left:20px', 'class' => ' btn btn-success pull-left walletBtcButton', 'data-clipboard-target' => '#walletbtc']);?>
					</div>
				</div>
				<input style = "opacity: 0; height: 0px;" name="sumbtc1" id = "sumbtc" value = "<?php echo number_format($paymentForm->fromSumWithComission(), 6);?>">
			</div>
		</div>
    </div>
    <div class="col-md-3" style = "text-align: center; font-size: 16px;">
		<?php if ($paymentForm->getFromCurrency()->one()->key == Currency::KEY_BTC): ?>
			<?php echo Yii::t('app', 'Для удобства перевода вы можете воспользоваться QR кодом:'); ?><br/>
			<img style = "width: 200px;" src = "/office/qrcode/<?php echo $paymentForm->wallet; ?>/<?php echo $paymentForm->fromSumWithComission(); ?>">
		<?php endif;?>

	</div>
</div>

<div class="row" style = "margin-top: 40px;">
 <div class="col-md-8">

        <h3 class="box-title">
        	<span style = "display: inline-block; width: 40px; height: 40px;padding-top: 5px;" class="circle circle-md bg-warning">
            	<i style = "" class="fa fa-exclamation"></i>
            </span>
        	&nbsp;&nbsp; <?php echo Yii::t('app', 'Important Information, how to make BTC transactions faster'); ?>
        </h3>
        <div class="table-responsive" >
            <table class="table" cellspacing="5" style="width: 97%; margin-left: 1px;">
                <thead>
                    <tr>
                        <th><?php echo Yii::t('app', 'Time from'); ?></th>
                        <th><?php echo Yii::t('app', 'Time to'); ?></th>
                        <th><?php echo Yii::t('app', 'In BTC'); ?></th>
                        <th><?php echo Yii::t('app', 'In USD'); ?></th>
                        <th><?php echo Yii::t('app', 'IN Satoshi'); ?></th>
                    </tr>
                </thead>
                <tbody>
                	<?php $doubles = []; $range = [[0, 180], [15, 480], [40, 600]];?>
                	<?php foreach(array_reverse($currencyCourseList['fees']) as $currencyCourse):?>
                		<?php $arrTitle = $currencyCourse['minMinutes'] . '_' . $currencyCourse['maxMinutes'];?>
                		<?php if (empty($doubles[$arrTitle])): ?>
                			<?php if (!empty($range) && ($currencyCourse['minMinutes'] >= $range[0][0] && $currencyCourse['maxMinutes'] <= $range[0][1])):?>
                				<?php unset($range[0]); sort($range);;?>
	                			<?php $doubles[$arrTitle] = $arrTitle;?>
			                    <tr class="advance-table-row active">
			                        <td><?php echo $currencyCourse['minMinutes'];?> min</td>
			                        <td><?php echo $currencyCourse['maxMinutes'];?> min</td>
			                        <td><?php echo number_format($currencyCourse['minFee'] / 1000000, 6, ',', '.');?></td>
			                        <td><?php echo number_format(($currencyCourse['minFee'] / $paymentForm->course_to_usd) * 226, 2, ',', '.');?></td>
			                        <td><?php echo $currencyCourse['minFee'];?></td>
			                    </tr>
			                    <tr>
			                        <td colspan="5" class="sm-pd"></td>
			                    </tr>
		                	<?php endif;?>
		                <?php endif;?>
	                <?php endforeach;?>
                </tbody>
            </table>

     </div>
 </div>
 <div class="col-md-4">
        <h3 class="box-title">
        	<span style = "display: inline-block; width: 40px; height: 40px;padding-top: 5px;" class="circle circle-md bg-warning">
            	<i style = "" class="fa fa-exclamation"></i>
            </span>
        	&nbsp;&nbsp; <?php echo Yii::t('app', 'Attention'); ?>
        </h3>
        <br/>
        <?php echo Yii::t('app', 'If you want to make fast BTC transaction, yout will need change transaction fee via table:'); ?>
 </div>
</div>

<?php $this->registerJs('
new Clipboard(".walletBtcButton").on("success", function(e) {
   $.toast({
    heading: "' . Yii::t('app', 'Coping to buffer success') . '",
    text: "' . Yii::t('app', 'Information is in buffer') . '",
    position: "top-right",
    loaderBg:"#ff6849",
    icon: "info",
    hideAfter: 3000, 
    stack: 6
  });
});

new Clipboard(".subBtcButton").on("success", function(e) {
	$(".sumbtc2").select();
   $.toast({
    heading: "' . Yii::t('app', 'Coping to buffer success') . '",
    text: "' . Yii::t('app', 'Information is in buffer') . '",
    position: "top-right",
    loaderBg:"#ff6849",
    icon: "info",
    hideAfter: 3000, 
    stack: 6
  });
});
');?>

<?php $this->registerJs('
  $(function() {

    function timer(){
      $.get("/office/toclosepaymentdate/' . $paymentForm->hash . '", function(data){

        if (data == ""){
          $(".timeToCloseWrap").hide();
        }else{
          $(".timeToCloseWrap").show();
          $(".timeToClose").html(data);
        }
      });

      setTimeout(timer, 1000);
    }

    timer()

  });
');?>
