<?php

use app\modules\matrix\components\StorytsaMarketing;
use app\modules\profile\widgets\UserTreeElementWidget;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>

<div id = "animationSandbox">
    <div class="content-box">
      <h3 class="content-box-header clearfix"><?php echo Yii::t('app', 'Как это работает');?>
        <div class="font-size-11 float-right">
          <a href="#" title=""><i class="glyph-icon opacity-hover icon-star-o	"></i></a>
        </div>
      </h3>
      <div class="content-box-wrapper text-center clearfix">
        <p style = "text-align: justify;">
        	<?php echo Yii::t('app', '<img src = "/images/gallery-grid-condensed-2-640x420.jpg" style = "float: right; margin-left: 20px; width: 300px;">Современные технологии достигли такого уровня, что сложившаяся структура организации способствует повышению качества новых предложений. Внезапно, акционеры крупнейших компаний и по сей день остаются уделом либералов, которые жаждут быть подвергнуты целой серии независимых исследований. Кстати, сторонники тоталитаризма в науке являются только методом политического участия и объективно рассмотрены соответствующими инстанциями.<br/><br/>
        		Генерация рыбатекста происходит довольно просто: есть несколько фиксированных наборов фраз и словочетаний, из которых в определенном порядке формируются предложения. Предложения складываются в абзацы – и вы наслаждетесь очередным бредошедевром.

Сама идея работы генератора заимствована у псевдосоветского "универсального кода речей", из которого мы выдернули используемые в нем словосочетания, запилили приличное количество собственных, в несколько раз усложнили алгоритм, добавив новые схемы сборки, – и оформили в виде быстрого и удобного сервиса для получения тестового контента.<br/><br/>
Другое название – "универсальный генератор речей". По легенде, всякие депутаты и руководители в СССР использовали в своих выступлениях заготовленный набор совмещающихся между собой словосочетаний, что позволяло нести псевдоумную ахинею часами. Что-то вроде дорвеев для политсобраний
        	');?>
        </p>

        <?php $form = ActiveForm::begin(); ?>
        <br/>
			<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
			  'class' => 'butt js--triggerAnimation btn btn-success btn-lg mrg20B',
			  'value' => 1,
			  'name' => 'marketing-matrix']) ?>
		<?php ActiveForm::end();?>
      </div>
    </div>
</div>


