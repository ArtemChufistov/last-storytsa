<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>

<div class="white-box">
  <div class="tab-content">
    <h3 class="box-title"><?php echo Yii::t('app', 'Тикет №{id}', ['id' => $currentTicket->id]);?></h3>
    <div class="tab-content">
      <div class="form-group">
        <label class="control-label" ><?php echo Yii::t('app', 'Отдел');?></label>
        <pre class="form-control" ><?php echo Yii::t('app', $currentTicket->getDepartment()->one()->name);?></pre>
      </div>
      <div class="form-group">
        <label class="control-label" ><?php echo Yii::t('app', 'Текст');?></label>
        <pre class="form-control" style = "height: 80px;"><?php echo $currentTicket->text;?></pre>
      </div>
    </div>
  </div>
</div>

<div class="white-box">
  <div class="tab-content">
    <h3 class="box-title">
      <?php echo Yii::t('app', 'Чат с');?>
      <?php if ($currentTicket->getDepartment()->one()->id == 1):?>
        <?php echo Yii::t('app', 'поддержкой');?>
      <?php elseif($currentTicket->getDepartment()->one()->id == 2):?>
        <?php echo Yii::t('app', 'администрацией');?>
      <?php endif;?>
    </h3>
    <div class="tab-content">

      <div class="panel-wrapper collapse in" aria-expanded="true">
          <div class="panel-body">
              <div class="chat-box" style="height: 510px;border: 1px solid beige; overflow-y: scroll;">
                  <ul class="chat-list slimscroll" tabindex="5005" >

                      <?php foreach(array_reverse($ticketMessages) as $ticketMessageItem): ?>
                        <li <?php if ($ticketMessageItem->getUser()->one()->id != $user->id):?>class="odd"<?php endif;?>>
                            <div class="chat-image"> <img alt="male" src="<?php echo $ticketMessageItem->getUser()->one()->getImage();?>"> </div>
                            <div class="chat-body">
                                <div class="chat-text">
                                    <h4><?php echo $ticketMessageItem->getUser()->one()->login;?></h4>
                                    <p> <?php echo $ticketMessageItem->text;?> </p> <b><?php echo date('H:i d-m-Y', strtotime($ticketMessageItem->dateAdd));?></b> </div>
                            </div>
                        </li>
                      <?php endforeach;?>
                  </ul>
              </div>

          <?php $form = ActiveForm::begin([
            'options' => [
                'class'=>'input-group support-message',
            ],
          ]); ?>

              <?= $form->field($ticketMessage, 'text')->textInput([
                  'maxlength' => true,
                  'value' => '',
                  'placeholder' => $ticketMessage->getAttributeLabel('text')
              ]) ?>

            <div class="input-group-btn" style = "padding-top:25px;">
                <?= Html::submitButton('<i class="fa fa-plus"></i>', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end();?>

          </div>

      </div>
      
    </div>
  </div>
</div>
<script type="text/javascript">

$( document ).ready(function() {
  $(".chat-list").animate({ scrollTop: $(".chat-list")[0].scrollHeight}, 1000);
});
</script>
