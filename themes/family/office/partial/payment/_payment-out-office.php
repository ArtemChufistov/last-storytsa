<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$payWallet = $user->getPayWallet($paymentForm->pay_system_id, $paymentForm->currency_id);
?>
<?php if ($payWallet->wallet == ''):?>
	<strong><?php echo Yii::t('app', 'Для вывода средств с баланса Вам необходимо указать свой <a href = "{url}">кошелек</a>', ['url' => Url::to('/profile/office/walletdetail')]);?></strong>
<?php else:?>
	<?php if ($paymentForm->isNewRecord):?>
	    <?php $form = ActiveForm::begin(); ?>

	    	<p><?php echo Yii::t('app', 'Укажите сумму, которую хотите Вы хотите снять со своего личного счёта в кабинете');?></p>

	    	<?= $form->field($paymentForm, 'currency_id')->textInput(['readonly' => true, 'value' => $paymentForm->getCurrency()->one()->title]) ?>

	        <?= $form->field($paymentForm, 'wallet')->textInput(['readonly' => true, 'value' => $payWallet->wallet])->label(Yii::t('app', 'Ваш кошелек')) ?>

	        <?= $form->field($paymentForm, 'sum') ?>
	    
	        <div class="form-group">
	            <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-success']) ?>
	        </div>
	    <?php ActiveForm::end(); ?>
	<?php else:?>
		<?php echo Yii::t('app', 'Указаная сумма: <strong>{sum} {currency}</strong> будет списана с вашего личного счёта и отправлена на кошелек <strong>{wallet}</strong>',[
			'sum' => $paymentForm->sum,
			'wallet' => $paymentForm->wallet,
			'currency' => $paymentForm->getCurrency()->one()->title,
		])?>

		</br>
		</br>
		<p class="margin"><?php echo Yii::t('app', 'Кошелек BitCoin');?></p>
	  	<div class="input-group">
			<div class="input-group-btn">
				<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-wallet"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
			</div>
			<input class="form-control" readonly="true" type="text" id = "payment-wallet" value = "<?php echo $paymentForm->wallet;?>">
		</div>
		</br>
		<p class="margin"><?php echo Yii::t('app', 'Сумма BTC');?></p>
	  	<div class="input-group">
			<div class="input-group-btn">
				<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-sum"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
			</div>
			<input class="form-control" readonly="true" type="text" id = "payment-sum" value = "<?php echo $paymentForm->sum;?>">
		</div>

	<?php endif;?>
<?php endif;?>