<?php use app\modules\payment\models\UserPayWallet; ?>

<p><strong><?php echo Yii::t('app', 'Платёж от:');?></strong> <?php echo date('h-i d-m-Y', strtotime($payment->date_add));?></p>
<p><strong><?php echo Yii::t('app', 'Кошелёк BitCoin:');?></strong> <?php echo $payment->wallet;?></p>
<p><strong><?php echo Yii::t('app', 'Переводимая сумма BTC:');?></strong> <?php echo $payment->from_sum;?></p>
<p><strong><?php echo Yii::t('app', 'Начисляемая сумма USD:');?></strong> <?php echo $payment->to_sum;?></p>
<p><strong><?php echo Yii::t('app', 'Статус:');?></strong> <?php echo $payment->getStatusTitle();?></p>

<?php if (!empty($coinbaseTransaction)):?>
	<?php echo Yii::t('app', 'Хэш транзакции: '); ?> <?php echo $coinbaseTransaction->network_hash;?></br>
	<a target = "_blank" href = "<?php echo UserPayWallet::BLOCKHAIN_SHOW_URL . $coinbaseTransaction->network_hash; ?>"><?php echo Yii::t('app', 'Посмотреть на blokchain.info'); ?></a>
<?php endif;?>