<?php
use app\modules\profile\models\User;
use app\modules\invest\models\UserInvest;


$user = User::find()->where(['login' => $child['login']])->one();

$userInfo = $user->initUserInfo();
?>
<tr <?php if ($level > 0 ):?>style="display:none;"<?php endif;?> parentLogin = "<?php echo $parent['login'];?>" currentLogin = "<?php echo $child['login'];?>">
    <td <?php echo padding15($level);?>  >
        <table><tr><td>
         <div class="circle-md pull-left circle bg-info" style = "width: 25px; padding-top: 4px; height: 25px; font-size: 14px !important;"><?php echo $level+1;?></div>
        </td>
        <?php if (!empty($child['nodes'])):?>
            <td><div class="circle-md pull-right circle bg-success treeItemICon" style = "margin-left: 2px;width: 25px; padding-top: 4px; height: 25px; font-size: 14px !important;"><i class="ti-plus"></i></div></td>
        <?php endif;?>
        </tr>
        </table>
    </td>
    <td <?php echo padding15($level);?>>
        <img src="<?php echo $user->getImage();?>" alt="user-img" class="img-circle" width="36" style = "float: left; margin-right: 5px;">
        <?php echo $user->login;?>
        <br/>
        <span class="text-muted">
            <?php echo empty($user->first_name) ? Yii::t('app', 'Имя') : $user->first_name;?> 
            <?php echo empty($user->last_name) ? Yii::t('app', 'Фамилия'): $user->last_name;?>
        </span>
    </td>
    <td>
    	<strong><?php echo Yii::t('app', 'E-mail')?>:</strong> <?php echo $user->email;?><br/>
        <br/>
        <?php if (!empty($user->skype)): ?>
        	<span class="text-muted"><strong><?php echo Yii::t('app', 'Skype')?></strong> : <?php echo $user->skype;?></span>
        <?php endif;?>
        <?php if (!empty($user->phone)): ?>
        	<span class="text-muted"><strong><?php echo Yii::t('app', 'Телефон')?></strong> : <?php echo $user->phone;?></span>
        <?php endif;?>
    </td>
    <td  class="text-center">
        <span class="text-muted"> <?php echo empty($userInfo->self_invest) ? 0 : number_format($userInfo->self_invest, 2);?> USD</span>
    </td>
    <td  class="text-center">
        <span class="text-muted"> <?php echo empty($userInfo->struct_invest) ? 0 : number_format($userInfo->struct_invest, 2);?> USD</span>
    </td>
</tr>
