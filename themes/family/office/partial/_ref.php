<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;

$matrix = 1;
?>

<div class="white-box clearfix">
  <h3 class="box-title"><?php echo Yii::t('app', 'Ваша реферальная ссылка');?></h3>

  <?php if (!empty($matrix)):?>
    <div class="form-group">
        <?php echo Html::input('text', 'refLink', Url::home(true) . '?ref=' . $user->login, ['id' => 'refLink', 'class' => 'form-control' , 'readonly' => true]);?>
    </div>
    <div class="form-group">
        <?php echo Html::submitButton('<i class="glyphicon glyphicon-share-alt"></i> ' . Yii::t('app', 'Скопировать в буфер обмена'), ['class' => 'btn-clipboard-ref btn btn-success pull-left', 'data-clipboard-target' => '#refLink']);?>
    </div>
  <?php else:?>
    <?php echo Yii::t('app', 'Реферальная ссылка будет доступна только после</br> <a style = "font-weight: bold; text-decoration:underline;" href = "/profile/office/program/1">принятия участия в программе.</a>')?>
  <?php endif;?>
</div>

<?php $this->registerJs('
new Clipboard(".btn-clipboard-ref").on("success", function(e) {
   $.toast({
    heading: "' . Yii::t('app', 'Ссылка скопирована в буфер обмена') . '",
    text: "' . Yii::t('app', 'Information is in buffer') . '",
    position: "top-right",
    loaderBg:"#ff6849",
    icon: "info",
    hideAfter: 3000, 
    stack: 6
  });
});

');?>