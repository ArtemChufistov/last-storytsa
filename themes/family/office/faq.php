<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Часто задавемые вопросы');
$this->params['breadcrumbs'][] = $this->title;

?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => $this->title]
    ]
]);?>

<div class="row">
  <div class="col-md-12">

      <div class="panel-group" id="exampleAccordionDefault" aria-multiselectable="true" role="tablist">
          <div class="panel">
              <div class="panel-heading" id="exampleHeadingDefaultOne" role="tab"> <a class="panel-title" data-toggle="collapse" href="#exampleCollapseDefaultOne" data-parent="#exampleAccordionDefault" aria-expanded="true" aria-controls="exampleCollapseDefaultOne"> <?php echo Yii::t('app', 'Заголовок вопроса 1');?> </a> </div>
              <div class="panel-collapse collapse in" id="exampleCollapseDefaultOne" aria-labelledby="exampleHeadingDefaultOne" role="tabpanel">
                  <div class="panel-body">
                    <?php echo Yii::t('app', 'Полное описание вопроса 1,Полное описание вопроса 1,Полное описание вопроса 1,Полное описание вопроса 1');?>
                  </div>
              </div>
          </div>
          <div class="panel">
              <div class="panel-heading" id="exampleHeadingDefaultTwo" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultTwo" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultTwo"> <?php echo Yii::t('app', 'Заголовок вопроса 2');?> </a> </div>
              <div class="panel-collapse collapse" id="exampleCollapseDefaultTwo" aria-labelledby="exampleHeadingDefaultTwo" role="tabpanel">
                  <div class="panel-body">
                    <?php echo Yii::t('app', 'Полное описание вопроса 2,Полное описание вопроса 2,Полное описание вопроса 2,Полное описание вопроса 2');?>
                  </div>
              </div>
          </div>
          <div class="panel">
              <div class="panel-heading" id="exampleHeadingDefaultThree" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultThree" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultThree"> <?php echo Yii::t('app', 'Заголовок вопроса 3');?> </a> </div>
              <div class="panel-collapse collapse" id="exampleCollapseDefaultThree" aria-labelledby="exampleHeadingDefaultThree" role="tabpanel">
                  <div class="panel-body">
                    <?php echo Yii::t('app', 'Полное описание вопроса 3,Полное описание вопроса 3,Полное описание вопроса 3,Полное описание вопроса 3');?>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>