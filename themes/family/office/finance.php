<?php

use app\modules\mainpage\models\Preference;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Payment;
use app\modules\finance\models\Currency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = Yii::t('user', 'Финансы');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$currencyFreez24 = Currency::find()->where(['key' => Currency::KEY_FREEZ24])->one();
$currencyFreez   = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();
$currencyDepozit = Currency::find()->where(['key' => Currency::KEY_DEPOZIT])->one();
$balanceFreez24  = $user->getBalances([$currencyFreez24->id])[$currencyFreez24->id];
$balanceFreez    = $user->getBalances([$currencyFreez->id])[$currencyFreez->id];
$balanceDepozit  = $user->getBalances([$currencyDepozit->id])[$currencyDepozit->id];

$pendingPaymentTime = Preference::find()->where(['key' => Preference::KEY_PENDING_PAYMENT_TIME])->one();
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/user'), 'title' => $this->title]
    ]
]);?>

<style>
.counter{
  margin-top: 0px;
  margin-bottom: 0px;
}
.col-in li.col-middle{
  width: 100%;
}
</style>
<div class="white-box">
  <div class="row row-in">
    <div class="col-lg-3 col-sm-6 row-in-br">
      <table style = "width: 100%;">
        <tr>
          <td rowspan = "2">
            <ul class="col-in">
              <li>
                  <span class="circle circle-md bg-danger"><i class="ti-wallet"></i></span>
              </li>
            </ul>
          </td>
          <td>
            <ul class="col-in">
              <li class="col-middle">
                  <h4>
                    <a class="mytooltip" href="javascript:void(0)" style = "color: #313131;">
                        <?php echo Yii::t('app', 'Ваш баланс');?>
                        <span class="tooltip-content5">
                            <span class="tooltip-text3">
                                <span class="tooltip-inner2"><?php echo Yii::t('app', 'Ваш основной баланс, доступный к выводу или созданию дополнительных инвестиций');?></span>
                            </span>
                        </span>
                    </a>
                  </h4>
                  <div class="progress">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> 
                    </div>
                  </div>
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            <ul class="col-in">
              <li class="col-last" style = "float:left;">
                <h3 class="counter text-left" style = "margin-bottom: 10px;"><?php foreach($user->getBalances() as $balance):?><?php echo number_format($balance->value, 2);?><?php endforeach;?> 
                <span style = "font-size: 14px;text-align: right;"><?php echo $balance->getCurrency()->one()->key;?></span></h3> 
              </li>
            </ul>
          </td>
        </tr>
      </table>
    </div>
    <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
      <table style = "width: 100%;">
        <tr>
          <td rowspan = "2">
            <ul class="col-in">
              <li>
                  <span class="circle circle-md bg-info"><i class="ti-wallet"></i></span>
              </li>
            </ul>
          </td>
          <td>
            <ul class="col-in">
              <li class="col-middle">
                <h4>
                  <a class="mytooltip" href="javascript:void(0)" style = "color: #313131;">
                    <?php echo Yii::t('app', 'Реферальные за личников');?>
                    <span class="tooltip-content5">
                        <span class="tooltip-text3">
                            <span class="tooltip-inner2"><?php echo Yii::t('app', 'Будут переведены на основной баланс в течении 24 часов');?></span>
                        </span>
                    </span>
                  </a>
                </h4>
                <div class="progress">
                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> 
                  </div>
                </div>
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            <ul class="col-in">
              <li class="col-last" style = "float:left;">
                <h3 class="counter text-right" style = "margin-bottom: 10px;"><?php echo number_format($balanceFreez24->value, 2);?>
                <span style = "font-size: 14px;text-align: right;" style = "text-align: right;">USD</span></h3>
              </li>
            </ul>
          </td>
        </tr>
      </table>
    </div>
    <div class="col-lg-3 col-sm-6 row-in-br">
      <table style = "width: 100%;">
        <tr>
          <td rowspan = "2">
            <ul class="col-in">
              <li>
                  <span class="circle circle-md bg-success"><i class="fa fa-dollar"></i></span>
              </li>
            </ul>
          </td>
          <td>
            <ul class="col-in">
              <li class="col-middle">
                <h4>
                  <a class="mytooltip" href="javascript:void(0)" style = "color: #313131;">
                  <?php echo Yii::t('app', 'Накопленные проценты');?>
                  <span class="tooltip-content5">
                      <span class="tooltip-text3">
                          <span class="tooltip-inner2"><?php echo Yii::t('app', 'Здесь накапливаются проценты по вашим вкладам и переводятся на баланс в течении 10 дней');?></span>
                      </span>
                  </span>
                  </a>
                </h4>
                <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> 
                  </div>
                </div>
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            <ul class="col-in">
              <li class="col-last" style = "float:left;">
                <h3 class="counter text-right" style = "margin-bottom: 10px;"><?php echo number_format($balanceFreez->value, 2);?>
                <span style = "font-size: 14px;text-align: right;">USD</span></h3>
              </li>
            </ul>
          </td>
        </tr>
      </table>
    </div>
    <div class="col-lg-3 col-sm-6  b-0">
      <table style = "width: 100%;">
        <tr>
          <td rowspan = "2">
            <ul class="col-in">
              <li>
                  <span class="circle circle-md bg-warning"><i class="fa fa-dollar"></i></span>
              </li>
            </ul>
          </td>
          <td>
            <ul class="col-in">
              <li class="col-middle">
                <h4>
                  <a class="mytooltip" href="javascript:void(0)" style = "color: #313131;">
                    <?php echo Yii::t('app', 'Реферальные со структуры');?>
                    <span class="tooltip-content5">
                        <span class="tooltip-text3">
                            <span class="tooltip-inner2"><?php echo Yii::t('app', 'Здесь накапливаются ваши реферальные отчисления со структуры и через 7 дней на них них офрмляется депозит под 1.5% дневных на 3 месяца');?></span>
                        </span>
                    </span>
                  </a>
                </h4>
                <div class="progress">
                  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> 
                  </div>
                </div>
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            <ul class="col-in">
              <li class="col-last" style = "float:left;">
                <h3 class="counter text-right" style = "margin-bottom: 10px;"><?php echo number_format($balanceDepozit->value, 2);?>
                <span style = "font-size: 14px;text-align: right;">USD</span></h3>
              </li>
            </ul>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>

<div class="row content">
  <div class="col-md-6 col-xs-12">
    <div class="panel">
      <div class="panel-heading"><?php echo Yii::t('app', 'Операции со счётом');?></div>
      <div class="tab-content panel-heading" style = "margin-top: 0px;">
        <div class ="row" style = "padding-top: 0px;"><div class="col-md-12 col-xs-12" style = "font-weight: normal; font-size: 14px;">
          <p><?php echo Yii::t('app', 'Для <strong>Пополнения/Снятия</strong> средств личного кабинета выберите соответствующее действие');?></p>
          <p> 
            <span style = "display: inline-block; width: 40px; height: 40px;padding-top: 2px;" class="circle circle-md bg-warning"><i style = "" class="fa fa-exclamation"></i></span>
            &nbsp;&nbsp;
            <a style = "font-size: 18px;" href = "/howtomaktbtcfaster" target = "_blank"><?php echo Yii::t('app', 'Learn how to make BTC transaction faster')?></a>
          </p><br/><br/></div></div>
        <div class="content-box-wrapper text-center clearfix">
          <div class="row">
            <div class="col-md-4 col-xs-4">
              <a href= "<?php echo Url::to(['/profile/office/in']);?>" class="btn btn-success pull-left" >
                <i class="fa fa-level-down"></i> <?php echo Yii::t('app', 'Пополнить личный счёт');?>
              </a>
            </div>
            <div class="col-md-4 col-xs-4">
            </div>
            <div class="col-md-4 col-xs-4">
              <a href= "<?php echo Url::to(['/profile/office/out']);?>" class="btn btn-success pull-right" >
                <i class="fa fa-level-up"></i> <?php echo Yii::t('app', 'Снять с личного счёта');?>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $paymentProvider,
        'rowOptions'=>function($model){
            return ['paymentHash' => $model->hash, 'class' => 'showPaymentInfo'];
        },
        'filterModel' => $payment,
        'id' => 'payment-grid',
        'layout' => '
          <div class="panel">
            <div class="panel-heading">' . Yii::t('app', 'Ваши ордера на пополнения/снятия') . '</div>
            <div class="table-responsive">
              {items}
              <div style = "text-align:center;">{pager}</div>
            </div>
          </div>
          ',
        'columns' => [
            [
              'attribute' => 'type',
              'format' => 'raw',
              'filter' => Select2::widget([
                  'name' => 'PaymentFinanceSearch[type]',
                  'data' => Payment::getFinanceTypeArray(),
                  'theme' => Select2::THEME_BOOTSTRAP,
                  'hideSearch' => true,
                  'options' => [
                      'placeholder' => Yii::t('app', 'Выберите тип'),
                      'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                  ]
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-type', ['model' => $model]);},
            ],[
              'attribute' => 'realSum',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payments-sum', ['model' => $model]);},
            ],[
              'attribute' => 'status',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-status', ['model' => $model]);},
              'filter' => Select2::widget([
                  'name' => 'PaymentFinanceSearch[status]',
                  'data' => Payment::getStatusArray(),
                  'theme' => Select2::THEME_BOOTSTRAP,
                  'hideSearch' => true,
                  'options' => [
                      'placeholder' => Yii::t('app', 'Выберите статус'),
                      'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null
                  ]
              ]),
            ],[
              'attribute' => 'date_add',
              'format' => 'raw',
              'filter' => true,
              'filter' => \yii\jui\DatePicker::widget([
                  'model'=>$payment,
                  'attribute'=>'date_add',
                  'options' => ['class' => 'form-control'],
                  'language' => \Yii::$app->language,
                  'dateFormat' => 'dd-MM-yyyy',
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_payment-date-add', ['model' => $model]);},
            ]
        ],
    ]); ?>
  <?php Pjax::end(); ?>

  </div>

  <div class="col-md-6 col-xs-12">

    <div class="white-box">
      <h3 class="box-title"><?php echo Yii::t('app', 'Перевод участнику');?></h3>
      <div class = "tab-content ">
        <?php $form = ActiveForm::begin(); ?>

        <div class ="form-group">
          <?= $form->field($usertransactionForm, 'to_login')->textInput(); ?>
        </div>

        <div class ="form-group">
          <?= $form->field($usertransactionForm, 'sum')->textInput(); ?>
        </div>

        <div class ="form-group">
        <?php echo $form->field($usertransactionForm, 'currency_id')->widget(Select2::classname(), [
            'data' =>  ArrayHelper::map($userCurrencyArray, 'id', 'title'),
            'options' => ['placeholder' => Yii::t('app', 'Выберите валюту...')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
      </div>
    </div>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $transactionProvider,
        'filterModel' => $transaction,
        'id' => 'transaction-grid',
        'layout' => '
          <div class="panel">
            <div class="panel-heading">' . Yii::t('app', 'Движения по счёту') . '</div>
            <div class="table-responsive">
              {items}
              <div style = "text-align:center;">{pager}</div>
            </div>
          </div>',
        'columns' => [
            [
              'attribute' => 'type',
              'format' => 'raw',
              'filter' => Select2::widget([
                  'name' => 'TransactionFinanceSearch[type]',
                  'data' => Transaction::getOfficeTypeArray(),
                  'theme' => Select2::THEME_BOOTSTRAP,
                  'hideSearch' => true,
                  'options' => [
                      'placeholder' => Yii::t('app', 'Выберите тип'),
                      'value' => isset($_GET['TransactionFinanceSearch[type]']) ? $_GET['TransactionFinanceSearch[type]'] : null
                  ]
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-type', ['model' => $model]);},
            ],[
              'attribute' => 'sum',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-sum', ['model' => $model]);},
            ],[
              'attribute' => 'date_add',
              'format' => 'raw',
              'filter' => true,
              'filter' => \yii\jui\DatePicker::widget([
                  'model'=>$transaction,
                  'attribute'=>'date_add',
                  'options' => ['class' => 'form-control'],
                  'language' => \Yii::$app->language,
                  'dateFormat' => 'dd-MM-yyyy',
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-date-add', ['model' => $model]);},
            ]
        ],
    ]); ?>
  <?php Pjax::end(); ?>

  </div>
</div>

<style>
#payment-grid tbody tr{
  cursor: pointer;
}
</style>


<div class="modal fade paymentInfoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo Yii::t('app', 'Информация о платеже');?></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <span class = "timeToCloseWrap" style = "margin-left: 10px;"><strong class = "timeToClose"></strong> <?php echo Yii::t('app', 'До отмены заявки осталось: ');?></span>
        <button type="button" class="btn btn-info" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

<?php $this->registerJs('
  $(function() {
    var csrfToken = "' . Yii::$app->request->getCsrfToken() . '";
    $(".content").on("click", ".showPaymentInfo", function(){
      paymentHash = $(this).attr("paymentHash");

      $(location).attr("href", "/office/showpayment/" + paymentHash);
      return;
      $.get("/office/showpaymentinfo/" + paymentHash, function(data){
        timer(paymentHash);
        $(".paymentInfoModal").find(".modal-body").html(data);
        $(".paymentInfoModal").modal("show");
      });
    })

    function timer(){
      $.get("/office/toclosepaymentdate/" + paymentHash, function(data){

        if (data == ""){
          $(".timeToCloseWrap").hide();
        }else{
          $(".timeToCloseWrap").show();
          $(".timeToClose").html(data);
        }
      });

      setTimeout(timer, 1000);
    }

  });
');?>

<?php $successMessage = Yii::$app->session->getFlash('innerTransactionSuccess');?>

<?php if (!empty($successMessage)):?>

<div class="modal fade successModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div style = "width: 90%; font-weight: bold;" class="modal-title"><?php echo Yii::t('app', 'Внимание !');?></div>
        </div>
        <div class="modal-body">
        <?php echo $successMessage;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

  <?php $this->registerJs("
      jQuery('.successModal').modal('show');
  ", View::POS_END);?>
<?php endif;?>


<style>
.table-striped tr:hover{
  background-color: #f1f2f7 !important;
}
</style>