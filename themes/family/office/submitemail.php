<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::t('user', 'E-mail подтверждён');
?>

<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <form class="form-horizontal form-material" id="loginform" action="" style = "text-align: center;">
        <?php echo Yii::t('app', 'Поздравляем вы успешно подтвердили свой <br/><strong>E-mail</strong><br/>');?>
        <p><?php echo Yii::t('app', 'Теперь Вам доступен полный функционал личного кабинета');?></p>
        </br>
        <a href = "<?php echo Url::to(['/profile/office/dashboard']);?>" style = "font-weight:bold; text-decoration: underline;">
          <?php echo Yii::t('app', 'Перейти в личный кабинет');?>
        </a>
      </form>
    </div>
  </div>
</section>