<?php

use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use yii\grid\GridView;
use moonland\tinymce\TinyMCE;

$this->title = Yii::t('user', 'Оставить отзыв');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/user'), 'title' => $this->title]
    ]
]);?>


<div class="row">
  <div class="col-md-6 col-xs-6">
    <div class="white-box">
      <div class="tab-content">
        <h3 class="box-title"><?php echo Yii::t('app', 'Задайте свой вопрос и мы на него ответим');?></h3>
        <div class="content-box-wrapper text-center clearfix">

          <?php $form = ActiveForm::begin([
          'id' => 'form-profile',
          'options' => [
              'class'=>'form',
              'enctype'=>'multipart/form-data'
          ],
          ]); ?>

          <?= $form->field($response, 'name')->textInput([
              'maxlength' => true,
              'placeholder' => $response->getAttributeLabel('name')
          ]) ?>

          <?= $form->field($response, 'text')->textArea(['rows' => '9', 'class' => 'textarea form-control']); ?>

          <div class="row">
            <div class="col-sm-6">
              <?= $form->field($response, 'photo1', ['options' => [
                'class' => 'btn btn-default btn-file'], 'template' => '<i class="fa fa-paperclip"></i> ' . Yii::t('app', 'Изображение № 1'). '{input}'])->fileInput() ?>
              <p class="help-block"><?php echo Yii::t('app', 'Максимум');?> 2MB</p>

              <?= $form->field($response, 'photo2', ['options' => [
                'class' => 'btn btn-default btn-file'], 'template' => '<i class="fa fa-paperclip"></i> ' . Yii::t('app', 'Изображение № 2'). '{input}'])->fileInput() ?>
              <p class="help-block"><?php echo Yii::t('app', 'Максимум');?> 2MB</p>

              <?= $form->field($response, 'photo3', ['options' => [
                'class' => 'btn btn-default btn-file'], 'template' => '<i class="fa fa-paperclip"></i> ' . Yii::t('app', 'Изображение № 3'). '{input}'])->fileInput() ?>
              <p class="help-block"><?php echo Yii::t('app', 'Максимум');?> 2MB</p>
            </div>

            <div class="col-sm-6">
              <?= $form->field($response, 'video1')->textInput([
                  'maxlength' => true,
                  'placeholder' => $response->getAttributeLabel('video1')
              ]) ?>

              <?= $form->field($response, 'video2')->textInput([
                  'maxlength' => true,
                  'placeholder' => $response->getAttributeLabel('video2')
              ]) ?>

              <?= $form->field($response, 'video3')->textInput([
                  'maxlength' => true,
                  'placeholder' => $response->getAttributeLabel('video3')
              ]) ?>
            </div>
          </div>

          <div class="form-group">
              <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Сохранить'), [
                  'class' => 'btn btn-success pull-right',
                  'name' => 'signup-button']) ?>
          </div>

          <?php ActiveForm::end();?>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-6">
  <div class="white-box">
    <div class="tab-content">
      <h3 class="box-title"><?php echo Yii::t('app', 'Обратите внимание');?></h3>
      <div class="content-box-wrapper text-center clearfix">
        <?php echo Yii::t('app', '<p>Расскажите о Ваших впечатлениях, о проекте, о полученных выплатах, поделитесь своей историей успеха ! Постарайтесь написать более развернутый текст Вашего отзыва - новым участникам интересно знать Ваше мнение. Отзывы в одно предложение в стиле \'Все хорошо, спасибо.\' \'Отличный проект спасибо всем.\' - публиковаться не будут.</p></br><strong>Важно</strong><p>Отзывы размещаются на главной странице проекта. В них запрещена какая либо реклама своих ресурсов, личных контактов или реф ссылок. Главную страницу посещают участники разных команд и нужно уважать партнёров, которые с ними провели работу и пригласили в проект.</p>');?>
      </div>
    </div>
  </div>
  <?php echo \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_response-list', ['responseArray' => $responseArray]);?>
</div>
</div>

<?php if(Yii::$app->session->getFlash('success')): ?>
  <div class="modalError modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('success');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalError').modal('show');
", View::POS_END);?>

<?php endif;?>