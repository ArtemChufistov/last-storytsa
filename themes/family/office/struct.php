<?php

use app\modules\profile\widgets\InviteFriendsByEmailWidget;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Структура');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

function padding15($padding){ if ($padding > 0){return 'style = "padding-left: ' . ($padding * 15). 'px;"';};};

$tableRowView = '@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_table-row';

?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/user'), 'title' => $this->title]
    ]
]);?>
<div class="row">
  <div class="col-md-3">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_ref', ['user' => $user]);?>
  </div>
  <div class="col-md-3">
    <?= InviteFriendsByEmailWidget::widget(['user' => $user]); ?>
  </div>

    <?php if (!empty($userTree)):?>
    <div class="col-md-3">
     <div class="white-box">
      <h3 class="box-title"><?php echo Yii::t('app', 'Поиск по структуре');?></h3>
      <div class="form-group">
        <label for="input-search" class="sr-only"><?php echo Yii::t('app', 'Поиск по дереву');?>:</label>
        <input type="input" class="form-control" id="input-search" placeholder="<?php echo Yii::t('app', 'Введите для поиска...');?>" value="">
      </div>
      <button type="button" class="btn btn-success" id="btn-search"><i class="glyphicon glyphicon-search"></i> <?php echo Yii::t('app', 'Поиск');?></button>
      <button type="button" class="btn btn-info" id="btn-clear-search"><?php echo Yii::t('app', 'Очистить');?></button>
     </div>
    </div>
    <div class="col-md-3">
      <div class="white-box resultBox" style = "display: block; height: 219px; overflow: hidden;">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo Yii::t('app', 'Search result');?></h3>
        </div>

        <div class="box-body no-padding clearfix">
          <table class="table table-striped" id = "search-output">
          </table>
        </div>
      </div>
    </div>

    <?php endif;?>
</div>

<div class = "row">
    <div class="userList">
    </div>
</div>

<div class="row">
  <div class="col-md-12 col-xs-12">
  <?php /*
  <div class="white-box">
    <h3 class="box-title"><?php echo Yii::t('app', 'Информация');?></h3>
      <p class ="text-trancpancy"><?php echo Yii::t('app', 'Пригласите своих друзей и знакомых, пусть они станут участниками как и Вы. Все ваши приглашённые попадут в Вашу структуру, тем самым вы сможете получать подарки все вместе. Для этого необходимо всего-лишь поделиться своей реферальной ссылкой. <a style = "font-weight: bold" target = "_blank" href = "/marketing">Полный текст маркетинга</a>');?></p>
  </div>
  */ ?>
  <?php if (!empty($userTree)):?>
  <div class="panel">
      <div class="panel-heading">
        <?php echo Yii::t('app', 'Ваши бизнес партнёры');?>
        <span style="font-weight: normal; font-size: 14px; float: right; margin-right: 10px;"><?php echo Yii::t('app', 'Count in all lines: <strong>{count}</strong>', ['count' => count($user->descendants()->all())]);?></span>
        <span style="font-weight: normal; font-size: 14px; float: right; margin-right: 10px;"><?php echo Yii::t('app', 'Count in first line: <strong>{count}</strong>', ['count' => count($userTree)]);?></span>
      </div>

      <div class="table-responsive">
          <table class="table table-hover manage-u-table">
              <thead>
                <tr>
                    <th style="width: 100px;"><?php echo Yii::t('app', 'Уровень');?></th>
                    <th><?php echo Yii::t('app', 'Логин');?></th>
                    <th><?php echo Yii::t('app', 'Контакты');?></th>
                    <th class="text-center"><?php echo Yii::t('app', 'Личные инвестиции');?></th>
                    <th class="text-center"><?php echo Yii::t('app', 'Инвестиции структуры');?></th>
                </tr>
              </thead>
              <tbody>
                <?php $num = 0; ?>
                <?php foreach($userTree as $itemLevel1):?>
                  <?php $num++;?>
                  <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel1, 'parent' => ['login' => $user->login], 'level' => 0,'num' => $num]);?>
                  <?php if (!empty($itemLevel1['nodes'])):?>
                    <?php foreach($itemLevel1['nodes'] as $itemLevel2):?>
                      <?php $num++;?>
                      <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel2, 'parent' => $itemLevel1, 'level' => 1, 'num' => $num]);?>
                      <?php if (!empty($itemLevel2['nodes'])):?>
                        <?php foreach($itemLevel2['nodes'] as $itemLevel3):?>
                          <?php $num++;?>
                          <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel3, 'parent' => $itemLevel2, 'level' => 2, 'num' => $num]);?>
                          <?php if (!empty($itemLevel3['nodes'])):?>
                          <?php foreach($itemLevel3['nodes'] as $itemLevel4):?>
                            <?php $num++;?>
                            <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel4, 'parent' => $itemLevel3, 'level' => 3, 'num' => $num]);?>
                            <?php if (!empty($itemLevel4['nodes'])):?>
                            <?php foreach($itemLevel4['nodes'] as $itemLevel5):?>
                              <?php $num++;?>
                              <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel5, 'parent' => $itemLevel4, 'level' => 4, 'num' => $num]);?>
                              <?php if (!empty($itemLevel5['nodes'])):?>
                              <?php foreach($itemLevel5['nodes'] as $itemLevel6):?>
                                <?php $num++;?>
                                <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel6, 'parent' => $itemLevel5, 'level' => 5, 'num' => $num]);?>
                                <?php if (!empty($itemLevel6['nodes'])):?>
                                <?php foreach($itemLevel6['nodes'] as $itemLevel7):?>
                                  <?php $num++;?>
                                  <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel7, 'parent' => $itemLevel6, 'level' => 6, 'num' => $num]);?>
                                  <?php if (!empty($itemLevel7['nodes'])):?>
                                  <?php foreach($itemLevel7['nodes'] as $itemLevel8):?>
                                    <?php $num++;?>
                                    <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel8, 'parent' => $itemLevel7, 'level' => 7, 'num' => $num]);?>
                                    <?php if (!empty($itemLevel8['nodes'])):?>
                                    <?php foreach($itemLevel8['nodes'] as $itemLevel9):?>
                                      <?php $num++;?>
                                      <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel9, 'parent' => $itemLevel8, 'level' => 8, 'num' => $num]);?>
                                      <?php if (!empty($itemLevel9['nodes'])):?>
                                      <?php foreach($itemLevel9['nodes'] as $itemLevel10):?>
                                        <?php $num++;?>
                                        <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel10, 'parent' => $itemLevel9, 'level' => 9, 'num' => $num]);?>
                                        <?php if (!empty($itemLevel10['nodes'])):?>
                                        <?php foreach($itemLevel10['nodes'] as $itemLevel11):?>
                                          <?php $num++;?>
                                          <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel11, 'parent' => $itemLevel10, 'level' => 10, 'num' => $num]);?>
                                          <?php if (!empty($itemLevel11['nodes'])):?>
                                          <?php foreach($itemLevel11['nodes'] as $itemLevel12):?>
                                            <?php $num++;?>
                                            <?php echo Yii::$app->controller->renderPartial($tableRowView, ['child' => $itemLevel12, 'parent' => $itemLevel11, 'level' => 11, 'num' => $num]);?>
                                          <?php endforeach;?>
                                        <?php endif;?>
                                        <?php endforeach;?>
                                      <?php endif;?>
                                      <?php endforeach;?>
                                    <?php endif;?>
                                    <?php endforeach;?>
                                  <?php endif;?>
                                  <?php endforeach;?>
                                <?php endif;?>
                                <?php endforeach;?>
                              <?php endif;?>
                              <?php endforeach;?>
                            <?php endif;?>
                            <?php endforeach;?>
                          <?php endif;?>
                          <?php endforeach;?>
                        <?php endif;?>
                        <?php endforeach;?>
                      <?php endif;?>
                    <?php endforeach;?>
                  <?php endif;?>
                 <?php endforeach;?>
              </tbody>
          </table>
      </div>
  </div>

    <div id="treeview-searchable" class="" style = "display: none;"></div>
  </div>

  <?php $this->registerJs("
    var defaultData = " . json_encode($userTree) . "; 
    $(function() {

      var csrfToken = '" . Yii::$app->request->getCsrfToken() . "';

      function showUser(login)
      {
        if ($('.userList').find('[login=\"' + login + '\"]').html() != undefined){
          $('.userList').find('[login=\"' + login + '\"]').show();
          return;
        }

        $.post('/office/showuser', { login: login, _csrf: csrfToken }, function(data){
          $(data).insertBefore('.userList');
        });
      }

      jQuery(document).on('click', '.list-group-item', function(){
        showUser(jQuery(this).find('.login').html());
      })

      jQuery('#search-output').on('click', 'td', function(){
        showUser(jQuery(this).find('span').html());
      })

      jQuery(document).on('click', '.closeUserInfo', function(){
        $(this).parents('.userInfoPartial').remove();
      })

      var searchableTree = $('#treeview-searchable').treeview({
        data: defaultData,
      });

      var search = function(e) {
        var pattern = $('#input-search').val();

        var results = searchableTree.treeview('search', [ pattern ]);

        var output = '<tr><td>' + results.length + ' " . Yii::t('app', 'совпадений найдено') . "</td></tr>';

        $.each(results, function (index, result) {
          output += '<tr><td>' + result.text + '</td></tr>';
        });

        $('#search-output').html(output);
        $('.resultBox').show();
      }

      $('#btn-search').on('click', search);
      $('#input-search').on('keyup', search);

      $('#btn-clear-search').on('click', function (e) {
        searchableTree.treeview('clearSearch');
        $('#input-search').val('');
        $('#search-output').html('');
      });
    });

    $(document).on('click', '.treeItemICon', function(){

      var currentLogin = $(this).parent().parent().parent().parent().parent().parent().attr('currentLogin');

      if ($(this).find('i').hasClass( 'ti-plus' )){
        $(this).find('i').removeClass('ti-plus');
        $(this).find('i').addClass('ti-minus');
        $('.manage-u-table').find('[parentLogin=' + currentLogin + ']').show('slow');
      }else{
        $(this).find('i').removeClass('ti-minus');
        $(this).find('i').addClass('ti-plus');

        $('.manage-u-table').find('[parentLogin=' + currentLogin + ']').hide('slow');
      }

    })

  ", View::POS_END);?>

  <?php endif;?>

</div>
<style type="text/css">
  .manage-u-table{
    min-width: 1100px;
    overflow-y: scroll;
  }
  .treeItemICon{
    font-weight: bold;
    cursor:pointer;
  }
  #search-output tr td{
    cursor: pointer;
  }
</style>