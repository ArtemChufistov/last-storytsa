<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
?>
<div class="white-box clearfix" style = "padding-top: 31px;">

  <a class="mytooltip" href="javascript:void(0)" style = "color: #313131;text-transform:uppercase; font-weight: 500; font-size: 16px;">
    <?php echo Yii::t('app', 'Пригласить друзей по E-mail');?>
    <span class="tooltip-content5">
      <span class="tooltip-text3">
        <span class="tooltip-inner2"><?php echo Yii::t('app', 'Вы можете пригласить своих друзей, указав их E-mail через запятую');?></span>
      </span>
    </span>
  </a>

  <?php $form = ActiveForm::begin([]); ?>

  <div class = "form-group">
    <?= $form->field($inviteForm, 'separate_emails')->textInput(['class' => 'form-control', 'value' => '', 'placeholder' => Yii::t('app', 'Список E-mail через запятую')])->label(''); ?>
  </div>

  <?= Html::submitButton('<i class="glyphicon glyphicon-envelope"></i> '.Yii::t('app', 'Пригласить'), ['class' => 'btn btn-success waves-effect waves-light m-r-10']) ?>

  <?php ActiveForm::end();?>
</div>

<?php $successMessage = Yii::$app->session->getFlash('successInviteByEmail');?>

<?php if (!empty($successMessage)):?>

<div class="modal fade successModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div style = "width: 90%; font-weight: bold;" class="modal-title"><?php echo Yii::t('app', 'Внимание !');?></div>
        </div>
        <div class="modal-body">
        <?php echo $successMessage;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
      </div>
    </div>
  </div>
</div>

  <?php $this->registerJs("
      jQuery('.successModal').modal('show');
  ", View::POS_END);?>
<?php endif;?>