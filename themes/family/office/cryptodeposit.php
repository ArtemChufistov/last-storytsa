<?php
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Currency;
use app\modules\event\models\Event;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('app', 'Криптодепозиторий');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);

$currencyDic = Currency::find()->where(['key' => Currency::KEY_DIO])->one();

$balance = $user->getBalances([$currencyDic->id])[$currencyDic->id];

$trTypeArray = [Transaction::TYPE_INVEST_PERCENT => Yii::t('app', 'Процент со вклада')];
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/user'), 'title' => $this->title]
    ]
]);?>

<div class="row">
  <div class="col-md-8 col-lg-8">
   <div class="white-box clearfix" style="text-align: justify;">
    <img src = "/family/dic.png" style="float: right; margin-left: 25px; margin-bottom: 15px; width: 230px; margin-top: -10px;">
   		<p style = "font-size: 16px;"><?php echo Yii::t('app', 'Текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория<br/><br/> Текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория, текст криптодепозитория');?></p>
   </div>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $transactionProvider,
        'filterModel' => $transaction,
        'id' => 'transaction-grid',
        'layout' => '
          <div class="panel">
            <div class="panel-heading">' . Yii::t('app', 'IC Activity') . '</div>
            <div class="table-responsive">
              {items}
              <div style = "text-align:center;">{pager}</div>
            </div>
          </div>',
        'columns' => [
            [
              'attribute' => 'type',
              'format' => 'raw',
              'filter' => Select2::widget([
                  'name' => 'TransactionFinanceSearch[type]',
                  'data' => $trTypeArray,
                  'theme' => Select2::THEME_BOOTSTRAP,
                  'hideSearch' => true,
                  'options' => [
                      'placeholder' => Yii::t('app', 'Выберите тип'),
                      'value' => isset($_GET['TransactionFinanceSearch[type]']) ? $_GET['TransactionFinanceSearch[type]'] : null
                  ]
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-type', ['model' => $model]);},
            ],[
              'attribute' => 'sum',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-sum', ['model' => $model]);},
            ],[
              'attribute' => 'course',
              'format' => 'raw',
              'filter' => true,
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-course', ['model' => $model]);},
            ],[
              'attribute' => 'date_add',
              'format' => 'raw',
              'filter' => true,
              'filter' => \yii\jui\DatePicker::widget([
                  'model'=>$transaction,
                  'attribute'=>'date_add',
                  'options' => ['class' => 'form-control'],
                  'language' => \Yii::$app->language,
                  'dateFormat' => 'dd-MM-yyyy',
              ]),
              'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_transaction-date-add', ['model' => $model]);},
            ]
        ],
    ]); ?>
  <?php Pjax::end(); ?>

  </div>
	<div class="col-md-4 col-lg-4">
	    <div class="panel wallet-widgets">
	        <div class="panel-body">
	            <ul class="side-icon-text">
	                <li class="m-0"><span class="circle circle-md bg-success di vm" style ="margin-right: 10px;"><i class="ti-plus" ></i></span><span class="di vm"> <h1 class="m-b-0">
                    <h3 style = "font-size: 36px; font-weight: 100;"><?php echo number_format($balance->value, 4, ',', ' ')?> 
                    <span style = "font-size: 14px;text-align: right;">ic</span></h3><h5 class="m-t-0"><?php echo Yii::t('app', 'Ваш криптобаланс');?></h5></span></li>
	            </ul>
	        </div>
	        <div id="morris-area-chart2" style="height:208px"></div>
	        <ul class="wallet-list">
	            <li><i class="icon-wallet"></i><a href="/comingsoon"><?php echo Yii::t('app', 'Текущий курс: {course} USD', ['course' => $currencyDic->course_to_usd]);?></a></li>
	        </ul>
	    </div>
	</div>
</div>

<style type="text/css">
    #morris-area-chart2{
        overflow: hidden;
    }
</style>

<?php $this->registerJs("
Morris.Area({
    element: 'morris-area-chart2'
    , data: [{
            period: '2010'
            , SiteA: 50
            , SiteB: 0
        , }, {
            period: '2011'
            , SiteA: 160
            , SiteB: 100
        , }, {
            period: '2012'
            , SiteA: 110
            , SiteB: 60
        , }, {
            period: '2013'
            , SiteA: 60
            , SiteB: 200
        , }, {
            period: '2014'
            , SiteA: 130
            , SiteB: 150
        , }, {
            period: '2015'
            , SiteA: 200
            , SiteB: 90
        , }
        , {
            period: '2016'
            , SiteA: 100
            , SiteB: 150
        , }]
    , xkey: 'period'
    , ykeys: ['SiteA', 'SiteB']
    , labels: ['" . Yii::t("app", 'В разработке') . "', '" . Yii::t("app", 'В разработке') . "']
    , pointSize: 0
    , fillOpacity: 0.1
    , pointStrokeColors: ['#79e580', '#2cabe3']
    , behaveLikeLine: true
    , gridLineColor: '#ffffff'
    , lineWidth: 2
    , smooth: true
    , hideHover: 'auto'
    , lineColors: ['#79e580', '#2cabe3']
    , resize: true
});
  ", View::POS_END);?>