<?php
use app\modules\event\models\Event;
use lowbase\user\UserAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Статистика');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/user'), 'title' => $this->title]
    ]
]);?>

<style type="text/css">
  #w0-filters{
    display:  none;
  }
</style>

<div class="row">
  <div class="col-md-4 col-xs-4">
   <div class="white-box">
    <h3 class="box-title"><?php echo Yii::t('app', 'Участников в структуре');?></h3>
    <ul class="list-inline two-part">
        <li><i class="icon-people text-info"></i></li>
        <li class="text-right"><span class="counter"><?php echo $user->descendants()->count();?></span></li>
    </ul>
    </div>
   <div class="white-box">
    <h3 class="box-title"><?php echo Yii::t('app', 'Личных партнёров');?></h3>
    <ul class="list-inline two-part">
        <li><i class="icon-people text-info"></i></li>
        <li class="text-right"><span class="counter"><?php echo $user->children()->count();?></span></li>
    </ul>
    </div>
   <div class="white-box">
    <h3 class="box-title"><?php echo Yii::t('app', 'Переходов по REF ссылке');?></h3>
    <ul class="list-inline two-part">
        <li><i class="icon-people text-info"></i></li>
        <li class="text-right"><span class="counter"><?php echo $user->getEvents()->where(['type' => Event::TYPE_REF_LINK])->count();?></span></li>
    </ul>
    </div>
  </div>
  <div class="col-md-4 col-xs-4">
   <div class="white-box">
    <h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i> <?php echo Yii::t('app', 'События');?></h3>
      <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $eventProvider,
            'filterModel' => $eventModel,
            'layout' => '
                <div class="box-body no-padding eventTable">{items}</div><div style = "text-align: center;">{pager}</div>',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'type',
                    'format' => 'html',
                    'filter' => false,
                    'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-type', ['model' => $model]);},
                ],
                [
                    'attribute' => 'date_add',
                    'format' => 'html',
                    'filter' => false,
                    'value' => function ($model) { return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/grid/_event-date', ['model' => $model]);},
                ],
            ],
          ]); ?>
      <?php Pjax::end(); ?>
   </div>
  </div>
  <div class="col-md-4 col-xs-4">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/partial/_ref', ['user' => $user]);?>
  </div>
</div>

<style>
.table-striped tr:hover{
  background-color: #f1f2f7 !important;
}
</style>