<?php
use app\modules\finance\models\Payment;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
?>

<style type="text/css">
.form-wizard .acti .wizard-step {
  background: #00bca4 none repeat scroll 0 0;
  color: #fff;
}
.form-wizard li a:hover{
  text-decoration: none;
}
</style>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/user'), 'title' => $this->title]
    ]
]);?>

<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0"><?php echo Yii::t('app', 'Пополнение счёта');?></h3>
            <div class="wizard" id = "exampleBasic">
                <?php echo Yii::$app->controller->renderPartial(
                  '@app/themes/' . Yii::$app->params['theme'] . '/office/partial/in/_' . strtolower(\yii\helpers\StringHelper::basename(get_class($paymentForm))),
                          ['paymentForm' => $paymentForm, 'user' => $user, 'currencyArray' => $currencyArray]); ?>
            </div>
        </div>
    </div>
</div>