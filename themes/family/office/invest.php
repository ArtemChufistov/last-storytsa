<?php
use app\modules\invest\models\UserInvest;
use app\modules\event\models\Event;
use lowbase\user\UserAsset;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('app', 'Инвестиционные портфели');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>

<script src="/js/jquery-ui.min.js"></script>
<script src="/js/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" href="/css/styles-price.css">
<style type="text/css">
	.info-box-icon{
		line-height: 19px;
	}
	.smallSpanText{
		font-size: 13px;
	}
	a.info-box:hover{
		border: 1px solid #333;
	}
  .ui-slider-handle, .ui-slider-handle label{
    cursor: pointer;
  }
  #amount-label1, #amount-label2, #amount-label3{
    padding: 4px;
    vertical-align: sub;
  }
  .country-state h2{
    font-size: 20px;
  }
  .country-state small{
    font-size: 16px;
  }
  .investPrice{
    color: #313131;
  }
  .price-form{
    padding-bottom: 0px;
  }
</style>

<?php if(Yii::$app->session->getFlash('message')): ?>
  <div class="modalMessage modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('message');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalMessage').modal('show');
", View::POS_END);?>

<?php endif;?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => $this->title]
    ]
]);?>

<div class="row">
	<div class="col-md-6 col-lg-4 col-xs-12 col-sm-6">
        <div class="white-box">
            <h3 class="m-t-20 m-b-20" style = "text-align: center; font-size: 24px;"><?php echo Yii::t('app', 'Пакет <strong>"Simple"</strong>');?></h3>
			<ul class="country-state">
                <li>
                    <h2><?php echo Yii::t('app', 'Доходность');?></h2>
                    <small>0.9% <i class="fa fa-level-up text-success"></i></small>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="1.5" style="width:60%;">
                        	<span class="sr-only">0.9%</span>
                        </div>
                    </div>
                </li>
                <li>
                    <h2><?php echo Yii::t('app', 'Депозит');?></h2> <small><?php echo Yii::t('app', '90 дней');?></small>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="365" style="width:25%;"> <span class="sr-only"><?php echo Yii::t('app', '90 дней');?></span></div>
                    </div>
                </li>
            </ul>

            <div class="form-horizontal form-pricing" role="form">
              <div class="price-slider">
                <span style="height: 40px; margin-bottom: 25px;"><?php echo Yii::t('app', 'Минимальная сумма {sum} USD', ['sum' => 100]);?></span>
                <div class="col-sm-12">
                  <div id="slider1"></div>
                </div>
              </div>

              <div class="price-form">
                <div class="form-group">
                  <label for="amount1" class="col-sm-6 control-label"><?php echo Yii::t('app', 'Инвестируемая сумма ($):');?></label>
                  <div class="col-sm-6">
                    <input class="price lead form-control investPrice" id="amount-label1" investPriceNum="1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="percent" class="col-sm-6 control-label"><?php echo Yii::t('app', 'Начисленные проценты ($):');?></label>
                  <div class="col-sm-6">
                    <input type="hidden" id="percent1" class="form-control">
                    <p class="price lead" id="percent-label1"></p>
                  </div>
                </div>
                <hr class="style">
                <div class="form-group" style = "margin: 0px;">
                  <label for="total" class="col-sm-6 control-label" ><strong><?php echo Yii::t('app', 'Всего к получению:');?> </strong></label>
                  <div class="col-sm-6">
                    <input type="hidden" id="total1" class="form-control">
                    <p class="price lead" id="total-label1"></p>

                  </div>
                </div>
              </div>
            </div>

            <button class="btn btn-success btn-rounded waves-effect waves-light btn-block p-10 invest1">
            	<?php echo Yii::t('app', 'Инвестировать');?>
            </button>

            <button class="btn btn-danger btn-rounded waves-effect waves-light btn-block p-10 dangerinvest1" style="display: none;">
              <?php echo Yii::t('app', 'Укажите сумму больше 100$');?>
            </button>

            <div class="modal fade" tabindex="-1" role="dialog" id = "invest1">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <div style = "width: 90%;" class="modal-title"><?php echo Yii::t('app', 'Инвестирование'); ?></div>
                    </div>
                    <div class="modal-body">
                        <?php $form = ActiveForm::begin(); ?>

                            <div class="price-table-content">
                                <div class="price-row"><?php echo Yii::t('app', 'Инвестируемая сумма <strong id = "amount11"></strong> USD');?></div>
                                <div class="price-row"><?php echo Yii::t('app', 'Получаемая сумма <strong id = "total11"></strong> USD');?></div>
                                <div class="price-row"><?php echo Yii::t('app', 'Вы можете выбрать возвратный вариант инвестиции в течении 14 дней, но в этом случае Вам будет недоступны дополнительные начисления по маркетингу');?>
                                <br/>
                                <br/>
                                    <div class="checkbox">
                                        <input id="checkbox1" type="checkbox" name = "invest-type[returnType]">
                                        <label for="checkbox1"> <?php echo Yii::t('app', 'Возвратный'); ?> </label>
                                    </div>
                                </div>
                                <div class="price-row"><?php echo Yii::t('app', 'Для создания инвестици нажмите кнопку <strong>"Подтвердить"</strong>');?></div>
                            </div>

                            <input type="hidden" id="amount1" class="form-control" name = "invest-type[amount]">

                            <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Подтвердить'), [
                              'class' => 'btn btn-block btn-success pull-left',
                              'value' => 1,
                              'name' => 'invest-type[type]']) ?>
                        <?php ActiveForm::end();?>
                        <div style="clear: both;"></div>
                    </div>
                </div>
              </div>
            </div>
        </div>
    </div>
	<div class="col-md-6 col-lg-4 col-xs-12 col-sm-6">
        <div class="white-box">
          <h3 class="m-t-20 m-b-20" style = "text-align: center; font-size: 24px"><?php echo Yii::t('app', 'Пакет <strong>"Medium"</strong>');?></h3>
			<ul class="country-state">
                <li>
                    <h2><?php echo Yii::t('app', 'Доходность');?></h2>
                    <small>1.2% <i class="fa fa-level-up text-success"></i></small>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="1.5" style="width:80%;">
                        	<span class="sr-only">1.2%</span>
                        </div>
                    </div>
                </li>
                <li>
                    <h2><?php echo Yii::t('app', 'Депозит');?></h2> <small><?php echo Yii::t('app', '180 дней');?> </small>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="365" style="width:50%;"> <span class="sr-only"><?php echo Yii::t('app', '180 дней');?></span></div>
                    </div>
                </li>
            </ul>

            <div class="form-horizontal form-pricing" role="form">
              <div class="price-slider">

                <span style="height: 40px; margin-bottom: 25px;"><?php echo Yii::t('app', 'Минимальная сумма {sum} USD', ['sum' => 100]);?></span>
                <div class="col-sm-12">
                  <div id="slider2"></div>
                </div>
              </div>

              <div class="price-form">
                <div class="form-group">
                  <label for="amount1" class="col-sm-6 control-label"><?php echo Yii::t('app', 'Инвестируемая сумма ($):');?></label>
                  <div class="col-sm-6">
                    <input class="price lead form-control investPrice" id="amount-label2" investPriceNum="2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="percent" class="col-sm-6 control-label"><?php echo Yii::t('app', 'Начисленные проценты ($):');?></label>
                  <div class="col-sm-6">
                    <input type="hidden" id="percent2" class="form-control">
                    <p class="price lead" id="percent-label2"></p>
                  </div>
                </div>
                <hr class="style">
                <div class="form-group" style = "margin: 0px;">
                  <label for="total" class="col-sm-6 control-label"><strong><?php echo Yii::t('app', 'Всего к получению:');?> </strong></label>
                  <div class="col-sm-6">
                    <input type="hidden" id="total2" class="form-control">
                    <p class="price lead" id="total-label2"></p>

                  </div>
                </div>
              </div>
            </div>

            <button class="btn btn-success btn-rounded waves-effect waves-light btn-block p-10 invest2">
            	<?php echo Yii::t('app', 'Инвестировать');?>
            </button>

            <button class="btn btn-danger btn-rounded waves-effect waves-light btn-block p-10 dangerinvest2" style="display: none;">
              <?php echo Yii::t('app', 'Укажите сумму больше 100$');?>
            </button>

            <div class="modal fade" tabindex="-1" role="dialog" id = "invest2">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <div style = "width: 90%;" class="modal-title"><?php echo Yii::t('app', 'Инвестирование'); ?></div>
                    </div>
                    <div class="modal-body">
                        <?php $form = ActiveForm::begin(); ?>

                            <div class="price-table-content">
                                <div class="price-row"><?php echo Yii::t('app', 'Инвестируемая сумма <strong id = "amount22"></strong> USD');?></div>
                                <div class="price-row"><?php echo Yii::t('app', 'Получаемая сумма <strong id = "total22"></strong> USD');?></div>
                                <div class="price-row"><?php echo Yii::t('app', 'Вы можете выбрать возвратный вариант инвестиции в течении 14 дней, но в этом случае Вам будет недоступны дополнительные начисления по маркетингу');?>
                                <br/>
                                <br/>
                                    <div class="checkbox">
                                        <input id="checkbox2" type="checkbox" name = "invest-type[returnType]">
                                        <label for="checkbox2"> <?php echo Yii::t('app', 'Возвратный'); ?> </label>
                                    </div>
                                </div>
                                <div class="price-row"><?php echo Yii::t('app', 'Для создания инвестици нажмите кнопку <strong>"Подтвердить"</strong>');?></div>
                            </div>

                            <input type="hidden" id="amount2" class="form-control" name = "invest-type[amount]">

                            <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Подтвердить'), [
                              'class' => 'btn btn-block btn-success pull-left',
                              'value' => 2,
                              'name' => 'invest-type[type]']) ?>
                        <?php ActiveForm::end();?>
                        <div style="clear: both;"></div>
                    </div>
                </div>
              </div>
            </div>

        </div>
    </div>
	<div class="col-md-6 col-lg-4 col-xs-12 col-sm-6">
        <div class="white-box">
          <h3 class="m-t-20 m-b-20" style = "text-align: center; font-size: 24px"><?php echo Yii::t('app', 'Пакет <strong>"Gold"</strong>');?></h3>
			<ul class="country-state">
                <li>
                    <h2><?php echo Yii::t('app', 'Доходность');?></h2>
                    <small>1.5% <i class="fa fa-level-up text-success"></i></small>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="1.5" style="width:100%;">
                        	<span class="sr-only">1.5%</span>
                        </div>
                    </div>
                </li>
                <li>
                    <h2><?php echo Yii::t('app', 'Депозит');?></h2> <small><?php echo Yii::t('app', '365 дней');?> </small>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="365" style="width:100%;"> <span class="sr-only"><?php echo Yii::t('app', '365 дней');?></span></div>
                    </div>
                </li>
            </ul>

            <div class="form-horizontal form-pricing" role="form">
              <div class="price-slider">
                <span style="height: 40px; margin-bottom: 25px;"><?php echo Yii::t('app', 'Минимальная сумма {sum} USD', ['sum' => 100]);?></span>
                <div class="col-sm-12">
                  <div id="slider3"></div>
                </div>
              </div>

              <div class="price-form">
                <div class="form-group">
                  <label for="amount3" class="col-sm-6 control-label"><?php echo Yii::t('app', 'Инвестируемая сумма ($):');?></label>
                  <div class="col-sm-6">
                    <input class="price lead form-control investPrice" id="amount-label3" investPriceNum="3">
                  </div>
                </div>
                <div class="form-group">
                  <label for="percent" class="col-sm-6 control-label"><?php echo Yii::t('app', 'Начисленные проценты ($):');?></label>
                  <div class="col-sm-6">
                    <input type="hidden" id="percent2" class="form-control">
                    <p class="price lead" id="percent-label3"></p>
                  </div>
                </div>
                <hr class="style">
                <div class="form-group" style = "margin: 0px;">
                  <label for="total" class="col-sm-6 control-label"><strong><?php echo Yii::t('app', 'Всего к получению:');?> </strong></label>
                  <div class="col-sm-6">
                    <input type="hidden" id="total2" class="form-control">
                    <p class="price lead" id="total-label3"></p>
                  </div>
                </div>
              </div>
            </div>
            <button class="btn btn-success btn-rounded waves-effect waves-light btn-block p-10 invest3">
            	<?php echo Yii::t('app', 'Инвестировать');?>
            </button>

            <button class="btn btn-danger btn-rounded waves-effect waves-light btn-block p-10 dangerinvest3" style="display: none;">
              <?php echo Yii::t('app', 'Укажите сумму больше 100$');?>
            </button>

            <div class="modal fade" tabindex="-1" role="dialog" id = "invest3">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <div style = "width: 90%;" class="modal-title"><?php echo Yii::t('app', 'Инвестирование'); ?></div>
                    </div>
                    <div class="modal-body">
                        <?php $form = ActiveForm::begin(); ?>

                            <div class="price-table-content">
                                <div class="price-row"><?php echo Yii::t('app', 'Инвестируемая сумма <strong id = "amount33"></strong> USD');?></div>
                                <div class="price-row"><?php echo Yii::t('app', 'Получаемая сумма <strong id = "total33"></strong> USD');?></div>
                                <div class="price-row"><?php echo Yii::t('app', 'Вы можете выбрать возвратный вариант инвестиции в течении 14 дней, но в этом случае Вам будет недоступны дополнительные начисления по маркетингу');?>
                                <br/>
                                <br/>
                                    <div class="checkbox">
                                        <input id="checkbox3" type="checkbox" name = "invest-type[returnType]">
                                        <label for="checkbox3"> <?php echo Yii::t('app', 'Возвратный'); ?> </label>
                                    </div>
                                </div>
                                <div class="price-row"><?php echo Yii::t('app', 'Для создания инвестици нажмите кнопку <strong>"Подтвердить"</strong>');?></div>
                            </div>

                            <input type="hidden" id="amount3" class="form-control" name = "invest-type[amount]">

                            <?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Подтвердить'), [
                              'class' => 'btn btn-block btn-success pull-left',
                              'value' => 3,
                              'name' => 'invest-type[type]']) ?>
                        <?php ActiveForm::end();?>
                        <div style="clear: both;"></div>
                    </div>
                </div>
              </div>
            </div>

        </div>
    </div>
</div>

<script>
$(document).ready(function() {
  $("#slider1").slider({
      animate: true,
      value:1500,
      min: 100,
      max: 20000,
      step: 10,
      slide: function(event, ui) {
        update1(1,ui.value); //changed
      }
  });
  $("#amount1").val(1500);
  $("#amount-label1").val(0);

  update1();

  $("#slider2").slider({
      animate: true,
      value:1500,
      min: 100,
      max: 20000,
      step: 10,
      slide: function(event, ui) {
        update2(1,ui.value); //changed
      }
  });
  $("#amount2").val(1500);
  $("#amount-label2").val(0);

  update2();

  $("#slider3").slider({
      animate: true,
      value:1500,
      min: 100,
      max: 20000,
      step: 10,
      slide: function(event, ui) {
        update3(1,ui.value); //changed
      }
  });
  $("#amount3").val(1500);
  $("#amount-label3").val(0);

  update3();
});

function update1(slider,val) {
  var formatNumber = {
    separador: ",",
    sepDecimal: '.',
    formatear:function (num){
    num +='';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
    }
    return this.simbol + splitLeft  +splitRight;
    },
    new:function(num, simbol){
    this.simbol = simbol ||'';
    return this.formatear(num);
    }
  }
  //changed. Now, directly take value from ui.value. if not set (initial, will use current value.)
  var $amount = slider == 1?val:$("#amount1").val();

  $percent = (($amount / 100) * 0.9) * 90;
  $total = (parseFloat($percent) + parseFloat($amount)).toFixed(2);
  $percent = (parseFloat($percent)).toFixed(2);
  $total = formatNumber.new($total,"$");
  $( "#amount1" ).val($amount);
  $( "#amount11" ).text($amount);
  $( "#amount-label1" ).val($amount);
  $( "#percent-label1").text($percent);
  $( "#total1" ).val($total);
  $( "#total11" ).text($total);
  if ($total == '$NaN'){
    $total = '$0.00';
  }
  $( "#total-label1" ).text($total);

  $('#slider1 span').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
  $('#slider1 span').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');

}

function update2(slider,val) {
  var formatNumber = {
    separador: ",",
    sepDecimal: '.',
    formatear:function (num){
    num +='';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
    }
    return this.simbol + splitLeft  +splitRight;
    },
    new:function(num, simbol){
    this.simbol = simbol ||'';
    return this.formatear(num);
    }
  }
  //changed. Now, directly take value from ui.value. if not set (initial, will use current value.)
  var $amount = slider == 1?val:$("#amount2").val();

  $percent = (($amount / 100) * 1.2) * 180;
  $total = (parseFloat($percent) + parseFloat($amount)).toFixed(2);
  $percent = (parseFloat($percent)).toFixed(2);
  $total = formatNumber.new($total,"$");
  $( "#amount2" ).val($amount);
  $( "#amount22" ).text($amount);
  $( "#amount-label2" ).val($amount);
  $( "#percent-label2").text($percent);
  $( "#total2" ).val($total);
  $( "#total22" ).text($total);
  if ($total == '$NaN'){
    $total = '$0.00';
  }
  $( "#total-label2" ).text($total);

  $('#slider2 span').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
  $('#slider2 span').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');

}

function update3(slider,val) {
  var formatNumber = {
    separador: ",",
    sepDecimal: '.',
    formatear:function (num){
    num +='';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
    }
    return this.simbol + splitLeft  +splitRight;
    },
    new:function(num, simbol){
    this.simbol = simbol ||'';
    return this.formatear(num);
    }
  }
  //changed. Now, directly take value from ui.value. if not set (initial, will use current value.)
  var $amount = slider == 1?val:$("#amount3").val();

  $percent = (($amount / 100) * 1.5) * 365;
  $total = (parseFloat($percent) + parseFloat($amount)).toFixed(2);
  $percent = (parseFloat($percent)).toFixed(2);
  $total = formatNumber.new($total,"$");
  $( "#amount3" ).val($amount);
  $( "#amount33" ).text($amount);
  $( "#amount-label3" ).val($amount);
  $( "#percent-label3").text($percent);
  $( "#total3" ).val($total);
  $( "#total33" ).text($total);
  if ($total == '$NaN'){
    $total = '$0.00';
  }
  $( "#total-label3" ).text($total);

  $('#slider3 span').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
  $('#slider3 span').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
}

$('.investPrice').keydown(function(event){
  if(event.keyCode == 190 || event.keyCode == 188){
    event.preventDefault();
  }
});
$('.investPrice').keyup(function(event){
  var sum = $(this).val();

  sum = sum.replace(/^0+/,'');

  if(event.keyCode == 38){
    sum = parseInt(sum) + 10;
  }
  
  if(event.keyCode == 40){
    sum = parseInt(sum) - 10;
  }

  if (isNaN(sum) || sum < 100){
    if (isNaN(sum) || sum < 0){
      sum = 0;
    }
    $('.invest' + $(this).attr('investPriceNum')).hide();
    $('.dangerinvest' + $(this).attr('investPriceNum')).show();
  }else{
    $('.invest' + $(this).attr('investPriceNum')).show();
    $('.dangerinvest' + $(this).attr('investPriceNum')).hide();
  }

  if (sum > 20000){
    sum = 20000;
  }

  if ($(this).attr('investPriceNum') == 1){
    $("#slider1").slider('option', 'value', sum);
    update1(1, sum);
  }

  if ($(this).attr('investPriceNum') == 2){
    $("#slider2").slider('option', 'value', sum);
    update2(1, sum);
  }

  if ($(this).attr('investPriceNum') == 3){
    $("#slider3").slider('option', 'value', sum);
    update3(1, sum);
  }

  $(this).val(sum);
})

</script>

<?php $this->registerJs("
jQuery('.invest1').on('click', function(){
    jQuery('#invest1').modal('show');
})
jQuery('.invest2').on('click', function(){
    jQuery('#invest2').modal('show');
})
jQuery('.invest3').on('click', function(){
    jQuery('#invest3').modal('show');
})
", View::POS_END);?>