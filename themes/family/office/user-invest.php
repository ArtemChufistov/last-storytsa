<?php
use app\modules\invest\models\UserInvest;
use app\modules\invest\models\InvestPref;
use app\modules\finance\models\Currency;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('user', 'Ваши инвестиции');
$this->params['breadcrumbs'][] = $this->title;

$currenyBTC      = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
$currencyFreez24 = Currency::find()->where(['key' => Currency::KEY_FREEZ24])->one();
$currencyFreez   = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();
$currencyDepozit = Currency::find()->where(['key' => Currency::KEY_DEPOZIT])->one();
$balanceFreez24  = $user->getBalances([$currencyFreez24->id])[$currencyFreez24->id];
$balanceFreez    = $user->getBalances([$currencyFreez->id])[$currencyFreez->id];
$balanceDepozit  = $user->getBalances([$currencyDepozit->id])[$currencyDepozit->id];

$userInvestArray = UserInvest::find()->where(['user_id' => $user->id])->andWhere(['status' => UserInvest::STATUS_ACTIVE])->andWhere(['invest_type_id' => [1, 2, 3]])->all();
$userInvestDepositArray = UserInvest::find()->where(['user_id' => $user->id])->andWhere(['status' => UserInvest::STATUS_ACTIVE])->andWhere(['invest_type_id' => [4]])->all();

?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/user'), 'title' => $this->title]
    ]
]);?>


<script type="text/javascript">
function number_format( number, decimals, dec_point, thousands_sep ) {

    var i, j, kw, kd, km;

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");

    return km + kw + kd;
}

</script>

<?php if (!empty($userInvestArray)):?>
	<div class="row">
	  <div class="col-md-12 col-xs-12">
		<div class="white-box">
		    <h3 class="box-title"><?php echo Yii::t('app', 'Основные инвестиции');?></h3>
		    <ul class="nav customtab nav-tabs" role="tablist">
		        <li role="presentation" class="active"><a href="#maininvestgraph" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"><i class="ti-layout-grid2"></i> <?php echo Yii::t('app', 'Графика');?></span></a></li>
		        <li role="presentation" class="">
		        	<a href="#maininvestlist" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">
		        		<span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs"><i class="ti-menu"></i> <?php echo Yii::t('app', 'Список');?></span>
		        	</a>
		        </li>
		        <li role="presentation" class="">
		        	<a href="/office/invest">
		        		<span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs"><i class="fa fa-usd"></i> <?php echo Yii::t('app', 'Оформить инвестицию');?></span>
		        	</a>
		        </li>
		    </ul>

		    <!-- Tab panes -->
		    <div class="tab-content">
		        <div role="tabpanel" class="tab-pane fade active in" id="maininvestgraph">
				    <div class="row">
				    <?php foreach($userInvestArray as $num => $userInvest ):?>

				        <?php $investPrefPercent = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_STAT_PERCENT])->one();?>
				        <?php $investPrefPayPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_PAY_PERIOD])->one();?>
				        <?php $investPrefPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_INVEST_PERIOD])->one();?>
				        <?php $investPrefCancelPercent = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_INVEST_CANCEL_PERCENT])->one();?>
				        <?php $periodInDays = $investPrefPeriod->value / $investPrefPayPeriod->value;?>
				        <?php $periodInDaysOut = ((($periodInDays * $investPrefPercent->value) - $userInvest->closed_percent ) / $investPrefPercent->value);?>

				        	<div class="col-lg-4 col-sm-6 col-xs-12">
				        		<div class="white-box" style = "border: 2px solid #f7fafc;">
				                <h3 class="box-title"><?php echo Yii::t('app', 'Инвестиция от ({date})', ['date' => date('H:i d-m-Y', strtotime($userInvest->date_add))]);?></h3>
				                <div id="morris-donut-chart<?php echo $num;?>" style="height:318px; padding-top: 10px;"></div>
				                <?php $this->registerJs("
				                    Morris.Donut({
				                        element: 'morris-donut-chart" . $num . "'
				                        , data: [{
				                            label: '" . Yii::t('app', 'Получено') . "'
				                            , value: " . number_format($userInvest->paid_sum, 2, '.', '') . "
				                            , }, {
				                            label: '" . Yii::t('app', 'Проценты по вкладу') . "'
				                            , value: " . number_format(($userInvest->sum / 100) * ($investPrefPercent->value * $periodInDaysOut), 2, '.', ''). "
				                            }, {
				                            label: '" . Yii::t('app', 'Вложенная сумма') . "'
				                            , value: " . number_format($userInvest->sum, 2, '.', '') . "
				                            }
				                            ],
				                        formatter: function (x) { return number_format(x, 2, '.', ' ') + ' USD'}
				                        , resize: true
				                        , colors: ['#ff7676', '#2cabe3', '#53e69d']
				                    });
				                    "); ?>
				                <div class="row p-t-30 clearfix">
				                    <div class="col-xs-12">
				                        <h3 class="font-medium"><?php echo Yii::t('app', 'К получению: <strong>{paidSum}</strong> USD', ['paidSum' => number_format((($userInvest->sum / 100) * ($investPrefPercent->value *  $periodInDaysOut)) + $userInvest->sum, 2)]);?></h3>
				                        <h5 class="text-muted m-t-0"><?php echo Yii::t('app', 'Получено: {paidSum} USD', ['paidSum' => number_format($userInvest->paid_sum, 2)]);?></h5>
				                        <h5 class="text-muted m-t-0"><?php echo Yii::t('app', 'Дневной доход <strong>{sum}</strong> USD', ['sum' => number_format(($userInvest->sum / 100) * $investPrefPercent->value, 2)]);?></h5>
				                        <h5 class="text-muted m-t-0"><?php echo Yii::t('app', 'Активна до <strong>{date}</strong>', ['date' => date('d-m-Y',$investPrefPeriod->value + strtotime($userInvest->date_add)  )]);?></h5>
				                    </div>

				                    <div class = "row" style = "height: 180px;">
				                    	<div class="col-lg-12">
				                    		<?php if ($userInvest->returnType == UserInvest::RETURN_TYPE_RETURN14):?>
				                    			<button class="btn btn-danger btn-rounded waves-effect waves-light btn-block p-10 cancelInvest">
				                    				<?php echo Yii::t('app', 'Отменить инвестицию');?>
				                    			</button>

												<div class="modal fade cancelInvestModal" tabindex="-1" role="dialog">
												  <div class="modal-dialog" role="document">
												    <div class="modal-content">
												        <div class="modal-header">
												          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												          <div style = "width: 90%; font-weight: bold;" class="modal-title"><?php echo Yii::t('app', 'Внимание !');?></div>
												        </div>
												        <div class="modal-body">
												        	<p style = "font-size: 16px;"><?php echo Yii::t('app', 'Отменяя инвестицию вы отказываетесь получать <strong>{sum}</strong> USD каждый день в течении <strong>{days}</strong> дней.<br/> При отмене инвестиции будет удержана комиссия в размере <strong>{comissionSum}</strong> USD.<br/><br/> Вы уверены, что хотите отменить свою инвестицию ?', [
												        		'sum' => ($userInvest->sum / 100) * $investPrefPercent->value,
												        		'days' => $investPrefPeriod->value / 86400,
												        		'comissionSum' => ($userInvest->sum / 100) * $investPrefCancelPercent->value,
												        		]);?></p>
												        </div>
														<div class="modal-footer">
														<?php $form = ActiveForm::begin(); ?>

															<?= $form->field($cancelUserInvestForm, 'user_invest_type_datestamp')->hiddenInput(['value' => strtotime($userInvest->date_add)])->label(''); ?>

															<?= Html::submitButton(Yii::t('app', 'Я отменяю инвестицию'), [
												                'class' => 'btn btn-danger',
												                'name' => 'cancelUserInvest']) ?>

												        	<button type="button" class="btn btn-success" data-dismiss="modal"><?php echo Yii::t('app', 'Я хочу оставить эту инвестицию');?></button>
												        <?php ActiveForm::end();?>
												     	</div>
												    </div>
												  </div>
												</div>

				                    		<?php endif;?>
				                    	</div>
				                    </div>
				                </div>
				                </div>
				            </div>

				    <?php endforeach;?>
				    </div>

				    <h3 style = "text-align: center;"><?php echo Yii::t('app', 'Для дополнительного дохода вы можете оформить новую инвестицию');?></h3>
				    <div class = "row" style = "text-align: center;">
				    	<a href = "/office/invest" class="btn btn-info btn-rounded waves-effect waves-light p-10" style = "padding-left: 40px !important;padding-right: 40px !important; font-size: 18px;"><?php echo Yii::t('app', 'Оформить новую инвестицию');?></a>
				    </div>

		        </div>
		        <div role="tabpanel" class="tab-pane fade" id="maininvestlist">
					<div class="table-responsive">
	                    <table class="table table-hover manage-u-table">
	                        <thead>
	                            <tr>
	                                <th class="text-center" width="70">№</th>
	                                <th><?php echo Yii::t('app', 'Вложенная сумма');?></th>
	                                <th><?php echo Yii::t('app', 'Дневной доход');?></th>
	                                <th><?php echo Yii::t('app', 'Получено');?></th>
	                                <th><?php echo Yii::t('app', 'Начисление за весь срок');?></th>
	                                <th><?php echo Yii::t('app', 'Дата создания');?></th>
	                                <th><?php echo Yii::t('app', 'Дата окончания');?></th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        <?php $inSum = 0; $outSum = 0;?>
						    <?php foreach($userInvestArray as $num => $userInvest ):?>

						        <?php $investPrefPercent = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_STAT_PERCENT])->one();?>
						        <?php $investPrefPayPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_PAY_PERIOD])->one();?>
						        <?php $investPrefPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_INVEST_PERIOD])->one();?>
						        <?php $periodInDays = $investPrefPeriod->value / $investPrefPayPeriod->value;?>

						        <?php $outSum += ($userInvest->sum + ($userInvest->sum / 100) * ($investPrefPercent->value * $periodInDays));?>

						        <?php $inSum += $userInvest->sum;?>

	                            <tr>
	                                <td class="text-center"><?php echo $num + 1;?></td>
	                                <td><strong><?php echo number_format($userInvest->sum, 2);?></strong> USD</td>
	                                <td><strong><?php echo number_format(($userInvest->sum / 100) * $investPrefPercent->value, 2);?></strong> USD</td>
	                                <td><strong><?php echo number_format($userInvest->paid_sum, 2);?></strong> USD</td>
	                                <td><strong><?php echo number_format($userInvest->sum + ($userInvest->sum / 100) * ($investPrefPercent->value * $periodInDays), 2);?></strong> USD</td>
	                                <td>
	                                	<?php echo date('d-m-Y', strtotime($userInvest->date_add));?>
	                                	<br><span class="text-muted"><?php echo date('H:i', strtotime($userInvest->date_add));?></span>
	                                </td>
	                                <td>
	                                	<?php echo date('d-m-Y',$investPrefPeriod->value + strtotime($userInvest->date_add));?>
	                                	<br><span class="text-muted"><?php echo date('H:i',$investPrefPeriod->value + strtotime($userInvest->date_add));?></span>
	                                </td>
	                            </tr>
	                        <?php endforeach;?>
	                        </tbody>
	                    </table>
	                    <p>
	                    	<strong><?php echo Yii::t('app', 'Всего к начислению за весь срок:');?></strong> <?php echo number_format($outSum, 2);?> USD<br/>
	                    	<strong><?php echo Yii::t('app', 'Всего вложено:');?></strong> <?php echo number_format($inSum, 2);?> USD<br/>
	                    </p>
	                </div>
		        </div>
		    </div>
		</div>
	  </div>
	</div>
<?php endif;?>

<?php if (!empty($userInvestDepositArray)):?>
<div class="row">
  <div class="col-md-12 col-xs-12">
    <div class="white-box">
      <div class="tab-content">
        <h3 class="box-title"><?php echo Yii::t('app', 'Депозитные инвестиции');?></h3>
	    <ul class="nav customtab nav-tabs" role="tablist">
	        <li role="presentation" class="active"><a href="#depositinvestgraph" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"><i class="ti-layout-grid2"></i> <?php echo Yii::t('app', 'Графика');?></span></a></li>
	        <li role="presentation" class=""><a href="#depositinvestlist" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs"><i class="ti-menu"></i> <?php echo Yii::t('app', 'Список');?></span></a></li>
	    </ul>
        <div class="content-box-wrapper text-center clearfix">
        	<div role="tabpanel" class="tab-pane fade active in" id="depositinvestgraph" style = "padding-top: 30px;">
        		<div class ="row">
			    <?php foreach($userInvestDepositArray as $num => $userInvest ):?>

			        <?php $investPrefPercent = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_STAT_PERCENT])->one();?>
			        <?php $investPrefPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_INVEST_PERIOD])->one();?>
			        <?php $investPrefPayPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_PAY_PERIOD])->one();?>
			        <?php $periodInDays = $investPrefPeriod->value / $investPrefPayPeriod->value;?>
			        <?php $periodInDaysOut = ((($periodInDays * $investPrefPercent->value) - $userInvest->closed_percent ) / $investPrefPercent->value);?>

					<div class="col-lg-4 col-sm-6 col-xs-12">
		        		<div class="white-box" style = "border: 2px solid #f7fafc;">
		                <h3 class="box-title"><?php echo Yii::t('app', 'Депозитная инвестиция от ({date})', ['date' => date('H:i d-m-Y', strtotime($userInvest->date_add))]);?></h3>
		                <div id="morris-donut-chart-deposit<?php echo $num;?>" style="height:318px; padding-top: 10px;"></div>
		                <?php $this->registerJs("
		                    Morris.Donut({
		                        element: 'morris-donut-chart-deposit" . $num . "'
		                        , data: [{
		                            label: '" . Yii::t('app', 'Получено') . "'
		                            , value: " . number_format($userInvest->paid_sum, 2, '.', '') . "
		                            , }, {
		                            label: '" . Yii::t('app', 'Проценты по вкладу') . "'
		                            , value: " . number_format(($userInvest->sum / 100) * ($investPrefPercent->value *  $periodInDaysOut), 2, '.', ''). "
		                            }, {
		                            label: '" . Yii::t('app', 'Вложенная сумма') . "'
		                            , value: " . number_format($userInvest->sum, 2, '.', ''). "
		                            }
		                            ],
		                        formatter: function (x) { return number_format(x, 2, '.', ' ') + ' USD'}
		                        , resize: true
		                        , colors: ['#ff7676', '#2cabe3', '#53e69d']
		                    });
		                    "); ?>
		                <div class="row p-t-30 clearfix">
		                    <div class="col-xs-12">
		                        <h3 class="font-medium" style = "text-align: left;"><?php echo Yii::t('app', 'К получению: <strong>{paidSum}</strong> USD', ['paidSum' => number_format((($userInvest->sum / 100) * ($investPrefPercent->value *  $periodInDaysOut)) + $userInvest->sum, 2)]);?></h3>
		                        <h5 class="text-muted m-t-0" style = "text-align: left;"><?php echo Yii::t('app', 'Получено: <strong>{paidSum}</strong> USD', ['paidSum' => number_format($userInvest->paid_sum, 2)]);?></h5>
		                        <h5 class="text-muted m-t-0" style = "text-align: left;"><?php echo Yii::t('app', 'Дневной доход <strong>{sum}</strong> USD', ['sum' => number_format(($userInvest->sum / 100) * $investPrefPercent->value, 2)]);?></h5>
		                        <h5 class="text-muted m-t-0" style = "text-align: left;"><?php echo Yii::t('app', 'Активна до <strong>{date}</strong>', ['date' => date('d-m-Y',$investPrefPeriod->value + strtotime($userInvest->date_add)  )]);?></h5>
		                    </div>

		                </div>
		                </div>
		            </div>
			    <?php endforeach;?>
			    </div>
        	</div>
        	<div role="tabpanel" class="tab-pane fade active in" id="depositinvestlist">
        	</div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif;?>

<?php if (empty($userInvestDepositArray) && empty($userInvestArray)):?>
	<div class="row">
	  <div class="col-md-12 col-xs-12">
		<div class="white-box">
		    <h3 style = "text-align: center;"><?php echo Yii::t('app', 'Чтобы получать стабильный доход, оформите инвестицию');?></h3>
		    <div class = "row" style = "text-align: center;">
		    	<a href = "/office/invest" class="btn btn-info btn-rounded waves-effect waves-light p-10" style = "padding-left: 30px !important;padding-right: 30px !important; font-size: 18px;"><?php echo Yii::t('app', 'Оформить инвестицию');?></a>
		    </div>
		</div>
	  </div>
	</div>
<?php endif;?>

<?php if (!empty($cancelUserInvestForm->getErrors())):?>
	<?php $this->registerJs("
	    jQuery('.cancelInvestError').modal('show');
	", View::POS_END);?>
<?php endif;?>
<?php $this->registerJs("
jQuery('.cancelInvest').on('click', function(){
    jQuery(this).parent().find('.cancelInvestModal').modal('show');
})
", View::POS_END);?>
