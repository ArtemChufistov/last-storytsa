<?php
use app\modules\finance\models\CryptoWallet;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyGraph;
use app\modules\finance\models\Payment;
use app\modules\invest\models\UserInvest;
use app\modules\matrix\models\Matrix;
use app\modules\finance\models\Transaction;
use app\modules\event\models\Event;
use app\modules\invest\models\InvestPref;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Панель управления');
$this->params['breadcrumbs'][] = $this->title;

$currenyBTC      = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
$currencyFreez24 = Currency::find()->where(['key' => Currency::KEY_FREEZ24])->one();
$currencyFreez   = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();
$currencyDepozit = Currency::find()->where(['key' => Currency::KEY_DEPOZIT])->one();
$balanceFreez24  = $user->getBalances([$currencyFreez24->id])[$currencyFreez24->id];
$balanceFreez    = $user->getBalances([$currencyFreez->id])[$currencyFreez->id];
$balanceDepozit  = $user->getBalances([$currencyDepozit->id])[$currencyDepozit->id];

$investPrefArray = InvestPref::find()->where(['key' => InvestPref::KEY_SIMPLE_INVEST])->all();

$simpleInvestIds = [];
foreach ($investPrefArray as $investPref) {
    $simpleInvestIds[$investPref->invest_type_id] = $investPref->invest_type_id;
}

$userInvestArray = UserInvest::find()->where(['user_id' => $user->id])->andWhere(['status' => UserInvest::STATUS_ACTIVE])->andWhere(['invest_type_id' => $simpleInvestIds])->all();


$investPrefArray = InvestPref::find()->where(['key' => InvestPref::KEY_DEPOSIT_INVEST])->all();

$depositInvestIds = [];
foreach ($investPrefArray as $investPref) {
    $depositInvestIds[$investPref->invest_type_id] = $investPref->invest_type_id;
}

$userInvestDepositArray = UserInvest::find()->where(['user_id' => $user->id])->andWhere(['status' => UserInvest::STATUS_ACTIVE])->andWhere(['invest_type_id' => $depositInvestIds])->all();


$children = $user->children()->all();

$childrenWithStruct = [];
foreach ($children as $child) {
    $countChildren = count($child->children()->all());
    if ($countChildren > 0){
        $childrenWithStruct[] = [
            'user' => $child,
            'countChildren' => $countChildren
        ];
    }
}

for ($j = 0; $j < count($childrenWithStruct) - 1; $j++){
    for ($i = 0; $i < count($childrenWithStruct) - $j - 1; $i++){
        // если текущий элемент больше следующего
        if ($childrenWithStruct[$i]['countChildren'] > $childrenWithStruct[$i + 1]['countChildren']){
            // меняем местами элементы
            $tmp_var = $childrenWithStruct[$i + 1];
            $childrenWithStruct[$i + 1] = $childrenWithStruct[$i];
            $childrenWithStruct[$i] = $tmp_var;
        }
    }
}
?>

<?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_title_breadcrumbs', [
    'title' => $this->title,
    'breadcrumbs' => [
        ['link' => Url::to('/office/dashboard'), 'title' => $this->title]
    ]
]);?>

<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>

<div class="row">
        <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">
                    <a class="mytooltip" href="javascript:void(0)" style = "color: #313131;">
                        <?php echo str_replace(' ', '<br/>', Yii::t('app', 'Ваш баланс'));?>
                        <span class="tooltip-content5">
                            <span class="tooltip-text3">
                                <span class="tooltip-inner2"><?php echo Yii::t('app', 'Ваш основной баланс, доступный к выводу или созданию дополнительных инвестиций');?></span>
                            </span>
                        </span>
                    </a>
                </h3>
                <ul class="list-inline two-part">
                    <li class="text-left" >
                        <span class="counter"><?php foreach($user->getBalances() as $balance):?><?php echo number_format($balance->value, 2);?><?php endforeach;?></span>
                        <p><?php echo $balance->getCurrency()->one()->key;?></p>
                    </li>
                    <li class = "text-right"><i class="icon-wallet text-purple"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">
                    <a class="mytooltip" href="javascript:void(0)" style = "color: #313131;">
                        <?php echo str_replace(' ', '<br/>', Yii::t('app', 'Реферальные за личников'));?>
                        <span class="tooltip-content5">
                            <span class="tooltip-text3">
                                <span class="tooltip-inner2"><?php echo Yii::t('app', 'Будут переведены на основной баланс в течении 24 часов');?></span>
                            </span>
                        </span>
                    </a>
                </h3>
                <ul class="list-inline two-part">
                    <li class="text-left"><span class=""><?php echo number_format($balanceFreez24->value, 2);?></span> <p>USD</p></li>
                    <li class="text-right"><i class="icon-wallet text-danger"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="white-box">
                    <a class="mytooltip" href="javascript:void(0)" style = "color: #313131;">
                        <h3 class="box-title"><?php echo str_replace(' ', '<br/>', Yii::t('app', 'Накопленные проценты'));?>
                        <span class="tooltip-content5">
                            <span class="tooltip-text3">
                                <span class="tooltip-inner2"><?php echo Yii::t('app', 'Здесь накапливаются проценты по вашим вкладам и переводятся на баланс в течении 10 дней');?></span>
                            </span>
                        </span>
                    </a>
                </h3>
                <ul class="list-inline two-part">
                    <li class="text-left"><span class=""><?php echo number_format($balanceFreez->value, 2);?></span> <p>USD</p></li>
                    <li class="text-right"><i class="icon-wallet text-info"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">
                    <a class="mytooltip" href="javascript:void(0)" style = "color: #313131;">
                        <?php echo str_replace(' ', '<br/>', Yii::t('app', 'Реферальные со структуры'));?>
                        <span class="tooltip-content5">
                            <span class="tooltip-text3">
                                <span class="tooltip-inner2"><?php echo Yii::t('app', 'Здесь накапливаются ваши реферальные отчисления со структуры и через 7 дней на них них оформляется депозит под 1.5% дневных на 3 месяца');?></span>
                            </span>
                        </span>
                    </a>
                </h3>
                <ul class="list-inline two-part">
                    <li class="text-left"><span class=""><?php echo number_format($balanceDepozit->value, 2);?></span> <p>USD</p></li>
                    <li class="text-right"><i class="ti-wallet text-success"></i></li>
                </ul>
            </div>
        </div>
</div>

<!-- .row -->
<div class="row">
    <div class="col-lg-6 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <div class="white-box" id="particles-js">
                    <h3 class="box-title"><?php echo Yii::t('app', 'Текущий курс BTC');?></h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-speedometer  text-info"></i></li>
                        <li class="text-right"><span class="counter"><?php echo $currenyBTC->course_to_usd;?></span> USD</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
<div class="col-lg-12 col-sm-12 col-xs-12">

<div class="panel">
<div class="panel-body">
<h3 class="title-hero">
    <?php echo Yii::t('app', 'Последние 10 операций');?>
</h3>
<div class="example-box-wrapper">
<table id="datatable-tabletools" class="table table-striped table-bordered" cellspacing="0" width="100%">
<thead>
    <thead>
        <tr>
            <th><?php echo Yii::t('app', 'Тип');?></th>
            <th><?php echo Yii::t('app', 'Сумма');?></th>
            <th><?php echo Yii::t('app', 'Дата');?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach(Transaction::find()->where(['from_user_id' => $user->id])->orWHere(['to_user_id' => $user->id])->limit(10)->orderBy(['id' => SORT_DESC])->all() as $transaction):?>
            <?php $toUser = $transaction->getToUser()->one();?>
            <?php $fromUser = $transaction->getFromUser()->one();?>
            <tr>
                <td><?php echo $transaction->getTypeTitle();?></td>
                <td>
                    <?php if (!empty($toUser) && $toUser->id == $user->id):?>
                        <span class="label label-success label-rouded">+<?php echo number_format($transaction->sum, 2);?> <?php echo $transaction->getCurrency()->one()->title;?></span>
                    <?php else:?>
                        <span class="label label-danger label-rouded">-<?php echo number_format($transaction->sum, 2);?> <?php echo $transaction->getCurrency()->one()->title;?></span>
                    <?php endif;?>
                </td>
                <td><?php echo date("H:i:s d-m-y", strtotime($transaction->date_add));?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
</div>
</div>

</div>


    </div>
    <div class="col-lg-12 col-sm-12 col-xs-12">

<div class="panel">
<div class="panel-body">
<h3 class="title-hero">
    <?php echo Yii::t('app', 'Посление <strong>10</strong> событий');?>
</h3>
<div class="example-box-wrapper">
<table id="datatable-tabletools" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th><?php echo Yii::t('app', 'Событие');?></th>
            <th><?php echo Yii::t('app', 'Логин');?></th>
            <th><?php echo Yii::t('app', 'Дата');?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach(Event::find()->where(['user_id' => $user->id])->limit(10)->orderBy(['date_add' => SORT_DESC])->all() as $event):?>
            <?php $toUser = $event->getToUser()->one();?>
            <?php if (!empty($toUser)):?>
                <tr>
                    <td><?php echo $event->getTypeTitle();?></td>
                    <td><?php echo $toUser->login;?></td>
                    <td><?php echo date("H-i-s d-m-y", strtotime($event->date_add));?></td>
                </tr>
            <?php endif;?>
        <?php endforeach;?>
    </tbody>
</table>
</div>
</div>
</div>


    </div>
        </div>
    </div>

    <div class="col-lg-6 col-xs-12">
        <div class="panel clearfix">
            <div class="p-20 text-center">
                <h4 class="m-t-30 font-medium"><?php echo Yii::t('app', 'Топ активных лидеров');?></h4>
                <ul class="dp-table m-t-30">
                    <?php if (empty($childrenWithStruct)):?>
                        <h3><?php echo Yii::t('app', 'Приглашайте ваших знакомых для увеличения своей прибыли');?></h3>
                    <?php else:?>
                        <?php foreach(array_reverse($childrenWithStruct) as $num => $child):?>
                            <li><img src="<?php echo $child['user']->getImage();?>" alt="<?php echo $child['user']->login;?>" width="60" data-toggle="tooltip" title="<?php echo $child['user']->login;?>" class="img-circle"></li>
                        <?php endforeach;?>
                    <?php endif;?>
                </ul>
            </div>
            <div class = "p-20 text-center">
                <a href="<?php echo Url::to(['/office/struct'])?>" class="btn btn-rounded btn-success"><i class="ti-list m-r-5"></i><?php echo Yii::t('app', 'Посмотреть структуру');?></a>
            </div>
        </div>
    </div>

<script type="text/javascript">
function number_format( number, decimals, dec_point, thousands_sep ) {

    var i, j, kw, kd, km;

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");

    return km + kw + kd;
}

</script>

    <div class="col-lg-6 col-sm-12 col-xs-12">
        <div class="row">

<?php if (!empty($userInvestArray)):?>

    <?php
        $userInvestPaidSum = 0;
        $userInvestPSum = 0;
        $userInvestSum = 0;
    ?>
    <?php foreach($userInvestArray as $num => $userInvest ):?>

        <?php $investPrefPercent = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_STAT_PERCENT])->one();?>
        <?php $investPrefPayPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_PAY_PERIOD])->one();?>
        <?php $investPrefPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_INVEST_PERIOD])->one();?>
        <?php $periodInDays = $investPrefPeriod->value / $investPrefPayPeriod->value;?>
        <?php $periodInDaysOut = ((($periodInDays * $investPrefPercent->value) - $userInvest->closed_percent ) / $investPrefPercent->value);?>

        <?php $userInvestPaidSum += $userInvest->paid_sum?>
        <?php $userInvestPSum += $userInvest->sum; ?>
        <?php $userInvestSum += (($userInvest->sum / 100) * ($investPrefPercent->value * $periodInDaysOut));?>
    <?php endforeach;?>

        <div <?php if (empty($userInvestDepositArray)):?>class="col-lg-12 col-sm-12 col-xs-12"<?php else:?>class="col-lg-6 col-sm-6 col-xs-12"<?php endif;?>>
            <div class="white-box">
                <h3 class="box-title"><?php echo Yii::t('app', 'Ваши инвестиции');?></h3>
                <div id="morris-donut-chart" style="height:318px; padding-top: 50px;"></div>
                <?php $this->registerJs("
                    Morris.Donut({
                        element: 'morris-donut-chart'
                        , data: [{
                            label: '" . Yii::t('app', 'Получено') . "'
                            , value: " . number_format($userInvestPaidSum, 2, '.', '') . "
                            , }, {
                            label: '" . Yii::t('app', 'Начисленный процент') . "'
                            , value: " . number_format($userInvestSum, 2, '.', '') . "
                            }, {
                            label: '" . Yii::t('app', 'Вложенная сумма') . "'
                            , value: " . number_format($userInvestPSum, 2, '.', ''). "
                            }
                            ],
                            formatter: function (x) { return number_format(x, 2, '.', ' ') + ' USD'}
                        , resize: true
                        , colors: ['#ff7676', '#2cabe3', '#53e69d']
                    });
                    "); ?>
                <div class="row p-t-30">
                    <div class="col-xs-12 p-t-30">
                        <h4 class="font-medium"><span style = "font-weight: normal; font-size: 16px;"><?php echo Yii::t('app', 'Всего вложено:');?></span> <?php echo number_format($userInvestPSum, 2); ?> USD</h4>
                        <h4 class="font-medium"><span style = "font-weight: normal; font-size: 16px;"><?php echo Yii::t('app', 'Всего получено:');?></span> <?php echo number_format($userInvestPaidSum, 2); ?> USD</h4>
                        <h4 class="font-medium"><span style = "font-weight: normal; font-size: 16px;"><?php echo Yii::t('app', 'Ещё получите:');?></span> <?php echo number_format(($userInvestPSum + $userInvestSum), 2); ?> USD</h4>
                    </div>
                    <div style = "text-align: center;">
                        <a href="<?php echo Url::to(['/office/onuserinvest'])?>" class="btn btn-rounded btn-success"><i class="ti-list m-r-5"></i><?php echo Yii::t('app', 'Посмотреть инвестиции');?></a>
                    </div>
                </div>
            </div>
        </div>


<?php endif;?>

<?php $deposiPaidSum = 0;?>
<?php if (!empty($userInvestDepositArray)):?>

    <?php
        $userInvestPaidSum = 0;
        $userInvestPSum = 0;
        $userInvestSum = 0;
    ?>

    <?php foreach($userInvestDepositArray as $num => $userInvest ):?>

        <?php $investPrefPercent = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_STAT_PERCENT])->one();?>
        <?php $investPrefPayPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_PAY_PERIOD])->one();?>
        <?php $investPrefPeriod = InvestPref::find()->where(['invest_type_id' => $userInvest->invest_type_id])->andWhere(['key' => InvestPref::KEY_INVEST_PERIOD])->one();?>
        <?php $periodInDays = $investPrefPeriod->value / $investPrefPayPeriod->value;?>
        <?php $periodInDaysOut = ((($periodInDays * $investPrefPercent->value) - $userInvest->closed_percent ) / $investPrefPercent->value);?>

        <?php $userInvestPaidSum += $userInvest->paid_sum?>
        <?php $userInvestPSum += $userInvest->sum; ?>
        <?php $userInvestSum += (($userInvest->sum / 100) * ($investPrefPercent->value * $periodInDaysOut));?>

    <?php endforeach;?>
            <div <?php if (empty($userInvestArray)):?>class="col-lg-12 col-sm-12 col-xs-12"<?php else:?>class="col-lg-6 col-sm-6 col-xs-12"<?php endif;?>>
                <div class="white-box">
                    <h3 class="box-title"><?php echo Yii::t('app', 'Ваши депозитные инвестиции');?></h3>
                    <div id="morris-donut-chart1" style="height:318px; padding-top: 50px;"></div>
                    <?php $this->registerJs("
                        Morris.Donut({
                            element: 'morris-donut-chart1'
                            , data: [{
                                label: '" . Yii::t('app', 'Получено') . "'
                                , value: " . number_format($userInvestPaidSum, 2) . "
                                , }, {
                                label: '" . Yii::t('app', 'Начисленный процент') . "'
                                , value: " . number_format($userInvestSum, 2). "
                                }, {
                                label: '" . Yii::t('app', 'Вложенная сумма') . "'
                                , value: " . number_format($userInvestPSum, 2) . "
                                }
                                ],
                            formatter: function (x) { return number_format(x, 2, '.', ' ') + ' USD'}
                            , resize: true
                            , colors: ['#ff7676', '#2cabe3', '#53e69d']
                        });
                        "); ?>
                    <div class="row p-t-30">
                    <div class="col-xs-12 p-t-30">
                        <h4 class="font-medium"><span style = "font-weight: normal; font-size: 16px;"><?php echo Yii::t('app', 'Всего вложено:');?></span> <?php echo number_format($userInvestPSum, 2); ?> USD</h4>
                        <h4 class="font-medium"><span style = "font-weight: normal; font-size: 16px;"><?php echo Yii::t('app', 'Всего получено:');?></span> <?php echo number_format($userInvestPaidSum, 2); ?> USD</h4>
                        <h4 class="font-medium"><span style = "font-weight: normal; font-size: 16px;"><?php echo Yii::t('app', 'Ещё получите:');?></span> <?php echo number_format($userInvestPSum + $userInvestSum, 2); ?> USD</h4>
                    </div>
                    <div style = "text-align: center;">
                        <a href="<?php echo Url::to(['/office/onuserinvest'])?>" class="btn btn-rounded btn-success"><i class="ti-list m-r-5"></i><?php echo Yii::t('app', 'Посмотреть инвестиции');?></a>
                    </div>
                    </div>
                </div>
            </div>
<?php endif;?>

<?php if (empty($userInvestArray) && empty($userInvestDepositArray)):?>
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="white-box">
            <h3 style="text-align: center;"><?php echo Yii::t('app', 'Чтобы получать стабильный доход, оформите инвестицию');?></h3>
            <div class="row" style="text-align: center;">
                <a href="/office/invest" class="btn btn-info btn-rounded waves-effect waves-light p-10" style="padding-left: 30px !important;padding-right: 30px !important; font-size: 18px;"><?php echo Yii::t('app', 'Оформить инвестицию');?></a>
            </div>
        </div>
    </div>
<?php endif;?>
        </div>
    </div>
</div>


<style>
.table-striped tr:hover{
  background-color: #f1f2f7 !important;
}
.particles-js-canvas-el{
    position: absolute;
    top: 0px;
    width: 96% !important;
}
#particles-js{
    position: relative;
}
</style>

<script type="text/javascript">
particlesJS("particles-js", {
  "particles": {
    "number": {
      "value": 150,
      "density": {
        "enable": true,
        "value_area": 700
      }
    },
    "color": {
      "value": ["#aa73ff", "#f8c210", "#83d238", "#33b1f8"]
    },
    "shape": {
      "type": "circle",
      "stroke": {
        "width": 0,
        "color": "#000000"
      },
      "polygon": {
        "nb_sides": 15
      }
    },
    "opacity": {
      "value": 0.9
      ,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 1.5,
        "opacity_min": 0.15,
        "sync": false
      }
    },
    "size": {
      "value": 2.5,
      "random": false,
      "anim": {
        "enable": true,
        "speed": 2,
        "size_min": 0.15,
        "sync": false
      }
    },
    "line_linked": {
      "enable": true,
      "distance": 110,
      "color": "#33b1f8",
      "opacity": 0.5,
      "width": 1
    },
    "move": {
      "enable": true,
      "speed": 1.8,
      "direction": "none",
      "random": false,
      "straight": false,
      "out_mode": "out",
      "bounce": false,
      "attract": {
        "enable": false,
        "rotateX": 600,
        "rotateY": 1200
      }
    }
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": false,
        "mode": "repulse"
      },
      "onclick": {
        "enable": false,
        "mode": "push"
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 400,
        "line_linked": {
          "opacity": 1
        }
      },
      "bubble": {
        "distance": 400,
        "size": 40,
        "duration": 2,
        "opacity": 8,
        "speed": 3
      },
      "repulse": {
        "distance": 200,
        "duration": 0.4
      },
      "push": {
        "particles_nb": 4
      },
      "remove": {
        "particles_nb": 2
      }
    }
  },
  "retina_detect": true
});
var count_particles, stats, update;
stats = new Stats;
stats.setMode(0);
stats.domElement.style.position = 'absolute';
stats.domElement.style.left = '0px';
stats.domElement.style.top = '0px';
document.body.appendChild(stats.domElement);
count_particles = document.querySelector('.js-count-particles');
update = function() {
  stats.begin();
  stats.end();
  if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
    count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
  }
  requestAnimationFrame(update);
};
requestAnimationFrame(update);;
</script>