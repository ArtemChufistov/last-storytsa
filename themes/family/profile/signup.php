<?php
use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use lowbase\user\components\AuthChoice;
use app\modules\mainpage\models\Preference;

$this->title = Yii::t('app', 'Регистрация');
?>
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="new-login-register">
  <div class="lg-info-panel">
    <div class="inner-panel">
      <div class="lg-content">
        <h2><?php echo Yii::t('app', 'When today is over, it will never come back.');?></h2>
      </div>
    </div>
  </div>
  <div class="new-login-box">
    <div class="white-box">
        <?php $form = ActiveForm::begin([
          'id' => 'loginform',
          'fieldConfig' => [
              'template' => "{input}\n{hint}\n{error}",
  
          ],
          'options' => [
              'class' => 'form-horizontal '
          ]
        ]); ?>

        <a href="javascript:void(0)" class="text-center db">
          <img src="/family/dechmont_logo2.png" alt="Dechmont" style = "width: 175px;" /><br/>
          </a>  
        <div class="form-group m-t-40">
          <div class="col-xs-12">
            <?php $readonlyPref = Preference::find()->where(['key' => Preference::KEY_USER_REG_CAN_PARENT_EDIT])->one()->value == 1 ? false : true;?>

            <?= $form->field($model, 'ref')->textInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('ref'),
                'readonly' => $readonlyPref,
                'id' => 'ref-user-name',
            ]); ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <?= $form->field($model, 'login')->textInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('login'),
                'id' => 'login-user-name',
            ]); ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <?= $form->field($model, 'email')->textInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('email'),
                'id' => 'email-user-name',
            ]); ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <?= $form->field($model, 'password')->passwordInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('password'),
            ]); ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">        
            <?= $form->field($model, 'passwordRepeat')->passwordInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('passwordRepeat'),
            ]); ?>
          </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox checkbox-primary pull-left p-t-0" style = "padding-left: 5px;">
                    <?php $labelLicense = Yii::t('app', 'Я согласен с условиями') . ' ' . Html::a(Yii::t('app', 'лицензионного соглашения'), ['/licenseagreement'], ['target' => '_blank']);?>

                    <?= $form->field($model, 'licenseAgree', ['template' => '{input}{label}{error}', 'options' => ['tag' => false]])->checkBox([], false)->label($labelLicense,['class'=>'label-text']);?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class = "col-xs-12">
                <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
                    'imageOptions' => [
                        'id' => 'my-captcha-image',
                    ],
                    'captchaAction' => '/profile/profile/captcha',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => $model->getAttributeLabel('captcha')
                    ],
                    'template' => '
                    <div class="row">
                      <div class="col-lg-8">{input}</div>
                    </div>
                    <div class="row">
                      <div class="col-lg-4">{image}</div>
                    </div>',
                ]);
                ?>
            </div>
        </div>

        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <?= Html::submitButton('<i class="glyphicon glyphicon-user"></i> '.Yii::t('user', 'Зарегистрироваться'), [
            'class' => 'btn btn-info btn-block text-uppercase waves-effect waves-light',
            'name' => 'login-button']) ?>
          </div>
        </div>
      
      <?php ActiveForm::end(); ?>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
            <?= Yii::t('app', 'Если регистрировались ранее, можете')?> <?=Html::a(Yii::t('app', '<strong>войти на сайт</strong>'), ['/login'], ['class' => ['text-picton-blue']])?>,
            <?= Yii::t('app', 'используя Логин или E-mail')?>
        </div>
      </div>
    </div>
  </div>
</section>

<style>

#my-captcha-image{
    cursor: pointer;
}

.new-login-register{
  overflow-y: scroll;
}
</style>