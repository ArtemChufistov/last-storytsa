<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Восстановление пароля');
?>

<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box clearfix">
        <h3 class="box-title m-b-20"><?= Html::encode($this->title) ?></h3>
          <div class ="row">
          <?php if (Yii::$app->session->hasFlash('reset-success')):?>
            <div class='text-center'><?php echo Yii::$app->session->getFlash('reset-success');?></div>
          <?php else: ?>

            <?php $form = ActiveForm::begin([
              'fieldConfig' => [
                  'template' => "{input}\n{hint}\n{error}",
              ],
              'options' => [
                  'data-form-output' => 'form-output-global',
                  'data-form-type' => 'contact',
                  'class' => 'text-left offset-top-30'
              ]
            ]); ?>

          <div class="form-group ">
            <div class="col-xs-12">
              <?= $form->field($forget, 'email')->textInput([
                  'maxlength' => true,
                  'placeholder' => $forget->getAttributeLabel('email'),
              ]); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
            <?= $form->field($forget, 'password')->textInput([
                'maxlength' => true,
                'placeholder' => $forget->getAttributeLabel('password'),
            ]); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <?= $form->field($forget, 'captcha')->widget(Captcha::className(), [
                  'captchaAction' => '/profile/profile/captcha',
                  'options' => [
                      'class' => 'form-control',
                      'placeholder' => $forget->getAttributeLabel('captcha')
                  ]
              ]);
              ?>
            </div>
          </div>
          <div class="form-group text-center m-t-20">
            <div class="col-xs-12">
              <?= Html::submitButton(Yii::t('user', 'Сменить пароль'), [
                'class' => 'btn btn-primary btn-block text-uppercase waves-effect waves-light',
                'name' => 'login-button']) ?>
            </div>
          </div>

          <?php ActiveForm::end();?>

        <?php endif;?>

        </div>
        <div class="offset-top-30 text-sm-left text-dark text-extra-small row">
          <div class="form-group " style = "margin-top: 20px;">
            <div class="col-xs-12">
              <?=Html::a(Yii::t('user', 'Личный кабинет'), ['/login'], ['class' => 'text-picton-blue'])?>
            </div>
            <div class="col-xs-12">
              <?=Html::a(Yii::t('user', 'Регистрация'), ['/signup'], ['class' => 'text-picton-blue'])?>
              </div>
          </div>
        </div>
    </div>
  </div>
</section>