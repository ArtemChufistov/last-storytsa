<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use lowbase\document\DocumentAsset;
use app\modules\profile\models\User;
use app\modules\finance\models\Payment;
use app\modules\profile\components\AuthChoice;
use app\modules\profile\models\forms\LoginForm;

$this->title = Yii::t('user', 'Вход на сайт');
?>
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="new-login-register">
  <div class="lg-info-panel">
    <div class="inner-panel">
      <div class="lg-content">
        <h2><?php echo Yii::t('app', 'When today is over, it will never come back.');?></h2>
      </div>
    </div>
  </div>
  <div class="new-login-box">
    <div class="white-box">
        <?php $form = ActiveForm::begin([
          'id' => 'loginform',
          'fieldConfig' => [
              'template' => "{input}\n{hint}\n{error}",
          ],
          'options' => [
              'class' => 'form-horizontal'
          ]
        ]); ?>

        <a href="javascript:void(0)" class="text-center db">

          <img src="/family/dechmont_logo2.png" alt="Dechmont" style = "width: 175px;" /><br/>

          </a>  
        <div class="form-group m-t-40">
          <div class="col-xs-12">
            <?= $form->field($model, 'loginOrEmail')->textInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('loginOrEmail'),
                'options' => [ 'class' => 'form-control']
            ]); ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <?= $form->field($model, 'password')->passwordInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('password'),
                'options' => [ 'class' => 'form-control']
            ]); ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <?=Html::a(Yii::t('user', '<i class="fa fa-lock m-r-5"></i>Восстановить пароль'), ['/resetpasswd'], [
                'class' => 'ext-dark pull-right'
            ])?>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <?= Html::submitButton('<i class="glyphicon glyphicon-log-in"></i> '.Yii::t('user', 'Войти'), [
            'class' => 'btn btn-info btn-block text-uppercase waves-effect waves-light',
            'name' => 'login-button']) ?>
          </div>
          <div class="col-xs-12" style = "margin-top: 20px;">
            <?=Html::a(Yii::t('user', '<i class="glyphicon glyphicon-user"></i> Зарегистрироваться'), ['/signup'], ['class' => 'btn btn-block btn-success'])?>
          </div>
        </div>
      
      <?php ActiveForm::end(); ?>
<?php /*
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
          <p class="text-extra-small text-dark offset-top-4"><?=Yii::t('user', 'Войти с помощью социальных сетей')?>:</p>
          <div class="social">
            <?=AuthChoice::widget([
              'baseAuthUrl' => ['/profile/auth/index'],
              'clientCssClass' => 'col-xs-3',
            ])?>
          </div>
        </div>
      </div>
*/ ?>
    </div>
  </div>
</section>
<style type="text/css">
  .new-login-register{
    overflow-y: scroll;
  }
</style>