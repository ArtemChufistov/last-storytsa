<?php
use yii\web\View;
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;

$matrix   = Matrix::find()->where(['user_id' => $this->params['user']->id])->one();

$user = $this->params['user'];
$subMenuActive = 0;
?>

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav">
        <div class="sidebar-head">
            <h3>
                <span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu"><?php echo Yii::t('app', 'Личный кабинет');?></span>
            </h3>
        </div>
        <ul class="nav" id="side-menu">
            <?php foreach ($menu->children(1)->all() as $num => $child): ?>
                <li>
                    <a href="<?=Url::to([$child->link]);?>" class="waves-effect <?php if (Url::to([$requestUrl]) == Url::to([$child->link])):?>active<?php endif;?>">
                        <?php echo $child->icon; ?>
                        <span class="hide-menu">
                            <?php echo $child->title?> <span class="fa arrow"></span>
                        </span>
                    </a>
                    <?php if (!empty($child->children(2)->all())):?>
                        <ul class="nav nav-second-level">
                            <?php foreach ($child->children(2)->all() as $num => $childChild): ?>
                                <li>
                                    <a href="<?=Url::to([$childChild->link]);?>"><i class=" fa-fw"><?php echo $childChild->icon; ?></i><span class="hide-menu"><?php echo $childChild->title?> </span></a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif;?>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
</div>
