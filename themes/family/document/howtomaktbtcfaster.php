<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\finance\models\Currency;
$this->title = Yii::t('app', 'How to make BTC transactions faster');

$url = 'https://bitcoinfees.earn.com/api/v1/fees/list';
$currencyCourseList = json_decode(file_get_contents($url), true);

$currencyBTC = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
?>

<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register" style = "overflow-y: scroll;">
  <div class="login-box" style = "width: 620px;">
    <div class="white-box">
      <form class="form-horizontal form-material">
        
        <div class="form-group">
          <div class="col-xs-12 text-center">
            <div class="user-thumb text-center">
              <h3><?php echo $this->title;?></h3>
              <p><?php echo Yii::t('app', 'If you want to make fast BTC transaction, yout will need change transaction fee via table:'); ?></p>
            </div>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12 text-center" style = "text-align: center;">
            <table class="table" cellspacing="14">
                <thead>
                    <tr>
                        <th><?php echo Yii::t('app', 'Time from'); ?></th>
                        <th><?php echo Yii::t('app', 'Time to'); ?></th>
                        <th><?php echo Yii::t('app', 'In BTC'); ?></th>
                        <th><?php echo Yii::t('app', 'In USD'); ?></th>
                        <th><?php echo Yii::t('app', 'IN Satoshi'); ?></th>
                    </tr>
                </thead>
                <tbody>
                  <?php $doubles = []; ?>
                  <?php foreach(array_reverse($currencyCourseList['fees']) as $currencyCourse):?>
                    <?php $arrTitle = $currencyCourse['minMinutes'] . '_' . $currencyCourse['maxMinutes'];?>
                    <?php if (empty($doubles[$arrTitle])): ?>
                        <?php $doubles[$arrTitle] = $arrTitle;?>
                          <tr class="advance-table-row active">
                              <td><?php echo $currencyCourse['minMinutes'];?> min</td>
                              <td><?php echo $currencyCourse['maxMinutes'];?> min</td>
                              <td><?php echo number_format($currencyCourse['minFee'] / 1000000, 6, ',', '.');?></td>
                              <td><?php echo number_format(($currencyCourse['minFee'] / $currencyBTC->course_to_usd) * 226, 2, ',', '.');?></td>
                              <td><?php echo $currencyCourse['minFee'];?></td>
                          </tr>
                          <tr>
                              <td colspan="5" class="sm-pd"></td>
                          </tr>
                    <?php endif;?>
                  <?php endforeach;?>
                </tbody>
            </table>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>