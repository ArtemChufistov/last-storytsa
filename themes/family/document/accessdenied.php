<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Доступ запрещён');
?>

<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box" style = "width: 620px;">
    <div class="white-box">
      <form class="form-horizontal form-material">
        
        <div class="form-group">
          <div class="col-xs-12 text-center">
            <div class="user-thumb text-center">
              <h3><?php echo Yii::t('app', 'Ошибка');?></h3>
            </div>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12 text-center" style = "text-align: center;">
            <?= Yii::t('app', 'Вам необходимо подтвердить свой E-mail<br/> перейти в 
              <a style = "font-weight: bold;" href = "/office/dashboard">личный кабинет</a>');?>
            <br/>
            <br/>
            <a href="/" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40"><?php echo Yii::t('app', 'Вернуться на главную');?></a>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>