<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
		'label' => 'White Paper CITT.io',
		'url' => '/howitworks'
	],
];

$this->title = $model->title;
?>

<section class="breadcrumb-classic">
<div class="shell section-34 section-sm-50">
  <div class="range range-lg-middle">
    <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="icon-lg mdi mdi-camera-iris icon icon-white"></span></div>
    <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
      <h2><span class="big"><?php echo Yii::t('app', 'Как это работает');?></span></h2>
    </div>
    <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
      <ul class="list-inline list-inline-dashed p">
        <li><a href="/"><?php echo Yii::t('app', 'Главная');?></a></li>
        <li><?php echo Yii::t('app', 'Как это работает');?>
        </li>
      </ul>
    </div>
  </div>
</div>

</section>

<main class="page-content">
<!-- What we do-->
<section class="section-98 section-sm-110">
  <div class="shell">
    <h1><?php echo Yii::t('app', 'Уникальный маркетинг');?></h1>
    <hr class="divider bg-mantis">
    <p class="inset-left-11p inset-right-11p"><?php echo Yii::t('app', '
    <img src="http://marketing-assets.avvo.com/uploads/sites/4/2016/02/shutterstock_296195525.jpg">
    <p style="text-align: center;"><strong><span style="color:#66cc33">УНИКАЛЬНЫЙ МАРКЕТИНГ СИСТЕМЫ &laquo;BIT.INFO&raquo; ПРОСТ И ПОНЯТЕН!</span></strong></p>

<p style="text-align: justify;">Очень короткая матрица 1 + 3. Матрица заполняется по правилу: сверху - вниз, слева - направо. Все участники становятся в матрицу в порядке очередности регистрации и оплаты в &laquo;Bit.info&raquo;.</p>

<p style="text-align: justify;">Как только в матрицу Партнера &laquo;A&raquo; встают три партнера &laquo;B&raquo;, &laquo;C&raquo; и &laquo;D&raquo; его матрица закрывается и он получает вознаграждение. Так же у Партнера &laquo;A&raquo; появляются два клона &laquo;A1&raquo; и &laquo;A2&raquo;, которые сразу встают под Партнера &laquo;В&raquo;, и на 2/3 закрывают матрицу Партнера &laquo;B&raquo;. Как только регистрируется новый участник Партнер &laquo;E&raquo;, матрица Партнера &laquo;B&raquo; закрывается. Партнер &laquo;B&raquo; выходит на вознаграждение и у него тоже открываются два клона &laquo;B1&raquo; и &laquo;B2&raquo;, которые встают в матрицу Партнера &laquo;C&raquo; и тоже закрывают ее на 2/3. Достаточно одного нового участника &laquo;F&raquo; чтобы матрица Партнера &laquo;C&raquo; закрылась, и он получил вознаграждение. Два клона &laquo;С1&raquo; и &laquo;С2&raquo; встают в матрицу Партнера &laquo;D&raquo; и как только в нее попадает Партнер &laquo;G&raquo; матрица у Партнера &laquo;D&raquo; закрывается.</p>

<p style="text-align: justify;">Далее становится еще интереснее,&nbsp;клоны &laquo;D1&raquo; и &laquo;D2&raquo; становятся под клон &laquo;A1&raquo; и как только под него попадает Партнер &laquo;H&raquo; матрица у клона &laquo;A1&raquo; закрывается и Партнер &laquo;А&raquo; снова получает вознаграждение. Но и это еще не все,&nbsp;у&nbsp;Партнера &laquo;А&raquo; сразу же открываются еще два клона &laquo;A3&raquo; и &laquo;A4&raquo;, которые встают под его же второй клон &laquo;A2&raquo; и закрывают его матрицу на 2/3. Таким образом получается, что в этом случае Партнер &laquo;A&raquo; закрывает на 2/3 матрицу своего второго клона &laquo;A2&raquo; своими же клонами &laquo;A3&raquo; и &laquo;A4&raquo;. Достаточно одного нового участника &laquo;I&raquo;, чтобы матрица у клона &laquo;A2&raquo; закрылась и Партнер &laquo;A&raquo; снова получил вознаграждение.</p>

<p style="text-align: justify;">И так далее заполняются матрицы у всех участников проекта. Клоны разрастаются. Участники начинают <span style="color:#66cc33"><strong>зарабатывать все больше и больше</strong></span>, даже при небольшом количестве новых Партнеров системы &laquo;Bit.info&raquo;, так как все матрицы кроме самой первой (в которой нужны 4 новых участника) на 2/3 закрываются клонами.</p>

<p style="text-align: justify;">Именно поэтому уникальный маркетинг системы &laquo;Bit.info&raquo; позволит нам работать очень долго и Вы сможете <span style="color:#66cc33"><strong>получать постоянный</strong></span>&nbsp;пассивный доход.</p>
');?></p>
    <div class="range offset-top-66">
      <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-cellphone-link"></span>
                <div>
                  <h4 class="text-bold offset-top-20"><?php echo Yii::t('app', '<span style="color:#66cc33">Оплата участия 100 $ единоразово</span>');?></h4>
                  
                </div>
      </div>
      <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-chart-line"></span>
                <div>
                  <h4 class="text-bold offset-top-20"><?php echo Yii::t('app', '<span style="color:#66cc33">Многократное получение прибыли</span>');?></h4>
                  
                </div>
      </div>
      <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-pen"></span>
                <div>
                  <h4 class="text-bold offset-top-20"><?php echo Yii::t('app', '<span style="color:#66cc33">Нет обязательных приглашений</span>');?></h4>
                  
                </div>
      </div>
      <div class="range-spacer veil reveal-md-inline-block offset-top-41 offset-md-top-66"></div>
      <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-timetable"></span>
                <div>
                  <h4 class="text-bold offset-top-20"><?php echo Yii::t('app', '<span style="color:#66cc33">Равномерное распределение участников</span>');?></h4>
                  
                </div>
      </div>
      <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-verified"></span>
                <div>
                  <h4 class="text-bold offset-top-20"><?php echo Yii::t('app', '<span style="color:#66cc33">Выгодная партнерская программа</span>');?></h4>
                  
                </div>
      </div>
      <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-lg icon-default icon-filled mdi mdi-video"></span>
                <div>
                  <h4 class="text-bold offset-top-20"><?php echo Yii::t('app', '<span style="color:#66cc33">Бесплатный доступ к платным Информационным Продуктам</span>');?></h4>
                  
                </div>
      </div>
    </div>
    <hr class="bg-lightest offset-top-66">
    <div class="range range-xs-middle text-md-left">
      <div class="cell-md-6"><img src="http://boston.workbar.com/wp-content/uploads/sites/2/2010/06/coworking-solution.jpeg" width="570" height="368" alt="" class="img-responsive center-block"></div>
      <div class="cell-md-6">
        <h1><?php echo Yii::t('app', 'Партнерская программа');?></h1>
        <hr class="divider bg-mantis hr-md-left-0">
        <?php echo Yii::t('app', '



<p style="text-align: justify;">В системе &laquo;Bit.info&raquo;&nbsp;<span style="color:#66cc33"><strong>НЕТ</strong></span>&nbsp;обязательных приглашений. Все партнеры и клоны их аккаунтов распределяются в матрице равномерно по определенному алгоритму.</p>

<p style="text-align: justify;">НО приглашать в систему &laquo;Bit.info&raquo;&nbsp;<span style="color:#66cc33"><strong>ОЧЕНЬ ВЫГОДНО!</strong></span></p>

<p style="text-align: justify;">За каждого лично приглашенного партнера Вы получаете 10 $. И получаете их многократно, так как Вы будете получать по 10 $ и за каждый клон Вашего партнера.</p>

<p style="text-align: justify;">Пример:</p>

<p style="text-align: justify;">Вы пригласили одного партнера и сразу заработали 10 $. У Вашего партнера закрылась матрица и открылись два клона, и Вы сразу же получаете еще 20 $. Затем Вы получите 40 $, затем 80 $, затем 160 $, затем 320 $ и так далее. Вы будете получать по 10 $ за каждый новый клон Вашего партнера снова и снова.</p>

<p style="text-align: justify;">Поэтому, даже пригласив всего одного партнера, Вы будете иметь постоянную прибыль. А представьте себе, что Вы построите сеть из 2, 3, 5, или, например 10 подписчиков?</p>

<p style="text-align: justify;"><span style="color:#66cc33"><strong>ВАШ ДОХОД БУДЕТ РАСТИ С КАЖДЫМ ДНЕМ!</strong></span></p>

<p style="text-align: justify;">Поэтому&nbsp;в &laquo;Bit.info&raquo; заниматься построением собственной сети очень выгодно!</p>

<p style="text-align: justify;">Благодаря информации полученной в нашей системе &laquo;Bit.info&raquo; и ее уникальному маркетингу, а также особенностям партнерской программы, Вы легко соберете доходный и сбалансированный инвестиционный портфель из криптовалют, разовьете свой бизнес и выйдите на постоянный, пассивный доход!</p>
');?><br/><a href="/signup" class="btn btn-primary offset-top-14"><?php echo Yii::t('app', 'Присоединиться');?></a>
      </div>
    </div>
  </div>
</section>
<section class="section-98 section-sm-110">
  <div class="shell">
    <h1><?php echo Yii::t('app', 'Выберите план');?></h1>
    <div class="divider bg-mantis"></div>
    <div class="range range-xs-bottom offset-top-41 range-sm-center">
      <div class="cell-md-3">
                <!-- Planning Box type 4-->
                <div class="box-planning box-planning-type-4">
                  <div class="box-planning-header context-dark bg-mantis">
                    <div class="box-planning-label">
                      <p class="big text-bold">Most Popular</p>
                    </div>
                    <h3>Free</h3>
                    <p class="h2 plan-price"><sup class="big">$</sup>59<sub class="text-white">/month</sub>
                    </p>
                  </div>
                  <div class="box-planning-body">
                    <ul class="list-separated list-unstyled">
                      <li><span class="text-bold">1GB</span><span class="text-dark">Space amount</span></li>
                      <li><span class="text-bold">5</span><span class="text-dark">users</span></li>
                      <li><span class="text-bold">5GB</span><span class="text-dark">Bandwidth</span></li>
                      <li><span class="text-dark">No Support</span></li>
                      <li><span class="text-bold">1</span><span class="text-dark">Databases</span></li>
                    </ul><a href="/signup" class="btn btn-block btn-default btn-rect">Sign up</a>
                  </div>
                </div>
      </div>
      <div class="cell-md-3">
                <!-- Planning Box type 4-->
                <div class="box-planning box-planning-type-4">
                  <div class="box-planning-header context-dark bg-malibu">
                    <div class="box-planning-label">
                      <p class="big text-bold">Most Popular</p>
                    </div>
                    <h3>Starter</h3>
                    <p class="h2 plan-price"><sup class="big">$</sup>59<sub class="text-white">/month</sub>
                    </p>
                  </div>
                  <div class="box-planning-body">
                    <ul class="list-separated list-unstyled">
                      <li><span class="text-bold">5GB</span><span class="text-dark">Space amount</span></li>
                      <li><span class="text-bold">20</span><span class="text-dark">users</span></li>
                      <li><span class="text-bold">10GB</span><span class="text-dark">Bandwidth</span></li>
                      <li><span class="text-dark">No Support</span></li>
                      <li><span class="text-bold">1</span><span class="text-dark">Databases</span></li>
                    </ul><a href="/signup" class="btn btn-block btn-default btn-rect">Sign up</a>
                  </div>
                </div>
      </div>
      <div class="cell-md-3">
                <!-- Planning Box type 4-->
                <div class="box-planning box-planning-type-4 active">
                  <div class="box-planning-header context-dark bg-saffron">
                    <div class="box-planning-label">
                      <p class="big text-bold">Most Popular</p>
                    </div>
                    <h3>Business</h3>
                    <p class="h2 plan-price"><sup class="big">$</sup>59<sub class="text-white">/month</sub>
                    </p>
                  </div>
                  <div class="box-planning-body">
                    <ul class="list-separated list-unstyled">
                      <li><span class="text-bold">10GB</span><span class="text-dark">Space amount</span></li>
                      <li><span class="text-bold">Unlimited</span><span class="text-dark">users</span></li>
                      <li><span class="text-bold">30GB</span><span class="text-dark">Bandwidth</span></li>
                      <li><span class="text-dark">Free Support</span></li>
                      <li><span class="text-bold">20</span><span class="text-dark">Databases</span></li>
                    </ul><a href="/signup" class="btn btn-block btn-default btn-rect">Sign up</a>
                  </div>
                </div>
      </div>
      <div class="cell-md-3">
                <!-- Planning Box type 4-->
                <div class="box-planning box-planning-type-4">
                  <div class="box-planning-header context-dark bg-red">
                    <div class="box-planning-label">
                      <p class="big text-bold">Most Popular</p>
                    </div>
                    <h3>Ultimate</h3>
                    <p class="h2 plan-price"><sup class="big">$</sup>59<sub class="text-white">/month</sub>
                    </p>
                  </div>
                  <div class="box-planning-body">
                    <ul class="list-separated list-unstyled">
                      <li><span class="text-bold">100GB</span><span class="text-dark">Space amount</span></li>
                      <li><span class="text-bold">Unlimited</span><span class="text-dark">users</span></li>
                      <li><span class="text-bold">60GB</span><span class="text-dark">Bandwidth</span></li>
                      <li><span class="text-dark">Free support</span></li>
                      <li><span class="text-bold">Unlimited</span><span class="text-dark">Databases</span></li>
                    </ul><a href="/signup" class="btn btn-block btn-default btn-rect">Sign up</a>
                  </div>
                </div>
      </div>
    </div>
  </div>
</section>
<!-- Buy Now-->
<section>
          <!-- Call to action type 2-->
          <section class="section-66 bg-blue-gray context-dark">
            <div class="shell">
              <div class="range range-xs-middle range-condensed">
                <div class="cell-md-8 cell-lg-9 text-center text-md-left">
                  <h2><span class="big"><?php echo Yii::t('app', 'Начните зарабатывать прямо сейчас!');?></span></h2>
                </div>
                <div class="cell-md-4 cell-lg-3 offset-top-41 offset-md-top-0"><a href="/signup" class="btn btn-icon btn-lg btn-default btn-anis-effect btn-icon-left"><span class="icon mdi mdi-cart-outline"></span><?php echo Yii::t('app', 'Присоединиться');?></a>
                </div>
              </div>
            </div>
          </section>
</section>
</main>