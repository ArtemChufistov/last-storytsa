<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\finance\models\Currency;
$this->title = Yii::t('app', 'Coming soon');
?>

<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register" style = "overflow-y: scroll;">
  <div class="login-box" style = "width: 620px;">
    <div class="white-box">
      <form class="form-horizontal form-material">
        
        <div class="form-group">
          <div class="col-xs-12 text-center">
            <div class="user-thumb text-center">
              <h3><?php echo $this->title;?></h3>
              <p><?php echo Yii::t('app', 'IC course coming soon');?></p>
            </div>
            <br/>
            <a href="/office/cryptodeposit" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40"><?php echo Yii::t('app', 'Go to previos page');?></a>
          </div>
        </div>

      </form>
    </div>
  </div>
</section>