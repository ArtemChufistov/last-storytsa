<?php

use app\modules\profile\components\AuthChoice;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Регистрация');

?>
<?php if (!empty(Yii::$app->session->getFlash('error'))): ?>
    <?php echo $this->render('partial/_error', ['error' => Yii::$app->session->getFlash('error')]); ?>
<?php endif;?>

<style type="text/css">
    .input-group{
        margin-top: 25px;
    }
    #my-captcha-image{
        cursor: pointer;
    }
</style>
<div class="center-vertical">
    <div class="center-content row">
        <div class="col-lg-3  center-margin">
            <div class="content-box wow bounceInDown modal-content">
                <h3 class="content-box-header content-box-header-alt bg-default">
                    <span class="icon-separator">
                        <i class="glyph-icon icon-user"></i>
                    </span>
                    <span class="header-wrapper">
                        <?php echo Yii::t('app', 'Регистрация'); ?>
                        <small><?php echo Yii::t('app', 'Укажите информацию о себе'); ?></small>
                    </span>
                </h3>
                <div class="content-box-wrapper">

                    <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "{input}\n{hint}\n{error}"]]);?>

                    <?=$form->field($model, 'login', ['template' => '<div class="input-group">{input}<span class="input-group-addon bg-blue"><i class="glyph-icon icon-star"></i></span></div>{error}'])->textInput([
	'maxlength' => true,
	'placeholder' => $model->getAttributeLabel('login'),
]);?>

                    <?=$form->field($model, 'email', ['template' => '<div class="input-group">{input}<span class="input-group-addon bg-blue"><i class="glyph-icon icon-envelope-o"></i></span></div>{error}'])->textInput([
	'maxlength' => true,
	'placeholder' => $model->getAttributeLabel('email'),
]);?>

                    <?=$form->field($model, 'password', ['template' => '<div class="input-group">{input}<span class="input-group-addon bg-blue"><i class="glyph-icon icon-unlock-alt"></i></span></div>{error}'])->passwordInput([
	'maxlength' => true,
	'placeholder' => $model->getAttributeLabel('password'),
]);?>

                    <?=$form->field($model, 'passwordRepeat', ['template' => '<div class="input-group">{input}<span class="input-group-addon bg-blue"><i class="glyph-icon icon-unlock-alt"></i></span></div>{error}'])->passwordInput([
	'maxlength' => true,
	'placeholder' => $model->getAttributeLabel('passwordRepeat'),
]);?>

                    <div class="form-group">
                        <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
	'imageOptions' => [
		'id' => 'my-captcha-image',
	],
	'captchaAction' => '/profile/profile/captcha',
	'options' => [
		'class' => 'form-control',
		'placeholder' => $model->getAttributeLabel('captcha'),
	],
	'template' => '<div class="row">
                            <div class="col-lg-5">{input}</div>
                            <div class="col-lg-4">{image}</div>
                            </div>',
]);
?>
                    </div>

                    <?php $labelLicense = '<span style = "font-size:12px;">' . Yii::t('app', 'Я согласен с условиями') . ' ' . Html::a(Yii::t('app', 'лицензионного соглашения') . '</span>', ['#'], [
	'data-toggle' => 'modal',
	'data-target' => '.bs-example-modal-lg',
	'class' => 'text-picton-blue',
]) . '</span>';?>

                    <div class="checkbox checkbox-success">
                        <?=$form->field($model, 'licenseAgree', ['template' => '<label>{input}{error}</label>', 'options' => ['tag' => 'div']])->checkBox(['label' => $labelLicense, 'class' => 'custom-checkbox', 'id' => 'inlineCheckbox111']);?>
                    </div>

                    </br>

                    <div class="form-group">
                        <?=Html::submitButton('<i class="glyphicon glyphicon-user"></i> ' . Yii::t('app', 'Зарегистрироваться'), [
	'class' => 'btn btn-success btn-block',
])?>
                    </div>

                    <?php ActiveForm::end();?>

                    <div class="offset-top-28" style = "text-align: center;">
                        <div class="text-dark text-extra-small" style = "font-size:12px; margin-bottom: 15px;"><?=Yii::t('app', 'Зарегистрироваться с помощью социальных сетей')?>:</div>

                        <div class="text-center" style="text-align: center">
                            <?=AuthChoice::widget([
	'baseAuthUrl' => ['/profile/auth/index'],
	'clientCssClass' => 'col-xs-3',
])?>
                        </div>

                        <span class="text-dark text-extra-small" style = "font-size:12px;">
                            <?=Yii::t('app', 'Если регистрировались ранее, можете')?> <?=Html::a(Yii::t('app', 'войти на сайт'), ['/login'], ['class' => ['text-picton-blue'], 'style' => 'font-weight: bold; color: #55bbeb; '])?>,
                            <?=Yii::t('app', 'используя Логин или E-mail')?>.
                        </span>
                    </div>

                </div>
            </div>

            <?php echo $this->render('partial/_licenseAgreement'); ?>
        </div>
    </div>
</div>