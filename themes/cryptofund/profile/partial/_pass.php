<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
?>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= Yii::t('app', 'Восстановление пароля');?></h4>
            </div>
            <div class="modal-body">

                <?php if (Yii::$app->session->hasFlash('reset-success')):?>
                    <div class='text-center'><?php echo Yii::$app->session->getFlash('reset-success');?></div>
                <?php else: ?>

                    <?php $form = ActiveForm::begin([
                        'id' => 'pass-form',
                        'fieldConfig' => [
                            'template' => "{input}\n{hint}\n{error}"
                        ],
                    ]);

                    echo $form->field($model, 'email')->textInput([
                        'maxlength' => true,
                        'placeholder' => $model->getAttributeLabel('email')
                    ]);

                    echo $form->field($model, 'password')->passwordInput([
                        'maxlength' => true,
                        'placeholder' => $model->getAttributeLabel('password')
                    ]);

                    echo $form->field($model, 'captcha')->widget(Captcha::className(), [
                        'captchaAction' => '/profile/profile/captcha',
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => $model->getAttributeLabel('captcha')
                        ],
                        'template' => '<div class="row">
                                <div class="col-lg-8">{input}</div>
                                <div class="col-lg-4">{image}</div>
                                </div>',
                    ]);
                    ?>

                    <p class="hint-block">
                        <?= Yii::t('app', 'Ссылка с активацией нового пароля будет отправлена на Email, указанный при регистрации') ?>.
                    </p>

                    <div class="form-group text-center">
                        <?= Html::submitButton('<i class="glyphicon glyphicon-refresh"></i> ' . Yii::t('app', 'Сбросить пароль'), [
                          'class' => 'btn btn-sm btn-icon btn-block btn-primary offset-top-20',
                          'name' => 'login-button']);
                        ?>
                    </div>

                <?php ActiveForm::end();?>
                <?php endif;?>

            </div>
        </div>
    </div>
</div>

<?php if (Yii::$app->session->hasFlash('reset-success')|| $model->hasErrors()) {
    $this->registerJs('$(".bs-example-modal-lg").modal("show")');
}
