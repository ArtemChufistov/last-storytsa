<?php

use app\modules\profile\components\AuthChoice;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Вход на сайт');

?>
<?php if (!empty(Yii::$app->session->getFlash('error'))): ?>
    <?php echo $this->render('partial/_error', ['error' => Yii::$app->session->getFlash('error')]); ?>
<?php endif;?>

<div class="center-vertical">
  <div class="center-content row">
    <div class="col-lg-3  center-margin">
      <div class="content-box wow bounceInDown modal-content">
        <h3 class="content-box-header content-box-header-alt bg-default">
            <span class="icon-separator">
                <i class="glyph-icon icon-key"></i>
            </span>
            <span class="header-wrapper">
                <?php echo Yii::t('app', 'Личный кабинет'); ?>
                <small><?php echo Yii::t('app', 'Введите свой логин и пароль'); ?></small>
            </span>
        </h3>
        <div class="content-box-wrapper">

          <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "{input}\n{hint}\n{error}"]]);?>

          <?=$form->field($model, 'loginOrEmail', ['template' => '<div class="input-group">{input}<span class="input-group-addon bg-blue"><i class="glyph-icon icon-envelope-o"></i></span></div>{error}'])->textInput([
	'maxlength' => true,
	'placeholder' => $model->getAttributeLabel('loginOrEmail'),
]);?>

          <?=$form->field($model, 'password', ['template' => '<div class="input-group">{input}<span class="input-group-addon bg-blue"><i class="glyph-icon icon-unlock-alt"></i></span></div>{error}'])->passwordInput([
	'maxlength' => true,
	'placeholder' => $model->getAttributeLabel('password'),
]);?>

          <div class="form-group-sign">
            <?php $labelRememberMe = '<span class="checkbox-custom-dummy"></span><span class="text-dark text-extra-small">' . Yii::t('app', 'Запомнить меня') . '</span>';?>

            <div class="checkbox checkbox-success">
              <?=$form->field($model, 'rememberMe', ['template' => '<label class="checkbox-inline">{input}{error}</label>', 'options' => ['tag' => 'div']])->checkBox(['label' => $labelRememberMe, 'id' => 'inlineCheckbox111', 'class' => 'custom-checkbox']);?>
            </div>
          </div>

          <div class="form-group-sign">
              <?=Html::submitButton('<i class="glyphicon glyphicon-log-in"></i> ' . Yii::t('user', 'Войти'), [
	'class' => 'btn btn-success btn-block',
	'name' => 'login-button'])?>
          </div>

          <?php ActiveForm::end();?>

          <div class="offset-top-30 text-dark text-extra-small" style="text-align: center; margin-top: 15px;">
            <?=Yii::t('user', 'Если')?> <?=Html::a(Yii::t('user', 'регистрировались'), ['/signup'], ['class' => 'text-picton-blue', 'style' => 'font-weight: bold; color: #55bbeb;'])?> <?=Yii::t('user', 'ранее, но забыли пароль, нажмите')?>
            <?=Html::a(Yii::t('user', 'восстановить пароль'), ['#'], [
	'data-toggle' => 'modal',
	'data-target' => '.bs-example-modal-lg',
	'class' => 'text-picton-blue',
	'style' => 'font-weight: bold; color: #55bbeb;',
])?>.
          </div>

          <p class="text-extra-small text-dark offset-top-4" style="text-align: center; margin-top: 15px;margin-bottom: 10px;"><?=Yii::t('user', 'Войти с помощью социальных сетей')?>:</p>

          <div style="text-align: center">
              <?=AuthChoice::widget([
	'baseAuthUrl' => ['/profile/auth/index'],
	'clientCssClass' => 'col-xs-3',
])?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php echo $this->render('partial/_pass', ['model' => $forget]); ?>