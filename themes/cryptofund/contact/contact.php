<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\web\View;

$this->title = Yii::t('app', 'Связаться с нами');

$this->params['breadcrumbs']= [ [
    'label' => $this->title,
    'url' => '/contact'
  ],
]

?>

<?php if(Yii::$app->session->hasFlash('success')): ?>

	<div class="successModal modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><?= Yii::t('app', 'Ваше сообщение принято');?></h4>
	      </div>
	      <div class="modal-body">
	        <p><?= Yii::t('app', 'Спасибо, что оставили нам сообщение, мы обязательно свяжемся с вам в ближайшее время');?></p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app', 'Закрыть');?></button>
	        <button type="button" class="btn btn-primary" data-dismiss="modal"><?= Yii::t('app', 'Ok');?></button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<?php echo $this->registerJs("$('.successModal').modal('show');", View::POS_END);?>

<?php endif;?>

<div class="hero-box hero-box-smaller bg-gradient-5 font-inverse">
    <div class="container" style = "margin-top: 170px;">
        <h1 class="pad0A hero-heading font-size-28 wow fadeInDown" data-wow-duration="0.6s"><?php echo $this->title;?></h1>
    </div>
    <div class="hero-overlay bg-black"></div>
</div>

<div class="mrg25T pad25T pad25B">
    <div class="container mrg25T mrg25B row">
        <div class="col-md-8">
            <form class="form-horizontal bordered-row" id="demo-form" data-parsley-validate="">
                <div class="content-box">
                    <h3 class="content-box-header bg-default">
                        Send us an email
                    </h3>
                    <div class="content-box-wrapper">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Name:</label>
                                <div class="col-sm-6">
                                    <input type="text" placeholder="Required Field" required class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email:</label>
                                <div class="col-sm-6">
                                    <input type="text" data-parsley-type="email" placeholder="Email address" required class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Website:</label>
                                <div class="col-sm-6">
                                    <input type="text" data-parsley-type="url" placeholder="URL address" required class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Message:</label>
                                <div class="col-sm-6">
                                    <textarea id="" name="" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"></label>
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" required name="terms">
                                            Accept Terms &amp; Conditions
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-pane">
                        <button class="btn btn-info">Send</button>
                        <button class="btn btn-link font-gray-dark">Cancel</button>
                    </div>
                </div>
            </form>

        </div>
        <div class="col-md-4">
            <div class="row mrg15T">
                <div class="col-md-4">
                    <a href="#" title="Follow us on Facebook" class="btn btn-block tooltip-button bg-facebook">
                        <i class="glyph-icon icon-facebook"></i>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#" title="Follow us on Twitter" class="btn btn-block tooltip-button bg-twitter">
                        <i class="glyph-icon icon-twitter"></i>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#" title="Follow us on Google+" class="btn btn-block tooltip-button bg-google">
                        <i class="glyph-icon icon-google-plus"></i>
                    </a>
                </div>
            </div>
            <div class="divider mrg25T mrg25B"></div>
            <h3>Get in touch</h3>
            <ul class="contact-list mrg15T mrg25B reset-ul">
                <li>
                    <i class="glyph-icon icon-home"></i>
                    5804 Quaking Embers Trail, Tiger, Missouri
                </li>
                <li>
                    <i class="glyph-icon icon-phone"></i>
                    (636) 517-1243
                </li>
                <li>
                    <i class="glyph-icon icon-envelope-o"></i>
                    <a href="#" title="">homepage@example.com</a>
                </li>
            </ul>
            <div class="divider mrg25T mrg25B"></div>
            <h3 class="mrg15B">About us</h3>
            <p class="font-gray-dark">For science, music, sport. The languages only differ in their grammar, their pronunciation and their most common words.</p>
        </div>
    </div>
</div>
<div class="main-footer bg-gradient-4 clearfix">
    <div class="container clearfix">
        <div class="col-md-3 pad25R">
            <div class="header">About us</div>
            <p class="about-us">
                sollicitudin eu erat. Pellentesque ornare mi vitae sem consequat ac bibendum neque adipiscing.
            </p>
            <div class="divider"></div>
            <div class="header">Footer background</div>
            <div class="theme-color-wrapper clearfix">
                <h5>Solids</h5>
                <a class="tooltip-button set-footer-style bg-primary" data-header-bg="bg-primary" title="" href="#" data-original-title="Primary">Primary</a>
                <a class="tooltip-button set-footer-style bg-green" data-header-bg="bg-green" title="" href="#" data-original-title="Green">Green</a>
                <a class="tooltip-button set-footer-style bg-red" data-header-bg="bg-red" title="" href="#" data-original-title="Red">Red</a>
                <a class="tooltip-button set-footer-style bg-blue" data-header-bg="bg-blue" title="" href="#" data-original-title="Blue">Blue</a>
                <a class="tooltip-button set-footer-style bg-warning" data-header-bg="bg-warning" title="" href="#" data-original-title="Warning">Warning</a>
                <a class="tooltip-button set-footer-style bg-purple" data-header-bg="bg-purple" title="" href="#" data-original-title="Purple">Purple</a>
                <a class="tooltip-button set-footer-style bg-black" data-header-bg="bg-black" title="" href="#" data-original-title="Black">Black</a>

                <div class="clear"></div>

                <h5 class="mrg15T">Gradients</h5>
                <a class="tooltip-button set-footer-style bg-gradient-1" data-header-bg="bg-gradient-1" title="" href="#" data-original-title="Gradient 1">Gradient 1</a>
                <a class="tooltip-button set-footer-style bg-gradient-2" data-header-bg="bg-gradient-2" title="" href="#" data-original-title="Gradient 2">Gradient 2</a>
                <a class="tooltip-button set-footer-style bg-gradient-3" data-header-bg="bg-gradient-3" title="" href="#" data-original-title="Gradient 3">Gradient 3</a>
                <a class="tooltip-button set-footer-style bg-gradient-4" data-header-bg="bg-gradient-4" title="" href="#" data-original-title="Gradient 4">Gradient 4</a>
                <a class="tooltip-button set-footer-style bg-gradient-5" data-header-bg="bg-gradient-5" title="" href="#" data-original-title="Gradient 5">Gradient 5</a>
                <a class="tooltip-button set-footer-style bg-gradient-6" data-header-bg="bg-gradient-6" title="" href="#" data-original-title="Gradient 6">Gradient 6</a>
                <a class="tooltip-button set-footer-style bg-gradient-7" data-header-bg="bg-gradient-7" title="" href="#" data-original-title="Gradient 7">Gradient 7</a>
                <a class="tooltip-button set-footer-style bg-gradient-8" data-header-bg="bg-gradient-8" title="" href="#" data-original-title="Gradient 8">Gradient 8</a>
            </div>
        </div>
        <div class="col-md-4">
            <h3 class="header">Recent posts</h3>
            <div class="posts-list">
                <ul>
                    <li>
                        <div class="post-image">
                            <a href="/cryptofund/image-resources/stock-images/img-10.jpg" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="Blog post title">
                                <img class="img-responsive" src="/cryptofund/image-resources/stock-images/img-10.jpg" alt="">
                            </a>
                        </div>
                        <div class="post-body">
                            <a class="post-title" href="blog-single.html" title="">
                                <h3>When our power of choice is untrammelled prevents</h3>
                            </a>
                            by <a href="#">Hector Tomales</a> on 16.04.2015
                        </div>
                    </li>
                    <li>
                        <div class="post-image">
                            <a href="/cryptofund/image-resources/stock-images/img-11.jpg" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="Blog post title">
                                <img class="img-responsive" src="/cryptofund/image-resources/stock-images/img-11.jpg" alt="">
                            </a>
                        </div>
                        <div class="post-body">
                            <a class="post-title" href="blog-single.html" title="">
                                <h3>And when nothing prevents our being able</h3>
                            </a>
                            by <a href="#">Hector Tomales</a> on 16.04.2015
                        </div>
                    </li>
                    <li>
                        <div class="post-image">
                            <a href="/cryptofund/image-resources/stock-images/img-12.jpg" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="Blog post title">
                                <img class="img-responsive" src="/cryptofund/image-resources/stock-images/img-12.jpg" alt="">
                            </a>
                        </div>
                        <div class="post-body">
                            <a class="post-title" href="blog-single.html" title="">
                                <h3>When our power of choice is untrammelled</h3>
                            </a>
                            by <a href="#">Hector Tomales</a> on 16.04.2015
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <h3 class="header">Components</h3>
            <ul class="footer-nav">
                <li><a href="hero-static.html" title="Static hero sections"><span>Static sections</span></a></li>
                <li><a href="hero-alignments.html" title="Hero alignments"><span>Hero alignments</span></a></li>
                <li><a href="hero-overlays.html" title="Hero overlays"><span>Hero overlays</span></a></li>
                <li><a href="hero-video.html" title="Hero with video backgrounds"><span>Video sections</span></a></li>
                <li><a href="hero-elements.html" title="Hero sections with elements"><span>Hero elements</span></a></li>
                <li><a href="hero-parallax.html" title="Hero with parallax backgrounds"><span>Parallax sections</span></a></li>
                <li><a href="portfolio-3col.html" title="Portfolio with 3 columns"><span>Portfolio 3 columns</span></a></li>
                <li><a href="contact-us.html" title="Contact us"><span>Contact us</span></a></li>
                <li><a href="features-box.html" title="Features boxes"><span>Features boxes</span></a></li>
            </ul>
        </div>
        <div class="col-md-3">
            <h3 class="header">Photo Gallery</h3>
            <div class="row no-gutter">
                <div class="col-xs-4">
                    <a href="/cryptofund/image-resources/stock-images/img-20.jpg" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="Blog post title">
                        <img class="img-responsive" src="/cryptofund/image-resources/stock-images/img-20.jpg" alt="">
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="/cryptofund/image-resources/stock-images/img-19.jpg" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="Blog post title">
                        <img class="img-responsive" src="/cryptofund/image-resources/stock-images/img-19.jpg" alt="">
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="/cryptofund/image-resources/stock-images/img-18.jpg" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="Blog post title">
                        <img class="img-responsive" src="/cryptofund/image-resources/stock-images/img-18.jpg" alt="">
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="/cryptofund/image-resources/stock-images/img-17.jpg" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="Blog post title">
                        <img class="img-responsive" src="/cryptofund/image-resources/stock-images/img-17.jpg" alt="">
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="/cryptofund/image-resources/stock-images/img-16.jpg" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="Blog post title">
                        <img class="img-responsive" src="/cryptofund/image-resources/stock-images/img-16.jpg" alt="">
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="/cryptofund/image-resources/stock-images/img-15.jpg" class="prettyphoto" rel="prettyPhoto[pp_gal]" title="Blog post title">
                        <img class="img-responsive" src="/cryptofund/image-resources/stock-images/img-15.jpg" alt="">
                    </a>
                </div>
            </div>
            <h3 class="header">Contact us</h3>
            <ul class="footer-contact">
                <li>
                    <i class="glyph-icon icon-home"></i>
                    5804 Quaking Embers Trail, Tiger, Missouri
                </li>
                <li>
                    <i class="glyph-icon icon-phone"></i>
                    (636) 517-1243
                </li>
                <li>
                    <i class="glyph-icon icon-envelope-o"></i>
                    <a href="#" title="">homepage@example.com</a>
                </li>
            </ul>
        </div>
    </div>
