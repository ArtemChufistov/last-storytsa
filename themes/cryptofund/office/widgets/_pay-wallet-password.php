<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(['options' => ['class' => ['form-horizontal bordered-row']]]);?>
  <div class="form-group">
    <div class="col-sm-3">
    </div>
    <div class="col-sm-4">
      	<?=Html::submitButton('<i class="glyphicon glyphicon-send"></i> ' . Yii::t('app', 'Получить платёжный пароль'), [
	'class' => 'btn btn-md btn-post float-left btn-success',
	'value' => 1,
	'name' => 'send-pay-wallet'])?>
      <?php if (!empty($message)): ?>
        </br>
        </br>
        <strong class="help-block col-md-12"><?php echo $message; ?></strong>
      <?php endif;?>
    </div>
  </div>
<?php ActiveForm::end();?>
