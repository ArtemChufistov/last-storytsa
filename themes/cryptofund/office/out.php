<?php
use app\modules\finance\models\Payment;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>

<div id="page-content-wrapper">
  <div id="page-content">
    <div class="container">
      <div id="page-title">
          <h2><?php echo $this->title; ?></h2>
          <p><?php echo Yii::t('app', 'Здесь вы можете обменять <strong>CITT</strong>'); ?></p>
      </div>
      <?php echo Yii::$app->controller->renderPartial(
	'@app/themes/' . Yii::$app->params['theme'] . '/office/partial/out/_' . strtolower(\yii\helpers\StringHelper::basename(get_class($paymentForm))),
	['paymentForm' => $paymentForm, 'user' => $user, 'currencyArray' => $currencyArray]); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-body">
    <?php if (!empty($paymentProvider->getModels())): ?>
      <div class="box">
      <?php Pjax::begin();?>

        <?=GridView::widget([
	'dataProvider' => $paymentProvider,
	'filterModel' => $payment,
	'id' => 'payment-grid',
	'layout' => '
      <div class="box-header">
        <h3 class="box-title" style = "float: left;">' . Yii::t('app', 'Обрабатываемые платежи') . '</h3>
        <div class="box-tools"><div class="page-success pagination-sm no-margin pull-right">{pager}</div></div>
      </div>
     </div><div class="box-body no-padding eventTable">{items}</div>',
	'columns' => [['class' => 'yii\grid\SerialColumn'],
		[
			'attribute' => 'to_sum',
			'label' => Yii::t('app', 'Списываемая сумма'),
			'format' => 'raw',
			'filter' => true,
			'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_payment-from-sum', ['model' => $model]);},
		], [
			'attribute' => 'realSum',
			'label' => Yii::t('app', 'Отправляемая сумма'),
			'format' => 'raw',
			'filter' => true,
			'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_payment-to-sum', ['model' => $model]);},
		], [
			'attribute' => 'status',
			'format' => 'raw',
			'filter' => true,
			'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_payment-status', ['model' => $model]);},
			'filter' => Select2::widget([
				'name' => 'PaymentFinanceSearch[status]',
				'data' => Payment::getStatusArray(),
				'theme' => Select2::THEME_BOOTSTRAP,
				'hideSearch' => true,
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите статус'),
					'value' => isset($_GET['PaymentFinanceSearch[type]']) ? $_GET['PaymentFinanceSearch[type]'] : null,
				],
			]),
		], [
			'attribute' => 'wallet',
			'format' => 'raw',
			'filter' => true,
			'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_payment-wallet', ['model' => $model]);},
		], [
			'attribute' => 'date_add',
			'format' => 'raw',
			'filter' => true,
			'filter' => \yii\jui\DatePicker::widget([
				'model' => $payment,
				'attribute' => 'date_add',
				'options' => ['class' => 'form-control'],
				'language' => 'ru',
				'dateFormat' => 'dd-MM-yyyy',
			]),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/grid/_payment-date-add', ['model' => $model]);},
		],
	],
]);?>
      <?php Pjax::end();?>

    <?php endif;?>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>