<?php
use yii\helpers\Url;

$this->title = Yii::t('user', 'E-mail подтверждён');
?>

<div class="center-vertical">
  <div class="center-content row">
    <div class="col-lg-3  center-margin">
      <div class="content-box wow bounceInDown modal-content">
        <h3 class="content-box-header content-box-header-alt bg-default">
            <span class="icon-separator">
                <i class="glyph-icon icon-play"></i>
            </span>
            <span class="header-wrapper">
                <?php echo Yii::t('app', 'Поздравляем вы успешно подтвердили свой <strong>E-mail</strong>');?>
            </span>
        </h3>
        <div class="content-box-wrapper" style = "text-align: center;">
          <p><?php echo Yii::t('app', 'Теперь Вам доступен полный функционал личного кабинета');?></p>
          </br>
          <a href = "<?php echo Url::to('/profile/office/dashboard');?>" style = "font-weight:bold; text-decoration: underline;">
            <?php echo Yii::t('app', 'Перейти в личный кабинет');?>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>