<?php
use app\assets\cryptofund\FrontAppAssetBottom;
use app\assets\cryptofund\FrontAppAssetTop;
use app\modules\menu\widgets\MenuWidget;
use yii\helpers\Html;

FrontAppAssetTop::register($this);
FrontAppAssetBottom::register($this);

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <?=Html::csrfMetaTags()?>
        <title><?=Html::encode($this->title)?></title>
        <?php $this->head()?>

        <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_head_meta'); ?>
    </head>

    <body class="body-alt">
      <?php $this->beginBody()?>

        <div id="loading">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
        <div id="page-wrapper">
            <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']); ?>

            <?=$content?>

            <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_footer'); ?>
        </div>

      <?php $this->endBody()?>
    </body>
</html>
<?php $this->endPage()?>