<?php

$this->params['breadcrumbs'] = [[
	'label' => $message,
	'url' => '/404',
],
];

$this->title = $message;
?>


<div class="hero-box hero-box-smaller poly-bg-2 font-inverse" data-top-bottom="background-position: 50% 0px;" data-bottom-top="background-position: 50% -600px;">
    <div class="container" style = "margin-top: 170px;">
        <h1 class="hero-heading wow fadeInDown" data-wow-duration="0.6s"><?php echo $this->title; ?></h1>
        <p class="hero-text wow bounceInUp" data-wow-duration="0.9s" data-wow-delay="0.2s"><?php echo Yii::t('app', 'Ошибка'); ?></p>
    </div>
    <div class="hero-overlay bg-black"></div>
</div>

<style type="text/css">
    html,body {
        height: 100%;
    }
    body {
        background: #fff;
        overflow: hidden;
    }

</style>

<script type="text/javascript" src="../../assets/widgets/wow/wow.js"></script>
<script type="text/javascript">
    /* WOW animations */

    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
</script>

<div class="container features-tour-box" style = "margin-bottom:230px;">
  <div class="row">
    <div class="col-md-12">
      <img src="/cryptofund/image-resources/blurred-bg/blurred-bg-7.jpg" class="login-img wow fadeIn" alt="">

      <div class="center-vertical">
          <div class="center-content row">

              <div class="col-md-6 center-margin">
                  <div class="server-message wow bounceInDown inverse">
                      <div style = "margin-bottom: 20px;"><?=$message;?></div>
                      <a href = "/" class="btn btn-lg btn-success"><?php echo Yii::t('app', 'Вернуться на главную'); ?></a>
                  </div>
              </div>

          </div>
      </div>
    </div>
    <div class="mrg25B"></div>
  </div>
</div>
