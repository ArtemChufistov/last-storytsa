<meta name="description" content="Sitt.io - криптовалютный фонд">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/cryptofund/favicon_c.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/cryptofund/favicon_c.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/cryptofund/favicon_c.png">
<link rel="apple-touch-icon-precomposed" href="/cryptofund/favicon_c.png">
<link rel="shortcut icon" href="/cryptofund/favicon_c.png">

<script type="text/javascript">
    $(window).load(function(){
        setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
    });
</script>