<?php
use app\modules\finance\models\CryptoWallet;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyGraph;
//use yii;

$this->title = Yii::t('app', $model->title);
?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="fullpage">
  <div class="owl-slider-2 slider-wrapper">

      <div class="poly-bg-1 hero-box font-inverse hero-box-smaller">
          <div class="container clearfix" style = "margin-top: 180px;">
              <div class="col-md-4 img-holder wow fadeInUp">
                  <img src="/cryptofund/images/phone-4.png" alt="">
              </div>
              <div class="col-md-8">
                <h2 class="hero-heading wow fadeInDown" data-wow-duration="0.6s"><?=Yii::t('app', 'Управление цифровыми активами');?></h2>
                <p class="pad25T mrg25B hero-text wow bounceInUp" data-wow-duration="0.9s" data-wow-delay="0.2s"><?=Yii::t('app', 'CITT – CRYPTO INVESTMENT and TOKEN TRADING. Простой путь к сложной децентрализованной экономике.');?></p>
                <a href = "signup" class="mrg5R btn btn-alt btn-hover btn-lg btn-outline-inverse remove-bg">
                    <span><?=Yii::t('app', 'Присоединиться');?></span>
                    <i class="glyph-icon icon-arrow-right"></i>
                </a>
                <a href = "login" class="btn btn-alt btn-hover btn-lg btn-outline-inverse remove-bg">
                    <span><?=Yii::t('app', 'Личный кабинет');?></span>
                    <i class="glyph-icon icon-home"></i>
                </a>
              </div>
          </div>
      </div>

  </div>
  <style>
    #chart {
        min-width: 310px;
        height: 400px;
        margin: 0 auto
    }
  </style>
  <div style = "background: linear-gradient(to top, #2a2a2b, #2a2a2b);" class = "row">
      <div class="container" >
          <h3 class="hero-heading wow fadeInDown" data-wow-duration="0.6s" style = "text-align: center;margin-top: 20px;  color: #ccc; font-size: 22px;"><?=Yii::t('app', 'График курса <strong>CITT</strong>');?></h3>
          <div class="col-xs-12">
              <div id="chart" style = "margin-top: 20px;margin-bottom: 30px"></div>
          </div>
      </div>
  </div>
  <script>
  $( document ).ready(function() {

<?php
$currencyVoucher = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();
$graph = CurrencyGraph::find()
	->select(['*, DATE_FORMAT(date, "%Y-%m-%d") AS day'])
	->where(['type' => CurrencyGraph::TYPE_2_HOURE])
	->andWhere(['currency_id' => $currencyVoucher->id])
	->andWhere(['>=', 'date', '2017-05-01 00:00:00'])
	->orderBy(['date' => SORT_ASC])
	->groupBy('day')
	->all();?>

<?php

foreach ($graph as $item) {
	$xaxis[] = date('d-m', strtotime($item->date));
	$yaxis1dLast30Day[] = $item->value;
}?>

var data = [
<?php foreach ($graph as $item): ?>
 <?php echo '["' . date('d-m-Y', strtotime($item->date)) . '", ' . $item->value . '],'; ?>
<?php endforeach;?>
];
console.log(data);
        Highcharts.theme = {
           colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
              '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
           chart: {
              backgroundColor: '#2a2a2b',
              style: {
                 fontFamily: '\'Unica One\', sans-serif'
              },
              plotBorderColor: '#606063'
           },
           title: {
              style: {
                 color: '#E0E0E3',
                 textTransform: 'uppercase',
                 fontSize: '20px'
              }
           },
           subtitle: {
              style: {
                 color: '#E0E0E3',
                 textTransform: 'uppercase'
              }
           },
           xAxis: {
              gridLineColor: '#707073',
              labels: {
                 style: {
                    color: '#E0E0E3'
                 }
              },
              lineColor: '#707073',
              minorGridLineColor: '#505053',
              tickColor: '#707073',
              title: {
                 style: {
                    color: '#A0A0A3'

                 }
              }
           },
           yAxis: {
              gridLineColor: '#707073',
              labels: {
                 style: {
                    color: '#E0E0E3'
                 }
              },
              lineColor: '#707073',
              minorGridLineColor: '#505053',
              tickColor: '#707073',
              tickWidth: 1,
              title: {
                 style: {
                    color: '#A0A0A3'
                 }
              }
           },
           tooltip: {
              backgroundColor: 'rgba(0, 0, 0, 0.85)',
              style: {
                 color: '#F0F0F0'
              }
           },
           plotOptions: {
              series: {
                 dataLabels: {
                    color: '#B0B0B3'
                 },
                 marker: {
                    lineColor: '#333'
                 }
              },
              boxplot: {
                 fillColor: '#505053'
              },
              candlestick: {
                 lineColor: 'white'
              },
              errorbar: {
                 color: 'white'
              }
           },
           legend: {
              itemStyle: {
                 color: '#E0E0E3'
              },
              itemHoverStyle: {
                 color: '#FFF'
              },
              itemHiddenStyle: {
                 color: '#606063'
              }
           },
           credits: {
              style: {
                 color: '#666'
              }
           },
           labels: {
              style: {
                 color: '#707073'
              }
           },

           drilldown: {
              activeAxisLabelStyle: {
                 color: '#F0F0F3'
              },
              activeDataLabelStyle: {
                 color: '#F0F0F3'
              }
           },

           navigation: {
              buttonOptions: {
                 symbolStroke: '#DDDDDD',
                 theme: {
                    fill: '#505053'
                 }
              }
           },

           rangeSelector: {
              buttonTheme: {
                 fill: '#505053',
                 stroke: '#000000',
                 style: {
                    color: '#CCC'
                 },
                 states: {
                    hover: {
                       fill: '#707073',
                       stroke: '#000000',
                       style: {
                          color: 'white'
                       }
                    },
                    select: {
                       fill: '#000003',
                       stroke: '#000000',
                       style: {
                          color: 'white'
                       }
                    }
                 }
              },
              inputBoxBorderColor: '#505053',
              inputStyle: {
                 backgroundColor: '#333',
                 color: 'silver'
              },
              labelStyle: {
                 color: 'silver'
              }
           },

           navigator: {
              handles: {
                 backgroundColor: '#666',
                 borderColor: '#AAA'
              },
              outlineColor: '#CCC',
              maskFill: 'rgba(255,255,255,0.1)',
              series: {
                 color: '#7798BF',
                 lineColor: '#A6C7ED'
              },
              xAxis: {
                 gridLineColor: '#505053'
              }
           },

           scrollbar: {
              barBackgroundColor: '#808083',
              barBorderColor: '#808083',
              buttonArrowColor: '#CCC',
              buttonBackgroundColor: '#606063',
              buttonBorderColor: '#606063',
              rifleColor: '#FFF',
              trackBackgroundColor: '#404043',
              trackBorderColor: '#404043'
           },

           legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
           background2: '#505053',
           dataLabelsColor: '#B0B0B3',
           textColor: '#C0C0C0',
           contrastTextColor: '#F0F0F3',
           maskColor: 'rgba(255,255,255,0.3)'
        };

        Highcharts.setOptions(Highcharts.theme);

        Highcharts.chart('chart', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'График Курса CITT / USD'
            },
            subtitle: {
                text: ' '
            },
            xAxis: {
                categories: <?php echo json_encode($xaxis); ?>
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },
            series: [{
                type: 'area',
                name: 'Курс CITT / USD',
                data: data
            }]
        });

});
  </script>
  <div class="large-padding pad25B">
      <div class="container pad25B row">
          <div class="col-md-4">
              <div class="icon-box icon-box-offset-large bg-default content-box icon-boxed">
                  <i class="icon-large glyph-icon bg-white border-default btn-border icon-linecons-money wow bounceInDown" data-wow-delay="1s"></i>
                  <h3 class="text-transform-upr icon-title wow fadeInUp"><?=Yii::t('app', 'Высокая доходность');?></h3>
                  <p class="icon-content wow fadeInUp"><?=Yii::t('app', 'Благодаря росту капитализации мирового рынка цифровых активов - появилась возможность увеличить доходность инвестиций.');?></p>
              </div>
          </div>
          <div class="col-md-4">
              <div class="icon-box icon-box-offset-large bg-default content-box icon-boxed">
                  <i class="icon-large glyph-icon bg-white border-default btn-border icon-linecons-globe wow bounceInDown" data-wow-delay="1.5s"></i>
                  <h3 class="text-transform-upr icon-title wow fadeInUp"><?=Yii::t('app', 'Диверсификация');?></h3>
                  <p class="icon-content wow fadeInUp"><?=Yii::t('app', 'Благодаря сложному портфелю из различных цифровых активов, мы сократили риски и обеспечили высокий доход.');?></p>
              </div>
          </div>
          <div class="col-md-4">
              <div class="icon-box icon-box-offset-large bg-default content-box icon-boxed">
                  <i class="icon-large glyph-icon bg-white border-default btn-border icon-linecons-wallet wow bounceInDown" data-wow-delay="2s"></i>
                  <h3 class="text-transform-upr icon-title wow fadeInUp"><?=Yii::t('app', 'Безопасно и просто');?></h3>
                  <p class="icon-content wow fadeInUp"><?=Yii::t('app', 'Вам не нужно изучать сотни видов цифровых активов, достаточно просто купить токены фонда - прямо с вашей банковской карты.');?></p>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="cta-box-btn bg-yellow">
    <div class="container">
        <a href="/signup" class="btn btn-info" title="">
            <?=Yii::t('app', 'ПРИСОЕДИНИТЬСЯ');?>
            <span><?=Yii::t('app', 'получи СВОИ токены CITT уже СЕЙЧАС');?></span>
        </a>
    </div>
</div>
<div class="hero-box bg-black font-inverse">
    <div class="container">

        <h1 class="hero-heading wow fadeInDown" data-wow-duration="0.6s"><?=Yii::t('app', 'Как это работает?');?></h1>
        <p class="hero-text wow bounceInUp" data-wow-duration="0.9s" data-wow-delay="0.2s"><?=Yii::t('app', 'Мы размещаем фиатные средства в различные цифровые активы, в соответствии с рыночной обстановкой и аналитическими прогнозами, для достижения максимальной эффективности роста портфеля в целом. Таким образом формируется динамический инвестиционный портфель CITT. По мере удорожания стоимости цифровых активов фонда (стоимости портфеля CITT) по отношению к доллару США, увеличивается и стоимость токена CITT. Пользователь может купить и продать токены фонда по цене на текущий момент, цена рассчитывается автоматически. Момент в который пользователь может купить или продать токены фонда он выбирает самостоятельно.');?></p>
    </div>
    <div class="hero-video video-example-2"></div>
    <div class="hero-overlay bg-black opacity-30"></div>
</div>
<div class="poly-bg-9 hero-box hero-right font-inverse hero-box-smaller">
    <div class="container clearfix">
        <div class="col-md-7">
            <h3 class="hero-heading wow fadeInDown mrg20B" data-wow-duration="0.6s"><?=Yii::t('app', 'Почему CITT?');?></h3>
            <p class="hero-text wow bounceInUp mrg25B" data-wow-duration="0.9s" data-wow-delay="0.2s"><?=Yii::t('app', 'Фонд внедряет бизнес-модель, позволяющую инвесторам извлекать выгоду из развития рынков цифровых активов, избегая сложностей, рисков и технических препятствий. Покупка и безопасное хранение большого разнообразия цифровых активов может быть довольно сложной задачей. CITT решает эту задачу – все делается в несколько кликов в личном кабинете пользователя используя привычные инструменты.');?></p>
            <a href="/signup" class="btn-outline-inverse hero-btn wow fadeInUp" data-wow-delay="0.4s" title="Purchase Button"><?=Yii::t('app', 'Приобрести токены CITT');?></a>
        </div>
        <div class="col-md-5 img-holder wow fadeInUp">


<!-- Styles -->
<style>
#container {
    min-width: 310px;
    max-width: 800px;
    height: 400px;
    margin: 0 auto
}
.highcharts-button{
    display: none;
}
.highcharts-credits{
    display: none;
}
.highcharts-title{
    display: none;
}
.highcharts-text-outline{
    display: none;
}
</style>


<div id="container"></div>

<?php $wallets = CryptoWallet::find()->all();

$walletInfo = [];
foreach ($wallets as $wallet) {
	$y = $wallet->balance * $wallet->getCurrency()->one()->course_to_usd;
	$walletInfo[] = ['name' => $wallet->getCurrency()->one()->key, 'y' => $y];
}

?>

<script>

// Radialize the colors
Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
    return {
        radialGradient: {
            cx: 0.5,
            cy: 0.3,
            r: 0.7
        },
        stops: [
            [0, color],
            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
        ]
    };
});

// Build the chart
Highcharts.chart('container', {
    chart: {
        backgroundColor: null,
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        shadow: false,
        borderColor: 'black',
        type: 'pie'
    },
    title: {
        text: ''
    },
    legend: {
        borderColor: 'black'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                },
                connectorColor: 'silver',
                borderColor: 'black'
            },
            borderColor : '#00bca4',

        }
    },
    series: [{
        name: "<?php echo Yii::t('app', 'Криптовалюта'); ?>",
        data: <?php echo json_encode($walletInfo); ?>
    }]
});
</script>

<!-- HTML -->
<div id="chartdiv"></div>

        </div>
    </div>
</div>
<div class="container mrg25B">
    <h3 class="p-title">
        <span><?=Yii::t('app', 'План развития');?></span>
    </h3>
    <div class="owl-carousel-1 slider-wrapper inverse arrows-outside carousel-wrapper">

        <div>
            <div class="thumbnail-box-wrapper mrg5A">
                <div class="thumbnail-box">
                    <a class="thumb-link" href="#" title=""></a>
                    <div class="thumb-content">
                        <div class="center-vertical">
                            <div class="center-content">
                                <i class="icon-helper icon-center animated zoomInUp font-white glyph-icon icon-linecons-camera"></i>
                            </div>
                        </div>
                    </div>
                    <div class="thumb-overlay bg-black"></div>
                    <img src="/cryptofund/image-resources/stock-images/rm1.jpg" alt="">
                </div>
                <div class="thumb-pane">
                    <h3 class="thumb-heading animated rollIn">
                        <a href="#" title="">
                            <?=Yii::t('app', 'Март 2017');?>
                        </a>
                        <small><?=Yii::t('app', 'Разработка концепции');?></small>
                    </h3>
                </div>
            </div>
        </div>
        <div>
            <div class="thumbnail-box-wrapper mrg5A">
                <div class="thumbnail-box">
                    <a class="thumb-link" href="#" title=""></a>
                    <div class="thumb-content">
                        <div class="center-vertical">
                            <div class="center-content">
                                <i class="icon-helper icon-center animated zoomInUp font-white glyph-icon icon-linecons-camera"></i>
                            </div>
                        </div>
                    </div>
                    <div class="thumb-overlay bg-black"></div>
                    <img src="/cryptofund/image-resources/stock-images/rm2.jpg" alt="">
                </div>
                <div class="thumb-pane">
                    <h3 class="thumb-heading animated rollIn">
                        <a href="#" title="">
                            <?=Yii::t('app', 'Июнь 2017');?>
                        </a>
                        <small><?=Yii::t('app', 'Частная продажа токенов');?></small>
                    </h3>
                </div>
            </div>
        </div>
        <div>
            <div class="thumbnail-box-wrapper mrg5A">
                <div class="thumbnail-box">
                    <a class="thumb-link" href="#" title=""></a>
                    <div class="thumb-content">
                        <div class="center-vertical">
                            <div class="center-content">
                                <i class="icon-helper icon-center animated zoomInUp font-white glyph-icon icon-linecons-camera"></i>
                            </div>
                        </div>
                    </div>
                    <div class="thumb-overlay bg-black"></div>
                    <img src="/cryptofund/image-resources/stock-images/rm3.jpg" alt="">
                </div>
                <div class="thumb-pane">
                    <h3 class="thumb-heading animated rollIn">
                        <a href="#" title="">
                            <?=Yii::t('app', 'Август 2017');?>
                        </a>
                        <small><?=Yii::t('app', 'Открытая продажа токенов');?></small>
                    </h3>
                </div>
            </div>
        </div>
        <div>
            <div class="thumbnail-box-wrapper mrg5A">
                <div class="thumbnail-box">
                    <a class="thumb-link" href="#" title=""></a>
                    <div class="thumb-content">
                        <div class="center-vertical">
                            <div class="center-content">
                                <i class="icon-helper icon-center animated zoomInUp font-white glyph-icon icon-linecons-camera"></i>
                            </div>
                        </div>
                    </div>
                    <div class="thumb-overlay bg-black"></div>
                    <img src="/cryptofund/image-resources/stock-images/rm4.jpg" alt="">
                </div>
                <div class="thumb-pane">
                    <h3 class="thumb-heading animated rollIn">
                        <a href="#" title="">
                            <?=Yii::t('app', 'Ноябрь 2017');?>
                        </a>
                        <small><?=Yii::t('app', 'Проведение ICO');?></small>
                    </h3>
                </div>
            </div>
        </div>
        <div>
            <div class="thumbnail-box-wrapper mrg5A">
                <div class="thumbnail-box">
                    <a class="thumb-link" href="#" title=""></a>
                    <div class="thumb-content">
                        <div class="center-vertical">
                            <div class="center-content">
                                <i class="icon-helper icon-center animated zoomInUp font-white glyph-icon icon-linecons-camera"></i>
                            </div>
                        </div>
                    </div>
                    <div class="thumb-overlay bg-black"></div>
                    <img src="/cryptofund/image-resources/stock-images/rm5.jpg" alt="">
                </div>
                <div class="thumb-pane">
                    <h3 class="thumb-heading animated rollIn">
                        <a href="#" title="">
                            <?=Yii::t('app', 'Декабрь 2017');?>
                        </a>
                        <small><?=Yii::t('app', 'Выпуск криптовалютных токенов');?></small>
                    </h3>
                </div>
            </div>
        </div>
        <div>
            <div class="thumbnail-box-wrapper mrg5A">
                <div class="thumbnail-box">
                    <a class="thumb-link" href="#" title=""></a>
                    <div class="thumb-content">
                        <div class="center-vertical">
                            <div class="center-content">
                                <i class="icon-helper icon-center animated zoomInUp font-white glyph-icon icon-linecons-camera"></i>
                            </div>
                        </div>
                    </div>
                    <div class="thumb-overlay bg-black"></div>
                    <img src="/cryptofund/image-resources/stock-images/rm6.jpg" alt="">
                </div>
                <div class="thumb-pane">
                    <h3 class="thumb-heading animated rollIn">
                        <a href="#" title="">
                            <?=Yii::t('app', 'Январь 2018');?>
                        </a>
                        <small><?=Yii::t('app', 'Отчет за 2017');?></small>
                    </h3>
                </div>
            </div>
        </div>

    </div>
    <div class="mrg25B"></div>
</div>
