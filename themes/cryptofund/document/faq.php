<?php
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'Faq',
    'url' => '/faq'
  ],
];
$this->title = $model->title;
?>
<div class="hero-box hero-box-smaller full-bg-9 font-inverse" data-top-bottom="background-position: 50% 0px;" data-bottom-top="background-position: 50% -600px;">
    <div class="container" style = "margin-top: 170px;">
        <h1 class="hero-heading wow fadeInDown" data-wow-duration="0.6s"><?php echo $this->title;?></h1>
        <p class="hero-text wow bounceInUp" data-wow-duration="0.9s" data-wow-delay="0.2s"><?php echo Yii::t('app', 'Часто задаваемые вопросы');?></p>
    </div>
    <div class="hero-overlay bg-black"></div>
</div>

<div id="page-title">
  <div class = "container">
    </br>
    </br>
    <div id="theme-options" class="admin-options">
    <a href="javascript:void(0);" class="btn btn-primary theme-switcher tooltip-button" data-placement="left" title="Color schemes and layout options">
        <i class="glyph-icon icon-linecons-cog icon-spin"></i>
    </a>
    <div id="theme-switcher-wrapper">
        <div class="scroll-switcher">
            <h5 class="header">Layout options</h5>
            <ul class="reset-ul">
                <li>
                    <label for="boxed-layout">Boxed layout</label>
                    <input type="checkbox" data-toggletarget="boxed-layout" id="boxed-layout" class="input-switch-alt">
                </li>
                <li>
                    <label for="fixed-header">Fixed header</label>
                    <input type="checkbox" data-toggletarget="fixed-header" id="fixed-header" class="input-switch-alt">
                </li>
                <li>
                    <label for="fixed-sidebar">Fixed sidebar</label>
                    <input type="checkbox" data-toggletarget="fixed-sidebar" id="fixed-sidebar" class="input-switch-alt">
                </li>
                <li>
                    <label for="closed-sidebar">Closed sidebar</label>
                    <input type="checkbox" data-toggletarget="closed-sidebar" id="closed-sidebar" class="input-switch-alt">
                </li>
            </ul>
            <div class="boxed-bg-wrapper hide">
                <h5 class="header">
                    Boxed Page Background
                    <a class="set-background-style" data-header-bg="" title="Remove all styles" href="javascript:void(0);">Clear</a>
                </h5>
                <div class="theme-color-wrapper clearfix">
                    <h5>Patterns</h5>
                    <a class="tooltip-button set-background-style pattern-bg-3" data-header-bg="pattern-bg-3" title="Pattern 3" href="javascript:void(0);">Pattern 3</a>
                    <a class="tooltip-button set-background-style pattern-bg-4" data-header-bg="pattern-bg-4" title="Pattern 4" href="javascript:void(0);">Pattern 4</a>
                    <a class="tooltip-button set-background-style pattern-bg-5" data-header-bg="pattern-bg-5" title="Pattern 5" href="javascript:void(0);">Pattern 5</a>
                    <a class="tooltip-button set-background-style pattern-bg-6" data-header-bg="pattern-bg-6" title="Pattern 6" href="javascript:void(0);">Pattern 6</a>
                    <a class="tooltip-button set-background-style pattern-bg-7" data-header-bg="pattern-bg-7" title="Pattern 7" href="javascript:void(0);">Pattern 7</a>
                    <a class="tooltip-button set-background-style pattern-bg-8" data-header-bg="pattern-bg-8" title="Pattern 8" href="javascript:void(0);">Pattern 8</a>
                    <a class="tooltip-button set-background-style pattern-bg-9" data-header-bg="pattern-bg-9" title="Pattern 9" href="javascript:void(0);">Pattern 9</a>
                    <a class="tooltip-button set-background-style pattern-bg-10" data-header-bg="pattern-bg-10" title="Pattern 10" href="javascript:void(0);">Pattern 10</a>

                    <div class="clear"></div>

                    <h5 class="mrg15T">Solids &amp;Images</h5>
                    <a class="tooltip-button set-background-style bg-black" data-header-bg="bg-black" title="Black" href="javascript:void(0);">Black</a>
                    <a class="tooltip-button set-background-style bg-gray mrg10R" data-header-bg="bg-gray" title="Gray" href="javascript:void(0);">Gray</a>

                    <a class="tooltip-button set-background-style full-bg-1" data-header-bg="full-bg-1 fixed-bg" title="Image 1" href="javascript:void(0);">Image 1</a>
                    <a class="tooltip-button set-background-style full-bg-2" data-header-bg="full-bg-2 fixed-bg" title="Image 2" href="javascript:void(0);">Image 2</a>
                    <a class="tooltip-button set-background-style full-bg-3" data-header-bg="full-bg-3 fixed-bg" title="Image 3" href="javascript:void(0);">Image 3</a>
                    <a class="tooltip-button set-background-style full-bg-4" data-header-bg="full-bg-4 fixed-bg" title="Image 4" href="javascript:void(0);">Image 4</a>
                    <a class="tooltip-button set-background-style full-bg-5" data-header-bg="full-bg-5 fixed-bg" title="Image 5" href="javascript:void(0);">Image 5</a>
                    <a class="tooltip-button set-background-style full-bg-6" data-header-bg="full-bg-6 fixed-bg" title="Image 6" href="javascript:void(0);">Image 6</a>

                </div>
            </div>
            <h5 class="header">
                Header Style
                <a class="set-adminheader-style" data-header-bg="bg-gradient-9" title="Remove all styles" href="javascript:void(0);">Clear</a>
            </h5>
            <div class="theme-color-wrapper clearfix">
                <h5>Solids</h5>
                <a class="tooltip-button set-adminheader-style bg-primary" data-header-bg="bg-primary font-inverse" title="Primary" href="javascript:void(0);">Primary</a>
                <a class="tooltip-button set-adminheader-style bg-green" data-header-bg="bg-green font-inverse" title="Green" href="javascript:void(0);">Green</a>
                <a class="tooltip-button set-adminheader-style bg-red" data-header-bg="bg-red font-inverse" title="Red" href="javascript:void(0);">Red</a>
                <a class="tooltip-button set-adminheader-style bg-blue" data-header-bg="bg-blue font-inverse" title="Blue" href="javascript:void(0);">Blue</a>
                <a class="tooltip-button set-adminheader-style bg-warning" data-header-bg="bg-warning font-inverse" title="Warning" href="javascript:void(0);">Warning</a>
                <a class="tooltip-button set-adminheader-style bg-purple" data-header-bg="bg-purple font-inverse" title="Purple" href="javascript:void(0);">Purple</a>
                <a class="tooltip-button set-adminheader-style bg-black" data-header-bg="bg-black font-inverse" title="Black" href="javascript:void(0);">Black</a>

                <div class="clear"></div>

                <h5 class="mrg15T">Gradients</h5>
                <a class="tooltip-button set-adminheader-style bg-gradient-1" data-header-bg="bg-gradient-1 font-inverse" title="Gradient 1" href="javascript:void(0);">Gradient 1</a>
                <a class="tooltip-button set-adminheader-style bg-gradient-2" data-header-bg="bg-gradient-2 font-inverse" title="Gradient 2" href="javascript:void(0);">Gradient 2</a>
                <a class="tooltip-button set-adminheader-style bg-gradient-3" data-header-bg="bg-gradient-3 font-inverse" title="Gradient 3" href="javascript:void(0);">Gradient 3</a>
                <a class="tooltip-button set-adminheader-style bg-gradient-4" data-header-bg="bg-gradient-4 font-inverse" title="Gradient 4" href="javascript:void(0);">Gradient 4</a>
                <a class="tooltip-button set-adminheader-style bg-gradient-5" data-header-bg="bg-gradient-5 font-inverse" title="Gradient 5" href="javascript:void(0);">Gradient 5</a>
                <a class="tooltip-button set-adminheader-style bg-gradient-6" data-header-bg="bg-gradient-6 font-inverse" title="Gradient 6" href="javascript:void(0);">Gradient 6</a>
                <a class="tooltip-button set-adminheader-style bg-gradient-7" data-header-bg="bg-gradient-7 font-inverse" title="Gradient 7" href="javascript:void(0);">Gradient 7</a>
                <a class="tooltip-button set-adminheader-style bg-gradient-8" data-header-bg="bg-gradient-8 font-inverse" title="Gradient 8" href="javascript:void(0);">Gradient 8</a>
            </div>
            <h5 class="header">
                Sidebar Style
                <a class="set-sidebar-style" data-header-bg="" title="Remove all styles" href="javascript:void(0);">Clear</a>
            </h5>
            <div class="theme-color-wrapper clearfix">
                <h5>Solids</h5>
                <a class="tooltip-button set-sidebar-style bg-primary" data-header-bg="bg-primary font-inverse" title="Primary" href="javascript:void(0);">Primary</a>
                <a class="tooltip-button set-sidebar-style bg-green" data-header-bg="bg-green font-inverse" title="Green" href="javascript:void(0);">Green</a>
                <a class="tooltip-button set-sidebar-style bg-red" data-header-bg="bg-red font-inverse" title="Red" href="javascript:void(0);">Red</a>
                <a class="tooltip-button set-sidebar-style bg-blue" data-header-bg="bg-blue font-inverse" title="Blue" href="javascript:void(0);">Blue</a>
                <a class="tooltip-button set-sidebar-style bg-warning" data-header-bg="bg-warning font-inverse" title="Warning" href="javascript:void(0);">Warning</a>
                <a class="tooltip-button set-sidebar-style bg-purple" data-header-bg="bg-purple font-inverse" title="Purple" href="javascript:void(0);">Purple</a>
                <a class="tooltip-button set-sidebar-style bg-black" data-header-bg="bg-black font-inverse" title="Black" href="javascript:void(0);">Black</a>

                <div class="clear"></div>

                <h5 class="mrg15T">Gradients</h5>
                <a class="tooltip-button set-sidebar-style bg-gradient-1" data-header-bg="bg-gradient-1 font-inverse" title="Gradient 1" href="javascript:void(0);">Gradient 1</a>
                <a class="tooltip-button set-sidebar-style bg-gradient-2" data-header-bg="bg-gradient-2 font-inverse" title="Gradient 2" href="javascript:void(0);">Gradient 2</a>
                <a class="tooltip-button set-sidebar-style bg-gradient-3" data-header-bg="bg-gradient-3 font-inverse" title="Gradient 3" href="javascript:void(0);">Gradient 3</a>
                <a class="tooltip-button set-sidebar-style bg-gradient-4" data-header-bg="bg-gradient-4 font-inverse" title="Gradient 4" href="javascript:void(0);">Gradient 4</a>
                <a class="tooltip-button set-sidebar-style bg-gradient-5" data-header-bg="bg-gradient-5 font-inverse" title="Gradient 5" href="javascript:void(0);">Gradient 5</a>
                <a class="tooltip-button set-sidebar-style bg-gradient-6" data-header-bg="bg-gradient-6 font-inverse" title="Gradient 6" href="javascript:void(0);">Gradient 6</a>
                <a class="tooltip-button set-sidebar-style bg-gradient-7" data-header-bg="bg-gradient-7 font-inverse" title="Gradient 7" href="javascript:void(0);">Gradient 7</a>
                <a class="tooltip-button set-sidebar-style bg-gradient-8" data-header-bg="bg-gradient-8 font-inverse" title="Gradient 8" href="javascript:void(0);">Gradient 8</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-3">
      <ul class="list-group">
          <li class="mrg10B">
              <a href="#faq-tab-1" data-toggle="tab" class="list-group-item bg-white">
                  Как получить доход
                  <i class="glyph-icon icon-angle-right mrg0A"></i>
              </a>
          </li>
          <li class="mrg10B">
              <a href="#faq-tab-2" data-toggle="tab" class="list-group-item bg-white">
                  Общие вопросы
                  <i class="glyph-icon font-green icon-angle-right mrg0A"></i>
              </a>
          </li>
          <li class="mrg10B">
              <a href="#faq-tab-3" data-toggle="tab" class="list-group-item bg-white">
                  Условия инвестирования
                  <i class="glyph-icon icon-angle-right mrg0A"></i>
              </a>
          </li>
          <li class="mrg10B">
              <a href="#faq-tab-4" data-toggle="tab" class="list-group-item bg-white">
                  Условия предоставления услуг
                  <i class="glyph-icon icon-angle-right mrg0A"></i>
              </a>
          </li>
      </ul>
  </div>
  <div class="col-md-9">
      <div class="tab-content">
          <div class="tab-pane fade active in pad0A" id="faq-tab-1">
              <div class="panel-group" id="accordion5">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion5" href="#collapseOne">
                                  <?php echo Yii::t('app', '1. Как инвестировать?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">
                          <div class="panel-body pad0B">
                              <p class="mrg15B"><?php echo Yii::t('app', '1. Вам нужно зарегистрироваться на сайте: <a href="http://www.example.com/">http://citt.fund/signup</a> 
</br>2. Нажмите на кнопку «Получить CITT» в левой части сайта: <a href="http://www.example.com/">http://citt.fund/en/profile/office/in</a> 
</br>3. Укажите количество CITT, которое Вы хотите приобрести и нажмите “Далее”.
</br>4. Выберете валюту, которой Вы хотите произвести расчет за покупку CITT.
</br>5. Выберете платежную систему, по которой Вы хотите произвести расчет.
</br>6. На следующем этапе Вам необходимо произвести платеж за покупку CITT. Проведение платежа может занять до 10 минут, пока ваша транзакция не завершится. Вы можете увидеть текущий статус транзакции на странице «Получить CITT» внизу.
');?></p>

                              <p class="mrg15B"></p>

                              <p class="mrg15B"></p>
                          </div>
                      </div>
                  </div>
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion5" href="#collapseTwo">
                                  <?php echo Yii::t('app', '2. Где я могу проверить свой текущий баланс?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">Вы можете посмотреть ваш баланс на главном экране кабинета сайта: <a href="http://www.example.com/">http://citt.fund/en/profile/office/dashboard</a></p>
                              <p class="mrg15B"></p>

                          </div>
                      </div>
                  </div>
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion5" href="#collapseThree">
                                  <?php echo Yii::t('app', '3. Как обменять средства?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseThree" class="panel-collapse collapse">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">1. Нажмите на кнопку «Обменять CITT» в левой части сайта: <a href="http://www.example.com/">http://citt.fund/en/profile/office/out</a> 
</br>2. Выберите количество токенов, которое вы хотите продать.
</br>3. Выберите актив (валюту), который вы хотите получить.
</br>4. Укажите свой кошелек, на который хотите получить средства за обмен CITT
</br>5. Введите свой плаежный пароль.
</br>6. Кликните на зеленую кнопку «Обменять CITT» в нижней части формы.
</br>7. Отправьте ваши CITT токены на адрес для обмена.
</br>8. После того, как вы отправите на обмен ваши токены, вы получите ваши средства.
</br>Обмен ваших средств может занимать до 24 часов.
</p>

                              <p class="mrg15B"></p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="tab-pane fade pad0A" id="faq-tab-2">
              <div class="panel-group" id="accordion1">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
                                  <?php echo Yii::t('app', '4. Я забыл свой логин/пароль. Что мне делать?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseOne1" class="panel-collapse collapse in">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">Мы рекомендуем хранить ваш пароль в безопасности, но если вы его потеряете, есть возможность его восстановить, пройдя по ссылке: <a href="http://www.example.com/">http://citt.fund/login</a>, выбрать “Восстановить пароль”, а затем заполнить форму в всплывающем окне.
</p>

                              <p class="mrg15B"></p>
                          </div>
                      </div>
                  </div>
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1">
                                  <?php echo Yii::t('app', '5. Как The CITT Fund принимает средства?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseTwo1" class="panel-collapse collapse">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">Мы принимаем большинство платежных систем которые есть сейчас в интернете. Также мы принимаем оплаты с Mastercard и Visa. Кроме этого мы принимаем btc и eth в качестве платежей.
</p>

                              <p class="mrg15B"></p>
                          </div>
                      </div>
                  </div>
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion1" href="#collapseThree1">
                                  <?php echo Yii::t('app', '6. Как фонд обеспечивает безопасность работы с криптовалютами? Как минимизируются риски работы с биржами криптовалют? В безопасности ли средства?
');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseThree1" class="panel-collapse collapse">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">Фонд обеспечивает безопасность следующими способами:
</br>1. Максимизация хранения активов на холодных кошельках и сведение к нулю объемов хранения на биржевых аккаунтах.
</br>2. Работа через кошельки с мультиподписями (операции одобряются как минимум двумя менеджерами), что минимизирует риск ошибок из-за человеческого фактора.
</br>3. Работа только на надежных биржевых площадках, на которых проходят максимальные объемы торговых операций.
</p>

                              <p class="mrg15B">Все средства безопасно содержатся в аккаунтах и кошельках (вы можете проверить текущие количества по % ссылке в вашем кабинете).
</p>

                              <p class="mrg15B">Фонд разработан на платформе Ethereum, являющейся наиболее эффективной и безопасной платформой существующей сегодня.</p>
                              <p class="mrg15B"></p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="tab-pane fade pad0A" id="faq-tab-3">
              <div class="panel-group" id="accordion2">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2">
                                  <?php echo Yii::t('app', '7. Могу ли я инвестировать наличные?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseOne2" class="panel-collapse collapse in">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">Нет, но вы можете сделать это переводом с банковской карты.</p>

                              <p class="mrg15B"></p>
                          </div>
                      </div>
                  </div>
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2">
                                  <?php echo Yii::t('app', '8. Есть ли ограничения по объему инвестирования?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseTwo2" class="panel-collapse collapse">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">Нет, вы можете инвестировать столько, сколько считаете нужным.</p>

                              <p class="mrg15B"></p>


                          </div>
                      </div>
                  </div>
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThree2">
                                  <?php echo Yii::t('app', '9. Есть ли минимальный/максимальный таймфрейм для инвестирования? Нужно ли мне хранить средства определенное время?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseThree2" class="panel-collapse collapse">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">Нет, вы можете инвестировать и изымать средства в любой момент.</p>

                              <p class="mrg15B"></p>


                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="tab-pane fade pad0A" id="faq-tab-4">
              <div class="panel-group" id="accordion3">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne4">
                                  <?php echo Yii::t('app', '10. В чем заключается ваше предложение?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseOne4" class="panel-collapse collapse in">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">Мы предлагаем инвестировать в крипто экономику через наш Фонд. Наш текущий портфель можно найти здесь: <a href="http://www.example.com/">http://citt.fund</a></p>

                              <p class="mrg15B">Мы демонстрируем высокую доходность, высокий уровень безопасности и инвестируем только в проверенные активы и активы с высокой потенциальной доходностью, тем не менее диверсифицируя портфель для снижения рисков.</p>
                          </div>
                      </div>
                  </div>
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo4">
                                  <?php echo Yii::t('app', '11. Какие есть для инвестора способы снижения рисков частичной или полной потери средств например вследствии атаки на фонд и вывода портфеля 3-ми лицами?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseTwo4" class="panel-collapse collapse">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">Инвестор должен безопасно хранить приватные ключи от своего кошелька с CITT. Защита средств инвесторов в портфеле фонда обеспечивается менеджерами фонда.</p>

                              <p class="mrg15B"></p>
                          </div>
                      </div>
                  </div>
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree4">
                                  <?php echo Yii::t('app', '12. Проводится ли верификация инвесторов?');?>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseThree4" class="panel-collapse collapse">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">В настоящий момент верификация не проводится. В будущем планируется использовать систему верификации. Клиенты будут уведомлены об этом дополнительно.</p>

                              <p class="mrg15B"></p>
                      </div>
                  </div>
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree4">
                                  
                              </a>
                          </h4>
                      </div>
                      <div id="collapseThree4" class="panel-collapse collapse">
                          <div class="panel-body pad0B">
                              <p class="mrg15B">It will be as simple as Occidental; in fact, it will be Occidental. To an English person, it will seem like simplified English, as a skeptical Cambridge friend of mine told me what Occidental is. The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words.</p>

                              <p class="mrg15B">Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators. To achieve this, it would be necessary to have uniform grammar, pronunciation and more common words. If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be Occidental.</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>