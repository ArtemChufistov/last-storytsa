<?php
use yii\helpers\Url;
?>
<ul id="sidebar-menu">
  <li class="header"><?=Yii::t('app', 'Личный кабинет');?></li>
  <?php foreach ($menu->children($menu->depth + 1)->all() as $num => $child): ?>
    <li class="divider"></li>
    <?php $children = $child->children()->all();?>
    <li class="<?php if ($requestUrl == Url::to([$child->link])): ?>sfHover<?php endif;?>">
        <a href="<?=Url::to([$child->link]);?>" class = "sf-with-ul">
          <?=$child->icon;?> <span><?=$child->title;?></span>
        </a>
    </li>
  <?php endforeach;?>
</ul>