<?php
use app\modules\lang\widgets\WLang;
use yii\helpers\Url;
?>
<div class="main-header header-fixed font-inverse header-opacity header-resizable wow bounceInDown" data-0="padding: 15px 0; background: rgba(255,255,255,0.1);" data-250="padding: 0px 0; background: rgba(0,0,0,1)">
  <div class="container">
    <a href="/" class="header-logo" title="Cryptofund"></a>
    <div class="right-header-btn">
        <div id="mobile-navigation">
            <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target=".header-nav"><span></span></button>
        </div>
    </div>

    <ul class="header-nav collapse">

      <?php foreach ($menu->children()->all() as $num => $children): ?>
        <li>
            <a href="<?=Url::to([$children->link]);?>" title="<?=$children->title;?>">
                <?=$children->title;?>
                <?php if (!empty($children->children()->all())): ?><i class="glyph-icon icon-angle-down"></i><?php endif;?>
            </a>
            <?php if (!empty($children->children()->all())): ?>
              <ul>
                <?php foreach ($children->children()->all() as $numChild => $childChildren): ?>
                  <li><a href="<?=Url::to([$childChildren->link]);?>" title="<?=$childChildren->title;?>"><span><?=$childChildren->title;?></span></a></li>
                <?php endforeach;?>
              </ul>
            <?php endif;?>
        </li>
      <?php endforeach;?>
      <?=WLang::widget();?>
    </ul>
  </div>
</div>
