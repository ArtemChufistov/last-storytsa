<ul class="sidebar-menu">
  <li class="header"><?= Yii::t('app', 'Личный кабинет');?></li>
  <?php foreach($menu->children($menu->depth + 1)->all() as $num => $child):?>
    <?php $children = $child->children()->all();?>
    <li class="<?php if($requestUrl == $child->link):?>active<?php endif;?> treeview">
      <a href="<?= $child->link;?>">
        <?= $child->icon;?> <span><?= $child->title;?></span>
        <?php if (!empty($children)):?>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        <?php endif;?>
      </a>
      <?php if (!empty($children)):?>
        <ul class="treeview-menu">
          <?php foreach($children as $numChild => $childChildren):?>
            <li><a href="<?= $childChildren->link;?>"><?= $childChildren->icon;?> <?= $childChildren->title;?></a></li>
          <?php endforeach;?>
        </ul>
      <?php endif;?>
    </li>
  <?php endforeach;?>

    <?php $user = Yii::$app->user->identity;?>

    <script type="text/javascript">(function() {
      if (window.pluso)if (typeof window.pluso.start == "function") return;
      if (window.ifpluso==undefined) { window.ifpluso = 1;
        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
        var h=d[g]('body')[0];
        h.appendChild(s);
      }})();</script>
    <div style="margin-left: 10px;" class="pluso" data-background="#ebebeb" data-options="small,square,line,horizontal,counter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,moimir,email" data-url="http://volcano.money?ref=<?php echo $user->login;?>" data-title="Денежный вулкан" data-description="Денежный вулкан, получать деньги - это просто"></div>

<!--
  <li class="active treeview">
    <a href="#">
      <i class="fa fa-dashboard"></i> <span>Dashboard</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
      <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-files-o"></i>
      <span>Layout Options</span>
      <span class="pull-right-container">
        <span class="label label-primary pull-right">4</span>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
      <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
      <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
      <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
    </ul>
  </li>
  <li>
    <a href="pages/widgets.html">
      <i class="fa fa-th"></i> <span>Widgets</span>
      <span class="pull-right-container">
        <small class="label pull-right bg-green">new</small>
      </span>
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-pie-chart"></i>
      <span>Charts</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
      <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
      <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
      <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
    </ul>
  </li>
 -->
</ul>