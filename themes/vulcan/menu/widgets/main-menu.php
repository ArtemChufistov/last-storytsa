<?php
use yii\web\View;
use yii\helpers\Url;
?>

<header class="page-head">
  <div class="rd-navbar-wrap">
    <nav data-lg-stick-up-offset="79px" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" class="rd-navbar rd-navbar-top-panel rd-navbar-light" data-lg-auto-height="true" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
      <div class="container">
        <div class="rd-navbar-inner">
          <div class="rd-navbar-top-panel">
            <div class="left-side">
              <ul class="rd-navbar-top-links list-unstyled">
                <li><a href="<?php echo Url::to(['/profile/profile/login']);?>" class="text-uppercase text-ubold"><small><?php echo Yii::t('app', 'Личный кабинет');?></small></a></li>
                <li><a href="<?php echo Url::to(['/profile/profile/signup']);?>" class="text-uppercase text-ubold"><small><?php echo Yii::t('app', 'Регистрация');?></small></a></li>
              </ul>
            </div>

            <div class="right-side" style = "height: 5px;
    margin-left: 10px;
    margin-top: 7px;">

              <div id="google_translate_element" style = "height: 32px;overflow: hidden; margin-top:-4px;"></div><script type="text/javascript">
              function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
              }
              </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

            </div>

            <div class="right-side">
              <address class="contact-info text-left"><span class="p"><span class="icon mdi mdi-email-open"></span><a href="mailto:#">support@volcano.money</a></span></address>
            </div>
          </div>
          <!-- RD Navbar Panel-->
          <div class="rd-navbar-panel">
            <!-- RD Navbar Toggle-->
            <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
            <!-- RD Navbar Top Panel Toggle-->
            <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-top-panel" class="rd-navbar-top-panel-toggle"><span></span></button>
            <!--Navbar Brand-->
            <div class="rd-navbar-brand"><a href="/"><img class='img-responsive' width='232' height='34' src='/images/intense/logo-dark.png' alt=''/></a>
    <?php $user = Yii::$app->user->identity;?>

    <script type="text/javascript">(function() {
      if (window.pluso)if (typeof window.pluso.start == "function") return;
      if (window.ifpluso==undefined) { window.ifpluso = 1;
        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
        var h=d[g]('body')[0];
        h.appendChild(s);
      }})();</script>
    <div style="margin-left: 30px;" class="pluso" data-background="#ebebeb" data-options="small,square,line,horizontal,counter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,moimir,email" data-url="http://volcano.money?ref=<?php echo $user->login;?>" data-title="Денежный вулкан" data-description="Денежный вулкан, получать деньги - это просто"></div>

            </div>
          </div>
          <div class="rd-navbar-menu-wrap">
            <div class="rd-navbar-nav-wrap">
              <div class="rd-navbar-mobile-scroll">
                <!--Navbar Brand Mobile-->
                <div class="rd-navbar-mobile-brand"><a href="/"><img class='img-responsive' width='232' height='34' src='/images/intense/logo-dark.png' alt=''/></a></div>
                <!-- RD Navbar Nav-->
                <ul class="rd-navbar-nav">
                  <?php foreach($menu->children()->all() as $num => $children):?>
                    <li class="<?php if($requestUrl == $children->link):?>active<?php endif;?>" ><a href="<?= $children->link;?>"><span><?= $children->title;?></span></a>
                      <?php if (!empty($children->children()->all())):?>
                        <ul class="rd-navbar-dropdown">
                          <?php foreach($children->children()->all() as $numChild => $childChildren):?>
                            <li><a href="<?= $childChildren->link;?>"><span class="text-middle"><?= $childChildren->title;?></span></a>
                              <ul class="rd-navbar-dropdown">
                                <li><a href="<?= $childChildren->link;?>"><span class="text-middle"><?= $childChildren->title;?></span></a>
                                </li>
                              </ul>
                            </li>
                          <?php endforeach;?>
                        </ul>
                      <?php endif;?>
                    </li>
                  <?php endforeach;?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
</header>