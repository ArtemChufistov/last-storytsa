<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use lowbase\user\UserAsset;
use app\modules\profile\components\AuthChoice;

$this->title = Yii::t('user', 'Вход на сайт');
$this->params['breadcrumbs'][] = $this->title;
UserAsset::register($this);
?>
<!-- Icon Box Type 4-->
<span class="icon icon-circle icon-bordered icon-lg icon-default mdi mdi-account-multiple-outline"></span>
<div>
  <div class="offset-top-24 text-darker big text-bold"><?= Html::encode($this->title) ?></div>
  <p class="text-extra-small text-dark offset-top-4"><?= Yii::t('app', 'Пожалуйста введите свой Логин и пароль');?></p>
</div>

<?php echo $this->render('partial/_pass', ['model' => $forget]);?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}"
    ],
    'options' => [
        'data-form-output' => 'form-output-global',
        'data-form-type' => 'contact',
        'class' => 'text-left offset-top-30'
    ]
]); ?>

  <div class="form-group-sign">
    <div class="input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-account-outline"></span></span>

        <?= $form->field($model, 'loginOrEmail')->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('loginOrEmail')
        ]); ?>

    </div>
  </div>
  <div class="form-group-sign offset-top-20">
    <div class="input-group-sm"><span class="input-group-addon input-group-addon-inverse"><span class="input-group-icon mdi mdi-lock-open-outline"></span></span>

        <?= $form->field($model, 'password')->passwordInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('password')
        ]);?>

    </div>
  </div>

  <div class="form-group-sign">

        <?php $labelRememberMe = '<span class="checkbox-custom-dummy"></span><span class="text-dark text-extra-small">' . Yii::t('app', 'Запомнить меня'). '</span>';?>

        <?= $form->field($model, 'rememberMe', ['template' => '<label class="checkbox-inline">{input}{error}</label>', 'options'=> ['tag' => 'div']])->checkBox(['label' => $labelRememberMe, 'class' => 'checkbox-custom']);?>

  </div>

  <div class="form-group-sign">
      <?= Html::submitButton('<i class="glyphicon glyphicon-log-in"></i> '.Yii::t('user', 'Войти'), [
          'class' => 'btn btn-sm btn-icon btn-block btn-primary offset-top-20 offset-bottom-10',
          'name' => 'login-button']) ?>
  </div>

<?php ActiveForm::end(); ?>
<div class="offset-top-30 text-sm-left text-dark text-extra-small">
  <?=Yii::t('user', 'Если')?> <?=Html::a(Yii::t('user', 'регистрировались'), ['/signup'], ['class' => 'text-picton-blue'])?> <?=Yii::t('user', 'ранее, но забыли пароль, нажмите')?>
  <?=Html::a(Yii::t('user', 'восстановить пароль'), ['#'], [
      'data-toggle' => 'modal',
      'data-target' => '#pass',
      'class' => 'text-picton-blue'
  ])?>.
</div>


<p class="text-extra-small text-dark offset-top-4"><?=Yii::t('user', 'Войти с помощью социальных сетей')?>:</p>

<div class="text-center" style="text-align: center">
    <?= AuthChoice::widget([
            'baseAuthUrl' => ['/profile/auth/index'],
            'clientCssClass' => 'col-xs-3'
    ]) ?>
</div>

