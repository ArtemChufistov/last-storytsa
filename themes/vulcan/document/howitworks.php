<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
		'label' => 'Как это работает',
		'url' => '/howitworks'
	],
];

$this->title = $model->title;
?>

<section class="breadcrumb-classic">
  <div class="shell section-34 section-sm-50">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="mdi mdi-presentation icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h2><span class="big"><?php echo $model->title;?></span></h2>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
        <?= Breadcrumbs::widget([
          'options' => ['class' => 'list-inline list-inline-dashed p'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
  </svg>
</section>
<!-- Page Content-->
<main class="page-content">
  <!-- Classic Gallery Carousel-->
  <!-- Classic Thumbnail-->
  <!-- Section single project type 2-->
  <section class="section-top-98 section-sm-top-110 section-bottom-34">
    <div class="shell">
      <h2 class="text-bold"><?= Yii::t('app', 'Маркетинг');?></h2>
      <hr class="divider bg-mantis">
      <div class="offset-sm-top-66">
        <div class="range">
          <div class="cell-md-7 cell-lg-12">
              <!-- Owl Carousel-->
              <div data-items="1" data-dots="true" data-nav="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]" data-photo-swipe-gallery class="owl-carousel owl-carousel-classic"><a class="thumbnail-classic" data-photo-swipe-item="" data-size="716x404" href="/volcano/slide/vesuvius1.jpg">
                          <figure><img width="716" height="404" src="/volcano/slide/newberry1.jpg" alt="">
                          </figure></a><a class="thumbnail-classic" data-photo-swipe-item="" data-size="716x404" href="/volcano/slide/newberry1.jpg">
                          
                          <figure><img width="716" height="404" src="/volcano/slide/newberry2.jpg" alt="">
                          </figure></a><a class="thumbnail-classic" data-photo-swipe-item="" data-size="716x404" href="/volcano/slide/newberry2.jpg">

                          <figure><img width="716" height="404" src="/volcano/slide/vesuvius1.jpg" alt="">
                          </figure></a>
              </div>
            <div class="text-sm-left offset-top-50">
              <h5 class="text-bold"><?= Yii::t('app', 'Всё очень просто');?></h5>
              <p><?= Yii::t('app', 'После прохождения полного цикла в двух программах Newberry & Vesuvius заработок составляет: 0.1565 btc + 0.833 btc = 0.9895 BTC');?></p>
                        </div>
            <div class="offset-top-30">
                <!-- Bootstrap Table-->
                <div class="table-responsive clearfix">
                  <table class="table table-striped">
                    <tr>
                      <th><?= Yii::t('app', 'Таблица, в которой наглядно прдемонстрировано сколько участник зарабатывает');?></th>
                      <th></th>
                    </tr>
                    <tr>
                      <th><?= Yii::t('app', 'ASAMA');?></th>
                      <td><?= Yii::t('app', 'переход');?></td>
                    </tr>
                    <tr>
                      <th><?= Yii::t('app', 'BROMO');?></th>
                      <td><?= Yii::t('app', '+0.01');?></td>
                    </tr>
                    <tr>
                      <th><?= Yii::t('app', 'CERRO BRAVO');?></th>
                      <td><?= Yii::t('app', 'переход');?></td>
                    </tr>
                    <tr>
                      <th><?= Yii::t('app', 'DALLOL');?></th>
                      <td><?= Yii::t('app', '+0.1465');?></td>
                    </tr>
                    <tr>
                      <th><?= Yii::t('app', 'KILIMANJARO');?></th>
                      <td><?= Yii::t('app', '+0.07');?></td>
                    </tr>
                     <tr>
                      <th><?= Yii::t('app', 'LASKAR');?></th>
                      <td><?= Yii::t('app', '+0.763');?></td>
                    </tr>
                     <tr>
                      <th><?= Yii::t('app', 'ВСЕГО ДОХОД');?></th>
                      <td><?= Yii::t('app', '+0.9895');?></td>
                    </tr>
                  </table>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<style>
.mainVideoWrap{
  background: rgba(0, 0, 0, 0) linear-gradient(90deg, #ffa200 0%, #ff7e00 100%) repeat scroll 0 0;
    border-radius: 2px;
    box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.4);
    margin: 0 auto;
    padding: 3px;
    width: 1000px;
    height: 700px;
}

@media (max-width: 799px) { 
  .mainVideoWrap{
    width: 500px !important;
    height: 370px;
  }
}
@media (min-width: 800px) { 
  .mainVideoWrap{
    width: 600px !important;
    height: 420px;
  }
}
@media (min-width: 1200px) { 
  .mainVideoWrap{
    width: 700px !important;
    height: 420px;
  }
}
.mainVideoWrap iframe{
  width: 100%;
  height: 100%;
}
</style>

  <section class="section-top-98 section-bottom-124">
    <div class="shell mainVideoWrap">
      <iframe style = "display:block; margin: 0 auto;"  src="https://www.youtube.com/embed/2YsZXd9nLjs" frameborder="0" allowfullscreen></iframe>    
</div>
  </section>
</main>
