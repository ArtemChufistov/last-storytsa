<?php
use yii\web\View;
use yii\helpers\Url;

$this->title = $model->title;
?>

<div data-on="false" data-md-on="true" class="bg-gray-base rd-parallax">
  <div data-speed="0.45" data-type="media" data-url="images/backgrounds/background-10-1920x1062.jpg" class="rd-parallax-layer"></div>
  <div data-speed="0.3" data-type="html" data-md-fade="true" class="rd-parallax-layer">
    <section class="bg-overlay-gray-darkest context-dark">
      <div class="shell">
        <div class="range">
          <div class="range range-xs-middle range-xs-center section-cover section-110">
            <div class="cell-xs-10 cell-sm-8 cell-md-6">
              <h1><?php echo Yii::t('app', 'Добро пожаловать в Денежный Вулкан');?></h1>
              <p><span class="big"><?php echo Yii::t('app', 'Проект который поможет Вам в реализации своих целей');?></span></p>
              <div class="offset-top-34">
                <div class="group">
                  <a href="<?php echo Url::to(['/profile/profile/signup']);?>" class="btn btn-primary"><?php echo Yii::t('app', 'Регистрация');?></a>
                  <a href="<?php echo Url::to(['/profile/profile/login']);?>" class="btn btn-default"><?php echo Yii::t('app', 'Личный кабинет');?></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<div id="section-terry"></div>
<main class="page-content">
  <!-- Thumbnails terry-->
  <section class="section-34">
    <div class="shell-fluid">
      <div class="range range-xs-center">
        <div class="cell-xs-8 cell-sm-6 cell-lg-3">
          <!-- Thumbnail Terry-->
          <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="images/skins/skin-sunrise-01-442x280.jpg" alt=""/></a>
            <figcaption>
              <div>
                <h4 class="thumbnail-terry-title"><?php echo Yii::t('app', 'Будь непредсказуемым');?></h4>
              </div>
              <p class="thumbnail-terry-desc offset-top-0"><?php echo Yii::t('app', 'Только те, кто предпринимают абсурдные попытки,</br> смогут достичь невозможного.');?></p>
            </figcaption>
          </figure>
        </div>
        <div class="cell-xs-8 cell-sm-6 cell-lg-3 offset-top-30 offset-sm-top-0">
          <!-- Thumbnail Terry-->
          <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="images/skins/skin-sunrise-02-442x280.jpg" alt=""/></a>
            <figcaption>
              <div>
                <h4 class="thumbnail-terry-title"><?php echo Yii::t('app', 'Расширяй горизонты');?></h4>
              </div>
              <p class="thumbnail-terry-desc offset-top-0"><?php echo Yii::t('app', 'Всегда есть возможность сделать миллион из пары</br> долларов, но видят и используют ее лишь единицы.');?></p>
            </figcaption>
          </figure>
        </div>
        <div class="cell-xs-8 cell-sm-6 cell-lg-3 offset-top-30 offset-lg-top-0">
          <!-- Thumbnail Terry-->
          <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="images/skins/skin-sunrise-03-442x280.jpg" alt=""/></a>
            <figcaption>
              <div>
                <h4 class="thumbnail-terry-title"><?php echo Yii::t('app', 'Принимай решения');?></h4>
              </div>
              <p class="thumbnail-terry-desc offset-top-0"><?php echo Yii::t('app', 'Для первого шага достаточно веры. Не обязательно</br> видеть всю лестницу, чтобы сделать первый шаг.');?></p>
            </figcaption>
          </figure>
        </div>
        <div class="cell-xs-8 cell-sm-6 cell-lg-3 offset-top-30 offset-lg-top-0">
          <!-- Thumbnail Terry-->
          <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="images/skins/skin-sunrise-04-442x280.jpg" alt=""/></a>
            <figcaption>
              <div>
                <h4 class="thumbnail-terry-title"><?php echo Yii::t('app', 'Используй возможности');?></h4>
              </div>
              <p class="thumbnail-terry-desc offset-top-0"><?php echo Yii::t('app', 'Каждому человеку дается множество</br> возможностей изменить свою жизнь. ');?></p>
            </figcaption>
          </figure>
        </div>
      </div>
    </div>
  </section>
  <!-- Intense Is The Most Flexible Bootstrap Theme-->
  <section class="section-bottom-98 section-top-34 section-md-124">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-xs-10">
          <h2 class="text-bold"><?php echo Yii::t('app', 'Наша миссия: помоги себе, помогая другим');?></h2>
          <hr class="divider bg-saffron">
        </div>
        <div class="cell-xs-8 offset-top-50 offset-sm-top-66">
          <p><?php echo Yii::t('app', 'Проект “Денежный Вулкан” обладает рядом преимуществ:');?></p>
        </div>
      </div>
      <div class="range range-md-middle offset-top-98">
        <div class="cell-md-4 cell-md-push-1"><img src="images/moqups/moqup-home-mobile.png" alt="" width="247" height="470" class="center-block img-responsive"></div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 text-md-right offset-top-34 offset-md-top-0">
          <div class="unit unit-spacing-sm unit-inverse unit-md unit-md-horizontal">
            <div class="unit-body">
              <h4 class="offset-md-top-20"><?php echo Yii::t('app', 'Неделящиеся матрицы');?></h4>
              <p><?php echo Yii::t('app', 'Привычная система заполнения матриц, проверенная временем');?></p>
            </div>
            <div class="unit-right"><span class="icon icon-circle icon-lg icon-bordered icon-primary mdi mdi-lan"></span></div>
          </div>
          <div class="unit unit-spacing-sm unit-inverse unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-body">
              <h4 class="offset-md-top-20"><?php echo Yii::t('app', 'Умные реинвесты');?></h4>
              <p><?php echo Yii::t('app', 'Умный механизм реинвестов для достижения мксимального эффекта');?></p>
            </div>
            <div class="unit-right"><span class="icon icon-circle icon-lg icon-bordered icon-primary mdi mdi-call-split"></span></div>
          </div>
          <div class="unit unit-spacing-sm unit-inverse unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-body">
              <h4 class="offset-md-top-20"><?php echo Yii::t('app', 'Использование Биткоина');?></h4>
              <p><?php echo Yii::t('app', 'Популярная и самая прибыльная денежная единица');?></p>
            </div>
            <div class="unit-right"><span class="icon icon-circle icon-lg icon-bordered icon-primary mdi mdi-currency-btc"></span></div>
          </div>
        </div>
        <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 text-md-left cell-md-push-1 offset-top-50 offset-md-top-0">
          <div class="unit unit-spacing-sm unit-md unit-md-horizontal">
            <div class="unit-left"><span class="icon icon-circle icon-lg icon-bordered icon-primary mdi mdi-database-plus"></span></div>
            <div class="unit-body">
              <h4 class="offset-md-top-20"><?php echo Yii::t('app', 'Крупные бонусы');?></h4>
              <p><?php echo Yii::t('app', 'Большие денежные вознаграждения за закрытие мест');?></p>
            </div>
          </div>
          <div class="unit unit-spacing-sm unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-left"><span class="icon icon-circle icon-lg icon-bordered icon-primary mdi mdi-heart-outline"></span></div>
            <div class="unit-body">
              <h4 class="offset-md-top-20"><?php echo Yii::t('app', 'Легкий старт');?></h4>
              <p><?php echo Yii::t('app', 'Маленькая стоимость участия, доступная абсолютно каждому');?></p>
            </div>
          </div>
          <div class="unit unit-spacing-sm unit-md unit-md-horizontal offset-top-50 offset-md-top-30">
            <div class="unit-left"><span class="icon icon-circle icon-lg icon-bordered icon-primary mdi mdi-earth"></span></div>
            <div class="unit-body">
              <h4 class="offset-md-top-20"><?php echo Yii::t('app', 'Множество стран');?></h4>
              <p>
                <?php echo Yii::t('app', 'Принять участие может каждый, независимо от страны проживания');?>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Take Full Control of Your Site-->
  <section>
    <!-- RD Parallax-->
    <div data-on="false" data-md-on="true" class="rd-parallax">
      <div data-speed="0.35" data-type="media" data-url="images/backgrounds/background-32-1920x1033.jpg" class="rd-parallax-layer"></div>
      <div data-speed="0" data-type="html" class="rd-parallax-layer">
        <div class="bg-overlay-gray-darkest">
          <div class="shell section-98 section-md-124 context-dark">
            <h2 class="text-bold"><?php echo Yii::t('app', 'Что вы можете получить от проекта?');?></h2>
            <hr class="divider bg-saffron">
            <div class="range offset-top-66">
              <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered  icon-carrot-filled mdi mdi-cash-usd"></span>
                <div>
                  <div class="offset-top-34">
                    <h5 class="text-bold text-spacing-60"><?php echo Yii::t('app', 'Финансы');?></h5>
                  </div>
                  <div class="offset-top-10">
                    <p><?php echo Yii::t('app', 'Решить мелкие финансовые проблемы');?></p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-carrot-filled mdi mdi-repeat-once"></span>
                <div>
                  <div class="offset-top-34">
                    <h5 class="text-bold text-spacing-60"><?php echo Yii::t('app', 'Пассивный доход');?></h5>
                  </div>
                  <div class="offset-top-10">
                    <p><?php echo Yii::t('app', 'Создать постоянный источник дохода');?></p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66 offset-md-top-0">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-carrot-filled mdi mdi-library-books"></span>
                <div>
                  <div class="offset-top-34">
                    <h5 class="text-bold text-spacing-60"><?php echo Yii::t('app', 'Планы');?></h5>
                  </div>
                  <div class="offset-top-10">
                    <p><?php echo Yii::t('app', 'Эффективно реализовать свои планы');?></p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-carrot-filled mdi mdi-nature-people"></span>
                <div>
                  <div class="offset-top-34">
                    <h5 class="text-bold text-spacing-60"><?php echo Yii::t('app', 'Независимость');?></h5>
                  </div>
                  <div class="offset-top-10">
                    <p><?php echo Yii::t('app', 'Стать финансово - независимым');?></p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-carrot-filled mdi mdi-beach"></span>
                <div>
                  <div class="offset-top-34">
                    <h5 class="text-bold text-spacing-60"><?php echo Yii::t('app', 'Путешествие');?></h5>
                  </div>
                  <div class="offset-top-10">
                    <p><?php echo Yii::t('app', 'Работать в любом месте планеты');?></p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-8 cell-sm-preffix-2 cell-md-4 cell-md-preffix-0 offset-top-66">
                <!-- Icon Box Type 4--><span class="icon icon-circle icon-bordered icon-carrot-filled mdi mdi-account-multiple-outline"></span>
                <div>
                  <div class="offset-top-34">
                    <h5 class="text-bold text-spacing-60"><?php echo Yii::t('app', 'Успех');?></h5>
                  </div>
                  <div class="offset-top-10">
                    <p><?php echo Yii::t('app', 'Быть всегда на волне успеха');?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Intense is exactly what you need-->
  <section class="section-66 bg-saffron context-dark">
    <div class="shell">
      <div class="range range-xs-center range-xs-middle">
        <div class="cell-lg-7">
          <h2 class="text-bold"><?php echo Yii::t('app', 'Принять участие в проекте прямо сейчас');?></h2>
        </div>
        <div class="cell-lg-3">
          <a href="<?php echo Url::to(['/profile/profile/signup']);?>" class="btn btn-default btn-icon btn-icon-left"><span class="icon mdi mdi mdi-account-key"></span><span><?php echo Yii::t('app', 'Регистрация');?></span></a>
        </div>
      </div>
    </div>
  </section>

<style>
.mainVideoWrap{
  background: rgba(0, 0, 0, 0) linear-gradient(90deg, #ffa200 0%, #ff7e00 100%) repeat scroll 0 0;
  border-radius: 2px;
  box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.4);
  margin: 0 auto;
  padding: 3px;
  width: 1000px;
  height: 700px;
}

@media (max-width: 799px) { 
  .mainVideoWrap{
    height: 370px;
  }
}
@media (min-width: 800px) { 
  .mainVideoWrap{
    height: 420px;
  }
}
@media (min-width: 1200px) { 
  .mainVideoWrap{
    height: 420px;
  }
}
.mainVideoWrap iframe{
  width: 100%;
  height: 100%;
}
</style>

  <section class="section-98 section-sm-110">
    <div class="shell">
      <h2 class="text-bold"><?php echo Yii::t('app', 'О проекте');?></h2>
      <hr class="divider bg-saffron">
      <div class="range range-xs-center offset-top-34 offset-md-top-66">
        <div class="cell-md-6 mainVideoWrap">
          <iframe src="https://www.youtube.com/embed/c0_ucOk6YsE" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="cell-md-6 text-md-left inset-md-left-50">
          <p><?php echo Yii::t('app', 'Работает по простому принципу: “Пригласи двоих”, которые повторят тебя и получат все. Самый простой маркетинг, созданный специально для того чтобы начать зарабатывать легко и просто. В конечном итоге Вулкан сможет стать для Вас надежным финансовым помощником и духовным наставником. В нем вы сможете найти новых друзей, завести новые деловые контакты, вместе реализовать цели. Что в конечном итоге может дать неограниченные возможности и может расширить ваш горизонт.');?></p>
        </div>
      </div>
    </div>
  </section>
  <!-- Counters-->
  <section>
    <!-- RD Parallax-->
    <div data-on="false" data-md-on="true" class="rd-parallax">
      <div data-speed="0.35" data-type="media" data-url="images/backgrounds/background-33-1920x474.jpg" class="rd-parallax-layer"></div>
      <div data-speed="0" data-type="html" class="rd-parallax-layer">
        <div class="section-66 section-sm-98 bg-overlay-gray-darkest context-dark">
          <div class="shell-wide">
            <div class="range">
              <div class="cell-sm-6 cell-md-3 cell-md-preffix-0">
                <!-- Counter type 1-->
                <div class="counter-type-1">
                  <!--
                  <div class="h1"><span data-step="3000" data-from="0" data-to="58249" class="big counter text-bold text-carrot"></span>
                    <hr class="divider"/>
                  </div>
                  <h6 class="text-uppercase text-spacing-60 font-default"><?php echo Yii::t('app', 'Циферка 1');?></h6>
                  -->
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-66 offset-sm-top-0">
                <!-- Counter type 1-->
                <div class="counter-type-1">
                  <!--
                  <div class="h1"><span data-speed="2500" data-from="0" data-to="246" class="big counter text-bold text-carrot"></span><span class="big text-bold text-carrot">K</span>
                    <hr class="divider"/>
                  </div>
                  <h6 class="text-uppercase text-spacing-60 font-default"><?php echo Yii::t('app', 'Циферка 2');?></h6>
                  -->
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-66 offset-md-top-0">
                <!-- Counter type 1-->
                <div class="counter-type-1">
                  <!--
                  <div class="h1"><span data-step="1500" data-from="0" data-to="1200" class="big counter text-bold text-carrot"></span>
                    <hr class="divider"/>
                  </div>
                  <h6 class="text-uppercase text-spacing-60 font-default"><?php echo Yii::t('app', 'Циферка 3');?></h6>
                  -->
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-66 offset-md-top-0">
                <!-- Counter type 1-->
                <div class="counter-type-1">
                  <!--
                  <div class="h1"><span data-speed="1300" data-from="0" data-to="834" class="big counter text-bold text-carrot"></span><span class="big text-bold text-carrot">K</span>
                    <hr class="divider"/>
                  </div>
                  <h6 class="text-uppercase text-spacing-60 font-default"><?php echo Yii::t('app', 'Циферка 4');?></h6>
                  -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</main>
