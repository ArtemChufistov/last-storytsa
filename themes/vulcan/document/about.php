<?php
use yii\widgets\Breadcrumbs;
use yii\web\View;
use yii\helpers\Url;


$this->params['breadcrumbs']= [ [
    'label' => 'О Нас',
    'url' => '/about'
  ],
];
$this->title = $model->title;
?>

<section class="breadcrumb-classic">
  <div class="shell section-34 section-sm-50">
    <div class="range range-lg-middle">
      <div class="cell-lg-2 veil reveal-sm-block cell-lg-push-2"><span class="mdi mdi-account-convert icon icon-white"></span></div>
      <div class="cell-lg-5 veil reveal-lg-block cell-lg-push-1 text-lg-left">
        <h2><span class="big"><?= Yii::t('app', 'О Нас');?></span></h2>
      </div>
      <div class="offset-top-0 offset-sm-top-10 cell-lg-5 offset-lg-top-0 small cell-lg-push-3 text-lg-right">
        <?= Breadcrumbs::widget([
          'options' => ['class' => 'list-inline list-inline-dashed p'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
      </div>
    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle-bottom">
    <defs>
      <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
        <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
      </lineargradient>
    </defs>
    <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
  </svg>
</section>
<!-- Page Content-->
<main class="page-content">
  <section class="section-98 section-sm-110">
    <div class="shell">
      <h1><?php echo Yii::t('app', 'Почему мы');?></h1>
      <hr class="divider bg-mantis">
      <!-- About Me-->
      <section>
        <div class="range range-xs-center offset-top-66">
          <div class="cell-lg-5 text-lg-right"><img src="images/users/user-john-doe-437x437.jpg" width="437" height="437" alt="" class="img-responsive center-block">
            <div class="range range-xs-center offset-top-34">
              <div class="cell-sm-8 cell-md-6 cell-lg-12">
                <div class="inset-xs-left-80 inset-xs-right-80"><a href="<?php echo Url::to(['profile/profile/signup']);?>" class="btn btn-lg btn-block btn-primary"><?php echo Yii::t('app', 'Присоединиться');?></a></div>
              </div>
            </div>
          </div>
          <div class="cell-sm-10 text-left cell-md-9 offset-top-41 cell-lg-5 offset-lg-top-0">
            <div class="inset-sm-left-30">
              <p><?php echo Yii::t('app', 'Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие.');?></p>
              <p><?php echo Yii::t('app', 'Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие.');?></p>
              <p><?php echo Yii::t('app', 'Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие.');?></p>
              <p><?php echo Yii::t('app', 'Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие. Всякий текст, описание проекта, кто мы такие.');?></p>
            </div>
          </div>
        </div>
      </section>
    </div>
  </section>
</main>