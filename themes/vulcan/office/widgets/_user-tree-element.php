<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\matrix\models\MatrixUserSetting;
?>
<span class="person tooltips" >

	<?php if (!empty($user)):?>
		<div class="box box-widget widget-user-2">
			<div class="widget-user-header bg-maroon">
				<div class="widget-user-image">
					<img class="img-circle imgAvatar" href = "/profile/office/program<?php echo !empty($parentPlace) ? '/'. $parentPlace->slug : '';?><?php echo !empty($currentPlace) ? '/' . $currentPlace->slug : '';?>" src="<?php echo $user->getImage();?>" alt="<?php echo $user->login;?>">
				</div>
				<h3 class="widget-user-username"><?php if(empty($user->first_name) || empty($user->last_name)){echo $user->login;}else{ echo $user->first_name . ' ' . $user->last_name;}?></h3>
				<h5 class="widget-user-desc">
					<?php if ($currentUser->id == $user->id):?>
						<?php echo Yii::t('app', '{num} место', ['num' => $currentPlace->name]);?>
					<?php else:?>
						<?php echo  $user->email;?>
					<?php endif;?>
				</h5>
			</div>
			<div class="box-footer no-padding">
				<ul class="nav nav-stacked">
					<li>
					<?php if (!empty($parentPlace) && $parentPlace->id == $currentPlace->id):?>
						<?php $parentShowPlace = $parentPlace->parent()->one();?>
							<?php if (!empty($parentShowPlace)):?>
								<span class="pull-left badge bg-maroon" style = "margin:1px;"><?php echo Yii::t('app', 'Верхнее место');?>: <?php echo  $parentShowPlace->getUser()->one()->login;?></span> 
							<?php endif;?>
					<?php endif;?>
					<?php if (!empty($user->skype)):?>
						<span class="pull-left badge bg-maroon" style = "margin:1px;"><?php echo Yii::t('app', 'Skype');?>: <?php echo  $user->skype;?></span> 
					<?php endif;?> 
						<span class="pull-left badge bg-maroon" style = "margin:1px;"><?php echo Yii::t('app', 'Логин');?>: <?php echo  $user->login;?></span> 
					<?php if (!empty($user->phone)):?>
						<span class="pull-left badge bg-maroon" style = "margin:1px;"><?php echo Yii::t('app', 'Телефон');?>: <?php echo  $user->phone;?></span>
					<?php endif;?>
					</li>
				</ul>
			</div>
		</div>

    	<img src="<?php echo $user->getImage(); ?>" alt="" class = "imgAvatar" 
    		href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $currentPlace->slug, 'parentPlaceSlug' => $parentPlace->slug]);?>">
    <?php elseif (!empty($ancestorPlace)):?>
    	<?php if ($matrixUserSetting->active == MatrixUserSetting::ACTIVE_TRUE):?>
			<div class="box box-widget widget-user-2">
				<div class="widget-user-header bg-maroon" style = "height:90px;">
					<?php echo Yii::t('app', 'Следующий приглашенный встанет');?>
				</div>
				<div class="box-footer no-padding">
					<?php echo Yii::t('app', 'на это место');?>
				</div>
			</div>
    	<?php else:?>
			<div class="box box-widget widget-user-2">
				<div class="widget-user-header bg-maroon" style = "height:90px;">
					<?php echo Yii::t('app', 'Вы можете поставить место своего приглашённого');?>
				</div>
				<div class="box-footer no-padding">
					<?php echo Yii::t('app', 'или своё дополнительное место');?>
				</div>
			</div>
    	<?php endif;?>

    	<img src="/volcano/whooo.jpg" alt="">
    <?php else:?>
    	<img src="/volcano/whooo.jpg" alt="">
    <?php endif;?>
    
	<?php if (!empty($user)):?>
		<p class="name">
    		<?php echo $user->login;?></br>
    		<b>
				<?php if ($currentUser->id == $user->id):?>
					<?php echo Yii::t('app', '{num} место', ['num' => $currentPlace->name]);?>
				<?php else:?>
					<?php echo  $user->email;?>
				<?php endif;?>
					</br><?php echo date('H:i:s d-m-y', strtotime($currentPlace->date_add));?>
    		</b>
    	</p>
	<?php elseif (!empty($ancestorPlace)):?>
        <?php $form = ActiveForm::begin(['options' => ['class' => 'name']]);?>
            <?= $form->field($matrixUserSetting, 'active', ['template' => '{input}'])->checkBox(['class' => 'minimal-red checkBoxPlace']); ?>
          	<?= $form->field($matrixUserSetting, 'sort', ['template' => '{input}'])->hiddenInput()->label(false); ?>
          	<?= $form->field($matrixUserSetting, 'user_login', ['template' => '{input}'])->hiddenInput()->label(false); ?>
          	<?= $form->field($matrixUserSetting, 'matrix_slug', ['template' => '{input}'])->hiddenInput()->label(false); ?>
          	<?= $form->field($matrixUserSetting, 'matrix_root_slug', ['template' => '{input}'])->hiddenInput()->label(false); ?>
        <?php ActiveForm::end(); ?>
    <?php else:?>
    	<p class="name"><?php echo Yii::t('app', 'Свободное</br> <b>место</b>');?></p>
    <?php endif;?>

</span>
