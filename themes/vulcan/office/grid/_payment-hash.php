<?php
use app\modules\payment\models\PaySystem;
use app\modules\payment\models\CoinbaseTransaction;
?>
<?php if ($model->getPaySystem()->one()->key == PaySystem::KEY_COINBASE):?>
	<?php $coinbaseTransaction = CoinbaseTransaction::find()->where(['payment_id' => $model->id])->one();?>
	<?php if (!empty($coinbaseTransaction)):?>
		<a href = "" class = "showPaymentInfo" paymentHash = "<?php echo $model->hash;?>"><?php echo Yii::t('app', 'Посмотреть');?></a>
	<?php endif;?>
<?php else:?>
	<?php echo $model->transaction_hash;?>
<?php endif;?>