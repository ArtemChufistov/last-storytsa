<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\payment\models\Currency;
use app\modules\payment\models\PaySystem;

?>
<?php if ($paymentForm->isNewRecord):?>
    <?php $form = ActiveForm::begin(); ?>
        <h4><?php echo Yii::t('app', 'Пополнение личного счёта');?></h4>
        </br>
        <?php if ($paymentForm->currency_id == ''):?>

            <?php echo $form->field($paymentForm, 'currency_id')->widget(Select2::classname(), [
                'data' =>  ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Выберите валюту...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-success']) ?>
            </div>
        <?php elseif($paymentForm->sum == ''):?>
            <p><?php echo Yii::t('app', 'Укажите сумму, на которую хотите пополнить свой личный счёт в кабинете');?></p>

            <?= $form->field($paymentForm, 'currency_id')->hiddenInput()->label(false); ?>

            <?= $form->field($paymentForm, 'sum') ?>
        
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-success']) ?>
            </div>
        <?php elseif($paymentForm->pay_system_id == ''):?>
            <p><?php echo Yii::t('app', 'Выберите платёжную систему:');?></p>

            <?= $form->field($paymentForm, 'currency_id')->hiddenInput()->label(false); ?>
            
            <?= $form->field($paymentForm, 'sum')->hiddenInput()->label(false); ?>

            <?php echo $form->field($paymentForm, 'pay_system_id')->widget(Select2::classname(), [
                'data' =>  ArrayHelper::map($paymentForm->getCurrency()->one()->getPaySystems()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Выберите платёжную систему...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-success']) ?>
            </div>

        <?php elseif (!empty($paymentForm->getErrors())):?>
            <?php foreach($paymentForm->getErrors() as $error):?>
                <p><?php echo $error[0];?></p>
            <?php endforeach;?>
        <?php endif;?>
    <?php ActiveForm::end(); ?>
<?php else:?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/payment/_payment-in-' . $paymentForm->getPaySystem()->one()->key, [
        'paymentForm' => $paymentForm
    ]);?>

<?php endif;?>