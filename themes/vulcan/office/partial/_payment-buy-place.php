<?php

use app\modules\matrix\components\StorytsaMarketing;
use app\modules\profile\widgets\UserTreeElementWidget;
use lowbase\user\components\AuthKeysManager;
use lowbase\user\models\Country;
use lowbase\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use lowbase\user\UserAsset;
use yii\web\View;
?>

<p><?php echo Yii::t('app', '<p style = "text-align: justify;">
Платформа Вулкан позволяет обмениваться денежными средствами на добровольных началах физическим и юридическим лицам. Каждый участник платформы получает возможность жертвовать разные суммы другим участникам, присоединившимся к платформе раньше него. Каждый участник также получает возможность иметь пассивный доход за счет пожертвования от других участников, присоединившихся позже.</br>
</br>
Для начала работы вам необходимо оплатить участие в программе. </br>
</br>
Так после того как Вы отправили свое первое пожертвование, для Вас откроются все остальные преимущества системы.</br>
</br>
Работает по простому принципу: “Пригласи двоих”, которые повторят тебя и получат все. Самый простой маркетинг, созданный специально для того чтобы начать зарабатывать легко и просто. В конечном итоге Вулкан сможет стать для Вас надежным финансовым помощником и духовным наставником. В нем вы сможете найти новых друзей, завести новые деловые контакты, вместе реализовать цели. Что в конечном итоге может дать неограниченные возможности и может расширить ваш горизонт.
</br>
</br>
</p>');?></p>
<strong>
<?php if ($matrixCategory->name == 'Предварительная программа'):?>
	<strong>Стоимость участия 0.0055 BTC</strong>
<?php else:?>
	<strong>Стоимость участия 0.077 BTC</strong>
<?php endif;?>
</strong>
</br>
</br>
<?php if ($matrixCategory->name == 'Предварительная программа'):?>
	<div class="col-md-6">
		<button href = "#" class = "btn btn-block btn-success pull-left" data-toggle="modal" data-target="#modalSubmitProgram1"><i class="fa fa-check" aria-hidden="true"></i>
			<?php echo Yii::t('app', 'Принять участие в Asama');?>
		</button>
	</div>
	<div class="col-md-6">
		<button href = "#" class = "btn btn-block btn-warning pull-left" data-toggle="modal" data-target="#modalSubmitProgram2"><i class="fa fa-check" aria-hidden="true"></i>
			<?php echo Yii::t('app', 'Принять участие в Cerro Bravo');?>
		</button>
	</div>
<?php else:?>
	<div class="col-md-6">
		<button href = "#" class = "btn btn-block btn-info pull-left" data-toggle="modal" data-target="#modalSubmitProgram3"><i class="fa fa-check" aria-hidden="true"></i>
			<?php echo Yii::t('app', 'Принять участие в Kilimanjaro');?>
		</button>
	</div>
	<div class="col-md-6">
		<button href = "#" class = "btn btn-block bg-purple pull-left" data-toggle="modal" data-target="#modalSubmitProgram4"><i class="fa fa-check" aria-hidden="true"></i>
			<?php echo Yii::t('app', 'Принять участие в lascar');?>
		</button>
	</div>
<?php endif;?>

  <div class="modal" id = "modalSubmitProgram1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
	        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете участие в программе?');?> </br> 
			<strong>Asama</strong>
			</br>
			</br>
	        </h4>
			<?php $form = ActiveForm::begin(); ?>
				<?php echo Html::hiddenInput('marketing-matrix[rootId]', $rootMatrixes[0]->id);?>
				<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
				  'class' => 'btn btn-block btn-success pull-left',
				  'value' => 1,
				  'name' => 'marketing-matrix[submit]']) ?>
			<?php ActiveForm::end();?>
			</br>
			</br>
			<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
			<div style="clear: both;"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id = "modalSubmitProgram2">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
	        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете участие в программе?');?> </br> 
			<strong>Cerro Bravo</strong>
			</br>
			</br>
	        </h4>
			<?php $form = ActiveForm::begin(); ?>
				<?php echo Html::hiddenInput('marketing-matrix[rootId]', $rootMatrixes[2]->id);?>
				<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
				  'class' => 'btn btn-block btn-success pull-left',
				  'value' => 1,
				  'name' => 'marketing-matrix[submit]']) ?>
			<?php ActiveForm::end();?>
			</br>
			</br>
			<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
			<div style="clear: both;"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id = "modalSubmitProgram3">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
	        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете участие в программе?');?> </br> 
			<strong>Kilimanjaro</strong>
			</br>
			</br>
	        </h4>
			<?php $form = ActiveForm::begin(); ?>
				<?php echo Html::hiddenInput('marketing-matrix[rootId]', $rootMatrixes[0]->id);?>
				<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
				  'class' => 'btn btn-block btn-success pull-left',
				  'value' => 1,
				  'name' => 'marketing-matrix[submit]']) ?>
			<?php ActiveForm::end();?>
			</br>
			</br>
			<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
			<div style="clear: both;"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id = "modalSubmitProgram4">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
	        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете участие в программе?');?> </br> 
			<strong>lascar</strong>
			</br>
			</br>
	        </h4>
			<?php $form = ActiveForm::begin(); ?>
				<?php echo Html::hiddenInput('marketing-matrix[rootId]', $rootMatrixes[1]->id);?>
				<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
				  'class' => 'btn btn-block btn-success pull-left',
				  'value' => 1,
				  'name' => 'marketing-matrix[submit]']) ?>
			<?php ActiveForm::end();?>
			</br>
			</br>
			<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
			<div style="clear: both;"></div>
        </div>
      </div>
    </div>
  </div>
