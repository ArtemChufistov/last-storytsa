<?php
use app\modules\profile\widgets\UserTreeElementWidget;
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\matrix\models\MatrixUserSettingForm;
use app\modules\matrix\models\MatrixUserSetting;
use app\modules\matrix\models\MatrixPref;
use app\modules\matrix\models\Matrix;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

ini_set('memory_limit', '-1');
?>

<div class="cf nestable-lists">
    <ol class="dd-list">
        <?php for($lvlCtr1 = 0; $lvlCtr1 < $countChildren->value ; $lvlCtr1++){?>
            <?php $lvl1 = 1;$childLvl1 = Matrix::find()->where(['parent_id' => $currentPlace->id])->andWhere(['sort' => $lvlCtr1])->one();?>
            <?php if (empty($childLvl1)):?>
                <?php $matrixUserSetting = MatrixUserSettingForm::find()->where([
                    'matrix_root_id' => $currentPlace->root_id,
                    'matrix_id' => $currentPlace->id, 
                    'user_id' => $user->id,
                    'active' => MatrixUserSetting::ACTIVE_TRUE,
                    'sort' => $lvlCtr1,
                    ])->one()
                ;?>
                <li class="dd-item dd3-item">
                    <div class="dd-handle dd3-handle"><?php echo $lvl1; ?></div><div class="dd3-content freePlace">
                        <?php echo Yii::t('app', 'Свободное место');?>
                        <?php if (!empty($matrixUserSetting)):?>
                            <?php echo Yii::t('app', '(Следующий приглашенный встанет на это место)');?>
                        <?php endif;?>
                    </div>
                </li>
            <?php else:?>
                <li class="dd-item dd3-item" >
                    <div class="dd-handle dd3-handle"><?php echo $lvl1; ?></div><div class="dd3-content">
                        <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $childLvl1->slug, 'parentPlaceSlug' => $currentPlace->slug]);?>">
                            <?php echo $childLvl1->getUser()->one()->login;?>
                        </a>
                    </div>
                    <ol class="dd-list">
                        <?php for($lvlCtr2 = 0; $lvlCtr2 < $countChildren->value ; $lvlCtr2++){?>
                            <?php $lvl2 = 2;$childLvl2 = Matrix::find()->where(['parent_id' => $childLvl1->id])->andWhere(['sort' => $lvlCtr2])->one();?>
                            <?php if (empty($childLvl2)):?>
                                <?php $matrixUserSetting = MatrixUserSettingForm::find()->where([
                                    'matrix_root_id' => $currentPlace->root_id,
                                    'matrix_id' => $childLvl2->id, 
                                    'user_id' => $user->id,
                                    'active' => MatrixUserSetting::ACTIVE_TRUE,
                                    'sort' => $lvlCtr2,
                                    ])->one()
                                ;?>
                                <li class="dd-item dd3-item">
                                    <div class="dd-handle dd3-handle"><?php echo $lvl2; ?></div><div class="dd3-content freePlace">
                                        <?php echo Yii::t('app', 'Свободное место');?>
                                        <?php if (!empty($matrixUserSetting)):?>
                                            <?php echo Yii::t('app', '(Следующий приглашенный встанет на это место)');?>
                                        <?php endif;?>
                                    </div>
                                </li>
                            <?php else:?>
                                <li class="dd-item dd3-item" >
                                    <div class="dd-handle dd3-handle"><?php echo $lvl2; ?></div>
                                        <div class="dd3-content">
                                            <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $childLvl2->slug, 'parentPlaceSlug' => $currentPlace->slug]);?>">
                                                <?php echo $childLvl2->getUser()->one()->login;?>
                                            </a>
                                        </div>
                                    <ol class="dd-list">
                                        <?php for($lvlCtr3 = 0; $lvlCtr3 < $countChildren->value ; $lvlCtr3++){?>
                                            <?php $lvl3 = 3;$childLvl3 = Matrix::find()->where(['parent_id' => $childLvl2->id])->andWhere(['sort' => $lvlCtr3])->one();?>
                                            <?php if (empty($childLvl3)):?>
                                                <?php $matrixUserSetting = MatrixUserSettingForm::find()->where([
                                                    'matrix_root_id' => $currentPlace->root_id,
                                                    'matrix_id' => $childLvl3->id, 
                                                    'user_id' => $user->id,
                                                    'active' => MatrixUserSetting::ACTIVE_TRUE,
                                                    'sort' => $lvlCtr3,
                                                    ])->one()
                                                ;?>
                                                <li class="dd-item dd3-item">
                                                    <div class="dd-handle dd3-handle"><?php echo $lvl3; ?></div><div class="dd3-content freePlace">
                                                        <?php echo Yii::t('app', 'Свободное место');?>
                                                        <?php if (!empty($matrixUserSetting)):?>
                                                            <?php echo Yii::t('app', '(Следующий приглашенный встанет на это место)');?>
                                                        <?php endif;?>
                                                    </div>
                                                </li>
                                            <?php else:?>
                                                <li class="dd-item dd3-item" >
                                                    <div class="dd-handle dd3-handle"><?php echo $lvl3; ?></div>
                                                        <div class="dd3-content">
                                                            <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $childLvl3->slug, 'parentPlaceSlug' => $currentPlace->slug]);?>">
                                                                <?php echo $childLvl3->getUser()->one()->login;?>
                                                            </a>
                                                        </div>
                                                    <ol class="dd-list">
                                                        <?php for($lvlCtr4 = 0; $lvlCtr4 < $countChildren->value ; $lvlCtr4++){?>
                                                            <?php $lvl4 = 4;$childLvl4 = Matrix::find()->where(['parent_id' => $childLvl3->id])->andWhere(['sort' => $lvlCtr4])->one();?>
                                                            <?php if (empty($childLvl4)):?>
                                                                <?php $matrixUserSetting = MatrixUserSettingForm::find()->where([
                                                                    'matrix_root_id' => $currentPlace->root_id,
                                                                    'matrix_id' => $childLvl4->id, 
                                                                    'user_id' => $user->id,
                                                                    'active' => MatrixUserSetting::ACTIVE_TRUE,
                                                                    'sort' => $lvlCtr4,
                                                                    ])->one()
                                                                ;?>
                                                                <li class="dd-item dd3-item">
                                                                    <div class="dd-handle dd3-handle"><?php echo $lvl4; ?></div><div class="dd3-content freePlace">
                                                                        <?php echo Yii::t('app', 'Свободное место');?>
                                                                        <?php if (!empty($matrixUserSetting)):?>
                                                                            <?php echo Yii::t('app', '(Следующий приглашенный встанет на это место)');?>
                                                                        <?php endif;?>
                                                                    </div>
                                                                </li>
                                                            <?php else:?>
                                                                <li class="dd-item dd3-item" >
                                                                    <div class="dd-handle dd3-handle"><?php echo $lvl4; ?></div>
                                                                    <div class="dd3-content">
                                                                        <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $childLvl4->slug, 'parentPlaceSlug' => $currentPlace->slug]);?>">
                                                                            <?php echo $childLvl4->getUser()->one()->login;?>
                                                                        </a>
                                                                    </div>
                                                                    <ol class="dd-list">
                                                                        <?php for($lvlCtr5 = 0; $lvlCtr5 < $countChildren->value ; $lvlCtr5++){?>
                                                                            <?php $lvl5 = 5;$childLvl5 = Matrix::find()->where(['parent_id' => $childLvl4->id])->andWhere(['sort' => $lvlCtr5])->one();?>
                                                                            <?php if (empty($childLvl5)):?>
                                                                                <?php $matrixUserSetting = MatrixUserSettingForm::find()->where([
                                                                                    'matrix_root_id' => $currentPlace->root_id,
                                                                                    'matrix_id' => $childLvl5->id, 
                                                                                    'user_id' => $user->id,
                                                                                    'active' => MatrixUserSetting::ACTIVE_TRUE,
                                                                                    'sort' => $lvlCtr5,
                                                                                    ])->one()
                                                                                ;?>
                                                                                <li class="dd-item dd3-item">
                                                                                    <div class="dd-handle dd3-handle"><?php echo $lvl5; ?></div><div class="dd3-content freePlace">
                                                                                        <?php echo Yii::t('app', 'Свободное место');?>
                                                                                        <?php if (!empty($matrixUserSetting)):?>
                                                                                            <?php echo Yii::t('app', '(Следующий приглашенный встанет на это место)');?>
                                                                                        <?php endif;?>
                                                                                    </div>
                                                                                </li>
                                                                            <?php else:?>
                                                                                <li class="dd-item dd3-item" >
                                                                                    <div class="dd-handle dd3-handle"><?php echo $lvl5; ?></div>
                                                                                        <div class="dd3-content">
                                                                                            <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $childLvl5->slug, 'parentPlaceSlug' => $currentPlace->slug]);?>">
                                                                                                <?php echo $childLvl5->getUser()->one()->login;?>
                                                                                            </a>
                                                                                        </div>
                                                                                    <ol class="dd-list">
                                                                                        <?php for($lvlCtr6 = 0; $lvlCtr6 < $countChildren->value ; $lvlCtr6++){?>
                                                                                            <?php $lvl6 = 6;$childLvl6 = Matrix::find()->where(['parent_id' => $childLvl5->id])->andWhere(['sort' => $lvlCtr6])->one();?>
                                                                                            <?php if (empty($childLvl6)):?>
                                                                                                <?php $matrixUserSetting = MatrixUserSettingForm::find()->where([
                                                                                                    'matrix_root_id' => $currentPlace->root_id,
                                                                                                    'matrix_id' => $childLvl6->id, 
                                                                                                    'user_id' => $user->id,
                                                                                                    'active' => MatrixUserSetting::ACTIVE_TRUE,
                                                                                                    'sort' => $lvlCtr6,
                                                                                                    ])->one()
                                                                                                ;?>
                                                                                                <li class="dd-item dd3-item">
                                                                                                    <div class="dd-handle dd3-handle"><?php echo $lvl6; ?></div><div class="dd3-content freePlace">
                                                                                                        <?php echo Yii::t('app', 'Свободное место');?>
                                                                                                        <?php if (!empty($matrixUserSetting)):?>
                                                                                                            <?php echo Yii::t('app', '(Следующий приглашенный встанет на это место)');?>
                                                                                                        <?php endif;?>
                                                                                                    </div>
                                                                                                </li>
                                                                                            <?php else:?>
                                                                                                <li class="dd-item dd3-item" >
                                                                                                    <div class="dd-handle dd3-handle"><?php echo $lvl6; ?></div>
                                                                                                    <div class="dd3-content">
                                                                                                        <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $childLvl6->slug, 'parentPlaceSlug' => $currentPlace->slug]);?>">
                                                                                                            <?php echo $childLvl6->getUser()->one()->login;?>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <ol class="dd-list">
                                                                                                        <?php for($lvlCtr7 = 0; $lvlCtr7 < $countChildren->value ; $lvlCtr7++){?>
                                                                                                            <?php $lvl7 = 7;$childLvl7 = Matrix::find()->where(['parent_id' => $childLvl6->id])->andWhere(['sort' => $lvlCtr7])->one();?>
                                                                                                            <?php if (empty($childLvl7)):?>
                                                                                                                <?php $matrixUserSetting = MatrixUserSettingForm::find()->where([
                                                                                                                    'matrix_root_id' => $currentPlace->root_id,
                                                                                                                    'matrix_id' => $childLvl7->id, 
                                                                                                                    'user_id' => $user->id,
                                                                                                                    'active' => MatrixUserSetting::ACTIVE_TRUE,
                                                                                                                    'sort' => $lvlCtr7,
                                                                                                                    ])->one()
                                                                                                                ;?>
                                                                                                                <li class="dd-item dd3-item">
                                                                                                                    <div class="dd-handle dd3-handle"><?php echo $lvl7; ?></div><div class="dd3-content freePlace">
                                                                                                                        <?php echo Yii::t('app', 'Свободное место');?>
                                                                                                                        <?php if (!empty($matrixUserSetting)):?>
                                                                                                                            <?php echo Yii::t('app', '(Следующий приглашенный встанет на это место)');?>
                                                                                                                        <?php endif;?>
                                                                                                                    </div>
                                                                                                                </li>
                                                                                                            <?php else:?>
                                                                                                                <li class="dd-item dd3-item" >
                                                                                                                    <div class="dd-handle dd3-handle"><?php echo $lvl7; ?></div><div class="dd3-content">
                                                                                                                        <a href = "<?php echo Url::to(['/profile/office/program', 'matrixCategory' => $matrixCategory->id, 'matrix' => $rootMatrix->slug, 'placeSlug' => $childLvl7->slug, 'parentPlaceSlug' => $currentPlace->slug]);?>">
                                                                                                                            <?php echo $childLvl7->getUser()->one()->login;?>
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                </li>
                                                                                                            <?php endif;?>
                                                                                                        <?php };?>
                                                                                                    </ol>
                                                                                                </li>
                                                                                            <?php endif;?>
                                                                                        <?php };?>
                                                                                    </ol>
                                                                                </li>
                                                                            <?php endif;?>
                                                                        <?php };?>
                                                                    </ol>
                                                                </li>
                                                            <?php endif;?>
                                                        <?php };?>
                                                    </ol>
                                                </li>
                                            <?php endif;?>
                                        <?php };?>
                                    </ol>
                                </li>
                            <?php endif;?>
                        <?php };?>
                    </ol>
                </li>
            <?php endif;?>
        <?php };?>
    </ol>
</div>