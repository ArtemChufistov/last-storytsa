<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="box box-danger userInfoPartial" login=<?php echo $user->login?>>
  <div class="box-header with-border">
    <h3 class="box-title"><?php echo Yii::t('app', 'Информация об участнике');?> <?php echo $user->login;?></h3>
    <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
      <div class="btn-group" data-toggle="btn-toggle">
        <button class="btn btn-danger btn-sm" type="button" data-widget="remove">
        <i class="fa fa-times"></i>
        </button>
      </div>
    </div>
  </div>
  <div class="box-body">
  	<p>Имя: <?php echo $user->first_name;?></p>
  	<p>Фамилия: <?php echo $user->last_name;?></p>
    <p>E-mail: <?php echo $user->email;?></p>
    <p>Skype: <?php echo $user->skype;?></p>
  	<p>Телефон: <?php echo $user->phone;?></p>
  </div>
</div>