<nav>
  <ul>
    <?php foreach($menu->children()->all() as $num => $children):?>
      <li><a href="<?= $children->link;?>"><?= $children->title;?></a></li>
      <?php if (!empty($children->children()->all())):?>
        <ul>
          <?php foreach($children->children()->all() as $numChild => $childChildren):?>
            <li><a href="<?= $childChildren->link;?>"><?= $childChildren->title;?></a>
              <ul>
                <li><a href=""><?= $childChildren->title;?></a>
                </li>
              </ul>
            </li>
          <?php endforeach;?>
        </ul>
      <?php endif;?>
    <?php endforeach;?>
  </ul>
  <div class="search">
    <input type="text" name="search-box" id="search-box" placeholder="Поиск..." />
    <input type="submit" value="&#xf002;" />
  </div>
</nav>