<div class="slider-wrapper">
  <div class="slider">
    <ul>
      <li>
        <h1>Займы для бизнеса и стартапов</h1>
        <h2>Краткосрочные займы на любые цели от 5 000 до 100 000 рублей</h2>

        <a href="#" class="slider-button">Оформить заявку</a>
      </li>
      <li>
        <h1>Займы для бизнеса и стартапов 2</h1>
        <h2>Краткосрочные займы на любые цели от 5 000 до 100 000 рублей</h2>

        <a href="#" class="slider-button">Оформить заявку</a>
      </li>
      <li>
        <h1>Займы для бизнеса и стартапов 3</h1>
        <h2>Краткосрочные займы на любые цели от 5 000 до 100 000 рублей</h2>

        <a href="#" class="slider-button">Оформить заявку</a>
      </li>
    </ul>
  </div>
  <a href="#" id="left-chevron" class="left-chevron"><i class="fa fa-chevron-circle-left fa-3x" aria-hidden="true"></i></a>
  <a href="#" id="right-chevron" class="right-chevron"><i class="fa fa-chevron-circle-right fa-3x" aria-hidden="true"></i></a>
  <ul class="dots">
    <a href="#" class="active"><li></li></a>
    <a href="#"><li></li></a>
    <a href="#"><li></li></a>
  </ul>
</div>

<div class="info">
  <div class="calculator">
    <h1>Рассчитать займ</h1>
    <form>
      <div class="radio-group">
        <div class="radio-field">
          <input type="radio" name="experience" value="new" checked />
          <label>Новый клиент</label>
        </div>
        <div class="radio-field">
          <input type="radio" name="experience" value="existing" />
          <label>Уже брал займ в Рус Финанс</label>
        </div>
      </div>
      <div class="range-field">
        <label>Хочу занять</label>
        <input name="sum" id="sum" type="range" min="100" max="1100" step="100" value="1000" />
        <span class="amount">1000 руб</span>
      </div>
      <div class="range-field">
        <label>На срок до</label>
        <input name="time" id="time" type="range" min="1" max="50" step="1" value="45" />
        <span class="amount">45 дней</span>
      </div>

      <p><em>Переплатите:</em><span id="overpay">1680</span> <i class="fa fa-rub" aria-hidden="true"></i></p>
      <p><em>Вернете:</em><span id="return">11330</span> <i class="fa fa-rub" aria-hidden="true"></i></p>
      <p><em>До:</em><span id="date">19.12.2016</span></p>

      <div class="action-links">
        <a href="#" class="calculator-request-button"><i class="fa fa-bell-o fa-lg" aria-hidden="true"></i> Оформить заявку</a>
        <a href="#" class="more-link">Подробнее >></a>
      </div>
    </form>
  </div>
  <div class="infographics">
    <div class="steps">
      <h1>Как мы работаем</h1>
      <ul>
        <li>
          <h1>1</h1>
          <p>
            Заполните анкету
          </p>
        </li>
        <li>
          <h1>2</h1>
          <p>
            Одобрение в течение 1 часа
          </p>
        </li>
        <li>
          <h1>3</h1>
          <p>
            Получите займ
          </p>
        </li>
      </ul>
    </div>
    <div class="video">
      <a href="#">
        <img src="images/video-preview.png" class="video-preview" />
      </a>
    </div>
  </div>
</div>

<div class="about">
  <h1>Микрофинансовая организация Рус Финанс</h1>

  <p>Микрофинансовая организация «Рус Финанс» уже более 3 лет успешно работает на российском рынке, поступательно развиваясь и совершенствуясь.</p>
  <p>С момента открытия мы усердно работали, прогнозировали тенденции, предвосхищали события, умело лавировали в мощном финансовом потоке современного рынка, искали индивидуальные подходы к экономическим потребностям наших клиентов и всегда старались давать грамотные и максимально полезные советы. Высокая трудоспособность, следование изначально установленным принципам и максимальная клиентоориентированность, помогли «Рус Финанс» пройти свой собственный путь становления и прочно укрепиться на экспертной позиции в микрофинансовом мире.</p>
  <p>Компания «Рус Финанс» постоянно развивается, ищет новые идеи, обменивается опытом с коллегами и участвует в важных финансовых мероприятих международного значения. <a href="#" class="about-more-link">Подробнее >></a></p>
</div>

<div class="news">
  <h1>Новости и акции</h1>

  <div class="news-grid">
    <div class="article">
      <div class="details">
        <img src="images/first-article.png" class="article-image" />
        <div class="titles">
          <h1>Александр Шустов выступил на XV Национальной конференции по микрофинансированию и финансовой доступности.</h1>
          <h2>22 ноября 2016</h2>
        </div>
      </div>
      <p>
        16-17 ноября 2016 года в Санкт-Петербурге проходила XV Национальная Конференция по микрофинансированию и финансовой доступности «Микрофинансирование. ...
      </p>
      <p>
        <a href="#">Подробнее >></a>
      </p>
    </div>
    <div class="article">
      <div class="details">
        <img src="images/second-article.png" class="article-image" />
        <div class="titles">
          <h1>Минск встал: «Почему немного снега зимой становится проблемой?!» фото</h1>
          <h2>21 ноября 2016</h2>
        </div>
      </div>
      <p>
        Движение транспорта в разных районах Минскаостановилось, хотя в городе, со слов директора «Минского автодора», работают девять снегоуборочных машин и четыре самосвала с подсы...
      </p>
      <p>
        <a href="#">Подробнее >></a>
      </p>
    </div>
  </div>
</div>