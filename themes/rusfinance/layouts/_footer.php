<footer>
  <div class="container">
    <ul class="prefooter">
      <li>
        <h1>О компании</h1>

        <ul class="links">
          <li><a href="#">Общая информация</a></li>
          <li><a href="#">Руководство</a></li>
          <li><a href="#">Реквизиты</a></li>
          <li><a href="#">Учредительные документы</a></li>
          <li><a href="#">Свидетельство ЦБ</a></li>
          <li><a href="#">Новости</a></li>
        </ul>
      </li>
      <li>
        <h1>Микрозаймы</h1>

        <ul class="links">
          <li><a href="#">Получить займ</a></li>
          <li><a href="#">Займ для бизнеса</a></li>
          <li><a href="#">Оплатить займ</a></li>
          <li><a href="#">Калькулятор микрозаймов</a></li>
          <li><a href="#">Законодательство</a></li>
          <li><a href="#">Правила предоставления</a></li>
        </ul>
      </li>
      <li>
        <h1>Оформить заявку</h1>

        <ul class="links">
          <li><a href="#">На размещение денег</a></li>
          <li><a href="#">Заявка на займ</a></li>
          <li><a href="#">Займ для бизнеса</a></li>
        </ul>
      </li>
      <li>
        <h1>Инвестиции</h1>

        <h1><a href="#">Отделения</a></h1>
      </li>
    </ul>
    <div class="contacts">
      <div class="copyrights">
        <p>© Микрофинансовая организация «РУС ФИНАНС», 2013-2017</p>
        <p>Свидетельство ЦБ РФ №6514030450234405328</p>
        <p>Использование материалов сайта только при условии прямой ссылки на источник.</p>
      </div>
      <ul class="social-buttons">
        <li>
          <a href="#"><img src="images/facebook-small-icon.png"></a>
        </li>
        <li>
          <a href="#"><img src="images/twitter-small-icon.png"></a>
        </li>
        <li>
          <a href="#"><img src="images/linkedin-small-icon.png"></a>
        </li>
        <li>
          <a href="#"><img src="images/vk-small-icon.png"></a>
        </li>
        <li>
          <a href="#"><img src="images/ok-small-icon.png"></a>
        </li>
        <li>
          <a href="#"><img src="images/youtube-small-icon.png"></a>
        </li>
    </ul>
    </div>
  </div>
</footer>