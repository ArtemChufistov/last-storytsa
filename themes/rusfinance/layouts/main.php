<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\FrontendAppAsset;
use app\modules\menu\widgets\MenuWidget;

FrontendAppAsset::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=805, initial-scale=1.0">
    <title>Рус Финанс</title>
    <link rel="stylesheet" href="/css/index.css">
</head>

<body>
    <header>
        <div class="container">
            <ul>
              <li>
                <i class="fa fa-thumbs-o-up fa-lg" aria-hidden="true"></i> Комания 2016 года он-лайн микрокредитования
              </li>
              <li>
                <i class="fa fa-cc-mastercard fa-lg" aria-hidden="true"></i> Выдано займов на сумму 1 500 151 020 <i class="fa fa-rub" aria-hidden="true"></i>
              </li>
              <li>
                <a href="#"><i class="fa fa-user fa-lg" aria-hidden="true"></i> Личный кабинет</a>
              </li>
            </ul>
        </div>
    </header>

    <div class="content">
      <div class="container">
        <div class="header-grid">
          <a href = "/"><img src="images/logo.png" class="logo" /></a>
          <a href="#" class="request-button"><i class="fa fa-bell-o fa-lg" aria-hidden="true"></i> Оформить заявку</a>
          <div class="app-grid">
            <a href="#" class="google-app-link">
              <p><small>Доступно в</small></p>
              <p><big>Google Play</big></p>
            </a>
            <a href="#" class="apple-app-link">
              <p><small>Доступно в</small></p>
              <p><big>App Store</big></p>
            </a>
          </div>
        </div>

        <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']);?>

        <?= $content ?>

      </div>
    </div>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/_footer');?>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://use.fontawesome.com/934ebf4b5c.js"></script>
<script src="/js/index.js"></script>

</html>

<?php $this->endPage() ?>