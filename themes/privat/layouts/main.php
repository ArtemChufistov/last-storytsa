<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\BetexproFrontAppAsset;
use app\modules\menu\widgets\MenuWidget;

BetexproFrontAppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
  <head>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_head_meta');?>
    <link rel="stylesheet" href="/css/font-awesome.css">
    <?php $this->head() ?>
  </head>
  <body>
    <?php $this->beginBody() ?>

    <div class="page text-center">
      <?php echo MenuWidget::widget(['menuName' => 'topMenu', 'view' => 'main-menu']);?>

      <?= $content ?>

      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_footer');?>

    </div>

    <?php $this->endBody() ?>

<script>
(function ($) {

// SIGNATURE PROGRESS
function progressBar() {
    console.log("progressBar");

    var animationLength = $('.progress-wrap').data('speed');
    var i = 1;
    $('.progress-bar').each(function(){
        var getPercent = $(this).data('progress-percent');
        $(this).stop().animate({
            width: getPercent
        }, animationLength);
        i++;
    });

    $('.progress-count').each(function(){

        var i = 1;
        var count = $(this).data('progress-count');
        var step = animationLength / count;
        var that = $(this);

        var int = setInterval(function () {
            if (i <= count) {
                that.html(i + '%');
            }
            else {
                clearInterval(int);
            }
            i++;
        }, step);
    });
}
(function ($) {
    var wr = $('.progress-wrap');
    var cc = 1;

    $(window).scroll(function(){
        var targetPos = wr.offset().top;
        var targetPosBottom = wr.offset().top + wr.outerHeight();
        var winHeight = $(window).height();
        var scrollToElem = targetPosBottom - winHeight;
        var winScrollTop = $(this).scrollTop();

        if (winScrollTop > scrollToElem) {
            if(targetPos > winScrollTop){
                if (cc < 2){
                    $(document).ready(function () {
                        progressBar();
                    });
                    cc = cc + 2;
                }
            }
        }
    });
})(jQuery);

})(jQuery);


/* Progress Input
 ========================================================*/
;
(function ($) {

  var percent = 0.0087;
  var summ = 500000;

  function recalculate() {
      $('#summ-error').slideUp('fast');
      $('#daily-bar, #monthly-bar, #annually-bar').find('span').hide();
      $('#daily-bar').animate({width: (55 * percent / 0.0095) + '%'});
      $('#monthly-bar').animate({width: (77 * percent / 0.0095) + '%'});
      $('#annually-bar').animate({width: (100 * percent / 0.0095) + '%'});
      $('#daily-bar').find('span').html((summ / (5*30)).toFixed(2) + ' USD');
      $('#monthly-bar').find('span').html((summ / 5).toFixed(2) + ' USD');
      $('#annually-bar').find('span').html((summ * 2.4).toFixed(2) + ' USD');
      $('#daily-bar, #monthly-bar, #annually-bar').find('span').show();
  }

  $('input[name=summ]').on('input', function(e) {
    if ($('.progress-wrap .btn.btn-primary').length)
      recalculate();
  });

  $('#100tariff').click(function(e) {
    e.preventDefault();
    $('.progress-wrap .btn.btn-primary').removeClass('btn-primary').addClass('btn-gray');
    $(e.currentTarget).removeClass('btn-gray').addClass('btn-primary');
    summ = 100;
    percent = 0.0065;
    recalculate();
  });

  $('#500tariff').click(function(e) {
    e.preventDefault();
    $('.progress-wrap .btn.btn-primary').removeClass('btn-primary').addClass('btn-gray');
    $(e.currentTarget).removeClass('btn-gray').addClass('btn-primary');
    summ = 500;
    percent = 0.0087;
    recalculate();
  });

  $('#1000tariff').click(function(e) {
    e.preventDefault();
    $('.progress-wrap .btn.btn-primary').removeClass('btn-primary').addClass('btn-gray');
    $(e.currentTarget).removeClass('btn-gray').addClass('btn-primary');
    summ = 1000;
    percent = 0.0099;
    recalculate();
  });

  $('#3000tariff').click(function(e) {
    e.preventDefault();
    $('.progress-wrap .btn.btn-primary').removeClass('btn-primary').addClass('btn-gray');
    $(e.currentTarget).removeClass('btn-gray').addClass('btn-primary');
    summ = 3000;
    percent = 0.0112;
    recalculate();
  });

  $('#5000tariff').click(function(e) {
    e.preventDefault();
    $('.progress-wrap .btn.btn-primary').removeClass('btn-primary').addClass('btn-gray');
    $(e.currentTarget).removeClass('btn-gray').addClass('btn-primary');
    summ = 5000;
    percent = 0.0132;
    recalculate();
  });

  $('#10000tariff').click(function(e) {
    e.preventDefault();
    $('.progress-wrap .btn.btn-primary').removeClass('btn-primary').addClass('btn-gray');
    $(e.currentTarget).removeClass('btn-gray').addClass('btn-primary');
    summ = 10000;
    percent = 0.0162;
    recalculate();
  });

  $(document).ready(function() {
    $('#500tariff').trigger('click');
  });

})(jQuery);

</script>

  </body>
</html>
<?php $this->endPage() ?>