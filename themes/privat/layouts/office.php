<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\OfficeHeadAppAsset;
use app\assets\OfficeEndAppAsset;
use app\modules\menu\widgets\MenuWidget;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\widgets\ChangeEmailWidget;

OfficeHeadAppAsset::register($this);
OfficeEndAppAsset::register($this);

?>

<?php $this->beginPage() ?>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="/investprivat/favicon.ico" type="image/x-icon"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="/css/font-awesome.css">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

<script>
  $.noConflict();
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">

  <header class="main-header">
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_office_head', ['user' => $this->params['user']]);?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_user_panel', ['user' => $this->params['user']]);?>

      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Поиск...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>

      <?php echo MenuWidget::widget(['menuName' => 'profileMenu', 'view' => 'office-menu']);?>

    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $this->title;?>
      </h1>
      <?php 
        $this->params['breadcrumbs'][] = ['label' => 'Офис','url' => '/profile/office/index',];
        $this->params['breadcrumbs'] = array_reverse($this->params['breadcrumbs']);
        echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],])
      ?>

    </section>

    <section class="content">
      <?php echo $content;?>
    </section>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2017 <a href="<?php echo Url::home();?>">Investprivat.com</a>.</strong> All rights
    reserved.
    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/_yandex_metrika-footer');?>
  </footer>

</div>

<?= ChangeEmailWidget::widget(['user' => $this->params['user']->identity]) ?>

<?php $this->registerJs('
jQuery(\'input[type="checkbox"].minimal, input[type="radio"].minimal\').iCheck({
  checkboxClass: \'icheckbox_minimal-blue\',
  radioClass: \'iradio_minimal-blue\'
});
//Red color scheme for iCheck
jQuery(\'input[type="checkbox"].minimal-red, input[type="radio"].minimal-red\').iCheck({
  checkboxClass: \'icheckbox_minimal-red\',
  radioClass: \'iradio_minimal-red\'
});
//Flat red color scheme for iCheck
jQuery(\'input[type="checkbox"].flat-red, input[type="radio"].flat-red\').iCheck({
  checkboxClass: \'icheckbox_flat-green\',
  radioClass: \'iradio_flat-green\'
});
//jQuery.widget.bridge(\'uibutton\', $.ui.button);');?>
</script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>