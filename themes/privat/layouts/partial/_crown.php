<button data-rd-navbar-toggle=".rd-navbar-panel-inner" type="submit" class="rd-navbar-collapse-toggle"><span></span></button>
<div class="bg-white">
  <div class="rd-navbar-inner">
    <div class="rd-navbar-top-panel">
      <!-- RD Navbar Panel-->
      <div class="rd-navbar-panel">
        <!-- RD Navbar Toggle-->
        <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
        <!-- RD Navbar Brand-->
        <div class="rd-navbar-brand"><a href="/" class="brand-name">
            <img src = "/investprivat/logo_black.png" height="62px">

        </div>
      </div>
      <div class="rd-navbar-panel-inner">

        <div>
          <ul class="list-inline list-inline-xs">
            <li><a href="https://vk.com/invest_privat" class="icon icon-circle icon-primary icon-xs fa fa-vk"></a></li>
            <li><a href="https://www.youtube.com/channel/UClmUk0oIidwn6S9vlKwEA5Q" class="icon icon-circle icon-primary icon-xs fa-youtube"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>