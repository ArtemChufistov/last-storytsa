<?php
use app\modules\menu\widgets\MenuWidget;
use app\modules\news\widgets\LastNewsWidget;
use app\modules\profile\widgets\SubscribeWidget;
?>

<footer class="page-footer bg-darkest section-top-13 section-bottom-15 section-lg-top-90 text-sm-left">
  <div class="shell">
    <div class="range range-lg-right">

      <?php echo MenuWidget::widget(['menuName' => 'botomMenu', 'view' => 'bottom-menu']);?>
      <?php echo SubscribeWidget::widget();?>
      <div class="cell-sm-6 cell-lg-4 offset-top-45 offset-lg-top-0">
        <h5 class="text-white"><?php echo Yii::t('app', 'Кто мы?');?></h5>
        <p class="text-lighter offset-top-17"><?php echo Yii::t('app', 'Сообщество людей, которые готовы принимать решения для общей прибыли');?></p>
        <img src= "/images/bitlogo.png" style="width: 300px;">
      </div>
      <div class="cell-lg-9 offset-top-45 offset-md-top-110">
        <p class="font-size-10"><a href="/privacy" class="text-white">Пользовательское соглашение</a><span>&nbsp; | &nbsp;</span><a href="/index#about-block" class="text-white">О Нас</a><span class="inset-md-left-100 inset-lg-left-220">&nbsp;  &#169;</span><span id="copyright-year"></span><span class="text-white">&nbsp; InvestPrivat &nbsp;</span><?php echo Yii::t('app', 'Все права защищены');?>
        </p>
      </div>
    </div>
  </div>
</footer>