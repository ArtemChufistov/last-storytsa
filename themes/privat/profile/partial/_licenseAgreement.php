<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

Modal::begin([
    'header' => '<h1 class="text-center">' . Yii::t('app', 'Лицензионное соглашение') . '</h1>',
    'toggleButton' => false,
    'id' => 'pass'
]);
?>

    <p class="hint-block" style="text-align: justify;">
        <?= Yii::t('app', '
До принятия каких-либо действий, пожалуйста, прочитайте настоящий документ.  Ваше безоговорочное согласие с условиями, указанными ниже, а также ваше понимание и согласие с настоящим документом (Правила).
Настоящее Соглашение регулирует использование Инвестприват (далее именуемого «Сайт»).  При просмотре, передаче, копировании, сохранении и / или любым другим способом с использованием данного Сайта услуги или функции, предлагаемые этим сайтом или на нем, и / или его материалы, Вы соглашаетесь со всеми условиями и правилами, изложенными ниже.  Если вы не согласны с какими-либо или всеми условиями и положениями настоящего Соглашения, пожалуйста, прекратите использование Сайта. 
</br>1.Термины и определения </br>


В настоящем документе следующие термины имеют значения, указанные ниже:
«Сайт» - Сайт в Интернете ИНВЕСТПРИВАТ
«Биткойн» - или Битко́ин (англ. Bitcoin, от bit — «бит» и coin — «монета») — пиринговая платёжная система, использующая одноимённую расчётную единицу и одноимённый протокол передачи данных. Для обеспечения функционирования и защиты системы используются криптографические методы. Вся информация о транзакциях между адресами системы доступна в открытом виде
«Blockchain» - Реестр записей операций в порядке их выполнения хранится большое количество независимых членов. Все члены системы имеют регистр, который автоматически обновляет до последней версии , когда она исправленная. Каждый владелец счета имеет доступ к информации о любой сделке , когда - либо сделанная , начиная с первым. Пользователи играют роль нотариуса , подтверждающего достоверность информации , содержащейся в базе данных.
«Доверительное управление» -  передача актива в управление другому (доверенному) лицу с целью получения прибыли от этого управления.
«Инвестиции» - (англ. Investments) — размещение капитала с целью получения прибыли.
«Инвестиционный пакет»- отражает сумму вложенных средств
Инвестор - физ
</br>2. Общие правила :

</br>2.1 Инвестором  может являться любое дееспособное лицо, достигшее 18-ти летнего возраста, являющееся гражданином любой страны, законодательство которой не противоречит условиям данного соглашения.
</br>2.2  В случае необходимости ИНВЕСТПРИВАТ может потребовать документы для Верификации вашей личности и проверки регистрационных данных. Это может быть вызвано необходимостью подтверждения Вас, как владельца аккаунта.
</br>2.3 В соответствии с условиями настоящего Соглашения Инвестор осуществляет инвестирование денежных средств с помощью криптовалюты биткойн  для получения прибыли.
</br>2.4 ИНВЕСТПРИВАТ осуществляет управление деньгами для получения прибыли и сохранности привлечённого капитала. 

</br>3.Правила:

</br>3.1 Инвестор может иметь один электронный кабинет и открывать в нем любое количество инвестиционных пакетов.
</br>3.2 Активация аккаунта автоматически подтверждает согласие Инвестора с настоящими условиями Соглашения о сотрудничестве.
</br>3.3 Пополнение счёта в системе возможно только путём внесения биткойнов в электронном кабинете.
</br>3.4 При отсутствии доступа к сайту, из-за  технических сбоев в работе,  начисление положенных процентов  и прибыли  выполняется согласно плана.
</br>3.5 Недостатки, связанные с работой платежных систем и обменных пунктов работающих с биткойном Инвестор берет на себя.

</br>4. Порядок работы ИНВЕСТПРИВАТ:

</br>4.1 ИНВЕСТПРИВАТ работает каждый день по ненормированному часовому графику.  В выходные дни служба поддержки может осуществлять свои функции лишь частично.
</br>4.2 Каждый пакет, открытый внутри одного аккаунта, имеет свой временной отсчет и выплаты согласна правил.
</br>4.3 Вы можете не выводить проценты и купить на них очередной инвестиционный пакет.
</br>4.5 Сумма процентов может быть внесена в виде полного пакета. Если Вам недостаточно средств для приобретения очередного пакета, Вы можете дополнить денежные средства на счет или подождать, пока сумма начисляемых процентов и реферальных бонусов будет позволять купить дополнительный инвестиционный пакет.
</br>4.6 Комиссию на ввод и вывод оплачивает отправитель или получатель согласно тарифов, используемых платежных систем, работающих с биткойнами.
</br>4.7 Совершать заявку на вывод средств можно с 1 по 5 число месяца.
4.9 Денежные средства принимаются в биткойнах, а предлагаемые пакеты фиксируются в долларах и выплачиваются в биткойнах по курсу доллара 20% в месяц на сумму вложенных средств.
</br>4.10 Приобретенный пакет позволяет получать 20 % в месяц в течении 12 месяцев, после указанного срока пакет считается реализованным.
</br>4.11 Каждый пакет открытый внутри одного аккаунта имеет свой временной отсчет и выплаты согласна правил.
</br>4.12 Предусмотренная реферальная программа позволяет получать 10% единоразово от суммы приобретаемого пакета лично  приглашенного Инвестора зарегистрированного по Вашей реферальной ссылке.
</br>4.13 Реферальные вознаграждения выплачиваются в том же порядке , что и основные проценты. 
</br>5 Обязанности:

</br>5.1 Инвестор обязуется:

</br>- Использовать достоверную информацию о своей личности при регистрации на сайте компании.
</br>- Не использовать способы недобросовестной рекламы для продвижения своей деятельности с целью получения вознаграждений.
</br>- Своевременно выполнять взятые на себя обязательства в соответствии с настоящим Соглашением в период всего его действия
</br>-Сохранять от третьих лиц информацию и пароли, касающиеся ИНВЕСТПРИВАТА.

</br>5.2 ИНВЕСТПРИВАТ Обязуется:
</br>- Осуществлять выплату прибыли и вознаграждений Инвестору в соответствии с настоящим Соглашением.
</br>- Обеспечить техническую и информационную поддержку работы on-line сервиса. 
</br>- Соблюдать конфиденциальность сделки между Инвестором и ИНВЕСТПРИВАТ
</br>- Обеспечить безопасность и сохранность инвестиционных средств, переданных Инвестором.
</br>- Выполнять все выплаты, оформленные в соответствии с правилами настоящего соглашения.
</br>5.3 Инвестор вправе:
</br>- Получать прибыль от инвестиционных пакетов и партнёрское вознаграждение в соответствии с условиями и правилами.
</br>- Получать необходимую техническую и информационную поддержку.
</br>- Обратиться в адрес тех. поддержки с целью изменения своих персональных данных, если есть необходимость, с соответствующим документальным подтверждением.
</br>- Использовать рекламно-информационные продукты для привлечения партнёров.
</br>- Производить разъяснительную-информационную деятельность среди партнёров и потенциальных инвесторов, не искажая фактов.

</br>6. Порядок изменения и расторжения Соглашения:

</br>6.1 Настоящее соглашение может быть расторгнуто по взаимному согласию. 
</br>6.2 Условия настоящего Соглашения могут быть изменены по требованию ИНВЕСТПРИВАТ в одностороннем порядке. Любые изменения и дополнения заменяют или аннулируют предыдущие условия, действовавшие до момента внесения изменений в полном или частичном объёме и не могут противоречить друг другу.
</br>6.3 Инвестор принимает все изменения и дополнения к настоящему Соглашению после соответствующего уведомления, если продолжает пользоваться.

</br>7. Срок действия Соглашения:

</br>7.1 Настоящее Соглашение вступает в силу с момента регистрации аккаунта.
</br>7.2 Срок действия настоящего Соглашения длится до момента выполнения всех обязательств между Инвестором и ИНВЕСТПРИВАТ или до момента вывода своих инвестиционных средств из компании с последующим закрытием аккаунта Инвестора и личного счёта.

</br>8. Форс-мажор:

</br>8.1 В случае наступления обстоятельств непреодолимой силы (чрезвычайных и неотвратимых), возникших помимо воли и желания Сторон, которые нельзя предвидеть или избежать, в том числе объявления или фактическая война, гражданские волнения, эпидемии, блокада, эмбарго, пожары, землетрясения,наводнения и другие природные стихийные бедствия, а также издание актов государственных органов.
ИНВЕСТПРИВАТ не несёт ответственности за какие либо повреждения или потерю данных, в следствии наступления форс-мажорных обстоятельств, которые находятся вне компании.
</br>9. Прочие условия:

</br>9.1 Принятие Инвестором настоящего Соглашения является активация личного счёта, кабинета в ИНВЕСТПРИВАТ.
</br>9.2 Инвестор понимает и согласен с тем, что инвестиции имеют некую степень риска. Инвестиционные средства, переданные в доверительное управление  будут находиться в целости и сохранности
') ?>.
    </p>

<?php Modal::end();?>