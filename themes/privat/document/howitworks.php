<?php
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
		'label' => 'Как это работает',
		'url' => '/howitworks'
	],
];

$this->title = $model->title;
?>

<main class="page-content">
  <!--section1-->
  <section class="section-70 section-md-135 bg-gray-light">
    <div class="shell">
      <h3><?php echo Yii::t('app', 'Как это работает');?></h3>
      <p class="text-regular font-size-12"><?php echo Yii::t('app', 'простыми словами о сложном');?></p>
      <hr class="hr offset-top-40 bg-gray-light">
      <div class="range range-xs-center offset-top-40">
        <div class="cell-sm-8">
          <p class="h6 font-default text-transform-none text-base"><?php echo Yii::t('app', 'В данном разделе вы можете ознакомится нашей работой, как мы работаем и что предлгаем вам.');?></p>
        </div>
      </div>
    </div>
  </section>
  <!-- section2-->
  <section class="text-sm-left">
    <div class="shell">
      <div class="range range-xs-center range-sm-left offset-top-0">
        <div class="cell-xs-10 cell-sm-7 cell-lg-6 section-image-aside section-image-aside-right text-sm-left">
          <div style="background-image: url(images/services-01-960x680.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
          <div class="section-image-aside-body section-65 section-md-top-125 section-md-bottom-183 inset-lg-right-110">
            <h3><?php echo Yii::t('app', 'Первые шаги');?></h3>
            <div class="offset-top-30">
              <p class="line-height-21"><?php echo Yii::t('app', 'Все очень просто, в пару кликов вы сможете поднять свое благосостояние');?></p>
              <p class="line-height-21 offset-top-20"><?php echo Yii::t('app', 'Для регистрации вам необходимо иметь e-mail и первичные навыки работы с компьютером. Проити регистрацию на нашем сайте, подтвердить свои учетные данные, после ознакомиться с правилами и в путь!');?></p>
            </div>
            <div class="offset-top-35 offset-md-top-65"><a href="services-single.html" class="btn btn-primary"><?php echo Yii::t('app', 'Присоединиться');?></a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- section3-->
  <section class="text-sm-left">
    <div class="shell">
      <div class="range range-xs-center range-sm-right offset-top-0">
        <div class="cell-xs-10 cell-sm-7 cell-lg-6 section-image-aside section-image-aside-left text-sm-left">
          <div style="background-image: url(images/services-02-960x680.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
          <div class="section-image-aside-body section-65 section-md-top-125 section-md-bottom-183 inset-lg-left-110">
            <h3><?php echo Yii::t('app', 'Оформление депозита');?></h3>
            <div class="offset-top-30">
              <p class="line-height-21"><?php echo Yii::t('app', 'Условия оформления очень простые.');?></p>
              <p class="line-height-21 offset-top-20"><?php echo Yii::t('app', 'Здесь сами условия.');?></p>
            </div>
            <div class="offset-top-35 offset-md-top-65"><a href="services-single.html" class="btn btn-primary"><?php echo Yii::t('app', 'Присоединиться');?></a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- section4-->
  <section class="text-sm-left">
    <div class="shell">
      <div class="range range-xs-center range-sm-left offset-top-0">
        <div class="cell-xs-10 cell-sm-7 cell-lg-6 section-image-aside section-image-aside-right text-sm-left">
          <div style="background-image: url(images/services-03-960x680.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
          <div class="section-image-aside-body section-65 section-md-top-125 section-md-bottom-183 inset-lg-right-110">
            <h3><?php echo Yii::t('app', 'Привлекайте партнеров');?></h3>
            <div class="offset-top-30">
              <p class="line-height-21"><?php echo Yii::t('app', 'Привлекайте партнеров для расшинеия своего бизнеса');?></p>
              <p class="line-height-21 offset-top-20"><?php echo Yii::t('app', 'В личном кабинете вам будет выдана реферальная ссылка для распространения ее среди своих знакомых и друзей. Так же буду доступн рекламные баннера с уникальным реферальным кодом, при клике на который, человек сможет ознакомиться с нашим проектом и попасть к вам в структуру');?></p>
              <p class="line-height-21 offset-top-20"><?php echo Yii::t('app', 'За привлечения партнеров проект отчисляем вам проценты');?></p>
            </div>
            <div class="offset-top-35 offset-md-top-65"><a href="services-single.html" class="btn btn-primary"><?php echo Yii::t('app', 'Присоединиться');?></a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- section5-->
  <section class="text-sm-left">
    <div class="shell">
      <div class="range range-xs-center range-sm-right offset-top-0">
        <div class="cell-xs-10 cell-sm-7 cell-lg-6 section-image-aside section-image-aside-left text-sm-left">
          <div style="background-image: url(images/services-04-960x680.jpg)" class="section-image-aside-img veil reveal-sm-block"></div>
          <div class="section-image-aside-body section-65 section-md-top-125 section-md-bottom-183 inset-lg-left-110">
            <h3><?php echo Yii::t('app', 'Получайте прибыль');?></h3>
            <div class="offset-top-30">
              <p class="line-height-21"><?php echo Yii::t('app', 'Вас ждет прекрасное будущее!');?></p>
              <p class="line-height-21 offset-top-20"><?php echo Yii::t('app', 'Зарабатывайте вместе с нами, и у вас не будет никаких проблем. Все очень просто! Пройдите регистрацию, привлекайте друзей и знакомых а так же посещайте вебинары и наши группы в социальных сетях!');?></p>
            </div>
            <div class="offset-top-35 offset-md-top-65"><a href="services-single.html" class="btn btn-primary"><?php echo Yii::t('app', 'Присоединиться');?></a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
