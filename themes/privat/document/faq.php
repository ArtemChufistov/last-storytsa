<?php
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs']= [ [
    'label' => 'Faq',
    'url' => '/faq'
  ],
];
$this->title = $model->title;
?>

<!-- Page Content-->
<main class="page-content">
  <!-- Faq Variant 2-->
  <section class="section-66 section-sm-top-110 section-lg-bottom-0">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-lg-6"><img src="images/faq.png" width="456" height="600" alt="" class="veil reveal-lg-inline-block"></div>
        <div class="cell-sm-9 cell-lg-6 offset-top-0">
          <h3><?php echo Yii::t('app', 'Основные вопросы');?></h3>
          <hr class="divider bg-mantis hr-lg-left-0">
          <div class="offset-top-41 offset-lg-top-66">
            <div role="tablist" aria-multiselectable="true" id="accordion-1" class="panel-group accordion offset-top-0">
              <div class="panel panel-default">
                <div role="tab" id="headingOne" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Кто может стать инвестором ?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingOne" id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body"><?= Yii::t('app', 'Инвестором  может являться любое дееспособное лицо, достигшее 18-ти летнего возраста, являющееся гражданином любой страны, законодательство которой не противоречит условиям данного соглашения.
');?>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div role="tab" id="headingTwo" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Сколько аккаутнов можно иметь одному участнику ?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingTwo" id="collapseTwo" class="panel-collapse collapse">
                  <div class="panel-body"><?= Yii::t('app', 'Инвестор может иметь один электронный кабинет и открывать в нем любое количество инвестиционных пакетов.');?>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div role="tab" id="headingThree" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Какое количество пакетов может иметь инвестор ?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingThree" id="collapseThree" class="panel-collapse collapse">
                  <div class="panel-body"><div class="panel-body"><?= Yii::t('app', 'Инвестор может иметь один электронный кабинет и открывать в нем любое количество инвестиционных пакетов.');?></div>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div role="tab" id="headingFour" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Как пополнить счет для покупки пакета ?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingFour" id="collapseFour" class="panel-collapse collapse">
                  <div class="panel-body"><div class="panel-body"><?= Yii::t('app', 'Пополнение счёта в системе возможно только путём внесения биткойнов в электронном кабинете.');?>
                  </div>
                </div>
              </div>
            </div>
              <div class="panel panel-default">
                <div role="tab" id="headingFive" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Сколько процентов инвестор имеет от инвестиционного пакета?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingFive" id="collapseFive" class="panel-collapse collapse">
                  <div class="panel-body"><div class="panel-body"><?= Yii::t('app', 'Приобретенный пакет позволяет получать 20 % в месяц в течении 12 месяцев, после указанного срока пакет считается реализованным.');?>
                  </div>
                </div>
              </div>
            </div>
              <div class="panel panel-default">
                <div role="tab" id="headingSix" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSix" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Как работает реферальная программа ?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingSix" id="collapseSix" class="panel-collapse collapse">
                  <div class="panel-body"><div class="panel-body"><?= Yii::t('app', 'Предусмотренная реферальная программа позволяет получать 10% единоразово от суммы приобретаемого пакета лично  приглашенного Инвестора зарегистрированного по Вашей реферальной ссылке.');?>
                  </div>
                </div>
              </div>
            </div>
              <div class="panel panel-default">
                <div role="tab" id="headingSeven" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSeven" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Когда инвестор может сделать заявку на вывод средств?');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingSeven" id="collapseSeven" class="panel-collapse collapse">
                  <div class="panel-body"><div class="panel-body"><?= Yii::t('app', 'Совершать заявку на вывод средств можно с 1 по 5 число месяца.');?>
                  </div>
                </div>
              </div>
            </div>
              <div class="panel panel-default">
                <div role="tab" id="headingEight" class="panel-heading">
                  <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseEight" aria-expanded="true" class="collapsed"><?= Yii::t('app', 'Когда инвестор получает свои проценты от инвестиционных пакетов ');?></a></div>
                </div>
                <div role="tabpanel" aria-labelledby="headingEight" id="collapseEight" class="panel-collapse collapse">
                  <div class="panel-body"><div class="panel-body"><?= Yii::t('app', 'Денежные средства выплачиваются в срок с первого по седьмое число каждого месяца при условии.');?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- First Workout - Free-->
  <section class="section-66 context-dark bg-blue-gray">
    <div class="shell">
      <h4><?= Yii::t('app', 'Если вы не нашли ответ на свой вопрос вы можете');?></h4><a href="<?php echo Url::to(['contact/contact/index']);?>" class="btn btn-default btn-icon btn-icon-left offset-top-20"><span class="icon mdi mdi-email-outline"></span><?= Yii::t('app', 'Связаться с нами');?></a></br></br>
    </div>
  </section>
</main>