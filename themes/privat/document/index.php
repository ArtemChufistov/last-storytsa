<?php
use yii\web\View;
use yii\helpers\Url;

$this->title = $model->title;
?>
<main class="page-content">
  <section>
    <!-- Swiper-->
    <div data-height="43%" data-loop="true" data-dragable="false" data-min-height="480px" data-slide-effect="true" class="swiper-container swiper-slider">
      <div class="swiper-wrapper">
        <div data-slide-bg="images/slide-01.jpg" style="background-position: 80% center" class="swiper-slide">
          <div class="swiper-slide-caption"></div>
        </div>
      </div>
      <div class="jumbotron-custom" style="margin-top: -130px;">
        <div class="shell">
          <div class="range range-xs-center">
            <div class="cell-md-9 text-center cell-xs-10">
              <div>
                <h1><span class="text-primary view-animate fadeInUp delay-08 duration-1 animated"><?php echo Yii::t('app', 'Инвест');?></span><span class="text-blue view-animate fadeInDown delay-08 duration-1 animated"><?php echo Yii::t('app', 'Приват');?></span></h1>
              </div>
              <div class="offset-top-20 view-animate fadeInUpSmall delay-16 duration-1">
                <p class="h6 text-blue font-default text-light text-transform-none" style="font-weight: bold;"><?php echo Yii::t('app', 'это проект, </br>созданный для частного инвестирования');?></p>
              </div>
              <div class="offset-top-20 offset-lg-top-50 view-animate zoomInSmall delay-2 duration-1"><a href="/signup" class="btn btn-primary"><?php echo Yii::t('app', 'Присоединиться');?></a></div>
            </div>
          </div>
        </div>
      </div>
      <!-- Swiper Pagination-->
      <div class="swiper-pagination"></div>
    </div>
  </section>

  <section class="section-top-65 section-md-top-135">



    <div class="shell">
      <h3 id = "about-block"><?php echo Yii::t('app', 'О Нас');?></h3>
      <p class="font-size-12"><?php echo Yii::t('app', 'Мы - команда профессионалов!');?></p>
      <hr class="hr offset-top-20">
      <div class="range range-xs-center offset-top-20">
        <div class="cell-xs-10 cell-lg-8">

          <div class="offset-top-8">
            <p><a href="/"><strong><?php echo Yii::t('app', 'Инвест Приват');?></strong></a><span><?php echo Yii::t('app', '- это проект, созданный для частного инвестирования.');?></span> 

            </p>
            <p><?php echo Yii::t('app', 'Наш опыт позволяет успешно торговать на бирже, преумножая деньги в долгосрочной перспективе. Наша Цель - удовлетворить Ваше желание по увеличению имеющихся денежных средств. Наше сотрудничество позволит Вам иметь пассивный доход - 20 процентов в месяц и неограниченный доход при активной позиции с помощью реферальной одноуровневой программы в 10 процентов от вклада Вашего приглашенного.');?>
            </p>
            <p><a href="#"><strong><?php echo Yii::t('app', 'Инвестиционная программа предлагает пакеты различных номиналов: ');?></strong></a></p>
          </div>
        </div>
      </div>

      <div class="range range-xs-center text-sm-left">
        <div class="cell-sm-3 cell-md-3">
          <div class="thumbnail-classic bg-white"><img src="/investprivat/bitcoin-grey-930x488.jpg" alt="" width="370" height="210" class="img-responsive img-fullwidth"/>
            <div class="caption">
              <h6><a href="#" class="text-dark"><?php echo Yii::t('app', '100 USD');?></a></h6>
            </div>
          </div>
        </div>
        <div class="cell-sm-3 cell-md-3">
          <div class="thumbnail-classic bg-white"><img src="/investprivat/bitcoin-BTE-21.jpg" alt=""  height="210" class="img-responsive img-fullwidth"/>
            <div class="caption">
              <h6><a href="#" class="text-dark"><?php echo Yii::t('app', '500 USD');?></a></h6>
            </div>
          </div>
        </div>
        <div class="cell-sm-3 cell-md-3">
          <div class="thumbnail-classic bg-white"><img src="/investprivat/bitcoin2.jpg" alt="" width="370" height="210" class="img-responsive img-fullwidth"/>
            <div class="caption">
              <h6><a href="#" class="text-dark"><?php echo Yii::t('app', '1000 USD');?></a></h6>
            </div>
          </div>
        </div>
      </div>
      <div class="range range-xs-center text-sm-left">
        <div class="cell-sm-3 cell-md-3">
          <div class="thumbnail-classic bg-white"><img src="/investprivat/160504-bitcoin-b1.jpg" alt="" width="370" height="210" class="img-responsive img-fullwidth"/>
            <div class="caption">
              <h6><a href="#" class="text-dark"><?php echo Yii::t('app', '3 000 USD');?></a></h6>
            </div>
          </div>
        </div>
        <div class="cell-sm-3 cell-md-3">
          <div class="thumbnail-classic bg-white"><img src="/investprivat/bitcoin-wallpaper-2665831-56a6a45c3df78cf7728f8ae51.jpg" alt="" width="370" height="210" class="img-responsive img-fullwidth"/>
            <div class="caption">
              <h6><a href="#" class="text-dark"><?php echo Yii::t('app', '5 000 USD');?></a></h6>
            </div>
          </div>
        </div>
        <div class="cell-sm-3 cell-md-3">
          <div class="thumbnail-classic bg-white"><img src="/privat/bitc123.jpg" alt="" width="370" height="210" class="img-responsive img-fullwidth"/>
            <div class="caption">
              <h6><a href="#" class="text-dark"><?php echo Yii::t('app', '10 000 USD');?></a></h6>
            </div>
          </div>
        </div>
      </div>
      <div class="offset-top-20">
        <p><a href="#"><strong><?php echo Yii::t('app', 'На пакет начисляется 20 %  в месяц и выплачивается на Ваш привязанный кошелек с 1 по 5 число каждого нового месяца .');?></strong></a></p>
        <p>
          <?php echo Yii::t('app', 'Любой из предложенных пакетов можно покупать многократно в одном личном кабинете. Вклад фиксируется в рублях, но хождение денежных средств осуществляется с помощью электронной криптовалюты биткойн по курсу на момент покупки пакета. Пакет имеет ограниченный срок 1 год. Инвестируя сумму денег в какой - либо пакет Вы имеете возможность получать от номинала пакета указанную сумму процентов. Так в течении первых пяти месяцев без учета реферальной программы Вы получаете тело вклада, а с шестого  по двенадцатый месяц- чистую прибыль. Пакеты работают независимо друг от друга и могут быть открыты в одном бекофисе в разное время. Полученные проценты Вы можете выводить ежемесячно в указанный срок (с 1 по 5 число), нажав на кнопку вывода. Кроме того, Вы имеете возможность не выводить проценты в указанный срок, формируя тем самым покупку дополнительных пакетов.');?>
        </p>
        <p><a href="#"><strong><?php echo Yii::t('app', 'Реферальная программа');?></strong></a></p>
        <p><?php echo Yii::t('app', 'Это программа, позволяющая получать дополнительный доход от рекомендаций новым вкладчикам сотрудничества с ИнвестПриват. Так от каждой Вашей рекомендации Вы получаете 10 процентов от вклада Вашего приглашенного инвестора. Реферальные вознаграждения Вы можете обналичить в срок, предусмотренный для выплат ( с 1 по 5 е ) число. Или сформировать из реферальных бонусов свой очередной инвестиционный пакет. Данные условия позволят ИнвестПриват развиваться и приумножать Ваши инвестиции на долгосрочной надежной основе для реализации цели и удовлетворения всех Ваших потребностей.');?></p>
      </div>
    </div>
  </section>
  
  <!--section features-->
  <section class="section-60 section-md-top-122 section-md-bottom-122">
    <div class="shell">
      <div class="range range-xs-center range-xs-bottom features-list">
        <div class="cell-sm-6 cell-md-3 wow fadeInUp delay-04 duration-1">
          <div class="thumbnail-features">
            <div class="unit unit-vertical unit-spacing-sm">
              <div class="unit-left">
                <i class="fa-line-chart" aria-hidden="true" style = "font-style: normal; font-size: 38px; color:#5dd39e;"></i>
              </div>
              <div class="unit-body">
                <h6 class="offset-top-10"><?php echo Yii::t('app', '20% в месяц');?></h6><a href="#" class="btn btn-xs btn-default offset-top-30"><?php echo Yii::t('app', 'Узнать подробнее');?></a>
              </div>
            </div>
          </div>
        </div>
        <div class="cell-sm-6 cell-md-3 wow fadeInDown delay-04 duration-1 offset-top-55 offset-sm-top-0">
          <div class="thumbnail-features">
            <div class="unit unit-vertical unit-spacing-sm">
              <div class="unit-left"><i class="fa-street-view" aria-hidden="true" style = "font-style: normal; font-size: 38px; color:#5dd39e;"></i></div>
              <div class="unit-body">
                <h6 class="offset-top-10"><?php echo Yii::t('app', '10% реферальные');?></h6><a href="/signup" class="btn btn-xs btn-default offset-top-30"><?php echo Yii::t('app', 'Присоединиться');?></a>
              </div>
            </div>
          </div>
        </div>
        <div class="cell-sm-6 cell-md-3 wow fadeInUp delay-04 duration-1 offset-top-55 offset-md-top-0">
          <div class="thumbnail-features">
            <div class="unit unit-vertical unit-spacing-sm">
              <div class="unit-left"><i class="fa fa-heart" aria-hidden="true" style = "font-style: normal; font-size: 38px; color:#5dd39e;"></i></div>
              <div class="unit-body">
                <h6 class="offset-top-10"><?php echo Yii::t('app', 'Гарантированный');?> <br class="visible-lg-inline"> <?php echo Yii::t('app', 'результат');?></h6><a href="#" class="btn btn-xs btn-default offset-top-30"><?php echo Yii::t('app', 'Подробнее');?></a>
              </div>
            </div>
          </div>
        </div>
        <div class="cell-sm-6 cell-md-3 wow fadeInDown delay-04 duration-1 offset-top-55 offset-md-top-0">
          <div class="thumbnail-features">
            <div class="unit unit-vertical unit-spacing-sm">
              <div class="unit-left"><i class="fa-area-chart" aria-hidden="true" style = "font-style: normal; font-size: 38px; color:#5dd39e;"></i></div>
              <div class="unit-body">
                <h6 class="offset-top-10"><?php echo Yii::t('app', 'Стабильный доход');?></h6><a href="/signup" class="btn btn-xs btn-default offset-top-30"><?php echo Yii::t('app', 'Присоединиться');?></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--quote-->
  <section>
    <!-- RD Parallax-->
    <div data-on="false" data-md-on="true" class="rd-parallax">
      <div data-speed="0.35" data-type="media" data-url="images/parallax-07.jpg" class="rd-parallax-layer"></div>
      <div data-speed="0.15" data-type="html" class="rd-parallax-layer">
        <div class="shell section-75 section-md-bottom-105">
          <div class="range range-xs-center">
            <div class="cell-sm">
              <div class="jumbotron-custom-variant-0">
                <h3 class="text-white" style="color:#5dd39e;"><?php echo Yii::t('app', 'Будь на волне успеха с Инвест Приват');?></h3>
              </div><a href="contacts.html" class="btn btn-primary offset-top-35"><?php echo Yii::t('app', 'Присоединиться');?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="offset-top-70 offset-md-top-114" style = "margin-bottom: 100px;">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-xs-10">
          <h3 id = "calc-block"><?php echo Yii::t('app', 'Калькулятор');?></h3>
          <hr class="divider">
          <div class="range range-xs-center offset-top-20">
            <p><?php echo Yii::t('app', 'Используя наш калькулятор, вы можете с легкостью рассчитать свою прибыль в компании <strong style="color:#5dd39e;">Инвест Приват</strong>. Применяйте различные настройки калькулятора при выборе суммы инвестиций.');?>
            </p>
          </div>
          <div class="progress-wrap offset-top-25" data-speed="2500">
            <div class="row">
              <div class="col-md-4">

                <ul class="inline-list">
                  <li><a id="100tariff" href="#" class="btn btn-gray"><span>100 USD</span></a></li>
                  <li><a id="500tariff" href="#" class="btn btn-gray"><span>500 USD</span></a></li>
                  <li><a id="1000tariff" href="#" class="btn btn-gray"><span>1 000 USD</span></a></li>
                  <li><a id="3000tariff" href="#" class="btn btn-gray"><span>3 000 USD</span></a></li>
                  <li><a id="5000tariff" href="#" class="btn btn-gray"><span>5 000 USD</span></a></li>
                  <li><a id="10000tariff" href="#" class="btn btn-gray"><span>10 000 USD</span></a></li>
                </ul>
              </div>
              <div class="col-md-8">
                <h6 class = "progressbar-title">В день</h6>
                <div class="progress">
                  <div id="daily-bar" class="progress-bar progress-bar__secondary" data-progress-percent="38%">
                    <span class="progress-count" data-progress-count="0"></span>
                  </div>
                </div>

                <h6 class = "progressbar-title">В месяц</h6>
                <div class="progress">
                  <div id="monthly-bar" class="progress-bar progress-bar__secondary2" data-progress-percent="53%">
                    <span class="progress-count" data-progress-count="0"></span>
                  </div>
                </div>

                <h6 class = "progressbar-title">За весь срок</h6>
                <div class="progress">
                  <div id="annually-bar" class="progress-bar progress-bar__secondary3" data-progress-percent="68%">
                    <span class="progress-count" data-progress-count="0"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

</main>

<style type="text/css">
  .progress-count{
    font-weight: bold;
  }
  .progressbar-title{
    margin-bottom: 15px;
  }
  #daily-bar{
    background-color: #f44236;
  }
  #monthly-bar{
    background-color: #febd00;
  }
  #annually-bar{
    background-color: #4baf4f;
  }
</style>