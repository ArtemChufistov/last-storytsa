<header class="page-head header-default">
  <!-- RD Navbar-->
  <div class="rd-navbar-wrap">
    <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-static" data-md-device-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-sm-stick-up-offset="50px" data-lg-stick-up-offset="100px" class="rd-navbar">
      <?php echo Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/layouts/partial/_crown');?>
      <div class="rd-navbar-inner">
        <div class="rd-navbar-menu-panel">
          <div class="rd-navbar-nav-wrap">
            <ul class="rd-navbar-nav">
              <?php foreach($menu->children()->all() as $num => $children):?>
                <li class="<?php if($requestUrl == $children->link):?>active<?php endif;?>" ><a href="<?= $children->link;?>"><?= $children->title;?></a>
                  <?php if (!empty($children->children()->all())):?>
                    <ul class="rd-navbar-dropdown">
                      <?php foreach($children->children()->all() as $numChild => $childChildren):?>
                        <li><a href="<?= $childChildren->link;?>"><?= $childChildren->title;?></a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="<?= $childChildren->link;?>"><?= $childChildren->title;?></a>
                            </li>
                          </ul>
                        </li>
                      <?php endforeach;?>
                    </ul>
                  <?php endif;?>
                </li>
              <?php endforeach;?>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  </div>
</header>