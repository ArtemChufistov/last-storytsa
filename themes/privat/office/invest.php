<?php
use app\modules\event\models\Event;
use lowbase\user\UserAsset;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('app', 'Инвестиционный портфель');
$this->params['breadcrumbs'][] = $this->title;
$assets = UserAsset::register($this);
?>
<style type="text/css">
	.info-box-icon{
		line-height: 19px;
	}
	.smallSpanText{
		font-size: 13px;
	}
	a.info-box:hover{
		border: 1px solid #333;
	}
</style>

<?php if(Yii::$app->session->getFlash('message')): ?>
  <div class="modalMessage modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
        </div>
        <div class="modal-body">
          <?php echo Yii::$app->session->getFlash('message');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
        </div>
      </div>
    </div>
  </div>

<?php $this->registerJs("
  $('.modalMessage').modal('show');
", View::POS_END);?>

<?php endif;?>

<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true"></i><?php echo Yii::t('app', 'Подробно о портфелях');?></h3>
      </div>
      <div class="box-body text-trancpancy">
      	<div class="col-md-9">
        	<?php echo Yii::t('app', '<strong>На каждый пакет начисляется 20 % в месяц.</strong></br>
        	Выплачивается на Ваш привязанный кошелек с 1 по 5 число каждого нового месяца.
		Любой из предложенных пакетов можно покупать многократно в одном личном кабинете. Вклад фиксируется в USD, но хождение денежных средств осуществляется с помощью электронной криптовалюты биткойн по курсу на момент покупки пакета. Пакет имеет ограниченный срок 1 год. Инвестируя сумму денег в какой - либо пакет Вы имеете возможность получать от номинала пакета указанную сумму процентов. Так в течении первых пяти месяцев без учета реферальной программы Вы получаете тело вклада, а с шестого  по двенадцатый месяц- чистую прибыль. Пакеты работают независимо друг от друга и могут быть открыты в одном бекофисе в разное время. Полученные проценты Вы можете выводить ежемесячно в указанный срок (с 1 по 5 число), нажав на кнопку вывода. Кроме того, Вы имеете возможность не выводить проценты в указанный срок, формируя тем самым покупку дополнительных пакетов.');?>
		</div>
		<div class="col-md-3">
			<img src = "/privat/bitcoin-coins1.png" style = "width: 300px;">
		</div>
      </div>
    </div>
  </div>
</div>
<h3> <?php echo Yii::t('app', 'Выберите портфель');?> </h3>
<div class="row">

	<div class="col-md-4 col-sm-6 col-xs-12">
		<a href= "#" class="invest-type info-box bg-aqua" data-toggle = "modal" data-target = "#investType1">
			<span class="info-box-icon">
				<i class="fa fa-bookmark-o"></i>
				<span class="info-box-number">100 </br>
					<span class = "smallSpanText">USD</span>
				</span>
			</span>
			<div class="info-box-content">
				<strong class="progress-description"><span style = "font-size: 18px;">20 </span><?php echo Yii::t('app', 'USD в месяц');?></strong>
				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<strong class="progress-description"><span style = "font-size: 18px;">240 </span><?php echo Yii::t('app', 'USD за год');?></strong>
			</div>
		</a>

		  <div class="modal" id = "investType1">
		    <div class="modal-dialog" role="document">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
		        </div>
		        <div class="modal-body">
			        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете покупку инвестиционного пакета?</br><strong>с вашего счета спишется 100 USD</strong>');?> </br> 
					</br>
			        </h4>
					<?php $form = ActiveForm::begin(); ?>
						<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
						  'class' => 'btn btn-block btn-success pull-left',
						  'value' => 1,
						  'name' => 'invest-type']) ?>
					<?php ActiveForm::end();?>
					</br>
					</br>
					<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
					<div style="clear: both;"></div>
		        </div>
		      </div>
		    </div>
		  </div>

	</div>
	<div class="col-md-4 col-sm-6 col-xs-12">
		<a href= "#" class="invest-type info-box bg-green" data-toggle = "modal" data-target = "#investType2">
			<span class="info-box-icon">
				<i class="fa fa-heart-o"></i>
				<span class="info-box-number">500 </br>
					<span class = "smallSpanText">USD</span>
				</span>
			</span>
			<div class="info-box-content">
				<strong class="progress-description"><span style = "font-size: 18px;">100 </span><?php echo Yii::t('app', 'USD в месяц');?></strong>
				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<strong class="progress-description"><span style = "font-size: 18px;">1 200 </span><?php echo Yii::t('app', 'USD за год');?></strong>
			</div>
		</a>

		  <div class="modal" id = "investType2">
		    <div class="modal-dialog" role="document">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
		        </div>
		        <div class="modal-body">
			        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете покупку инвестиционного пакета?</br><strong>с вашего счета спишется 500 USD</strong>');?> </br> 
					</br>
			        </h4>
					<?php $form = ActiveForm::begin(); ?>
						<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
						  'class' => 'btn btn-block btn-success pull-left',
						  'value' => 2,
						  'name' => 'invest-type']) ?>
					<?php ActiveForm::end();?>
					</br>
					</br>
					<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
					<div style="clear: both;"></div>
		        </div>
		      </div>
		    </div>
		  </div>

	</div>
	<div class="col-md-4 col-sm-6 col-xs-12">
		<a href= "#" class="invest-type info-box bg-red" data-toggle = "modal" data-target = "#investType3">
			<span class="info-box-icon">
				<i class="fa fa-diamond"></i>
				<span class="info-box-number">1000 </br>
					<span class = "smallSpanText">USD</span>
				</span>
			</span>
			<div class="info-box-content">
				<strong class="progress-description"><span style = "font-size: 18px;">200 </span><?php echo Yii::t('app', 'USD в месяц');?></strong>
				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<strong class="progress-description"><span style = "font-size: 18px;">2400 </span><?php echo Yii::t('app', 'USD за год');?></strong>
			</div>
		</a>

		  <div class="modal" id = "investType3">
		    <div class="modal-dialog" role="document">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
		        </div>
		        <div class="modal-body">
			        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете покупку инвестиционного пакета?</br><strong>с вашего счета спишется 1000 USD</strong>');?> </br> 
					</br>
			        </h4>
					<?php $form = ActiveForm::begin(); ?>
						<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
						  'class' => 'btn btn-block btn-success pull-left',
						  'value' => 3,
						  'name' => 'invest-type']) ?>
					<?php ActiveForm::end();?>
					</br>
					</br>
					<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
					<div style="clear: both;"></div>
		        </div>
		      </div>
		    </div>
		  </div>

	</div>
	<div class="col-md-4 col-sm-6 col-xs-12">
		<a href= "#" class="invest-type info-box bg-aqua" data-toggle = "modal" data-target = "#investType4">
			<span class="info-box-icon">
				<i class="fa fa-cube"></i>
				<span class="info-box-number">3 000 </br>
					<span class = "smallSpanText">USD</span>
				</span>
			</span>
			<div class="info-box-content">
				<strong class="progress-description"><span style = "font-size: 18px;">600 </span><?php echo Yii::t('app', 'USD в месяц');?></strong>
				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<strong class="progress-description"><span style = "font-size: 18px;">7 200 </span><?php echo Yii::t('app', 'USD за год');?></strong>
			</div>
		</a>

		  <div class="modal" id = "investType4">
		    <div class="modal-dialog" role="document">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
		        </div>
		        <div class="modal-body">
			        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете покупку инвестиционного пакета?</br><strong>с вашего счета спишется 3000 USD</strong>');?> </br> 
					</br>
			        </h4>
					<?php $form = ActiveForm::begin(); ?>
						<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
						  'class' => 'btn btn-block btn-success pull-left',
						  'value' => 4,
						  'name' => 'invest-type']) ?>
					<?php ActiveForm::end();?>
					</br>
					</br>
					<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
					<div style="clear: both;"></div>
		        </div>
		      </div>
		    </div>
		  </div>

	</div>
	<div class="col-md-4 col-sm-6 col-xs-12">
		<a href= "#" class="invest-type info-box bg-green" data-toggle = "modal" data-target = "#investType5">
			<span class="info-box-icon">
				<i class="fa fa-bolt"></i>
				<span class="info-box-number">5 000 </br>
					<span class = "smallSpanText">USD</span>
				</span>
			</span>
			<div class="info-box-content">
				<strong class="progress-description"><span style = "font-size: 18px;">1 000 </span><?php echo Yii::t('app', 'USD в месяц');?></strong>
				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<strong class="progress-description"><span style = "font-size: 18px;">12 000 </span><?php echo Yii::t('app', 'USD за год');?></strong>
			</div>
		</a>

		  <div class="modal" id = "investType5">
		    <div class="modal-dialog" role="document">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
		        </div>
		        <div class="modal-body">
			        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете покупку инвестиционного пакета?</br><strong>с вашего счета спишется 5000 USD</strong>');?> </br> 
					</br>
			        </h4>
					<?php $form = ActiveForm::begin(); ?>
						<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
						  'class' => 'btn btn-block btn-success pull-left',
						  'value' => 5,
						  'name' => 'invest-type']) ?>
					<?php ActiveForm::end();?>
					</br>
					</br>
					<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
					<div style="clear: both;"></div>
		        </div>
		      </div>
		    </div>
		  </div>

	</div>
	<div class="col-md-4 col-sm-6 col-xs-12">
		<a href= "#" class="invest-type info-box bg-red" data-toggle = "modal" data-target = "#investType6">
			<span class="info-box-icon">
				<i class="fa fa-university"></i>
				<span class="info-box-number">10 000 </br>
					<span class = "smallSpanText">USD</span>
				</span>
			</span>
			<div class="info-box-content">
				<strong class="progress-description"><span style = "font-size: 18px;">2 000 </span><?php echo Yii::t('app', 'USD в месяц');?></strong>
				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<strong class="progress-description"><span style = "font-size: 18px;">24 000 </span><?php echo Yii::t('app', 'USD за год');?></strong>
			</div>
		</a>

		  <div class="modal" id = "investType6">
		    <div class="modal-dialog" role="document">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		          <h4 class="modal-title"><?php echo Yii::t('app', 'Внимание');?></h4>
		        </div>
		        <div class="modal-body">
			        <h4 style = "text-align: center;"><?php echo Yii::t('app', 'Вы подтверждаете покупку инвестиционного пакета?</br><strong>с вашего счета спишется 10 000 USD</strong>');?> </br> 
					</br>
			        </h4>
					<?php $form = ActiveForm::begin(); ?>
						<?= Html::submitButton('<i class="fa fa-check" aria-hidden="true"></i> '.Yii::t('app', 'Принять участие'), [
						  'class' => 'btn btn-block btn-success pull-left',
						  'value' => 6,
						  'name' => 'invest-type']) ?>
					<?php ActiveForm::end();?>
					</br>
					</br>
					<button type="button" class="btn btn-block btn-secondary pull-left" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть');?></button>
					<div style="clear: both;"></div>
		        </div>
		      </div>
		    </div>
		  </div>

	</div>
</div>

<?php if (!empty($userInvestArray)):?>
	<h3> <?php echo Yii::t('app', 'Ваши инвестиционные портфели');?> </h3>
	<div class = "row">
		<?php foreach($userInvestArray as $num => $userInvest ):?>
			<div class="col-md-4 col-sm-6 col-xs-12">
			  <div class="box box-success">
			    <div class="box-header with-border">
			      <h3 class="box-title"><?php echo Yii::t('app', 'Портфель');?> <strong style="color: #00a65a;font-size: 18px;"><?php echo $userInvest->sum;?> USD</strong></h3>
			    </div>
			    <div class="box-body">
			      <canvas id="pieChart<?php echo $num;?>" style="height:250px"></canvas>
			      <strong>Дата открытия: <?php echo date('d-m-Y', strtotime($userInvest->date_add));?></strong></br>
			      <strong>Начисленная сумма: <span style="color: #00a65a;font-size: 18px;"><?php echo $userInvest->paid_sum;?> USD</span></strong></br>
			    </div>
			  </div>

	<?php $this->registerJs('
	    var pieChartCanvas' . $num . ' = $("#pieChart' . $num . '").get(0).getContext("2d");
	    var pieChart' . $num . ' = new Chart(pieChartCanvas' . $num . ');
	    var PieData' . $num . ' = [
	      {
	        value: ' . $userInvest->paid_sum . ',
	        color: "#f56954",
	        highlight: "#f56954",
	        label: "начислено USD"
	      },
	      {
	        value: ' . ( $userInvest->sum * 2 ) . ',
	        color: "#00a65a",
	        highlight: "#00a65a",
	        label: "всего USD"
	      },
	    ];
	    var pieOptions' . $num . ' = {
	      segmentShowStroke: true,
	      segmentStrokeColor: "#fff",
	      segmentStrokeWidth: 2,
	      percentageInnerCutout: 50, 
	      animationSteps: 100,
	      animationEasing: "easeOutBounce",
	      animateRotate: true,
	      animateScale: false,
	      responsive: true,
	      maintainAspectRatio: true,
	      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
	    };
	    pieChart' . $num . '.Doughnut(PieData' . $num . ', pieOptions' . $num . ');
	');
	?>

			</div>
		<?php endforeach;?>
	</div>
<?php endif;?>