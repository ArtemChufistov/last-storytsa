<?php echo Yii::t('app', 'Для пополнения своего личного счета на сумму: <strong>{to_sum} {to_currency}</strong> Вам необходимо перевести <strong>{sum} {currency}</strong> на кошелек <strong>{wallet}</strong>',[
	'sum' => $paymentForm->sum,
	'to_sum' => $paymentForm->to_sum,
	'currency' => $paymentForm->getCurrency()->one()->title,
	'to_currency' => $paymentForm->getToCurrency()->one()->title,
	'wallet' => $paymentForm->wallet,
])?>

<p class="margin"><?php echo Yii::t('app', 'Кошелек BitCoin');?></p>
	<div class="input-group">
	<div class="input-group-btn">
		<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-wallet"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
	</div>
	<input class="form-control" readonly="true" type="text" id = "payment-wallet" value = "<?php echo $paymentForm->wallet;?>">
</div>
</br>
<p class="margin"><?php echo Yii::t('app', 'Начисляемая Сумма {currency}', ['currency' => $paymentForm->getToCurrency()->one()->title]);?></p>
<div class="input-group">
	<div class="input-group-btn">
		<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-to-sum"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
	</div>
	<input class="form-control" readonly="true" type="text" id = "payment-to-sum" value = "<?php echo $paymentForm->to_sum;?>">
</div>
</br>
<p class="margin"><?php echo Yii::t('app', 'Сумма для перевода {currency}', ['currency' => $paymentForm->getCurrency()->one()->title]);?></p>
<div class="input-group">
	<div class="input-group-btn">
		<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-sum"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
	</div>
	<input class="form-control" readonly="true" type="text" id = "payment-sum" value = "<?php echo $paymentForm->sum;?>">
</div>
<br><p class="margin"><?php echo Yii::t('app', 'Или вы можете отсканировать QR код:');?></p>
<img class = "pull-left" src = "/profile/office/qrcode/<?php echo $paymentForm->wallet;?>/<?php echo $paymentForm->to_sum;?>">