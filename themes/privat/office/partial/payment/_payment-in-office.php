<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\payment\models\Currency;
use app\modules\payment\models\PaySystem;
use app\modules\payment\models\CurrencyCourse;

?>
<?php if ($paymentForm->isNewRecord):?>
    <?php $form = ActiveForm::begin(); ?>
        <h4><?php echo Yii::t('app', 'Пополнение личного счёта');?></h4>
        </br>
        <?php if ($paymentForm->to_currency_id == ''):?>

            <?php echo $form->field($paymentForm, 'to_currency_id')->widget(Select2::classname(), [
                'data' =>  ArrayHelper::map(Currency::getLkCurrencies(), 'id', 'title'),
                'options' => ['placeholder' => 'Выберите пополняемую валюту...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-success']) ?>
            </div>
        <?php elseif ($paymentForm->currency_id == ''):?>

            <?= $form->field($paymentForm, 'to_currency_id')->hiddenInput()->label(false); ?>

            <?php echo $form->field($paymentForm, 'currency_id')->widget(Select2::classname(), [
                'data' =>  ArrayHelper::map(Currency::find()->where(['key' => Currency::KEY_BTC])->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Выберите валюту для пополнения...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-success']) ?>
            </div>
        <?php elseif($paymentForm->to_sum == ''):?>
            <p><?php echo Yii::t('app', 'Укажите сумму, на которую хотите пополнить свой личный счёт в кабинете');?></p>

            <?= $form->field($paymentForm, 'to_currency_id')->hiddenInput()->label(false); ?>
            <?= $form->field($paymentForm, 'currency_id')->hiddenInput()->label(false); ?>

            <?= $form->field($paymentForm, 'to_sum')->label(Yii::t('app', 'Сумма {currency}', ['currency' => $paymentForm->getToCurrency()->one()->key])); ?>
        
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php if ($paymentForm->to_currency_id != $paymentForm->currency_id):?>
                <div class="form-group">
                    <strong>
                    <?php echo Yii::t('app', 'Внимание текущий курс {currency}/{to_currency}: <span style = "color: red; font-size: 18px"> {course} </span>', [
                        'currency' => $paymentForm->getCurrency()->one()->key,
                        'to_currency' => $paymentForm->getToCurrency()->one()->key,
                        'course' => CurrencyCourse::find()->where(['from_currency' => $paymentForm->getCurrency()->one()->id])->andWhere(['to_currency' => $paymentForm->getToCurrency()->one()->id])->one()->course
                    ])?></strong>
                </div>
            <?php endif;?>
        <?php elseif($paymentForm->pay_system_id == ''):?>
            <p><?php echo Yii::t('app', 'Выберите платёжную систему:');?></p>

            <?= $form->field($paymentForm, 'to_currency_id')->hiddenInput()->label(false); ?>
            <?= $form->field($paymentForm, 'currency_id')->hiddenInput()->label(false); ?>
            <?= $form->field($paymentForm, 'to_sum')->hiddenInput()->label(false); ?>

            <?php echo $form->field($paymentForm, 'pay_system_id')->widget(Select2::classname(), [
                'data' =>  ArrayHelper::map($paymentForm->getCurrency()->one()->getPaySystems()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Выберите платёжную систему...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-success']) ?>
            </div>

        <?php elseif (!empty($paymentForm->getErrors())):?>
            <?php foreach($paymentForm->getErrors() as $error):?>
                <p><?php echo $error[0];?></p>
            <?php endforeach;?>
        <?php endif;?>
    <?php ActiveForm::end(); ?>
<?php else:?>

    <?php echo Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] .'/office/partial/payment/_payment-in-' . $paymentForm->getPaySystem()->one()->key, [
        'paymentForm' => $paymentForm
    ]);?>

<?php endif;?>