<?php echo Yii::t('app', 'Указаная сумма: <strong>{sum} {currency}</strong> будет списана с вашего личного счёта и отправлена на кошелек <strong>{wallet}</strong>',[
	'sum' => $paymentForm->sum,
	'wallet' => $paymentForm->wallet,
	'currency' => $paymentForm->getCurrency()->one()->title,
])?>

</br>
</br>
<p class="margin"><?php echo Yii::t('app', 'Кошелек BitCoin');?></p>
	<div class="input-group">
	<div class="input-group-btn">
		<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-wallet"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
	</div>
	<input class="form-control" readonly="true" type="text" id = "payment-wallet" value = "<?php echo $paymentForm->wallet;?>">
</div>

</br>
<p class="margin"><?php echo Yii::t('app', 'Списываемая Сумма {currency}', ['currency' => $paymentForm->getCurrency()->one()->title]);?></p>
<div class="input-group">
	<div class="input-group-btn">
		<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-sum"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
	</div>
	<input class="form-control" readonly="true" type="text" id = "payment-sum" value = "<?php echo $paymentForm->sum;?>">
</div>
</br>
<p class="margin"><?php echo Yii::t('app', 'Отправляемая Сумма {currency}', ['currency' => $paymentForm->getToCurrency()->one()->title]);?></p>
<div class="input-group">
	<div class="input-group-btn">
		<button class="btn btn-success payment-wallet" type="button" data-clipboard-target = "#payment-to-sum"><?php echo Yii::t('app', 'Скопировать в буфер обмена');?></button>
	</div>
	<input class="form-control" readonly="true" type="text" id = "payment-to-sum" value = "<?php echo $paymentForm->to_sum;?>">
</div>

