<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\matrix\models\Matrix;

$matrix = Matrix::find()->where(['user_id' => $user->id])->one();
?>

<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-heart-o" aria-hidden="true"></i><?php echo Yii::t('app', 'Ваша реферальная ссылка');?></h3>
  </div>
  <div class="box-body">
      <div class="form-group">
          <?php echo Html::input('text', 'refLink', Url::home(true) . '?ref=' . $user->login, ['id' => 'refLink', 'class' => 'form-control' , 'readonly' => true]);?>
      </div>
      <div class="form-group">
          <?php echo Html::input('button', '', Yii::t('app', 'Скопировать в буфер обмена'), ['class' => 'btn-clipboard btn btn-success pull-left', 'data-clipboard-target' => '#refLink']);?>
      </div>
  </div>
</div>

<?php $this->registerJs('new Clipboard(".btn-clipboard");');?>