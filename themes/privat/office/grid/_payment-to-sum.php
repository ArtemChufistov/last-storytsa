<?php if ($model->from_user_id == \Yii::$app->user->identity->id):?>
	<span class="badge bg-red"><?php echo $model->to_sum;?> <?php echo $model->getToCurrency()->one()->key;?></span>
<?php else:?>
	<span class="badge bg-green">+<?php echo $model->to_sum;?> <?php echo $model->getToCurrency()->one()->key;?></span>
<?php endif;?>