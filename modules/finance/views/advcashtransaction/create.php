<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\AdvcashTransaction */

$this->title = Yii::t('app', 'Create Advcash Transaction');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Advcash Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advcash-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
