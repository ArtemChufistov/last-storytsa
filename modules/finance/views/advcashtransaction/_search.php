<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\search\AdvcashTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advcash-transaction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ac_transfer') ?>

    <?= $form->field($model, 'ac_start_date') ?>

    <?= $form->field($model, 'ac_sci_name') ?>

    <?= $form->field($model, 'ac_src_wallet') ?>

    <?php // echo $form->field($model, 'ac_dest_wallet') ?>

    <?php // echo $form->field($model, 'ac_order_id') ?>

    <?php // echo $form->field($model, 'ac_amount') ?>

    <?php // echo $form->field($model, 'ac_merchant_currency') ?>

    <?php // echo $form->field($model, 'treated') ?>

    <?php // echo $form->field($model, 'date_tread') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
