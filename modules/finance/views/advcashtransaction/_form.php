<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\AdvcashTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advcash-transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ac_transfer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ac_start_date')->textInput() ?>

    <?= $form->field($model, 'ac_sci_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ac_src_wallet')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ac_dest_wallet')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ac_order_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ac_amount')->textInput() ?>

    <?= $form->field($model, 'ac_merchant_currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'treated')->textInput() ?>

    <?= $form->field($model, 'date_tread')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
