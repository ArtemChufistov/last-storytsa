<?php

use app\modules\finance\models\Payment;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

?>

<div class="payment-search">

    <?php $form = ActiveForm::begin([
	'action' => ['index'],
	'method' => 'get',
]);?>

    <?=$form->field($model, 'id')?>

    <?php echo $form->field($model, 'from_user_id')->widget(Select2::classname(), [
	'initValueText' => $model->getFromUser()->one() ? $model->getFromUser()->one()->login : '',
	'value' => $model->getFromUser()->one() ? $model->getFromUser()->one()->id : '',
	'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
	'theme' => Select2::THEME_BOOTSTRAP,
	'pluginOptions' => [
		'minimumInputLength' => 2,
		'ajax' => [
			'url' => Url::to(['/profile/backuser/searhbylogin']),
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { return {q:params.term}; }'),
		],
		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		'templateResult' => new JsExpression('function(user) { return user.login; }'),
		'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
	],
]); ?>

    <?php echo $form->field($model, 'to_user_id')->widget(Select2::classname(), [
	'initValueText' => $model->getToUser()->one() ? $model->getToUser()->one()->login : '',
	'value' => $model->getToUser()->one() ? $model->getToUser()->one()->id : '',
	'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
	'theme' => Select2::THEME_BOOTSTRAP,
	'pluginOptions' => [
		'minimumInputLength' => 2,
		'ajax' => [
			'url' => Url::to(['/profile/backuser/searhbylogin']),
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { return {q:params.term}; }'),
		],
		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		'templateResult' => new JsExpression('function(user) { return user.login; }'),
		'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
	],
]); ?>

    <?php echo $form->field($model, 'status')->widget(Select2::classname(), [
	'data' => Payment::getStatusArray(),
	'options' => ['placeholder' => 'Выберите статус...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

    <?=$form->field($model, 'from_sum')?>

    <?php // echo $form->field($model, 'currency_id') ?>

    <?php // echo $form->field($model, 'hash') ?>

    <?php // echo $form->field($model, 'date_add') ?>

    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Поиск'), ['class' => 'btn btn-primary'])?>
        <?=Html::resetButton(Yii::t('app', 'Сбросить'), ['class' => 'btn btn-default'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
