<?php

use app\modules\finance\models\Currency;
use app\modules\finance\models\Payment;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */
/* @var $form yii\widgets\ActiveForm */

$url = \yii\helpers\Url::to(['/admin/finance/payment/userlist']);
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

        <?php $form = ActiveForm::begin();?>

        <?php echo $form->field($model, 'from_user_id')->widget(Select2::classname(), [
	'initValueText' => !empty($model->getFromUser()->one()) ? $model->getFromUser()->one()->login : '',
	'options' => ['placeholder' => 'Выберите пользователя'],
	'pluginOptions' => [
		'allowClear' => true,
		'minimumInputLength' => 2,
		'language' => [
			'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
		],
		'ajax' => [
			'url' => $url,
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { console.log(params);return {q:params.term}; }'),
		],
		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		'templateResult' => new JsExpression('function(city) { return city.text; }'),
		'templateSelection' => new JsExpression('function (city) { return city.text; }'),
	],
]);
?>

        <?php echo $form->field($model, 'to_user_id')->widget(Select2::classname(), [
	'initValueText' => !empty($model->getToUser()->one()) ? $model->getToUser()->one()->login : '', // set the initial display text
	'options' => ['placeholder' => 'Выберите пользователя'],
	'pluginOptions' => [
		'allowClear' => true,
		'minimumInputLength' => 2,
		'language' => [
			'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
		],
		'ajax' => [
			'url' => $url,
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { console.log(params);return {q:params.term}; }'),
		],
		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		'templateResult' => new JsExpression('function(city) { return city.text; }'),
		'templateSelection' => new JsExpression('function (city) { return city.text; }'),
	],
]);
?>

        <?php echo $form->field($model, 'status')->widget(Select2::classname(), [
	'data' => Payment::getStatusArray(),
	'options' => ['placeholder' => 'Выберите статус...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

        <?=$form->field($model, 'to_sum')->textInput()?>

        <?php echo $form->field($model, 'from_currency_id')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
	'options' => ['placeholder' => 'Выберите валюту...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

        <?=$form->field($model, 'hash')->textInput(['maxlength' => true])?>

        <?=$form->field($model, 'date_add')->widget(DatePicker::classname(), [
	'options' => ['placeholder' => $model->getAttributeLabel('date_add')],
	'type' => DatePicker::TYPE_COMPONENT_APPEND,
	'pluginOptions' => [
		'autoclose' => true,
		'format' => 'yyyy-mm-dd',
	],
]);?>

        <?=$form->field($model, 'additional_info')->widget(CKEditor::className(), [
	'editorOptions' => ElFinder::ckeditorOptions('elfinder', []),
]);?>

        <div class="form-group">
            <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
        </div>

        <?php ActiveForm::end();?>

        </div>
      </div>
    </div>
</div>
