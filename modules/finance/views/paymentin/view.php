<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\Payment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Платежи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?=Html::encode($this->title)?></h2>
        </div>
        <div class="box-body">

            <?=Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
            <?=Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
	'class' => 'btn btn-danger',
	'data' => [
		'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
		'method' => 'post',
	],
])?>

            <?=DetailView::widget([
	'model' => $model,
	'attributes' => [
		'id',
		[
			'attribute' => 'from_user_id',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_from_user_id', ['model' => $model]);},
		], [
			'attribute' => 'to_user_id',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_to_user_id', ['model' => $model]);},
		], [
			'attribute' => 'status',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_status', ['model' => $model]);},
		], [
			'attribute' => 'hash',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_hash', ['model' => $model]);},
		], [
			'attribute' => 'transaction_hash',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_transaction_hash', ['model' => $model]);},
		],
		'from_sum',
		'to_sum',
		[
			'attribute' => 'to_currency_id',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_to-currency', ['model' => $model]);},
		],
		'date_add',
	],
])?>
        </div>
      </div>
    </div>
  </div>
</div>
