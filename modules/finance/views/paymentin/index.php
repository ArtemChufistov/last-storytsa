<?php

use app\modules\finance\models\Currency;
use app\modules\finance\models\Payment;
use app\modules\finance\models\PaySystem;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Платежи');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

  <h1><?=Html::encode($this->title)?></h1>
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo Yii::t('app', 'Поиск'); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header">

        </div>
        <?php Pjax::begin();?>

            <div class="box-body no-padding paymentGrid">
                <?=GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		['class' => 'yii\grid\ActionColumn'],
		'id',
		[
			'attribute' => 'to_user_id',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[to_user_id]',
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Введите логин'),
				],
				'pluginOptions' => [
					'minimumInputLength' => 2,
					'ajax' => [
						'url' => Url::to(['/profile/backuser/searhbylogin']),
						'dataType' => 'json',
						'data' => new JsExpression('function(params) { return {q:params.term}; }'),
					],
					'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					'templateResult' => new JsExpression('function(user) { return user.login; }'),
					'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
				],
			]),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_to_user_id', ['model' => $model]);},
		], [
			'attribute' => 'from_currency_id',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[from_currency_id]',
				'value' => $searchModel->from_currency_id,
				'attribute' => 'from_currency_id',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]),
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_from-currency', ['model' => $model]);},
		], [
			'attribute' => 'to_currency_id',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[to_currency_id]',
				'value' => $searchModel->to_currency_id,
				'attribute' => 'to_currency_id',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]),
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_to-currency', ['model' => $model]);},
		],
		'from_sum',
		'to_sum',
		[
			'attribute' => 'from_pay_system_id',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[from_pay_system_id]',
				'value' => $searchModel->from_pay_system_id,
				'attribute' => 'from_pay_system_id',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => ArrayHelper::map(PaySystem::find()->all(), 'id', 'name'),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]),
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_from-pay-system', ['model' => $model]);},
		], [
			'attribute' => 'status',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[status]',
				'value' => $searchModel->status,
				'attribute' => 'status',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => array_merge([Yii::t('app', 'Не выбрано')], Payment::getStatusArray()),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]
			),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_status', ['model' => $model]);},
		], [
			'attribute' => Yii::t('app', 'Действия'),
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_payment-actions', ['model' => $model]);},
		], [
			'attribute' => 'date_add',
			'format' => 'raw',
			'filter' => DatePicker::widget([
				'model' => $searchModel,
				'attribute' => 'date_add',
				'options' => ['class' => 'form-control'],
				'language' => 'ru',

			]),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_date-add', ['model' => $model]);},
		],
	]]);?>
            </div>
        <?php Pjax::end();?>
      </div>
    </div>
  </div>
</div>

<div class="processPayModal modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo Yii::t('app', 'Оплаты'); ?></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Закрыть'); ?></button>
      </div>
    </div>
  </div>
</div>

<?php
$this->registerJs("
$('.payment-index').on('click', '.toprocesssystem', function(){
  $.get( $(this).attr('href'), function( data ) {
    $.pjax.reload({container:'.paymentGrid'});
    $('.processPayModal').find('.modal-body').html(data);
    $('.processPayModal').modal('show');
  });
  return false;
})
");
?>

