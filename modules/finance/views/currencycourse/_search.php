<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CurrencyCourseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-course-search">

    <?php $form = ActiveForm::begin([
	'action' => ['index'],
	'method' => 'get',
	'options' => [
		'data-pjax' => 1,
	],
]);?>

    <?=$form->field($model, 'id')?>

    <?=$form->field($model, 'from_pay_system_id')?>

    <?=$form->field($model, 'to_pay_system_id')?>

    <?=$form->field($model, 'from_currency')?>

    <?=$form->field($model, 'to_currency')?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'course') ?>

    <?php // echo $form->field($model, 'course_function') ?>

    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary'])?>
        <?=Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
