<?php

use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\models\CurrencyCourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Курсы валют');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-course-index">

    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск'); ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                <h1><?=Html::encode($this->title)?></h1>
                <p>
                    <?=Html::a(Yii::t('app', 'Добавить курс'), ['create'], ['class' => 'btn btn-success'])?>
                </p>
            </div>
            <?php Pjax::begin();?>

                <?=GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],
		'id',
		[
			'attribute' => 'from_pay_system_id',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[from_pay_system_id]',
				'value' => $searchModel->from_pay_system_id,
				'attribute' => 'from_pay_system_id',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => ArrayHelper::map(PaySystem::find()->all(), 'id', 'name'),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]),
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_from-pay-system', ['model' => $model]);},
		], [
			'attribute' => 'to_pay_system_id',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[to_pay_system_id]',
				'value' => $searchModel->to_pay_system_id,
				'attribute' => 'to_pay_system_id',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => ArrayHelper::map(PaySystem::find()->all(), 'id', 'name'),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]),
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_to-pay-system', ['model' => $model]);},
		], [
			'attribute' => 'from_currency',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[from_currency]',
				'value' => $searchModel->to_pay_system_id,
				'attribute' => 'to_pay_system_id',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]),
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_from-currency', ['model' => $model]);},
		], [
			'attribute' => 'to_currency',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[to_currency]',
				'value' => $searchModel->to_currency,
				'attribute' => 'to_currency',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]),
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_to-currency', ['model' => $model]);},
		], [
			'attribute' => 'type',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[type]',
				'value' => $searchModel->type,
				'attribute' => 'type',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => array_merge([Yii::t('app', 'Не выбрано')], CurrencyCourse::getTypeArray()),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]
			),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_type', ['model' => $model]);},
		],
		'course',
		[
			'attribute' => 'comission_type',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[comission_type]',
				'value' => $searchModel->comission_type,
				'attribute' => 'type',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => array_merge([Yii::t('app', 'Не выбрано')], CurrencyCourse::getComissionTypeArray()),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]
			),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_comission-type', ['model' => $model]);},
		],
		'comission',
		[
			'attribute' => 'active',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[active]',
				'value' => $searchModel->active,
				'attribute' => 'type',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => array_merge([Yii::t('app', 'Не выбрано')], CurrencyCourse::getActiveArray()),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]
			),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_active', ['model' => $model]);},
		],
		['class' => 'yii\grid\ActionColumn'],
	],
]);?>
                </div>
            <?php Pjax::end();?>
          </div>
        </div>
    </div>
</div>
