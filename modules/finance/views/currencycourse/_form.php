<?php

use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CurrencyCourse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

            <?php $form = ActiveForm::begin();?>

            <?php echo $form->field($model, 'from_pay_system_id')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(PaySystem::find()->all(), 'id', 'name'),
	'options' => ['placeholder' => 'Выберите начальную плат. систему'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

            <?php echo $form->field($model, 'to_pay_system_id')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(PaySystem::find()->all(), 'id', 'name'),
	'options' => ['placeholder' => 'Выберите конечную плат. систему'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

            <?php echo $form->field($model, 'from_currency')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
	'options' => ['placeholder' => 'Выберите начальную валюту...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

            <?php echo $form->field($model, 'to_currency')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
	'options' => ['placeholder' => 'Выберите конечную валюту...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

            <?php echo $form->field($model, 'type')->widget(Select2::classname(), [
	'data' => CurrencyCourse::getTypeArray(),
	'options' => ['placeholder' => 'Выберите тип...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

            <?php echo $form->field($model, 'comission_type')->widget(Select2::classname(), [
	'data' => CurrencyCourse::getComissionTypeArray(),
	'options' => ['placeholder' => 'Выберите тип...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

            <?=$form->field($model, 'comission')->textInput(['maxlength' => true])?>

            <?=$form->field($model, 'course')->textInput(['maxlength' => true])?>

            <?=$form->field($model, 'course_function')->textInput(['maxlength' => true])?>

            <?=$form->field($model, 'min_sum')->textInput(['maxlength' => true])?>

            <?=$form->field($model, 'one_active')->textInput(['maxlength' => true])?>

            <?php echo $form->field($model, 'active')->widget(Select2::classname(), [
	'data' => CurrencyCourse::getActiveArray(),
	'options' => ['placeholder' => 'Выберите тип...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>


            <div class="form-group">
                <?=Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success'])?>
            </div>

            <?php ActiveForm::end();?>
        </div>
      </div>
    </div>
</div>
