<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CurrencyCourse */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Курсы валют'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-course-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?=Html::encode($this->title)?></h2>
        </div>
        <div class="box-body">

            <p>
                <?=Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
                <?=Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
	'class' => 'btn btn-danger',
	'data' => [
		'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
		'method' => 'post',
	],
])?>
            </p>

            <?=DetailView::widget([
	'model' => $model,
	'attributes' => [
		'id',
		[
			'attribute' => 'from_pay_system_id',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_from-pay-system', ['model' => $model]);},
		], [
			'attribute' => 'to_pay_system_id',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_to-pay-system', ['model' => $model]);},
		], [
			'attribute' => 'from_currency',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_from-currency', ['model' => $model]);},
		], [
			'attribute' => 'to_currency',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_to-currency', ['model' => $model]);},
		],
		'type',
		'course',
		'course_function',
	],
])?>
        </div>
      </div>
    </div>
  </div>
</div>
