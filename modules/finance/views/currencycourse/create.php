<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CurrencyCourse */

$this->title = Yii::t('app', 'Добавление курса валют');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Курсы валют'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-course-create">

    <h1><?=Html::encode($this->title)?></h1>

    <?=$this->render('_form', [
	'model' => $model,
])?>

</div>
