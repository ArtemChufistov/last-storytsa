<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\UserPayWalletPerfectMoney */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-pay-wallet-perfect-money-form">

    <?php $form = ActiveForm::begin();?>

    <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
	'initValueText' => $model->getUser()->one()->login,
	'value' => $model->getUser()->one()->id,
	'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
	'theme' => Select2::THEME_BOOTSTRAP,
	'pluginOptions' => [
		'minimumInputLength' => 3,
		'ajax' => [
			'url' => Url::to(['/profile/backuser/searhbylogin']),
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { return {q:params.term}; }'),
		],
		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		'templateResult' => new JsExpression('function(user) { return user.login; }'),
		'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
	],
]); ?>

    <?=$form->field($model, 'wallet')->textInput(['maxlength' => true])?>

    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
