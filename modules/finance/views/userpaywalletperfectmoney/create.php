<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\UserPayWalletPerfectMoney */

$this->title = Yii::t('app', 'Добавление кошелька Perfect Money');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Кошельки Perfect Money участников'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-pay-wallet-perfect-money-create">

    <h1><?=Html::encode($this->title)?></h1>

    <?=$this->render('_form', [
	'model' => $model,
])?>

</div>
