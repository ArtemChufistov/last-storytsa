<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\UserPayWalletPerfectMoneySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-pay-wallet-perfect-money-search">

    <?php $form = ActiveForm::begin([
	'action' => ['index'],
	'method' => 'get',
	'options' => [
		'data-pjax' => 1,
	],
]);?>

    <?=$form->field($model, 'id')?>

    <?=$form->field($model, 'user_id')?>

    <?=$form->field($model, 'pay_system_id')?>

    <?=$form->field($model, 'currency_id')?>

    <?php // echo $form->field($model, 'wallet') ?>

    <?php // echo $form->field($model, 'in_wallet') ?>

    <?php // echo $form->field($model, 'wallet_id') ?>

    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary'])?>
        <?=Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
