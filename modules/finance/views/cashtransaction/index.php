<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use app\modules\finance\models\CashTransaction;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\models\search\CashTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Платёжные поручения');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-transaction-index">

    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                <h1><?=Html::encode($this->title)?></h1>
            </div>
            <?php Pjax::begin();?>

            <div class="box-body no-padding">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'payment_id',
                    [
                        'attribute' => Yii::t('app', 'Логин'),
                        'format' => 'raw',
                        'filter' => Select2::widget([
                            'name' => (new ReflectionClass($searchModel))->getShortName() . '[user_id]',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Введите логин'),
                            ],
                            'pluginOptions' => [
                                'minimumInputLength' => 2,
                                'ajax' => [
                                    'url' => Url::to(['/profile/backuser/searhbylogin']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                            ],
                        ]),

                        'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_cash-transaction-login', ['model' => $model]);},
                    ],[
                        'attribute' => 'amount',
                        'format' => 'raw',
                        'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_cash-transaction-amount', ['model' => $model]);},
                    ],
                    'name',
                    'address',
                    'email:email',
                    'phone',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'filter' => Select2::widget([
                            'name' => (new ReflectionClass($searchModel))->getShortName() . '[status]',
                            'value' => $searchModel->status,
                            'attribute' => 'status',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'data' => array_merge([Yii::t('app', 'Не выбрано')], CashTransaction::getStatusArray()),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Выберите'),
                                'style' => ['width' => '150px'],
                            ]]
                        ),
                        'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_cash-transaction-status', ['model' => $model]);},
                    ],[
                        'attribute' => 'date_add',
                        'format' => 'raw',
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'date_add',
                            'options' => ['class' => 'form-control'],
                            'language' => 'ru',

                        ]),
                        'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_date-add', ['model' => $model]);},
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            </div>

            <?php Pjax::end();?>
          </div>
        </div>
    </div>
</div>
