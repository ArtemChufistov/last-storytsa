<?php

use app\modules\finance\models\Currency;
use app\modules\finance\models\CashTransaction;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CashTransaction */
/* @var $form yii\widgets\ActiveForm */

$payment = $model->getPayment()->one();
$user = $payment->getToUser()->one();
if (!empty($user)){
    $userLogin = $user->login;
}else{
    $user = $payment->getFromUser()->one();
    $userLogin = $user->login;
}

?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <div class="form-group field-cashtransaction-name required">
                <label class="control-label" for="cashtransaction-name"><?php echo Yii::t('app', 'Логин');?></label>
                <?= Html::input('text', 'login', $userLogin, ['class' => 'form-control', 'readonly' => true]) ?>
            </div>

            <div class="form-group field-cashtransaction-name required">
                <label class="control-label" for="cashtransaction-name"><?php echo Yii::t('app', 'Платёж №');?></label>
                <?= Html::input('text', 'payment_id', $payment->id, ['class' => 'form-control', 'readonly' => true]) ?>
            </div>

            <?php echo $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => CashTransaction::getUserStatusArray(),
                'options' => ['placeholder' => 'Выберите статус...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>

            <?= $form->field($model, 'amount')->textInput() ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'skype')->textInput(['maxlength' => true]) ?>

            <?=$form->field($model, 'date_add')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => $model->getAttributeLabel('date_add')],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ],
            ]);?>

            <?=$form->field($model, 'description')->widget(CKEditor::className(), [
                'editorOptions' => ElFinder::ckeditorOptions('elfinder', []),
            ]);?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
      </div>
    </div>
</div>
