<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CashTransaction */

$this->title = Yii::t('app', 'Добавление платёжного поручения');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Платёжные поручения'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
