<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\PaymentLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-log-form">

    <?php $form = ActiveForm::begin();?>

    <?=$form->field($model, 'payment_id')->textInput()?>

    <?=$form->field($model, 'status')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'date_add')->textInput()?>

    <div class="form-group">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
