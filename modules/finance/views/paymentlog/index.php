<?php

use app\modules\finance\models\Payment;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\models\PaymentLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'История изменения статусов заявки');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-log-index">
    <h1><?=Html::encode($this->title)?></h1>
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск'); ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">

            </div>
            <?php Pjax::begin();?>

                <div class="box-body no-padding">

                    <?=GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],

		'id',
		'payment_id',
		[
			'attribute' => 'status',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[status]',
				'value' => $searchModel->status,
				'attribute' => 'status',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => array_merge([Yii::t('app', 'Не выбрано')], Payment::getStatusArray()),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]
			),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_status-log', ['model' => $model]);},
		],
		'date_add',

		['class' => 'yii\grid\ActionColumn'],
	],
]);?>
                </div>
            <?php Pjax::end();?>
          </div>
        </div>
    </div>
</div>
