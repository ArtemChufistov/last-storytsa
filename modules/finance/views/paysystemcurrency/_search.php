<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\PaySystemCurrencySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pay-system-currency-search">

    <?php $form = ActiveForm::begin([
	'action' => ['index'],
	'method' => 'get',
	'options' => [
		'data-pjax' => 1,
	],
]);?>

    <?=$form->field($model, 'id')?>

    <?=$form->field($model, 'pay_system_id')?>

    <?=$form->field($model, 'currency_id')?>

    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary'])?>
        <?=Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
