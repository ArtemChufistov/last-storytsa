<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\PaySystemCurrency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pay-system-currency-form">

    <?php $form = ActiveForm::begin();?>

    <?=$form->field($model, 'pay_system_id')->textInput()?>

    <?=$form->field($model, 'currency_id')->textInput()?>

    <div class="form-group">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
