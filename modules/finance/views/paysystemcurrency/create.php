<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\PaySystemCurrency */

$this->title = Yii::t('app', 'Create Pay System Currency');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pay System Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-system-currency-create">

    <h1><?=Html::encode($this->title)?></h1>

    <?=$this->render('_form', [
	'model' => $model,
])?>

</div>
