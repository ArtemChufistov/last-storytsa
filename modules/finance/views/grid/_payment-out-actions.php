<?php use yii\helpers\Url;?>

<a class = "toprocesssystem" href = "<?php echo Url::to(['/finance/paymentout/toprocesssystem', 'id' => $model->id]); ?>" title = "<?php echo Yii::t('app', 'В обработку системой'); ?>">
	<span class ="glyphicon glyphicon-repeat"></span>
</a>