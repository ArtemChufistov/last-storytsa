<?php 
use app\modules\finance\models\Payment;

$payment = $model->getPayment()->one();
?>

<?php if (!empty($payment) && $payment->type == Payment::TYPE_IN_OFFICE):?>
	<small class="label label-success"></i><?php echo $model->amount;?> <?php echo $payment->getFromCurrency()->one()->key;?></small>
<?php elseif(!empty($payment) && $payment->type == Payment::TYPE_OUT_OFFICE): ?>
	<small class="label label-danger"></i>-<?php echo $model->amount;?> <?php echo $payment->getToCurrency()->one()->key;?></small>
<?php endif;?>