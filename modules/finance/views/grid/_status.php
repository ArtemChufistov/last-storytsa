<?php
use app\modules\finance\models\Payment;
?>
<?php if ($model->status == Payment::STATUS_WAIT): ?>
	<small class="label label-info"><i class="fa fa-clock-o"></i> <?php echo $model->getStatusTitle(); ?></small>
<?php elseif ($model->status == Payment::STATUS_WAIT_SYSTEM): ?>
	<small class="label label-primary"><i class="fa fa-clock-o"></i> <?php echo $model->getStatusTitle(); ?></small>
<?php elseif ($model->status == Payment::PROCESS_SYSTEM): ?>
	<small class="label label-default"><i class="fa fa-clock-bolt"></i> <?php echo $model->getStatusTitle(); ?></small>
<?php elseif ($model->status == Payment::STATUS_OK): ?>
	<small class="label label-success"><i class="fa fa-check-circle-o"></i> <?php echo $model->getStatusTitle(); ?></small>
<?php elseif ($model->status == Payment::STATUS_CANCEL): ?>
	<small class="label label-warning"><i class="fa fa-times"></i> <?php echo $model->getStatusTitle(); ?></small>
<?php endif;?>