<?php
use app\modules\finance\models\CashTransaction;
?>
<?php if ($model->status == CashTransaction::STATUS_NEW): ?>
	<small class="label label-info"><i class="fa fa-clock-o"></i> <?php echo $model->getStatusTitle(); ?></small>
<?php elseif ($model->status == CashTransaction::STATUS_OK): ?>
	<small class="label label-primary"><i class="fa fa-clock-o"></i> <?php echo $model->getStatusTitle(); ?></small>
<?php elseif ($model->status == CashTransaction::STATUS_PAY): ?>
	<small class="label label-default"><i class="fa fa-clock-bolt"></i> <?php echo $model->getStatusTitle(); ?></small>
<?php endif;?>