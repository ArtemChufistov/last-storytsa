<?php use yii\helpers\Url;?>
<a class="showcart"  title = "<?php echo Yii::t('app', 'Подробная информация'); ?>" href="<?php echo Url::to(['/finance/paymentout/detailinfo', 'id' => $model->id]);?>">
    <span class ="glyphicon glyphicon-list-alt"></span>
</a>
<a class = "toprocesssystem" href = "<?php echo empty($model->from_user_id) ? Url::to(['/finance/paymentin/toprocesssystem', 'id' => $model->id]) : Url::to(['/finance/paymentout/toprocesssystem', 'id' => $model->id]); ?>" title = "<?php echo Yii::t('app', 'В обработку системой'); ?>">
	<span class ="glyphicon glyphicon-repeat"></span>
</a>