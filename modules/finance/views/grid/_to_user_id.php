<?php
use yii\helpers\Url;

$userInfo = $model->getToUser()->one();
?>
<?php if (!empty($userInfo)): ?>
	<a href="<?php echo Url::to(['/admin-user/user/view', 'id' => $userInfo->id]); ?>" target="_blank" ><?php echo $userInfo->login; ?></a>
<?php endif;?>