<?php

use app\modules\finance\models\Currency;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\Currency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

		    <?php $form = ActiveForm::begin();?>

	        <?php echo $form->field($model, 'key')->widget(Select2::classname(), [
	'data' => Currency::getKeyArray(),
	'options' => ['placeholder' => 'Выберите валюту...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

		    <?=$form->field($model, 'title')->textInput(['maxlength' => true])?>
		    <?=$form->field($model, 'course_to_usd')->textInput(['maxlength' => true])?>
		    <?=$form->field($model, 'auto_course_update')->textInput(['maxlength' => true])?>
		    <?=$form->field($model, 'func_course_update')->textInput(['maxlength' => true])?>

	        <?php echo $form->field($model, 'group')->widget(Select2::classname(), [
	'data' => Currency::getGroupArray(),
	'options' => ['placeholder' => 'Выберите группу...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

		    <div class="form-group">
		        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
		    </div>

		    <?php ActiveForm::end();?>

        </div>
      </div>
    </div>
</div>
