<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\Currency */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Валюты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?=Html::encode($this->title)?></h2>
        </div>
        <div class="box-body">

            <p>
                <?=Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
                <?=Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                	'class' => 'btn btn-danger',
                	'data' => [
                		'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                		'method' => 'post',
                	],
                ])?>
            </p>

            <?=DetailView::widget([
            	'model' => $model,
            	'attributes' => [
            		'id',
            		'key',
            		'title',
            	],
            ])?>

        </div>
      </div>
    </div>
</div>
