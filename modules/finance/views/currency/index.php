<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\models\CurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Валюты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-index">

    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск'); ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                <h1><?=Html::encode($this->title)?></h1>
                <p>
                    <?=Html::a(Yii::t('app', 'Добавить валюту'), ['create'], ['class' => 'btn btn-success'])?>
                </p>
            </div>
            <?php Pjax::begin();?>

                <div class="box-body no-padding">

                <?=GridView::widget([
                	'dataProvider' => $dataProvider,
                	'filterModel' => $searchModel,
                	'columns' => [
                		['class' => 'yii\grid\SerialColumn'],

                		'id',
                		'key',
                		'title',
                		'course_to_usd',
                		'auto_course_update',
                		'group',
                		['class' => 'yii\grid\ActionColumn'],
                	],
                ]);?>
                </div>
            <?php Pjax::end();?>
          </div>
        </div>
    </div>
</div>

