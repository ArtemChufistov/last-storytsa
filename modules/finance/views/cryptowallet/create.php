<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CryptoWallet */

$this->title = Yii::t('app', 'Добавление крипто кошелька');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Крипто кошельки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crypto-wallet-create">

    <h1><?=Html::encode($this->title)?></h1>

    <?=$this->render('_form', [
	'model' => $model,
])?>

</div>
