<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CryptoWalletSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crypto-wallet-search">

    <?php $form = ActiveForm::begin([
	'action' => ['index'],
	'method' => 'get',
	'options' => [
		'data-pjax' => 1,
	],
]);?>

    <?=$form->field($model, 'id')?>

    <?=$form->field($model, 'wallet')?>

    <?=$form->field($model, 'balance')?>

    <?=$form->field($model, 'info')?>

    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary'])?>
        <?=Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
