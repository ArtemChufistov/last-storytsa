<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CryptoWallet */

$this->title = Yii::t('app', 'Редактирование {modelClass}: ', [
	'modelClass' => 'кошелька',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Крипто кошельки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="crypto-wallet-update">

    <h1><?=Html::encode($this->title)?></h1>

    <?=$this->render('_form', [
	'model' => $model,
])?>

</div>
