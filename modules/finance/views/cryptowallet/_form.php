<?php

use app\modules\finance\models\Currency;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CryptoWallet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

            <?php $form = ActiveForm::begin();?>

            <?=$form->field($model, 'wallet')->textInput(['maxlength' => true])?>

            <?=$form->field($model, 'balance')->textInput()?>

            <?php echo $form->field($model, 'currency_id')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
	'options' => ['placeholder' => 'Выберите валюту...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

            <?=$form->field($model, 'info')->textInput(['maxlength' => true])?>

            <div class="form-group">
                <?=Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success'])?>
            </div>

            <?php ActiveForm::end();?>

        </div>
      </div>
    </div>
</div>
