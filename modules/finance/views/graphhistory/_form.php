<?php

use app\modules\finance\models\Currency;
use app\modules\finance\models\forms\CurrencyGraphHistoryForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */
/* @var $form yii\widgets\ActiveForm */

$url = \yii\helpers\Url::to(['/admin/finance/graphpoloniex/index']);
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

        <?php $form = ActiveForm::begin([
	'action' => ['index'],
	'method' => 'post',
]);?>

        <?php echo $form->field($model, 'from_currency_id')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
	'options' => ['placeholder' => 'Выберите валюту...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

        <?php echo $form->field($model, 'to_currency_id')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
	'options' => ['placeholder' => 'Выберите валюту...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

        <?=$form->field($model, 'date_from')->widget(DatePicker::classname(), [
	'options' => ['placeholder' => $model->getAttributeLabel('date_from')],
	'type' => DatePicker::TYPE_COMPONENT_APPEND,
	'pluginOptions' => [
		'autoclose' => true,
		'format' => 'yyyy-mm-dd',
	],
]);?>

        <?=$form->field($model, 'date_to')->widget(DatePicker::classname(), [
	'options' => ['placeholder' => $model->getAttributeLabel('date_to'), 'value' => date('Y-m-d')],
	'type' => DatePicker::TYPE_COMPONENT_APPEND,
	'pluginOptions' => [
		'autoclose' => true,
		'format' => 'yyyy-mm-dd',
	],
]);?>

        <?=$form->field($model, 'breakdown_type')->textInput(['value' => 7200])?>

        <?php echo $form->field($model, 'exchange')->widget(Select2::classname(), [
	'data' => CurrencyGraphHistoryForm::getExchangeArray(),
	'options' => ['placeholder' => 'Выберите биржу...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

        <div class="form-group">
            <?=Html::submitButton(Yii::t('app', 'Залить'), ['class' => 'btn btn-primary'])?>
        </div>

        <?php ActiveForm::end();?>

        </div>
      </div>
    </div>
</div>
