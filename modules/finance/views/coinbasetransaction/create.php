<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CoinbaseTransaction */

$this->title = Yii::t('app', 'Create Coinbase Transaction');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coinbase Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coinbase-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
