<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CoinbaseTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coinbase-transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wallet_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'resource')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'resource_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'instant_exchange')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'network_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'network_hash')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'processed')->textInput() ?>

    <?= $form->field($model, 'payment_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
