<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\search\CoinbaseTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coinbase-transaction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'wallet_id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'resource') ?>

    <?php // echo $form->field($model, 'resource_path') ?>

    <?php // echo $form->field($model, 'instant_exchange') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'network_status') ?>

    <?php // echo $form->field($model, 'network_hash') ?>

    <?php // echo $form->field($model, 'processed') ?>

    <?php // echo $form->field($model, 'payment_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
