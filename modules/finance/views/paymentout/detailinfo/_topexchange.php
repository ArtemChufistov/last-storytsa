<?php use yii\helpers\Url;?>
<?php
$paymentData = json_decode($payment->additional_info);

$paymentInfo = [
    'sub_pay_system' => Yii::t('app', 'Платёжная система'),
    'recive_wallet' => Yii::t('app', 'Кошелёк'),
    'user_phone' => Yii::t('app', 'Телефон'),
    'user_email' => Yii::t('app', 'E-mail'),
]
?>
<div class="row mb-10">
    <div class="col-md-4">
        <?php echo Yii::t('app', 'Пользователь'); ?>:
    </div>
    <div class="col-md-4">
        <?php echo $payment->getFromUser()->one()->login; ?>
    </div>
</div>
<div class="row mb-10">
    <div class="col-md-4">
        <?php echo Yii::t('app', 'Сумма'); ?>:
    </div>
    <div class="col-md-4">
        <?php echo $payment->to_sum; ?>
    </div>
</div>

<?php foreach($paymentData as $dataKey => $dataItem):?>
    <?php if (isset($paymentInfo[$dataKey])): ?>
    <div class="row mb-10">
        <div class="col-md-4">
            <?php echo $paymentInfo[$dataKey]; ?>:
        </div>
        <div class="col-md-4">
            <?php echo $dataItem; ?>
        </div>
    </div>
    <?php endif;?>
<?php endforeach;?>

<div class="row mb-10">
    <div class="col-md-4">
        <a href = "<?php echo empty($payment->from_user_id) ? Url::to(['/finance/paymentin/toprocesssystem', 'id' => $payment->id]) : Url::to(['/finance/paymentout/toprocesssystem', 'id' => $payment->id]); ?>" class="btn btn-primary toprocesssystem" data-dismiss="modal"><?php echo Yii::t('app', 'Оплатить'); ?></a>
    </div>
</div>

<?php if (!empty($payment->error)): ?>
    <div class="row mb-10">
        <div class="col-md-4">
            <?php echo $payment->error;?>
        </div>
    </div>
<?php endif;?>
