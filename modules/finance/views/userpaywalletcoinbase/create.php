<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\UserPayWalletBitcoin */

$this->title = Yii::t('app', 'Добавить Кошелек Coinbase');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Кошельки Coinbase участников'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-pay-wallet-coinbase-create">

    <h1><?=Html::encode($this->title)?></h1>

    <?=$this->render('_form', [
	'model' => $model,
])?>

</div>
