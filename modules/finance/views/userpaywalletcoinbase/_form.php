<?php

use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\UserPayWalletBitcoin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
            <div class="user-pay-wallet-bitcoin-form">

                <?php $form = ActiveForm::begin();?>

                <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
	'initValueText' => $model->getUser()->one()->login,
	'value' => $model->getUser()->one()->id,
	'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
	'theme' => Select2::THEME_BOOTSTRAP,
	'pluginOptions' => [
		'minimumInputLength' => 2,
		'ajax' => [
			'url' => Url::to(['/profile/backuser/searhbylogin']),
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { return {q:params.term}; }'),
		],
		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		'templateResult' => new JsExpression('function(user) { return user.login; }'),
		'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
	],
]); ?>

                <?php echo $form->field($model, 'pay_system_id')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(PaySystem::find()->all(), 'id', 'name'),
	'options' => ['placeholder' => Yii::t('app', 'Выберите')],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

                <?php echo $form->field($model, 'currency_id')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
	'options' => ['placeholder' => Yii::t('app', 'Выберите')],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

                <?=$form->field($model, 'site_url')->textInput(['maxlength' => true])?>

                <?=$form->field($model, 'wallet')->textInput(['maxlength' => true])->label(Yii::t('app', 'Кошелек для вывода'));?>

                <?=$form->field($model, 'in_wallet')->textInput(['maxlength' => true])?>

                <div class="form-group">
                    <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
                </div>

                <?php ActiveForm::end();?>

            </div>
        </div>
      </div>
    </div>
</div>
