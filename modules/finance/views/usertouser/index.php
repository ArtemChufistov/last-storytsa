<?php

use app\modules\finance\models\Transaction;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Внутренние переводы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-index">

    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск'); ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                <h1><?=Html::encode($this->title)?></h1>
                <p>
                    <?=Html::a(Yii::t('app', 'Сделать перевод'), ['create'], ['class' => 'btn btn-success'])?>
                </p>
            </div>
            <?php Pjax::begin();?>

                <div class="box-body no-padding">

                    <?=GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],
		'id',
		[
			'attribute' => 'from_user_id',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[from_user_id]',
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Введите логин'),
				],
				'pluginOptions' => [
					'minimumInputLength' => 2,
					'ajax' => [
						'url' => Url::to(['/profile/backuser/searhbylogin']),
						'dataType' => 'json',
						'data' => new JsExpression('function(params) { return {q:params.term}; }'),
					],
					'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					'templateResult' => new JsExpression('function(user) { return user.login; }'),
					'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
				],
			]),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_from_user_id', ['model' => $model]);},
		], [
			'attribute' => 'to_user_id',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[to_user_id]',
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Введите логин'),
				],
				'pluginOptions' => [
					'minimumInputLength' => 2,
					'ajax' => [
						'url' => Url::to(['/profile/backuser/searhbylogin']),
						'dataType' => 'json',
						'data' => new JsExpression('function(params) { return {q:params.term}; }'),
					],
					'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					'templateResult' => new JsExpression('function(user) { return user.login; }'),
					'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
				],
			]),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_to_user_id', ['model' => $model]);},
		], [
			'attribute' => 'type',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[type]',
				'value' => $searchModel->type,
				'attribute' => 'type',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => array_merge([Yii::t('app', 'Не выбрано')], Transaction::getTypeArray()),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]
			),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_type', ['model' => $model]);},
		],
		'sum',
		[
			'attribute' => 'currency_id',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_currency-id', ['model' => $model]);},
		], [
			'attribute' => 'date_add',
			'format' => 'raw',
			'filter' => DatePicker::widget([
				'model' => $searchModel,
				'attribute' => 'date_add',
				'options' => ['class' => 'form-control'],
				'language' => 'ru',

			]),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_date-add', ['model' => $model]);},
		], [
			'attribute' => 'id',
			'header' => Yii::t('app', 'Действия'),
			'format' => 'raw',
			'value' => function ($model) {
				return Yii::$app->controller->renderPartial('/grid/_recalculate-balance', ['model' => $model]);
			},
		],
		['class' => 'yii\grid\ActionColumn'],
	],
]);?>
                </div>
            <?php Pjax::end();?>
          </div>
        </div>
    </div>
</div>
<?php
$this->registerJs("

$('.transaction-index').on('click', '.recalculateBalance', function(){
    $.ajax({
      url: '/admin/payment/transaction/recalculatebalance?userId=' + $(this).attr('userId'),
    }).done(function() {
    });
})
");
?>
