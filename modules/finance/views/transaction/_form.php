<?php

use app\modules\finance\models\Currency;
use app\modules\finance\models\Transaction;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

            <?php $form = ActiveForm::begin();?>

                <?php echo $form->field($model, 'from_user_id')->widget(Select2::classname(), [
	'initValueText' => !empty($model->getFromUser()->one()) ? $model->getFromUser()->one()->login : '',
	'value' => !empty($model->getFromUser()->one()) ? $model->getFromUser()->one()->id : '',
	'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
	'theme' => Select2::THEME_BOOTSTRAP,
	'pluginOptions' => [
		'minimumInputLength' => 2,
		'ajax' => [
			'url' => Url::to(['/profile/backuser/searhbylogin']),
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { return {q:params.term}; }'),
		],
		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		'templateResult' => new JsExpression('function(user) { return user.login; }'),
		'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
	],
]); ?>

                <?php echo $form->field($model, 'to_user_id')->widget(Select2::classname(), [
	'initValueText' => !empty($model->getToUser()->one()) ? $model->getToUser()->one()->login : '',
	'value' => !empty($model->getToUser()->one()) ? $model->getToUser()->one()->id : '',
	'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
	'theme' => Select2::THEME_BOOTSTRAP,
	'pluginOptions' => [
		'minimumInputLength' => 2,
		'ajax' => [
			'url' => Url::to(['/profile/backuser/searhbylogin']),
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { return {q:params.term}; }'),
		],
		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		'templateResult' => new JsExpression('function(user) { return user.login; }'),
		'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
	],
]); ?>

            <?php echo $form->field($model, 'type')->widget(Select2::classname(), [
	'data' => Transaction::getTypeArray(),
	'options' => ['placeholder' => 'Выберите тип...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

            <?=$form->field($model, 'sum')->textInput()?>

            <?php echo $form->field($model, 'currency_id')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
	'options' => ['placeholder' => 'Выберите валюту...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

            <?=$form->field($model, 'payment_id')->textInput()?>

            <?=$form->field($model, 'data')->textarea(['rows' => 6])?>

            <?=$form->field($model, 'hash')->textInput(['maxlength' => true])?>

            <?=$form->field($model, 'date_add')->widget(DatePicker::classname(), [
	'options' => ['placeholder' => $model->getAttributeLabel('date_add')],
	'type' => DatePicker::TYPE_COMPONENT_APPEND,
	'pluginOptions' => [
		'autoclose' => true,
		'format' => 'yyyy-mm-dd',
	],
]);?>

            <?=$form->field($model, 'matrix_id')->textInput()?>

            <?=$form->field($model, 'matrix_queue_id')->textInput()?>

            <div class="form-group">
                <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
            </div>

            <?php ActiveForm::end();?>

        </div>
      </div>
    </div>
</div>
