<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\Transaction */

$this->title = Yii::t('app', 'Добавление транзакции');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Транзакции'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-create">

    <h1><?=Html::encode($this->title)?></h1>

    <?=$this->render('_form', [
	'model' => $model,
])?>

</div>
