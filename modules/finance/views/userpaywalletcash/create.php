<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\UserPayWalletCash */

$this->title = Yii::t('app', 'Create User Pay Wallet Cash');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Pay Wallet Cashes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-pay-wallet-cash-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
