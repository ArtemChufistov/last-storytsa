<?php

use app\modules\finance\models\PaySystem;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\models\PaySystemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Платёжные системы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-system-index">

    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск'); ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                <h1><?=Html::encode($this->title)?></h1>
                <p>
                    <?=Html::a(Yii::t('app', 'Добавить платёжную систему'), ['create'], ['class' => 'btn btn-success'])?>
                </p>
            </div>
            <?php Pjax::begin();?>

                <div class="box-body no-padding">

                <?=GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],

		'id',
		[
			'attribute' => 'key',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[key]',
				'value' => $searchModel->key,
				'attribute' => 'key',
				'theme' => Select2::THEME_BOOTSTRAP,
				'data' => array_merge([Yii::t('app', 'Не выбрано')], PaySystem::getPaySystemKeys()),
				'options' => [
					'placeholder' => Yii::t('app', 'Выберите'),
					'style' => ['width' => '150px'],
				]]
			),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_key', ['model' => $model]);},
		],
		'name',
		['class' => 'yii\grid\ActionColumn'],
	],
]);?>
                </div>
            <?php Pjax::end();?>
          </div>
        </div>
    </div>
</div>
