<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\PaySystem */

$this->title = Yii::t('app', 'Добавление платёжной системы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pay Systems'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-system-create">

    <h1><?=Html::encode($this->title)?></h1>

    <?=$this->render('_form', [
	'model' => $model,
])?>

</div>
