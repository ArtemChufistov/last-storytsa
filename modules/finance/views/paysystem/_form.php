<?php

use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\PaySystem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

		    <?php $form = ActiveForm::begin();?>

		    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	        <?php echo $form->field($model, 'key')->widget(Select2::classname(), [
	'data' => PaySystem::getPaySystemKeys(),
	'options' => ['placeholder' => 'Выберите пл. систему...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

	        <?php $model->currencyArray = $model->getCurrencies()->all();
echo $form->field($model, 'currencyArray')->widget(Select2::classname(), [
	'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
	'options' => ['placeholder' => Yii::t('app', 'Выберите валюты'), 'multiple' => true],
	'pluginOptions' => [
		'tags' => true,
		'tokenSeparators' => [',', ' '],
		'maximumInputLength' => 10,
	],
])->label(Yii::t('app', 'Валюты'));

?>

		    <div class="form-group">
		        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
		    </div>

		    <?php ActiveForm::end();?>

        </div>
      </div>
    </div>
</div>

