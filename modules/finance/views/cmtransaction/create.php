<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CmTransaction */

$this->title = Yii::t('app', 'Create Cm Transaction');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cm Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cm-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
