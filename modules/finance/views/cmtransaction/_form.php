<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CmTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cm-transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'account')->textInput() ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vout')->textInput() ?>

    <?= $form->field($model, 'confirmations')->textInput() ?>

    <?= $form->field($model, 'blockhash')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blockindex')->textInput() ?>

    <?= $form->field($model, 'blocktime')->textInput() ?>

    <?= $form->field($model, 'txid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'time')->textInput() ?>

    <?= $form->field($model, 'timereceived')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
