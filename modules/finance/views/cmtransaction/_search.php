<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\models\CmTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cm-transaction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'account') ?>

    <?= $form->field($model, 'address') ?>

    <?= $form->field($model, 'category') ?>

    <?= $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'label') ?>

    <?php // echo $form->field($model, 'vout') ?>

    <?php // echo $form->field($model, 'confirmations') ?>

    <?php // echo $form->field($model, 'blockhash') ?>

    <?php // echo $form->field($model, 'blockindex') ?>

    <?php // echo $form->field($model, 'blocktime') ?>

    <?php // echo $form->field($model, 'txid') ?>

    <?php // echo $form->field($model, 'time') ?>

    <?php // echo $form->field($model, 'timereceived') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
