<?php

use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\models\UserPayWalletBitcoinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Кошельки Биткоин участников');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-pay-wallet-bitcoin-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск'); ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
              <h1><?=Html::encode($this->title)?></h1>
            </div>
            <div class="box-body no-padding">
            <?php Pjax::begin();?>


                <p>
                    <?=Html::a(Yii::t('app', 'Создать кошелек Биткоин'), ['create'], ['class' => 'btn btn-success'])?>
                </p>

                <?=GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],

		'id',
		[
			'attribute' => 'user_id',
			'format' => 'raw',
			'filter' => Select2::widget([
				'name' => (new ReflectionClass($searchModel))->getShortName() . '[user_id]',
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Введите логин'),
				],
				'pluginOptions' => [
					'minimumInputLength' => 2,
					'ajax' => [
						'url' => Url::to(['/profile/backuser/searhbylogin']),
						'dataType' => 'json',
						'data' => new JsExpression('function(params) { return {q:params.term}; }'),
					],
					'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					'templateResult' => new JsExpression('function(user) { return user.login; }'),
					'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
				],
			]),
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
		], [
			'attribute' => 'currency_id',
			'format' => 'raw',
			'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_currency_id', ['model' => $model]);},
		],
		'wallet',

		['class' => 'yii\grid\ActionColumn'],
	],
]);?>
            <?php Pjax::end();?>
            </div>
          </div>
        </div>
    </div>
</div>
