<?php

namespace app\modules\finance;

use yii\base\BootstrapInterface;
/**
 * finance module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\finance\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'finance/backpay/<action>', 
                'route' => 'finance/backpay/<action>'
            ]
        ], false);
    }
}
