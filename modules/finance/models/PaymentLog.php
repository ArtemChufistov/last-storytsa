<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "payment_log".
 *
 * @property integer $id
 * @property integer $payment_id
 * @property string $status
 * @property string $date_add
 */
class PaymentLog extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'payment_log';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['payment_id'], 'integer'],
			[['status'], 'required'],
			[['date_add', 'additional_info', 'error'], 'safe'],
			[['status'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'payment_id' => Yii::t('app', 'Номер платежа'),
			'status' => Yii::t('app', 'Статус'),
			'date_add' => Yii::t('app', 'Дата добавления'),
			'additional_info' => Yii::t('app', 'Доп инфо'),
			'error' => Yii::t('app', 'Ошибки'),
		];
	}
}
