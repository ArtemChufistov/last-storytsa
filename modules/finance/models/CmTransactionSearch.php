<?php

namespace app\modules\finance\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\models\CmTransaction;

/**
 * CmTransactionSearch represents the model behind the search form of `app\modules\finance\models\CmTransaction`.
 */
class CmTransactionSearch extends CmTransaction
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'account', 'vout', 'confirmations', 'blockindex'], 'integer'],
            [['address', 'category', 'label', 'blockhash', 'blocktime', 'txid', 'time', 'timereceived'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'account' => $this->account,
            'amount' => $this->amount,
            'vout' => $this->vout,
            'confirmations' => $this->confirmations,
            'blockindex' => $this->blockindex,
            'blocktime' => $this->blocktime,
            'time' => $this->time,
            'timereceived' => $this->timereceived,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'blockhash', $this->blockhash])
            ->andFilterWhere(['like', 'txid', $this->txid]);

        return $dataProvider;
    }
}
