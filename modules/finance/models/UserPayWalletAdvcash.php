<?php

namespace app\modules\finance\models;

use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\UserPayWallet;
use app\modules\profile\models\User;
use app\modules\finance\components\paysystem\AdvcashComponent;

use Yii;

/**
 * This is the model class for table "{{%user_pay_wallet_advcash}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $pay_system_id
 * @property int $currency_id
 * @property string $name
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $description
 */
class UserPayWalletAdvcash extends UserPayWallet
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_pay_wallet_advcash}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'pay_system_id', 'currency_id', 'address', 'pay_password'], 'required'],
            ['pay_password', 'validatePayPassword'],
            [['user_id', 'pay_system_id', 'currency_id'], 'integer'],
            [['description'], 'string'],
            ['address', 'addressValidate', 'skipOnEmpty' => false],
            ['address', 'firstUValidate', 'skipOnEmpty' => false],
            [['name', 'address', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function getInfoFields() {
        return [
            'address',
            'pay_password',
        ];
    }

    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getPaySystem() {
        return $this->hasOne(PaySystem::className(), ['id' => 'pay_system_id']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function payAttributes() {
        return [
            'wallet' => $this->address,
        ];
    }

    public function addressValidate($attribute, $params)
    {
        $error = AdvcashComponent::validateWallet($this->$attribute);

        /*
        if (!empty($error)){
            $this->addError('address', Yii::t('app', 'wallet does not exist or incorrect'));
        }
        */
    }

    public function firstUValidate($attribute, $params)
    {
        if ($this->getCurrency()->one()->key == Currency::KEY_USD && mb_substr($this->$attribute, 0, 1) != 'U'){
            if (mb_substr($this->$attribute, 0, 1) != 'u'){
                $this->addError('address', Yii::t('app', 'Неправильный кошелёк'));
            }
        }elseif ($this->getCurrency()->one()->key == Currency::KEY_RUR && mb_substr($this->$attribute, 0, 1) != 'R'){
            if (mb_substr($this->$attribute, 0, 1) != 'r'){
                $this->addError('address', Yii::t('app', 'Неправильный кошелёк'));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'pay_system_id' => Yii::t('app', 'Pay System ID'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'name' => Yii::t('app', 'Name'),
            'address' => Yii::t('app', 'Кошелёк'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
