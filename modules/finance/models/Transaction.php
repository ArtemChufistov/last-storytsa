<?php

namespace app\modules\finance\models;

use app\modules\finance\models\Currency;
use app\modules\finance\models\TransactionType;
use app\modules\matrix\models\MatrixQueue;
use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $type
 * @property double $sum
 * @property integer $currency_id
 * @property integer $payment_id
 * @property string $data
 * @property string $hash
 * @property string $date_add
 */
class Transaction extends \yii\db\ActiveRecord {
	const TYPE_PLUS_INVEST_DOHOD = 'plus_invest_dohod';
	const TYPE_MINUS_INVEST_DOHOD = 'minus_invest_dohod';
	const TYPE_PARTNER_BUY_PLACE = 'partner_buy_place';
	const TYPE_PARTNER_BUY_AKCII = 'buy_akcii';
	const TYPE_MINUS_REF_BONUS = 'minus_ref_bonus';
	const TYPE_PLUS_REF_BONUS = 'plus_ref_bonus';
	const TYPE_PARTNER_BUY_PLACE_TO_DEPOSIT = 'partner_buy_place_to_deposit';
	const TYPE_REF_BONUS = 'ref_bonus';
	const TYPE_INVEST_PERCENT = 'invest_percent';
	const TYPE_PARTNER_PARENT_BUY_PLACE = 'partner_parent_buy_place';
	const TYPE_BUY_MATRIX_PLACE = 'buy_matrix_place';
	const TYPE_BUY_INVEST = 'buy_invest';
	const TYPE_REINVEST_MATRIX_PLACE = 'reinvest_matrix_place';
	const TYPE_WITHDRAW_MATRIX_PLACE = 'withdraw_matrix_place';
	const TYPE_PERC_OUT_OFFICE = 'perc_out_office';
	const TYPE_CHARGE_OFFICE = 'charge_office';
	const TYPE_OUT_OFFICE = 'out_office';
	const TYPE_INNER_PAY = 'inner_pay';
	const TYPE_FOR_VIDEO = 'for_video';
	const TYPE_TO_BALANCE_BY_CLOSE= 'to_balance_by_close';
	const TYPE_RETURN_FROM_INVEST = 'return_from_invest';
	const TYPE_CREATE_DEPOSIT_INVEST = 'create_deposit_invest';
	const TYPE_ADMIN_BONUS = 'admin_bonus';
	const TYPE_BONUS = 'bonus';
	const TYPE_CHARGE_TO_DEPOSIT = 'charge_to_deposit';
	const TYPE_PROMO1 = 'promo1';
	const TYPE_MATHCING_BONUS = 'matching_bonus';
	const TYPE_PARTNER_INVEST_DIV_1 = 'partner_invest_div_1';
	const TYPE_PARTNER_INVEST_DIV_2 = 'partner_invest_div_2';
	const TYPE_PARTNER_INVEST_DIV_3 = 'partner_invest_div_3';
	const TYPE_PARTNER_INVEST_DIV_4 = 'partner_invest_div_4';
	const TYPE_PARTNER_INVEST_DIV_5 = 'partner_invest_div_5';
	const TYPE_PARTNER_INVEST_DIV_6 = 'partner_invest_div_6';
	const TYPE_PARTNER_INVEST_DIV_7 = 'partner_invest_div_7';
	const TYPE_PARTNER_INVEST_DIV_8 = 'partner_invest_div_8';
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'transaction';
	}

	public static function getOfficeTypeArray() {

		$transactionTypeArray = TransactionType::find()->where(['active' => TransactionType::ACTIVE_TRUE])->all();

		$types = self::getTypeArray();

		$resultTypes = [];
		foreach ($transactionTypeArray as $transactionType) {
			if (in_array($transactionType->key, array_keys($types))) {
				$resultTypes[$transactionType->key] = $types[$transactionType->key];
			}
		}

		return $resultTypes;
	}

	public static function getTypeArray() {
		return [
			self::TYPE_MINUS_INVEST_DOHOD=> Yii::t('app', 'Списание с инвест. дохода'),
			self::TYPE_PLUS_INVEST_DOHOD=> Yii::t('app', 'Начисление инвест. дохода на баланс'),
			self::TYPE_MINUS_REF_BONUS=> Yii::t('app', 'Списание реф бонуса'),
			self::TYPE_PLUS_REF_BONUS => Yii::t('app', 'Начисление реф бонуса на баланс'),
			self::TYPE_PARTNER_BUY_PLACE => Yii::t('app', 'Начисление на баланс'),
			self::TYPE_PARTNER_BUY_AKCII => Yii::t('app', 'Покупка акций'),
			self::TYPE_PARTNER_BUY_PLACE_TO_DEPOSIT => Yii::t('app', 'Маркетинговый доход'),
			self::TYPE_REF_BONUS => Yii::t('app', 'Реферальный бонус'),
			self::TYPE_RETURN_FROM_INVEST => Yii::t('app', 'Возврат инвестиции'),
			self::TYPE_INVEST_PERCENT => Yii::t('app', 'Процент со вклада'),
			self::TYPE_TO_BALANCE_BY_CLOSE => Yii::t('app', 'Начисление на баланс за закрытие места'),
			self::TYPE_PARTNER_PARENT_BUY_PLACE => Yii::t('app', 'Партнёрское вознаграждение'),
			self::TYPE_BUY_MATRIX_PLACE => Yii::t('app', 'Оплата участия'),
			self::TYPE_BUY_INVEST => Yii::t('app', 'Покупка инвестиционного пакета'),
			self::TYPE_REINVEST_MATRIX_PLACE => Yii::t('app', 'Реинвест в программу'),
			self::TYPE_WITHDRAW_MATRIX_PLACE => Yii::t('app', 'Списание с баланса'),
			self::TYPE_PERC_OUT_OFFICE => Yii::t('app', 'Процент с ввода/вывода'),
			self::TYPE_CHARGE_OFFICE => Yii::t('app', 'Пополнение ЛС'),
			self::TYPE_OUT_OFFICE => Yii::t('app', 'Снятие с ЛС'),
			self::TYPE_ADMIN_BONUS => Yii::t('app', 'Админские'),
			self::TYPE_BONUS => Yii::t('app', 'Бонус'),
			self::TYPE_INNER_PAY => Yii::t('app', 'Внутренний перевод'),
			self::TYPE_FOR_VIDEO => Yii::t('app', 'Просмотр видео'),
			self::TYPE_CREATE_DEPOSIT_INVEST => Yii::t('app', 'Создание депозитной инвестиции'),
			self::TYPE_CHARGE_TO_DEPOSIT => Yii::t('app', 'Начисление на депозит'),
			self::TYPE_PROMO1 => Yii::t('app', 'Промо бонус'),
			self::TYPE_MATHCING_BONUS => Yii::t('app', 'Матчинг бонус'),
			self::TYPE_PARTNER_INVEST_DIV_1 => Yii::t('app', 'Партнёрское вознаграждение Ур.1'),
			self::TYPE_PARTNER_INVEST_DIV_2 => Yii::t('app', 'Партнёрское вознаграждение Ур.2'),
			self::TYPE_PARTNER_INVEST_DIV_3 => Yii::t('app', 'Партнёрское вознаграждение Ур.3'),
			self::TYPE_PARTNER_INVEST_DIV_4 => Yii::t('app', 'Партнёрское вознаграждение Ур.4'),
			self::TYPE_PARTNER_INVEST_DIV_5 => Yii::t('app', 'Партнёрское вознаграждение Ур.5'),
			self::TYPE_PARTNER_INVEST_DIV_6 => Yii::t('app', 'Партнёрское вознаграждение Ур.6'),
			self::TYPE_PARTNER_INVEST_DIV_7 => Yii::t('app', 'Партнёрское вознаграждение Ур.7'),
			self::TYPE_PARTNER_INVEST_DIV_8 => Yii::t('app', 'Партнёрское вознаграждение Ур.8'),
		];
	}

	public function getTypeTitle() {
		$array = self::getTypeArray();

		return $array[$this->type];
	}

	public function getMatrixQueue() {
		return $this->hasOne(MatrixQueue::className(), ['id' => 'matrix_queue_id']);
	}

	// сделать внутренний перевод
	public function makeInnerPay($from_user_id, $to_user_id, $sum, $currency_id) {

		$transaction = new Transaction;
		$transaction->from_user_id = $from_user_id;
		$transaction->data = $to_user_id;
		$transaction->type = self::TYPE_INNER_PAY;
		$transaction->sum = $sum;
		$transaction->currency_id = $currency_id;
		$transaction->save();

		$fromUser = User::find()->where(['id' => $from_user_id])->one();

        $frombalance = $fromUser->getBalances([$transaction->currency_id])[$transaction->currency_id];
        $frombalance->recalculateValue();

		$transaction = new Transaction;
		$transaction->to_user_id = $to_user_id;
		$transaction->data = $from_user_id;
		$transaction->type = self::TYPE_INNER_PAY;
		$transaction->sum = $sum;
		$transaction->currency_id = $currency_id;
		$transaction->save();

		$toUser = User::find()->where(['id' => $to_user_id])->one();

        $toUser = $toUser->getBalances([$transaction->currency_id])[$transaction->currency_id];
        $toUser->recalculateValue();
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['from_user_id', 'to_user_id', 'currency_id', 'payment_id'], 'integer'],
			[['type'], 'required'],
			[['sum'], 'number'],
			[['date_add', 'matrix_queue_id', 'matrix_id', 'hash', 'invest_type_id', 'course', 'data'], 'safe'],
			[['type', 'hash'], 'string', 'max' => 255],
		];
	}

	public function beforeSave($insert) {
		if ($this->isNewRecord && empty($this->date_add)) {
			$this->date_add = date('Y-m-d H:i:s');
		}

		if (parent::beforeSave($insert)) {
			return true;
		} else {
			return false;
		}
	}

	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
	}

	public function getToUser() {
		return $this->hasOne(User::className(), ['id' => 'to_user_id']);
	}

	public function getFromUser() {
		return $this->hasOne(User::className(), ['id' => 'from_user_id']);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'from_user_id' => Yii::t('app', 'С пользователя'),
			'to_user_id' => Yii::t('app', 'Пользователю'),
			'type' => Yii::t('app', 'Тип'),
			'sum' => Yii::t('app', 'Сумма'),
			'currency_id' => Yii::t('app', 'Валюта'),
			'payment_id' => Yii::t('app', 'Платёж'),
			'data' => Yii::t('app', 'Инфо'),
			'hash' => Yii::t('app', 'Хэш'),
			'course' => Yii::t('app', 'Курс'),
			'date_add' => Yii::t('app', 'Дата добавления'),
			'matrix_queue_id' => Yii::t('app', 'Ид очереди в матрицу'),
			'matrix_id' => Yii::t('app', 'Ид элемента матрицы'),
			'invest_type_id' => Yii::t('app', 'Ид инвест. пакета'),
		];
	}
}
