<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "transaction_type".
 *
 * @property int $id
 * @property string $key
 * @property int $active
 */
class TransactionType extends \yii\db\ActiveRecord {
	const ACTIVE_TRUE = 1;
	const ACTIVE_FALSE = 0;
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'transaction_type';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['active'], 'integer'],
			[['key'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'key' => Yii::t('app', 'Key'),
			'active' => Yii::t('app', 'Active'),
		];
	}
}
