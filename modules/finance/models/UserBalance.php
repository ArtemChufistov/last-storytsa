<?php

namespace app\modules\finance\models;

use app\modules\profile\models\User;
use app\modules\finance\models\Currency;
use app\modules\finance\models\Transaction;
use Yii;

/**
 * This is the model class for table "user_balance".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $currency_id
 * @property double $value
 */
class UserBalance extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'user_balance';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id', 'currency_id'], 'integer'],
			[['value'], 'number'],
		];
	}

	// пересчет баланса пользователя для валюты
	public function recalculateValue() {
		// сумма всех исходящих транзакций
		$minusSum = Transaction::find()->select('SUM(sum) as sum')->where(['from_user_id' => $this->user_id, 'currency_id' => $this->currency_id])->one();

		// сумма всех входящих транзакций
		$plusSum = Transaction::find()->select('SUM(sum) as sum')->where(['to_user_id' => $this->user_id, 'currency_id' => $this->currency_id])->one();

		// сумма всех заявок на вывод
		$outOfficePayment = Payment::find()->select('SUM(realSum) as realSum')
			->where(['from_user_id' => $this->user_id, 'from_currency_id' => $this->currency_id])
			->andWhere(['status' => [Payment::STATUS_WAIT, Payment::STATUS_WAIT_SYSTEM, Payment::PROCESS_SYSTEM]])->one();

		$this->value = ($plusSum->sum - $minusSum->sum) - $outOfficePayment->realSum;
		$this->block_value = $outOfficePayment->realSum;
		$this->save();
	}

	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
	}

	public function showValue() {
		$currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
		if (!empty($currency) && $this->currency_id == $currency->id) {
			return number_format($this->value, 6);
		} else {
			return number_format($this->value, 2);
		}
	}

	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'currency_id' => Yii::t('app', 'Currency ID'),
			'value' => Yii::t('app', 'Value'),
			'voucher_value' => Yii::t('app', 'Voucher value'),
			'block_value' => Yii::t('app', 'Block value'),
		];
	}
}
