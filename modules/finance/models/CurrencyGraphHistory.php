<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "{{%currency_graph_history}}".
 *
 * @property int $id
 * @property int $currency_id
 * @property double $value
 * @property string $date
 */
class CurrencyGraphHistory extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%currency_graph_history}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['from_currency_id', 'to_currency_id'], 'integer'],
			[['course', 'course_to_usd'], 'number'],
			[['date'], 'safe'],
		];
	}

	public function getFromCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'from_currency_id']);
	}

	public function getToCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'to_currency_id']);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'from_currency_id' => Yii::t('app', 'Начальная валюта'),
			'to_currency_id' => Yii::t('app', 'Конечная валюта'),
			'course' => Yii::t('app', 'Курс'),
			'course_to_usd' => Yii::t('app', 'Курс к доллару'),
			'date' => Yii::t('app', 'Date'),
		];
	}
}
