<?php

namespace app\modules\finance\models;

use app\modules\finance\models\PaySystem;
use Yii;

/**
 * This is the model class for table "curency".
 *
 * @property integer $id
 * @property string $key
 * @property string $title
 */
class Currency extends \yii\db\ActiveRecord {
	const KEY_BTC = 'BTC';
	const KEY_USD = 'USD';
	const KEY_EUR = 'EUR';
	const KEY_UAH = 'UAH';
	const KEY_RUR = 'RUB';
	const KEY_LTC = 'LTC';
	const KEY_ETH = 'ETH';
	const KEY_DOGE = 'DOGE';
	const KEY_RIPPLE = 'XRP';
	const KEY_VOUCHER = 'VOU';
	const KEY_DEPOZIT = 'DEPOZIT';
	const KEY_FREEZ = 'FREEZ';
	const KEY_FREEZ24 = 'FREEZ24';
	const KEY_ETC = 'ETC';
	const KEY_SC = 'SC';
	const KEY_GNT = 'GNT';
	const KEY_BCN = 'BCN';
	const KEY_ZEC = 'ZEC';
	const KEY_GNO = 'GNO';
	const KEY_XRP = 'XRP';
	const KEY_BTS = 'BTS';
	const KEY_DGB = 'DGB';
	const KEY_XMR = 'XMR';
	const KEY_DASH = 'DASH';
	const KEY_XEM = 'XEM';
	const KEY_STR = 'STR';
	const KEY_USDT = 'USDT';
	const KEY_DIO = 'DechIO';
	const KEY_FREE_POINT = 'FREE_POINT';
	const KEY_BTC_ABC = 'BTCABC';
	const KEY_BTC_SV = 'BTCSV';

	const AUTO_COURSE_UPDATE_TRUE = 1;
	const AUTO_COURSE_UPDATE_FALSE = 0;

	const GROUP_SPECIAL_1 = 1;
	const GROUP_SPECIAL_2 = 2;
	const GROUP_SPECIAL_3 = 3;
	const GROUP_BUILD_GRAPH = 4;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'currency';
	}

	public function getKeyArray() {
		return [
			self::KEY_BTC => Yii::t('app', 'Биткоин'),
			self::KEY_USD => Yii::t('app', 'Доллар'),
			self::KEY_EUR => Yii::t('app', 'Евро'),
			self::KEY_UAH => Yii::t('app', 'Гривна'),
			self::KEY_RUR => Yii::t('app', 'Рубль'),
			self::KEY_LTC => Yii::t('app', 'Лайткоин'),
			self::KEY_ETH => Yii::t('app', 'Эфириум'),
			self::KEY_DOGE => Yii::t('app', 'Доги'),
			self::KEY_RIPPLE => Yii::t('app', 'Рипл'),
			self::KEY_VOUCHER => Yii::t('app', 'Ваучер'),
			self::KEY_DEPOZIT => Yii::t('app', 'Депозит'),
			self::KEY_FREEZ => Yii::t('app', 'Замороженый'),
			self::KEY_FREEZ24 => Yii::t('app', 'Замороженый на 24 часа'),
			self::KEY_ETC => Yii::t('app', 'Ethereum Classic'),
			self::KEY_SC => Yii::t('app', 'SiaCoin'),
			self::KEY_GNT => Yii::t('app', 'Golem'),
			self::KEY_BCN => Yii::t('app', 'Bytecoin'),
			self::KEY_ZEC => Yii::t('app', 'Zcash'),
			self::KEY_GNO => Yii::t('app', 'Gnosis'),
			self::KEY_XRP => Yii::t('app', 'Ripple'),
			self::KEY_BTS => Yii::t('app', 'BitShares'),
			self::KEY_DGB => Yii::t('app', 'DigiByte'),
			self::KEY_XMR => Yii::t('app', 'Monero'),
			self::KEY_DASH => Yii::t('app', 'DASH'),
			self::KEY_XEM => Yii::t('app', 'NEM'),
			self::KEY_STR => Yii::t('app', 'Stellar Lumen'),
			self::KEY_USDT => Yii::t('app', 'USDT'),
			self::KEY_DIO => Yii::t('app', 'Dechmont IO'),
			self::KEY_FREE_POINT => Yii::t('app', 'Free Point'),
		];
	}

	public function getGroupArray() {
		return [
			self::GROUP_SPECIAL_1 => Yii::t('app', 'Спец. группа 1'),
			self::GROUP_SPECIAL_2 => Yii::t('app', 'Спец. группа 2'),
			self::GROUP_SPECIAL_3 => Yii::t('app', 'Спец. группа 3'),
			self::GROUP_BUILD_GRAPH => Yii::t('app', 'Группа для построения графика'),

		];
	}

	public function getPaySystems() {
		return $this->hasMany(PaySystem::className(), ['id' => 'pay_system_id'])
			->viaTable(PaySystemCurrency::tableName(), ['currency_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['key', 'title'], 'required'],
			[['course_to_usd', 'auto_course_update', 'func_course_update', 'group'], 'safe'],
			[['key', 'title'], 'string', 'max' => 255],
		];
	}

	public static function allWithoutVoucher() {
		$currencyArray = self::find()->where(['!=', 'key', self::KEY_VOUCHER])->all();

		return $currencyArray;
	}

	public function getCryptoWallets() {
		return $this->hasMany(CryptoWallet::className(), ['currency_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'key' => Yii::t('app', 'Ключ'),
			'title' => Yii::t('app', 'Название'),
			'group' => Yii::t('app', 'Группа валюты'),
			'course_to_usd' => Yii::t('app', 'Курс к Доллару'),
			'auto_course_update' => Yii::t('app', 'Автообновление курса'),
			'func_course_update' => Yii::t('app', 'Функция обновления'),
		];
	}
}
