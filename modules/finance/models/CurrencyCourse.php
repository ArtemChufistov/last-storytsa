<?php

namespace app\modules\finance\models;

use app\modules\finance\models\PaySystem;
use Yii;

/**
 * This is the model class for table "{{%currency_course}}".
 *
 * @property int $id
 * @property int $from_pay_system_id
 * @property int $to_pay_system_id
 * @property int $from_currency
 * @property int $to_currency
 * @property string $type
 * @property string $course
 * @property string $course_function
 */
class CurrencyCourse extends \yii\db\ActiveRecord {
	const TYPE_AUTO = 'auto';
	const TYPE_MANUAL = 'manual';

	const ACTIVE_TRUE = 1;
	const ACTIVE_FALSE = 0;

	const COMISSION_TYPE_FIX = 'fix';
	const COMISSION_TYPE_PERCENT = 'percent';

	const TRANSFER_TYPE_AUTO = 'transfer_auto';
	const TRANSFER_TYPE_MANUAL = 'transfer_manual';

	const ONE_ACTIVE_TRUE = 1;
	const ONE_ACTIVE_FALSE = 0;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%currency_course}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['from_pay_system_id', 'to_pay_system_id', 'from_currency', 'to_currency', 'active'], 'integer'],
			[['comission', 'comission_type', 'min_sum', 'one_active'], 'safe'],
			[['type', 'course', 'course_function'], 'string', 'max' => 255],
		];
	}

	public static function getTypeArray() {
		return [
			self::TYPE_AUTO => Yii::t('app', 'Авто'),
			self::TYPE_MANUAL => Yii::t('app', 'Вручную'),
		];
	}

	public function getTypetitle() {
		$typeArray = self::getTypeArray();

		return $typeArray[$this->type];
	}

	public static function getActiveArray() {
		return [
			self::ACTIVE_TRUE => Yii::t('app', 'Активен'),
			self::ACTIVE_FALSE => Yii::t('app', 'Не активен'),
		];
	}

	public function getActivetitle() {
		$typeArray = self::getActiveArray();

		return $typeArray[$this->active];
	}

	public static function getComissionTypeArray() {
		return [
			self::COMISSION_TYPE_FIX => Yii::t('app', 'Фиксированная комиссия'),
			self::COMISSION_TYPE_PERCENT => Yii::t('app', 'Процент от снимаемой суммы'),
		];
	}

	public static function getTransferTypeArray() {
		return [
			self::TRANSFER_TYPE_AUTO => Yii::t('app', 'Авто отправка'),
			self::TRANSFER_TYPE_MANUAL => Yii::t('app', 'Ручная отправка'),
		];
	}

	public function getComissionTypetitle() {
		$typeArray = self::getActiveArray();

		return $typeArray[$this->active];
	}

	public function getFromCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'from_currency']);
	}

	public function getToCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'to_currency']);
	}

	public function getToPaySystem() {
		return $this->hasOne(PaySystem::className(), ['id' => 'to_pay_system_id']);
	}

	public function getFromPaySystem() {
		return $this->hasOne(PaySystem::className(), ['id' => 'from_pay_system_id']);
	}

	// взять активное направление
	public static function getActivePayDirection($payment, $distinctCondition = '') {
		$currCourse = CurrencyCourse::find()
			->where(['active' => CurrencyCourse::ACTIVE_TRUE]);

		if (!empty($distinctCondition)) {
			$currCourse->select($distinctCondition)->distinct($distinctCondition);
		}
		if (!empty($payment->to_pay_system_id)) {
			$currCourse->andWhere(['to_pay_system_id' => $payment->to_pay_system_id]);
		}
		if (!empty($payment->from_pay_system_id)) {
			$currCourse->andWhere(['from_pay_system_id' => $payment->from_pay_system_id]);
		}
		if (!empty($payment->to_currency_id)) {
			$currCourse->andWhere(['to_currency' => $payment->to_currency_id]);
		}
		if (!empty($payment->from_currency_id)) {
			$currCourse->andWhere(['from_currency' => $payment->from_currency_id]);
		}
		return $currCourse->all();
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'from_pay_system_id' => Yii::t('app', 'От платёжной системы'),
			'to_pay_system_id' => Yii::t('app', 'На платёжную систему'),
			'from_currency' => Yii::t('app', 'Начальная валюта'),
			'to_currency' => Yii::t('app', 'Конечная валюта'),
			'type' => Yii::t('app', 'Тип обновления курса'),
			'course' => Yii::t('app', 'Курс'),
			'course_function' => Yii::t('app', 'Функция обновления курса'),
			'comission' => Yii::t('app', 'Комиссия'),
			'comission_type' => Yii::t('app', 'Тип комиссии'),
			'active' => Yii::t('app', 'Активен'),
			'transfer_type' => Yii::t('app', 'Тип отправки'),
			'min_sum' => Yii::t('app', 'Минимальная сумма'),
			'one_active' => Yii::t('app', 'Только один платеж активен'),
		];
	}
}
