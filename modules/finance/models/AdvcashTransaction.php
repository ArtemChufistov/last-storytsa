<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "advcash_transaction".
 *
 * @property int $id
 * @property string $ac_transfer
 * @property string $ac_start_date
 * @property string $ac_sci_name
 * @property string $ac_src_wallet
 * @property string $ac_dest_wallet
 * @property string $ac_order_id
 * @property double $ac_amount
 * @property string $ac_merchant_currency
 */
class AdvcashTransaction extends \yii\db\ActiveRecord{
    const TREATED_TRUE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advcash_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ac_start_date', 'treated', 'amount', 'date_tread'], 'safe'],
            [['ac_amount'], 'number'],
            [['ac_transfer', 'ac_sci_name', 'ac_src_wallet', 'ac_dest_wallet', 'ac_order_id', 'ac_merchant_currency'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ac_transfer' => Yii::t('app', 'Ac Transfer'),
            'ac_start_date' => Yii::t('app', 'Ac Start Date'),
            'ac_sci_name' => Yii::t('app', 'Ac Sci Name'),
            'ac_src_wallet' => Yii::t('app', 'Ac Src Wallet'),
            'ac_dest_wallet' => Yii::t('app', 'Ac Dest Wallet'),
            'ac_order_id' => Yii::t('app', 'Ac Order ID'),
            'ac_amount' => Yii::t('app', 'Ac Amount'),
            'amount' => Yii::t('app', 'Amount'),
            'ac_merchant_currency' => Yii::t('app', 'Ac Merchant Currency'),
            'treated' => Yii::t('app', 'treated'),
            'date_tread' => Yii::t('app', 'date_tread'),
        ];
    }
}
