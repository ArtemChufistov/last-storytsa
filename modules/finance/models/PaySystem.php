<?php

namespace app\modules\finance\models;

use app\modules\finance\models\PaySystemCurrency;
use Yii;

/**
 * This is the model class for table "pay_system".
 *
 * @property integer $id
 * @property string $key
 * @property string $name
 */
class PaySystem extends \yii\db\ActiveRecord {
	const KEY_BITCOIN      = 'Bitcoin';
	const KEY_COINBASE     = 'Coinbase';
	const KEY_PERFECTMONEY = 'PerfectMoney';
	const KEY_TOP_EXCHANGE = 'Topexchange';
	const KEY_CRYPCHANT    = 'Crypchant';
	const KEY_ADVCASH      = 'Advcash';
	const KEY_Payeer       = 'Payeer';
	const KEY_CASH         = 'Cash';
	const KEY_LK           = 'lk';
	const KEY_CM           = 'Cm';

	public $currencyArray;
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'pay_system';
	}

	public static function getPaySystemKeys() {
		return [
			self::KEY_CM => Yii::t('app', 'Cm'),
			self::KEY_BITCOIN => Yii::t('app', 'Bitcoin'),
			self::KEY_COINBASE => Yii::t('app', 'Coinbase'),
			self::KEY_CRYPCHANT => Yii::t('app', 'Crypchant'),
			self::KEY_PERFECTMONEY => Yii::t('app', 'Perfect Money'),
			self::KEY_ADVCASH => Yii::t('app', 'Advanced Cash'),
			self::KEY_Payeer => Yii::t('app', 'Payeer'),
			self::KEY_TOP_EXCHANGE => Yii::t('app', 'Top-exchange'),
			self::KEY_CASH => Yii::t('app', 'Наличные'),
			self::KEY_LK => Yii::t('app', 'Личный кабинет'),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['key', 'name'], 'required'],
			[['currencyArray'], 'safe'],
			[['key', 'name'], 'string', 'max' => 255],
		];
	}

	public function getCurrencies() {
		return $this->hasMany(Currency::className(), ['id' => 'currency_id'])
			->viaTable(PaySystemCurrency::tableName(), ['pay_system_id' => 'id']);
	}

	public function afterSave($insert, $changedAttributes) {
		if (!empty($this->currencyArray)) {

			foreach ($this->currencyArray as $currencyId) {
				$paySystemCurrency = new PaySystemCurrency;
				$paySystemCurrency->currency_id = $currencyId;
				$paySystemCurrency->pay_system_id = $this->id;

				$paySystemCurrency->save();
			}
		}

		parent::afterSave($insert, $changedAttributes);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'key' => Yii::t('app', 'Ключ'),
			'name' => Yii::t('app', 'Название'),
			'currencyArray' => Yii::t('app', 'Валюты'),
		];
	}
}
