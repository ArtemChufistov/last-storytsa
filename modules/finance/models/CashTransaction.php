<?php

namespace app\modules\finance\models;

use app\modules\finance\models\Payment;
use Yii;

/**
 * This is the model class for table "cash_transaction".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $skype
 * @property double $sum
 * @property string $status
 * @property int $payment_id
 * @property string $date_add
 */
class CashTransaction extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_PENDING = 'pending';
    const STATUS_OK = 'ok';
    const STATUS_PAY = 'pay';
    /**
     * @inheritdoc
     */

    public function getStatusArray() {
        return [
            self::STATUS_NEW => Yii::t('app', 'Новый'),
            self::STATUS_PENDING => Yii::t('app', 'Подтверждён'),
            self::STATUS_OK => Yii::t('app', 'Проверен'),
            self::STATUS_PAY => Yii::t('app', 'Оплачен'),
        ];
    }

    public function getUserStatusArray() {
        return [
            self::STATUS_NEW => Yii::t('app', 'Новый'),
            self::STATUS_PENDING => Yii::t('app', 'Подтверждён'),
            self::STATUS_OK => Yii::t('app', 'Проверен'),
        ];
    }

    public function getStatusTitle(){
        $data = self::getStatusArray();

        return $data[$this->status];
    }

    public static function tableName()
    {
        return 'cash_transaction';
    }

    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount'], 'number'],
            [['phone', 'name'], 'required'],
            [['payment_id'], 'integer'],
            [['date_add', 'description'], 'safe'],
            [['name', 'address', 'email', 'phone', 'skype', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'ФИО'),
            'address' => Yii::t('app', 'Адрес'),
            'email' => Yii::t('app', 'E-mail'),
            'phone' => Yii::t('app', 'Телефон'),
            'skype' => Yii::t('app', 'Skype'),
            'amount' => Yii::t('app', 'Сумма'),
            'status' => Yii::t('app', 'Статус'),
            'payment_id' => Yii::t('app', 'Номер платежа'),
            'date_add' => Yii::t('app', 'Дата добавления'),
            'description' => Yii::t('app', 'Заметки'),
        ];
    }
}
