<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "pay_system_currency".
 *
 * @property integer $id
 * @property integer $pay_system_id
 * @property integer $currency_id
 */
class PaySystemCurrency extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'pay_system_currency';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['pay_system_id', 'currency_id'], 'required'],
			[['pay_system_id', 'currency_id'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'pay_system_id' => Yii::t('app', 'Pay System ID'),
			'currency_id' => Yii::t('app', 'Currency ID'),
		];
	}
}
