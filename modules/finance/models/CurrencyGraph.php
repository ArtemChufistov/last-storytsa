<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "{{%currency_graph}}".
 *
 * @property int $id
 * @property double $value
 * @property int $currency_id
 * @property string $date
 * @property string $type
 */
class CurrencyGraph extends \yii\db\ActiveRecord {
	const TYPE_5_MINUTE = '5min';
	const TYPE_15_MINUTE = '15min';
	const TYPE_30_MINUTE = '30min';
	const TYPE_2_HOURE = '2hour';
	const TYPE_4_HOURE = '4hour';
	const TYPE_1_DAY = '1day';
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%currency_graph}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['value'], 'number'],
			[['currency_id'], 'integer'],
			[['date'], 'safe'],
			[['type'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'value' => Yii::t('app', 'Value'),
			'currency_id' => Yii::t('app', 'Currency ID'),
			'date' => Yii::t('app', 'Date'),
			'type' => Yii::t('app', 'Type'),
		];
	}

	public function getTypeArray() {
		return [
			self::TYPE_5_MINUTE => Yii::t('app', '5 мин'),
			self::TYPE_15_MINUTE => Yii::t('app', '15 мин'),
			self::TYPE_30_MINUTE => Yii::t('app', '30 мин'),
			self::TYPE_2_HOURE => Yii::t('app', '2 часа'),
			self::TYPE_4_HOURE => Yii::t('app', '4 часа'),
			self::TYPE_1_DAY => Yii::t('app', '1 день'),
		];
	}

	public static function getTypeStepsInSecondsArray() {
		return [
			self::TYPE_5_MINUTE => 300,
			self::TYPE_15_MINUTE => 900,
			self::TYPE_30_MINUTE => 1800,
			self::TYPE_2_HOURE => 7200,
			self::TYPE_4_HOURE => 14400,
			self::TYPE_1_DAY => 86400,
		];
	}

	public static function getTypeInSecond($type) {
		$data = self::getTypeStepsInSecondsArray();

		return $data[$type];
	}

}