<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "{{%crypto_wallet}}".
 *
 * @property int $id
 * @property string $wallet
 * @property double $balance
 * @property double $course_to_usd
 * @property string $info
 * @property string $function
 */
class CryptoWallet extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%crypto_wallet}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['balance', 'currency_id'], 'number'],
			[['wallet', 'info'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'wallet' => Yii::t('app', 'Номер кошелька'),
			'balance' => Yii::t('app', 'Баланс'),
			'currency_id' => Yii::t('app', 'Валюта'),
			'info' => Yii::t('app', 'Доп. инфо'),
		];
	}

	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
	}
}
