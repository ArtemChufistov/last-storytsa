<?php

namespace app\modules\finance\models;

use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\UserPayWallet;
use app\modules\profile\models\User;
use linslin\yii2\curl;
use Yii;

/**
 * This is the model class for table "{{%user_pay_wallet_cash}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $pay_system_id
 * @property int $currency_id
 * @property string $name
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $description
 */
class UserPayWalletCash extends UserPayWallet {
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_pay_wallet_cash}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'pay_system_id', 'currency_id', 'address', 'pay_password'], 'required'],
            [['user_id', 'pay_system_id', 'currency_id'], 'integer'],
            [['description'], 'string'],
            [['name', 'address', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function payAttributes() {
        return [
            'wallet' => $this->address
        ];
    }

    public function getInfoFields() {
        return [
            'name',
            'address',
            'email',
            'phone',
            'pay_password',
        ];
    }

    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getPaySystem() {
        return $this->hasOne(PaySystem::className(), ['id' => 'pay_system_id']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'pay_system_id' => Yii::t('app', 'Pay System ID'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'name' => Yii::t('app', 'ФИО'),
            'address' => Yii::t('app', 'Адрес'),
            'email' => Yii::t('app', 'E-mail'),
            'phone' => Yii::t('app', 'Телефон'),
            'description' => Yii::t('app', 'Описание'),
            'pay_password' => Yii::t('app', 'Платёжный пароль (полученный Вами по E-mail)'),
        ];
    }
}
