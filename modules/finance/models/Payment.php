<?php

namespace app\modules\finance\models;

use app\modules\finance\models\Currency;
use app\modules\finance\models\PaymentLog;
use app\modules\matrix\models\Matrix;
use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $status
 * @property double $sum
 * @property integer $currency_id
 * @property string $hash
 * @property string $date_add
 */
class Payment extends \yii\db\ActiveRecord {
	const TYPE_BUY_PLACE = 'buy_place'; // тип покупка места
	const TYPE_BUY_LEVEL = 'buy_level'; // тип покупка уровня
	const TYPE_IN_OFFICE = 'in_office'; // тип пополнить личный кабинет
	const TYPE_OUT_OFFICE = 'out_office'; // тип пополнить личный кабинет

	const STATUS_WAIT = 'wait'; // ожидание подтверждения польтзователя
	const STATUS_WAIT_SYSTEM = 'wait_system'; // ожидание подтверждение системы
	const PROCESS_SYSTEM = 'process_system'; // обработка системой
	const STATUS_OK = 'ok'; // обработано
	const STATUS_CANCEL = 'cancel'; // отменено
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'payment';
	}

	public static function getStatusArray() {
		return [
			self::STATUS_WAIT => Yii::t('app', 'Ожидает оплаты'),
			self::STATUS_WAIT_SYSTEM => Yii::t('app', 'Ожидает подтверждения системы'),
			self::PROCESS_SYSTEM => Yii::t('app', 'Обработка системой'),
			self::STATUS_OK => Yii::t('app', 'Оплачено'),
			self::STATUS_CANCEL => Yii::t('app', 'Отменено'),
		];
	}

	public function getStatusTitle() {
		$array = self::getStatusArray();

		return $array[$this->status];
	}

	public static function getTypeArray() {
		return [
			self::TYPE_BUY_PLACE => Yii::t('app', 'Покупка места'),
			self::TYPE_BUY_LEVEL => Yii::t('app', 'Покупка уровня'),
			self::TYPE_IN_OFFICE => Yii::t('app', 'Пополнение личного счёта'),
			self::TYPE_OUT_OFFICE => Yii::t('app', 'Снятие с личного счёта'),
		];
	}

	public static function getFinanceTypeArray() {
		return [
			self::TYPE_IN_OFFICE => Yii::t('app', 'Пополнение личного счёта'),
			self::TYPE_OUT_OFFICE => Yii::t('app', 'Снятие с личного счёта'),
		];
	}

	public function getTypeTitle() {
		$array = self::getTypeArray();

		if ($this->type == self::TYPE_BUY_LEVEL) {
			return $array[$this->type] . ' № ' . $this->additional_info;
		}

		return $array[$this->type];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['from_user_id', 'from_pay_system_id', 'to_user_id', 'from_currency_id'], 'integer'],
			[['from_sum', 'to_sum'], 'number'],
			[['date_add', 'comment', 'type', 'transaction_hash', 'wallet', 'additional_info', 'matrix_id', 'error', 'course_to_usd', 'charged_sum', 'course', 'to_currency_id', 'to_pay_system_id', 'to_sum', 'wallet_id'], 'safe'],
			[['status'], 'string', 'max' => 255],
		];
	}

	public function showToSum() {
		$currencyBtc = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
		if ($this->currency_id == $currencyBtc->id) {
			return number_format($this->to_sum, 6);
		} else {
			return number_format($this->to_sum, 2);
		}
	}

	public function showRealSum() {
		$currencyBtc = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
		if ($this->currency_id == $currencyBtc->id) {
			return number_format($this->realSum, 6);
		} else {
			return number_format($this->realSum, 2);
		}
	}

	public function getFromCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'from_currency_id']);
	}

	public function getToCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'to_currency_id']);
	}

	public function getToUser() {
		return $this->hasOne(User::className(), ['id' => 'to_user_id']);
	}

	public function getFromUser() {
		return $this->hasOne(User::className(), ['id' => 'from_user_id']);
	}

	public function getMatrix() {
		return $this->hasOne(Matrix::className(), ['id' => 'matrix_id']);
	}

	public function getFromPaySystem() {
		return $this->hasOne(PaySystem::className(), ['id' => 'from_pay_system_id']);
	}

	public function getToPaySystem() {
		return $this->hasOne(PaySystem::className(), ['id' => 'to_pay_system_id']);
	}

	public function getToUserWallet() {
		return $this->getToUser()->one()->getPayWallet($this->from_pay_system_id, $this->from_currency_id);
	}

	public function getFromUserWallet() {
		return $this->getFromUser()->one()->getPayWallet($this->to_pay_system_id, $this->to_currency_id);
	}

	public function getFromUserBalance() {
		$user = $this->getFromUser()->one();
		foreach ($user->getBalances() as $balance) {
			if ($balance->currency_id == $this->from_currency_id) {
				return $balance;
			}
		}
	}

	public function beforeSave($insert) {
		$paymentLog = PaymentLog::find()->where(['payment_id' => $this->id, 'status' => self::STATUS_OK])->one();

		if (!empty($paymentLog)) {

			$ip = empty($_SERVER['REMOTE_ADDR']) ? 'self' : $_SERVER['REMOTE_ADDR'];

			if (!empty(Yii::$app->user)) {
				$login = Yii::$app->user->identity->login;
			} else {
				$login = 'server';
			}

			$paymentLog->error = Yii::t('app', 'Попытка поменять статус у заявки с ип: {ip} , логин: {login} ', [
				'login' => $login,
				'ip' => $ip,
			]);

			$paymentLog->save(false);
			return false;
		}

		if ($this->hash == '') {
			$this->date_add = date('Y-m-d H:i:s');
			$this->hash = strtoupper(md5(time()));
		}

		if (parent::beforeSave($insert)) {
			return true;
		}
		return false;
	}

	public function afterSave($insert, $changedAttributes) {

		$paymentLog = new PaymentLog;
		$paymentLog->payment_id = $this->id;
		$paymentLog->status = $this->status;
		$paymentLog->date_add = date('Y-m-d H:i:s');
		$paymentLog->save(false);

		parent::afterSave($insert, $changedAttributes);

	}

	// расчёт суммы начисления(пересчёт в единую валюту если необходимо)
	public function recalcChargeSum() {
		$this->to_sum = $this->realSum * $this->course;
		$this->charged_sum = $this->to_sum;

		$this->save(false);
		return $this;
	}

	public function fromSumWithComission() {
		return $this->from_sum + $this->comission_from_sum;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'from_user_id' => Yii::t('app', 'От участника'),
			'to_user_id' => Yii::t('app', 'Участнику'),
			'status' => Yii::t('app', 'Статус'),
			'from_sum' => Yii::t('app', 'Начальная сумма'),
			'to_sum' => Yii::t('app', 'Конечная сумма'),
			'realSum' => Yii::t('app', 'Cумма'),
			'from_currency_id' => Yii::t('app', 'Начальная Валюта'),
			'to_currency_id' => Yii::t('app', 'Конечная валюта'),
			'hash' => Yii::t('app', 'Хэш операции'),
			'date_add' => Yii::t('app', 'Дата'),
			'from_pay_system_id' => Yii::t('app', 'Начальная платёжная система'),
			'to_pay_system_id' => Yii::t('app', 'Конечная платёжную систему'),
			'comment' => Yii::t('app', 'Комментарий'),
			'type' => Yii::t('app', 'Тип'),
			'wallet' => Yii::t('app', 'Кошелек'),
			'wallet_id' => Yii::t('app', 'Id Кошелька'),
			'transaction_hash' => Yii::t('app', 'Хэш транзакции'),
			'additional_info' => Yii::t('app', 'Дополнительная информация'),
			'matrix_id' => Yii::t('app', 'Номер места в матрице'),
			'course_to_usd' => Yii::t('app', 'Курс к доллару на момент обмена'),
			'charged_sum' => Yii::t('app', 'Начисленая сумма в кабинет'),
			'error' => Yii::t('app', 'Ошибки'),
			'course' => Yii::t('app', 'Курс'),
			'comission_from_sum' => Yii::t('app', 'Комиссия от начальной суммы валюты'),
			'comission_to_sum' => Yii::t('app', 'Комиссия от конечной суммы валюты'),
		];
	}
}
