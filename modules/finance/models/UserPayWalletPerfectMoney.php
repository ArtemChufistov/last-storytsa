<?php

namespace app\modules\finance\models;

use app\modules\finance\models\Currency;
use app\modules\finance\models\UserPayWallet;
use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "user_pay_wallet_perfectmoney".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $pay_system_id
 * @property integer $currency_id
 * @property string $wallet
 */
class UserPayWalletPerfectMoney extends UserPayWallet {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'user_pay_wallet_perfectmoney';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id', 'pay_system_id', 'currency_id', 'wallet', 'pay_password'], 'required'],
			['pay_password', 'validatePayPassword'],
			['wallet', 'perfectMoneyValidate'],
			[['user_id', 'pay_system_id', 'currency_id'], 'integer'],
			[['wallet'], 'string', 'max' => 255],
		];
	}

	// валидация кошелька
	public function perfectMoneyValidate($attribute, $params) {
		/*
			        if ( !preg_match("/^[13][0-9a-zA-Z]{25,34}$/i", $this->$attribute) ){
			            $this->addError($attribute, Yii::t('app', 'Вы указали неверный кошелек BitCoin'));
			        }
		*/
	}

	public function getInfoFields() {
		return [
			'wallet',
			'pay_password',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'Участник'),
			'pay_system_id' => Yii::t('app', 'Платёжная система'),
			'currency_id' => Yii::t('app', 'Валюта'),
			'wallet' => Yii::t('app', 'Кошелек'),
			'pay_password' => Yii::t('app', 'Платёжный пароль (полученный Вами по E-mail)'),
		];
	}
}
