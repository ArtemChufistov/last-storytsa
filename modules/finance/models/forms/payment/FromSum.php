<?php

namespace app\modules\finance\models\forms\payment;

use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\Payment;
use Yii;

class FromSum extends Payment {

    public function formName() {
        $class = new \ReflectionClass(get_parent_class($this));
        return $class->getShortName();
    }

	public function attributeLabels() {
		return [
			'from_sum' => Yii::t('app', 'Сумма'),
		];
	}

	public function rules() {
		return [
			[['from_sum'], 'required'],
			[['from_user_id', 'status', 'from_currency_id'], 'safe'],
			[['from_sum'], 'number'],
			[['from_sum'], 'userBalanceValidate'],
			[['from_sum'], 'minSumValidate'],
		];
	}

	public function userBalanceValidate() {

		if (empty($this->from_currency_id)) {
			$this->addError('from_currency_id', Yii::t('app', 'Такой валюты несуществует'));
		}

		$balances = $this->getFromUser()->one()->getBalances();

		foreach ($balances as $balance) {
			$balance->recalculateValue();
			if ($balance->value < $this->from_sum) {
				$this->addError('from_sum', Yii::t('app', 'Максимальная сумма для вывода {sum} {currency}', [
					'sum' => $balance->value,
					'currency' => $balance->getCurrency()->one()->title,
				]));
			}
		}
	}

	public function minSumValidate()
	{
		$minSum = 0;
		$balances = $this->getFromUser()->one()->getBalances();

		foreach ($balances as $balance) {
			if ($balance->value < $minSum) {
				$this->addError('from_sum', Yii::t('app', 'Минимальная сумма для вывода {sum} {currency}', [
					'sum' => $minSum,
					'currency' => $balance->getCurrency()->one()->title,
				]));
			}
		}
	}

	public function load($data, $formName = null) {
		$scope = $formName === null ? $this->formName() : $formName;

		$dataSet = false;
		if ($scope === '' && !empty($data)) {
			$this->setAttributes($data);
			$dataSet = true;
		} elseif (isset($data[$scope])) {
			$this->setAttributes($data[$scope]);
			$dataSet = true;
		}

		$currCourse = CurrencyCourse::getActivePayDirection($this, 'from_currency');
		if (count($currCourse) == 1) {
			$this->from_currency_id = $currCourse[0]->from_currency;
		}

		return $dataSet;
	}
}
