<?php

namespace app\modules\finance\models\forms\payment;

use app\modules\finance\models\Payment;
use Yii;

class ToSum extends Payment {

	public function attributeLabels() {
		return [
			'to_sum' => Yii::t('app', 'Сумма'),
		];
	}

	public function rules() {
		$minSum = 0.01;
		return [
			[['to_sum'], 'required'],
			[['to_user_id', 'status'], 'safe'],
			[['to_sum'], 'number', 'min' => $minSum, 'message' => Yii::t('app', 'Сумма должна быть больше {minSum}', ['minSum' => $minSum])],
		];
	}
}
