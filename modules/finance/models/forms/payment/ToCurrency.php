<?php

namespace app\modules\finance\models\forms\payment;

use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\Payment;
use Yii;

class ToCurrency extends Payment {

    public function formName() {
        $class = new \ReflectionClass(get_parent_class($this));
        return $class->getShortName();
    }

    public function attributeLabels() {
		return [
			'to_currency_id' => Yii::t('app', 'Пополняемая валюта'),
		];
	}

	public function rules() {
		return [
			[['to_currency_id'], 'required'],
			[['to_pay_system_id', 'to_sum', 'from_sum', 'to_user_id', 'from_user_id', 'status', 'type', 'from_pay_system_id'], 'safe'],
			[['to_currency_id'], 'toCurrencyValidate'],
		];
	}

	public function toCurrencyValidate($attribute, $params) {
		$currCourse = CurrencyCourse::getActivePayDirection($this, 'to_currency');
		if (empty($currCourse)) {
			$this->addError('to_currency_id', Yii::t('app', 'Такой валюты несуществует'));
		}
	}

	public function load($data, $formName = null) {
		$scope = $formName === null ? $this->formName() : $formName;

		$dataSet = false;
		if ($scope === '' && !empty($data)) {
			$this->setAttributes($data);
			$dataSet = true;
		} elseif (isset($data[$scope])) {
			$this->setAttributes($data[$scope]);
			$dataSet = true;
		}

		$currCourse = CurrencyCourse::getActivePayDirection($this, 'to_currency');

		if (count($currCourse) == 1) {
			$this->to_currency_id = $currCourse[0]->to_currency;
			$dataSet = true;
		}

		return $dataSet;
	}
}
