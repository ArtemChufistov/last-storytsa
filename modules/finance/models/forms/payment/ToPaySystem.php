<?php

namespace app\modules\finance\models\forms\payment;

use app\modules\finance\components\paysystem\PaySystemComponent;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\Payment;
use Yii;

class ToPaySystem extends Payment {

	public $sub_pay_system;

    public function formName() {
        $class = new \ReflectionClass(get_parent_class($this));
        return $class->getShortName();
    }

	public function attributes() {
		return array_merge(parent::attributes(), ['sub_pay_system']);
	}

	public function rules() {
		return [
			[['from_pay_system_id'], 'fromPaySystemValidate', 'skipOnEmpty' => false],
			[['sub_pay_system', 'to_user_id', 'from_user_id', 'to_sum', 'from_sum', 'status', 'to_currency_id', 'from_currency_id', 'to_pay_system_id', 'additional_info', 'type', 'course_to_usd'], 'safe'],
			[['from_sum'], 'fromSumValidate'],
			[['from_pay_system_id'], 'additionalInfoValidate', 'skipOnEmpty' => false],
		];
	}

	public function additionalInfoValidate($attribute = '', $params = '') {
		$result = PaySystemComponent::validatePaymentAttributes($this);
		if (!empty($result)) {
			$this->addError('from_pay_system_id', $result);
		}
	}

	public function fromPaySystemValidate($attribute, $params) {

		$currCourse = CurrencyCourse::getActivePayDirection($this, 'to_pay_system_id');
		if (empty($currCourse)) {
			$this->addError('from_pay_system_id', Yii::t('app', 'Нельзя осуществить перевод'));
		}

	}

	public function fromSumValidate($attribute, $params) {
		$currCourse = CurrencyCourse::getActivePayDirection($this);

		if (count($currCourse) == 1 && $this->from_sum < $currCourse[0]->min_sum) {
			$this->addError('from_sum', Yii::t('app', 'Минимальная сумма для вывода {min_sum} {currency}', ['min_sum' => $currCourse[0]->min_sum, 'currency' => $currCourse[0]->getFromCurrency()->one()->title]));
		}
	}

	public function load($data, $formName = null) {
		$scope = $formName === null ? $this->formName() : $formName;

		$dataSet = false;
		if ($scope === '' && !empty($data)) {
			$this->setAttributes($data);
			$dataSet = true;
		} elseif (isset($data[$scope])) {
			$this->setAttributes($data[$scope]);
			$dataSet = true;
		}

		$currCourse = CurrencyCourse::getActivePayDirection($this, 'to_pay_system_id');
		if (count($currCourse) == 1) {
			$this->to_pay_system_id = $currCourse[0]->to_pay_system_id;
			$dataSet = true;
		}

		return $dataSet;
	}

}
