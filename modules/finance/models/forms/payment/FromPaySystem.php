<?php

namespace app\modules\finance\models\forms\payment;

use app\modules\finance\components\paysystem\PaySystemComponent;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\Payment;
use Yii;

class FromPaySystem extends Payment {

	public $sub_pay_system;

	public function attributes() {
		return array_merge(parent::attributes(), ['sub_pay_system']);
	}

	public function rules() {
		return [
			[['from_pay_system_id'], 'fromPaySystemValidate', 'skipOnEmpty' => false],
			[['sub_pay_system', 'to_user_id', 'to_sum', 'status', 'to_currency_id', 'from_currency_id', 'to_pay_system_id', 'additional_info', 'type', 'course_to_usd'], 'safe'],
			[['from_pay_system_id'], 'additionalInfoValidate', 'skipOnEmpty' => false],
			['to_sum', 'validateCountPayment']
		];
	}
	public function additionalInfoValidate($attribute = '', $params = '') {
		$result = PaySystemComponent::validatePaymentAttributes($this);
		if (!empty($result)) {
			$this->addError('from_pay_system_id', $result);
		}
	}

	public function fromPaySystemValidate($attribute, $params) {

		$currCourse = CurrencyCourse::getActivePayDirection($this, 'from_pay_system_id');
		if (empty($currCourse)) {
			$this->addError('from_pay_system_id', Yii::t('app', 'Нельзя осуществить перевод'));
		}
	}

	public function load($data, $formName = null) {
		$scope = $formName === null ? $this->formName() : $formName;

		$dataSet = false;
		if ($scope === '' && !empty($data)) {
			$this->setAttributes($data);
			$dataSet = true;
		} elseif (isset($data[$scope])) {
			$this->setAttributes($data[$scope]);
			$dataSet = true;
		}

		$currCourse = CurrencyCourse::getActivePayDirection($this);
		if (count($currCourse) == 1) {
			$this->from_pay_system_id = $currCourse[0]->from_pay_system_id;

			$additionalInfo = PaySystemComponent::additionalInfoFrom($this);

			if (empty($additionalInfo)) {
				$dataSet = false;
			} else {

				$this->additional_info = json_encode($additionalInfo);
				if (!empty($additionalInfo) && !empty($additionalInfo['wallet_id'])){
					$this->wallet_id = $additionalInfo['wallet_id'];
				}

				if (!empty($additionalInfo) && !empty($additionalInfo['wallet'])){
					$this->wallet = $additionalInfo['wallet'];
				}

				$dataSet = true;
			}
		}

		return $dataSet;
	}

	public function validateCountPayment()
	{
		$currCourse = CurrencyCourse::getActivePayDirection($this);

		if ($currCourse[0]->one_active == CurrencyCourse::ONE_ACTIVE_TRUE){
			$payment = Payment::find()
				->where(['to_user_id' => $this->to_user_id])
				->andWhere(['from_pay_system_id' => $this->from_pay_system_id])
				->andWhere(['to_pay_system_id' => $this->to_pay_system_id])
				->andWhere(['from_currency_id' => $this->from_currency_id])
				->andWhere(['to_currency_id' => $this->to_currency_id])
				->andWhere(['status' => Payment::STATUS_WAIT_SYSTEM])
				->one();

			if (!empty($payment)){
				$this->addError('from_pay_system_id', Yii::t('app', 'Вам необходимо произвести оплату по уже существующей заявке'));
			}
		}
	}
}
