<?php

namespace app\modules\finance\models\forms\payment;

use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\Payment;
use Yii;

class FromCurrency extends Payment {

    public function formName() {
        $class = new \ReflectionClass(get_parent_class($this));
        return $class->getShortName();
    }

	public function attributeLabels() {
		return [
			'from_currency_id' => Yii::t('app', 'Валюта для пополнения'),
		];
	}

	public function rules() {
		return [
			[['from_currency_id'], 'required'],
			[['to_currency_id', 'from_currency_id', 'to_pay_system_id', 'from_pay_system_id', 'to_user_id', 'from_user_id', 'to_sum', 'from_sum', 'status', 'type'], 'safe'],
			[['from_currency_id'], 'fromCurrencyValidate'],
		];
	}

	public function fromCurrencyValidate($attribute, $params) {

		$currCourse = CurrencyCourse::getActivePayDirection($this, 'from_currency');

		if (empty($currCourse)) {
			$this->addError('from_currency_id', Yii::t('app', 'Такой валюты несуществует'));
		}
	}

	public function load($data, $formName = null) {
		$scope = $formName === null ? $this->formName() : $formName;

		$dataSet = false;
		if ($scope === '' && !empty($data)) {
			$this->setAttributes($data);
			$dataSet = true;
		} elseif (isset($data[$scope])) {
			$this->setAttributes($data[$scope]);
			$dataSet = true;
		}

		$currCourse = CurrencyCourse::getActivePayDirection($this, 'from_currency');
		if (count($currCourse) == 1) {
			$this->from_currency_id = $currCourse[0]->from_currency;
			$dataSet = true;
		}

		return $dataSet;
	}
}
