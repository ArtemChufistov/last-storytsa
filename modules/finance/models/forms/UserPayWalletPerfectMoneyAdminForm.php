<?php

namespace app\modules\finance\models\forms;

use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\UserPayWallet;
use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "user_pay_wallet_perfectmoney".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $pay_system_id
 * @property integer $currency_id
 * @property string $wallet
 */
class UserPayWalletPerfectMoneyAdminForm extends UserPayWallet {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'user_pay_wallet_perfectmoney';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id', 'pay_system_id', 'currency_id', 'wallet'], 'required'],
			[['user_id', 'pay_system_id', 'currency_id'], 'integer'],
			[['wallet'], 'string', 'max' => 255],
		];
	}

	public function getInfoFields() {
		return [
			'wallet',
			'pay_password',
		];
	}

	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
	}

	public function getPaySystem() {
		return $this->hasOne(PaySystem::className(), ['id' => 'pay_system_id']);
	}

	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'Участник'),
			'pay_system_id' => Yii::t('app', 'Платёжная система'),
			'currency_id' => Yii::t('app', 'Валюта'),
			'wallet' => Yii::t('app', 'Кошелек'),
		];
	}
}
