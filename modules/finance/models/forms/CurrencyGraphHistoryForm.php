<?php

namespace app\modules\finance\models\forms;

use app\modules\finance\models\CurrencyGraphHistory;
use Yii;

class CurrencyGraphHistoryForm extends CurrencyGraphHistory {

	public $from_currency_id;
	public $to_currency_id;
	public $date_from;
	public $date_to;
	public $breakdown_type;
	public $exchange;

	const EXCHANGE_POLONIEX = 'Poloniex';
	const EXCHANGE_LIVECOIN = 'Livecoin';

	public static function getExchangeArray() {
		return [
			self::EXCHANGE_POLONIEX => Yii::t('app', 'poloniex.com'),
			self::EXCHANGE_LIVECOIN => Yii::t('app', 'livecoin.net'),
		];
	}

	public function attributeLabels() {
		return [
			'from_currency_id' => Yii::t('app', 'Начальная валюта'),
			'to_currency_id' => Yii::t('app', 'Конечная валюта'),
			'date_from' => Yii::t('app', 'Дата от'),
			'date_to' => Yii::t('app', 'Дата до'),
			'breakdown_type' => Yii::t('app', 'Тип разбивки'),
			'exchange' => Yii::t('app', 'Биржа'),
		];
	}

	public function rules() {
		return [
			[['from_currency_id', 'to_currency_id', 'date_from', 'date_to', 'breakdown_type', 'exchange'], 'required'],
		];
	}
}
