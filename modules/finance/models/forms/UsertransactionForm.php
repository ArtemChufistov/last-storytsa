<?php

namespace app\modules\finance\models\forms;

use app\modules\finance\models\Transaction;
use app\modules\profile\models\User;
use Yii;

class UsertransactionForm extends Transaction {
	public $sum;
	public $to_login;
	public $from_login;
	public $to_user_id;
	public $currency_id;
	public $from_user_id;

	public function attributeLabels() {
		return [
			'sum' => Yii::t('app', 'Сумма'),
			'currency_id' => Yii::t('app', 'Валюта'),
			'to_login' => Yii::t('app', 'Логин участника'),
			'to_user_id' => Yii::t('app', 'Участнику'),
			'from_login' => Yii::t('app', 'От участника'),
			'from_user_id' => Yii::t('app', 'От участника'),
		];
	}

	public function rules() {
		return [
			[['sum', 'currency_id', 'to_login', 'from_login'], 'required'],
			[['to_login'], 'validateToUserLogin'],
			[['from_login'], 'validateFromUserLogin'],
			[['to_login'], 'validateSelfLogin'],
			[['sum'], 'sumValidate'],
			[['from_login'], 'safe'],
		];
	}

	public function validateToUserLogin($attribute, $params) {
		$user = User::find()->where(['login' => $this->to_login])->one();

		if (empty($user)) {
			$this->addError($attribute, Yii::t('app', 'Пользователя с таким логином не существует'));
		} else {
			$this->to_user_id = $user->id;
		}
	}

	public function validateFromUserLogin($attribute, $params) {
		$user = User::find()->where(['login' => $this->from_login])->one();

		if (empty($user)) {
			$this->addError($attribute, Yii::t('app', 'Пользователя с таким логином не существует'));
		} else {
			$this->from_user_id = $user->id;
		}
	}

	public function validateSelfLogin($attribute, $params)
	{
		if ($this->to_login == $this->from_login) {
			$this->addError($attribute, Yii::t('app', 'Нельзя создавать перевод самому себе'));
		}
	}

	// валидация сумма
	public function sumValidate($attribute, $params) {
		$this->sum = str_replace(',', '.', $this->sum);
		$this->sum = str_replace('-', '', $this->sum);

		$user = User::find()->where(['id' => $this->from_user_id])->one();
		$userBalance = $user->getBalances([$this->currency_id])[$this->currency_id];

		$userBalance->recalculateValue();

		if ($userBalance->value < $this->sum) {
			$this->addError($attribute, Yii::t('app', 'Слишком большая сумма, Вам нужно указать сумму не большую суммы на Вашем личном счете'));
			return;
		}

		if ($userBalance->value < 0.001) {
			$this->addError($attribute, Yii::t('app', 'Слишком маленькая сумма, Вам нужно указать сумму не меньшую 0.001'));
			return;
		}

	}

	public function afterSave($insert, $changedAttributes) {
		if ($this->type == parent::TYPE_OUT_OFFICE) {

			$user = User::find()->where(['id' => $this->from_user_id])->one();
			$userBalance = $user->getBalances()[$this->currency_id];
			$userBalance->recalculateValue();
		}

		parent::afterSave($insert, $changedAttributes);
	}
}
