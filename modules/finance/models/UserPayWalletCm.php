<?php

namespace app\modules\finance\models;

use app\modules\finance\components\paysystem\CmComponent;
use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\UserPayWallet;
use app\modules\profile\models\User;
use linslin\yii2\curl;
use Yii;

/**
 * This is the model class for table "user_pay_wallet_cm".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $pay_system_id
 * @property integer $currency_id
 * @property string $wallet
 */
class UserPayWalletCm extends UserPayWallet {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'user_pay_wallet_cm';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id', 'pay_system_id', 'currency_id', 'wallet', 'pay_password'], 'required'],
			['pay_password', 'validatePayPassword'],
			['wallet', 'blockChainValidate'],
			[['in_wallet'], 'safe'],
			[['user_id', 'pay_system_id', 'currency_id'], 'integer'],
			[['wallet'], 'string', 'max' => 255],
		];
	}

	public function blockChainValidate($attribute, $params) {

		if ($this->getCurrency()->one()->key == Currency::KEY_BTC) {
			$curl = new curl\Curl();

			$url = UserPayWalletBitcoin::BLOCKHAIN_WALLET_URL . $this->$attribute;
			$response = $curl->get($url);
			if ($response == 'Checksum does not validate') {
				$this->addError($attribute, Yii::t('app', 'Кошелек не найден в сети BitCoin'));
				return;
			}

			$wallet = json_decode($response, true);

			if (empty($wallet)) {
				$this->addError($attribute, Yii::t('app', 'Такой кошелек не зарегистрирован в сети BitCoin'));
				return;
			}

			$inWallet = $this->find()->where(['in_wallet' => $this->$attribute])->one();

			if (!empty($inWallet)) {
				$this->addError($attribute, Yii::t('app', 'Этот кошелёк запрещен, пожалуйста укажите другой кошелек'));
				return;
			}
		}
	}

	public function payAttributes() {
		return [
			'wallet' => $this->wallet,
		];
	}

	public function getInfoFields() {
		return [
			'wallet',
			'pay_password',
		];
	}

	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
	}

	public function getPaySystem() {
		return $this->hasOne(PaySystem::className(), ['id' => 'pay_system_id']);
	}

	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'Участник'),
			'pay_system_id' => Yii::t('app', 'Платёжная система'),
			'currency_id' => Yii::t('app', 'Валюта'),
			'wallet' => Yii::t('app', 'Кошелек'),
			'in_wallet' => Yii::t('app', 'Кошелек для пополнения'),
			'pay_password' => Yii::t('app', 'Платёжный пароль (полученный Вами по E-mail)'),
		];
	}

	public function beforeSave($insert) {
		if ($this->in_wallet == '') {
			$wallet = CmComponent::getInPaymentAddress($this->getCurrency()->one()->key);
			$this->in_wallet = $wallet;
		}

		if (parent::beforeSave($insert)) {
			return true;
		} else {
			return false;
		}
	}
}
