<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "{{%coinbase_transaction}}".
 *
 * @property integer $id
 * @property string $wallet_id
 * @property string $type
 * @property double $amount
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $resource
 * @property string $resource_path
 * @property string $instant_exchange
 * @property string $network_status
 * @property string $network_hash
 * @property integer $processed
 * @property integer $payment_id
 */
class CoinbaseTransaction extends \yii\db\ActiveRecord {
	const STATUS_COMPLETED = 'completed';
	const NETWORK_STATUS_CONFIRMED = 'confirmed';
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%coinbase_transaction}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['wallet_id', 'type', 'resource', 'resource_path', 'network_status', 'network_hash'], 'required'],
			[['amount'], 'number'],
			[['created_at', 'updated_at', 'instant_exchange', 'status'], 'safe'],
			[['processed', 'payment_id'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'wallet_id' => Yii::t('app', 'Wallet ID'),
			'type' => Yii::t('app', 'Type'),
			'amount' => Yii::t('app', 'Amount'),
			'description' => Yii::t('app', 'Description'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'resource' => Yii::t('app', 'Resource'),
			'resource_path' => Yii::t('app', 'Resource Path'),
			'instant_exchange' => Yii::t('app', 'Instant Exchange'),
			'status' => Yii::t('app', 'Status'),
			'network_status' => Yii::t('app', 'Network Status'),
			'network_hash' => Yii::t('app', 'Network Hash'),
			'processed' => Yii::t('app', 'Processed'),
			'payment_id' => Yii::t('app', 'Payment ID'),
		];
	}
}
