<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "{{%cm_transaction}}".
 *
 * @property int $id
 * @property int $account
 * @property string $address
 * @property string $category
 * @property double $amount
 * @property string $label
 * @property int $vout
 * @property int $confirmations
 * @property string $blockhash
 * @property int $blockindex
 * @property string $blocktime
 * @property string $txid
 * @property string $time
 * @property string $timereceived
 */
class CmTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cm_transaction}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['account', 'vout', 'confirmations', 'blockindex'], 'integer'],
            [['amount'], 'number'],
            [['blocktime', 'time', 'timereceived', 'payment_id'], 'safe'],
            [['address', 'category', 'label', 'blockhash', 'txid'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'payment_id' => Yii::t('app', 'Payment Id'),
            'account' => Yii::t('app', 'Account'),
            'address' => Yii::t('app', 'Address'),
            'category' => Yii::t('app', 'Category'),
            'amount' => Yii::t('app', 'Amount'),
            'label' => Yii::t('app', 'Label'),
            'vout' => Yii::t('app', 'Vout'),
            'confirmations' => Yii::t('app', 'Confirmations'),
            'blockhash' => Yii::t('app', 'Blockhash'),
            'blockindex' => Yii::t('app', 'Blockindex'),
            'blocktime' => Yii::t('app', 'Blocktime'),
            'txid' => Yii::t('app', 'Txid'),
            'time' => Yii::t('app', 'Time'),
            'timereceived' => Yii::t('app', 'Timereceived'),
        ];
    }
}
