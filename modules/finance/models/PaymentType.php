<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "payment_type".
 *
 * @property int $id
 * @property string $key
 * @property int $active
 */
class PaymentType extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'payment_type';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['active'], 'integer'],
			[['key'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'key' => Yii::t('app', 'Key'),
			'active' => Yii::t('app', 'Active'),
		];
	}
}
