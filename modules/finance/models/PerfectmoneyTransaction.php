<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "{{%perfectmoney_transaction}}".
 *
 * @property int $id
 * @property int $payment_id
 * @property string $payee_account
 * @property double $amount
 * @property string $units
 * @property string $batch_num
 * @property string $payeer_account
 * @property string $timestamp
 */
class PerfectmoneyTransaction extends \yii\db\ActiveRecord {
	const TREATED_TRUE = 1;
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%perfectmoney_transaction}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['amount'], 'number'],
			[['timestamp', 'v2_hash', 'payment_id', 'treated', 'date_tread'], 'safe'],
			[['payee_account', 'units', 'batch_num', 'payeer_account'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'payment_id' => Yii::t('app', 'Payment ID'),
			'payee_account' => Yii::t('app', 'Payee Account'),
			'amount' => Yii::t('app', 'Amount'),
			'units' => Yii::t('app', 'Units'),
			'batch_num' => Yii::t('app', 'Batch Num'),
			'payeer_account' => Yii::t('app', 'Payeer Account'),
			'timestamp' => Yii::t('app', 'Timestamp'),
			'v2_hash' => Yii::t('app', 'v2_hash'),
			'treated' => Yii::t('app', 'treated'),
			'date_tread' => Yii::t('app', 'date_tread'),
		];
	}
}
