<?php

namespace app\modules\finance\models;

use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\UserPayWallet;
use app\modules\profile\models\User;
use linslin\yii2\curl;
use Yii;

/**
 * This is the model class for table "user_pay_wallet_bitcoin".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $pay_system_id
 * @property integer $currency_id
 * @property string $site_url
 * @property string $wallet
 */
class UserPayWalletBitcoin extends UserPayWallet {

	const BLOCKHAIN_URL = 'https://blockchain.info/ru/rawtx/';
	const BLOCKHAIN_SHOW_URL = 'https://blockchain.info/ru/tx/';
	const BLOCKHAIN_WALLET_URL = 'https://blockchain.info/ru/rawaddr/';

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'user_pay_wallet_bitcoin';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id', 'pay_system_id', 'currency_id', 'wallet', 'pay_password'], 'required'],
			['pay_password', 'validatePayPassword'],
			['wallet', 'bitCoinValidate'],
			['wallet', 'blockChainValidate'],
			[['in_wallet', 'wallet_id'], 'safe'],
			[['user_id', 'pay_system_id', 'currency_id'], 'integer'],
			[['site_url', 'wallet'], 'string', 'max' => 255],
		];
	}

	// валидация кошелька
	public function bitCoinValidate($attribute, $params) {
		if (!preg_match("/^[13][0-9a-zA-Z]{25,34}$/i", $this->$attribute)) {
			$this->addError($attribute, Yii::t('app', 'Вы указали неверный кошелек BitCoin'));
		}
	}

	public function blockChainValidate($attribute, $params) {
		$curl = new curl\Curl();

		$url = UserPayWalletBitcoin::BLOCKHAIN_WALLET_URL . $this->$attribute;
		$response = $curl->get($url);

		if ($response == 'Checksum does not validate') {
			$this->addError($attribute, Yii::t('app', 'Кошелек не найден в сети BitCoin'));
			return;
		}

		$wallet = json_decode($response, true);

		if (empty($wallet)) {
			$this->addError($attribute, Yii::t('app', 'Такой кошелек не зарегистрирован в сети BitCoin'));
			return;
		}
	}

	public function getInfoFields() {
		return [
			'wallet',
			'pay_password',
		];
	}

	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
	}

	public function getPaySystem() {
		return $this->hasOne(PaySystem::className(), ['id' => 'pay_system_id']);
	}

	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'Участник'),
			'pay_system_id' => Yii::t('app', 'Платёжная система'),
			'currency_id' => Yii::t('app', 'Валюта'),
			'site_url' => Yii::t('app', 'Сайт кошелька'),
			'wallet' => Yii::t('app', 'Кошелек'),
			'in_wallet' => Yii::t('app', 'Кошелек для поплнений'),
			'wallet_id' => Yii::t('app', 'Id кошелька в сети'),
			'pay_password' => Yii::t('app', 'Платёжный пароль (полученный Вами по E-mail)'),
		];
	}
}
