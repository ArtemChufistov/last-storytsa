<?php

namespace app\modules\finance\models;

use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "user_pay_wallet_bitcoin".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $pay_system_id
 * @property integer $currency_id
 * @property string $site_url
 * @property string $wallet
 */
class UserPayWallet extends \yii\db\ActiveRecord {

	public $pay_password;

	public function validatePayPassword() {
		$user = User::find()->where(['id' => $this->user_id])->one();

		if ($user->pay_password_hash == '') {
			$this->addError('pay_password', Yii::t('app', 'Вам необходимо получить платёжный пароль, Нажмите на кнопку "Получить Платежный пароль" и проверьте почту.'));
			return;
		}

		if (!Yii::$app->security->validatePassword($this->pay_password, $user->pay_password_hash)) {
			$this->addError('pay_password', Yii::t('app', 'Платёжный пароль введён неверно'));
			return;
		}
	}

	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
	}

	public function getPaySystem() {
		return $this->hasOne(PaySystem::className(), ['id' => 'pay_system_id']);
	}

	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

}