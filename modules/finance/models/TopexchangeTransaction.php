<?php

namespace app\modules\finance\models;

use Yii;

/**
 * This is the model class for table "{{%topexchange_transaction}}".
 *
 * @property int $id
 * @property int $payment_id
 * @property string $merchant_name
 * @property string $merchant_title
 * @property string $payed_paysys
 * @property double $amount
 * @property string $payment_info
 * @property string $payment_num
 * @property string $sucess_url
 * @property string $error_url
 * @property string $obmen_order_id
 * @property string $obmen_recive_valute
 * @property string $obmen_timestamp
 */
class TopexchangeTransaction extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%topexchange_transaction}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['payment_id'], 'integer'],
			[['amount'], 'number'],
			[['obmen_timestamp'], 'safe'],
			[['merchant_name', 'merchant_title', 'payed_paysys', 'payment_info', 'payment_num', 'sucess_url', 'error_url', 'obmen_order_id', 'obmen_recive_valute'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'payment_id' => Yii::t('app', 'Payment ID'),
			'merchant_name' => Yii::t('app', 'Merchant Name'),
			'merchant_title' => Yii::t('app', 'Merchant Title'),
			'payed_paysys' => Yii::t('app', 'Payed Paysys'),
			'amount' => Yii::t('app', 'Amount'),
			'payment_info' => Yii::t('app', 'Payment Info'),
			'payment_num' => Yii::t('app', 'Payment Num'),
			'sucess_url' => Yii::t('app', 'Sucess Url'),
			'error_url' => Yii::t('app', 'Error Url'),
			'obmen_order_id' => Yii::t('app', 'Obmen Order ID'),
			'obmen_recive_valute' => Yii::t('app', 'Obmen Recive Valute'),
			'obmen_timestamp' => Yii::t('app', 'Obmen Timestamp'),
		];
	}
}
