<?php

namespace app\modules\finance\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\models\CashTransaction;

/**
 * CashTransactionSearch represents the model behind the search form of `app\modules\finance\models\CashTransaction`.
 */
class CashTransactionSearch extends CashTransaction
{
    public $user_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_id', 'user_id'], 'integer'],
            [['name', 'address', 'email', 'phone', 'skype', 'status', 'date_add'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CashTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date_add' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'amount' => $this->amount,
            'payment_id' => $this->payment_id,
        ]);

        if ($this->date_add != '') {
            $query->andFilterWhere(['between', 'date_add', date('Y-m-d 0:0:0', strtotime($this->date_add)), date('Y-m-d 23:59:59', strtotime($this->date_add))]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'skype', $this->skype])
            ->andFilterWhere(['like', 'status', $this->status]);

        if (!empty($this->user_id)){
            $query->leftJoin('payment AS to_payment', 'to_payment.id = cash_transaction.payment_id');
            $query->leftJoin('lb_user AS to_user', 'to_user.id = to_payment.to_user_id');

            $query->leftJoin('payment AS from_payment', 'from_payment.id = cash_transaction.payment_id');
            $query->leftJoin('lb_user AS from_user', 'from_user.id = from_payment.from_user_id');

            $query->andFilterWhere(['or', ['from_user.id' => $this->user_id], ['to_user.id' => $this->user_id]]);
        }

        return $dataProvider;
    }
}
