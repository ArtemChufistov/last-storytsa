<?php

namespace app\modules\finance\models\search;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use app\modules\finance\models\Currency;
use app\modules\finance\models\UserBalance;
use app\modules\finance\models\Transaction;

/**
 * Поиск среди пользователей
 * Class UserSearch
 * @package lowbase\user\models
 */
class TransactionStatSearch extends Transaction
{

    public $currency_id;
    public $perday;
    public $dateFrom;
    public $dateTo;
    public $officeTypeArray;

    /**
     * Сценарии
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function rules() {
        return [
            [['dateFrom', 'dateTo', 'officeTypeArray', 'currency_id'], 'safe'],
        ];
    }

    /**
     * Наименования дополнительных полей
     * аттрибутов, присущих модели поиска
     * @return array
     */
    public function attributeLabels()
    {
        $label = parent::attributeLabels();
        $label['count'] = Yii::t('app', 'count');
        $label['dateFrom'] = Yii::t('app', 'Дата от');
        $label['dateTo'] = Yii::t('app', 'Дата до');
        $label['currency_id'] = Yii::t('app', 'Валюта');
        $label['officeTypeArray'] = Yii::t('app', 'Типы транзакций');

        return $label;
    }

    public function initCurrency()
    {
        if (empty($this->currency_id)) {
            $currency = Currency::find()->one();
            $this->currency_id = $currency->id;
        }
    }

    public function initOfficeTypeArray()
    {
        if (empty($this->officeTypeArray)) {
            $transactionTypes = ArrayHelper::map(Transaction::find()->where(['currency_id' => $this->currency_id])->select('type')->distinct()->all(), 'type', 'type');

            $fullTransactionType = Transaction::getTypeArray();

            foreach($transactionTypes as $typeKey => $typeTitle){
                $this->officeTypeArray[$typeKey] = $fullTransactionType[$typeKey];
            }

        }else{
            $this->officeTypeArray = array_flip($this->officeTypeArray);
        }
    }

    public function initDateFromTo()
    {
        $time = time();
        if (empty($this->dateFrom)){
            $this->dateFrom = date('Y-m-d 00:00:00', $time - (60*60*24*30));
        }else{
            $this->dateFrom = date('Y-m-d 00:00:00', strtotime($this->dateFrom));
        }

        if (empty($this->dateTo)){
            $this->dateTo = date('Y-m-d 23:59:59', $time);
        }else{
            $this->dateTo = date('Y-m-d 23:59:59', strtotime($this->dateTo));
        }
    }

    /**
     * Создает DataProvider на основе переданных данных
     * @param $params - параметры
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $this->initCurrency();

        $this->initOfficeTypeArray();

        $this->initDateFromTo();

        $query = self::find();

        $query->select('sum(sum) as sum, DATE_FORMAT(date_add,"%Y-%m-%d") as `perday`, type, currency_id');

        $query->where(['currency_id' => $this->currency_id]);

        $query->andWhere(['>=', 'date_add', $this->dateFrom]);

        $query->andWhere(['<=', 'date_add', $this->dateTo]);

        $whereType = '';
        foreach ($this->officeTypeArray as $typeKey => $typeName) {
            $whereType .= 'type = "' . $typeKey . '" OR ';
        }

        $whereType = substr($whereType, 0, -4);

        $query->andWhere($whereType);

        $query->groupBy(['type', 'perday']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => ['date_add' => SORT_DESC],
            ),
        ]);

        return $dataProvider;
    }
}
