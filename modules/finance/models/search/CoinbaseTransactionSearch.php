<?php

namespace app\modules\finance\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\models\CoinbaseTransaction;

/**
 * CoinbaseTransactionSearch represents the model behind the search form of `app\modules\finance\models\CoinbaseTransaction`.
 */
class CoinbaseTransactionSearch extends CoinbaseTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'processed', 'payment_id'], 'integer'],
            [['wallet_id', 'type', 'description', 'created_at', 'updated_at', 'resource', 'resource_path', 'instant_exchange', 'status', 'network_status', 'network_hash'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoinbaseTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'amount' => $this->amount,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'processed' => $this->processed,
            'payment_id' => $this->payment_id,
        ]);

        $query->andFilterWhere(['like', 'wallet_id', $this->wallet_id])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'resource', $this->resource])
            ->andFilterWhere(['like', 'resource_path', $this->resource_path])
            ->andFilterWhere(['like', 'instant_exchange', $this->instant_exchange])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'network_status', $this->network_status])
            ->andFilterWhere(['like', 'network_hash', $this->network_hash]);

        return $dataProvider;
    }
}
