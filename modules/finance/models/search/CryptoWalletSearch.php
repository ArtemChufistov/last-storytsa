<?php

namespace app\modules\finance\models\search;

use app\modules\finance\models\CryptoWallet;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CryptoWalletSearch represents the model behind the search form of `app\modules\finance\models\CryptoWallet`.
 */
class CryptoWalletSearch extends CryptoWallet {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'currency_id'], 'integer'],
			[['wallet', 'info'], 'safe'],
			[['balance'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = CryptoWallet::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'balance' => $this->balance,
			'currency_id' => $this->currency_id,
		]);

		$query->andFilterWhere(['like', 'wallet', $this->wallet])
			->andFilterWhere(['like', 'info', $this->info]);

		return $dataProvider;
	}
}
