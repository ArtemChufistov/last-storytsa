<?php

namespace app\modules\finance\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\models\AdvcashTransaction;

/**
 * AdvcashTransactionSearch represents the model behind the search form of `app\modules\finance\models\AdvcashTransaction`.
 */
class AdvcashTransactionSearch extends AdvcashTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'treated'], 'integer'],
            [['ac_transfer', 'ac_start_date', 'ac_sci_name', 'ac_src_wallet', 'ac_dest_wallet', 'ac_order_id', 'ac_merchant_currency', 'date_tread'], 'safe'],
            [['ac_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdvcashTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ac_start_date' => $this->ac_start_date,
            'ac_amount' => $this->ac_amount,
            'treated' => $this->treated,
            'date_tread' => $this->date_tread,
        ]);

        $query->andFilterWhere(['like', 'ac_transfer', $this->ac_transfer])
            ->andFilterWhere(['like', 'ac_sci_name', $this->ac_sci_name])
            ->andFilterWhere(['like', 'ac_src_wallet', $this->ac_src_wallet])
            ->andFilterWhere(['like', 'ac_dest_wallet', $this->ac_dest_wallet])
            ->andFilterWhere(['like', 'ac_order_id', $this->ac_order_id])
            ->andFilterWhere(['like', 'ac_merchant_currency', $this->ac_merchant_currency]);

        return $dataProvider;
    }
}
