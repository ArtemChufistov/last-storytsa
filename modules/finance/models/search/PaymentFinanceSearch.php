<?php

namespace app\modules\finance\models\search;

use app\modules\finance\models\Payment;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaymentSearch represents the model behind the search form of `app\modules\finance\models\Payment`.
 */
class PaymentFinanceSearch extends Payment {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'from_user_id', 'to_user_id', 'from_currency_id'], 'integer'],
			[['status', 'transaction_hash', 'date_add', 'type', 'wallet', 'to_sum', 'from_sum'], 'safe'],
			[['from_sum', 'realSum'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Payment::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'type' => $this->type,
			'wallet' => $this->wallet,
			'from_currency_id' => $this->from_currency_id,
		]);

		//$query->andFilterWhere(['in', 'status', [Payment::STATUS_WAIT, Payment::STATUS_WAIT_SYSTEM, Payment::PROCESS_SYSTEM]]);

		$query->andFilterWhere(['or', ['to_user_id' => $this->from_user_id], ['from_user_id' => $this->from_user_id]]);

		$query->andFilterWhere(['like', 'status', $this->status])
			->andFilterWhere(['like', 'transaction_hash', $this->transaction_hash]);

		if ($this->from_sum != '') {
			$query->andFilterWhere(['between', 'from_sum', ($this->from_sum - 0.001), ($this->from_sum + 0.001)]);
		}

		if ($this->to_sum != '') {
			$query->andFilterWhere(['between', 'from_sum', ($this->to_sum - 0.001), ($this->to_sum + 0.001)]);
		}

		if ($this->realSum != '') {
			$query->andFilterWhere(['between', 'realSum', ($this->realSum - 0.001), ($this->realSum + 0.001)]);
		}

		if ($this->date_add != '') {
			$query->andFilterWhere(['between', 'date_add', date('Y-m-d 0:0:0', strtotime($this->date_add)), date('Y-m-d 23:59:59', strtotime($this->date_add))]);
		}

		return $dataProvider;
	}
}
