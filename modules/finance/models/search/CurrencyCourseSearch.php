<?php

namespace app\modules\finance\models\search;

use app\modules\finance\models\CurrencyCourse;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CurrencyCourseSearch represents the model behind the search form of `app\modules\finance\models\CurrencyCourse`.
 */
class CurrencyCourseSearch extends CurrencyCourse {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'from_pay_system_id', 'to_pay_system_id', 'from_currency', 'to_currency'], 'integer'],
			[['type', 'course', 'course_function'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = CurrencyCourse::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'from_pay_system_id' => $this->from_pay_system_id,
			'to_pay_system_id' => $this->to_pay_system_id,
			'from_currency' => $this->from_currency,
			'to_currency' => $this->to_currency,
		]);

		$query->andFilterWhere(['like', 'type', $this->type])
			->andFilterWhere(['like', 'course', $this->course])
			->andFilterWhere(['like', 'course_function', $this->course_function]);

		return $dataProvider;
	}
}
