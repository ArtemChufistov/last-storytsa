<?php

namespace app\modules\finance\models\search;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use app\modules\finance\models\Currency;
use app\modules\finance\models\UserBalance;
use app\modules\finance\models\Transaction;

/**
 * Поиск среди пользователей
 * Class UserSearch
 * @package lowbase\user\models
 */
class UserBalanceStatSearch extends Transaction
{

    public $balanceUser;
    public $currency_id;
    public $perday;
    public $dateFrom;
    public $dateTo;

    /**
     * Сценарии
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function rules() {
        return [
            [['dateFrom', 'dateTo', 'currency_id'], 'safe'],
        ];
    }

    /**
     * Наименования дополнительных полей
     * аттрибутов, присущих модели поиска
     * @return array
     */
    public function attributeLabels()
    {
        $label = parent::attributeLabels();
        $label['count'] = Yii::t('app', 'count');
        $label['dateFrom'] = Yii::t('app', 'Дата от');
        $label['dateTo'] = Yii::t('app', 'Дата до');
        $label['currency_id'] = Yii::t('app', 'Валюта');

        return $label;
    }

    public function initCurrency()
    {
        if (empty($this->currency)) {
            $currency = Currency::find()->one();
            $this->currency_id = $currency->id;
        }
    }

    public function initDateFromTo()
    {
        $time = time();
        if (empty($this->dateFrom)){
            $this->dateFrom = date('Y-m-d 00:00:00', $time - (60*60*24*30));
        }else{
            $this->dateFrom = date('Y-m-d 00:00:00', strtotime($this->dateFrom));
        }

        if (empty($this->dateTo)){
            $this->dateTo = date('Y-m-d 23:59:59', $time);
        }else{
            $this->dateTo = date('Y-m-d 23:59:59', strtotime($this->dateTo));
        }
    }

    public function initParams($params)
    {
        $this->load($params);

        $this->initCurrency();

        $this->initDateFromTo();
    }

    public function sumByDate($dateTo)
    {

        $plusTransaction = parent::find()
            ->select('sum(sum) as sum, type, currency_id')
            ->where(['currency_id' => $this->currency_id])
            ->andWhere(['<=', 'date_add', $dateTo])
            ->andWhere(['IS NOT', 'to_user_id', null])
            ->one();

        $minusTransaction = parent::find()
            ->select('sum(sum) as sum, type, currency_id')
            ->where(['currency_id' => $this->currency_id])
            ->andWhere(['<=', 'date_add', $dateTo])
            ->andWhere(['IS NOT', 'from_user_id', null])
            ->one();

        return $plusTransaction->sum - $minusTransaction->sum;
    }
}
