<?php

namespace app\modules\finance\models\search;

use app\modules\finance\models\UserPayWalletBitcoin;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserPayWalletBitcoinSearch represents the model behind the search form of `app\modules\finance\models\UserPayWalletBitcoin`.
 */
class UserPayWalletBitcoinSearch extends UserPayWalletBitcoin {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'user_id', 'pay_system_id', 'currency_id'], 'integer'],
			[['site_url', 'wallet'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = UserPayWalletBitcoin::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC,
				],
			],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'user_id' => $this->user_id,
			'pay_system_id' => $this->pay_system_id,
			'currency_id' => $this->currency_id,
		]);

		$query->andFilterWhere(['like', 'site_url', $this->site_url])
			->andFilterWhere(['like', 'wallet', $this->wallet]);

		return $dataProvider;
	}
}
