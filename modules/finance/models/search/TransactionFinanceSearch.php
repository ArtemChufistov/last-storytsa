<?php

namespace app\modules\finance\models\search;

use app\modules\finance\models\Transaction;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaymentSearch represents the model behind the search form of `app\modules\finance\models\Transaction`.
 */
class TransactionFinanceSearch extends Transaction {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'from_user_id', 'to_user_id', 'currency_id'], 'integer'],
			[['type', 'date_add', 'course'], 'safe'],
			[['sum'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Transaction::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'type' => $this->type,
			'course' => $this->course,
			'currency_id' => $this->currency_id,
		]);

		$query->andFilterWhere(['or', ['to_user_id' => $this->from_user_id], ['from_user_id' => $this->from_user_id]]);

		if ($this->sum != '') {
			$query->andFilterWhere(['between', 'sum', ($this->sum - 0.001), ($this->sum + 0.001)]);
		}

		if ($this->date_add != '') {
			$query->andFilterWhere(['between', 'date_add', date('Y-m-d 0:0:0', strtotime($this->date_add)), date('Y-m-d 23:59:59', strtotime($this->date_add))]);
		}

		if ($_SERVER['REMOTE_ADDR'] == '212.20.46.243'){
			//echo '<pre>';print_r($query->createCommand()->getRawSql());exit;
		}


		return $dataProvider;
	}
}
