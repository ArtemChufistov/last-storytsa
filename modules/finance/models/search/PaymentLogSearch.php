<?php

namespace app\modules\finance\models\search;

use app\modules\finance\models\PaymentLog;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaymentLogSearch represents the model behind the search form of `app\modules\finance\models\PaymentLog`.
 */
class PaymentLogSearch extends PaymentLog {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'payment_id'], 'integer'],
			[['status', 'date_add'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = PaymentLog::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'payment_id' => $this->payment_id,
			'date_add' => $this->date_add,
		]);

		$query->andFilterWhere(['like', 'status', $this->status]);

		return $dataProvider;
	}
}
