<?php

namespace app\modules\finance\models\search;

use app\modules\finance\models\Payment;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaymentSearch represents the model behind the search form of `app\modules\finance\models\Payment`.
 */
class PaymentSearch extends Payment {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'from_user_id', 'to_user_id', 'from_currency_id'], 'integer'],
			[['status', 'hash', 'date_add', 'transaction_hash', 'type', 'from_pay_system_id', 'to_pay_system_id', 'from_currency_id'], 'safe'],
			[['from_sum'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Payment::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC,
				],
			],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'from_user_id' => $this->from_user_id,
			'to_user_id' => $this->to_user_id,
			'from_sum' => $this->from_sum,
			'from_currency_id' => $this->from_currency_id,
			'from_pay_system_id' => $this->from_pay_system_id,
			'to_pay_system_id' => $this->to_pay_system_id,
			'type' => $this->type,
		]);

		if ($this->date_add != '') {
			$query->andFilterWhere(['between', 'date_add', date('Y-m-d 0:0:0', strtotime($this->date_add)), date('Y-m-d 23:59:59', strtotime($this->date_add))]);
		}

		if ($this->status) {
			$query->andFilterWhere(['status' => $this->status]);
		}

		$query->andFilterWhere(['like', 'hash', $this->hash]);
		$query->andFilterWhere(['like', 'transaction_hash', $this->transaction_hash]);

		return $dataProvider;
	}
}
