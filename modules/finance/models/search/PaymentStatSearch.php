<?php

namespace app\modules\finance\models\search;

use app\modules\finance\models\Currency;
use dosamigos\datepicker\DatePicker;
use app\modules\finance\models\Payment;
use app\modules\finance\models\PaySystem;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Поиск среди пользователей
 * Class UserSearch
 * @package lowbase\user\models
 */
class PaymentStatSearch extends Payment {

	public $sumPay;
	public $toSumPay;
	public $perday;

	public $currency;
	public $statusArray;
	public $officeTypeArray;
	public $dateFrom;
	public $dateTo;

	public function rules() {
		return [
			[['currency', 'officeTypeArray', 'dateFrom', 'dateTo', 'statusArray'], 'safe'],
		];
	}

	/**
	 * Сценарии
	 * @return array
	 */
	public function scenarios() {
		return Model::scenarios();
	}

	/**
	 * Наименования дополнительных полей
	 * аттрибутов, присущих модели поиска
	 * @return array
	 */
	public function attributeLabels() {
		$label = parent::attributeLabels();
		$label['sumPay'] = Yii::t('app', 'Сумма');
		$label['perday'] = Yii::t('app', 'Дни');
		$label['currency'] = Yii::t('app', 'Валюта');
		$label['dateFrom'] = Yii::t('app', 'Дата от');
		$label['dateTo'] = Yii::t('app', 'Дата до');
		$label['officeTypeArray'] = Yii::t('app', 'Типы платежа');
		$label['statusArray'] = Yii::t('app', 'Статусы');

		return $label;
	}

	public static function getOfficeTypeArray()
	{
		return [
			parent::TYPE_IN_OFFICE => Yii::t('app', 'Пополнение личного счёта'),
			parent::TYPE_OUT_OFFICE => Yii::t('app', 'Снятие с личного счёта'),
		];
	}

	public function initOfficeTypeArray()
	{
		if (empty($this->officeTypeArray)) {
			$this->officeTypeArray = self::getOfficeTypeArray();
			$i = 0;
			foreach($this->officeTypeArray as &$officeType){
				$officeType = $i;
				$i++;
			}
		}else{
			$this->officeTypeArray = array_flip($this->officeTypeArray);
		}
	}

	public function initCurrency()
	{
		if (empty($this->currency)) {
			$currency = Currency::find()->one();
			$this->currency = $currency->id;
		}
	}

	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['id' => 'currency']);
	}

	public function initStatusArray()
	{
		if (empty($this->statusArray)) {
			$this->statusArray = parent::getStatusArray();
			$i = 0;
			foreach($this->statusArray as &$statusTitle){
				$statusTitle = $i;
				$i++;
			}
		}else{
			$this->statusArray = array_flip($this->statusArray);
		}
	}

	public function initDateFromTo()
	{
		$time = time();
		if (empty($this->dateFrom)){
			$this->dateFrom = date('Y-m-d 00:00:00', $time - (60*60*24*30));
		}else{
			$this->dateFrom = date('Y-m-d 00:00:00', strtotime($this->dateFrom));
		}

		if (empty($this->dateTo)){
			$this->dateTo = date('Y-m-d 23:59:59', $time);
		}else{
			$this->dateTo = date('Y-m-d 23:59:59', strtotime($this->dateTo));
		}
	}

	// пополнения/снятия личного кабинета с группировкой по статусам
	public function search($params) {
		$this->load($params);

		$this->initCurrency();

		$this->initOfficeTypeArray();

		$this->initStatusArray();

		$this->initDateFromTo();

		$query = PaymentStatSearch::find();

		$query->select('sum(from_sum) as sumPay, sum(to_sum) as toSumPay, DATE_FORMAT(date_add,"%Y-%m-%d") as `perday`, status, from_pay_system_id, to_pay_system_id');

		$query->where(['>=', 'date_add', $this->dateFrom]);

		$query->andWhere(['<=', 'date_add', $this->dateTo]);

		$whereType = [];
		foreach ($this->officeTypeArray as $officeType => $num) {
			if ($officeType == parent::TYPE_IN_OFFICE){
				$whereType[] = '( to_currency_id = ' . $this->currency . ' AND type = "' . $officeType .'")';
			}elseif ($officeType == parent::TYPE_OUT_OFFICE){
				$whereType[] = '( from_currency_id = ' . $this->currency . ' AND type = "' . $officeType .'")';
			}	
		}

		$whereType = implode(' OR ', $whereType);

		$query->andWhere($whereType);

		$whereStatus = '';
		foreach ($this->statusArray as $statusKey => $statusName) {
			$whereStatus .= 'status = "' . $statusKey . '" OR ';
		}

		$whereStatus = substr($whereStatus, 0, -4);

		$query->andWhere($whereStatus);

		$query->groupBy(['status', 'perday', 'from_pay_system_id', 'to_pay_system_id']);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => false,
			'sort' => array(
				'defaultOrder' => ['date_add' => SORT_DESC],
			),
		]);	

		return $dataProvider;
	}
}