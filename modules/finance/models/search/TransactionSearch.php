<?php

namespace app\modules\finance\models\search;

use app\modules\finance\models\Transaction;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TransactionSearch represents the model behind the search form of `app\modules\finance\models\Transaction`.
 */
class TransactionSearch extends Transaction {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'from_user_id', 'to_user_id', 'currency_id', 'payment_id', 'matrix_id', 'matrix_queue_id'], 'integer'],
			[['type', 'data', 'hash', 'date_add'], 'safe'],
			[['sum'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Transaction::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'from_user_id' => $this->from_user_id,
			'to_user_id' => $this->to_user_id,
			'sum' => $this->sum,
			'currency_id' => $this->currency_id,
			'payment_id' => $this->payment_id,
			'date_add' => $this->date_add,
			'matrix_id' => $this->matrix_id,
			'matrix_queue_id' => $this->matrix_queue_id,
		]);

		$query->andFilterWhere(['like', 'type', $this->type])
			->andFilterWhere(['like', 'data', $this->data])
			->andFilterWhere(['like', 'hash', $this->hash]);

		return $dataProvider;
	}
}
