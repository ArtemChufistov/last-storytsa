<?php

namespace app\modules\finance\controllers;

use app\admin\controllers\AdminController;
use app\modules\finance\models\Payment;
use app\modules\finance\models\search\PaymentSearch;
use app\modules\profile\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentoutController extends AdminController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'view', 'create', 'update', 'delete', 'detail'],
				'rules' => [
					[
						'actions' => ['index', 'view', 'create', 'update', 'delete', 'detail'],
						'allow' => true,
						'roles' => ['administrator'],
					], [
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['paymentIndex'],
					], [
						'actions' => ['view'],
						'allow' => true,
						'roles' => ['paymentView'],
					], [
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['paymentCreate'],
					], [
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['paymentUpdate'],
					], [
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['paymentDelete'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Payment models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new PaymentSearch();
		$searchModel->type = Payment::TYPE_OUT_OFFICE;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionToprocesssystem($id) {
		$payment = Payment::find()->where(['id' => $id])->one();

		$payment->status = Payment::PROCESS_SYSTEM;
		$payment->save(false);

		$this->recalcUserBalanceForPayment($payment);

		echo Yii::t('app', 'Оплата поставлена в обработку системой');
	}

	public function  actionDetailinfo($id) {

	    $payment = Payment::find()->where(['id' => $id])->one();

	    echo $this->renderPartial('detailinfo/_' . strtolower($payment->getToPaySystem()->one()->key), [
	        'payment' => $payment
        ]);
    }

	private function recalcUserBalanceForPayment($payment) {
		if ($payment->from_user_id != '') {
			$fromUser = User::find()->where(['id' => $payment->from_user_id])->one();
			if (!empty($fromUser)) {
				$fromUser->getBalances();
				$fromUser->recalclulateBalances();
			}
		}

		if ($payment->to_user_id != '') {
			$toUser = User::find()->where(['id' => $payment->to_user_id])->one();
			if (!empty($toUser)) {
				$toUser->getBalances();
				$toUser->recalclulateBalances();
			}
		}
	}

	/**
	 * Displays a single Payment model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Payment model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Payment();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$this->recalcUserBalanceForPayment($model);
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Payment model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$this->recalcUserBalanceForPayment($model);
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionUserlist() {
		$data = ['results' => [
			['id' => 97, 'text' => 'user1'],
			['id' => 98, 'text' => 'user2'],
		]];

		return json_encode($data);
	}

	/**
	 * Deletes an existing Payment model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Payment model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Payment the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Payment::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
