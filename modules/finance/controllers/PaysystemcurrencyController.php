<?php

namespace app\modules\finance\controllers;

use app\admin\controllers\AdminController;
use app\modules\finance\models\PaySystemCurrency;
use app\modules\finance\models\search\PaySystemCurrencySearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PaysystemcurrencyController implements the CRUD actions for PaySystemCurrency model.
 */
class PaysystemcurrencyController extends AdminController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'view', 'create', 'update', 'delete'],
				'rules' => [
					[
						'actions' => ['index', 'view', 'create', 'update', 'delete'],
						'allow' => true,
						'roles' => ['administrator'],
					], [
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['paySystemСurrencyIndex'],
					], [
						'actions' => ['view'],
						'allow' => true,
						'roles' => ['paySystemСurrencyView'],
					], [
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['paySystemСurrencyCreate'],
					], [
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['paySystemСurrencyUpdate'],
					], [
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['paySystemСurrencyDelete'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all PaySystemCurrency models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new PaySystemCurrencySearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single PaySystemCurrency model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new PaySystemCurrency model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new PaySystemCurrency();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing PaySystemCurrency model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing PaySystemCurrency model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the PaySystemCurrency model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return PaySystemCurrency the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = PaySystemCurrency::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
