<?php

namespace app\modules\finance\controllers;

use app\admin\controllers\AdminController;
use app\modules\finance\components\paysystem\PoloniexComponent;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyGraphHistory;
use app\modules\finance\models\forms\CurrencyGraphHistoryForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * GraphhistoryController implements the CRUD actions for Payment model.
 */
class GraphhistoryController extends AdminController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index'],
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['administrator'],
					], [
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['graphpoloniexIndex'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all CurrencyGraphHistoryForm models.
	 * @return mixed
	 */
	public function actionIndex() {

		$model = new CurrencyGraphHistoryForm;
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {

			$graphInfo = [];
			if ($model->exchange == CurrencyGraphHistoryForm::EXCHANGE_POLONIEX) {

				$poloniexComp = new PoloniexComponent();
				$poloniexInfo = $poloniexComp->returnChartData(
					$model->getFromCurrency()->one()->key . '_' . $model->getToCurrency()->one()->key,
					strtotime($model->date_from . ' 00:00:00'),
					strtotime($model->date_to . ' 00:00:00'),
					$model->breakdown_type
				);

				foreach ($poloniexInfo as $poloniexItem) {
					$graphInfo[] = [
						'date' => $poloniexItem['date'],
						'value' => $poloniexItem['high'],
					];
				}
			}

			$counter = 0;
			foreach ($graphInfo as $item) {
				$date = date('Y-m-d H:i:s', $item['date']);
				$graphHistory = CurrencyGraphHistory::find()
					->where(['from_currency_id' => $model->from_currency_id])
					->andWhere(['to_currency_id' => $model->to_currency_id])
					->andWhere(['date' => $date])
					->one();
				if (empty($graphHistory)) {

					$graphHistory = new CurrencyGraphHistory;
					$graphHistory->from_currency_id = $model->from_currency_id;
					$graphHistory->to_currency_id = $model->to_currency_id;
					$graphHistory->course = $item['value'];
					$graphHistory->date = $date;

					$currencyUSDT = Currency::find()->where(['key' => Currency::KEY_USDT])->one();

					if ($model->from_currency_id == $currencyUSDT->id) {
						$graphHistory->course_to_usd = $item['value'];
					} else {
						$graphHistoryBTCUSDT = CurrencyGraphHistory::find()
							->where(['from_currency_id' => $currencyUSDT->id])
							->andWhere(['to_currency_id' => $model->from_currency_id])
							->andWhere(['date' => $date])
							->one();
						if (!empty($graphHistoryBTCUSDT)) {
							$graphHistory->course_to_usd = $graphHistoryBTCUSDT->course_to_usd * $graphHistory->course;
						}
					}

					$graphHistory->save();
					$counter++;
				}
			}

			$model->addError('breakdown_type', Yii::t('app', 'График успешно залит, всего добавлено: {count}', ['count' => $counter]));
		}
		return $this->render('index', [
			'model' => $model,
		]);
	}
}
