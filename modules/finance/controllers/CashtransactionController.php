<?php

namespace app\modules\finance\controllers;

use Yii;
use yii\filters\AccessControl;
use app\admin\controllers\AdminController;
use app\modules\finance\models\CashTransaction;
use app\modules\finance\models\search\CashTransactionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\finance\models\Payment;
use app\modules\finance\models\PaySystem;

/**
 * CashtransactionController implements the CRUD actions for CashTransaction model.
 */
class CashtransactionController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ], [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['cashtransactionIndex'],
                    ], [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['cashtransactionView'],
                    ], [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['cashtransactionCreate'],
                    ], [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['cashtransactionUpdate'],
                    ], [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['cashtransactionDelete'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CashTransaction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CashTransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CashTransaction model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CashTransaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CashTransaction();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CashTransaction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $payment = $model->getPayment()->one();

            if ($payment->getToPaySystem()->one()->key == PaySystem::KEY_CASH){
                $payment->status = Payment::PROCESS_SYSTEM;
                $payment->save(false);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CashTransaction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CashTransaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CashTransaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CashTransaction::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
