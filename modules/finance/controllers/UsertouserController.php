<?php

namespace app\modules\finance\controllers;

use app\admin\controllers\AdminController;
use app\modules\finance\models\search\TransactionSearch;
use app\modules\finance\models\Transaction;
use app\modules\profile\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UsertouserController implements the CRUD actions for Transaction model.
 */
class UsertouserController extends AdminController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'view', 'create', 'update', 'delete'],
				'rules' => [
					[
						'actions' => ['index', 'view', 'create', 'update', 'delete'],
						'allow' => true,
						'roles' => ['administrator'],
					], [
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['usertouserIndex'],
					], [
						'actions' => ['view'],
						'allow' => true,
						'roles' => ['usertouserView'],
					], [
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['usertouserCreate'],
					], [
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['usertouserUpdate'],
					], [
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['usertouserDelete'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Transaction models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new TransactionSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionRecalculatebalance() {
		$userId = $_GET['userId'];

		$user = User::find()->where(['id' => $userId])->one();

		$user->recalclulateBalances();
	}

	/**
	 * Displays a single Transaction model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Transaction model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Transaction();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			if (!empty($model->to_user_id)) {
				$user = User::find()->where(['id' => $model->to_user_id])->one();
			} else {
				$user = User::find()->where(['id' => $model->from_user_id])->one();
			}
			$user->recalclulateBalances();
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Transaction model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			if (!empty($model->to_user_id)) {
				$user = User::find()->where(['id' => $model->to_user_id])->one();
			} else {
				$user = User::find()->where(['id' => $model->from_user_id])->one();
			}
			$user->recalclulateBalances();
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Transaction model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Transaction model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Transaction the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Transaction::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
