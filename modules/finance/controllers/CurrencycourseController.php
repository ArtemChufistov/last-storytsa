<?php

namespace app\modules\finance\controllers;

use app\admin\controllers\AdminController;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\search\CurrencyCourseSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CurrencycourseController implements the CRUD actions for CurrencyCourse model.
 */
class CurrencycourseController extends AdminController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'view', 'create', 'update', 'delete'],
				'rules' => [
					[
						'actions' => ['index', 'view', 'create', 'update', 'delete'],
						'allow' => true,
						'roles' => ['administrator'],
					], [
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['currencycourseIndex'],
					], [
						'actions' => ['view'],
						'allow' => true,
						'roles' => ['currencycourseView'],
					], [
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['currencycourseCreate'],
					], [
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['currencycourseUpdate'],
					], [
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['currencycourseDelete'],
					],
				],
			],
		];
	}

	/**
	 * Lists all CurrencyCourse models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new CurrencyCourseSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single CurrencyCourse model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new CurrencyCourse model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new CurrencyCourse();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing CurrencyCourse model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing CurrencyCourse model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the CurrencyCourse model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return CurrencyCourse the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = CurrencyCourse::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
