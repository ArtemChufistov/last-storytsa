<?php

namespace app\modules\finance\controllers;

use app\modules\finance\models\Currency;
use app\modules\finance\models\TopexchangeTransaction;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\AdvcashTransaction;
use app\modules\finance\models\PerfectmoneyTransaction;
use app\modules\mainpage\models\Preference;
use Yii;
use yii\web\Controller;

class BackpayController extends Controller {

	public function beforeAction($action) {
		Yii::$app->request->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	// приёмник информации об оплате с сервера Perfect Money
	public function actionPerfectmoney() {
		if (!in_array($_SERVER['REMOTE_ADDR'], array('78.41.203.75'))) {
			return false;
		}

		$pmTransaction = new PerfectmoneyTransaction;

		if (!empty($_POST['ERROR'])) {
			$pmTransaction->v2_hash = print_r($_POST, true);
			$pmTransaction->save(false);
			return false;
		}

		$paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_PERFECTMONEY])->one();

		if ($_POST['PAYMENT_UNITS'] == 'USD') {
			$currency = Currency::find()->where(['key' => Currency::KEY_USD])->one();
			$preferencePayeeAcc = Preference::find()->where(['key' => Preference::KEY_PERF_MON_PAYEE_ACCOUNT_USD])->one();
		} elseif ($_POST['PAYMENT_UNITS'] == 'EUR') {
			$currency = Currency::find()->where(['key' => Currency::KEY_EUR])->one();
			$preferencePayeeAcc = Preference::find()->where(['key' => Preference::KEY_PERF_MON_PAYEE_ACCOUNT_EUR])->one();
		} else {
			return false;
		}

		$secret = Preference::find()->where(['key' => Preference::KEY_PERF_MON_SECRET])->one();

		$passPhrase = strtoupper(md5($secret->value));

		$passResult = strtoupper(md5(
			$_POST['PAYMENT_ID'] . ':' .
			$preferencePayeeAcc->value . ':' .
			$_POST['PAYMENT_AMOUNT'] . ':' .
			$_POST['PAYMENT_UNITS'] . ':' .
			$_POST['PAYMENT_BATCH_NUM'] . ':' .
			$_POST['PAYER_ACCOUNT'] . ':' .
			$passPhrase . ':' .
			$_POST['TIMESTAMPGMT']));

		if ($_POST['V2_HASH'] != $passResult) {
			return false;
		}

		$pmTransaction->payment_id = $_POST['PAYMENT_ID'];
		$pmTransaction->payee_account = $preferencePayeeAcc->value;
		$pmTransaction->amount = $_POST['PAYMENT_AMOUNT'];
		$pmTransaction->units = $_POST['PAYMENT_UNITS'];
		$pmTransaction->batch_num = $_POST['PAYMENT_BATCH_NUM'];
		$pmTransaction->payeer_account = $_POST['PAYER_ACCOUNT'];
		$pmTransaction->timestamp = $_POST['TIMESTAMPGMT'];
		$pmTransaction->v2_hash = $_POST['V2_HASH'];
		$pmTransaction->save(false);
	}

	// приёмник информации об оплате с сервера Top-exchange
	public function actionTopexchange() {

		$prefExchangePassword = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_PASSWORD])->one();

		$merchantPass = $prefExchangePassword->value;

		$postHash = $_POST['verificate_hash'];

		unset($_POST['verificate_hash']);

		$myHash = "";

		foreach ($_POST AS $key_post => $onePost) {
			if ($myHash == "") {$myHash = $onePost;} else {
				$myHash = $myHash . "::" . $onePost;
			}
		}

		$myHash = $myHash . "::" . $merchantPass;


		$myHash = hash("sha256", $myHash);

		if ($myHash == $postHash) {

			$exchTransaction = new TopexchangeTransaction;
			$exchTransaction->merchant_name = $_POST['merchant_name'];
			$exchTransaction->merchant_title = $_POST['merchant_title'];
			$exchTransaction->payed_paysys = $_POST['payed_paysys'];
			$exchTransaction->amount = $_POST['amount'];
			$exchTransaction->payment_info = $_POST['payment_info'];
			$exchTransaction->payment_num = $_POST['payment_num'];
			$exchTransaction->sucess_url = $_POST['sucess_url'];
			$exchTransaction->error_url = $_POST['error_url'];
			$exchTransaction->obmen_order_id = $_POST['obmen_order_id'];
			$exchTransaction->obmen_recive_valute = $_POST['obmen_recive_valute'];
			$exchTransaction->obmen_timestamp = date('Y-m-d H:i:s', $_POST['obmen_timestamp']);
			$exchTransaction->save(false);
		}
	}

	// приёмник информации об оплате с сервера AsvCash
	public function actionAdvcash(){
        $prefSciSercet = Preference::find()->where(['key' => Preference::KEY_SCI_SECRET])->one();

        $key =
            $_REQUEST['ac_transfer'] . ':' .
            $_REQUEST['ac_start_date'] . ':' .
            $_REQUEST['ac_sci_name'] . ':' .
            $_REQUEST['ac_src_wallet'] . ':' .
            $_REQUEST['ac_dest_wallet'] . ':' .
            $_REQUEST['ac_order_id'] . ':' .
            $_REQUEST['ac_amount'] . ':' .
            $_REQUEST['ac_merchant_currency'] . ':' . 
            $prefSciSercet->value;

        $key = hash('sha256', $key);

        if ($key != $_REQUEST['ac_hash']){
            exit('ошибка платежа');
        }

        $advcashTransaction = new AdvcashTransaction;
        $advcashTransaction->ac_transfer = $_REQUEST['ac_transfer'];
        $advcashTransaction->ac_start_date = $_REQUEST['ac_start_date'];
        $advcashTransaction->ac_sci_name = $_REQUEST['ac_sci_name'];
        $advcashTransaction->ac_src_wallet = $_REQUEST['ac_src_wallet'];
        $advcashTransaction->ac_dest_wallet = $_REQUEST['ac_dest_wallet'];
        $advcashTransaction->ac_order_id = $_REQUEST['ac_order_id'];
        $advcashTransaction->ac_amount = $_REQUEST['ac_amount'];
        $advcashTransaction->amount = $_REQUEST['ac_amount'];
        $advcashTransaction->ac_merchant_currency = $_REQUEST['ac_merchant_currency'];
        $advcashTransaction->save(false);
    }

	// приёмник информации об оплате с сервера Payeer
	public function actionPayeer(){
	}

	// график курсов
	public function actionGraph() {
		echo '';
	}
}
