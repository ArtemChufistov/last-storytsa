<?php

namespace app\modules\finance\controllers;

use app\admin\controllers\AdminController;
use app\modules\finance\models\forms\UserPayWalletCoinbaseAdminForm;
use app\modules\finance\models\search\UserPayWalletCoinbaseSearch;
use app\modules\finance\models\UserPayWalletCoinbase;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UserpaywalletcoinbaseController implements the CRUD actions for UserPayWalletCoinbase model.
 */
class UserpaywalletcoinbaseController extends AdminController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'view', 'create', 'update', 'delete'],
				'rules' => [
					[
						'actions' => ['index', 'view', 'create', 'update', 'delete'],
						'allow' => true,
						'roles' => ['administrator'],
					], [
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['userPayWalletCoinbaseIndex'],
					], [
						'actions' => ['view'],
						'allow' => true,
						'roles' => ['userPayWalletCoinbaseView'],
					], [
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['userPayWalletCoinbaseCreate'],
					], [
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['userPayWalletCoinbaseUpdate'],
					], [
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['userPayWalletCoinbaseDelete'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all UserPayWalletCoinbaseSearch models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new UserPayWalletCoinbaseSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single UserPayWalletCoinbaseAdminForm model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new UserPayWalletCoinbaseAdminForm model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new UserPayWalletCoinbaseAdminForm();

		if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing UserPayWalletCoinbaseAdminForm model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing UserPayWalletCoinbaseAdminForm model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the UserPayWalletCoinbaseAdminForm model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return UserPayWalletCoinbaseAdminForm the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = UserPayWalletCoinbaseAdminForm::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
