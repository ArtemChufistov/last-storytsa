<?php
namespace app\modules\finance\widgets;

use app\modules\profile\models\User;
use yii\base\Widget;

class PayWalletPasswordWidget extends Widget {

	public $user;

	public function init() {
	}

	public function run() {

		$message = '';
		if (!empty(\Yii::$app->request->post('send-pay-wallet'))) {

			$time = time() - strtotime($this->user->last_date_pay_password);
			if ($time > User::LAST_DATE_EMAIL_CONFIRM_TOKEN_EXPIRE) {
				$this->user->sendPayPassword();
				$message = \Yii::t('app', 'На E-mail {email} отправлен платёжный пароль', ['email' => $this->user->email]);
			} else {
				$message = \Yii::t('app', 'Повторная отправка пароля возможна через {sec} секунд', ['sec' => User::LAST_DATE_EMAIL_CONFIRM_TOKEN_EXPIRE - $time]);
			}
		}

		return \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/office/widgets/_pay-wallet-password', [
			'user' => $this->user,
			'message' => $message,
		]);
	}
}
?>