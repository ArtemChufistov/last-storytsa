<?php

namespace app\modules\finance\components;

use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;

class FinanceComponent {
	// взять валюты, которыми можно пополнять ЛК
	public static function getInLkCurrencies() {
		$paySystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();

		$currCourseArray = CurrencyCourse::find()
			->select('from_currency')
			->distinct()
			->where(['to_pay_system_id' => $paySystemLk->id])
			->andWhere(['active' => CurrencyCourse::ACTIVE_TRUE])
			->all();

		$currencyArray = [];
		foreach ($currCourseArray as $currCourse) {
			$currencyArray[] = $currCourse->getFromCurrency()->one();
		}

		return $currencyArray;
	}

	// взять валюты, в которые можно вывези из ЛК
	public static function getOutLkCurrencies() {
		$paySystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();

		$currCourseArray = CurrencyCourse::find()
			->select('to_currency')
			->distinct()
			->where(['from_pay_system_id' => $paySystemLk->id])
			->andWhere(['active' => CurrencyCourse::ACTIVE_TRUE])
			->all();

		$currencyArray = [];
		foreach ($currCourseArray as $currCourse) {
			$currencyArray[] = $currCourse->getToCurrency()->one();
		}

		return $currencyArray;
	}
}