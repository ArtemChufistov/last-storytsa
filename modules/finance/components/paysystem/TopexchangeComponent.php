<?php

namespace app\modules\finance\components\paysystem;

use app\modules\mainpage\models\Preference;
use yii\httpclient\Client;
use app\modules\finance\components\paysystem\additional\ExchangeApi;

class TopexchangeComponent {

    const API_URL = 'https://top-exchange.com';
    const INTERFACE_LANG = 'ru';

	// получение списка валют и курсов
	public static function getCurrencyCourseList() {
		$prefTopxchangeName = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_NAME])->one();
		$url = self::API_URL . '/obmen/get_merchant_cources/' . $prefTopxchangeName->value;
		$currencyCourseList = json_decode(file_get_contents($url), true);

		return $currencyCourseList;
	}

	// взять  данные для параметра send_paysys_identificator
	public static function getCourseListForPaySysIdent($send_paysys_identificator = '') {
		$currencyList = self::getCurrencyCourseList();

		foreach ($currencyList as $topexchangeCurrency) {

			if (!empty($send_paysys_identificator) && $topexchangeCurrency['send_paysys_identificator'] == $send_paysys_identificator) {
				return $topexchangeCurrency;
			}
		}

		return [];
	}

	// Взять баланс аккаунта со списком возможных выводов
	public static function getAccountBalances() {
        $prefLogin = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_LOGIN])->one();
        $prefPass = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_PASS])->one();

        $exchangeApi = new ExchangeApi();
        $exchangeApi->setup($prefLogin->value, $prefPass->value, self::API_URL);

        $data = $exchangeApi->get_balances(self::INTERFACE_LANG);

        if (!empty($data) && $data['is_ok'] == 'ok') {
            return $data['data']['balances'];
        }else{
            return [];
        }
    }

    public static function makeWithdraw($fields, $reciveCurrencyId, $apiTest = 0) {
        $prefLogin = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_LOGIN])->one();
        $prefPass = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_PASS])->one();
        $prefSendCurrency = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_SEND_CURRENCY])->one();

        $exchangeApi = new ExchangeApi();
        $exchangeApi->setup($prefLogin->value, $prefPass->value, self::API_URL);

        $data = $exchangeApi->make_withdraw($fields['withdraw_summ'], 0, $prefSendCurrency->value, $reciveCurrencyId, '', $fields, self::INTERFACE_LANG, $apiTest);

        return $data;
    }

    // Создать вывод

    // Взять историю транзакций
    public static function getTransactionHistory() {
        $client = new Client();

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl(self::API_URL . '/user_api/get_transactions_history')
            ->setData([
                'login' => $prefTopxchangeLogin->value,
                'api_pass' => $prefTopxchangePass->value,
                'date_start' => date('Y-m-d H:i:s'),
                'date_end' => date('Y-m-d H:i:s'),
                'page_num' => 1,
                'records_limit' => 500,
            ])->send();

        $data = $response->getData();
    }
}