<?php

namespace app\modules\finance\components\paysystem;

use app\modules\mainpage\models\Preference;
use app\modules\finance\models\Currency;
use yii\httpclient\Client;
use yii\web\HttpException;
use Yii;

class CrypchantComponent {

    const CURRENCY_BITCOIN = 'bitcoin';
    const CURRENCY_LITECOIN = 'litecoin';
    const CURRENCY_BITCOIN_CASH_ABC = 'bitcoin-cash-abc';
    const CURRENCY_BITCOIN_CASH_SV = 'bitcoin-cash-sv';
    const CURRENCY_DOGECOIN = 'dogecoin';
    const CURRENCY_ETHEREUM = 'ethereum';
    const CURRENCY_MONERO = 'monero';

    // Сгенерировать адрес кошелька для пополнения
    public function getInPaymentAddress($currency) {

        $cmCurrency = self::getCrypchantCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'generateAddress',
        ];

        $resp = self::send($options);

        return $resp['data']['newAaddress'];
    }

    // Получить все транзакции по аккаунту
    public function getAccountTransactions($currency, $count = 100) {
        $cmCurrency = self::getCrypchantCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getTransactionsList',
            "count" => $count,
            "skip" => 0,
            "watchOnly" => 'true'
        ];

        $resp = self::send($options);

        return $resp['data']['transactionsList'];
    }

    // Получить адреса аккаунта
    public function getAccountAddress($currency) {
        $cmCurrency = self::getCrypchantCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getAddress',
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Получить информацию по транзакции
    public function getTxidInfo($currency, $txId) {
        $cmCurrency = self::getCrypchantCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getTxidInfo',
            "txid" => $txId
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Получить транзакции по кошельку
    public function getTxidsByAddress($currency, $address) {
        $cmCurrency = self::getCrypchantCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getTxidsByAddress',
            "address" => $address,
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Баланс аккаунта
    public function getAccountBalance($currency, $confirmations = 1) {
        $cmCurrency = self::getCrypchantCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getCommonWalletBalance',
            "confirmations_num" => $confirmations,
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Баланс кошелька
    public function getAddressBalance($currency, $address, $confirmations = 1) {
        $cmCurrency = self::getCrypchantCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getBalanceByAddress',
            "address" => $address,
            "confirmations_num" => $confirmations,
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Совершить транзакцию
    public function sendTransaction($currency, $toAddress, $sum) {
        $cmCurrency = self::getCrypchantCurrency($currency, $toAddress, $sum);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'send',
            "toAddress" => $toAddress,
            "amount" => $sum,
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Post запрос
    private static function send($options) {
        $merchantId = Preference::find()->where(['key' => Preference::KEY_CRYPCHANT_API_ID])->one()->value;
        $secret = Preference::find()->where(['key' => Preference::KEY_CRYPCHANT_API_SECRET])->one()->value;

        $request = [
            "merchantId" => $merchantId,
            "secret" => $secret,
            "options" => $options
        ];

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('https://crypchant.com/api/v1')
            ->setData($request)
            ->send();

        $response = json_decode($response->content, true);

        if ($response['isOk'] == true){
            return $response;
        }else{
            throw new \Exception("Crypchant api error" . print_r($response, true), 1);
        }
    }

    private static function getCrypchantCurrency($currency) {
        switch ($currency) {
            case Currency::KEY_BTC:
                return self::CURRENCY_BITCOIN;
                break;

            case Currency::KEY_LTC:
                return self::CURRENCY_LITECOIN;
                break;

            case Currency::KEY_BTC_ABC:
                return self::CURRENCY_BITCOIN_CASH_ABC;
                break;

            case Currency::KEY_BTC_SV:
                return self::CURRENCY_BITCOIN_CASH_SV;
                break;

            case Currency::KEY_DOGE:
                return self::CURRENCY_DOGECOIN;
                break;

            case Currency::KEY_XMR:
                return self::CURRENCY_MONERO;
                break;
        }
    }
}