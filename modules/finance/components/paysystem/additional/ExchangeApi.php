<?php
namespace app\modules\finance\components\paysystem\additional;
/*
	CoinPayments.net API Class - v1.1
	Copyright 2014-2018 CoinPayments.net. All rights reserved.
	License: GPLv2 - http://www.gnu.org/licenses/gpl-2.0.txt
*/

class ExchangeApi {
    private $api_name = '';
    private $api_pass = '';
    private $exchange_url = '';

    public function setup($api_name, $api_pass, $exchange_url) {
        $this->api_name = $api_name;
        $this->api_pass = $api_pass;
        $this->exchange_url = $exchange_url;
    }

    private function is_setup() {
        return (!empty($this->api_name) && !empty($this->api_pass) && !empty($this->exchange_url));
    }

    /**
     * Getting the balances of your account, as well as the list of payment systems where these balances can be withdraw.<br />
     * @param interface_lang [Optional]: Language request. Values "en" OR "ru".
     */
    public function get_balances($interface_lang="en") {
        $url = $this->exchange_url."/user_api/get_balances";

        $req = array(
            'interface_lang' => $interface_lang,
        );

        return $this->api_call($url, $req);
    }

    /**
     * Getting transaction history on your account.<br />
     * @param date_start: History Start Date.
     * @param date_end: History End Date.
     * @param page_num [Optional]: for page navigation. Number of page. Default value: 1.
     * @param records_limit [Optional]: for page navigation. Count of records per page. Default value: 100, Max value: 100
     * @param operation_type [Optional]: only 'all' Or 'merchant_pay' Or 'api_withdraw' Or 'withdraw_money' need set.
     * @param interface_lang [Optional]: Language request. Values "en" OR "ru".
     */
    public function get_transactions_history($date_start, $date_end, $page_num = 1, $records_limit = 100, $operation_type="all", $interface_lang="en") {
        $url = $this->exchange_url."/user_api/get_transactions_history";

        $req = array(
            'interface_lang' => $interface_lang,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'page_num' => $page_num,
            'records_limit' => $records_limit,
            'operation_type' => $operation_type,
        );

        return $this->api_call($url, $req);
    }

    /**
     * Obtaining statuses of transactions put to output.<br />
     * @param trans_free_text [Optional]: if need search transaction by trans_free_text.
     * @param only_this_api_name [Optional]: if you want to get all the entries in your account or if you need to get only entries with the passed api_name. Default value: 1.
     * @param date_start [Optional]: default value: current date minus one year.
     * @param date_end [Optional]: default value: current date.
     * @param page_num [Optional]: for page navigation. Number of page. Default value: 1.
     * @param records_limit [Optional]: for page navigation. Count of records per page. Default value: 500, Max value: 500.
     */
    public function get_api_withdrawal_status($trans_free_text="", $only_this_api_name=1, $date_start="", $date_end="", $page_num = 1, $records_limit = 100) {
        $url = $this->exchange_url."/user_api/get_api_withdrawal_status";

        $req = array(
            'only_this_api_name' => $only_this_api_name,
            'trans_free_text' => $trans_free_text,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'page_num' => $page_num,
            'records_limit' => $records_limit,
        );

        return $this->api_call($url, $req);
    }


    /**
     * Creating an output operation.<br />
     * @param withdraw_summ: amount withdrawn from your balance.
     * @param recive_summ: The amount received on the wallet as a result of withdrawal.
     * @param send_currency_id: ID of your balance from where funds will be withdrawn.
     * @param recive_currency_id: ID of the payment system where funds will be sended.
     * @param trans_free_text [Optional]: the operation number in your system. Field Type: string (100).
     * @param transfer_fields: array of transfer_fields: recive_wallet, user_email, user_phone etc....
     * @param interface_lang [Optional]: Language request. Values "en" OR "ru".
     * @param api_test [Optional]: Transaction is test. Values "0" OR "1".
     */
    public function make_withdraw($withdraw_summ = 0, $recive_summ = 0, $send_currency_id, $recive_currency_id, $trans_free_text="", $transfer_fields, $interface_lang="en", $api_test=0) {
        $url = $this->exchange_url."/user_api/make_withdraw";

        $req = array(
            'send_currency_id' => $send_currency_id,
            'recive_currency_id' => $recive_currency_id,
            'trans_free_text' => $trans_free_text,
            'interface_lang' => $interface_lang,
            'api_test' => $api_test,
        );

        //Add transfer_fields: recive_wallet, user_email, user_phone etc...
        foreach($transfer_fields AS $key=>$one) $req[$key] = $one;

        //Only one of summs need set
        if ($withdraw_summ > 0 && $recive_summ <= 0) $req['withdraw_summ'] = $withdraw_summ;
        if ($recive_summ > 0) $req['recive_summ'] = $recive_summ;

        return $this->api_call($url, $req);
    }

    private function api_call($url, $req = array()) {
        if (!$this->is_setup()) {
            return array('error' => 'You have not called the Setup function with your private and public keys!');
        }

        //Set the API name
        $req['api_name'] = $this->api_name;
        $req['unix_time'] = time();

        //Generate the query string
        $post_data = http_build_query($req, '', '&');

        //Calculate the HMAC signature on the POST data
        $hmac = hash_hmac('sha512', $post_data, $this->api_pass);

        //Send API call to Exchange
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('HMAC: '.$hmac));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        $result = array("error"=>"", "is_ok"=>"");
        $data = curl_exec($ch);

        if (curl_errno($ch)!=0) {
            $result['error'] = "Curl error (errno: ".curl_errno($ch).') '.curl_error($ch);
            $result['is_ok'] = "error";
        }

        if ($result['is_ok'] == "") {
            $decode_data = json_decode($data, TRUE);

            if ($decode_data !== NULL && count($decode_data)) {
                $result = $decode_data;
            } else {
                $result['error'] = 'Unable to parse JSON result (errno: '.json_last_error().')';
                $result['is_ok'] = "error";

                //If you are using PHP 5.5.0 or higher you can use json_last_error_msg() for a better error message
                $php_version = explode('.', PHP_VERSION);
                $php_version = $php_version[0] * 10000 + $php_version[1] * 100 + $php_version[2];
                if ($php_version > 50500) $result['error'] = $result['error'].": ".json_last_error_msg();
            }
        }

        curl_close($ch);
        return $result;
    }
};
