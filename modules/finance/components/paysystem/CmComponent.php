<?php

namespace app\modules\finance\components\paysystem;

use app\modules\mainpage\models\Preference;
use app\modules\finance\models\Currency;
use yii\httpclient\Client;
use yii\web\HttpException;
use Yii;

class CmComponent {

	const CURRENCY_LITECOIN = 'litecoin';
	const CURRENCY_BITCOIN = 'bitcoin';

	// Сгенерировать адрес кошелька для пополнения
	public function getInPaymentAddress($currency) {

		$cmCurrency = self::getcmCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'generateAddress',
        ];

        $resp = self::send($options);

        return $resp['data']['newAaddress'];
	}

    // Получить все транзакции по аккаунту
    public function getAccountTransactions($currency, $count = 100) {
        $cmCurrency = self::getcmCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getTransactionsList',
            "count" => $count,
            "skip" => 0,
            "watchOnly" => 'true'
        ];

        $resp = self::send($options);

        return $resp['data']['transactionsList'];
    }

    // Получить адреса аккаунта
    public function getAccountAddress($currency) {
        $cmCurrency = self::getcmCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getAddress',
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Получить информацию по транзакции
    public function getTxidInfo($currency, $txId) {
        $cmCurrency = self::getcmCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getTxidInfo',
            "txid" => $txId
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Получить транзакции по кошельку
    public function getTxidsByAddress($currency, $address) {
        $cmCurrency = self::getcmCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getTxidsByAddress',
            "address" => $address,
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Баланс аккаунта
    public function getAccountBalance($currency, $confirmations = 1) {
        $cmCurrency = self::getcmCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getCommonWalletBalance',
            "confirmations_num" => $confirmations,
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Баланс кошелька
    public function getAddressBalance($currency, $address, $confirmations = 1) {
        $cmCurrency = self::getcmCurrency($currency);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'getBalanceByAddress',
            "address" => $address,
            "confirmations_num" => $confirmations,
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Совершить транзакцию
    public function sendTransaction($currency, $toAddress, $sum) {
        $cmCurrency = self::getcmCurrency($currency, $toAddress, $sum);

        $options = [
            "currency" => $cmCurrency,
            "command" => 'send',
            "toAddress" => $toAddress,
            "amount" => $sum,
        ];

        $resp = self::send($options);

        return $resp;
    }

    // Post запрос
    private static function send($options) {
		$merchantId = Preference::find()->where(['key' => Preference::KEY_CM_API_ID])->one()->value;
		$secret = Preference::find()->where(['key' => Preference::KEY_CM_API_SECRET])->one()->value;

        $request = [
            "merchantId" => $merchantId,
            "secret" => $secret,
            "options" => $options
        ];

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('http://crypchant.eva.technology/api/v1')
            ->setData($request)
            ->send();

        $response = json_decode($response->content, true);

        if ($response['isOk'] == true){
        	return $response;
        }else{
        	throw new \Exception("cm api error" . print_r($response, true), 1);
        }
    }

    private static function getcmCurrency($currency) {
    	switch ($currency) {
    		case Currency::KEY_BTC:
    			return self::CURRENCY_BITCOIN;
    			break;
    		
    		case Currency::KEY_LTC:
    			return self::CURRENCY_LITECOIN;
    			break;
    	}
    }
}