<?php

namespace app\modules\finance\components\paysystem;

use app\modules\finance\models\Currency;
use app\modules\mainpage\models\Preference;

class PerfectMoneyComponent {
	// запрос баланса
	public function getBalance() {
		$accountId = Preference::find()->where(['key' => Preference::KEY_PERF_MON_ACCOUND_ID])->one()->value;
		$passPhrase = Preference::find()->where(['key' => Preference::KEY_PERF_MON_SECRET])->one()->value;

		$f = fopen('https://perfectmoney.is/acct/balance.asp?AccountID=' . $accountId . '&PassPhrase=' . $passPhrase, 'rb');

		if ($f === false) {
			echo 'error openning url';
		}

		// getting data
		$out = array();
		$out = "";
		while (!feof($f)) {
			$out .= fgets($f);
		}

		fclose($f);

		// searching for hidden fields
		if (!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $out, $result, PREG_SET_ORDER)) {
			echo 'Ivalid output';
			exit;
		}

		// putting data to array
		$ar = "";
		foreach ($result as $item) {
			$key = $item[1];
			$ar[$key] = $item[2];
		}

		return $ar;
	}

	// отправка средств
	public function sendFunds($wallet, $currencyKey, $amount, $paymentId) {
		$password = Preference::find()->where(['key' => Preference::KEY_PERF_MON_PASSWORD])->one()->value;
		$accountId = Preference::find()->where(['key' => Preference::KEY_PERF_MON_ACCOUND_ID])->one()->value;

		if ($currencyKey == Currency::KEY_USD) {
			$preferencePayeeAcc = Preference::find()->where(['key' => Preference::KEY_PERF_MON_PAYEE_ACCOUNT_USD])->one();
		} elseif ($currencyKey == Currency::KEY_EUR) {
			$preferencePayeeAcc = Preference::find()->where(['key' => Preference::KEY_PERF_MON_PAYEE_ACCOUNT_EUR])->one();
		} else {
			throw new Exception("Error currency perfect money");
			return;
		}

		$getString = 'https://perfectmoney.is/acct/confirm.asp?AccountID=' . $accountId . '&PassPhrase=' . $password . '&Payer_Account=' . $preferencePayeeAcc->value . '&Payee_Account=' . $wallet . '&Amount=' . $amount . '&PAY_IN=1&PAYMENT_ID=' . $paymentId;

		$f = fopen($getString, 'rb');

		if ($f === false) {
			return array('ERROR' => 'error openning url');
		}

		// getting data
		$out = array();
		$out = "";
		while (!feof($f)) {
			$out .= fgets($f);
		}

		fclose($f);

		if (!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $out, $result, PREG_SET_ORDER)) {
			return array('ERROR' => 'Ivalid output');
		}

		$ar = "";
		foreach ($result as $item) {
			$key = $item[1];
			$ar[$key] = $item[2];
		}

		return $ar;
	}
}