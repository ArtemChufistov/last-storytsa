<?php

namespace app\modules\finance\components\paysystem;

use app\modules\mainpage\models\Preference;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Resource\Transaction;
use Coinbase\Wallet\Value\Money;

class CoinBaseComponent {

	const CURRENCY_BTC = 'BTC';
	const CURRENCY_LTC = 'LTC';
	const CURRENCY_ETH = 'EHT';

	// взять адрес кошелька для пополнения
	public function getInPaymentAddress($addressName = '', $currency) {
		extract(self::getAccountClientForCurrency($currency));

		$address = new Address(['name' => $addressName]);

		$client->createAccountAddress($account, $address);

		return $address;
	}

	// информация по адресу кошелька
	public function getAddressTransactions($address, $walletId, $currency) {
		extract(self::getAccountClientForCurrency($currency));

		$address = $client->getAccountAddress($account, $walletId);

		$transactions = $client->getAddressTransactions($address);

		return $transactions;
	}

	// отправка средств на кошелек
	public function sendFunds($wallet, $sum, $currency, $description = '') {

		extract(self::getAccountClientForCurrency($currency));

		$transaction = Transaction::send([
			'toBitcoinAddress' => $wallet,
			'amount' => new Money($sum, $currency),
			'description' => $description,
		]);

		$client->createAccountTransaction($account, $transaction);

		$tranData = $transaction->getRawData();

		// ждем обработки сетью
		while ($tranData['network']['status'] == 'pending') {
			$client->refreshTransaction($transaction);
			$tranData = $transaction->getRawData();
			sleep(1);
		}

		return $transaction;
	}

	// получить аккаунт, клиент от коинбейза для валюты
	public static function getAccountClientForCurrency($currency = 'BTC') {
		$apiKey = Preference::find()->where(['key' => Preference::KEY_COINBASE_API_KEY])->one()->value;
		$apiSecret = Preference::find()->where(['key' => Preference::KEY_COINBASE_API_SECRET])->one()->value;

		$configuration = Configuration::apiKey($apiKey, $apiSecret);
		$client = Client::create($configuration);

		foreach ($client->getAccounts() as $account) {
			if ($account->getCurrency() == $currency) {
				return ['account' => $account, 'client' => $client];
			}
		}

		throw new \Exception("No currency for coinbase account");
	}

	// запрос баланса
	public function getBalance() {
	}
}