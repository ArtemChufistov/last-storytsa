<?php

namespace app\modules\finance\components\paysystem;

class LivecoinComponent {
	// данные по торгам за последние 24 часа
	public function getticker($fromCurrency, $toCurrency = 'USD') {
		$url = "https://api.livecoin.net/exchange/ticker";

		$params = array('currencyPair' => $fromCurrency . '/' . $toCurrency);

		$fields = http_build_query($params, '', '&');

		$ch = curl_init($url . "?" . $fields);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if ($statusCode != 200) {
			throw new \Exception("Status code: $statusCode, response: $response", 1);
		}

		return json_decode($response, true);
	}
}