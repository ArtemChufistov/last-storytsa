<?php

namespace app\modules\finance\components\paysystem;

use app\modules\finance\components\ComissionComponent;
use app\modules\finance\components\paysystem\FchangeComponent;
use app\modules\finance\models\CashTransaction;
use app\modules\finance\models\PaySystem;
use Yii;

class PaySystemComponent {

	// взять необхоимые данные от платёжной системы для платежа
	public static function additionalInfoFrom($payment) {

		$additionalInfo = '';

		if ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_COINBASE) {
			$userWallet = $payment->getToUserWallet();
			$additionalInfo = ['wallet_id' => $userWallet->wallet_id, 'wallet' => $userWallet->in_wallet];

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_CM) {
			$userWallet = $payment->getToUserWallet();
			$additionalInfo = ['wallet_id' => $userWallet->id, 'wallet' => $userWallet->in_wallet];

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_TOP_EXCHANGE) {
			$topChangeData = TopexchangeComponent::getCourseListForPaySysIdent($payment->sub_pay_system);
			$additionalInfo = $topChangeData;

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_ADVCASH) {
			$additionalInfo = ['key' => PaySystem::KEY_ADVCASH];

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_PERFECTMONEY) {
			$additionalInfo = ['key' => PaySystem::KEY_PERFECTMONEY];

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_Payeer) {
			$additionalInfo = ['key' => PaySystem::KEY_Payeer];

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_CASH) {
			$additionalInfo = $payment->additional_info;
		}

		if (empty($additionalInfo)) {
			return false;
		} else {
			return $additionalInfo;
		}

	}

	// валидация дополнительных аттрибутов платёжной системы
	public static function validatePaymentAttributes($payment) {
		$result = '';
		if ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_COINBASE) {

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_TOP_EXCHANGE) {
			if (empty($payment->sub_pay_system)) {
				$result = Yii::t('app', 'Выберите платёжную систему');
			} else {
				$topChangeData = json_decode($payment->additional_info);
				if ($payment->to_sum < $topChangeData->min_summ) {
					$result = Yii::t('app', 'Мин. сумма для покупки: {min_summ}', ['min_summ' => $topChangeData->min_summ]);
				} elseif ($payment->to_sum > $topChangeData->max_summ) {
					$result = Yii::t('app', 'Макс. сумма для покупки: {min_summ}', ['min_summ' => $topChangeData->min_summ]);
				}
			}

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_ADVCASH) {

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_PERFECTMONEY) {

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_Payeer) {

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_CASH) {
			$cashTransaction = new CashTransaction;
			$attributes = json_decode($payment->additional_info, true);
			$cashTransaction->setAttributes($attributes);
			if (!$cashTransaction->validate()){

				foreach($cashTransaction->getErrors() as $error){
					$result = $error[0];
				}
			}

		}

		return $result;
	}

	// сохранение дополнительных аттрибутов платёжной системы
	public static function saveFromPaySysAttr($payment, $attributes) {
		if ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_COINBASE) {

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_TOP_EXCHANGE) {

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_ADVCASH) {

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_PERFECTMONEY) {

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_Payeer) {

		} elseif ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_CASH) {
			$cashTransaction = new CashTransaction;
			$cashTransaction->setAttributes($attributes);
			$cashTransaction->status = CashTransaction::STATUS_NEW;
			$cashTransaction->amount = $payment->from_sum;
			$cashTransaction->payment_id = $payment->id;
			$cashTransaction->date_add = date('Y-m-d H:i:s');
			$cashTransaction->save(false);
		}
	}

	// сохранение дополнительных аттрибутов платёжной системы
	public static function saveToPaySysAttr($payment, $attributes) {
		if ($payment->getToPaySystem()->one()->key == PaySystem::KEY_COINBASE) {

		} elseif ($payment->getToPaySystem()->one()->key == PaySystem::KEY_TOP_EXCHANGE) {
		    $info = [];
		    foreach ($attributes as $name => $attribute) {
		        if (!empty($attribute)) {
                    $info[$name] = $attribute;
                }

		        $payment->additional_info = json_encode($info);
		        $payment->save(false);
            }

		} elseif ($payment->getToPaySystem()->one()->key == PaySystem::KEY_ADVCASH) {

		} elseif ($payment->getToPaySystem()->one()->key == PaySystem::KEY_PERFECTMONEY) {

		} elseif ($payment->getToPaySystem()->one()->key == PaySystem::KEY_Payeer) {

		} elseif ($payment->getToPaySystem()->one()->key == PaySystem::KEY_CASH) {
			$cashTransaction = new CashTransaction;
			$cashTransaction->setAttributes($attributes);
			$cashTransaction->status = CashTransaction::STATUS_NEW;
			$cashTransaction->amount = $payment->to_sum;
			$cashTransaction->payment_id = $payment->id;
			$cashTransaction->date_add = date('Y-m-d H:i:s');
			$cashTransaction->save(false);
		}
	}

	// взять userWallet для платёжной системы
	public static function getUserWalletClassName($paySystem) {
		return 'app\modules\finance\models\UserPayWallet' . $paySystem->key;
	}
}