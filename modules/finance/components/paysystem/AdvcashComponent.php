<?php

namespace app\modules\finance\components\paysystem;

use app\modules\finance\models\Currency;
use app\modules\mainpage\models\Preference;

require_once("additional/advcash/MerchantWebService.php");

class AdvcashComponent {
	// запрос баланса
	public function getBalance() {

		$apiName = Preference::find()->where(['key' => Preference::KEY_ADVCASH_API_NAME])->one()->value;
		$accountEmail = Preference::find()->where(['key' => Preference::KEY_ADVCASH_ACCOUNT_EMAIL])->one()->value;
		$apiPassword = Preference::find()->where(['key' => Preference::KEY_ADVCASH_API_PASSWORD])->one()->value;

		$merchantWebService = new \MerchantWebService();

		$arg0 = new \authDTO();
		$arg0->apiName = $apiName;
		$arg0->accountEmail = $accountEmail;
		$arg0->authenticationToken = $merchantWebService->getAuthenticationToken($apiPassword);

		$getBalances = new \getBalances();
		$getBalances->arg0 = $arg0;

		try {
		    $getBalancesResponse = $merchantWebService->getBalances($getBalances);

		    echo print_r($getBalancesResponse->return, true);
		} catch (Exception $e) {
		    echo "ERROR MESSAGE => " . $e->getMessage() . "<br/>";
		    echo $e->getTraceAsString();
		}

		return $getBalancesResponse;
	}

	// отправка средств
    public function sendFunds($walletId, $currencyKey, $amount, $paymentId) {

        if ($currencyKey == Currency::KEY_RUR){
            $currencyKey = 'RUR';
        }

        $apiName = Preference::find()->where(['key' => Preference::KEY_ADVCASH_API_NAME])->one()->value;
        $accountEmail = Preference::find()->where(['key' => Preference::KEY_ADVCASH_ACCOUNT_EMAIL])->one()->value;
        $apiPassword = Preference::find()->where(['key' => Preference::KEY_ADVCASH_API_PASSWORD])->one()->value;

        $merchantWebService = new \MerchantWebService();

        $arg0 = new \authDTO();
        $arg0->apiName = $apiName;
        $arg0->accountEmail = $accountEmail;
        $arg0->authenticationToken = $merchantWebService->getAuthenticationToken($apiPassword);

        $arg1 = new \sendMoneyRequest();
        $arg1->amount = $amount;
        $arg1->currency = $currencyKey;
        $arg1->walletId = $walletId;
        //$arg1->walletId = $walletId;
        //$arg1->note = "note";
        $arg1->savePaymentTemplate = false;

        $validationSendMoney = new \validationSendMoney();
        $validationSendMoney->arg0 = $arg0;
        $validationSendMoney->arg1 = $arg1;

        $sendMoney = new \sendMoney();
        $sendMoney->arg0 = $arg0;
        $sendMoney->arg1 = $arg1;

        try {
            $merchantWebService->validationSendMoney($validationSendMoney);
            $sendMoneyResponse = $merchantWebService->sendMoney($sendMoney);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $sendMoneyResponse->return;
    }

    // валидация кошелька
    public function validateWallet($walletId) {

        $apiName = Preference::find()->where(['key' => Preference::KEY_ADVCASH_API_NAME])->one()->value;
        $accountEmail = Preference::find()->where(['key' => Preference::KEY_ADVCASH_ACCOUNT_EMAIL])->one()->value;
        $apiPassword = Preference::find()->where(['key' => Preference::KEY_ADVCASH_API_PASSWORD])->one()->value;

        $merchantWebService = new \MerchantWebService();

        $arg0 = new \authDTO();
        $arg0->apiName = $apiName;
        $arg0->accountEmail = $accountEmail;
        $arg0->authenticationToken = $merchantWebService->getAuthenticationToken($apiPassword);

        $arg1 = new \validateAccountRequestDTO();
        $arg1->walletId = $walletId;
        $arg1->firstName = 'first';
        $arg1->lastName = 'last';

        $validateAccount = new \validateAccount();
        $validateAccount->arg0 = $arg0;
        $validateAccount->arg1 = $arg1;

        try {
            $validateAccountsResponse = $merchantWebService->validateAccount($validateAccount);
        } catch (\Exception $e) {
            return str_replace('USER WITH email = [null] OR', '', $e->getMessage());
        }
    }
}