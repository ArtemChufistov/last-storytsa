<?php

namespace app\modules\finance\components;

use app\modules\finance\models\CurrencyCourse;
use app\modules\mainpage\models\Preference;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\Currency;

class ComissionComponent {

	// расчёт комиссии для  начальной суммы оплаты (возвращает массив комиссия сумма)
	public function calcFromSumByToSum($payment) {

		$currCourse = CurrencyCourse::getActivePayDirection($payment);

		if (empty($currCourse)) {
			throw new \Exception("Comission calculate error");
		} else {
			$currCourse = $currCourse[0];
		}

		if ($payment->getFromPaySystem()->one()->key == PaySystem::KEY_TOP_EXCHANGE){
			$defFchangeCur = Preference::find()->where(['key' => Preference::KEY_PREF_TOPEXCHANGE_DEFAULT_CUR])->one();
		}

		if (!empty($defFchangeCur) && $defFchangeCur->value == Currency::KEY_USD) {
			$data['fromSum'] = $payment->to_sum * $payment->getToCurrency()->one()->course_to_usd;
		} else {
			$data['fromSum'] = $payment->to_sum * $payment->getToCurrency()->one()->course_to_usd / $payment->getFromCurrency()->one()->course_to_usd;
		}

		if ($currCourse->comission_type == CurrencyCourse::COMISSION_TYPE_FIX) {
			$data['comissionFromSum'] = $currCourse->comission;
		} elseif ($currCourse->comission_type == CurrencyCourse::COMISSION_TYPE_PERCENT) {
			$data['comissionFromSum'] = ($data['fromSum'] / 100) * $currCourse->comission;
		}

		return self::roundFromSum($data);
	}

	// расчёт комиссии для конечной суммы оплаты (возвращает массив комиссия сумма)
	public function calcToSumByFromSum($payment) {

		$currCourse = CurrencyCourse::getActivePayDirection($payment);

		if (empty($currCourse)) {
			throw new \Exception("Comission calculate error");
		} else {
			$currCourse = $currCourse[0];
		}

		$data['toSum'] = $payment->from_sum * $payment->getFromCurrency()->one()->course_to_usd / $payment->getToCurrency()->one()->course_to_usd;

		if ($currCourse->comission_type == CurrencyCourse::COMISSION_TYPE_FIX) {
			$data['comissionToSum'] = $currCourse->comission;
		} elseif ($currCourse->comission_type == CurrencyCourse::COMISSION_TYPE_PERCENT) {
			$data['comissionToSum'] = ($data['toSum'] / 100) * $currCourse->comission;
		}

		return self::roundToSum($data);
	}

	// расчёт комиссии для начальной суммы основываясь на realSum оплаты (возвращает массив комиссия сумма)
	public function calcFromSumByRealSum($payment) {

		$currCourse = CurrencyCourse::getActivePayDirection($payment);

		if (empty($currCourse)) {
			throw new \Exception("Comission calculate error");
		} else {
			$currCourse = $currCourse[0];
		}

		if ($currCourse->comission_type == CurrencyCourse::COMISSION_TYPE_FIX) {
			$data['comissionFromSum'] = $currCourse->comission;
			$data['fromSum'] = $payment->realSum - $currCourse->comission;
		} elseif ($currCourse->comission_type == CurrencyCourse::COMISSION_TYPE_PERCENT) {
			$data['fromSum'] = (100 * $payment->realSum) / (100 + $currCourse->comission);
			$data['comissionFromSum'] = $payment->realSum - $data['fromSum'];
		}

		return self::roundFromSum($data);
	}

	public static function roundFromSum($data) {
		return [
			'fromSum' => number_format($data['fromSum'], 6, '.', ''),
			'comissionFromSum' => number_format($data['comissionFromSum'], 6, '.', ''),
		];
	}

	public static function roundToSum($data) {
		return [
			'toSum' => number_format($data['toSum'], 6, '.', ''),
			'comissionToSum' => number_format($data['comissionToSum'], 6, '.', ''),
		];
	}
}