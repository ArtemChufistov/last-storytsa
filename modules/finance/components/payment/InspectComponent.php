<?php

namespace app\modules\finance\components\payment;

use app\modules\finance\models\PerfectmoneyTransaction;
use app\modules\finance\models\TopexchangeTransaction;
use app\modules\finance\models\CoinbaseTransaction;
use app\modules\finance\models\AdvcashTransaction;
use app\modules\finance\models\CashTransaction;
use app\modules\finance\models\CmTransaction;
use app\modules\finance\models\Payment;

// Инспектор производит поиск транзакции для платежа
class InspectComponent {

	// возвращает транзакцию
	public static function process($payment) {

		if ($payment->type == Payment::TYPE_IN_OFFICE) {
			$inspectFunction = 'inspect' . $payment->getFromPaySystem()->one()->key;
		} elseif ($payment->type == Payment::TYPE_OUT_OFFICE) {
			//$inspectFunction = 'inspect' . $payment->getToPaySystem()->one()->key;
		}

		return self::$inspectFunction($payment);
	}

	// сверка платежа с Coinbase
	public function inspectCoinbase($payment) {

		$transaction = CoinbaseTransaction::find()
			->where(['wallet_id' => $payment->wallet_id])
			->andWhere(['payment_id' => null])
			->andWhere(['network_status' => CoinbaseTransaction::NETWORK_STATUS_CONFIRMED])
			->one();

		if (!empty($transaction)) {
			$transaction->payment_id = $payment->id;
		}

		return $transaction;
	}

	// сверка платежа с Perfect Money
	public function inspectPerfectmoney($payment) {

		$transaction = PerfectmoneyTransaction::find()
			->where(['payment_id' => $payment->id, 'units' => $payment->getFromCurrency()->one()->key])
			->andWhere(['is', 'treated', NULL])->one();

		if (!empty($transaction)) {
			$transaction->treated = PerfectmoneyTransaction::TREATED_TRUE;
			$transaction->date_tread = date('Y-m-d H:i:s');
		}

		return $transaction;
	}

	// сверка платежа с F-change
	public function inspectTopexchange($payment) {

		$transaction = TopexchangeTransaction::find()->where(['payment_num' => $payment->hash])->one();
		if (empty($transaction)) {
			return false;
		}

		if (!empty($transaction->payment_id)) {
			return false;
		}

		$transaction->payment_id = $payment->id;
		return $transaction;
	}

	// сверка платежа с платёжной системой Advanced Cash
	public function inspectAdvcash($payment) {

		$transaction = AdvcashTransaction::find()
			->where(['ac_order_id' => $payment->hash])
			->andWhere(['is', 'treated', NULL])->one();

		if (!empty($transaction)) {
			$transaction->treated = AdvcashTransaction::TREATED_TRUE;
			$transaction->date_tread = date('Y-m-d H:i:s');
		}

		return $transaction;
	}

	// сверка платежа с Payeer
	public function inspectPayeer($payment) {

		$transaction = PayeerTransaction::find()
			->where(['payment_id' => $payment->hash])
			->andWhere(['is', 'treated', NULL])->one();

		if (!empty($transaction)) {
			$transaction->treated = PayeerTransaction::TREATED_TRUE;
			$transaction->date_tread = date('Y-m-d H:i:s');
			$transaction->save(false);
		}

		return $transaction;
	}

	// сверка платежа Наличными
	public function inspectCash($payment) {

		$transaction = CashTransaction::find()
			->where(['payment_id' => $payment->id])
			->andWhere(['status' => CashTransaction::STATUS_OK])
			->one();

		if (!empty($transaction)) {
			$transaction->status = CashTransaction::STATUS_PAY;
			$transaction->save(false);
		}

		return $transaction;
	}

	// сверка платежа Cm
	public function inspectCm($payment) {

		$transaction = CmTransaction::find()
			->where(['payment_id' => null])
			->andWhere(['address' => $payment->wallet])
			->andWhere(['>=', 'confirmations', 2])
			->one();

		if (!empty($transaction)) {
			$transaction->payment_id = $payment->id;
		}

		return $transaction;
	}
}