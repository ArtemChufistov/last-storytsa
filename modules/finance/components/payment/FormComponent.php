<?php

namespace app\modules\finance\components\payment;

use app\modules\finance\components\paysystem\PaySystemComponent;
use app\modules\finance\models\forms\payment\FromCurrency;
use app\modules\finance\models\forms\payment\FromPaySystem;
use app\modules\finance\models\forms\payment\FromSum;
use app\modules\finance\models\forms\payment\ToCurrency;
use app\modules\finance\models\forms\payment\ToPaySystem;
use app\modules\finance\models\forms\payment\ToSum;
use app\modules\finance\components\ComissionComponent;
use app\modules\finance\models\Payment;
use app\modules\finance\models\PaySystem;

class FormComponent {
	private $user;
	private $data;

	public function set($data) {
		foreach ($data as $item => $value) {
			$this->$item = $value;
		}

		return $this;
	}

	// входящая оплата
	public function inPayment() {
		$paySystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
		$toSum = new ToSum;
		$toSum->type = Payment::TYPE_IN_OFFICE;
		$toSum->status = Payment::STATUS_WAIT_SYSTEM;
		$toSum->to_user_id = $this->user->id;
		$toSum->to_pay_system_id = $paySystemLk->id;

		// валидация суммы
		if ($toSum->load($this->data) && $toSum->validate()) {
			$toCurrency = new ToCurrency;
			$toCurrency->setAttributes($toSum->attributes);

			// валидация конечной валюты
			if ($toCurrency->load($this->data) && $toCurrency->validate()) {
				$fromCurrency = new FromCurrency;
				$fromCurrency->setAttributes($toCurrency->attributes);

				// валидация начальной валюты
				if ($fromCurrency->load($this->data) && $fromCurrency->validate()) {
					$fromPaySystem = new FromPaySystem;
					$fromPaySystem->setAttributes($fromCurrency->attributes);

					// валидация начальной платёжной системы
					if ($fromPaySystem->load($this->data) && $fromPaySystem->validate()) {
						$payment = new Payment;
						$payment->setAttributes($fromPaySystem->attributes);

						extract(ComissionComponent::calcFromSumByToSum($payment));
						$payment->from_sum = $fromSum;
						$payment->comission_from_sum = $comissionFromSum;
						$payment->course_to_usd = $payment->getFromCurrency()->one()->course_to_usd;
						$payment->course = $payment->getFromCurrency()->one()->course_to_usd / $payment->getToCurrency()->one()->course_to_usd;
						$payment->save();

						PaySystemComponent::saveFromPaySysAttr($payment, json_decode($fromPaySystem->additional_info, true));
						$paymentForm = $payment;
					} else {
						$paymentForm = $fromPaySystem;
					}
				} else {
					$paymentForm = $fromCurrency;
				}
			} else {
				$paymentForm = $toCurrency;
			}
		} else {
			$paymentForm = $toSum;
		}

		return $paymentForm;
	}

	// исходящая оплата
	public function outPayment() {
		$paySystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
		$fromSum = new FromSum;
		$fromSum->type = Payment::TYPE_OUT_OFFICE;
		$fromSum->status = Payment::STATUS_WAIT_SYSTEM;
		$fromSum->from_user_id = $this->user->id;
		$fromSum->from_pay_system_id = $paySystemLk->id;

		// валидация суммы
		if ($fromSum->load($this->data) && $fromSum->validate()) {
			$toCurrency = new ToCurrency;
			$toCurrency->setAttributes($fromSum->attributes);

			// валидация конечной валюты
			if ($toCurrency->load($this->data) && $toCurrency->validate()) {
				$fromCurrency = new FromCurrency;
				$fromCurrency->setAttributes($toCurrency->attributes);

				// валидация начальной валюты
				if ($fromCurrency->load($this->data) && $fromCurrency->validate()) {
					$toPaySystem = new ToPaySystem;
					$toPaySystem->setAttributes($fromCurrency->attributes);

					// валидация конечной платёжной системы
					if ($toPaySystem->load($this->data) && $toPaySystem->validate()) {
						$payment = new Payment;
						$payment->setAttributes($toPaySystem->attributes);
						$userPayWallet = $payment->getFromUserWallet();
						$userPayWallet->setFormName($toPaySystem->formName());

						// валидация кошелька пользователя
						if ($userPayWallet->load($this->data) && $userPayWallet->validate()) {
							$userPayWallet->save();
							$payment->setAttributes($userPayWallet->payAttributes());

							extract(ComissionComponent::calcToSumByFromSum($payment));
							$payment->realSum = $payment->from_sum;
							$payment->to_sum = $toSum;
							$payment->comission_to_sum = $comissionToSum;
							$payment->course_to_usd = $payment->getFromCurrency()->one()->course_to_usd;
							$payment->course = $payment->getFromCurrency()->one()->course_to_usd / $payment->getToCurrency()->one()->course_to_usd;

							if ($payment->save()) {
								PaySystemComponent::saveToPaySysAttr($payment, $userPayWallet->attributes);
								$payment->getFromUserBalance()->recalculateValue();
							}
							$paymentForm = $payment;
						} else {
							$paymentForm = $userPayWallet;
						}

					} else {
						$paymentForm = $toPaySystem;
					}
				} else {
					$paymentForm = $fromCurrency;
				}
			} else {
				$paymentForm = $toCurrency;
			}
		} else {
			$paymentForm = $fromSum;
		}

		return $paymentForm;
	}
}