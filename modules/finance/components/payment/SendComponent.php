<?php

namespace app\modules\finance\components\payment;

use app\modules\finance\components\paysystem\CoinBaseComponent;
use app\modules\finance\components\paysystem\AdvcashComponent;
use app\modules\finance\components\paysystem\CmComponent;
use app\modules\finance\components\paysystem\TopexchangeComponent;
use app\modules\finance\models\AdvcashTransaction;
use app\modules\finance\models\CoinbaseTransaction;
use app\modules\finance\models\CashTransaction;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\PerfectmoneyTransaction;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\TopexchangeTransaction;
use Yii;
use yii\db\Exception;

class SendComponent {

	public function process($payment) {
		$sendSumFunction = 'send' . $payment->getToPaySystem()->one()->key;
		if (self::$sendSumFunction($payment)) {
			return true;
		} else {
			return false;
		}
	}

	// Отправка платежа на lk
	public function sendlk($payment) {

		$transaction = Transaction::find()->where(['payment_id' => $payment->id])->andWhere(['type' => Transaction::TYPE_CHARGE_OFFICE])->one();
		if (!empty($transaction)) {
			throw new \Exception(Yii::t('app','payment allready send for transaction id = {num} payment id = {payId}', ['num' => $transaction->id, 'payId' => $payment->id]));
			return false;
		}

		$transaction = new Transaction;
		$transaction->to_user_id = $payment->to_user_id;
		$transaction->type = Transaction::TYPE_CHARGE_OFFICE;
		$transaction->sum = $payment->to_sum;
		$transaction->currency_id = $payment->to_currency_id;
		$transaction->payment_id = $payment->id;
		$transaction->save(false);

		$balances = $payment->getToUser()->one()->getBalances();
		$balances[$payment->to_currency_id]->recalculateValue();

		return true;
	}

	// Отправка платежа на Coinbase
	public function sendCoinbase($payment) {

		$cbTransaction = CoinbaseTransaction::find()->where(['payment_id' => $payment->id])->one();
		if (!empty($cbTransaction)) {
			throw new \Exception(Yii::t('app','payment allready send for transaction {num}', ['num' => $transaction->id]));
			return false;
		}

		$cbTransaction = new CoinbaseTransaction;
		$cbTransaction->payment_id = $payment->id;
		$cbTransaction->save(false);

		$sendSum = number_format($payment->to_sum - $payment->comission_to_sum, 6);
		$cbInfo = CoinBaseComponent::sendFunds($payment->wallet, $sendSum, $payment->getToCurrency()->one()->key, Yii::t('app', 'Вывод средств'));

		$cbData = $cbInfo->getRawData();

		if (empty($cbData['network']['hash'])) {
			throw new exception('crypto network do not reply');
			return false;
		}

		$cbTransaction->network_hash = $cbData['network']['hash'];
		$cbTransaction->wallet_id = $cbData['id'];
		$cbTransaction->type = $cbData['type'];
		$cbTransaction->status = $cbData['status'];
		$cbTransaction->amount = $cbData['amount']['amount'];
		$cbTransaction->description = $cbData['description'];
		$cbTransaction->created_at = $cbData['created_at'];
		$cbTransaction->updated_at = $cbData['updated_at'];
		$cbTransaction->resource = $cbData['resource'];
		$cbTransaction->resource_path = $cbData['resource_path'];
		$cbTransaction->instant_exchange = $cbData['instant_exchange'];
		$cbTransaction->network_status = $cbData['network']['status'];

		$cbTransaction->save(false);
		return true;
	}

	// Отправка платежа на Perfect Money
	public function sendPerfectMoney($payment) {

		$pmTransaction = PerfectMoneyTransaction::find()->where(['payment_id' => $payment->id])->one();
		if (!empty($pmTransaction)) {
			throw new \Exception(Yii::t('app','payment allready send for transaction id = {num} payment id = {payId}', ['num' => $transaction->id, 'payId' => $payment->id]));
			return false;
		}

		$user = User::find()->where(['id' => $payment->from_user_id])->one();
		$payWallet = $user->getPayWallet($payment->to_pay_system_id, $payment->currency_id);
		$sendSum = number_format($payment->to_sum - $payment->comission_to_sum, 2);
		$currencyKey = $payment->getCurrency()->one()->key;

		$pmTransaction = new PerfectMoneyTransaction;
		$pmTransaction->amount = $sendSum;
		$pmTransaction->payment_id = $payment->id;
		$pmTransaction->save(false);

		$pmResult = PerfectMoneyComponent::sendFunds($payWallet->wallet, $currencyKey, $sendSum, $payment->id);

		if (!empty($pmResult['ERROR'])) {
			print_r($pmResult);
		} else {
			$pmTransaction->batch_num = $pmResult['PAYMENT_BATCH_NUM'];
			$pmTransaction->save(false);
		}

		return true;
	}

	// Отправка платежа на AdvCash
	public function sendAdvcash($payment) {

		$advcashTransaction = AdvcashTransaction::find()->where(['ac_order_id' => $payment->hash])->one();
		if (!empty($advcashTransaction)) {
			throw new \Exception(Yii::t('app','payment allready send for transaction id = {num} payment id = {payId}', ['num' => $transaction->id, 'payId' => $payment->id]));
			return false;
		}

		$sendSum = number_format(($payment->to_sum - $payment->comission_to_sum), 2, '.', '');
		$currencyKey = $payment->getToCurrency()->one()->key;

		$advcashTransaction = new AdvcashTransaction;
		$advcashTransaction->ac_amount = $sendSum;
		$advcashTransaction->ac_order_id = $payment->hash;
		$advcashTransaction->save(false);

		echo 'Оплата Advcash на: ' . $payment->wallet . ' валюта: ' . $currencyKey . ' сумма: ' . $sendSum . ' платеж: ' . $payment->id . "\r\n";

		$result = AdvcashComponent::sendFunds($payment->wallet, $currencyKey, $sendSum, $payment->id);

		print_r($result);

		return true;
	}

	// отправка платежа на Payeer
	public function sendPayeer($payment) {

	}

	// Отправка платежа на Top-exchange
    public function sendTopexchange($payment) {
		$tx = TopexchangeTransaction::find()->where(['payment_id' => $payment->id])->one();

		if (!empty($tx)) {
			throw new \Exception(Yii::t('app','payment allready send for transaction id = {num} payment id = {payId}', ['num' => $tx->id, 'payId' => $payment->id]));
			return false;
		}

        $sendSum = number_format($payment->to_sum - $payment->comission_to_sum, 2);

        $tx = new TopexchangeTransaction;
        $tx->amount = $sendSum;
        $tx->payment_id = $payment->id;
        $tx->save(false);

        print_r($tx);

        $fields = json_decode($payment->additional_info, true);

        $fields['withdraw_summ'] = $fields['from_sum'];
        $subPaySystem = $fields['sub_pay_system'];

        $result = TopexchangeComponent::makeWithdraw($fields, $subPaySystem);

        if (!empty($result['validation_error'])) {
            print_r($result);
            throw new \Exception($result['validation_error']);
            return false;
        } else {
            print_r($result);
        }

        return true;
    }

	// Отправка платежа на Наличные
	public function sendCash($payment) {
		$cashTransaction = CashTransaction::find()->where(['payment_id' => $payment->id])->one();
		$cashTransaction->status = CashTransaction::STATUS_PAY;
		$cashTransaction->save(false);

		return true;
	}

	// Отправка платежа на Coinbase
	public function sendCm($payment) {

		print_r($payment);exit;
		$cbTransaction = CmTransaction::find()->where(['payment_id' => $payment->id])->one();
		if (!empty($cbTransaction)) {
			throw new \Exception(Yii::t('app','payment allready send for transaction {num}', ['num' => $transaction->id]));
			return false;
		}

		$cmTransaction = new CmTransaction;
		$cmTransaction->payment_id = $payment->id;
		$cmTransaction->save(false);

		$sendSum = number_format($payment->to_sum - $payment->comission_to_sum, 6);
		$cmInfo = CmComponent::sendTransaction($payment->getToCurrency()->one()->key, $payment->wallet, $sendSum);

		$cmTransaction->setAttributes($cmInfo);

		$cmTransaction->save(false);
		return true;
	}
}