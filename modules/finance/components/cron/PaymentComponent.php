<?php

namespace app\modules\finance\components\cron;

use app\modules\finance\components\ComissionComponent;
use app\modules\finance\components\payment\InspectComponent;
use app\modules\finance\components\payment\SendComponent;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\Payment;
use app\modules\mainpage\models\Preference;
use Yii;

class PaymentComponent {

	// отмена старых платежей(по умолчанию старше 24 часов)
	public static function cancelOld() {
		$pendingSeconds = Preference::find()->where(['key' => Preference::KEY_PENDING_PAYMENT_TIME])->one()->value;

		$query = Payment::find()->where(['in', 'status', [Payment::STATUS_WAIT, Payment::STATUS_WAIT_SYSTEM]])->andWhere(['<=', 'date_add', date('Y-m-d H:i:s', time() - $pendingSeconds)]);
		foreach ($query->all() as $payment) {

			$payment->status = Payment::STATUS_CANCEL;
			$payment->save(false);
			if (!empty($payment->from_user_id)) {

				$balances = $payment->getFromUser()->one()->getBalances();

				foreach ($balances as $balance) {
					$balance->recalculateValue();
				}
			}
		};
	}

	// сверка платежей
	public static function inspect() {
		$query = Payment::find()
			->where(['in', 'status', [Payment::STATUS_WAIT, Payment::STATUS_WAIT_SYSTEM]])
			->andWhere(['type' => Payment::TYPE_IN_OFFICE])
			->orderBy(['id' => SORT_DESC]);

		foreach ($query->all() as $payment) {

			$transaction = InspectComponent::process($payment);
			if (!empty($transaction)) {

				$transaction->save();
				$payment->realSum = $transaction->amount;
				// если сумма в транзакции несоответсвует сумме в заявке
				if (($payment->from_sum + $payment->comission_from_sum) != $payment->realSum) {

					extract(ComissionComponent::calcFromSumByRealSum($payment));
					$payment->from_sum = $fromSum;
					$payment->comission_from_sum = $comissionFromSum;
					$payment->to_sum = $payment->from_sum * $payment->course;
				}

				$payment->status = Payment::PROCESS_SYSTEM;
				$payment->save(false);
			}
		};
	}

	// обработка платежей
	public static function process() {
		$query = Payment::find()
			->where(['in', 'status', [Payment::PROCESS_SYSTEM]])
			->orderBy(['id' => SORT_DESC]);

		$paySystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();
		
		foreach ($query->all() as $payment) {

			$dbTransaction = Yii::$app->db->beginTransaction();
			try {
				SendComponent::process($payment);
				$payment->status = Payment::STATUS_OK;
				$payment->save(false);

				$dbTransaction->commit();
			} catch (\Exception $e) {
				$dbTransaction->rollback();

				$payment->error = $e->getMessage();
				$payment->status = Payment::STATUS_CANCEL;
				$payment->save();

				break;
			}

			if (!empty($paySystemLk) && $payment->getFromPaySystem()->one()->id == $paySystemLk->id) {

				$transaction = new Transaction;
				$transaction->from_user_id = $payment->from_user_id;
				$transaction->type = Transaction::TYPE_OUT_OFFICE;
				$transaction->sum = $payment->from_sum;
				$transaction->currency_id = $payment->from_currency_id;
				$transaction->payment_id = $payment->id;
				$transaction->save(false);

				$balances = $payment->getFromUser()->one()->getBalances();
				
				foreach ($balances as $balance) {
					$balance->recalculateValue();
				}
			}

		}
	}
}