<?php

namespace app\modules\finance\components\cron;

use app\modules\finance\models\CryptoWallet;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyGraph;
use app\modules\finance\models\CurrencyGraphHistory;
use app\modules\mainpage\models\Preference;

class CurrencyGraphComponent {
	// построение графика изменения цены для валюты
	public static function build() {
		$prefDateStart = Preference::find()->where(['key' => Preference::KEY_START_TIME_BUILD_GRAPH])->one();

		if (empty($prefDateStart)) {
			throw new Exception('Preference key for start date not set', 1);
		} else {
			$startTime = strtotime($prefDateStart->value);
		}

		$currentTime = time();

		$currencyArray = Currency::find()->where(['group' => Currency::GROUP_BUILD_GRAPH])->all();

		$steps = CurrencyGraph::getTypeStepsInSecondsArray();

		foreach ($currencyArray as $currency) {
			foreach ($steps as $stepName => $step) {

				$currencyGraphTime = CurrencyGraph::find()
					->where(['currency_id' => $currency->id])
					->andWhere(['type' => $stepName])
					->orderBy(['date' => SORT_DESC])
					->one();
				if (empty($currencyGraphTime) || ((strtotime($currencyGraphTime->date) + $step) < $currentTime)) {
					$currencyGraph = new CurrencyGraph;
					$currencyGraph->value = $currency->course_to_usd;
					$currencyGraph->currency_id = $currency->id;
					$currencyGraph->type = $stepName;

					if (empty($currencyGraphTime)) {
						$currencyGraph->date = date('Y-m-d H:i:s', $startTime);
					} else {
						$currencyGraph->date = date('Y-m-d H:i:s', strtotime($currencyGraphTime->date) + $step);
					}

					$currencyGraph->save(false);
				}
			}
		}
	}

	// тестовая функция - построить график по currency_graph_history криптокошельков через BTC
	public function buildByHistory($startDate = '2017-05-01 01:00:00', $curGraphType = '') {

		if (empty($currencyGraphType)) {
			$curGraphType = CurrencyGraph::TYPE_2_HOURE;
		}

		$currencyCitt = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();
		$currencyBTC = Currency::find()->where(['key' => Currency::KEY_BTC])->one();

		$graphTypeInSec = CurrencyGraph::getTypeInSecond($curGraphType);
		$cryptoWalletArray = CryptoWallet::find()->all();

		$startBalanceInUsd = 0;
		foreach ($cryptoWalletArray as $cryptoWallet) {
			if ($cryptoWallet->getCurrency()->one()->key == Currency::KEY_BTC) {
				$fromCurrency = Currency::find()->where(['key' => Currency::KEY_USDT])->one();
			} else {
				$fromCurrency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
			}

			$graphHistory = CurrencyGraphHistory::find()
				->where(['from_currency_id' => $fromCurrency->id])
				->andWhere(['to_currency_id' => $cryptoWallet->getCurrency()->one()->id])
				->andWhere(['date' => $startDate])
				->one();

			if (empty($graphHistory)) {
				echo ' Не найден курс для валюты: ' . $fromCurrency->key . ' с валюты: ' . $cryptoWallet->getCurrency()->one()->key . ' за: ' . $startDate . "\r\n";
				$courseToUsd = 0;
			} else {
				$courseToUsd = $graphHistory->course_to_usd;
			}

			$startBalanceInUsd += $cryptoWallet->balance * $courseToUsd;

			echo ' Кошелек: ' . $cryptoWallet->getCurrency()->one()->key .
			' Cумма: ' . $cryptoWallet->balance .
			' Курс: ' . $courseToUsd .
			' в USD: ' . $cryptoWallet->balance * $courseToUsd . "\r\n";

		}

		echo "\r\n" . ' Итого на начальную дату в USD: ' . $startBalanceInUsd . "\r\n";

		// строим график
		$currentTime = time();
		for ($date = strtotime($startDate); $date < $currentTime; $date += $graphTypeInSec) {
			$balanceInUsd = 0;

			foreach ($cryptoWalletArray as $cryptoWallet) {
				if ($cryptoWallet->getCurrency()->one()->key == Currency::KEY_BTC) {
					$fromCurrency = Currency::find()->where(['key' => Currency::KEY_USDT])->one();
				} else {
					$fromCurrency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
				}

				$graphHistory = CurrencyGraphHistory::find()
					->where(['from_currency_id' => $fromCurrency->id])
					->andWhere(['to_currency_id' => $cryptoWallet->getCurrency()->one()->id])
					->andWhere(['date' => date('Y-m-d H:i:s', $date)])
					->one();

				if (empty($graphHistory)) {
					echo ' Не найден курс для валюты: ' . $fromCurrency->key .
					' с валюты: ' . $cryptoWallet->getCurrency()->one()->key .
					' за: ' . date('Y-m-d H:i:s', $date) . "\r\n";
					$courseToUsd = 0;
				} else {
					$courseToUsd = $graphHistory->course_to_usd;
				}

				$balanceInUsd += $cryptoWallet->balance * $courseToUsd;
			}

			$currencyGraph = CurrencyGraph::find()
				->where(['date' => date('Y-m-d H:i:s', $date)])
				->andWhere(['type' => $curGraphType])
				->andWhere(['currency_id' => $currencyCitt->id])
				->one();

			if (empty($currencyGraph)) {
				$currencyGraph = new CurrencyGraph;
				$currencyGraph->date = date('Y-m-d H:i:s', $date);
				$currencyGraph->type = $curGraphType;
				$currencyGraph->currency_id = $currencyCitt->id;
			}

			$currencyGraph->value = $balanceInUsd / $startBalanceInUsd;
			$currencyGraph->save();
		}
	}
}