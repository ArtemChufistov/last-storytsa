<?php

namespace app\modules\finance\components\cron;

use app\modules\finance\components\paysystem\CoinBaseComponent;
use app\modules\finance\components\paysystem\CmComponent;
use app\modules\finance\models\CoinbaseTransaction;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\CmTransaction;

class SyncComponent {

	// синхронизация Coinbase транзакций с аккаунтом
	public static function CoinBaseTransactions() {
		$coinbasePaySystem = PaySystem::find()->where(['key' => PaySystem::KEY_COINBASE])->one();

		if (empty($coinbasePaySystem)){
			return;
		}

		$coinbaseCurrencyArray = CurrencyCourse::find()->where(['from_pay_system_id' => $coinbasePaySystem->id])->all();

		foreach ($coinbaseCurrencyArray as $coinbaseCurrency) {
			extract(CoinBaseComponent::getAccountClientForCurrency($coinbaseCurrency->getFromCurrency()->one()->key));

			foreach ($client->getAccountAddresses($account) as $address) {
				$addressRawData = $address->getRawData();
				foreach ($client->getAddressTransactions($address) as $transaction) {
					$tranData = $transaction->getRawData();

					if (empty($tranData['network']['hash'])) {
						continue;
					}

					$coinbaseTransaction = CoinbaseTransaction::find()->where(['network_hash' => $tranData['network']['hash']])->one();
					if (empty($coinbaseTransaction)) {
						$coinbaseTransaction = new CoinbaseTransaction;
					}

					$coinbaseTransaction->wallet_id = $addressRawData['id'];
					$coinbaseTransaction->type = $tranData['type'];
					$coinbaseTransaction->status = $tranData['status'];
					$coinbaseTransaction->amount = $tranData['amount']['amount'];
					$coinbaseTransaction->description = $tranData['description'];
					$coinbaseTransaction->created_at = $tranData['created_at'];
					$coinbaseTransaction->updated_at = $tranData['updated_at'];
					$coinbaseTransaction->resource = $tranData['resource'];
					$coinbaseTransaction->resource_path = $tranData['resource_path'];
					$coinbaseTransaction->instant_exchange = $tranData['instant_exchange'];
					$coinbaseTransaction->network_status = $tranData['network']['status'];
					$coinbaseTransaction->network_hash = $tranData['network']['hash'];
					$coinbaseTransaction->save(false);
				}
			}
		}
	}

	// синхронизация Cm транзакций с аккаунтом
	public static function CmTransactions() {
		$cmPaySystem = PaySystem::find()->where(['key' => PaySystem::KEY_CM])->one();
		$cmCurrencyArray = CurrencyCourse::find()->where(['from_pay_system_id' => $cmPaySystem->id])->all();

		foreach ($cmCurrencyArray as $cmCurrency) {
			
			$transactions = CmComponent::getAccountTransactions($cmCurrency->getToCurrency()->one()->key, 100);

			foreach($transactions as $transaction){

				if ($transaction['category'] == 'receive' && $transaction['confirmations'] > 0){
					$cmTransaction = CmTransaction::find()->where(['txid' => $transaction['txid']])->one();

					if (empty($cmTransaction)) {
						$cmTransaction = new CmTransaction;
					}

					$cmTransaction->setAttributes($transaction);
					$cmTransaction->time = date("Y-m-d H:i:s", $transaction['time']);
					$cmTransaction->blocktime = date("Y-m-d H:i:s", $transaction['blocktime']);
					$cmTransaction->timereceived = date("Y-m-d H:i:s", $transaction['timereceived']);
					$cmTransaction->save(false);
				}
			}
		}
	}
}