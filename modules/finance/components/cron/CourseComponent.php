<?php

namespace app\modules\finance\components\cron;

use app\modules\finance\components\paysystem\LivecoinComponent;
use app\modules\finance\components\paysystem\PoloniexComponent;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\Transaction;
use linslin\yii2\curl;

class CourseComponent {
	// обновление курса валют по таблице currency_course
	public function updateCurrencyCourseTable() {
		$currencyCourseArray = CurrencyCourse::find()->all();

		foreach ($currencyCourseArray as $currencyCourse) {
			if ($currencyCourse->type == CurrencyCourse::TYPE_AUTO) {

				if (($currencyCourse->course = self::getCourse($currencyCourse->getFromCurrency()->one()->key, $currencyCourse->getToCurrency()->one()->key))) {
					$currencyCourse->save(false);
				} else {
					sleep(2);
					if (($currencyCourse->course = self::getCourse($currencyCourse->getToCurrency()->one()->key, $currencyCourse->getFromCurrency()->one()->key))) {
						$currencyCourse->course = 1 / $currencyCourse->course;
						$currencyCourse->save(false);
					}
				}
			}
			sleep(2);
		}
	}

	// обновление курса к доллару по таблице currency
	public static function updateCurrencyCourseToUsd() {
		$currencyArray = Currency::find()->where(['auto_course_update' => Currency::AUTO_COURSE_UPDATE_TRUE])->all();

		foreach ($currencyArray as $currency) {

			try {

				echo 'Обновление в ' . date('Y-m-d H:i:s') . ' курса валюты: ' . $currency->key . "\r\n";

				$course = call_user_func($currency->func_course_update, $currency->key);

				$currency->course_to_usd = $course;

				$currency->save();

			} catch (\Exception $e) {
				echo $e->getMessage(), "\n";
			}
			sleep(2);
		}
	}

	// обновление курса CITT ваучера
	public static function getSittCourse($fromCurrency, $toCurrency = 'USD') {
		$currencyArray = Currency::find()->where(['group' => Currency::GROUP_SPECIAL_2])->all();

		$sumInUsd = 0;
		foreach ($currencyArray as $currency) {
			foreach ($currency->getCryptoWallets()->all() as $cryptoWallet) {
				$sumInUsd += $cryptoWallet->balance * $currency->course_to_usd;
			}
		}

		$transactionSumPlus = Transaction::find()->where(['is not', 'to_user_id', null])->sum('sum');
		$transactionSumMinus = Transaction::find()->where(['is not', 'from_user_id', null])->sum('sum');

		$courseToUsd = round($sumInUsd / ($transactionSumPlus - $transactionSumMinus), 2);

		return $courseToUsd;
	}

	// обновление курса DiC ваучера
	public static function getDicCourse($fromCurrency, $toCurrency = 'USD') {
		return 0.1;
	}

	public static function getFreePointCourse($fromCurrency, $toCurrency = 'USD')
	{
		return 1;
	}

	// обновление курса по Livecoin
	public static function getLivecoinCourse($fromCurrency, $toCurrency = 'USD') {
		$tickerInfo = LivecoinComponent::getticker($fromCurrency, $toCurrency);
		return $tickerInfo['last'];
	}

	// Обновление курса по BlockChain
	public static function getBlochchainCourse($fromCurrency, $toCurrency = 'USD') {
		$curl = new curl\Curl();
 
		$curl->setOption(CURLOPT_USERAGENT, 'Yii2-Curl-Agent');

       	$response = $curl->get('https://blockchain.info/ru/ticker');
       	$response = json_decode($response, true);

       	return $response[$toCurrency]['last'];
	}

	// обновление курса по Poloniex (обязательно должен быть определён курс BTC-USD)
	public static function getPoloniexCourse($fromCurrency, $toCurrency = 'USD') {
		$currencyBTC = Currency::find()->where(['key' => Currency::KEY_BTC])->one();

		$poloniexComp = new PoloniexComponent();
		$poloniexInfo = $poloniexComp->returnTicker();

		if (empty($poloniexInfo['BTC_' . $fromCurrency]['last'])) {
			if (empty($poloniexInfo[$fromCurrency . '_BTC']['last'])) {
				throw new \Exception('Currency pair not find');
			} else {
				$course = 1 / $poloniexInfo[$fromCurrency . '_BTC']['last'];
			}
		} else {
			$course = $poloniexInfo['BTC_' . $fromCurrency]['last'];
		}

		$courseToUsd = $course * $currencyBTC->course_to_usd;

		return $courseToUsd;
	}

	// обновление курса ЦентроБанк
	public static function getCbrCourse($fromCurrency, $toCurrency = 'USD')
	{
		$curl = new curl\Curl();
 
		$curl->setOption(CURLOPT_USERAGENT, 'Yii2-Curl-Agent');

       	$response = $curl->get('http://www.cbr.ru/scripts/XML_daily.asp');
		
		$reader = new \Sabre\Xml\Reader();
		$reader->xml($response);
		$result = $reader->parse();

       	
       	foreach ($result['value'] as $item) {
       		foreach ($item['value'] as $itemValue) {
       			if ($itemValue['name'] == '{}CharCode' && $itemValue['value'] == $toCurrency) {
	       			print_r($item);
	       			print_r($itemValue['value']);
	       			echo "\r\n";
	       			//exit;
       			}
       		}
       	}
	}
}