<?php
/**
 * @author Valentin Konusov <rlng-krsk@yandex.ru>
 * @copyright Copyright (c) 2014 Valentin Konusov
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/BioSin/yii2-closure-table-behavior
 */

namespace app\modules\finance\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\base\InvalidConfigException;
use app\modules\finance\models\Currency;
use app\modules\finance\models\UserBalance;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\CurrencyCourse;

class UserCurrencyBehavior extends Behavior
{
	public $user;
	public $withNoActive = false;

	// исключить валюты из списка возвращаемых для пользователя
	public function withNoActive()
	{
		$this->withNoActive = true;

		return $this->user;
	}

	// взять балансы пользователя
	public function getBalances($additionCurrencyes = []) {
		$paySystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();

		$currencyCourseRecord = CurrencyCourse::find()->where(['to_pay_system_id' => $paySystemLk->id]);

		if ($this->withNoActive == false){
			$currencyCourseRecord->andWhere(['active' => CurrencyCourse::ACTIVE_TRUE]);
		}

		$currencyCourseArray = $currencyCourseRecord->distinct(['to_currency'])->all();
		$balanceArray = [];
		foreach ($currencyCourseArray as $currencyCourse) {
			$userBalance = UserBalance::find()->where(['user_id' => $this->user->id, 'currency_id' => $currencyCourse->to_currency])->one();

			if (empty($userBalance)) {
				$userBalance = new UserBalance;
				$userBalance->user_id = $this->user->id;
				$userBalance->currency_id = $currencyCourse->to_currency;
				$userBalance->recalculateValue();
			}
			$balanceArray[$currencyCourse->to_currency] = $userBalance;
		}

		foreach($additionCurrencyes as $currencyId){
			$userBalance = UserBalance::find()->where(['user_id' => $this->user->id, 'currency_id' => $currencyId])->one();

			if (empty($userBalance)) {
				$userBalance = new UserBalance;
				$userBalance->user_id = $this->user->id;
				$userBalance->currency_id = $currencyId;
				$userBalance->recalculateValue();
			}
			$balanceArray[$currencyId] = $userBalance;
		}

		return $balanceArray;
	}

	// пересчитать балансы пользователя
	public function recalclulateBalances($additionCurrencyes = []) {
		foreach ($this->getBalances($additionCurrencyes) as $userBalance) {
			$userBalance->recalculateValue();
		}
	}
}