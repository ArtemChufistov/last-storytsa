<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\mail\models\Mail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
            <div class="mail-form">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'template_id')->textInput() ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'headers')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'from')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'to')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

                <?php $body = $model->content;
                    if (!empty($model->addition_info)){
                        foreach(json_decode($model->addition_info) as $name => $item){
                            $body = str_replace('{{' . $name . '}}', $item, $body);
                        }
                    }
                ?>

                <div class="form-group">
                    <label class="control-label" ><?php echo Yii::t('app', 'Конечный текст письма');?></label>
                    <div class="form-control" style = "height: 150px; overflow-y: scroll;"><?php echo $body; ?></div>
                </div>

                <?= $form->field($model, 'date_add')->textInput() ?>

                <?= $form->field($model, 'date_send')->textInput() ?>

                <?= $form->field($model, 'addition_info')->textarea(['rows' => 6]) ?>


                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
      </div>
    </div>
</div>
