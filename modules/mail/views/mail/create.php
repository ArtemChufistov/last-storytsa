<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\mail\models\Mail */

$this->title = Yii::t('app', 'Добавление письма');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Очередь почтовых сообщений'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
