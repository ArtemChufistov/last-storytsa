<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\mail\models\MailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Очередь почтовых сообщений');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск'); ?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                 <?=Html::a(Yii::t('app', 'Создать письмо'), ['create'], ['class' => 'btn btn-success'])?>
            </div>
            <?php Pjax::begin();?>

                <div class="box-body no-padding">

                <?=GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],

		'id',
		'template_id',
		'title',
		'headers:ntext',
		'from',
		'to',
		// 'content:ntext',
		'date_add',
		'date_send',
		// 'addition_info:ntext',

		['class' => 'yii\grid\ActionColumn'],
	],
]);?>

                </div>

            <?php Pjax::end();?>
          </div>
        </div>
    </div>
</div>
