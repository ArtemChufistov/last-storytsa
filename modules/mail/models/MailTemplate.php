<?php

namespace app\modules\mail\models;

use Yii;

/**
 * This is the model class for table "mail_template".
 *
 * @property integer $id
 * @property string $type
 * @property string $content
 */
class MailTemplate extends \yii\db\ActiveRecord
{
    const TYPE_PARTNER_REGISTER_SUCCESS_CONFIRM = 'partnerRegisterSuccessConfirm';
    const TYPE_REGISTRATION_SUCCESS_CONFIRM = 'registrationSuccessConfirm';
    const TYPE_UNCONFIRM_EMAIL_CODE = 'unConfirmEmailCode';
    const TYPE_SEND_INVITE_BY_MAIL = 'sendInviteByMail';
    const TYPE_SEND_TRANSACTION_CONFIRM_CODE = 'sendTransactionConfirmCode';
    const TYPE_INVITE_FOR_ACTIVATE_LK = 'inviteForActivateLK';
    const TYPE_CONFIRM_EMAIL_CODE = 'confirmEmailCode';
    const TYPE_RESET_PASSWORD = 'resetPassword';
    const TYPE_PAY_PASSWORD = 'payPassword';
    const TYPE_LOST_GIFT = 'lostGift';
    const TYPE_GET_GIFT = 'getGift';
    const TYPE_PROMO = 'promo';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mail_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['content'], 'string'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Тип'),
            'content' => Yii::t('app', 'Текст'),
        ];
    }
}
