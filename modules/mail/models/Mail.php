<?php

namespace app\modules\mail\models;

use Yii;

/**
 * This is the model class for table "mail".
 *
 * @property integer $id
 * @property integer $template_id
 * @property integer $title
 * @property string $headers
 * @property string $from
 * @property string $to
 * @property string $content
 * @property string $date_add
 * @property string $date_send
 * @property string $addition_info
 */
class Mail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_id'], 'integer'],
            [['headers', 'title', 'content', 'addition_info'], 'string'],
            [['to'], 'required'],
            [['date_add', 'date_send'], 'safe'],
            [['from', 'to', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'template_id' => Yii::t('app', 'Шаблон'),
            'title' => Yii::t('app', 'Заголовок письма'),
            'headers' => Yii::t('app', 'Тех. заголовки'),
            'from' => Yii::t('app', 'Отправитель'),
            'to' => Yii::t('app', 'Получатель'),
            'content' => Yii::t('app', 'Текст письма'),
            'date_add' => Yii::t('app', 'Дата добавления'),
            'date_send' => Yii::t('app', 'Дата отправки'),
            'addition_info' => Yii::t('app', 'Доп. инфо'),
        ];
    }
}
