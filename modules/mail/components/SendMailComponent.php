<?php

namespace app\modules\mail\components;

use app\modules\mail\models\Mail;
use app\modules\mail\models\MailTemplate;
use app\modules\mainpage\models\Preference;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class SendMailComponent {
	public static function run($args = []) {
		$frequency = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_FREQUENCY])->one()->value;

		$host = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_HOST])->one()->value;
		$username = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_USERNAME])->one()->value;
		$password = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_PASSWORD])->one()->value;
		$port = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_PORT])->one()->value;
		$encryption = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_ENCRYPTION])->one()->value;

		$transport = [
			'class' => 'Swift_SmtpTransport',
			'host' => $host,
			'username' => $username,
			'password' => $password,
			'port' => $port,
			'encryption' => $encryption,
		];

		Yii::$app->mail->setTransport($transport);

		$sendEmailFlag = true;

		while ($sendEmailFlag == true) {

			$mail = Mail::find()->where(['date_send' => NULL])->one();

			if (empty($mail)) {
				$sendEmailFlag = false;
				return;
			}

			echo 'sending mail to ' . $mail->to . "\r\n";

			$body = $mail->content;
			if (!empty($mail->addition_info)) {
				foreach (json_decode($mail->addition_info) as $name => $item) {
					$body = str_replace('{{' . $name . '}}', $item, $body);
				}
			}

			$body = str_replace('{{frontSiteUrl}}', Yii::$app->params['frontSiteUrl'], $body);

			$mail->date_send = date('Y-m-d H:i:s');
			$mail->save();

			Yii::$app->mail->compose()
				->setFrom($username)
				->setTo($mail->to)
				->setSubject($mail->title)
				->setHtmlBody($body)
				->send();

			sleep($frequency);
		}
	}

	// Успешная регистрация
	public static function successRegister($user) {
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_REGISTRATION_SUCCESS_CONFIRM])->one();

		$mail = new Mail;
		$mail->to = $user->email;
		$mail->title = Yii::t('app', 'Успешная регистрация на {site}', ['site' => Url::home(true)]);
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;

		$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['confirm', 'token' => $user->email_confirm_token]);

		$additionInfo = [
			'login' => Html::encode($user->login),
			'siteUrl' => Url::home(true),
			'confirmLink' => Html::a(Html::encode($confirmLink), $confirmLink),
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	public static function lostGift($user, $childUser, $sum, $level) {
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_LOST_GIFT])->one();

		$mail = new Mail;
		$mail->to = $user->email;
		$mail->title = Yii::t('app', 'Вы можете пропустить подарок на {site} !', ['site' => Url::home(true)]);
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;

		$additionInfo = [
			'login' => Html::encode($user->login),
			'childLogin' => $childUser->login,
			'childEmail' => $childLogin->email,
			'levelNum' => $level,
			'sum' => $sum,
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	public static function getGift($user, $childUser, $level) {
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_GET_GIFT])->one();

		$mail = new Mail;
		$mail->to = $user->email;
		$mail->title = Yii::t('app', 'Вам был отправлен подарок на  сайте {site}', ['site' => Url::home(true)]);
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;

		$additionInfo = [
			'login' => Html::encode($user->login),
			'childLogin' => $childLogin->login,
			'levelNum' => $levelNum,
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	// Письмо партнёру о том что в его структуре зарегистрировался участник
	public static function successPartnerRegister($user) {
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_PARTNER_REGISTER_SUCCESS_CONFIRM])->one();

		$mail = new Mail;
		$mail->to = $user->parent()->one()->email;
		$mail->title = Yii::t('app', 'В Вашей структуре появился новый участник');
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;

		$additionInfo = [
			'login' => Html::encode($user->parent()->one()->login),
			'partner_login' => Html::encode($user->login),
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	// Сброс пароль
	public static function passwordReset($user, $password) {
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_RESET_PASSWORD])->one();

		$mail = new Mail;
		$mail->to = $user->email;
		$mail->title = Yii::t('app', 'Восстановление пароля на сайте {site}', ['site' => Url::home(true)]);
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;

		$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['reset',
			'token' => $user->password_reset_token,
			'password' => $password,
		]);

		$resetPasswordLink = Html::a(Html::encode($confirmLink), $confirmLink);

		$additionInfo = [
			'login' => Html::encode($user->login),
			'password' => Html::encode($password),
			'resetPasswordLink' => $resetPasswordLink,
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	// Подтверждение E-mail
	public static function confirmEmailCode($user, $email) {
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_CONFIRM_EMAIL_CODE])->one();

		$mail = new Mail;
		$mail->to = $email;
		$mail->title = Yii::t('app', 'Код подтверждения на сайте: {site}', ['site' => Url::home(true)]);
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;
		$additionInfo = [
			'login' => Html::encode($user->login),
			'emailConfirmToken' => Html::encode($user->email_confirm_token),
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	// Подтверждение Transaction
	public static function confirmTransaction($user, $email, $tx_info) {
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_SEND_TRANSACTION_CONFIRM_CODE])->one();

		$mail = new Mail;
		$mail->to = $email;
		$mail->title = Yii::t('app', 'Код подтверждения транзакции на сайте: {site}', ['site' => Url::home(true)]);
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;
		$additionInfo = [
			'login' => Html::encode($user->login),
            'code' => $tx_info['code'],
            'currency' => MerchantHelper::prettyCoinName($tx_info['currency']),
            'address' => $tx_info['address'],
            'amount' => $tx_info['amount']
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	// Отключение E-mail
	public static function unConfirmEmailCode($user, $email) {
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_UNCONFIRM_EMAIL_CODE])->one();

		$mail = new Mail;
		$mail->to = $email;
		$mail->title = Yii::t('app', 'Код отключения E-mail от аккаунта на сайте {site}', ['site' => Url::home(true)]);
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;
		$additionInfo = [
			'login' => Html::encode($user->login),
			'emailConfirmToken' => Html::encode($user->email_confirm_token),
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	// Платёжный пароль
	public static function payPassword($user, $payPassword) {
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_PAY_PASSWORD])->one();

		$mail = new Mail;
		$mail->to = $user->email;
		$mail->title = Yii::t('app', 'Ваш платёжный пароль на сайте {site}', ['site' => Url::home(true)]);
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;
		$additionInfo = [
			'login' => Html::encode($user->login),
			'payPassword' => Html::encode($payPassword),
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	// Отправка приглашения от пользователя
	public function sendInviteByEMail($user, $email){
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_SEND_INVITE_BY_MAIL])->one();

		$mail = new Mail;
		$mail->to = $email;
		$mail->title = Yii::t('app', 'Ваш партнёр приглашает принять участие на {site}', ['site' => Url::home(true)]);
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;
		$additionInfo = [
			'email' => Html::encode($user->email),
			'login' => Html::encode($user->login),
			'emailInviter' => Html::encode($email),
			'partnerLink' => Url::home(true) . 'signup?ref=' . $user->login
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	// Промо рассылка
	public function sendPromo($user, $email){
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_PROMO])->one();

		$mail = new Mail;
		$mail->to = $email;
		$mail->title = Yii::t('app', 'Hello Partner, Ingamo Fund online');
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;
		$additionInfo = [
			'email' => Html::encode($user->email),
			'login' => Html::encode($user->login),
		];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}

	// тест отправки письма
	public function testEmail($host, $username, $password, $port, $encryption, $email)
	{
		echo 'host: ' . $host . ' username: ' . $username . ' password: ' . $password . ' port: ' . $port . ' encryption: ' . $encryption . "\r\n";

		$transport = [
			'class' => 'Swift_SmtpTransport',
			'host' => $host,
			'username' => $username,
			'password' => $password,
			'port' => $port,
			'encryption' => $encryption,
		];

		Yii::$app->mail->setTransport($transport);

		echo 'sending mail to ' . $email . "\r\n";

		$body = 'this is a test message from me, fo not reply';

		Yii::$app->mail->compose()
			->setFrom($username)
			->setTo($email)
			->setSubject('simple test message')
			->setHtmlBody($body)
			->send();
	}

	public function inviteForActivateLK($email)
	{
		$mailTemplate = MailTemplate::find()->where(['type' => MailTemplate::TYPE_INVITE_FOR_ACTIVATE_LK])->one();

		$mail = new Mail;
		$mail->to = $email;
		$mail->title = Yii::t('app', 'Activate your account for profit in Ingamo');
		$mail->content = $mailTemplate->content;
		$mail->template_id = $mailTemplate->id;
		$additionInfo = [];

		$mail->date_add = date('Y-m-d H:i:s');
		$mail->addition_info = json_encode($additionInfo);

		$mail->save();
	}
}
