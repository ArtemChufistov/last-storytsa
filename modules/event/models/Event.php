<?php

namespace app\modules\event\models;

use Yii;
use app\modules\profile\models\User;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $type
 * @property integer $user_id
 * @property integer $to_user_id
 * @property double $sum
 * @property integer $currency_id
 * @property string $date_add
 * @property string $addition_info
 */
class Event extends \yii\db\ActiveRecord
{
    const TYPE_REF_LINK = 'ref_link';
    const TYPE_REG = 'reg';
    const TYPE_BUY_PLACE = 'buy_place';
    const TYPE_BUY_LEVEL_1 = 'buy_level_1';
    const TYPE_BUY_LEVEL_2 = 'buy_level_2';
    const TYPE_BUY_LEVEL_3 = 'buy_level_3';
    const TYPE_BUY_LEVEL_4 = 'buy_level_4';
    const TYPE_BUY_LEVEL_5 = 'buy_level_5';
    const TYPE_BUY_LEVEL_6 = 'buy_level_6';
    const TYPE_LOST_GIFT = 'lost_gift';
    const TYPE_GET_GIFT = 'get_gift';

    public static function getTypeArray()
    {
        return [
            self::TYPE_REF_LINK => Yii::t('app', 'Переход по REF ссылке'),
            self::TYPE_REG => Yii::t('app', 'Регистрация партнёра'),
            self::TYPE_BUY_PLACE => Yii::t('app', 'Покупка места партнёром'),
            self::TYPE_BUY_LEVEL_1 => Yii::t('app', 'Покупка партнёром 1-го уровня'),
            self::TYPE_BUY_LEVEL_2 => Yii::t('app', 'Покупка партнёром 2-го уровня'),
            self::TYPE_BUY_LEVEL_3 => Yii::t('app', 'Покупка партнёром 3-го уровня'),
            self::TYPE_BUY_LEVEL_4 => Yii::t('app', 'Покупка партнёром 4-го уровня'),
            self::TYPE_BUY_LEVEL_5 => Yii::t('app', 'Покупка партнёром 5-го уровня'),
            self::TYPE_BUY_LEVEL_6 => Yii::t('app', 'Покупка партнёром 6-го уровня'),
            self::TYPE_LOST_GIFT => Yii::t('app', 'Пропуск подарка'),
            self::TYPE_GET_GIFT => Yii::t('app', 'Получение подарка'),
        ];
    }

    public function getTypeTitle()
    {
        $array = self::getTypeArray();

        return $array[$this->type];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['user_id', 'to_user_id', 'currency_id'], 'integer'],
            [['sum'], 'number'],
            [['date_add'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Тип'),
            'user_id' => Yii::t('app', 'Участник'),
            'to_user_id' => Yii::t('app', 'Участнику'),
            'sum' => Yii::t('app', 'Sum'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'date_add' => Yii::t('app', 'Дата'),
            'addition_info' => Yii::t('app', 'Addition Info'),
        ];
    }
}
