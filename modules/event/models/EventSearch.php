<?php

namespace app\modules\event\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\event\models\Event;

/**
 * EventSearch represents the model behind the search form of `app\modules\event\models\Event`.
 */
class EventSearch extends Event
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'to_user_id', 'currency_id'], 'integer'],
            [['type', 'date_add', 'addition_info'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        
        $dataProvider->pagination->pageSize = 10;
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'to_user_id' => $this->to_user_id,
            'sum' => $this->sum,
            'currency_id' => $this->currency_id,
        ]);

        if ($this->date_add != ''){
            $query->andFilterWhere(['between', 'date_add', date('Y-m-d 0:0:0', strtotime($this->date_add)), date('Y-m-d 23:59:59', strtotime($this->date_add))]);
        }

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'addition_info', $this->addition_info]);

        return $dataProvider;
    }
}
