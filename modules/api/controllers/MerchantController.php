<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 01.06.18
 * Time: 15:19
 */

namespace app\modules\api\controllers;


use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use app\modules\merchant\models\MAccount;
use app\modules\merchant\models\MAccountApiRequestLog;
use app\modules\merchant\models\MAccountWallet;
use app\modules\merchant\models\MNodesTransactions;
use app\modules\merchant\models\MPaymentTasks;
use app\modules\merchant\models\MSendTxid;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;
use ErrorException;
use yii\data\Pagination;

class MerchantController extends Controller
{
    private $merchantId;
    private $user_id;
    private $secret;
    private $options;
    private $currency;
    private $command;


    public $layout = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = 'jsonApi';
        $this->enableCsrfValidation = false;
        $this->checkParms();
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        return ["message" => $exception->getMessage()];
    }

    public function checkParms()
    {
        // print_r($_REQUEST);

        //принимаем POST/JSON
        // отправленная строка POST json
        $str = file_get_contents('php://input');
        if (isset(Yii::$app->request->post()['merchantId'])) {
            $request_arr = Yii::$app->request->post();
        } else {
            // пробуем создать массив из строки
            $request_arr = json_decode($str, true);
            if (!is_array($request_arr)) {
                throw new HttpException(500, "Нечитаемый запрос #153 " . json_encode($request_arr));
            }
        }


        $mAccountLogModel = new MAccountApiRequestLog();
        if (isset($request_arr['merchantId']))
            $mAccountLogModel->m_account_id = $request_arr['merchantId'];
        if (isset($request_arr['options']) && isset($request_arr['options']['currency']))
            $mAccountLogModel->currency = $request_arr['options']['currency'];
        if (isset($request_arr['options']) && isset($request_arr['options']['command']))
            $mAccountLogModel->command = $request_arr['options']['command'];


        $mAccountLogModel->request = json_encode($request_arr);
        $mAccountLogModel->save();


        $arr =
            [
                "merchantId",
                "secret",
                "options"
            ];

        if ($arr) {
            foreach ($arr as $v) {

                if (!$request_arr[$v]) {
                    throw new HttpException(500, "Потерян параметр - {$v}");
                }

                // Идентифицируем пользователя
                if ($v == 'merchantId') {

                    if (($model = MAccount::findOne($request_arr['merchantId'])) === null) {
                        throw new HttpException(500, "Доступ запрещен #152");
                    }
                    //TODO ломимся в базу по мерчант id
                    $secret = $request_arr['secret'];
                    if (isset($secret) && $secret == $model->secret) {
                        continue;
                    }

                    throw new HttpException(500, "Доступ запрещен #154");
                }

                // проверяем обязательные параметры опций
                if ($v == 'options' && $request_arr[$v]) {
                    $options = $request_arr[$v];

                    if (!is_array($options))
                        throw new HttpException(500, "Должен быть массивом - {$v}");

                    $oprions_required_arr = ['currency', 'command'];

                    if (isset($options['command']) && $options['command'] != 'getCurrenciesList' && $options['command'] != 'getDefaultFeesList') {
                        foreach ($oprions_required_arr as $v) {
                            if (!isset($options[$v])) {
                                throw new HttpException(500, "Потерян параметр options - {$v}");
                            }
                        }
                    }
                }
            }


            $this->merchantId = $request_arr['merchantId'];
            $this->secret = $request_arr['secret'];
            $this->options = $request_arr['options'];
            $this->command = $this->options['command'];

            if ($this->command != 'getCurrenciesList' && $this->command != 'getDefaultFeesList') {
                $this->currency = $this->options['currency'];
                if (!isset(MerchantHelper::avalableWalletsFromMerchantModuleArray()[$this->options['currency']])) {
                    throw new HttpException(500, "Валюта не определена");
                }
            }

            /** @var MAccount $mAccountModel */
            $mAccountModel = MAccount::find()->where(['id' => $this->merchantId])->one();
            $this->user_id = $mAccountModel->user_id;

        }
    }

//    public function actionIndex()
//    {
//        $result = "API Service";
//        return $result;
//    }


    public function actionV1()
    {

        $avalable_functions = [
            'generateAddress', // + ++ сгенерировать адрес
            'showAddresses', // + ++ получить адреса кошельков
            'getAddressesBalanceSum', // + +- общий баланс кошелька аккаунта
            'sendFromMerchant', // + + отправить платеж c мерчант аккаунта на кошелек

            'getTransactionsList', // + получить список транзакций
            'getTransactionsInfoById', // + получить информацию по транзакции
            'getCurrenciesList', // Списоу валют
            'getDefaultFeesList' // Список комисий по умолчанию


//            'getTxidInfo', // + получить транзакшн инфо

            // todo проверка аккаунта
//            'getTxidsByAddress', // получить транзакции по адресу
            //'getAddressBalance', // +- получить баланс адреса
//            'testSendFrom', // отправить кол-во на адрес
//            'sendFrom', // отправить кол-во на адрес
//            'getCommonWalletBalance',
//            'getAllMerchantBalances'

        ];

        $func = $this->command;

        if (in_array($func, $avalable_functions) && $func != 'getCurrenciesList' && $func != 'getDefaultFeesList') {
            return ArrayHelper::merge($this->$func(), ['currency' => $this->currency, 'currency_logo' => MerchantHelper::coinLogo($this->currency)]);
        } else {
            return $this->$func();
        }


        throw new HttpException(400, "Нет такой функции");
    }


    /**
     * ++ Список комиссий
     * @return array
     */
    private function getDefaultFeesList()
    {
        $feesList = [];
        foreach (MerchantHelper::avalableWalletsFromMerchantModuleArray() as $currency => $name) {
            $feesList[$currency] = MerchantHelper::nodeFee($currency);
        }

        return ['defaultFeesList' => $feesList];
    }

    /**
     * ++ Получить список валют
     * @return array
     */
    private function getCurrenciesList()
    {
        $currencies = MerchantHelper::avalableWalletsFromMerchantModuleArray();

        $currencyList = [];
        foreach ($currencies as $currency => $name) {
            $currencyList[$currency] = [
                'logo' => MerchantHelper::coinLogo($currency),
                'name' => $name
            ];
        }

        return ['currencyList' => $currencyList];
    }

    /**
     * ++ Сгенерировать адрес
     * @return array
     */
    private function generateAddress()
    {
        $newAaddress = Yii::$app->get($this->currency)->generateAddress($this->merchantId, $this->user_id);

        return ['newAaddress' => $newAaddress];
    }

    /**
     * ++ Получить адреса кошельков
     * @return array
     */
    private function showAddresses()
    {
        $count = isset($this->options['count']) ? $this->options['count'] : 1000;
        $page_num = isset($this->options['page_num']) ? $this->options['page_num'] : 0;
        $merchant_only = isset($this->options['merchant_only']) ? $this->options['merchant_only'] : false;

        $total = MAccountWallet::find()->where(['user_id' => $this->user_id])->andWhere(['currency_type' => $this->currency]);
        if ($merchant_only) {
            $total->andWhere(['m_account_id' => $this->merchantId]);
        }
        $total = $total->count();


        $mWalletAll = MAccountWallet::find()->where(['user_id' => $this->user_id])->andWhere(['currency_type' => $this->currency])
            ->limit($count)
            ->offset($page_num * $count)
            ->orderBy('created_at DESC')
            ->asArray();

        if ($merchant_only) {
            $mWalletAll->andWhere(['m_account_id' => $this->merchantId]);
        }
        $mWalletAll = $mWalletAll->all();


        $addresses = ArrayHelper::map($mWalletAll, 'currency_wallet', 'currency_wallet');

        return ['addresses' => array_values($addresses), 'addresses_total' => $total];
    }

    /**
     * ++ Получить баланс адреса
     * @return array
     */
    private function getAddressBalance()
    {
        $balance = Yii::$app->get($this->currency)->getAddressBalance($this->user_id, $this->options['address'], $this->options['confirmations_num']);

        return ['address_balance' => $balance];
    }

    /**
     * Общий баланс кошелька аккаунта
     * @return array
     */
    private function getAddressesBalanceSum()
    {
        $balance = Yii::$app->get($this->currency)->getAddressesBalanceSum($this->user_id, $this->options['confirmations_num']);

        return ['balance' => $balance];
    }


    /**
     * Отправить средства (Создаем пустую транзакцию)
     * @return array
     */
    private function sendFromMerchant()
    {
//    (поля fee_per_byte fee_per_transaction )

        $feePaySender = isset($this->options['feePaySender']) ? $this->options['feePaySender'] : true;

//        $fee = isset($this->options['fee']) ? $this->options['fee'] : false;
        $fee_per_transaction = isset($this->options['fee_per_transaction']) ? $this->options['fee_per_transaction'] : null;
        $fee_per_byte = isset($this->options['fee_per_byte']) ? $this->options['fee_per_byte'] : null;

        if ($this->options['amount'] <= 0) {
            throw new ErrorException('sendFromMerchant error: amount can\'t be negative or zero');
        }

        $crypchant_transaction_id = MerchantHelper::createNewSendTransaction($this->user_id, $this->merchantId, $this->currency,
            $this->options['toAddress'], $this->options['amount'], 0, MPaymentTasks::REQUEST_TYPE_API,
            MPaymentTasks::SHOW_TO_USER, $feePaySender, $fee_per_transaction, $fee_per_byte);

        return ['id' => $crypchant_transaction_id];
    }


    /**
     * Получить рекомендуемые комиссии
     * @return array
     */
    private function getRecomendedComission()
    {
        $balance = Yii::$app->get($this->currency)->getRecomendedComission($this->merchantId);

        return ['comissions' => $balance];
    }


    /**
     * Получить список транзакций
     * @return array
     */
    private function getTransactionsList()
    {
        $count = isset($this->options['count']) ? $this->options['count'] : 20;
        $page_num = isset($this->options['page_num']) ? $this->options['page_num'] : 0;
        $confirmations = isset($this->options['confirmations']) ? $this->options['confirmations'] : false;

        $total = MNodesTransactions::find()
            ->where(['user_id' => $this->user_id])
            ->andWhere(['currency' => $this->currency])
            ->count();

        $txList = MNodesTransactions::find()
            ->select("id, is_processing_complete, txid, currency, category, to_addr, amount, fee, created_at, confirmations, blockchain_time")
            ->where(['user_id' => $this->user_id])
            ->andWhere(['currency' => $this->currency])
            ->limit($count)
            ->offset($page_num * $count)
            ->orderBy(['id' => SORT_DESC])
//            ->asArray()
            ->all();
        // ->createCommand()->getRawSql();


        $newTxList = [];
        /** @var MNodesTransactions $tx */
        foreach ($txList as $tx) {
            if ($tx->is_processing_complete == MPaymentTasks::IS_COMPLETE && isset($tx->txid)) {
                $txidInfo = Yii::$app->get($this->currency)->getTransaction($tx->txid);
                if ($tx->confirmations != $txidInfo['confirmations']) {
                    $tx->confirmations = $txidInfo['confirmations'];
                    $tx->save();
                }
            }

            if ($tx['is_processing_complete'] == MNodesTransactions::IS_IN_PROCESSING) {
                $tx['txid'] = '';
            }
            $newTxList[] = $tx;
        }


        // если задано кол-во подтверждений
        if($confirmations){

            $total = MNodesTransactions::find()
                ->where(['user_id' => $this->user_id])
                ->andWhere(['currency' => $this->currency])
                ->andWhere('confirmations <= '.(int)$confirmations)
                ->count();

            $txList = MNodesTransactions::find()
                ->select("id, is_processing_complete, txid, currency, category, to_addr, amount, fee, created_at, confirmations, blockchain_time")
                ->where(['user_id' => $this->user_id])
                ->andWhere(['currency' => $this->currency])
                ->andWhere('confirmations <= '.(int)$confirmations)
                ->limit($count)
                ->offset($page_num * $count)
                ->orderBy(['id' => SORT_DESC])
                ->all();

            $newTxList = [];
            /** @var MNodesTransactions $tx */
            foreach ($txList as $tx) {
                if ($tx->is_processing_complete == MPaymentTasks::IS_COMPLETE && isset($tx->txid)) {
                    $txidInfo = Yii::$app->get($this->currency)->getTransaction($tx->txid);
                    if ($tx->confirmations != $txidInfo['confirmations']) {
                        $tx->confirmations = $txidInfo['confirmations'];
                        $tx->save();
                    }
                }

                if ($tx['is_processing_complete'] == MNodesTransactions::IS_IN_PROCESSING) {
                    $tx['txid'] = '';
                }
                $newTxList[] = $tx;
            }
        }


        return ['transactionsList' => $newTxList, 'tx_total' => $total];
    }

    /**
     * Получить информацию по транзакции
     * @return array
     */
    private function getTransactionsInfoById()
    {
        $id = isset($this->options['id']) ? (int)$this->options['id'] : false;

        if ($id) {

            /** @var MNodesTransactions $MNTModel */
            $MNTModel = MNodesTransactions::find()
                ->select("id, is_processing_complete, txid, currency, category, to_addr, amount, fee, created_at, confirmations, blockchain_time")
                ->where(['id' => $id])
                ->one();

            if (!isset($MNTModel)) {
                throw new ErrorException('getTransactionsInfoById error: undefined id #2');
            } else {

                // обновляем подтверждения если существует txid
                if ($MNTModel->is_processing_complete == MPaymentTasks::IS_COMPLETE && isset($tx->txid)) {
                    $txidInfo = Yii::$app->get($this->currency)->getTransaction($MNTModel->txid);
                    if ($MNTModel->confirmations != $txidInfo['confirmations']) {
                        $MNTModel->confirmations = $txidInfo['confirmations'];
                        $MNTModel->save();
                    }
                }

                if ($MNTModel->is_processing_complete == MNodesTransactions::IS_IN_PROCESSING) {
                    $MNTModel->txid = '';
                }
            }

            return ['transactionInfo' => $MNTModel];
        } else {
            throw new ErrorException('getTransactionsInfoById error: undefined id #1');
        }

    }

    /**
     * Получить информацию по транзакции
     * @return array
     */
    private function getTxidInfo()
    {
        $txidInfo = Yii::$app->get($this->currency)->getTransaction($this->options['txid']);

        return ['transactionInfo' => $txidInfo];
    }

    /**
     * Получить транзакции по адресу
     * @return array
     */
    private function getTxidsByAddress()
    {
        $transactions = Yii::$app->get($this->currency)->getTransactionsListByAddress($this->merchantId, $this->options['address']);

        return ['transactions' => $transactions];
    }

    /**
     * Отправить средства
     * @return array
     */
    private function sendFrom()
    {
        $this->options['fee'] = isset($this->options['fee']) ? $this->options['fee'] : '';
        $this->options['feePaySender'] = isset($this->options['feePaySender']) ? $this->options['feePaySender'] : '';

        $txid = Yii::$app->get($this->currency)->sendFrom($this->merchantId, $this->options['toAddress'],
            $this->options['amount'], $this->options['fee'], $this->options['feePaySender']);

        return ['txid' => $txid];
    }


    /**
     * Балансы всех кошельков
     * @return array
     */
    private function getAllMerchantBalances()
    {
        $walletsArr = MerchantHelper::avalableWalletsFromMerchantModuleArray();

        $walletsBalances = [];
        foreach ($walletsArr as $wallet => $prettyWallet) {
            $walletsBalances[$wallet] = Yii::$app->get($wallet)->getAccountBalance($this->merchantId);
        }

        return ['all_merchant_balances' => $walletsBalances];
    }

}