<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 01.06.18
 * Time: 15:19
 */

namespace app\modules\api\controllers;

use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use app\modules\merchant\models\MAccountWallet;
use app\modules\merchant\models\MNodesTransactions;
use app\modules\merchant\models\MPaymentTasks;
use app\modules\merchant\models\MSendTxid;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use app\modules\profile\models\User;

class NotifyController extends Controller
{
    public $layout = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = 'jsonApi';
        $this->enableCsrfValidation = false;
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        return ["message" => $exception->getMessage()];
    }

    public function bcoreAltcoinTxSave($currency, $txid)
    {

        if (isset($currency) && isset($txid)) {

            if (!isset(MerchantHelper::avalableWalletsFromMerchantModuleArray()[$currency])) {
                throw new HttpException(500, "Валюта не определена");
            }

            /////////////////////////////////////////////
            // обновляем существующую запись
            /** @var MPaymentTasks $mPaymentTask */
            $mPaymentTask = MPaymentTasks::find()->where(['txid' => $txid])
                ->andWhere(['currency' => $currency])
                ->andWhere(['is_processing_complete' => MPaymentTasks::IS_IN_PROCESSING])
                ->one();
            if (isset($mPaymentTask)) {
                /** @var MNodesTransactions $mNodTransActionModel */
                $mNodTransActionModel = MNodesTransactions::find()
                    ->where(['m_payment_task_id' => $mPaymentTask->id])
                    ->andWhere(['is_processing_complete' => MNodesTransactions::IS_IN_PROCESSING])
                    ->one();
                if (isset($mNodTransActionModel)) {
                    $mNodTransActionModel->txid = $txid;
                    $mNodTransActionModel->blockchain_time = $mPaymentTask->blockchain_time;
                    $mNodTransActionModel->fee = $mPaymentTask->fee;
                    $mNodTransActionModel->is_processing_complete = MNodesTransactions::IS_COMPLETE;

                    if (!$mNodTransActionModel->save()) {
                        throw new HttpException(500, "Ошибка обновления транзакции");
                    }

                    $mPaymentTask->is_processing_complete = MPaymentTasks::IS_COMPLETE;
                    $mPaymentTask->save();
                }
            }

            ////////////////////////////////////////////////

            echo "<pre>";
            echo "<br>";
            echo $txid;
            echo "<br><br>";

            $txInfo = Yii::$app->get($currency)->getTransaction($txid);
            print_r($txInfo);
            foreach ($txInfo['details'] as $addressTxInfo) {

                if (!isset($addressTxInfo['address']))
                    throw new HttpException(500, "Не найден адрес в базе");
                print_r($addressTxInfo);

                /** @var MAccountWallet $mWallet */
                $mWallet = MAccountWallet::find()->where(['currency_wallet' => $addressTxInfo['address']])->one();
                $m_account_id = isset($mWallet->m_account_id) ? $mWallet->m_account_id : null;


                $allExistedTransactions = MNodesTransactions::find()->where(['txid' => $txid])->andWhere(['user_id' => $addressTxInfo['account']])->all();

                if (count($allExistedTransactions) == 0) {

                    $nodeTransactionModel = new MNodesTransactions();
                    $nodeTransactionModel->txid = $txid;
                    $nodeTransactionModel->currency = $currency;
                    $nodeTransactionModel->user_id = $addressTxInfo['account'];
                    $nodeTransactionModel->m_account_id = $m_account_id;
                    //   $nodeTransactionModel->amount = $txInfo['amount'];
                    $nodeTransactionModel->confirmations = $txInfo['confirmations'];
                    $nodeTransactionModel->blockchain_time = date("Y-m-d H:i:s", $txInfo['time']);
                    $nodeTransactionModel->to_addr = $addressTxInfo['address']; //=> 3H8Q6LDGVmBRuyBxPW643eCBuyBwmTPqot
                    $nodeTransactionModel->category = $addressTxInfo['category']; //=> receive
                    $nodeTransactionModel->amount = $addressTxInfo['amount'];   //=> 0.0001

                    if (isset($addressTxInfo['fee']))
                        $nodeTransactionModel->fee = $addressTxInfo['fee'];   //=> 0.0001

                    if (!$nodeTransactionModel->save()) {
                        print_r($nodeTransactionModel->getErrors());
                        throw new HttpException(500, "Ошибка обработки");
                    }

                    print_r($nodeTransactionModel->getErrors());

                } else {
                    /** @var MNodesTransactions $transaction */
                    foreach ($allExistedTransactions as $transaction) {
                        $transaction->confirmations = $txInfo['confirmations'];

                        if (!$transaction->save()) {
                            print_r($transaction->getErrors());
                            throw new HttpException(500, "Ошибка обработки");
                        }
                    }
                }
            }
            return ['crypchant_receive_status' => 'ok', 'txid' => $txid];
        }

        throw new HttpException(500, "Notifier Error : #523");
    }


    public function actionRefresh()
    {
        echo "<pre>";
        /** @var User $user */
        foreach (User::find()->all() as $user) {
            foreach (MerchantHelper::avalableWalletsFromMerchantModuleArray() as $currency => $prettyName) {
                $list = Yii::$app->get($currency)->getTransactionsList($user->id);
                foreach ($list as $txInfo) {
                    if (isset($txInfo['txid'])) {
                        $this->bcoreAltcoinTxSave($currency, $txInfo['txid']);
                    }
                }
            }
        }
    }


    /**
     * Слушатель биткоин ноды
     *
     * @return array
     */
    public function actionBitcoinListener($txid)
    {
        $currency = 'bitcoin';

        $this->bcoreAltcoinTxSave($currency, $txid);
    }

    /**
     * Слушатель лайткоин ноды
     *
     * @return array
     */
    public function actionLitecoinListener($txid)
    {
        $currency = 'litecoin';

        $this->bcoreAltcoinTxSave($currency, $txid);
    }

    /**
     * Слушатель биткоин кеш ноды
     *
     * @return array
     */
    public function actionBitcoinCashListener($txid)
    {
        $currency = 'bitcoin-cash';

        $this->bcoreAltcoinTxSave($currency, $txid);
    }

    /**
     * Слушатель догикоин ноды
     *
     * @return array
     */
    public function actionDogecoinListener($txid)
    {
        $currency = 'dogecoin';

        $this->bcoreAltcoinTxSave($currency, $txid);
    }
}