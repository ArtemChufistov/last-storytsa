<?php

namespace app\modules\api\controllers;

use app\admin\modules\altcoin\models\TransferForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends Controller
{
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => ['index', 'addresses', 'newaddress', 'info'],
//                        'roles' => ['@'],
//                        'matchCallback' => function ($rule, $action) {
//                            return in_array(strtolower(Yii::$app->user->identity->email), $this->module->allowedUsers);
//                        },
//                    ],
//                ],
//            ],
//            [
//                'class' => 'yii\filters\PageCache',
//                'only' => ['index'],
//                'duration' => $this->module->mainPageCache,
//            ],
//        ];
//    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
               // 'only' => ['index', 'view', 'create', 'update', 'delete', 'multidelete', 'multiactive', 'multiblock', 'move', 'change', 'field'],
                'rules' => [
                    [
                        'actions' => ['index', 'addresses', 'newaddress', 'info'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        $this->layout = $this->module->layout;
    }

    public function actionIndex()
    {
        $wallets = [];

        foreach ($this->module->wallets as $wallet) {

            $wallets[$wallet]['name'] = $wallet;

            try {
                $wallets[$wallet]['info'] = Yii::$app->get($wallet)->getInfo();
            } catch (\Exception $e) {
                $wallets[$wallet]['info']['balance'] = -1;
                \Yii::error($e->getMessage());
            }

        }

        $transferForm = new TransferForm();

        if ($transferForm->load(Yii::$app->request->post()) && $transferForm->validate()) {

            $txid = Yii::$app->get($transferForm->currency)->send($transferForm->address, $transferForm->amount);

            $msg = "Перевод $transferForm->amount на $transferForm->address успешно завершен. Txid: " . $txid;

            Yii::$app->session->setFlash('transfer', $msg);
//            $this->refresh();
        }

        return $this->render('index', [
            'wallets' => $wallets,
            'transferForm' => $transferForm,
        ]);
    }

    public function actionAddresses($currency)
    {
        $addresses = [];

        try {
            $addresses = Yii::$app->get($currency)->showAddresses();
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
        }

        return $this->render('addresses', ['addresses' => $addresses, 'currency' => $currency]);
    }

    public function actionNewaddress($currency)
    {
        try {
            $address = Yii::$app->get($currency)->generateAddress();
//            echo $this->renderContent("Новый сгенерированный $currency адрес: " . $address);
            Yii::$app->session->setFlash('newAddress', $currency . " - " . $address);
//            Yii::$app->end();
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
        }

        return $this->redirect(['/altcoin-mod/default/index']);

    }

    public function actionInfo($currency)
    {
        $info = print_r(Yii::$app->get($currency)->getInfo(), true);

        if ($currency == 'bitcoin' || $currency == 'litecoin') {


        $info = print_r(Yii::$app->get($currency)->getClient()->getaccount('33v8uvmYmS9RvRzKALvSYhZ6FDCUiHgAfz'), true).'<br><br>';

// создаем новый аккаунт и новый адресс для него
//        $info .= print_r(Yii::$app->get($currency)->getClient()->getnewaddress('test_account'), true);

        $info .= print_r(Yii::$app->get($currency)->getClient()->getaddressesbyaccount('bitcoinrpc'), true) . "<br><br>";
        $info .= print_r(Yii::$app->get($currency)->getClient()->getaddressesbyaccount('test_account'), true);
        $info .= print_r(Yii::$app->get($currency)->getClient()->getaccountaddress('test_account'), true) . "<br><br>";
//        $info .= print_r(Yii::$app->get($currency)->getClient()->setaccount('test_account'), true);
        $info .= print_r(Yii::$app->get($currency)->getClient()->listaccounts(), true);

        $info .= print_r(Yii::$app->get($currency)->getInfo(), true);
        $info .= print_r(Yii::$app->get($currency)->getClient()->getmininginfo(), true);
        $info .= print_r(Yii::$app->get($currency)->getClient()->getmemoryinfo(), true);
//        getmininginfo
        $info .= print_r(Yii::$app->get($currency)->getClient()->getnetworkinfo(), true);
        $info .= print_r(Yii::$app->get($currency)->getClient()->getblockchaininfo(), true);

        $info .= print_r(Yii::$app->get($currency)->getClient()->getblockchaininfo(), true);

            $info .= print_r(Yii::$app->get($currency)->getClient()->getaddressesbyaccount('bitcoinrpc'), true);


        } elseif ($currency == 'ethereum') {

            $data = Yii::$app->get($currency)->getClient()->call('eth_accounts', []);
            $info .= print_r($data['result'], true);

            $data = Yii::$app->get($currency)->getClient()->call('eth_getBlockByHash', ['0xcb83d3dc792449782582a1e768ca883f3e1eb318', true]);
            $info .= print_r($data, true);

            $data = Yii::$app->get($currency)->getClient()->call('eth_newBlockFilter', []);
            $info .= print_r($data, true);

            $data = Yii::$app->get($currency)->getClient()->call('eth_getFilterChanges', [$data['result']]);
            $info .= print_r($data, true);


            // активный базовый адресс
            $data = Yii::$app->get($currency)->getClient()->call('eth_coinbase', []);
            $info .= print_r($data, true);

            $data = Yii::$app->get($currency)->getClient()->call('eth_mining', []);
            $info .= print_r($data, true);

            $info .= "<b>Returns the number of hashes per second that the node is mining with.</b><br>";
            $data = Yii::$app->get($currency)->getClient()->call('eth_hashrate', []);
            $info .= print_r(hexdec($data['result']), true);
            $info .= "<p><b>gasPrice in wei.</b></p>";
            $data = Yii::$app->get($currency)->getClient()->call('eth_gasPrice', []);
            $info .= print_r(hexdec($data['result']).' - '.$data['result'], true);
            $info .= "<p><b>Returns the number of transactions sent from an address.</b></p>";
            $data = Yii::$app->get($currency)->getClient()->call('eth_getTransactionCount', ['0x8057daaee5b7fd00be4c9a93cd41c17601dc09e0', 'latest']);
            $info .= print_r(hexdec($data['result']).' - '.$data['result'], true);
            $info .= "<p><b>Returns the number of transactions sent from an address.</b></p>";
            $data = Yii::$app->get($currency)->getClient()->call('eth_getCode', ['0x8057daaee5b7fd00be4c9a93cd41c17601dc09e0', '0x2']);
            $info .= print_r($data, true);

//            $info .= "<p><b>Generates and returns an estimate of how much gas is necessary to allow the transaction to complete.
//                            The transaction will not be added to the blockchain. Note that the estimate may be significantly more than the amount
//                            of gas actually used by the transaction, for a variety of reasons including EVM mechanics and node performance.</b></p>";
//            $data = Yii::$app->get($currency)->getClient()->call('eth_estimateGas', ["value" => "0x9184e72a"]);
//            $info .= print_r($data, true);

            $info .= "<p><b>Returns information about a block by hash.</b></p>";
            $data = Yii::$app->get($currency)->getClient()->call('eth_getBlockByHash', ['4943d941637411107494da9ec8bc04359d731bfd08b72b4d0edcbd4cd2ecb341', true]);
            $info .= print_r($data, true);

        }


        echo $this->renderContent("Информация по $currency: <pre>$info</pre>");



//        Yii::$app->response->format = Response::FORMAT_JSON;
//
//        return $info;
    }

}
