<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;

class DocController extends Controller
{
    public function init()
    {
        $this->layout = '@app/views/layouts/main.php';
        $this->enableCsrfValidation = false;
    }

    /**
     * Описание Api
     *
     * @return mixed
     */
    public function actionApi()
    {
        return $this->render('api');
    }



    public function actionTest(){

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return ['recive_status' => 'ok'];
    }
}
