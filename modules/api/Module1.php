<?php

namespace app\admin\modules\altcoin;

/**
 * Модуль альткоинов
 * унаследованный от модуля \lowbase\document\Module
 * Class Module
 * @package app\admin\document\document
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\admin\modules\altcoin\controllers';

    public $wallets = [];

    public $allowedUsers;

    public $mainPageCache = 0;

    public function init()
    {
        parent::init();
//        self::registerTranslations();
    }

    /**
     * Подключаем сообщения перевода
     */
    public static function registerTranslations()
    {
        if (!isset(\Yii::$app->i18n->translations['document']) && !isset(\Yii::$app->i18n->translations['document/*'])) {
            \Yii::$app->i18n->translations['document'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@lowbase/altcoin/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'document' => 'document.php'
                ]
            ];
        }
    }
}
