<?php

namespace app\modules\api\commands;

use app\admin\modules\altcoin\components\coinmarketcap\CoinMarketCap;
use app\admin\modules\altcoin\models\MCoinmarketcapTicker;
use app\modules\api\controllers\NotifyController;
use app\modules\merchant\components\cron\Refresher;
use app\modules\merchant\components\cron\TransactionProcessing;
use app\modules\merchant\components\cron\TxInfoSender;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use ErrorException;
use yii\console\Controller;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use Yii;
use ZMQ;
use ZMQContext;
use ZMQSocket;


class CryptoController extends Controller
{
    public $url = 'https://crypchant.com/api/v1';
    public $merchantId = 10122;
    public $secret = 'MUpKBrxgWYLrJJVf';


    public function send($options, $isJson = false)
    {


        $request = [
            "merchantId" => $this->merchantId,
            "secret" => $this->secret,
            "options" => $options
        ];


        if (!$isJson) {

            echo "POST request \n\n\n";

            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setUrl($this->url)
                ->setData($request)
                ->send();
            $response = json_decode($response->content, true);
            echo "\n\n";
            echo json_encode($request);
            echo "\n\n";
            echo json_encode($response);
            echo "\n\n";
//        echo print_r($response->content);

        } else {

            echo "JSON request \n\n\n";

            $data_string = json_encode($request);

            $ch = curl_init($this->url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);

            var_dump(json_decode($result));

        }


        $request = print_r($request, true);
        echo "Запрос: $request";
        $response = print_r($response, true);
        echo "Ответ: ".$response." \n\n";


    }

    /**
     * Сгенерировать адрес
     */
    public function actionGenerateAddress()
    {
        $options = [
            "currency" => 'dogecoin',
            "command" => 'generateAddress',
        ];

        $this->send($options);
    }

    /**
     * Получить адреса
     */
    public function actionShowAddress()
    {
        $options = [
            "currency" => 'bitcoin',
            "command" => 'showAddresses',
        ];

        $this->send($options, true);
//        $this->send($options);
    }

    /**
     * Баланс адреса
     */
    public function actionGetAddressBalance()
    {
        $options = [
            "currency" => 'ethereum',
            "command" => 'getAddressBalance',
            "address" => '0x0113fbb0c7201784c4500e6edb05d0ba7704854b',
            "confirmations_num" => 1,
        ];

        $this->send($options);
    }

    /**
     * Получить информацию по транзакции
     */
    public function actionGetTxidInfo()
    {
        $options = [
            "currency" => 'litecoin',
            "command" => 'getTxidInfo',
            "txid" => '1b468a9475df4ae5cf1baea1b75ce902aa5467bb6cbf854e5e056886b07b4cdd'
        ];

        $this->send($options);
    }

    /**
     * Получить транзакции по адресу
     */
    public function actionGetTxidsByAddress()
    {
        $options = [
            "currency" => 'litecoin',
            "command" => 'getTxidsByAddress',
            "address" => 'LiH3PA4ZF5e3n1AUvt8nDKUyvfAWZVHwFn',
        ];

        $this->send($options);
    }

    /**
     * Получить транзакции по адресу
     */
    public function actionGetTransactionsInfoById()
    {
        $options = [
            "currency" => 'dogecoin',
            "command" => 'getTransactionsInfoById',
            "id" => 314,
        ];

        $this->send($options);
    }

    /**
     * Отправить средства
     */
    public function actionSendFromMerchant()
    {
        $options = [
            "currency" => 'bitcoin',
            "command" => 'sendFromMerchant',
            "toAddress" => '34Mebs34f1go8uL4XuBoHc6FTgUGPejy6K',
            "amount" => '0.0001',
//            "feePaySender" => true,
//            "fee_per_transaction" => 0.0005,
            "fee_per_byte" => 1,



//            "fee" => 0.00002500
        ];

        $this->send($options);
    }

    /**
     * Общий баланс кошелька аккаунта
     */
    public function actionGetCommonWalletBalance()
    {
        $options = [
            "currency" => 'litecoin',
            "command" => 'getCommonWalletBalance',
            "confirmations_num" => 7,
        ];

        $this->send($options);
    }

    /**
     * Общий баланс кошелька аккаунта
     */
    public function actionGetTransactionsList()
    {
        echo date("'Y-m-d H:i:s'", time() - 1500);
        $options = [
            "currency" => 'dogecoin',
            "command" => 'getTransactionsList',
            "count" => 2,
            "page_num" => 0,
            "confirmations" => "5",
        ];

        $this->send($options);
    }


    /**
     * Баланс адреса
     */
    public function actionGetAllMerchantBalances()
    {
        $options = [
            "currency" => 'litecoin',
            "command" => 'getAllMerchantBalances',
        ];

        $this->send($options);
    }


    /**
     * Баланс адреса
     */
    public function actionGetCurrenciesList()
    {
        $options = [
            "command" => 'getDefaultFeesList',
        ];

        $this->send($options);
    }


    /**
     * Обновить биржевые цены криптовалют
     */
    public function actionRefresh()
    {

        $notify =  new NotifyController();
        $notify->actionRefresh();


        die;
//        $ticker = CoinMarketCap::getTicker(10, "USD");
        $ticker = CoinMarketCap::getTicker(10000, "USD");
        var_dump(count($ticker));


        Yii::$app->db->createCommand()->truncateTable(MCoinmarketcapTicker::tableName())->execute();

        foreach ($ticker as $oneTrick) {

            $model = new MCoinmarketcapTicker();

            $model->id = $oneTrick['id'];
            $model->name = $oneTrick['name'];
            $model->symbol = $oneTrick['symbol'];
            $model->rank = $oneTrick['rank'];
            $model->price_usd = $oneTrick['price_usd'];
            $model->price_btc = $oneTrick['price_btc'];
            $model->h24h_volume_usd = $oneTrick['24h_volume_usd'];
            $model->market_cap_usd = $oneTrick['market_cap_usd'];
            $model->available_supply = $oneTrick['available_supply'];
            $model->total_supply = $oneTrick['total_supply'];
            $model->max_supply = $oneTrick['max_supply'];
            $model->percent_change_1h = $oneTrick['percent_change_1h'];
            $model->percent_change_24h = $oneTrick['percent_change_24h'];
            $model->percent_change_7d = $oneTrick['percent_change_7d'];
            $model->last_updated = $oneTrick['last_updated'];


            $model->save();


        }


//        [
//            "id"=> "ethereum",
//            "name"=> "Ethereum",
//            "symbol"=> "ETH",
//            "rank"=> "2",
//            "price_usd"=> "610.981",
//            "price_btc"=> "0.0800268",
//            "24h_volume_usd"=> "1764530000.0",
//            "market_cap_usd"=> "61036524551.0",
//            "available_supply"=> "99899219.0",
//            "total_supply"=> "99899219.0",
//            "max_supply"=>  NULL,
//            "percent_change_1h"=> "0.38",
//            "percent_change_24h"=> "3.67",
//            "percent_change_7d"=> "7.04",
//            "last_updated" => "1528270162",
//        ];
    }


    /**
     * Процессинг алгоритм
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function actionProcessing()
    {


        TransactionProcessing::run();


        // VarDumper::dump(Yii::$app->get('ethereum')->txProcessing());


    }


    public function actionTxFee()
    {
        echo 'bitcoin'."\n";
        $client = Yii::$app->get('bitcoin')->getClient();
//        var_dump($client->getwalletinfo());
//        var_dump($client->getmempoolinfo());
        echo sprintf("%.8f", $client->estimatesmartfee(6)['feerate']);
        unset($client);
        echo "\n\n";

        echo 'bitcoin-cash'."\n";
        $client = Yii::$app->get('bitcoin-cash')->getClient();
//        var_dump($client->getwalletinfo());
//        var_dump($client->getmempoolinfo());
        var_dump($client->estimatefee(15));
//        var_dump($client->estimaterawfee(10));
//        echo sprintf("%.8f", $client->estimatefee(6)['feerate']);
        echo "\n".sprintf("%.8f", $client->estimatefee(2));
        unset($client);
        echo "\n\n";

        echo 'litecoin'."\n";
        $client = Yii::$app->get('litecoin')->getClient();
        var_dump($client->getwalletinfo());
        var_dump($client->getmempoolinfo());
        echo sprintf("%.8f", $client->estimatesmartfee(6)['feerate']);
        unset($client);
        echo "\n\n";

        echo 'dogecoin'."\n";
        $client = Yii::$app->get('dogecoin')->getClient();
        var_dump($client->getwalletinfo());
        var_dump($client->getmempoolinfo());
        var_dump($client->estimaterawfee(2));
        var_dump($client->estimatefee(2));

        echo sprintf("%.8f", $client->estimatesmartfee(1)['feerate']);
        unset($client);
        echo "\n\n";





//        $client = Yii::$app->get('litecoin')->getClient();
//        $client = Yii::$app->get('bitcoin-cash')->getClient();
        //  $client = Yii::$app->get('modero')->getClient();
        //$client->settxfee(0.001);

//die;
//TxInfoSender::run();
//die;
//        var_dump(Yii::$app->get('monero')->generateAddress());

   //      var_dump($client->help());
     //    var_dump($client->getwalletinfo());
         //var_dump($client->rescan(530000, 538000));
        // var_dump($client->importaddress('LTkG3BA5YopvAQGUB6cjJzBwG64U5J3i9y', '10122', true));
        // var_dump($client->importprivkey('T6N2DXTEShhBWM1PX9CijXaXH2ezuwR6g9qdWgT79A2NjiKf2fZJ', '10122', true));
 //        var_dump($client->listaccounts());
        // var_dump($client->dumpprivkey('LTkG3BA5YopvAQGUB6cjJzBwG64U5J3i9y'));
       //  var_dump($client->importwallet('second_step_wallets.dat'));
        // var_dump($client->sendtoaddress('LTkG3BA5YopvAQGUB6cjJzBwG64U5J3i9y', 0.09814949 - 0.000818));


//        var_dump($client->getwalletinfo());
//        var_dump($client->getmininginfo());
        //var_dump($client->estimatesmartfee());
        //var_dump($client->estimatefee());


       // echo sprintf("%.8f", $client->estimatesmartfee(6)['feerate']);
        echo "\n\n\n";

//        var_dump($client->listunspent(1, 9999999));
//        var_dump($client->getchaintips());



//        $arg1 = [ ["txid"=>'281263f2f2a5258dd83a4143bbbc51c82dc0966b5a15330cb1555d07e150234c', "vout"=>0]];
//        $arg2 = ['34KrxcrqVFdUTkHS9UiWgNiEEhwqcTD5kd'=>0.00099646];
//        $tl=  $client->createrawtransaction($arg1, $arg2);
//        var_dump($tl);
//
//
//        $tls = $client->signrawtransaction($tl);
//        var_dump($tls);


       // var_dump($client->testmempoolaccept($signrawtransaction['hex']));




       // $hex = $client->decoderawtransaction($signrawtransaction['hex']);

       // var_dump($hex);

//echo "\n\n  \n\n";
        //var_dump($client->gettransaction('b2cfa3f78a96a9a6ef7492e35f2f4d4984fd847c08e74084fe8f9f80e16cfe70'));

//        var_dump($client->sendrawtransaction($tls["hex"]));




       // throw new ErrorException('getinfo error: ' . $client->error);
        // VarDumper::dump(Yii::$app->get('ethereum')->estimateTxFee());


        // estimatefee 5



        foreach(MerchantHelper::avalableWalletsFromMerchantModuleArray() as $currency => $name)
        {

            echo $name . " - " .  MerchantHelper::nodeFee($currency) . "\n";

        }

    }

    public function actionUpDb(){

//        $sql = 'SHOW TABLES';
//        $tables = Yii::$app->db
//            ->createCommand($sql)
//            ->queryAll();
//        print_r(Yii::$app->db->schema->getTableNames());
//
//
//
//        print_r(Yii::$app->db->createCommand('set GLOBAL foreign_key_checks=0;')->execute());
//        print_r(Yii::$app->db->createCommand('ALTER TABLE m_account DISCARD TABLESPACE;')->execute());
//        print_r(Yii::$app->db->createCommand('ALTER TABLE m_account IMPORT TABLESPACE;')->execute());

        //Refresher::coinmarketcapRefresh();
    }



    public function actionNodeInfo(){

$wallets = MerchantHelper::avalableWalletsFromMerchantModuleArray();

foreach ($wallets as $currency => $wallet) {

   // if ($currency == 'ethereum') {
       // continue;


        echo "\n" . $wallet . "\n";
        $nodeInfo = Yii::$app->get($currency)->nodeInfo();

        print_r($nodeInfo);


    if ($currency != 'ethereum') {
        $client = Yii::$app->get($currency)->getClient();
        var_dump($client->listaccounts());
    }


//    }

    // $client = Yii::$app->get('litecoin')->getClient();

}
    }




    public function actionSend(){


//        var_dump(Yii::$app->get('ethereum')->sendFrom(10122, '0x7446bca66e39a9c76be7999a2b70693230e69008'
//            , '0x8c510bbbc19d8c9520d64299988d9b6a90b6bb8d', 3000000000000000, true));
//
//die;
      //  1QLEqQy9cy7hmJ6FupGPQWwjRFp3fXSnTe

        $client = Yii::$app->get('litecoin')->getClient();

//        var_dump(sprintf("%.8f", $client->estimatesmartfee('ECONOMICAL')));
        var_dump(sprintf("%.8f", $client->estimaterawfee(1)));
//
        $client->settxfee(sprintf("%.8f", 0.0001));


        var_dump($client->sendfrom('7', 'LT3upLjPJBi7fcqBHgvfQMRzZXGBesYQtm', 0.0001, 0, '', ''));
//
//        $client = Yii::$app->get('bitcoin')->getClient()
//            ->sendtoaddress('1WWQRZiACTzmwbGEf6NvF68ocm8EiJG1o', 0.00258156, '', '', true);


    }



    public function actionZmq(){


        /*
        *  Hello World client
        *  Connects REQ socket to tcp://localhost:5555
        *  Sends "Hello" to server, expects "World" back
        * @author Ian Barber <ian(dot)barber(at)gmail(dot)com>
        */

        $context = new ZMQContext();

//  Socket to talk to server
        echo "Connecting to hello world server…\n";
        $requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
        $requester->connect("tcp://localhost:5555");

        die;
        for ($request_nbr = 0; $request_nbr != 10; $request_nbr++) {
            printf ("Sending request %d…\n", $request_nbr);
            $requester->send("Hello");

            $reply = $requester->recv();
            printf ("Received reply %d: [%s]\n", $request_nbr, $reply);
        }




    }


    public function actionZmsgSend(){

        TxInfoSender::sendTxidToMUrl();

        //TransactionProcessing::getNewTransactionsAndCompleteThem();
       // TxInfoSender::run();

    }


}