<?php

use app\admin\modules\altcoin\models\TransferForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $wallets array */
/* @var $transferForm TransferForm */


$this->title = Yii::t('app', 'Ваши кошельки');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('newAddress')) { ?>
        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('newAddress') ?>
        </div>
    <?php } ?>

    <?php if (Yii::$app->session->hasFlash('transfer')) { ?>
        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('transfer') ?>
        </div>
    <?php } ?>

    <table class="table">
        <tr class="header">
            <td>Кошелек</td>
            <td>Баланс</td>
            <td>Статус</td>
            <td>

            </td>
        </tr>


        <?php foreach ($wallets as $wallet) { ?>
            <tr>
                <td><?= $wallet['name'] ?></td>
                <td><?= $wallet['info']['balance'] ?> </td>
                <td>
                    <?php
                    if ($wallet['info']['balance'] >= 0) {
                        echo '<strong>Работает...</strong>';
                    } else {
                        echo 'Нет соединения';
                    }
                    ?>
                </td>
                <td>
                    <?php if ($wallet['info']['balance'] >= 0) { ?>
                        <a href="<?= Url::to(['info', 'currency' => $wallet['name']]) ?>">
                            <button type="button" class="btn btn-info">Инфо.</button>
                        </a>
                        <a href="<?= Url::to(['addresses', 'currency' => $wallet['name']]) ?>">
                            <button type="button" class="btn btn-warning">Созданные адреса</button>
                        </a>
                        <a href="<?= Url::to(['newaddress', 'currency' => $wallet['name']]) ?>">
                            <button type="button" class="btn btn-success">Сгенерировать новый адрес</button>
                        </a>
                    <?php } ?>
                </td>
            </tr>

        <?php } ?>


    </table>

    <h1>Отправить</h1>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>

    <?= $form->field($transferForm,
        'address')->textInput(['placeholder' => 'Введите адресс : 3FP8y2nhH44HZ9nuM9dVVGkvCna31gXAHD', 'maxlength' => true, 'style' => 'width:450px; font-weight: bold']) ?>

    <?= $form->field($transferForm,
        'amount')->textInput(['placeholder' => 'Введите сумму', 'maxlength' => true, 'style' => 'width:270px; font-weight: bold']) ?>

    <?= $form->field($transferForm, 'currency')->dropDownList([
        'bitcoin' => 'BTC',
        'litecoin' => 'LTC',
//        'ethereum' => 'ETH',
//        'monero' => 'XMR',
//        'zcash' => 'ZEC',
    ], ['style' => 'width:100px; font-weight: bold']) ?>


    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
