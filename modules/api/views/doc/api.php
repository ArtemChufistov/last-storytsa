<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\captcha\Captcha;
use yii\web\View;
use Yii;


$this->title = Yii::t('app', 'API - Документация');

$this->params['breadcrumbs'] = [[
    'label' => $this->title,
    'url' => '/contact'
],
]

?>

<!-- Breadcrumbs -->
<section class="breadcrumbs-custom bg-image context-dark"
         style="background-image: url(/cmt/images/breadcrumbs-image-7.jpg);">
    <div class="container">
        <div class="breadcrumbs-custom__main">
            <h1 class="breadcrumbs-custom-title"><?php echo $this->title; ?></h1>
        </div>
        <ul class="breadcrumbs-custom__path">
            <li><a href="/"><?php echo Yii::t('app', 'Главная'); ?></a></li>
            <li class="active"><?php echo $this->title; ?></li>
        </ul>
    </div>
</section>

<style>
    .link-box {
        max-width: none;
    }
</style>

<section class="section section-md bg-default">
    <div class="container">
        <div class="row row-30">
            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="/api/v1"><span
                            class="icon link-box__icon linearicons-mouse-right"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Api URL'); ?></h4>
              <p><strong>https://crypchant.com/api/v1</strong> - Базовый API URL</p>
            </div></span>
            </div>


            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box"><span class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Основные параметры'); ?></h4>
              <p>
                <table style="margin-top: 20px;">
                    <tr>
                        <td style="width: 100px;"><strong>merchantId</strong></td>
                        <td>– (число/строка) Идентификатор созданного в лк мерчанта</td>
                    </tr>
                    <tr>
                        <td><strong>secret</strong></td>
                        <td>– (строка) Уникальный ключ генерируется автоматически при создании мерчанта</td>
                    </tr>
                    <tr>
                        <td><strong>options</strong></td>
                        <td> – (массив) Содержит обязательные параметры currency,  command.</td>
                    </tr>  
                </table>
                </p>
            </div></span>
            </div>


            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box"><span class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Доступные currency'); ?></h4>
              <p>
                <table style="margin-top: 20px;">

                                    <?php
                                    foreach (Yii::$app->modules['merchant']->wallets as $wallet) {
                                    ?>
                    <tr>
                        <td style="width: 100px;"><strong><?= \app\modules\merchant\components\merchant\helpers\MerchantHelper::prettyCoinName($wallet) ?> </strong></td>
                        <td> - <?= $wallet ?></td>
                    </tr>
                                    <?php
                                    }
                                    ?>
                </table>
                </p>
            </div></span>
            </div>


            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="#"><span
                            class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Пример запроса'); ?></h4>
              <p style="margin-top: 20px;">
                <?php
                $arr = [
                    "merchantId" => 10101,
                    "secret" => "5NasSSddsYbomJ55Kk46_Ww4Iu6VqUYnzW",
                    "options" =>
                        [
                            "currency" => "litecoin",
                            "command" => "getAddress",
                            'param' => 'Дополнительные параметр'
                        ]
                ];
                ?>
<pre>
<?php
print_r($arr);
?>
</pre>
                </p>
            </div></span>
            </div>





            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="#"><span
                            class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Список валют – "getCurrenciesList"'); ?></h4>
              <p style="margin-top: 20px;">
<?php
$request = '{"merchantId":10101,"secret":"5NasIL_YbomJ55Kk46_Ww4Iu6VqUYnzW","options":{"command":"getCurrenciesList"}}';
$request = print_r(json_decode($request, true), true);
echo Yii::t('app', '<strong>Запрос:</strong>') . "<br><pre>$request</pre>";
$response = '{"data":{"currencyList":{"bitcoin":{"logo":"BTC","name":"Bitcoin"},"litecoin":{"logo":"LTC","name":"Litecoin"},"bitcoin-cash":{"logo":"BCH","name":"Bitcoin Cash"},"dogecoin":{"logo":"DOGE","name":"Dogecoin"}}},"isOk":true,"status":200}';
$response = print_r(json_decode($response, true), true);
echo Yii::t('app', '<strong>Ответ:</strong>') . "<br><pre>$response</pre>";
?>
              </p>
            </div></span>
            </div>



            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="#"><span
                            class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Список комисий по умолчанию – "getDefaultFeesList"'); ?></h4>
              <p style="margin-top: 20px;">
<?php
$request = '{"merchantId":10101,"secret":"5NasIL_YbomJ55Kk46_Ww4Iu6VqUYnzW","options":{"command":"getDefaultFeesList"}}';
$request = print_r(json_decode($request, true), true);
echo Yii::t('app', '<strong>Запрос:</strong>') . "<br><pre>$request</pre>";
$response = '{"data":{"defaultFeesList":{"bitcoin":"0.00001000","litecoin":"0.00010186","bitcoin-cash":"0.00001004","dogecoin":1}},"isOk":true,"status":200}';
$response = print_r(json_decode($response, true), true);
echo Yii::t('app', '<strong>Ответ:</strong>') . "<br><pre>$response</pre>";
?>
              </p>
            </div></span>
            </div>










            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="#"><span
                            class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Cгенерировать адрес – "generateAddress"'); ?></h4>
              <p style="margin-top: 20px;">
<?php
$request = '{"merchantId":10101,"secret":"5NasIL_YbomJ55Kk46_Ww4Iu6VqUYnzW","options":{"currency":"litecoin","command":"generateAddress"}}';
$request = print_r(json_decode($request, true), true);
echo Yii::t('app', '<strong>Запрос:</strong>') . "<br><pre>$request</pre>";
$response = '{"data":{"newAaddress":"LcuGdHDWQQ9tu4ubpZdRcmeMPUuDv2si7S","currency":"litecoin"},"isOk":true,"status":200}';
$response = print_r(json_decode($response, true), true);
echo Yii::t('app', '<strong>Ответ:</strong>') . "<br><pre>$response</pre>";
?>
              </p>
            </div></span>
            </div>


            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="#"><span
                            class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Получить адреса – "showAddresses"'); ?></h4>
              <p style="margin-top: 20px;">
<?php
$request = '{"merchantId":10101,"secret":"5NasIL_YbomJ55Kk46_Ww4Iu6VqUYnzW","options":{"currency":"litecoin","command":"showAddresses", "count":"1000", "page_num":"0", "merchant_only":"true"}}';
$request = print_r(json_decode($request, true), true);
echo Yii::t('app', '<strong>Запрос:</strong>') . "<br><pre>$request</pre>";
$response = '{"data":{"addresses":["LZBDZ1LKWxroNXEeMDHFBV4ktxd2RjKgWj","Lb2z63Srxqe8RaN9ENYvcuScfpyN6FV6LN","LcFhjNG43Kun2xyUryNMYZo1QeQq8PDDJD","LiH3PA4ZF5e3n1AUvt8nDKUyvfAWZVHwFn"],"currency":"litecoin"},"isOk":true,"status":200}';
$response = print_r(json_decode($response, true), true);
echo Yii::t('app', '<strong>Ответ:</strong>') . "<br><pre>$response</pre>";
?>
              </p>
            </div></span>
            </div>



            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="#"><span
                            class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Получить транзакции – "getTransactionsList"'); ?></h4>
              <p style="margin-top: 20px;">
<?php
$request = '{"merchantId":10101,"secret":"5NasIL_YbomJ55Kk46_Ww4Iu6VqUYnzW","options":{"currency":"litecoin","command":"getTransactionsList", "count":"1000", "page_num":"0", "confirmations":"6"}}';
$request = print_r(json_decode($request, true), true);
echo Yii::t('app', '<strong>Запрос:</strong>') . "<br><pre>$request</pre>";
$response = '{"data":{"addresses":["LZBDZ1LKWxroNXEeMDHFBV4ktxd2RjKgWj","Lb2z63Srxqe8RaN9ENYvcuScfpyN6FV6LN","LcFhjNG43Kun2xyUryNMYZo1QeQq8PDDJD","LiH3PA4ZF5e3n1AUvt8nDKUyvfAWZVHwFn"],"currency":"litecoin"},"isOk":true,"status":200}';
$response = print_r(json_decode($response, true), true);
//echo Yii::t('app', '<strong>Ответ:</strong>') . "<br><pre>$response</pre>";
?>
              </p>
            </div></span>
            </div>






            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="#"><span
                            class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Получить информацию по id транзакции – "getTransactionsInfoById"'); ?></h4>
              <p style="margin-top: 20px;">
<?php
$request = '{"merchantId":10101,"secret":"5NasIL_YbomJ55Kk46_Ww4Iu6VqUYnzW","options":{"currency":"dogecoin","command":"getTransactionsInfoById", "id":"234"}}';
$request = print_r(json_decode($request, true), true);
echo Yii::t('app', '<strong>Запрос:</strong>') . "<br><pre>$request</pre>";
$response = '{"data":{"addresses":["LZBDZ1LKWxroNXEeMDHFBV4ktxd2RjKgWj","Lb2z63Srxqe8RaN9ENYvcuScfpyN6FV6LN","LcFhjNG43Kun2xyUryNMYZo1QeQq8PDDJD","LiH3PA4ZF5e3n1AUvt8nDKUyvfAWZVHwFn"],"currency":"litecoin"},"isOk":true,"status":200}';
$response = print_r(json_decode($response, true), true);
//echo Yii::t('app', '<strong>Ответ:</strong>') . "<br><pre>$response</pre>";
?>
              </p>
            </div></span>
            </div>








            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="#"><span
                            class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Отправить средства – "sendFromMerchant"'); ?></h4>
              <p style="margin-top: 20px;">
<?php
$request = '{"merchantId":10101,"secret":"5NasIL_YbomJ55Kk46_Ww4Iu6VqUYnzW","options":{"currency":"litecoin","command":"sendFromMerchant","toAddress":"LiH3PA4ZF5e3n1AUvt8nDKUyvfAWZVHwFn","amount":"0.015","fee_per_transaction":"0.00015","fee_per_byte":"3"}}';
$request = print_r(json_decode($request, true), true);
echo Yii::t('app', '<strong>Запрос:</strong>') . "<br><pre>$request</pre>";
$response = '{"data":{"crypchant_transaction_id":"124","currency":"litecoin"},"isOk":true,"status":200}';
$response = print_r(json_decode($response, true), true);
echo Yii::t('app', '<strong>Ответ:</strong>') . "<br><pre>$response</pre>";
?>
              </p>
            </div></span>
            </div>







            <div class="col-lg-12">
                <!-- Link Box--><span class="link-box" href="#"><span
                            class="icon link-box__icon linearicons-layers"></span>
            <div class="link-box__main">
              <h4><?php echo Yii::t('app', 'Общий баланс кошелька аккаунта – "getAddressesBalanceSum"'); ?></h4>
              <p style="margin-top: 20px;">
<?php
$request = '{"merchantId":10101,"secret":"5NasIL_YbomJ55Kk46_Ww4Iu6VqUYnzW","options":{"currency":"litecoin","command":"getAddressesBalanceSum","confirmations_num":7}}';
$request = print_r(json_decode($request, true), true);
echo Yii::t('app', '<strong>Запрос:</strong>') . "<br><pre>$request</pre>";
$response = '{"data":{"balance":0.049548,"currency":"litecoin"},"isOk":true,"status":200}';
$response = print_r(json_decode($response, true), true);
echo Yii::t('app', '<strong>Ответ:</strong>') . "<br><pre>$response</pre>";
?>
              </p>
            </div></span>
            </div>



        </div>
    </div>
</section>






