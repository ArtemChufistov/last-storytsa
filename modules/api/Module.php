<?php

namespace app\modules\api;

use yii\base\BootstrapInterface;

/**
 * profile module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    public $wallets = [];

    public $allowedUsers;

    public $mainPageCache = 0;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }

    /**
     * Собственные отображения
     * Допустимые параметры:
     *
     * signup - регистрация
     * login - авторизация
     * profile - профиль
     * repass - восстановление пароля
     * show - просмотр пользователя
     *
     * @param $customView - отображение
     * @param $default - отображение по умолчанию
     * @return mixed
     */
    public function getCustomView($customView, $default)
    {
        if (isset($this->customViews[$customView])) {
            return $this->customViews[$customView];
        } else {
            return $default;
        }
    }
    
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            //        //Test Url
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'check/test',
                'route' => 'api/doc/test',
            ],
            //        //API Service
            [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'api/v1',
                'route' => 'api/merchant/v1',
            ],
            //        //API Doc
            [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'api/doc',
                'route' => 'api/doc/api',
            ],
            //        //API Notification
            [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'api/notify/<action>',
                'route' => 'api/notify/<action>',
            ]
        ], false);

        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'app\modules\api\commands';
        }
    }
}
