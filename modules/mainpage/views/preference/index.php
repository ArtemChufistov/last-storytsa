<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\mainpage\models\Preference;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\mainpage\models\PreferenceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Настройки системы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preference-index">

    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                 <?= Html::a(Yii::t('app', 'Добавить настройки'), ['create'], ['class' => 'btn btn-success']) ?>
            </div>

            <?php Pjax::begin(); ?>

                <div class="box-body no-padding">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            [
                              'attribute' => 'key',
                              'format' => 'raw',
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[key]',
                                  'value' => $searchModel->key,
                                  'attribute' => 'key',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'data' => array_merge([Yii::t('app', 'Не выбрано')], Preference::getKeyArray()),
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Выберите'),
                                      'style' => ['width' => '150px'],
                                  ],]
                                ),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_key', ['model' => $model]);},
                            ],
                            'value',
                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>

            <?php Pjax::end(); ?>
          </div>
        </div>
    </div>
</div>
