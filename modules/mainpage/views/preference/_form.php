<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\mainpage\models\Preference;

/* @var $this yii\web\View */
/* @var $model app\modules\mainpage\models\Preference */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

		    <?php $form = ActiveForm::begin(); ?>

	        <?php echo $form->field($model, 'key')->widget(Select2::classname(), [
	            'data' => Preference::getKeyArray(),
	            'options' => ['placeholder' => 'Выберите статус...'],
	            'pluginOptions' => [
	                'allowClear' => true
	            ],
	        ]);?>

		    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

		    <div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>

		</div>
	  </div>
	</div>
</div>