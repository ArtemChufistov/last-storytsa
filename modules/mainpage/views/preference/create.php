<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\mainpage\models\Preference */

$this->title = Yii::t('app', 'Добавить настройки системы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки системы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preference-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
