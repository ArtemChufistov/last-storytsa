<?php

namespace app\modules\mainpage\components;

use dosamigos\qrcode\formats\FormatAbstract;
use dosamigos\qrcode\traits\UrlTrait;
use yii\base\InvalidConfigException;
use yii\validators\UrlValidator;

/**
 * Class BookMark formats a string to properly create a Bookmark QrCode
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @package dosamigos\qrcode\formats
 */
class SiteUrl extends FormatAbstract
{
    use UrlTrait;

    public $uri;

    /**
     * @inheritdoc
     */
    public function getText()
    {
        return "URL:{$this->uri}";
    }
}
