<?php

namespace app\modules\mainpage\controllers;

use Yii;
use yii\helpers\Url;
use dosamigos\qrcode\QrCode;
use dosamigos\qrcode\formats\Bitcoin;
use app\components\FrontendController;
use app\modules\mainpage\components\SiteUrl;

class SiteController extends FrontendController
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor' => '3373751' //синий
            ],
        ];
    }

    // QR code
    public function actionQrcodemain($login = '')
    {
    	$url = Url::home(true) . '?ref=' . $login;
        $siteUrl = new SiteUrl(['uri' => $url]);
        return QrCode::png($siteUrl->getText());
    }

    // QR code
    public function actionQrcodereg($login = '')
    {
    	$url = Url::home(true) . Url::to('signup') . '?ref=' . $login;
        $siteUrl = new SiteUrl(['uri' => $url]);
        return QrCode::png($siteUrl->getText());
    }
}