<?php

namespace app\modules\mainpage\models;

use Yii;

/**
 * This is the model class for table "preference".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class Preference extends \yii\db\ActiveRecord {
	// Mail
	const KEY_SEND_MAIL_FREQUENCY = 'sendMailFrequency';
	const KEY_SEND_MAIL_HOST = 'sendMailHost';
	const KEY_SEND_MAIL_USERNAME = 'sendMailUsername';
	const KEY_SEND_MAIL_PASSWORD = 'sendMailPassword';
	const KEY_SEND_MAIL_PORT = 'sendMailPort';
	const KEY_SEND_MAIL_ENCRYPTION = 'sendMailEncryption';
	// Payment
	const KEY_ONLY_ONE_CURRENCY = 'onlyOneCurrency';
	const KEY_OUT_COMISSION_TO_LOGIN = 'outComissionToLogin';
	const KEY_IN_COMISSION_TO_LOGIN = 'inComissionToLogin';
	const KEY_PENDING_PAYMENT_TIME = 'pendingPaymentTime';
	const KEY_VOUCHER_ACTIVE = 'voucherActive';
	const KEY_CUSTOM_PAY_OUT_VALIDATE = 'payOutValidate';
	const KEY_COUNT_PAY_OUT_VALIDATE = 'countOutValidate';
	const KEY_CUSTOM_OUT_COMISSION = 'customOutComission';
	// Coinbase
	const KEY_COINBASE_API_KEY = 'coinBaseApikey';
	const KEY_COINBASE_API_SECRET = 'coinBaseApiSecret';
	// Cm
	const KEY_CM_API_ID = 'cmApiId';
	const KEY_CM_API_SECRET = 'cmApiSecret';
	// Invest
	const KEY_INVEST_MARKETING = 'investMarketing';
	// Matrix
	const KEY_MATRIX_MARKETING = 'matrixMarketing';
	const KEY_COUNT_FROM_MATRIX_QUEUE = 'countFromMatrixQueue';
	//User register
	const KEY_USER_REG_CAN_PARENT_EDIT = 'userRegCanEditParent';
	const KEY_USER_REG_DEFAULT_PARENT = 'userRegDefaultParentLogin';
	const KEY_USER_EMAIL_UNIQUE = 'userEmailUnique';
	// Perfect Money
	const KEY_PERF_MON_SECRET = 'perfMonSecret';
	const KEY_PERF_MON_ACCOUND_ID = 'perfMonPayeeAccountId';
	const KEY_PERF_MON_PAYEE_ACCOUNT_USD = 'perfMonPayeeAccountUSD';
	const KEY_PERF_MON_PAYEE_ACCOUNT_EUR = 'perfMonPayeeAccountEUR';
	const KEY_PERF_MON_PAYEE_NAME = 'perfMonPayeeName';
	const KEY_PERF_MON_PASSWORD = 'perfMonPassword';
	const KEY_PERF_MON_STATUS_URL = 'perfMonStatusUrl';
	const KEY_PERF_MON_PAYMENT_URL = 'perfMonPaymentUrl';
	const KEY_PERF_MON_NO_PAYMENT_URL = 'perfMonNoPaymentUrl';
	// Top-exchange
	const KEY_PREF_TOPEXCHANGE_NAME = 'prefTopexchangeName';
	const KEY_PREF_TOPEXCHANGE_PASSWORD = 'prefTopexchangePassword';
	const KEY_PREF_TOPEXCHANGE_SUCCESS_URL = 'prefTopexchangeSuccessUrl';
	const KEY_PREF_TOPEXCHANGE_ERROR_URL = 'prefTopexchangeErrorUrl';
	const KEY_PREF_TOPEXCHANGE_DEFAULT_CUR = 'prefTopexchangeDefaultCur';
	const KEY_PREF_TOPEXCHANGE_LOGIN = 'prefTopexchangeLogin';
	const KEY_PREF_TOPEXCHANGE_PASS = 'prefTopexchangePass';
	const KEY_PREF_TOPEXCHANGE_SEND_CURRENCY = 'prefTopExchangeSendCurrency';
	// Crypchant
	const KEY_CRYPCHANT_API_ID = 'crypchantApiId';
	const KEY_CRYPCHANT_API_SECRET = 'crypchantApiSecret';
	// poloniex.com
	const KEY_POLONIEX_API_KEY = 'poloniexApiKey';
	const KEY_POLONIEX_API_SECRET = 'poloniexApiSecret';
	// graph
	const KEY_START_TIME_BUILD_GRAPH = 'startTimeBuildGraph';
	// cron run period
	const KEY_CRON_RUN_PERIOD = 'cronRunPeriodInSec';
	//  default role for registered user
	const KEY_REG_USER_DEFAULT_RULE = 'regUserDefaultRule';
	const KEY_VIDEO_COST = 'videoCost';
	// redirect to register if  ref in get
	const KEY_REDIRECT_TO_REGISTER_IF_REF_IN_GET = 'redirectToRegisterIfRefInGet';
	// AdvCash API
	const KEY_ADVCASH_API_NAME = 'advCashApiName';
	const KEY_ADVCASH_ACCOUNT_EMAIL = 'advCashAccountEmail';
	const KEY_ADVCASH_API_PASSWORD = 'advCashApiPassword';
	// AdvCash SCI
	const KEY_SCI_EMAIL = 'advCashSciEmail';
	const KEY_SCI_NAME = 'advCashSciName';
	const KEY_SCI_SECRET = 'advCashSciSecret';

	const KEY_SINGUP_NO_LOGIN = 'signUpNoLogin';

	const KEY_ADMIN_LAST_INVEST_ID = 'adminLastInvestId';
	const KEY_STRUCT_LAST_INVEST_ID = 'structLastInvestId';
	const KEY_UNDER_CONTRUCTION = 'technicalJob';

	const KEY_ADMITAD_OFFERS_URL = 'admitadOffersUrl';
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'preference';
	}

	public static function getKeyArray() {
		return [
			self::KEY_SEND_MAIL_FREQUENCY => Yii::t('app', 'Задержка между отправками писем'),
			self::KEY_SEND_MAIL_HOST => Yii::t('app', 'Хост отправки писем'),
			self::KEY_SEND_MAIL_USERNAME => Yii::t('app', 'Почтовый ящик для отправки писем'),
			self::KEY_SEND_MAIL_PASSWORD => Yii::t('app', 'Пароль для почтового ящика'),
			self::KEY_SEND_MAIL_PORT => Yii::t('app', 'Порт хоста отправки писем'),
			self::KEY_SEND_MAIL_ENCRYPTION => Yii::t('app', 'Кодировка письма'),
			self::KEY_PENDING_PAYMENT_TIME => Yii::t('app', 'Время до отмены заявки на оплату'),
			self::KEY_COINBASE_API_KEY => Yii::t('app', 'API ключ CoinBase'),
			self::KEY_COINBASE_API_SECRET => Yii::t('app', 'API секретная фраза CoinBase'),
			self::KEY_CRYPCHANT_API_ID => Yii::t('app', 'API ключ Crypchant'),
			self::KEY_CRYPCHANT_API_SECRET => Yii::t('app', 'API секретная фраза Crypchant'),
			self::KEY_MATRIX_MARKETING => Yii::t('app', 'Матричный маркетинг'),
			self::KEY_INVEST_MARKETING => Yii::t('app', 'Инвест маркетинг'),
			self::KEY_COUNT_FROM_MATRIX_QUEUE => Yii::t('app', 'Сколько мест брать за раз из матричной очереди'),
			self::KEY_USER_REG_CAN_PARENT_EDIT => Yii::t('app', 'Разрешено указать пригласителя при регистрации'),
			self::KEY_USER_REG_DEFAULT_PARENT => Yii::t('app', 'Пригласитель (ID пользователя) по умолчанию, если не указан при регистрации'),
			self::KEY_USER_EMAIL_UNIQUE => Yii::t('app', 'E-mail у пользователя уникален'),
			self::KEY_PERF_MON_SECRET => Yii::t('app', 'Perfect Money Секретная фраза'),
			self::KEY_PERF_MON_ACCOUND_ID => Yii::t('app', 'Perfect Money аккаунт id'),
			self::KEY_PERF_MON_PASSWORD => Yii::t('app', 'Perfect Money пароль'),
			self::KEY_PERF_MON_PAYEE_ACCOUNT_USD => Yii::t('app', 'Perfect Money аккаунт USD'),
			self::KEY_PERF_MON_PAYEE_ACCOUNT_EUR => Yii::t('app', 'Perfect Money аккаунт EUR'),
			self::KEY_PERF_MON_PAYEE_NAME => Yii::t('app', 'Perfect Money имя'),
			self::KEY_PERF_MON_STATUS_URL => Yii::t('app', 'Perfect Money status url'),
			self::KEY_PERF_MON_PAYMENT_URL => Yii::t('app', 'Perfect Money payment url'),
			self::KEY_PERF_MON_NO_PAYMENT_URL => Yii::t('app', 'Perfect Money no payment url'),
			self::KEY_ONLY_ONE_CURRENCY => Yii::t('app', 'Только одна валюта в личном кабинете'),
			self::KEY_IN_COMISSION_TO_LOGIN => Yii::t('app', 'Комиссия с ввода на логин'),
			self::KEY_OUT_COMISSION_TO_LOGIN => Yii::t('app', 'Комиссия с вывода на логин'),
			self::KEY_CUSTOM_PAY_OUT_VALIDATE => Yii::t('app', 'Валидатор средств на вывод'),
			self::KEY_COUNT_PAY_OUT_VALIDATE => Yii::t('app', 'Количество оплативших для валидатора средств на вывод'),
			self::KEY_VOUCHER_ACTIVE => Yii::t('app', 'Включить ваучеры'),
			self::KEY_CUSTOM_OUT_COMISSION => Yii::t('app', 'Функция подсчёта комиссии на вывод'),
			self::KEY_START_TIME_BUILD_GRAPH => Yii::t('app', 'Дата старта построения графика валют'),
			self::KEY_PREF_TOPEXCHANGE_NAME => Yii::t('app', 'Top-exchange название мерчанта'),
			self::KEY_PREF_TOPEXCHANGE_PASSWORD => Yii::t('app', 'Top-exchange пароль мерчанта'),
			self::KEY_PREF_TOPEXCHANGE_SUCCESS_URL => Yii::t('app', 'Top-exchange success url'),
			self::KEY_PREF_TOPEXCHANGE_ERROR_URL => Yii::t('app', 'Top-exchange error url'),
			self::KEY_PREF_TOPEXCHANGE_DEFAULT_CUR => Yii::t('app', 'Top-exchange валюта по умолчанию'),
			self::KEY_POLONIEX_API_KEY => Yii::t('app', 'Poloniex API KEY'),
			self::KEY_POLONIEX_API_SECRET => Yii::t('app', 'Poloniex API SECRET'),
			self::KEY_CRON_RUN_PERIOD => Yii::t('app', 'Частота запуска CRON в секундах'),
			self::KEY_REG_USER_DEFAULT_RULE => Yii::t('app', 'Роль по умолчанию для зарегистрированного пользователя'),
			self::KEY_VIDEO_COST => Yii::t('app', 'Цена видео'),
			self::KEY_REDIRECT_TO_REGISTER_IF_REF_IN_GET => Yii::t('app', 'Отправить на страницу регистрации если переход по реф ссылке'),
			self::KEY_ADVCASH_API_NAME => Yii::t('app', 'AdvCash API название'),
			self::KEY_ADVCASH_ACCOUNT_EMAIL => Yii::t('app', 'AdvCash Аккаунт E-mail'),
			self::KEY_ADVCASH_API_PASSWORD => Yii::t('app', 'AdvCash API пароль'),
			self::KEY_SCI_EMAIL => Yii::t('app', 'AdvCash SCI E-mail'),
			self::KEY_SCI_NAME => Yii::t('app', 'AdvCash SCI название'),
			self::KEY_SCI_SECRET => Yii::t('app', 'AdvCash SCI секретная фраза'),
			self::KEY_SINGUP_NO_LOGIN => Yii::t('app', 'Регистрация без логина (вместо логина будет использоваться ID)'),
			self::KEY_ADMIN_LAST_INVEST_ID => Yii::t('app', 'Последний ид купленного пакета для админского бонуса'),
			self::KEY_STRUCT_LAST_INVEST_ID => Yii::t('app', 'Последний ид купленного пакета для структурного бонуса'),
			self::KEY_UNDER_CONTRUCTION => Yii::t('app', 'Закрыть сайт на техническое обслуживание'),
			self::KEY_CM_API_ID => Yii::t('app', 'Cm API ID'),
			self::KEY_CM_API_SECRET => Yii::t('app', 'CM API Секрет'),
			self::KEY_ADMITAD_OFFERS_URL => Yii::t('app', 'Admitad офферная XML ссылка'),
		];
	}

	public function getKeyTitle() {
		$array = self::getKeyArray();

		return $array[$this->key];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['key', 'value'], 'required'],
			[['key', 'value'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'key' => Yii::t('app', 'Ключ'),
			'value' => Yii::t('app', 'Значение'),
		];
	}
}
