<?php
namespace app\modules\shop\widgets;

use yii\base\Widget;
use app\modules\shop\models\ProductReview;
use app\modules\shop\models\forms\ProductReviewForm;
use app\components\LayoutComponent;
use Yii;

class ProductReviews extends Widget{

	public $product;

	public $limit;

	public $view = 'product-reviews';

	public function init(){
	}

	public function run(){

		$reviewForm = new ProductReviewForm;

	    if ($reviewForm->load(Yii::$app->request->post()) && $reviewForm->validate()) {
	    	$review = new ProductReview;

	    	$review->product_id = $this->product->id;
	    	$review->active = ProductReview::ACTIVE_TRUE;
	    	$review->date_add = date('Y-m-d H:i:s');
	    	$review->setAttributes($reviewForm->attributes);

	        if ($review->save()) {
	        	Yii::$app->session->setFlash('review-success');
	        }
	    }

		$reviewQuery = ProductReview::find()->where(['product_id' => $this->product->id])->orderBy(['date_add' => SORT_DESC]);

		if (!empty($this->limit)) {
			$reviewQuery->limit($this->count);
		}

		$productReviews = $reviewQuery->all();

		return \Yii::$app->controller->renderPartial(LayoutComponent::themePath('/shop/widgets/' . $this->view), [
			'productReviews' => $productReviews,
			'reviewForm' => $reviewForm,
		]);
	}
}
?>