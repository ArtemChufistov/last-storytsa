<?php

namespace app\modules\shop\controllers;

use app\components\FrontendController;
use app\modules\shop\models\Campaign;
use app\modules\shop\models\Category;
use app\modules\shop\models\Product;
use app\modules\shop\models\OrderProduct;
use app\modules\shop\models\ParamCategory;
use app\modules\shop\models\search\ProductSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

class ShopController extends FrontendController
{
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor' => '3373751', //синий
            ],
        ];
    }

    public function actionCategory($key = '') {

        $campaign = '';
        $parentCategoryes = '';
        $category = Category::find()->where(['key' => $key])->one();
        $productModel = new ProductSearch();

        if (!empty($category)){
            $campaign = $category->getCampaign()->one();
            $productModel->categoryes = array_merge([$category->id => $category->id], ArrayHelper::map($category->descendants()->all(), 'id', 'id'));
            $parentCategoryes = $category->ancestors()->all();
        }

        $paramCategory = ParamCategory::find()
            ->where(['in', 'category_id', $productModel->categoryes])
            ->all();


        $orderProducts = OrderProduct::find()
            ->where(['in', 'category_id', $productModel->categoryes])
            ->groupBy(['product_id'])
            ->limit(10)->all();

        $productProvider = $productModel->search(Yii::$app->request->queryParams);

        return $this->render('shop/category', [
            'parentCategoryes' => $parentCategoryes,
            'productProvider' => $productProvider,
            'orderProducts' => $orderProducts,
            'productModel' => $productModel,
            'campaign' => $campaign,
            'category' => $category
        ]);
    }

    public function actionProduct($key) {
        if (!empty($key)){
            $product = Product::find()->where(['key' => $key])->one();
        }else{
            $product = '';
        }

        if (!empty($product)){
            $campaign = $product->getCampaign()->one();
            $category = $product->getCategory()->one();
            $parentCategoryes = $category->ancestors()->all();

            $categoryArray = array_merge([$category->id => $category->id], ArrayHelper::map($category->descendants()->all(), 'id', 'id'));

            $orderProducts = OrderProduct::find()
                ->where(['in', 'category_id', $categoryArray])
                ->groupBy(['product_id'])
                ->limit(10)
                ->all();

            $sameProducts = Product::find()
                ->where(['in', 'category_id', $categoryArray])
                ->andWhere(['!=', 'id', $product->id])
                ->limit(10)
                ->all();

        }else{
            $campaign = '';
            $category = '';
            $orderProducts = '';
        }

        return $this->render('shop/product', [
            'parentCategoryes' => $parentCategoryes,
            'orderProducts' => $orderProducts,
            'sameProducts' => $sameProducts,
            'campaign' => $campaign,
            'category' => $category,
            'product' => $product,
        ]);
    }

    public function actionProductsearch($word) {
        $productArray = Product::find()->where(['LIKE', 'name', $word])->all();

        return $this->render('shop/productsearch', [
            'productArray' => $productArray
        ]);
    }

    public function actionCampaign($key) {
        $campaign = Campaign::find()->where(['key' => $key])->one();
        return $this->render('shop/campaign', [
            'campaign' => $campaign
        ]);
    }

    public function actionOrder($key) {
        $product = Product::find()->where(['key' => $key])->one();

        $orderProduct = new OrderProduct;
        $orderProduct->product_id = $product->id;
        $orderProduct->category_id = $product->category_id;
        $orderProduct->campaign_id = $product->campaign_id;
        $orderProduct->date_add = date('Y-m-d H:i:s');

        $orderProduct->save();

        echo $product->url;
    }

    public function actionSitemap() {
        $this->layout = false;
        $host = Yii::$app->request->hostInfo;
        $urls = [];

        $urls[] = $host;

        foreach(Category::find()->all() as $category) {

            $urls[] = $host . '/category/' . $category->key;

            $categoryArray = array_merge([$category->id => $category->id], ArrayHelper::map($category->descendants()->all(), 'id', 'id'));

            $countProducts = Product::find()->where(['in', 'category_id', $categoryArray])->count();

            for ($i = 1; $i <= ceil($countProducts / 20); $i++) {
                $urls[] = $host . '/category/' . $category->key . '?page=' . $i;
            }

        }

        foreach (Product::find()->all() as $product) {
            $urls[] = $host . '/product/' . $product->key;
        }

        return $this->render('shop/sitemap', [
            'urls' => $urls
        ]);
    }
}
