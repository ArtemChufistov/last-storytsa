<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\search\CampaignSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="campaign-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'alias') ?>

    <?= $form->field($model, 'site_url') ?>

    <?= $form->field($model, 'logo') ?>

    <?php // echo $form->field($model, 'date_start') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'geotargeting') ?>

    <?php // echo $form->field($model, 'currency_id') ?>

    <?php // echo $form->field($model, 'postclick_cookie') ?>

    <?php // echo $form->field($model, 'rating') ?>

    <?php // echo $form->field($model, 'ecpm') ?>

    <?php // echo $form->field($model, 'ecpc') ?>

    <?php // echo $form->field($model, 'epc') ?>

    <?php // echo $form->field($model, 'cr') ?>

    <?php // echo $form->field($model, 'avg_hold_time') ?>

    <?php // echo $form->field($model, 'avg_money_transfer_time') ?>

    <?php // echo $form->field($model, 'actions') ?>

    <?php // echo $form->field($model, 'regions') ?>

    <?php // echo $form->field($model, 'categories') ?>

    <?php // echo $form->field($model, 'admitad_last_update') ?>

    <?php // echo $form->field($model, 'advertiser_last_update') ?>

    <?php // echo $form->field($model, 'delivery_options') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
