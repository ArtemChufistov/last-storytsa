<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\search\CampaignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Компании');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class = "row">
    <div class="col-md-12">
      <div class="box box-success collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo Yii::t('app', 'Поиск'); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header">
            <h1><?=Html::encode($this->title)?></h1>
            <p>
                <?= Html::a(Yii::t('app', 'Добавить Компанию'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'offer_id',
                    'name',
                    'alias',
                    'currency_id',
                    'site_url:url',
                    [
                    'attribute' => 'logo',
                    'format' => 'raw',
                    'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_logo', ['model' => $model]);},
                    ],
                    'rating',
                    'ecpm',
                    'ecpc',
                    'epc',
                    'cr',
                    'date_start',
                    [
                    'attribute' => 'original_products',
                    'format' => 'raw',
                    'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_original-product', ['model' => $model]);},
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
