<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Campaign */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Компании'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?=Html::encode($this->title)?></h2>
        </div>
        <div class="box-body">

            <p>
                <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'alias',
                    'site_url:url',
                    'logo',
                    'date_start',
                    'description:ntext',
                    'geotargeting',
                    'currency_id',
                    'postclick_cookie',
                    'rating',
                    'ecpm',
                    'ecpc',
                    'epc',
                    'cr',
                    'avg_hold_time',
                    'avg_money_transfer_time',
                    'actions:ntext',
                    'regions:ntext',
                    'categories:ntext',
                    'admitad_last_update',
                    'advertiser_last_update',
                    'delivery_options:ntext',
                ],
            ]) ?>

        </div>
      </div>
    </div>
  </div>
</div>