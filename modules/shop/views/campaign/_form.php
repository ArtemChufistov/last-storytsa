<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Campaign */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'site_url')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'logo')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'date_start')->textInput() ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'geotargeting')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'currency_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'postclick_cookie')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'rating')->textInput() ?>

            <?= $form->field($model, 'ecpm')->textInput() ?>

            <?= $form->field($model, 'ecpc')->textInput() ?>

            <?= $form->field($model, 'epc')->textInput() ?>

            <?= $form->field($model, 'cr')->textInput() ?>

            <?= $form->field($model, 'avg_hold_time')->textInput() ?>

            <?= $form->field($model, 'avg_money_transfer_time')->textInput() ?>

            <?= $form->field($model, 'actions')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'regions')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'categories')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'admitad_last_update')->textInput() ?>

            <?= $form->field($model, 'advertiser_last_update')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
      </div>
    </div>
</div>