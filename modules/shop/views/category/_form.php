<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

		    <?php $form = ActiveForm::begin(); ?>

		    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'campaign_id')->textInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'offer_id')->textInput(['maxlength' => true]) ?>

		    <div class="form-group">
		        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>
		</div>
	  </div>
	</div>
</div>
