<?php if (!empty($model->original_products)): ?>
	<a href = "<?php echo $model->original_products; ?>">
		<span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="<?php echo $model->original_products; ?>"><i class="fa fa-plus"></i></span>
	</a>
<?php else: ?>
	<span data-toggle="tooltip" title="" class="badge bg-blue" data-original-title="<?php echo Yii::t('app', 'Нет товаров'); ?>"><i class="fa fa-minus"></i></span>
<?php endif;?>