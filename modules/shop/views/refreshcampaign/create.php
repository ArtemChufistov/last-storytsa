<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\RefreshCampaign */

$this->title = Yii::t('app', 'Добавить задачу');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Задачи обновления компаний'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refresh-campaign-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
