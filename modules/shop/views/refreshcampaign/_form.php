<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\RefreshCampaign */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

		    <?php $form = ActiveForm::begin(); ?>

		    <?=$form->field($model, 'refrersh_date')->widget(DatePicker::classname(), [
				'options' => ['placeholder' => $model->getAttributeLabel('refrersh_date')],
				'type' => DatePicker::TYPE_COMPONENT_APPEND,
				'pluginOptions' => [
					'autoclose' => true,
					'format' => 'yyyy-mm-dd',
				],
			]);?>

		    <div class="form-group">
		        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>

		</div>
	  </div>
	</div>
</div>