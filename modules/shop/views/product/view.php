<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?=Html::encode($this->title)?></h2>
        </div>
        <div class="box-body">

            <p>
                <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'offer_id',
                    'available',
                    'campaign_id',
                    'category_offer_id',
                    'category_id',
                    'cpa',
                    'currencyId',
                    'delivery',
                    'manufacturer_warranty',
                    'model:ntext',
                    'modified_time',
                    'name:ntext',
                    'pickup',
                    'price',
                    'typePrefix',
                    'url:ntext',
                    'pictures:ntext',
                    'vendor',
                    'vendorCode',
                ],
            ]) ?>

        </div>
      </div>
    </div>
  </div>
</div>