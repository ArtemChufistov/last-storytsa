<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\shop\models\Campaign;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\RefreshOffer */
/* @var $form yii\widgets\ActiveForm */

?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?php echo $form->field($model, 'campaign_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Campaign::find()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Выберите компанию...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>

            <?=$form->field($model, 'refrersh_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => $model->getAttributeLabel('refrersh_date')],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ],
            ]);?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
</div>