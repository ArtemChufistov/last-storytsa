<?php

namespace app\modules\shop\components\cron;

use app\modules\mainpage\models\Preference;
use app\modules\shop\models\RefreshCampaign;
use app\modules\shop\models\RefreshOffer;
use app\modules\shop\models\CategoryTree;
use app\modules\shop\models\Campaign;
use app\modules\shop\models\Category;
use app\modules\shop\models\ParamCategory;
use app\modules\shop\models\ProductParam;
use app\modules\shop\models\Product;
use app\modules\shop\models\Param;
use app\components\LangComponent;
use yii\helpers\ArrayHelper;
use Yii;

class RefreshComponent {

    const OFFER_LOGO_PATH = '/campaign/logo/';
    const OFFER_LOGO_EXT  = '.png';

    const OFFER_PRODUCT_PATH = '/product/';
    const OFFER_PRODUCT_EXT  = '.png';

    // Обновление списка компаний
    public static function campaign() {

        $currentDate = date('Y-m-d H:i:s');
        $refreshCampaign = RefreshCampaign::find()
            ->where(['<=','refrersh_date', $currentDate])
            ->andWhere(['is', 'start_date', NULL])
            ->one();

        if (!empty($refreshCampaign)){
            $refreshCampaign->start_date = $currentDate;
            $refreshCampaign->save(false);
        }else{
            return;
        }

        $pref = Preference::find()
            ->where(['key' => Preference::KEY_ADMITAD_OFFERS_URL])
            ->one();

        if (empty($pref)) {
            throw new Exception(Yii::t('app', 'Заполните Admitad url в настройках'), 1);
            return;
        }

        $xmlData = file_get_contents($pref->value);

        $campaignArray = simplexml_load_string($xmlData);

        foreach($campaignArray as $offerCampaign) {

            $campaign = Campaign::find()
                ->where(['offer_id' => $offerCampaign->id])
                ->andWhere(['changeWhenUpdate' => Campaign::CHANGE_WHEN_UPDATE_TRUE])
                ->one();

            if (empty($campaign)){
                $campaign = new Campaign;
                $campaign->offer_id = $offerCampaign->id;
            }elseif ($campaign->changeWhenUpdate == Campaign::CHANGE_WHEN_UPDATE_FALSE) {
                continue;
            }

            $campaign->key = strtolower(LangComponent::rus2translit($offerCampaign->name));
            $campaign->name = $offerCampaign->name;
            $campaign->alias = $offerCampaign->name_aliases;
            $campaign->site_url = $offerCampaign->site_url;
            $campaign->date_start = $offerCampaign->date_start;
            $campaign->description = $offerCampaign->description;
            $campaign->geotargeting = $offerCampaign->geotargeting;
            $campaign->currency_id = $offerCampaign->currency_id;
            $campaign->postclick_cookie = $offerCampaign->postclick_cookie;
            $campaign->rating = $offerCampaign->rating;
            $campaign->ecpm = $offerCampaign->ecpm;
            $campaign->ecpc = $offerCampaign->ecpc;
            $campaign->epc = $offerCampaign->epc;
            $campaign->cr = $offerCampaign->cr;
            $campaign->avg_hold_time = $offerCampaign->avg_hold_time;
            $campaign->avg_money_transfer_time = $offerCampaign->avg_money_transfer_time;
            $campaign->actions = json_encode($offerCampaign->actions);
            $campaign->regions = json_encode($offerCampaign->regions);
            $campaign->categories = json_encode($offerCampaign->categories);
            $campaign->original_products = !empty($offerCampaign->original_products) ? $offerCampaign->original_products : '';

            $campaign->save(false);

            file_put_contents(self::offerLogo($campaign->id), file_get_contents($offerCampaign->logo));

            $campaign->logo = self::OFFER_LOGO_PATH . $campaign->id . self::OFFER_LOGO_EXT;
            $campaign->save(false);
        }

        $refreshCampaign->end_date = date('Y-m-d H:i:s');
        $refreshCampaign->save(false);
    }

    // Обновление списка товаров
    public static function offer() {

        $currentDate = date('Y-m-d H:i:s');
        $refreshOffer = RefreshOffer::find()
            ->where(['<=','refrersh_date', $currentDate])
            ->andWhere(['is', 'start_date', NULL])
            ->one();

        if (!empty($refreshOffer)){
            $refreshOffer->start_date = $currentDate;
            //$refreshOffer->save(false);
        }else{
            return;
        }

        $campaign = $refreshOffer->getCampaign()->one();

        $xmlData = file_get_contents($campaign->original_products);

        $catalog = simplexml_load_string($xmlData);

        if (empty($catalog->shop) || empty($catalog->shop->categories) || empty($catalog->shop->categories->category)) {
            throw new Exception(Yii::t('app', 'Тэг категорий не найден'), 1);
        }

        foreach($catalog->shop->categories->category as $offerCategory) {

            $offerCategoryId = (int)$offerCategory->attributes()->id;
            $parentCategoryId = !empty($offerCategory->attributes()->parentId) ? (int)$offerCategory->attributes()->parentId : 0;

            $category = Category::find()
                ->andWhere(['offer_id' => $offerCategoryId])
                ->andWhere(['parent_offer_id' => $parentCategoryId])
                ->andWhere(['campaign_id' => $campaign->id])
                ->one();

            if (empty($category)){
                $category = new Category;
                $category->campaign_id = $campaign->id;
                $category->offer_id = $offerCategoryId;
                $category->parent_offer_id = $parentCategoryId;
            }elseif ($category->changeWhenUpdate == Category::CHANGE_WHEN_UPDATE_FALSE) {
                continue;
            }

            $category->name = $offerCategory;
            $category->save(false);
            echo 'Категория обработана: ' . $offerCategory . "\r\n";
        }

        foreach (Category::find()->where(['campaign_id' => $campaign->id])->all() as $category) {
            $parentCategory = Category::find()
                ->andWhere(['offer_id' => $category->parent_offer_id])
                ->andWhere(['campaign_id' => $campaign->id])
                ->one();

            if (empty($parentCategory)){
                $parentId = 0;
            }else{
                $parentId = $parentCategory->id;
            }

            $category->parent_id = $parentId;
            $category->save(false);
        }

        $categoryArray = Category::find()->where(['campaign_id' => $campaign->id])->all();

        for ($i = 0; $i < count($categoryArray); $i++) {
            $category = $categoryArray[$i];

            $parentCategory = Category::find()
                ->andWhere(['id' => $category->parent_id])
                ->andWhere(['campaign_id' => $campaign->id])
                ->one();

            if (!empty($parentCategory) && $parentCategory->id > $category->id) {
                $newCategory = new Category;
                $newCategory->setAttributes($category->attributes);
                $newCategory->save(false);

                $category->delete();
                $categoryArray = Category::find()->where(['campaign_id' => $campaign->id])->all();
                $i = 0;
            }
        }

        foreach ($categoryArray as $category) {
            if ($category->changeWhenUpdate == Category::CHANGE_WHEN_UPDATE_TRUE){
                $parentCategory = Category::find()->where(['id' => $category->parent_id])->one();

                $categoryTree = CategoryTree::find()
                    ->where(['parent' => $category->id])
                    ->andWhere(['child' => $category->id])
                    ->andWhere(['depth' => 0])
                    ->one();

                if (empty($parentCategory) && empty($categoryTree)) {
                    $category->saveNodeAsRoot(false);
                } elseif (!empty($parentCategory) && empty($categoryTree)) {
                    $category->appendTo($parentCategory);
                } elseif (!empty($parentCategory) && !empty($categoryTree)) {
                    $category->moveTo($parentCategory);
                }

                $category->key = strtolower(LangComponent::rus2translit($category->name . '_' . $category->id));
                $category->save(false);
            }
        }

        if (empty($catalog->shop->offers) || empty($catalog->shop->offers->offer)) {
            throw new Exception(Yii::t('app', 'Тэг продукта не найден'), 1);
        }

        foreach($catalog->shop->offers->offer as $offerProduct) {

            $offerProductId = (int)$offerProduct->attributes()->id;
            $available = !empty($offerCategory->attributes()->available) ? $offerCategory->attributes()->available : true;
            $category = Category::find()
                ->andWhere(['offer_id' => $offerProduct->categoryId])
                ->andWhere(['campaign_id' => $campaign->id])
                ->one();

            $product = Product::find()
                ->andWhere(['offer_id' => $offerProductId])
                ->andWhere(['campaign_id' => $campaign->id])
                ->one();

            if (empty($product)) {
                $product = new Product;
                $product->offer_id = $offerProductId;
                $product->campaign_id = $campaign->id;
            }elseif ($product->changeWhenUpdate == Product::CHANGE_WHEN_UPDATE_FALSE) {
                continue;
            }

            $product->key = strtolower(LangComponent::rus2translit($offerProduct->name . '_' . $offerProduct->model . '_' . $offerProduct->id));
            $product->available = $available;
            $product->description = $offerProduct->description;
            $product->category_offer_id = $offerProduct->categoryId;
            $product->category_id = $category->id;
            $product->cpa = $offerProduct->cpa;
            $product->currencyId = $offerProduct->currencyId;
            $product->delivery = $offerProduct->delivery;
            $product->manufacturer_warranty = $offerProduct->manufacturer_warranty;
            $product->model = $offerProduct->model;
            $product->modified_time = $offerProduct->modified_time;
            $product->name = $offerProduct->name;
            $product->pickup = $offerProduct->param;
            $product->price = $offerProduct->price;
            $product->typePrefix = $offerProduct->typePrefix;
            $product->url = $offerProduct->url;
            $product->vendor = $offerProduct->vendor;
            $product->vendorCode = $offerProduct->vendorCode;
            $product->save(false);

            if (!empty($offerProduct->picture)){
                try{
                    file_put_contents(self::productLogo($product->id), file_get_contents($offerProduct->picture));
                    $product->pictures = self::OFFER_PRODUCT_PATH . $product->id . self::OFFER_PRODUCT_EXT;
                }catch(yii\base\ErrorException $e){
                    echo $e->getMessage() . "\r\n";
                    $product->pictures = $offerProduct->picture;
                }
            }

            $product->save(false);
            echo 'Товар обработан: ' . $offerProduct->name . "\r\n";

            if (!empty($offerProduct->param)) {
                foreach($offerProduct->param as $offerParam) {
                    $paramKey = strtolower(LangComponent::rus2translit($offerParam->attributes()->name));

                    $param = Param::find()->where(['key' => $paramKey])->andWhere(['campaign_id' => $refreshOffer->id])->one();

                    if (empty($param)){
                        $param = new Param;
                        $param->name = $offerParam->attributes()->name;
                        $param->key = strtolower(LangComponent::rus2translit($offerParam->attributes()->name));
                        $param->campaign_id = $refreshOffer->id;
                        $param->save(false);
                    }

                    $productParam = ProductParam::find()
                        ->where(['param_id' => $param->id])
                        ->andWhere(['product_id' => $product->id])
                        ->andWhere(['campaign_id' => $refreshOffer->id])->one();

                    if (empty($productParam)){
                        $productParam = new ProductParam;
                        $productParam->param_id = $param->id;
                        $productParam->product_id = $product->id;
                        $productParam->campaign_id = $refreshOffer->id;
                        $productParam->value = $offerParam;
                        $productParam->save(false);
                    }

                    $paramCategory = ParamCategory::find()
                        ->where(['param_id' => $param->id])
                        ->andWhere(['category_id' => $category->id])
                        ->andWhere(['campaign_id' => $refreshOffer->id])
                        ->andWhere(['value' => $offerParam])
                        ->one();

                    if (empty($paramCategory)) {
                        $paramCategory = new ParamCategory;
                        $paramCategory->param_id = $param->id;
                        $paramCategory->category_id = $category->id;
                        $paramCategory->campaign_id = $refreshOffer->id;
                        $paramCategory->value = $offerParam;
                        $paramCategory->save(false);
                    }
                }
            }
        }

        foreach (Category::find()->where(['campaign_id' => $campaign->id])->all() as $category) {

            $categoryArray = array_merge([$category->id => $category->id], ArrayHelper::map($category->descendants()->all(), 'id', 'id'));

            $minProduct = Product::find()
                ->where(['in', 'category_id', $categoryArray])
                ->orderBy(['price' => SORT_ASC])->one();


            $maxProduct = Product::find()
                ->where(['in', 'category_id', $categoryArray])
                ->orderBy(['price' => SORT_DESC])->one();

            $category->min_product_price = empty($minProduct) ? 0 : $minProduct->price;
            $category->max_product_price = empty($maxProduct) ? 0 : $maxProduct->price;
            $category->save();

            echo 'Определена мин/макс цена для категории: ' . $category->id . "\r\n";
        }
    }

    private static function offerLogo($name) {
        return realpath(dirname(__FILE__)) . '/../../../../web' . self::OFFER_LOGO_PATH . $name . self::OFFER_LOGO_EXT;
    }

    private static function productLogo($name) {
        return realpath(dirname(__FILE__)) . '/../../../../web' . self::OFFER_PRODUCT_PATH . $name . self::OFFER_PRODUCT_EXT;
    }
}