<?php

namespace app\modules\shop;

use yii\base\BootstrapInterface;
/**
 * shop module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\shop\controllers';

    public $photoPath1 = 'attach/shop';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'category/<key>',
                'route' => 'shop/shop/category',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'product/<key>',
                'route' => 'shop/shop/product',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'order/<key>',
                'route' => 'shop/shop/order',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'productsearch/<word>',
                'route' => 'shop/shop/productsearch',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'productsearch',
                'route' => 'shop/shop/productsearch',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'campaign/<key>',
                'route' => 'shop/shop/campaign',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'shop/captcha',
                'route' => 'shop/shop/captcha',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'sitemap.xml',
                'route' => 'shop/shop/sitemap',
            ]
        ], false);
    }
}
