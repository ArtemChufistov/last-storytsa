<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%campaign}}".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string $site_url
 * @property string $logo
 * @property string $date_start
 * @property string $description
 * @property string $geotargeting
 * @property string $currency_id
 * @property string $postclick_cookie
 * @property double $rating
 * @property double $ecpm
 * @property double $ecpc
 * @property double $epc
 * @property double $cr
 * @property double $avg_hold_time
 * @property double $avg_money_transfer_time
 * @property string $actions
 * @property string $regions
 * @property string $categories
 * @property string $admitad_last_update
 * @property string $advertiser_last_update
 * @property string $delivery-options
 */
class Campaign extends \yii\db\ActiveRecord
{
    const CHANGE_WHEN_UPDATE_TRUE = 1;
    const CHANGE_WHEN_UPDATE_FALSE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%campaign}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['date_start', 'admitad_last_update', 'advertiser_last_update', 'original_products', 'changeWhenUpdate', 'key'], 'safe'],
            [['description', 'actions', 'regions', 'categories', 'delivery-options'], 'string'],
            [['rating', 'ecpm', 'ecpc', 'epc', 'cr', 'avg_hold_time', 'avg_money_transfer_time', 'offer_id'], 'number'],
            [['name', 'alias', 'site_url', 'logo', 'geotargeting', 'currency_id'], 'string', 'max' => 255],
            [['postclick_cookie'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Ключ'),
            'name' => Yii::t('app', 'Название'),
            'alias' => Yii::t('app', 'Алиас'),
            'site_url' => Yii::t('app', 'Адрес сайта'),
            'logo' => Yii::t('app', 'Логотип'),
            'date_start' => Yii::t('app', 'Дата старта'),
            'description' => Yii::t('app', 'Описание'),
            'geotargeting' => Yii::t('app', 'Geotargeting'),
            'currency_id' => Yii::t('app', 'Валюта'),
            'postclick_cookie' => Yii::t('app', 'Postclick Cookie'),
            'rating' => Yii::t('app', 'Рейтинг'),
            'ecpm' => Yii::t('app', 'ECPM'),
            'ecpc' => Yii::t('app', 'ECPC'),
            'epc' => Yii::t('app', 'EPC'),
            'cr' => Yii::t('app', 'CR'),
            'avg_hold_time' => Yii::t('app', 'Avg Hold Time'),
            'avg_money_transfer_time' => Yii::t('app', 'Avg Money Transfer Time'),
            'actions' => Yii::t('app', 'Actions'),
            'regions' => Yii::t('app', 'Regions'),
            'categories' => Yii::t('app', 'Categories'),
            'admitad_last_update' => Yii::t('app', 'Admitad Last Update'),
            'advertiser_last_update' => Yii::t('app', 'Advertiser Last Update'),
            'delivery_options' => Yii::t('app', 'Delivery Options'),
            'offer_id' => Yii::t('app', 'ID магазина'),
            'original_products' => Yii::t('app', 'Ссылка на продукты'),
            'changeWhenUpdate' => Yii::t('app', 'Перезаписывать при обновлении'),
        ];
    }
}
