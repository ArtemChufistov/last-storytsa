<?php
namespace app\modules\shop\models;

use yii\db\ActiveRecord;
use valentinek\behaviors\ClosureTableQuery;
use \yii\db\ActiveQuery;

class CategoryQuery extends ActiveQuery
{
    public function behaviors() {
        return [
            [
                'class' => ClosureTableQuery::className(),
                'tableName' => 'category_tree'
            ],
        ];
    }
}

?>