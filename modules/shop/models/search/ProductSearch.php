<?php

namespace app\modules\shop\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\modules\shop\models\Product`.
 */
class ProductSearch extends Product
{
    public $categoryes;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'offer_id', 'available', 'campaign_id', 'category_offer_id', 'category_id', 'cpa', 'delivery', 'manufacturer_warranty', 'pickup'], 'integer'],
            [['currencyId', 'model', 'modified_time', 'name', 'typePrefix', 'url', 'pictures', 'vendor', 'vendorCode', 'param'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'offer_id' => $this->offer_id,
            'available' => $this->available,
            'campaign_id' => $this->campaign_id,
            'category_offer_id' => $this->category_offer_id,
            'category_id' => $this->category_id,
            'cpa' => $this->cpa,
            'delivery' => $this->delivery,
            'manufacturer_warranty' => $this->manufacturer_warranty,
            'modified_time' => $this->modified_time,
            'pickup' => $this->pickup,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'currencyId', $this->currencyId])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'typePrefix', $this->typePrefix])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'pictures', $this->pictures])
            ->andFilterWhere(['like', 'vendor', $this->vendor])
            ->andFilterWhere(['like', 'vendorCode', $this->vendorCode]);

        if (!empty($this->categoryes)) {
            $query->andFilterWhere(['in', 'category_id', $this->categoryes]);
        }

        return $dataProvider;
    }
}
