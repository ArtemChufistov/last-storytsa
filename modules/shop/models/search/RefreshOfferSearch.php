<?php

namespace app\modules\shop\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\RefreshOffer;

/**
 * RefreshOfferSearch represents the model behind the search form of `app\modules\shop\models\RefreshOffer`.
 */
class RefreshOfferSearch extends RefreshOffer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'campaign_id'], 'integer'],
            [['refrersh_date', 'start_date', 'end_date', 'new_categoryes', 'new_products', 'change_products'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefreshOffer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'campaign_id' => $this->campaign_id,
            'refrersh_date' => $this->refrersh_date,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
        ]);

        $query->andFilterWhere(['like', 'new_categoryes', $this->new_categoryes])
            ->andFilterWhere(['like', 'new_products', $this->new_products])
            ->andFilterWhere(['like', 'change_products', $this->change_products]);

        return $dataProvider;
    }
}
