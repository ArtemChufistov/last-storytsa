<?php

namespace app\modules\shop\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\Campaign;

/**
 * CampaignSearch represents the model behind the search form of `app\modules\shop\models\Campaign`.
 */
class CampaignSearch extends Campaign
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'alias', 'site_url', 'logo', 'date_start', 'description', 'geotargeting', 'currency_id', 'postclick_cookie', 'actions', 'regions', 'categories', 'admitad_last_update', 'advertiser_last_update', 'delivery-options', 'original_products'], 'safe'],
            [['rating', 'ecpm', 'ecpc', 'epc', 'cr', 'avg_hold_time', 'avg_money_transfer_time', 'offer_id'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Campaign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_start' => $this->date_start,
            'rating' => $this->rating,
            'ecpm' => $this->ecpm,
            'ecpc' => $this->ecpc,
            'epc' => $this->epc,
            'cr' => $this->cr,
            'avg_hold_time' => $this->avg_hold_time,
            'avg_money_transfer_time' => $this->avg_money_transfer_time,
            'admitad_last_update' => $this->admitad_last_update,
            'advertiser_last_update' => $this->advertiser_last_update,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'site_url', $this->site_url])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'geotargeting', $this->geotargeting])
            ->andFilterWhere(['like', 'currency_id', $this->currency_id])
            ->andFilterWhere(['like', 'postclick_cookie', $this->postclick_cookie])
            ->andFilterWhere(['like', 'actions', $this->actions])
            ->andFilterWhere(['like', 'regions', $this->regions])
            ->andFilterWhere(['like', 'categories', $this->categories]);

        return $dataProvider;
    }
}
