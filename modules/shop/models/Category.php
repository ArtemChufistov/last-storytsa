<?php

namespace app\modules\shop\models;

use valentinek\behaviors\ClosureTable;
use Yii;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $id
 * @property string $key
 * @property string $name
 * @property string $campaign_id
 * @property string $offer_id
 *
 * @property CategoryTree[] $categoryTrees
 * @property CategoryTree[] $categoryTrees0
 * @property Category[] $parents
 * @property Category[] $children
 */
class Category extends \yii\db\ActiveRecord
{
    const CHANGE_WHEN_UPDATE_TRUE = 1;
    const CHANGE_WHEN_UPDATE_FALSE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    public static function find()
    {
        return new CategoryQuery(static::className());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'name', 'campaign_id', 'offer_id'], 'required'],
            [['parent_offer_id', 'changeWhenUpdate', 'parent_id', 'min_product_price', 'max_product_price'], 'safe'],
            [['key', 'name', 'campaign_id', 'offer_id'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [[
            'class' => ClosureTable::className(),
            'tableName' => 'category_tree'
        ]];
    }

    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Род. категория'),
            'key' => Yii::t('app', 'Ключ'),
            'name' => Yii::t('app', 'Название'),
            'min_product_price' => Yii::t('app', 'Мин. цена продукта'),
            'max_product_price' => Yii::t('app', 'Макс. цена продукта'),
            'campaign_id' => Yii::t('app', 'Компания'),
            'offer_id' => Yii::t('app', 'Offer ID'),
            'parent_offer_id' => Yii::t('app', 'Parent Offer ID'),
            'changeWhenUpdate' => Yii::t('app', 'Перезаписывать при обновлении'),
        ];
    }
}
