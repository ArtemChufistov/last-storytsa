<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%refresh_offer}}".
 *
 * @property int $id
 * @property string $refrersh_date
 * @property string $start_date
 * @property string $end_date
 * @property string $new_categoryes
 * @property string $new_products
 * @property string $change_products
 */
class RefreshOffer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%refresh_offer}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['refrersh_date', 'start_date', 'end_date', 'campaign_id'], 'safe'],
            [['new_categoryes', 'new_products', 'change_products'], 'string'],
        ];
    }

    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'campaign_id' => Yii::t('app', 'Компания'),
            'refrersh_date' => Yii::t('app', 'Дата обновления'),
            'start_date' => Yii::t('app', 'Дата запуска'),
            'end_date' => Yii::t('app', 'Дата окончания'),
            'new_categoryes' => Yii::t('app', 'New Categoryes'),
            'new_products' => Yii::t('app', 'New Products'),
            'change_products' => Yii::t('app', 'Change Products'),
        ];
    }
}
