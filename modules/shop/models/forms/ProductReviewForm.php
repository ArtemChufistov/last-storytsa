<?php

namespace app\modules\shop\models\forms;

use app\modules\profile\models\User;
use Yii;
use yii\base\Model;

/**
 * Форма авторизации
 * Class LoginForm
 * @package lowbase\user\models\forms
 */
class ProductReviewForm extends Model {

    public $name;
    public $text;
    public $captcha;

    /**
     * Правила валидации
     * @return array
     */
    public function rules() {
        return [
            [['text'], 'string'],
            [['date_add'], 'safe'],
            [['name', 'text', 'captcha'], 'required'],
            ['captcha', 'captcha', 'captchaAction' => '/shop/shop/captcha'],
            [['name'], 'string', 'max' => 255],

        ];
    }

    /**
     * Наименование полей формы
     * @return array
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Ваше имя'),
            'text' => Yii::t('app', 'Текст отзыва'),
            'captcha' => Yii::t('app', 'Символы с картинки'),
        ];
    }
}
