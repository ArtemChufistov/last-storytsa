<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "product_review".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property int $active
 * @property string $date_add
 */
class ProductReview extends \yii\db\ActiveRecord
{
    const ACTIVE_TRUE = 1;
    const ACTIVE_FALSE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['active', 'product_id'], 'integer'],
            [['date_add'], 'safe'],
            [['name', 'text'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'name' => 'Name',
            'text' => 'Text',
            'active' => 'Active',
            'date_add' => 'Date Add',
        ];
    }
}
