<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%param}}".
 *
 * @property int $id
 * @property int $campaign_id
 * @property string $name
 * @property string $key
 */
class Param extends \yii\db\ActiveRecord
{
    const KEY_CVET = 'cvet';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%param}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['campaign_id'], 'required'],
            [['campaign_id'], 'integer'],
            [['name', 'key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'campaign_id' => Yii::t('app', 'Campaign ID'),
            'name' => Yii::t('app', 'Name'),
            'key' => Yii::t('app', 'Key'),
        ];
    }
}
