<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%refresh_campaign}}".
 *
 * @property int $id
 * @property string $refrersh_date
 * @property string $start_date
 * @property string $end_date
 */
class RefreshCampaign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%refresh_campaign}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['refrersh_date', 'start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'refrersh_date' => Yii::t('app', 'Дата обновления'),
            'start_date' => Yii::t('app', 'Дата запуска'),
            'end_date' => Yii::t('app', 'Дата окончания'),
        ];
    }
}
