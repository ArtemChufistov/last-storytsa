<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%product_param}}".
 *
 * @property int $id
 * @property int $param_id
 * @property int $product_id
 * @property int $campaign_id
 */
class ProductParam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product_param}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['param_id', 'product_id', 'campaign_id'], 'required'],
            [['param_id', 'product_id', 'campaign_id'], 'integer'],
            [['value'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'param_id' => Yii::t('app', 'Param ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'campaign_id' => Yii::t('app', 'Campaign ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }
}
