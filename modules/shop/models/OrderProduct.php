<?php

namespace app\modules\shop\models;

use app\modules\shop\models\Product;
use Yii;

/**
 * This is the model class for table "order_product".
 *
 * @property int $id
 * @property int $product_id
 * @property string $date_add
 */
class OrderProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'category_id', 'campaign_id'], 'integer'],
            [['date_add'], 'safe'],
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'category_id' => 'Category ID',
            'campaign_id' => 'Campaign ID',
            'date_add' => 'Date Add',
        ];
    }
}
