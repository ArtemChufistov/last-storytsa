<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $id
 * @property int $offer_id
 * @property int $available
 * @property int $campaign_id
 * @property int $category_offer_id
 * @property int $category_id
 * @property int $cpa
 * @property string $currencyId
 * @property int $delivery
 * @property int $manufacturer_warranty
 * @property string $model
 * @property string $modified_time
 * @property string $name
 * @property int $pickup
 * @property double $price
 * @property string $typePrefix
 * @property string $url
 * @property string $pictures
 * @property string $vendor
 * @property string $vendorCode
 */
class Product extends \yii\db\ActiveRecord
{
    const CHANGE_WHEN_UPDATE_TRUE = 1;
    const CHANGE_WHEN_UPDATE_FALSE = 0;

    const BESTSELLER_TRUE = 1;
    const BESTSELLER_FALSE = 0;

    const SPECIAL_TRUE = 1;
    const SPECIAL_FALSE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['offer_id', 'available', 'campaign_id', 'category_offer_id', 'category_id', 'cpa', 'delivery', 'manufacturer_warranty', 'pickup', 'bestseller', 'special', 'new', 'discount'], 'integer'],
            [['campaign_id', 'category_offer_id', 'category_id'], 'required'],
            [['model', 'name', 'url', 'pictures'], 'string'],
            [['modified_time', 'param', 'changeWhenUpdate', 'description', 'sales_notes'], 'safe'],
            [['price'], 'number'],
            [['currencyId', 'typePrefix', 'vendor', 'vendorCode'], 'string', 'max' => 255],
        ];
    }

    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Ключ'),
            'offer_id' => Yii::t('app', 'Offer ID'),
            'available' => Yii::t('app', 'Available'),
            'campaign_id' => Yii::t('app', 'Компания'),
            'category_offer_id' => Yii::t('app', 'Category Offer ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'cpa' => Yii::t('app', 'Cpa'),
            'currencyId' => Yii::t('app', 'Валюта'),
            'delivery' => Yii::t('app', 'Delivery'),
            'manufacturer_warranty' => Yii::t('app', 'Manufacturer Warranty'),
            'model' => Yii::t('app', 'Модель'),
            'modified_time' => Yii::t('app', 'Modified Time'),
            'name' => Yii::t('app', 'Название'),
            'description' => Yii::t('app', 'Описание'),
            'sales_notes' => Yii::t('app', 'Sales notes'),
            'pickup' => Yii::t('app', 'Pickup'),
            'price' => Yii::t('app', 'Цена'),
            'typePrefix' => Yii::t('app', 'Type Prefix'),
            'url' => Yii::t('app', 'Url'),
            'pictures' => Yii::t('app', 'Изображение'),
            'vendor' => Yii::t('app', 'Vendor'),
            'vendorCode' => Yii::t('app', 'Vendor Code'),
            'param' => Yii::t('app', 'Param'),
            'changeWhenUpdate' => Yii::t('app', 'Перезаписывать при обновлении'),
            'discount' => Yii::t('app', 'Дисконт'),
            'new' => Yii::t('app', 'Новинка'),
            'special' => Yii::t('app', 'Спец'),
            'bestseller' => Yii::t('app', 'Лучшие продажи'),
        ];
    }
}
