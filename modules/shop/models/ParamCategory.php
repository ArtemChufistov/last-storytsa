<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%param_category}}".
 *
 * @property int $id
 * @property int $param_id
 * @property int $category_id
 * @property int $campaign_id
 */
class ParamCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%param_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['param_id', 'category_id', 'campaign_id'], 'required'],
            [['value'], 'safe'],
            [['param_id', 'category_id', 'campaign_id'], 'integer'],
        ];
    }

    public function getParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'param_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'value' => Yii::t('app', 'Value'),
            'param_id' => Yii::t('app', 'Param ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'campaign_id' => Yii::t('app', 'Campaign ID'),
        ];
    }
}
