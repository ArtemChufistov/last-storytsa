<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "product_search".
 *
 * @property int $id
 * @property string $word
 * @property string $date_add
 */
class ProductSearch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_search';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_add'], 'safe'],
            [['word'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'word' => 'Word',
            'date_add' => 'Date Add',
        ];
    }
}
