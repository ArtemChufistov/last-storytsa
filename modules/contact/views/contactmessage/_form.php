<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\contact\models\ContactMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
            <div class="contact-message-form">

                <?php $form = ActiveForm::begin(); ?>

                <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'initValueText' => $model->getUser()->one()->login,
                    'value' => $model->getUser()->one()->id,
                    'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'minimumInputLength' => 2,
                        'ajax' => [
                            'url' => Url::to(['/profile/backuser/searhbylogin']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.login; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                    ],
                ]);?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'message')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', []),
                ]); ?>

                <?= $form->field($model, 'date_add')
                ->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => $model->getAttributeLabel('date_add')],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
      </div>
    </div>
</div>