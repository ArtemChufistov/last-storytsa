<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\contact\models\ContactMessage */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Обратная свзяь'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-message-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'user_id',
                    'name',
                    'email:email',
                    'message:ntext',
                    'date_add',
                ],
            ]) ?>

        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo Yii::t('app', 'Отправить сообщение на почту');?></h3>
        </div>

        <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">

                <?= $form->field($sendMailForm, 'email')->textInput(['maxlength' => true, 'readonly' => true, 'value' => $model->email]) ?>

                <?= $form->field($sendMailForm, 'theme')->textInput(['maxlength' => true, 'value' => Yii::t('app', 'Ответ на запрос обратной связи')]); ?>

                <?= $form->field($sendMailForm, 'text')->textArea(['id' => 'compose-textarea']); ?>

            </div>

        <div class="box-footer">
          <div class="pull-right">
            <?= Html::submitButton(Yii::t('app', '<i class="fa fa-envelope-o"></i> Отправить письмо'), ['class' => 'btn btn-success']) ?>
          </div>

        </div>

        <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>
<?php
$this->registerJs("
$(function () {
    $(\"#compose-textarea\").wysihtml5();
  });
");
?>