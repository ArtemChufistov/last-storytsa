<?php

use yii\web\JsExpression;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use app\modules\contact\models\ContactMessage;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\menu\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Обратная связь');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
              <h1><?= Html::encode($this->title) ?></h1>
            </div>
            <?php Pjax::begin(); ?>

                <div class="box-body no-padding">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            [
                              'attribute' => 'user_id',
                              'format' => 'raw',
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[user_id]',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Введите логин'),
                                  ],
                                  'pluginOptions' => [
                                      'minimumInputLength' => 2,
                                      'ajax' => [
                                          'url' => Url::to(['/profile/backuser/searhbylogin']),
                                          'dataType' => 'json',
                                          'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                      ],
                                      'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                      'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                      'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                                  ]
                              ]),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
                            ],
                            'name',
                            'email:email',
                            'message:ntext',
                            [
                              'attribute' => 'date_add',
                              'format' => 'raw',
                              'filter' => true,
                              'filter' => DatePicker::widget([
                                  'model'=>$searchModel,
                                  'attribute'=>'date_add',
                                  'options' => ['class' => 'form-control'],
                                  'language' => 'ru',
    
                              ]),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_date-add', ['model' => $model]);},
                            ],
                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            <?php Pjax::end(); ?>
          </div>
        </div>
    </div>
</div>
