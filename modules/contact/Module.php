<?php

namespace app\modules\contact;

use yii\base\BootstrapInterface;

/**
 * contact module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\contact\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'contact/feedback',
                'route' => 'contact/contact/index'
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'contact/captcha',
                'route' => 'contact/contact/captcha'
            ]
        ]);
    }
}
