<?php

namespace app\modules\contact\models;

use Yii;
use app\modules\profile\models\User;

/**
 * This is the model class for table "contact_message".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $message
 * @property string $date_add
 */
class ContactMessage extends \yii\db\ActiveRecord
{
    public $captcha;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['name', 'email', 'message', 'captcha'], 'required'],
            ['email', 'email'],
            [['message'], 'string', 'max'=>250],
            [['name', 'message', 'date_add'], 'safe'],
            [['captcha'], 'captcha', 'captchaAction' => 'contact/contact/captcha'], // Проверка капчи
            [['name','email', 'message'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            [['name', 'email'], 'string', 'max' => 255],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord == true){
            $this->date_add = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function typeNames()
    {
        return [
            'test',
            'test2',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Логин участника'),
            'name' => Yii::t('app', 'Имя'),
            'email' => Yii::t('app', 'E-mail'),
            'captcha' => Yii::t('app', 'Защитный код'),
            'message' => Yii::t('app', 'Текст сообщения'),
            'date_add' => Yii::t('app', 'Дата добавления'),
        ];
    }
}
