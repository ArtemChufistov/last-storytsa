<?php

namespace app\modules\contact\models\forms;

use app\modules\contact\models\ContactMessage;
use app\modules\mail\models\Mail;
use yii\helpers\Html;
use Yii;

class SendMailForm extends ContactMessage
{
    public $email;
    public $theme;
    public $text;

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return [
            [['email', 'theme', 'text'], 'required'],   // Обязательные поля
        ];
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['email'] = Yii::t('app', 'E-mail');
        $labels['theme'] = Yii::t('app', 'Тема письма');
        $labels['text'] = Yii::t('app', 'Текст письма');

        return $labels;
    }

    /**
     * Отправка email
     */
    public function compose()
    {
        $mail = new Mail;
        $mail->to = $this->email;
        $mail->title = $this->theme;
        $mail->content = $this->text;
        $mail->date_add = date('Y-m-d H:i:s');

        $mail->save();
    }
}
