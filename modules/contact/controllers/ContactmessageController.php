<?php

namespace app\modules\contact\controllers;

use Yii;
use app\modules\contact\models\ContactMessage;
use app\modules\contact\models\ContactMessageSearch;
use app\modules\contact\models\forms\SendMailForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\admin\controllers\AdminController;
use yii\filters\AccessControl;
/**
 * ContactmessageController implements the CRUD actions for ContactMessage model.
 */
class ContactmessageController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['contactMessageIndex'],
                    ],[
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['contactMessageView'],
                    ],[
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['contactMessageCreate'],
                    ],[
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['contactMessageUpdate'],
                    ],[
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['contactMessageDelete'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ContactMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContactMessage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $sendMailForm = new SendMailForm;

        if ($sendMailForm->load(Yii::$app->request->post()) && $sendMailForm->validate()) {
            $sendMailForm->compose();
            return $this->redirect(['index']);
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'sendMailForm' => $sendMailForm
        ]);
    }

    /**
     * Creates a new ContactMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ContactMessage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ContactMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ContactMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ContactMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContactMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContactMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
