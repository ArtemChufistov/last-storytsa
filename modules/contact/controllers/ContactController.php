<?php

namespace app\modules\contact\controllers;

use Yii;
use app\modules\contact\models\ContactMessage;
use app\components\FrontendController;
use yii\filters\AccessControl;

class ContactController extends FrontendController
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => null,
                'foreColor' => '3373751' //синий
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['captcha'],
                'rules' => [
                    [
                        'actions' => ['captcha'],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

	public function actionIndex()
	{
		$this->title = Yii::t('app', 'Связаться с нами');

	    $model = new ContactMessage();

	    if ($model->load(Yii::$app->request->post())) {
	        if ($model->save()) {

	        	Yii::$app->session->setFlash('success');
	        }
	    }

	    return $this->render('contact/contact', [
	        'model' => $model,
	    ]);

	}
}