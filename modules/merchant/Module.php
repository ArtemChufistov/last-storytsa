<?php

namespace app\modules\merchant;

use yii\base\BootstrapInterface;

/**
 * profile module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\merchant\controllers';

    // Мерчант свойства
    public $wallets = [];
    public $allowedUsers;
    public $mainPageCache = 0;
    // END Мерчант свойства

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }

    //Массив с собственными отображениями
    public $customViews = [];
    //Массив с собственными шаблонами писем
    public $customMailViews = [];
    //Action для капчи
    public $captchaAction = '/user/default/captcha';

    public $userPhotoPath = 'attach/user/images';

    public $photoPath1 = 'attach/response/images1';
    public $photoPath2 = 'attach/response/images2';
    public $photoPath3 = 'attach/response/images3';

    /**
     * Собственные отображения
     * Допустимые параметры:
     *
     * signup - регистрация
     * login - авторизация
     * profile - профиль
     * repass - восстановление пароля
     * show - просмотр пользователя
     *
     * @param $customView - отображение
     * @param $default - отображение по умолчанию
     * @return mixed
     */
    public function getCustomView($customView, $default)
    {
        if (isset($this->customViews[$customView])) {
            return $this->customViews[$customView];
        } else {
            return $default;
        }
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'm-account/<action>',
                'route' => 'merchant/m-account/<action>'
            ],
        ], false);
    }
}
