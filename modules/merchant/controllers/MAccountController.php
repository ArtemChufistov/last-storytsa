<?php

namespace app\modules\merchant\controllers;

use app\modules\merchant\components\merchant\Ethereum;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use app\modules\merchant\models\MAccount;
use app\modules\merchant\models\MAccountWallet;
use app\modules\merchant\models\MCoinmarketcapTicker;
use app\modules\merchant\models\search\MAccountSearch;
use app\modules\merchant\models\search\MAccountWalletSearch;
use app\modules\merchant\models\search\MNodesTransactionsSearch;
use app\modules\merchant\models\search\MSendTxidSearch;
use app\modules\merchant\models\TransferForm;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use app\components\UserController;
use yii\web\Response;


/**
 * MAccountController implements the CRUD actions for MAccount model.
 */
class MAccountController extends UserController
{
    public $user;

//    public function init()
//    {
//        $this->layout = '@app/views/layouts/main.php';
//    }

    public function init()
    {
        $this->user = Yii::$app->user->identity;
        $this->view->params['user'] = Yii::$app->user;

        if (!empty($this->user)) {
            $userInfo = $this->user->initUserInfo();
            $userInfo->lang = Yii::$app->language;
            $userInfo->online_date = date('Y-m-d H:i:s');
            $userInfo->save(false);
        }

        $this->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/office';
    }


    public function beforeAction($action)
    {
        if ($action->id == 'test') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }


    /**
     * Разделение ролей
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'update', 'delete', 'newaddress'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'update', 'delete', 'newaddress'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['userAutorized'],
                    ],
//                    [
//                        'actions' => ['view'],
//                        'allow' => true,
//                        'roles' => ['userView'],
//                    ],
//                    [
//                        'actions' => ['update', 'rmv', 'multiactive', 'multiblock'],
//                        'allow' => true,
//                        'roles' => ['userUpdate'],
//                    ],
//                    [
//                        'actions' => ['delete', 'multidelete'],
//                        'allow' => true,
//                        'roles' => ['userDelete'],
//                    ],
                ],
            ],
        ];
    }

    /**
     * + Список мерчантов владельца
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MAccountSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(
            Yii::$app->request->queryParams,
            [$searchModel->formName() => ['user_id' => Yii::$app->user->identity->id, 'is_delete' => 0]]
        ));
        $dataProvider->pagination = ['pageSize' => 1000];
        $dataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];


        return $this->render('m-account/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * + Список всех транзакций
     *
     * @return mixed
     */
    public function actionAllTransactions()
    {
//        $MAccountArray = ArrayHelper::map(MAccount::find()
//            ->where(['user_id' => Yii::$app->user->identity->id])
//            ->andWhere(['is_delete' => 0])
//            ->orderBy('id ASC')
//            ->all(), 'id', 'id');

        $searchModel = new MNodesTransactionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['user_id' => Yii::$app->user->identity->id]);
        $dataProvider->query->andFilterWhere(['in', 'currency', Yii::$app->modules['merchant']->wallets]);
       // $dataProvider->query->andFilterWhere(['in', 'm_id', $MAccountArray]);
       // $dataProvider->query->andFilterWhere(['isShowToUser' => 1]);
        $dataProvider->pagination = ['pageSize' => 1000];
        $dataProvider->sort = ['defaultOrder' => ['blockchain_time' => SORT_DESC]];


        return $this->render('m-account/all_transactions', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * + Список всех адресов
     *
     * @return mixed
     */
    public function actionAllAddresses()
    {
        $searchModel = new MAccountWalletSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['user_id' => Yii::$app->user->identity->id]);
        $dataProvider->query->andFilterWhere(['in', 'currency_type', Yii::$app->modules['merchant']->wallets]);

        $dataProvider->pagination = ['pageSize' => 20];
        $dataProvider->sort = ['defaultOrder' => ['created_at' => SORT_DESC]];


        return $this->render('m-account/all_addresses', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param MAccount $model
     * @return array
     */
    public function walletsInfo($model)
    {

        $wallets = [];

        foreach ($this->module->wallets as $wallet) {

            $wallets[$wallet]['m_id'] = $model->id;
            $wallets[$wallet]['name'] = $wallet;

            try {
                $wallets[$wallet]['balance'] = Yii::$app->get($wallet)->getAddressesBalanceSum($model->id);
            } catch (\Exception $e) {
                $wallets[$wallet]['balance'] = -1;
                Yii::error($e->getMessage());
            }

            if ($wallets[$wallet]['balance'] != -1) {
                $wallets[$wallet]['balance'] = MerchantHelper::coinPriceUSD($wallet, $wallets[$wallet]['balance'], true);
            }
        }

        $walletDataProvider = new ArrayDataProvider([
            'allModels' => $wallets,
//            'pager' => ['maxButtonCount' => 500],
            'pagination' => [
                'pageSize' => 1000,
            ],
            'sort' => [
                'attributes' =>
                    [
                        'm_id',
                        'name',
                        'balance',
                    ],
                'defaultOrder' => ['name' => SORT_ASC]
            ],
        ]);

        return $walletDataProvider;
    }

    /**
     * Список кошельков различных блокчейн систем
     *
     * @param $id integer
     * @return mixed
     */
    public function actionWallets($id)
    {
// /** @var Ethereum $wall */
//      //  $wall = Yii::$app->get('ethereum');
//        echo "<pre>";
//       // print_r($wall->showAddresses());
//
//        $data = Yii::$app->get('ethereum')->getClient()->call('eth_gasPrice', []);
////        Yii::$app->get($currency)->getClient()->call('eth_gasPrice', [])
//        $info = print_r($data['result'], true);
// echo $info;
//
//
//die;

        ////////
        $model = $this->findModel($id);

        $walletDataProvider = $this->walletsInfo($model);


        // Форма перевода
        $transferForm = new TransferForm();
        $transferForm->m_id = $id;


        if ($transferForm->load(Yii::$app->request->post()) && $transferForm->validate()) {

            $txid = Yii::$app->get($transferForm->currency)->sendFrom($id, $transferForm->address, $transferForm->amount, $transferForm->from_address);

            if (is_string($txid)) {
                $msg = Yii::t('app', 'Перевод {amount} на {address} успешно завершен. Txid: {txId}', [
                    'amount' => $transferForm->amount,
                    'address' => $transferForm->address,
                    'txId' => $txid
                ]);

                Yii::$app->session->setFlash('mTransfer', $msg);
            }

        }


        return $this->render('m-account/wallets', [
            'model' => $model,
            'walletDataProvider' => $walletDataProvider,
            'transferForm' => $transferForm
        ]);
    }

    /**
     * + Создать новый адрес
     *
     * @param $id integer
     * @param $currency string
     * @return mixed
     */
    public function actionNewaddress($currency)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        try {
            $address = Yii::$app->get($currency)->generateAddress(Yii::$app->user->identity->id);
//            Yii::$app->session->setFlash('mNewAddress', 'Новый адрес ' . $currency . " - " . $address);
            return ['mNewAddress' => 'Новый адрес ' . MerchantHelper::prettyCoinName($currency) . " - " . $address];
        } catch (\Exception $e) {
//            Yii::error($e->getMessage());
            return ['mNewAddress' => 'Что-то пошло не так - ошибка'];
        }
//        return $this->redirect(['/m-account/wallets', 'id' => $model->id]);
    }

    /**
     * + Адреса Кошелька
     *
     * @param $id integer
     * @param $currency string
     * @return mixed
     */
    public function actionWalletAddresses($id, $currency)
    {
        $model = $this->findModel($id);
        $addresses = [];

        try {
            $addresses = Yii::$app->get($currency)->showAddresses($model->id);
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
        }

        return $this->render('m-account/addresses', ['model' => $model, 'addresses' => $addresses, 'currency' => $currency]);
    }

    /**
     * -+ Список транзакций
     *
     * @param $id integer
     * @param $currency string
     */
    public function actionListtransactions($id, $currency)
    {

//        $listTx = Yii::$app->get($currency)->getTransactionsList(10114);
//
//        echo "<pre>";
//        print_r($listTx);
//        die;

        $model = $this->findModel($id);
        $transactions = [];

        try {
            $transactions = Yii::$app->get($currency)->getTransactionsList($model->id);
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
        }

//        echo "<pre>";
//        print_r($transactions);
//        die;


        return $this->render('m-account/listtransactions', ['model' => $model, 'transactions' => $transactions, 'currency' => $currency]);
    }

    /**
     * -+ Список транзакций по адресу
     *
     * @param $id integer
     * @param $currency string
     */
    public function actionListtransactionsByAddress($id, $currency, $address)
    {
        $model = $this->findModel($id);
        $transactions = [];

        try {
            $transactions = Yii::$app->get($currency)->getTransactionsListByAddress($model->id, $address);
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
        }

        return $this->render('m-account/listtransactions_by_address', ['model' => $model, 'transactions' => $transactions, 'currency' => $currency]);


//        $info = '';
//
//        foreach ($transactions as $transaction) {
//            $info .= print_r($transaction, true);
////            $info .= print_r(Yii::$app->get($currency)->getTransaction($transaction['txid']), true);
//        }
//
//        echo $this->renderContent("<pre>$info</pre>");
    }

//todo возможно убрать

    /**
     * Displays a single MAccount model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('m-account/view', [
            'model' => $model,
        ]);
    }

    /**
     * + Создать мерчант. Creates a new MAccount model.
     *
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MAccount();

        $model->secret = Yii::$app->security->generateRandomString(16);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->upload();

            return $this->redirect(['m-account/index']);
        }

        return $this->render('m-account/create', [
            'model' => $model,
        ]);
    }


    /**
     * Тестрование отправки сообщения
     *
     * @param $id
     * @return mixed
     */
    public function actionTestMsgResponse($id)
    {
        $model = $this->findModel($id);
        $answer = ['request' => '', 'response' => ''];

        if (Yii::$app->request->post()) {

            $answer = $model->sendToCustomerTxInfo();
        }

        return $this->render('m-account/test_send_msg_response', [
            'model' => $model,
            'answer' => $answer,
        ]);
    }


    public function actionTest()
    {

        $file = '/app/modules/merchant/controllers/testPost.txt';
// Открываем файл для получения существующего содержимого
        $current = file_get_contents($file);
// Добавляем нового человека в файл
        $current .= print_r($_REQUEST, true);
// Пишем содержимое обратно в файл
        file_put_contents($file, $current);


        $this->enableCsrfValidation = false;
        return "*ok*";
    }

    /**
     * + Обновить мерчант. Updates an existing MAccount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        // старый ключ
        $secret = $model->secret;

        if ($model->load(Yii::$app->request->post())) {

            // если пустое поле оставляем старый ключ
            $new_secret = $model->secret;
            $model->secret = empty($model->secret) ? $secret : $model->secret;

            // если новый не пустой оповещаем о новом
            if ($model->save() && !empty($new_secret)) {
                Yii::$app->session->setFlash('secret', "Секретный ключ для ({$model->id}) - " . $model->name . ": " . $model->secret);
            }

            $model->image = UploadedFile::getInstance($model, 'image');
            $model->upload();

            return $this->redirect(['m-account/index']);
        }

        $model->secret = '';
        return $this->render('m-account/update', [
            'model' => $model,
        ]);
    }

    /**
     * + Удалить мерчант. Deletes an existing MAccount model.
     * Физически не удаляем просто меняем статус is_delete = 1
     *
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_delete = 1;
        $model->save();

        return $this->redirect(['m-account/index']);
    }

    /**
     * + Создаем MAccount model based on its primary key value.
     *
     *
     * !!!!!!!!!! проверяем владельца модели !!!!!!!!!!!!
     *
     *
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MAccount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MAccount::findOne($id)) !== null) {

            // !!!! проверяем владельца модели мерчанта !!!!
            if (!isset(Yii::$app->user->identity->id) || $model->user_id != Yii::$app->user->identity->id) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
