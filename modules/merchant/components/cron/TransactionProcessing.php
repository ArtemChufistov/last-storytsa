<?php

namespace app\modules\merchant\components\cron;


use app\modules\merchant\components\merchant\coinmarketcap\CoinMarketCap;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use app\modules\merchant\models\MAccount;
use app\modules\merchant\models\MCoinmarketcapListings;
use app\modules\merchant\models\MCoinmarketcapTicker;
use app\modules\merchant\models\MPaymentTasks;
use app\modules\merchant\models\MSendTxid;
use Yii;

class TransactionProcessing
{


    public static function run()
    {
        self::getNewTransactionsAndCompleteThem();
    }

    public static function getNewTransactionsAndCompleteThem()
    {
        echo "\n \e[31m Processind... \e[0m \n";
        // ничего не менять!
        $mSendTxids = MPaymentTasks::find()
            ->where(['status' => MPaymentTasks::TX_STATUS_NEW])
            // прогружаем кроном только один раз при интервале крона 10 сек
            ->andWhere('created_at >= ' . date("'Y-m-d H:i:s'", time() - 19))
            ->orderBy('id ASC')
            ->all();

        echo " \e[32m Транзакций к обработке " . count($mSendTxids) . " \e[0m \n\n";

        /** @var MPaymentTasks $mSendTxid */
        foreach ($mSendTxids as $mSendTxid) {
            // не убирать не вкоем случае
            $mSendTxid->setStatus(MPaymentTasks::TX_STATUS_PROCESING);

            // Обработка транзакции на отправку
            Yii::$app->get($mSendTxid->currency)->sendTransactionFinisher($mSendTxid);
        }
    }

}

?>
