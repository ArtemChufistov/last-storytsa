<?php

namespace app\modules\merchant\components\cron;


use app\modules\merchant\components\merchant\coinmarketcap\CoinMarketCap;
use app\modules\merchant\models\MCoinmarketcapListings;
use app\modules\merchant\models\MCoinmarketcapTicker;
use Yii;

class Refresher
{


    public static function run()
    {

        self::coinmarketcapRefresh();
//        self::coinListRefresh();
//        self::saveCoinLogo();
    }

    /**
     * Обновить биржевые цены криптовалют
     */
    public static function coinmarketcapRefresh()
    {


//        $ticker = CoinMarketCap::getTicker(10, "USD");
        $ticker = CoinMarketCap::getTicker(10000, "USD");
        var_dump(count($ticker));


        Yii::$app->db->createCommand()->truncateTable(MCoinmarketcapTicker::tableName())->execute();

        foreach ($ticker as $oneTrick) {

            $model = new MCoinmarketcapTicker();

            $model->id = $oneTrick['id'];
            $model->name = $oneTrick['name'];
            $model->symbol = $oneTrick['symbol'];
            $model->rank = $oneTrick['rank'];
            $model->price_usd = $oneTrick['price_usd'];
            $model->price_btc = $oneTrick['price_btc'];
            $model->h24h_volume_usd = $oneTrick['24h_volume_usd'];
            $model->market_cap_usd = $oneTrick['market_cap_usd'];
            $model->available_supply = $oneTrick['available_supply'];
            $model->total_supply = $oneTrick['total_supply'];
            $model->max_supply = $oneTrick['max_supply'];
            $model->percent_change_1h = $oneTrick['percent_change_1h'];
            $model->percent_change_24h = $oneTrick['percent_change_24h'];
            $model->percent_change_7d = $oneTrick['percent_change_7d'];
            $model->last_updated = $oneTrick['last_updated'];


            $model->save();


        }


//        [
//            "id"=> "ethereum",
//            "name"=> "Ethereum",
//            "symbol"=> "ETH",
//            "rank"=> "2",
//            "price_usd"=> "610.981",
//            "price_btc"=> "0.0800268",
//            "24h_volume_usd"=> "1764530000.0",
//            "market_cap_usd"=> "61036524551.0",
//            "available_supply"=> "99899219.0",
//            "total_supply"=> "99899219.0",
//            "max_supply"=>  NULL,
//            "percent_change_1h"=> "0.38",
//            "percent_change_24h"=> "3.67",
//            "percent_change_7d"=> "7.04",
//            "last_updated" => "1528270162",
//        ];
    }



    public static function coinListRefresh(){


        $ticker = CoinMarketCap::getListings();

var_dump(count($ticker['data']));




        Yii::$app->db->createCommand()->truncateTable(MCoinmarketcapListings::tableName())->execute();

        foreach ($ticker['data'] as $oneTrick) {

            $model = new MCoinmarketcapListings();

            $model->id = $oneTrick['id'];
            $model->name = $oneTrick['name'];
            $model->symbol = $oneTrick['symbol'];
            $model->website_slug = $oneTrick['website_slug'];
            $model->save();


        }

//        ["id"]=>     int(2920)
//        ["name"]=>    string(6) "0xcert"
//        ["symbol"]=>      string(3) "ZXC"
//        ["website_slug"]=> string(6) "0xcert"

    }


    public static function saveCoinLogo(){


        $coins = MCoinmarketcapListings::find()->all();

        /** @var MCoinmarketcapListings $coin */
        foreach ($coins as $coin){

            if($coin->id > 291 && $coin->id != 101 && $coin->id != 135) {

                echo $coin->id . "\n";
//            128x128  16x16	200x200  32x32	64x64

                $image = file_get_contents('https://s2.coinmarketcap.com/static/img/coins/16x16/' . $coin->id . '.png');
                file_put_contents('/app/web/cmt/icons/coins/16x16/' . $coin->id . '.png', $image);


                $image2 = file_get_contents('https://s2.coinmarketcap.com/static/img/coins/32x32/' . $coin->id . '.png');
                file_put_contents('/app/web/cmt/icons/coins/32x32/' . $coin->id . '.png', $image2);


                $image3 = file_get_contents('https://s2.coinmarketcap.com/static/img/coins/64x64/' . $coin->id . '.png');
                file_put_contents('/app/web/cmt/icons/coins/64x64/' . $coin->id . '.png', $image3);


                $image4 = file_get_contents('https://s2.coinmarketcap.com/static/img/coins/128x128/' . $coin->id . '.png');
                file_put_contents('/app/web/cmt/icons/coins/128x128/' . $coin->id . '.png', $image4);


                try {
                    $image5 = file_get_contents('https://s2.coinmarketcap.com/static/img/coins/200x200/' . $coin->id . '.png');
                }catch (yii\base\ErrorException $e) {
                    echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
                }

              //  var_dump($image5);
                if(isset($image5))
                file_put_contents('/app/web/cmt/icons/coins/200x200/' . $coin->id . '.png', $image5);


                unset($image);
                unset($image2);
                unset($image3);
                unset($image4);
                unset($image5);



            }

        }

        https://s2.coinmarketcap.com/static/img/coins/128x128/1925.png



    }
}

?>
