<?php

namespace app\modules\merchant\components\cron;


use app\modules\merchant\components\merchant\coinmarketcap\CoinMarketCap;
use app\modules\merchant\models\MAccount;
use app\modules\merchant\models\MCoinmarketcapListings;
use app\modules\merchant\models\MCoinmarketcapTicker;
use app\modules\merchant\models\MNodesTransactions;
use app\modules\merchant\models\MSendTxid;
use Yii;

class TxInfoSender
{


    public static function run()
    {
        //  TransactionProcessing::run();

          self::sendTxidToMUrl();
    }


    public static function refreshForSendQueue()
    {

        $confirmations = 1;


        $m_accounts = MAccount::find()->where(['is_turn_on' => 1])->andWhere(" url != '' ")->all();
        var_dump(count($m_accounts));

        $wallets = Yii::$app->modules['api']->wallets;

        foreach ($wallets as $currency) {

            echo "\n\n" . $currency . "\n";
//            echo print_r(Yii::$app->get($currency)->getInfo(), true);
//            print_r(Yii::$app->get($currency)->getClient()->listaccounts());
//            echo "\n";


            /** @var MAccount $m_account_model */
            foreach ($m_accounts as $m_account_model) {
                echo $m_account_model->id . ' - ' . $m_account_model->url . "\n";

                $tx_arr = Yii::$app->get($currency)->getTransactionsList($m_account_model->id);
                $new_txids = 0;
                foreach ($tx_arr as $tx) {
                    if ($tx['confirmations'] >= $confirmations) {

                        $send_txid_model = new MSendTxid();
                        $send_txid_model->txid = $tx['txid'];
                        $send_txid_model->m_id = $m_account_model->id;
                        $send_txid_model->currency = $currency;
                        $send_txid_model->amount = $tx['amount'];
                        $send_txid_model->from_addr = $currency == 'ethereum' ? $tx['from'] : ' - ';
                        $send_txid_model->to_addr = $tx['address'];
                        $send_txid_model->category = $tx['category'];
                        $send_txid_model->status = MSendTxid::TX_STATUS_COMPLETE;
                        $send_txid_model->isShowToUser = MSendTxid::SHOW_TO_USER;

                        $send_txid_model->confirmations = $tx['confirmations'];
                        $send_txid_model->blockchain_time = date("Y-m-d H:i:s", $tx['time']);

                        $send_txid_model->validate();
                        print_r($send_txid_model->getErrors());

                        if ($send_txid_model->save())
                            $new_txids++;

                    }
                }

                echo "Добавлено новых: " . $new_txids . "\n";
            }
        }
    }


    public static function sendTxidToMUrl()
    {

        $waitingForSend = MNodesTransactions::find()
            ->where(['send_msg_status' => 0])
            ->andWhere(['category' => 'receive'])
            ->andWhere('m_account_id IS NOT NULL')
            ->all();

        echo "\n\n \e[34m Рассылка транзакций: " . count($waitingForSend) . " шт. \e[0m\n\n";

        /** @var MNodesTransactions $m_node_transaction_model */
        foreach ($waitingForSend as $m_node_transaction_model) {
            echo "\n \e[32m " . $m_node_transaction_model->m_account_id . ' - ' . $m_node_transaction_model->currency . ' - '
                . $m_node_transaction_model->txid . " на URL " . $m_node_transaction_model->mAccount->url . " \e[0m \n";

//            var_dump($m_send_txid_model->mAccount->sendToCustomerTxInfo($m_send_txid_model));
            $m_node_transaction_model->mAccount->sendToCustomerTxInfo($m_node_transaction_model);

            if($m_node_transaction_model->send_msg_status == 0) {
                echo "\n\e[31m Ошибка отправки \e[0m\n";
            }else{
                echo "\n\e[32m Успех \e[0m\n";
            }

        }
    }

}

?>
