<?php

namespace app\modules\merchant\components\merchant;

use app\modules\merchant\components\cron\TxInfoSender;

interface CommonMerchantNodeInterface
{
     public function nodeInfo();

    /**
     * ++ Создать новый адрес для мерчант аккаунта
     *
     * @param $accountName
     * @return mixed
     */
    public function generateAddress($accountName);

    /**
     * ++ Список адресов мерчант аккаунта
     *
     * @param $accountName
     * @return mixed
     */
    public function showAddresses($accountName);

    /**
     * ++ Баланс адреса
     *
     * @param $address
     * @param int $confirmations_num
     * @return mixed
     */
    public function getAddressBalance($accountName, $address, $confirmations_num = 0);

    /**
     * Баланс сумма по всем адресам аккаунта
     *
     * @param $accountName
     * @param int $confirmations_num
     * @return mixed
     * @throws ErrorException
     */
    public function getAddressesBalanceSum($accountName, $confirmations_num = 0);



    //public function getRecomendedComission($accountName);





    /**
     * Механизм обработки транзакции на отправку
     *
     * @param TxInfoSender $txInfoSender
     * @return mixed
     */
    public function sendTransactionFinisher($txInfoSender);


    public function getTransactionsListByAddress($accountName, $address);

    public function getTransaction($txid);

    public function getTransactionsList($accountName, $count = 1000, $skip = 0);
    //  public function sendFrom($accountName, $address, $amount, $from_address);


}