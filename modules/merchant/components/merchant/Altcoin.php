<?php

namespace app\modules\merchant\components\merchant;

use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use app\modules\merchant\models\MAccount;
use app\modules\merchant\models\MAccountWallet;
use app\modules\merchant\models\MPaymentTasks;
use app\modules\merchant\models\MSendTxid;
use app\modules\profile\components\merchant\clients\EasyBitcoin;
use ErrorException;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class Altcoin extends Component implements CommonMerchantNodeInterface
{
    /**
     * @var EasyBitcoin
     */
    protected $altcoinClient;

    public $currency;

    public $username;

    public $password;

    public $host;

    public $port;

    public function init()
    {
        parent::init();
        $this->altcoinClient = new EasyBitcoin($this->username, $this->password, $this->host, $this->port);
    }

    public function nodeInfo()
    {
        return $this->altcoinClient->getwalletinfo();
    }


    /**
     * +- Создать новый адрес для мерчант аккаунта
     *
     * @param string $user_id
     * @return mixed
     * @throws ErrorException
     */
    public function generateAddress($m_account_id = null, $user_id = null)
    {
        if ($user_id == null && $m_account_id == null)
            throw new ErrorException('getnewaddress error: 501');


        if (isset($m_account_id)) {
            /** @var MAccount $mAccount */
            $mAccount = MAccount::find()->where(['id' => $m_account_id])->one();

            if (isset($mAccount->user_id)) {

                if (isset($user_id) && $user_id != $mAccount->user_id) {
                    throw new ErrorException('getnewaddress data error: 503');
                }

                $user_id = $mAccount->user_id;
                $m_account_id = $mAccount->id;
            } else {
                throw new ErrorException('getnewaddress error: 502');
            }
        }


        if (is_int($user_id)) {

            $address = $this->altcoinClient->getnewaddress((string)$user_id);
            if ($this->altcoinClient->error == "") {

                $m_account_wallet_mod = new MAccountWallet();
                $m_account_wallet_mod->user_id = $user_id;
                $m_account_wallet_mod->m_account_id = $m_account_id;
                $m_account_wallet_mod->currency_type = $this->currency;
                $m_account_wallet_mod->currency_wallet = $address;

                if ($m_account_wallet_mod->save()) {
                    return $m_account_wallet_mod->currency_wallet;
                } else {
                    throw new ErrorException('getnewaddress error: 500');
                }

            } else {
                throw new ErrorException('getnewaddress error: ' . $this->altcoinClient->error);
            }
        } else {
            throw new ErrorException('getnewaddress error: not found owner');
        }
    }

    /**
     * ++ Список адресов мерчант аккаунта
     *
     * @param $account string
     * @return mixed
     * @throws ErrorException
     */
    public function showAddresses($accountName)
    {
        $addresses = $this->altcoinClient->getaddressesbyaccount((string)$accountName);

        if ($this->altcoinClient->error == "") {
            return $addresses;
        } else {
            throw new ErrorException('getaddressesbyaccount error: ' . $this->altcoinClient->error);
        }
    }

    /**
     * +- Баланс адреса
     *
     * @param $address
     * @param int $confirmations_num
     * @return mixed
     */
    public function getAddressBalance($accountName, $address, $confirmations_num = 0)
    {
        $addresses = $this->showAddresses($accountName);
        if (ArrayHelper::isIn($address, $addresses)) {
            /**
             * float $info
             */
            $info = $this->altcoinClient->getreceivedbyaddress((string)$address, (int)$confirmations_num);

            if ($this->altcoinClient->error == "") {
                return sprintf("%.8f", $info);
            } else {
                throw new ErrorException('getreceivedbyaddress error: ' . $this->altcoinClient->error);
            }

        } else {
            throw new ErrorException('Address not found in merchant wallet');
        }
    }

    /**
     * +- Баланс сумма по всем адресам аккаунта
     *
     * @param $user_id
     * @param int $confirmations_num
     * @return mixed
     * @throws ErrorException
     */
    public function getAddressesBalanceSum($user_id, $confirmations_num = 0)
    {
        $info = $this->altcoinClient->getbalance((string)$user_id, (int)$confirmations_num, false);

        if ($this->altcoinClient->error == "") {
            return sprintf("%.8f", $info);
        } else {
            throw new ErrorException('getbalance error: ' . $this->altcoinClient->error);
        }
    }


    /**
     * @param MPaymentTasks $mSendTxid
     * @throws \yii\base\InvalidConfigException
     */
    public function sendTransactionFinisher($mSendTxid)
    {

        $amount = MerchantHelper::abs($mSendTxid->amount);
        // if fee = 0 set avto fee
        // по умолчанию
        $fee_per_kByte = MerchantHelper::abs($mSendTxid->fee_per_kByte);

        if(isset($mSendTxid->fee_per_byte)){
            $fee_per_kByte = 1024 * $mSendTxid->fee_per_byte / 100000000;
            $mSendTxid->fee_per_kByte = $fee_per_kByte;
            $mSendTxid->save();
        }
        if(isset($mSendTxid->fee_per_transaction)){
            $fee_per_kByte = $mSendTxid->fee_per_transaction;
            $mSendTxid->fee_per_kByte = $fee_per_kByte;
            $mSendTxid->save();
        }

        // устанавливаем комиссию на ноде
        $this->altcoinClient->settxfee(sprintf("%.8f", $fee_per_kByte));

        $totalBalance = Yii::$app->get($mSendTxid->currency)->getAddressesBalanceSum($mSendTxid->user_id);

        if ($mSendTxid->feePaySender == 1)
            $fee_per_kByte = -1 * $fee_per_kByte;

        $neededSumm = $amount + $fee_per_kByte;

        echo "\n txID.$mSendTxid->id $mSendTxid->currency Перевод $amount => $mSendTxid->to_addr с комисией за килобайт $fee_per_kByte \n";

        if ($neededSumm <= $totalBalance) {

            $txid = Yii::$app->get($mSendTxid->currency)->sendFrom($mSendTxid->user_id, $mSendTxid->to_addr, $amount);

            echo " $mSendTxid->currency txid $txid \n";


            $mSendTxid->txid = $txid;
            $mSendTxid->status = MPaymentTasks::TX_STATUS_WAS_SEND;
            $mSendTxid->save();

            $txInfo = Yii::$app->get($mSendTxid->currency)->getTransaction($mSendTxid->txid);

            $mSendTxid->blockchain_time = date("Y-m-d H:i:s", $txInfo['time']);
            $mSendTxid->fee = $txInfo['fee'];
            $mSendTxid->confirmations = $txInfo['confirmations'];
            $mSendTxid->status = MPaymentTasks::TX_STATUS_COMPLETE;
            $mSendTxid->save();

        } else {
            // fail
            $mSendTxid->status = MPaymentTasks::TX_STATUS_FAIL;
        }
    }


    /**
     * Подробная информация по транзакции
     *
     * @param $txid
     * @return mixed
     * @throws ErrorException
     */
    public function getTransaction($txid)
    {
        $trans = $this->altcoinClient->gettransaction((string)$txid);

        if ($this->altcoinClient->error == "") {
            return $trans;
        } else {
            throw new ErrorException('gettransaction error: ' . $this->altcoinClient->error);
        }
    }


    /**
     * Функция перевода с учетом аккаунта отправителя
     *
     * @param $accountName
     * @param $toAddress
     * @param $amount - сумма
     * @param int $confirmations_num
     * @param string $comment - для внутреннего использования
     * @param string $commentTo - коммент получателю
     * @return string - txid
     * @throws ErrorException если нет ответа
     */
    public function sendFrom($accountName, $toAddress, $amount, $confirmations_num = 0, $comment = '', $commentTo = '')
    {
        $txid = $this->altcoinClient->sendfrom((string)$accountName, (string)$toAddress, $amount, $confirmations_num, $comment, $commentTo);

        if ($this->altcoinClient->error == "") {
            return $txid;
        } else {
            throw new ErrorException('sendfrom error: ' . $this->altcoinClient->error);
        }
    }


    public function getInfo()
    {
        $info = $this->altcoinClient->getwalletinfo();

        if ($this->altcoinClient->error == "") {
            return $info;
        } else {
            throw new ErrorException('getinfo error: ' . $this->altcoinClient->error);
        }
    }


    /**
     * Список транзакций
     *
     * @param $user_id
     * @param int $skip
     * @param int $count
     * @param bool $watchOnly
     * @return mixed
     * @throws ErrorException
     */
    public function getTransactionsList($user_id, $count = 1000, $skip = 0, $watchOnly = true)
    {
        $trans = $this->altcoinClient->listtransactions((string)$user_id, (int)$count, (int)$skip, (boolean)$watchOnly);

        if ($this->altcoinClient->error == "") {
            return $trans;
        } else {
            throw new ErrorException('listtransactions error: ' . $this->altcoinClient->error);
        }
    }

    public function getTransactionsListByAddress($account, $address)
    {
        $transactions = $this->getTransactionsList($account);

        $clearByAddress = [];
        foreach ($transactions as $transaction) {
            if ($transaction['address'] == $address) {
                $clearByAddress[] = $transaction;
            }
        }

        return $clearByAddress;
    }


    public function getClient()
    {
//    This call was removed in version 0.16.0. Use the appropriate fields from:
//    - getblockchaininfo: blocks, difficulty, chain
//    - getnetworkinfo: version, protocolversion, timeoffset, connections, proxy, relayfee, warnings
//    - getwalletinfo: balance, keypoololdest, keypoolsize, paytxfee, unlocked_until, walletversion
//
//    bitcoin-cli has the option -getinfo to collect and format these in the old format.
        return $this->altcoinClient;
    }




    //    /**
//     * Функция перевода без учета аккаунта
//     *
//     * @param $toAddress
//     * @param $amount
//     * @param string $comment
//     * @param string $commentTo
//     * @param bool $subtractFeeFromAmount
//     * @return string
//     * @throws ErrorException
//     */
    public function send($toAddress, $amount, $comment = '', $commentTo = '', $subtractFeeFromAmount = false)
    {
        $txid = $this->altcoinClient->sendtoaddress($toAddress, $amount, $comment, $commentTo, $subtractFeeFromAmount);

        if ($this->altcoinClient->error == "") {
            return $txid;
        } else {
            throw new ErrorException('sendtoaddress error: ' . $this->altcoinClient->error);
        }
    }
}
