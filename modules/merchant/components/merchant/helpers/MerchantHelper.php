<?php

namespace app\modules\merchant\components\merchant\helpers;

use app\modules\merchant\models\MAccount;
use app\modules\merchant\models\MAccountWallet;
use app\modules\merchant\models\MCoinmarketcapTicker;
use app\modules\merchant\models\MNodesTransactions;
use app\modules\merchant\models\MPaymentTasks;
use app\modules\merchant\models\MSendTxid;
use ErrorException;
use yii\helpers\ArrayHelper;
use Yii;
use app\modules\merchant\models\MCoinmarketcapListings;
use yii\helpers\VarDumper;

class MerchantHelper
{

    /**
     * Симпотичное название коина
     *
     * @param $currency
     * @return string
     */
    public static function prettyCoinName($currency)
    {
        /** @var MCoinmarketcapListings $capListModel */
        $capListModel = MCoinmarketcapListings::find()->where(['website_slug' => $currency])->one();

        return isset($capListModel) ? $capListModel->name : $currency;
    }

    /**
     * Лого коина (BTC, LTC...)
     *
     * @param $currency
     * @return string
     */
    public static function coinLogo($currency)
    {
        /** @var MCoinmarketcapListings $capListModel */
        $capListModel = MCoinmarketcapListings::find()->where(['website_slug' => $currency])->one();

        return isset($capListModel) ? $capListModel->symbol : $currency;
    }

    /**
     * Конвертим коин в USD
     *
     * @param $currency
     * @param $amount
     * @return float
     */
    public static function coinPriceUSD($currency, $amount, $is_add_info = false)
    {
        $mCoinMC = MCoinmarketcapTicker::find()->where(['id' => $currency])->one();

        if ($amount == 0)
            return 0;

        if ($is_add_info) {
            return $amount . ' ~ (' . MerchantHelper::coinPriceUSD($currency, $amount) . ' $) ';
        } else {
            return round($amount * $mCoinMC['price_usd'], 2);
        }
    }

    public static function inOutLable($amount)
    {
        if ($amount > 0) {
            return '<span class="label label-success rounded">&nbsp; ' . Yii::t('app', 'Получено') . ' &nbsp;</span>';
        } else {
            return '<span class="label label-orange rounded" style="background: #e67e22;">' . Yii::t('app', 'Отправлено') . '</span>';
        }
    }

    /**
     * Массив мерчантов авторизированного пользователя (для dropdownlist)
     *
     * @return array
     */
    public static function userMerchantsArray()
    {
        $merchantsArray = [];

        if (isset(Yii::$app->user)) {
            $m_models = MAccount::find()
                ->where(['user_id' => Yii::$app->user->identity->id])
                ->andWhere(['is_delete' => 0])
                ->orderBy('id ASC')
                ->all();

//            $merchantsArray = ArrayHelper::map($m_models, 'id', 'id');

            /** @var MAccount $merch */
            foreach ($m_models as $merch) {
                $merchantsArray[$merch->id] = $merch->id . " - " . $merch->name;
            }

        }

        return $merchantsArray;
    }

    /**
     * Список активных кошельков (для dropdownlist с красивыми названиями)
     *
     * @return array
     */
    public static function avalableWalletsFromMerchantModuleArray()
    {
        $currencyArray = [];

        foreach (Yii::$app->modules['merchant']->wallets as $currency) {
            /** @var \app\modules\merchant\models\MCoinmarketcapListings $coinInfoModel */
            $coinInfoModel = MCoinmarketcapListings::coinInfo($currency);
            $currencyArray[$currency] = $coinInfoModel->name;
        }

        return $currencyArray;
    }

    public static function coinLogoUrl($currency, $size = 32)
    {
        /** @var \app\modules\merchant\models\MCoinmarketcapListings $coinInfoModel */
        $coinInfoModel = MCoinmarketcapListings::coinInfo($currency);

        switch ($currency) {
            case 16:
                $url = "/cmt/icons/coins/16x16/$coinInfoModel->id.png";
                break;
            case 32:
                $url = "/cmt/icons/coins/32x32/$coinInfoModel->id.png";
                break;
            case 64:
                $url = "/cmt/icons/coins/64x64/$coinInfoModel->id.png";
                break;
            case 128:
                $url = "/cmt/icons/coins/128x128/$coinInfoModel->id.png";
                break;
            case 200:
                $url = "/cmt/icons/coins/200x200/$coinInfoModel->id.png";
                break;
            default:
                $url = "/cmt/icons/coins/32x32/$coinInfoModel->id.png";
        }

        return $url;
    }


    /**
     * Количество транзакций пользователя
     *
     * @return int
     */
    public static function totalUserTransaction($user_id)
    {
        $count = MNodesTransactions::find()->where(['user_id' => $user_id])->count();

        return $count;
    }

    /**
     * Транзакций в обработке 0 подтверждений
     *
     * @return int
     */
    public static function totalUserTransactionWithZeroConformation($user_id)
    {
        return MPaymentTasks::find()->where(['user_id' => $user_id])->andWhere('status != ' . MPaymentTasks::TX_STATUS_COMPLETE)->count();
    }

    /**
     * Транзакций на сумму (сумма движения по счету в USD по модулю)
     *
     * @return int
     */
    public static function totalUserTransactionWithUSDSumm($user_id)
    {
        $models = MNodesTransactions::find()->where(['user_id' => $user_id])->all();

        $totalCoinSummArr = [];
        /** @var MSendTxid $model */
        foreach ($models as $model) {
            if (!isset($totalCoinSummArr[$model->currency]))
                $totalCoinSummArr[$model->currency] = 0;

            $totalCoinSummArr[$model->currency] = $totalCoinSummArr[$model->currency] + abs($model->amount);
        }

        $total_usd_summ = 0;
        foreach ($totalCoinSummArr as $currency => $summ) {

            /** @var MCoinmarketcapTicker $cmarket */
            $cmarket = MCoinmarketcapTicker::find()->where(['id' => $currency])->one();

            if (isset($cmarket->price_usd) && $cmarket->price_usd > 0)
                $total_usd_summ = $total_usd_summ + $cmarket->price_usd * $summ;
        }

        return round($total_usd_summ, 2);
    }


    /**
     * Общий баланс(сложить все транзакции в usd)
     *
     * @return int
     */
    public static function totalUserBalanceByTransactionInUSD($merchantsArray = [])
    {
        $walletArr = self::avalableWalletsFromMerchantModuleArray();

        $total = 0;
        foreach ($walletArr as $currency => $currencyName) {
            $currencyBalance = self::getAccountBalance($currency);
            $total = $total + MerchantHelper::coinPriceUSD($currency, $currencyBalance);
        }

        return $total;
    }

    public static function genEthPass($accountName)
    {

        $passphrase = 'oi+t3@rXWUdtDPKl5Mb4hgfjhfghfd9v1hXqbTvQWDeNnzA';
        if (!empty($accountName)) {
            $eth_pass = md5($accountName . $passphrase);
        } else {
            $eth_pass = '';
        }

        return $eth_pass;
    }


    public static function walletAdresses($accountName, $currency = 'ethereum')
    {
        $addresses = [];
        try {
            $addresses = Yii::$app->get($currency)->showAddresses($accountName);
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
        }

        $dropdown_addresses = [];
        foreach ($addresses as $address) {
            $dropdown_addresses[$address] = $address . ' - ' . Yii::$app->get($currency)->getAddressBalance($accountName, $address, 0);
        }

        return $dropdown_addresses;
    }

    /**
     * Баланс по крипте
     *
     * @param $currency
     * @param int $confirmations_num
     */
    public static function getAccountBalance($currency, $confirmations_num = 0)
    {
        return Yii::$app->get($currency)->getAddressesBalanceSum(Yii::$app->user->identity->id, $confirmations_num);
    }


    public static function addressBlockchainLink($currency, $address)
    {
        switch ($currency) {
            case 'bitcoin':
                $url = "https://blockchain.info/address/" . $address;
                break;
            case 'litecoin':
                $url = "https://chainz.cryptoid.info/ltc/address.dws?" . $address;
                break;
            case 'bitcoin-cash':
                $url = "https://explorer.bitcoin.com/bch/address/" . $address;
                break;
            case 'dogecoin':
                $url = "https://dogechain.info/address/" . $address;
                break;
            case 'ethereum':
                $url = "https://etherscan.io/address/" . $address;
                break;
            case 'zcash':
                $url = "https://zchain.online/address/" . $address;
                break;
            default:
                $url = "#";
        }

        return "<a href='$url' target='_blank'>$address</a>";
    }


    public static function transactionBlockchainLink($currency, $txid, $make_tx_small = true)
    {
        if (isset($txid['txid']))
            $txid = $txid['txid'];


        switch ($currency) {
            case 'bitcoin':
                $url = "https://blockchain.com/btc/tx/" . $txid;
                break;
            case 'litecoin':
                $url = "https://chainz.cryptoid.info/ltc/tx.dws?" . $txid;
                break;
            case 'bitcoin-cash':
                $url = "https://explorer.bitcoin.com/bch/tx/" . $txid;
                break;
            case 'dogecoin':
                $url = "https://dogechain.info/tx/" . $txid;
                break;
            case 'ethereum':
                $url = "https://etherscan.io/tx/" . $txid;
                break;
            case 'zcash':
                $url = "https://zchain.online/tx/" . $txid;
                break;
            default:
                $url = "#";
        }

        $link_text = $txid;

        if ($make_tx_small)
            $link_text = substr($txid, 0, 20) . '...';

        return "<a href='$url' target='_blank' title='$txid'>$link_text</a>";
    }


    public static function nodeFee($currency, $estimate_block_num = 25)
    {
        $client = Yii::$app->get($currency)->getClient();

        switch ($currency) {
            case 'bitcoin':
                $fee = sprintf("%.8f", $client->estimatesmartfee($estimate_block_num)['feerate']);
                break;
            case 'litecoin':
                $fee = sprintf("%.8f", $client->estimatesmartfee($estimate_block_num)['feerate']);
                break;
            case 'bitcoin-cash':
                $fee = sprintf("%.8f", $client->estimatefee($estimate_block_num));
                break;
            case 'dogecoin':
                $fee = 1;
                break;
            case 'ethereum':
                $fee = 0;
                break;
            case 'zcash':
                $fee = 0;
                break;
            default:
                $fee = 0;
        }

        return $fee;
    }


    public static function createNewSendTransaction($user_id = null, $accountName = null, $currency, $addressTo, $amount, $id_parent = 0,
                                                    $request_type = MPaymentTasks::REQUEST_TYPE_API,
                                                    $isShowToUser = MPaymentTasks::SHOW_TO_USER,
                                                    $feePaySender = true, $fee_per_transaction = null, $fee_per_byte = null)
    {
        $totalBalance = Yii::$app->get($currency)->getAddressesBalanceSum($user_id);


        // Если комиссия не задана ставим по умолчанию
        $fee_per_kByte =  MerchantHelper::nodeFee($currency);
        $avalableBalance = $totalBalance - $fee_per_kByte;


        if ($avalableBalance >= $amount) {

            if ($amount > 0) {
                $amount = -1 * $amount;
            }

            $node_tx_model = new MNodesTransactions();
            $node_tx_model->is_processing_complete = MNodesTransactions::IS_IN_PROCESSING;
            $node_tx_model->user_id = $user_id;
            $node_tx_model->currency = $currency;
            $node_tx_model->category = MPaymentTasks::CATEGORY_SEND;
            $node_tx_model->to_addr = $addressTo;
            $node_tx_model->confirmations = 0;
            $node_tx_model->txid = 'processing...';
            $node_tx_model->amount = $amount;
            $node_tx_model->m_account_id = $accountName;


            if ($node_tx_model->save()) {

                // Создаем пустую транзакцию для последующей обработки
                $transactionModel = new MPaymentTasks();
                $transactionModel->m_node_transaction_id = $node_tx_model->id;
                $transactionModel->user_id = $user_id;
                $transactionModel->m_id = $accountName;
                $transactionModel->id_parent = $id_parent;
                $transactionModel->to_addr = $addressTo;
                $transactionModel->amount = $amount;
                $transactionModel->currency = $currency;
                $transactionModel->status = MPaymentTasks::TX_STATUS_CREATE_NEW_TX_FAIL;
                $transactionModel->category = MPaymentTasks::CATEGORY_SEND;
                $transactionModel->isShowToUser = $isShowToUser;
                $transactionModel->request_type = $request_type;
                $transactionModel->feePaySender = $feePaySender ? 1 : 0;
                //$transactionModel->fee = $fee; // todo вычислить комиссию перед отправкой потом
                $transactionModel->fee_per_kByte = $fee_per_kByte;
                $transactionModel->fee_per_transaction = $fee_per_transaction;
                $transactionModel->fee_per_byte = $fee_per_byte;

                if ($transactionModel->save() && isset($transactionModel->id)) {

                    $node_tx_model->m_payment_task_id = $transactionModel->id;
                    $node_tx_model->txid = $transactionModel->id . ' processing...';
                    if($node_tx_model->save()) {

                        $transactionModel->status = MPaymentTasks::TX_STATUS_NEW;
                        if($transactionModel->save()) {
                            return $node_tx_model->id;
                        }else{
                            throw new ErrorException('sendFromMerchant error: create tx #1120');
                        }

                    } else {

                        MNodesTransactions::deleteAll('is_processing_complete = ' . MNodesTransactions::IS_IN_PROCESSING . ' AND m_payment_task_id IS NULL ');
                        $transactionModel->delete();

                        throw new ErrorException('sendFromMerchant error: create tx #1122');
                    }

                } else {

                    MNodesTransactions::deleteAll('is_processing_complete = ' . MNodesTransactions::IS_IN_PROCESSING . ' AND m_payment_task_id IS NULL ');

                    throw new ErrorException('sendFromMerchant error: Вы уже дали задание отправить такую транзакцию - дождитесь завершения перед повторной отправкой');
                }

            } else {

                MNodesTransactions::deleteAll(' is_processing_complete = ' . MNodesTransactions::IS_IN_PROCESSING . ' AND m_payment_task_id IS NULL ');

                throw new ErrorException('sendFromMerchant error: create tx #1124');
            }

        } else {

            $avalableBalance = $avalableBalance < 0 ? 0 : $avalableBalance;

            if ($currency == 'ethereum') {
                throw new ErrorException('sendFromMerchant error: Not enough money avalable ' . sprintf("%.18f", $avalableBalance));
            } else {
                throw new ErrorException('sendFromMerchant error: Not enough money avalable ' . sprintf("%.8f", $avalableBalance));
            }
        }
    }

    /**
     * Модуль
     *
     * @param $num
     * @return float|int
     */
    public static function abs($num)
    {
        if ($num < 0)
            $num = -1 * $num;

        return $num;
    }


}