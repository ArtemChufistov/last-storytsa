<?php

namespace app\modules\merchant\components\merchant;

use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use app\modules\merchant\models\MAccount;
use app\modules\merchant\models\MAccountWallet;
use app\modules\merchant\models\MSendTxid;
use app\modules\profile\components\merchant\clients\RpcClient;
use valentinek\tests\unit\models\Related;
use yii\base\Component;
use yii\httpclient\Client;
use yii\helpers\ArrayHelper;
use etherscan\api\Etherscan;
use yii\helpers\VarDumper;
use ErrorException;
use Yii;


class Ethereum extends Component implements CommonMerchantNodeInterface
{
    const ETHERSCAN_API_KEY = 'YUUVP27GI4RHE7CMUWKGQK1MAABA285GTM';

    const ETHER_DIGITS = 1000000000000000000;

    /**
     * @var RpcClient
     */
    private $ethereumClient;

    public $currency;

    public $host;

    public $port;

    public $gas = 21000;

    public $gasPrice = 22000000000;


    public function init()
    {
        parent::init();
        $this->ethereumClient = new RpcClient(new Client(['baseUrl' => 'http://' . $this->host . ':' . $this->port]));
    }

    public function nodeInfo()
    {

        $data = $this->ethereumClient->call('eth_syncing', []);

        print_r($data);


        $info['balance'] = 0;
        $info['accounts'] = [];
        $data = $this->ethereumClient->call('eth_accounts', []);
        $addresses = $data['result'];

        foreach ($addresses as $address) {
            $data = $this->ethereumClient->call('eth_getBalance', [$address, 'latest']);
            $info['accounts'][$address] = hexdec($data['result']);
            $info['balance'] += $info['accounts'][$address];
        }

        // unset($info['accounts']);

//        return $info;


        foreach ($info['accounts'] as $address => $balance) {

//            $wallet = MAccountWallet::find()->where(['currency_wallet' => $address])->one();

//            if ($balance > 0) {
                echo $address . ' - ' . $balance . "\n";

                // $this->sendFrom($wallet->m_account_id, $address, $address_to, $balance, false);
//            }
        }
    }

    /**
     * ++ Создать новый адрес
     *
     * @param string $accountName
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function generateAddress($accountName)
    {
        /** @var MAccount $mAccount */
        $mAccount = MAccount::find()->where(['id' => $accountName])->one();

        if (isset($mAccount->user_id)) {
            $user_id = $mAccount->user_id;
        } else {
            $user_id = Yii::$app->user->identity->id;
            $accountName = 10104;
        }

        if (is_int($user_id)) {

            $password = MerchantHelper::genEthPass($user_id);

            $data = $this->ethereumClient->call('personal_newAccount', [(string)$password]);
            if (isset($data['error'])) {
                throw new ErrorException('personal_newAccount error raised: ' . $data['error']['message']);
            }

            if (isset($data['result'])) {
                $m_account_wallet_mod = new MAccountWallet();
                $m_account_wallet_mod->user_id = $user_id;
                $m_account_wallet_mod->m_account_id = $accountName;
                $m_account_wallet_mod->currency_type = 'ethereum';
                $m_account_wallet_mod->currency_wallet = $data['result'];

                if ($m_account_wallet_mod->save())
                    return $m_account_wallet_mod->currency_wallet;
            }

            throw new ErrorException('personal_newAccount error raised: ' . json_encode($m_account_wallet_mod->getErrors()));

        } else {
            throw new ErrorException('getnewaddress error: not found owner');
        }
    }

    /**
     * ++ Список адресов аккаунта
     *
     * @param $accountName
     * @return array
     */
    public function showAddresses($accountName)
    {
        $models = MAccount::find()->where(['user_id' => $accountName])->andWhere(['is_delete' => 0])->orderBy('id ASC')->all();
        $merchantsArray = ArrayHelper::map($models, 'id', 'id');

        $addresses = ArrayHelper::map(MAccountWallet::find()->where(['in', 'm_account_id', $merchantsArray])->andWhere(['currency_type' => 'ethereum'])
            ->orderBy('created_at DESC')->asArray()->all(), 'currency_wallet', 'currency_wallet');

        return array_values($addresses);
    }

    /**
     * +- Баланс адреса
     *
     * @param $address
     * @param int $confirmations_num
     * @return mixed
     */
    public function getAddressBalance($accountName, $address, $confirmations_num = 0, $int = false)
    {
        $addresses = $this->showAddresses($accountName);
        if (ArrayHelper::isIn($address, $addresses)) {

            $data = $this->ethereumClient->call('eth_getBalance', [(string)$address, 'latest']);
            if (isset($data['error'])) {
                throw new ErrorException('eth_getBalance error raised: ' . $data['error']['message']);
            }

            $info['accounts'][$address] = hexdec($data['result']) / self::ETHER_DIGITS;
            //$info['accounts'][$address] = floatval(hexdec($data['result']));
            // $info['balance'] += $info['accounts'][$address];

            if ($int) {
                return hexdec($data['result']);
            } else {
                return sprintf("%.18f", $info['accounts'][$address]);
            }
        } else {
            throw new ErrorException('Address not found in merchant wallet');
        }
    }

    /**
     * +- Баланс сумма по всем адресам аккаунта
     *
     * @param $accountName
     * @param int $confirmations_num
     * @return mixed
     * @throws ErrorException
     */
    public function getAddressesBalanceSum($accountName, $confirmations_num = 0)
    {
        $addreses = $this->showAddresses($accountName);

        $balance = 0;
        foreach ($addreses as $address) {
            $balance = $balance + $this->getAddressBalance($accountName, $address, $confirmations_num);
        }

        return $balance;
    }


    public function estimateTxFee(/*$address, $amount*/)
    {
        //$value = $amount * self::ETHER_DIGITS;
        $tranFee = $this->gasPrice * $this->gas;

        $response = [
            'recommended_fee' => $tranFee,
            'estimated_fee_auto',
            'details' => []
        ];

        return $response;
    }

    /**
     * Функция перевода с учетом аккаунта отправителя
     *
     * @param $accountName
     * @param $fromAddress
     * @param $toAddress
     * @param int $amount
     * @param bool $feePaySender
     * @param bool $fee
     * @param string $comment - для внутреннего использования
     * @param string $commentTo - коммент получателю
     * @return mixed
     * @throws ErrorException
     */
    public function sendFrom($accountName, $fromAddress, $toAddress, $amount, $feePaySender = true, $fee = false, $comment = '', $commentTo = '')
    {
        if ($fromAddress == $toAddress)
            throw new ErrorException('sendFrom: from_address = to_address');

//        echo "\n";
//        echo "\n";
//        echo $amount = $this->getAddressBalance($accountName, $fromAddress, 0, true);
//        echo "\n";
//        echo "\n";


        // количество обязательно INT !!
        $value = $amount;
        $tranFee = $this->gasPrice * $this->gas;


        if (true) {
            $params = [
                'from' => $fromAddress,
                'to' => $toAddress,
                'value' => '0x' . dechex($value),
            ];

            $response_gas = $this->ethereumClient->call('eth_estimateGas', [$params]);
            if (isset($response_gas['error'])) {
                throw new ErrorException('eth_estimateGas error raised: ' . $response_gas['error']['message']);
            }
            $response_gasPrice = $this->ethereumClient->call('eth_gasPrice', []);
            if (isset($response_gasPrice['error'])) {
                throw new ErrorException('eth_gasPrice error raised: ' . $response_gasPrice['error']['message']);
            }

            $gas = hexdec($response_gas['result']);
            $gasPrice = hexdec($response_gasPrice['result']);

            $newTranFee = $gas * $gasPrice;
            if ($newTranFee < $tranFee) {
                // echo "Устанавливаем меньшую цену за транзакцию";
                $this->gas = $gas;
                $this->gasPrice = $gasPrice;
                $tranFee = $newTranFee;
            }
        }


        if (!$feePaySender) {
            $value -= $tranFee;
        }


        echo "\n";
        print_r($fromAddress);
        echo "\n";
        print_r($toAddress);
        echo "\n";
        echo "\n";
        print_r($tranFee);
        echo "\n";
        print_r($this->gas);
        echo "\n";
        print_r($this->gasPrice);
        echo "\n";
        $value = sprintf("%.18f", $value); // !!!!!!!!!!!!!!!!!!!!! хотя валуе и целое

        print_r($value);
        echo "\n";
        echo "\n";

        echo "\n";
        echo "\n";
        print_r((int)($tranFee + $value));
        echo "\n";
        echo "\n";


//         var_dump($this->getAddressBalance($accountName, $fromAddress));
//
//         if($this->getAddressBalance($accountName, $fromAddress) == ($value + $tranFee)/self::ETHER_DIGITS){
//             echo "равны";
//         }

        //  die;
        $params = [
            'from' => $fromAddress,
            'to' => $toAddress,
            'gas' => (string)'0x' . dechex($this->gas),
            'gasPrice' => (string)'0x' . dechex($this->gasPrice),
            'value' => (string)'0x' . dechex($value),
        ];

        /*
                print_r($params);
                die;*/

        $passphrase = MerchantHelper::genEthPass(7);
        $data = $this->ethereumClient->call('personal_unlockAccount', [$fromAddress, $passphrase, 0]);
        if (isset($data['error'])) {
            throw new ErrorException('personal_unlockAccount error raised: ' . $data['error']['message']);
        }

        $response = $this->ethereumClient->call('eth_sendTransaction', [$params]);
        if (isset($response['error'])) {
            throw new ErrorException('eth_sendTransaction error raised: ' . $response['error']['message']);
        }

        $txid = isset($response['result']) ? $response['result'] : false;

        return $txid;
    }


    /**
     * @param MSendTxid $transactionModel
     * @return bool
     * @throws ErrorException
     */
    public function sendTransactionFinisher($transactionModel)
    {
        $amount = MerchantHelper::abs($transactionModel->amount);
        // if fee = 0 set avto fee
        $fee = MerchantHelper::abs($transactionModel->fee);

        $amount = $amount * self::ETHER_DIGITS;
        $tranFee = $this->gasPrice * $this->gas;


        $addressesAndBalancesArr = $this->showAddressesWithPlusBalance($transactionModel->m_id, true);
        unset($addressesAndBalancesArr[$transactionModel->to_addr]);

        $totalSumm = array_sum($addressesAndBalancesArr);
        $totalAdresses = count($addressesAndBalancesArr);
        // Доступный баланс с учетом количества транзакций
        $avalableBalance = $totalSumm - $totalAdresses * $tranFee;

        // Если баланса со всех кошельков хватит для транзакции
        if ($avalableBalance >= $amount) {

            if ($transactionModel->status < MSendTxid::TX_STATUS_WATING_FOR_SEND) {


                // Список кошельков для перевода
                $addresesForMakeTransactions = [];

                // кошельки с минимальным балансом
                $low_balances_arr = array_keys($addressesAndBalancesArr, min($addressesAndBalancesArr));
                // транзитный кошелек
                $transit_low_balance_address = array_shift($low_balances_arr);
                $transit_address_low_balance_amount = $addressesAndBalancesArr[$transit_low_balance_address];
                $addresesForMakeTransactions[$transit_low_balance_address] = $transit_address_low_balance_amount;
                // удаляем адрес с балансом из основного массива
                unset($addressesAndBalancesArr[$transit_low_balance_address]);
                //+++


                $balance_amount_needed = $amount + $tranFee;
                // Проверяем хватает ли нам баланса на самом маленьком кошельке
                if ($transit_address_low_balance_amount < $balance_amount_needed) {

                    $transactionModel->setStatus(MSendTxid::TX_STATUS_FOR_TRANSIT_ONE_WALLET);

                    // Если баланса не хватает ищем кошельки на которых есть недостоющий остаток
                    $nextBalanceNeed = $balance_amount_needed - $transit_address_low_balance_amount + $tranFee;
                    $filtered_balance = array_filter($addressesAndBalancesArr, function ($v) use ($nextBalanceNeed) {
                        return $v >= $nextBalanceNeed;
                    }, ARRAY_FILTER_USE_BOTH);

                    // если недостающий остаток найден добавляем его в массив Список кошельков для перевода
                    if (count($filtered_balance) > 0) {

                        $transactionModel->setStatus(MSendTxid::TX_STATUS_FOR_TRANSIT_TWO_WALLET);

                        $second_low_balances_arr = array_keys($filtered_balance, min($filtered_balance));
                        $second_low_balance_address = array_shift($second_low_balances_arr);
                        $second_address_low_balance_amount = $filtered_balance[$second_low_balance_address];
                        $addresesForMakeTransactions[$second_low_balance_address] = $second_address_low_balance_amount;

                        // если нет кошельков с нужным балансом
                    } else {

                        $transactionModel->setStatus(MSendTxid::TX_STATUS_FOR_TRANSIT_SEVERAL_WALLET);

                        // Перебираем от большего к меньшему
                        arsort($addressesAndBalancesArr);
                        foreach ($addressesAndBalancesArr as $addAddress => $addAmount) {
                            $addresesForMakeTransactions[$addAddress] = $addAmount;
                            $nextBalanceNeed = $nextBalanceNeed - $addAmount + $tranFee;

                            // Заканчиваем добавление кошельков если нужный баланс набран
                            if ($nextBalanceNeed <= 0) {
                                break;
                            }
                        }
                    }
                }

                $transit_address = $this->sendToTransit($transactionModel, $addresesForMakeTransactions);
            }


            $transactionModel->refresh();
            echo "\n\n# " . $transactionModel->id . "\n\n";
            $txid = $this->sendFrom($transactionModel->m_id, $transactionModel->from_addr, $transactionModel->to_addr, $amount);

            if ($txid) {

                $transactionModel->txid = $txid;
                $transactionModel->status = MSendTxid::TX_STATUS_COMPLETE;
                $transactionModel->save();

            } else {

                $transactionModel->setStatus(MSendTxid::TX_STATUS_WATING_FOR_SEND_AFTER_SEND);

                throw new ErrorException('sendFromMerchant error: Processing err 1');
            }

            return true;

        } else {

            $transactionModel->setStatus(MSendTxid::TX_STATUS_FAIL);

            throw new ErrorException('sendFromMerchant error: Not enough money');
        }

    }


    /**
     * @param MSendTxid $MSendTxidModel
     * @param $addresesWithBalances
     * @return mixed
     */
    public function sendToTransit($MSendTxidModel, $addresesWithBalances)
    {
        // кошельки с минимальным балансом
        $low_balances_arr = array_keys($addresesWithBalances, min($addresesWithBalances));
        // выбираем транзитный кошелек
        $transit_low_balance_address = array_shift($low_balances_arr);
        unset($addresesWithBalances[$transit_low_balance_address]);
        // Устанавливаем адрес отправителя в базовую транзакцию
        $MSendTxidModel->from_addr = $transit_low_balance_address;
        $MSendTxidModel->save();


        // отправляем средства на транзитный кошелек
        foreach ($addresesWithBalances as $fromAddress => $amount) {

            $txid = $this->sendFrom($MSendTxidModel->m_id, $fromAddress, $transit_low_balance_address, $amount, false);

            if ($txid) {

                $transactionModel = new MSendTxid();
                $transactionModel->id_parent = $MSendTxidModel->id;
                $transactionModel->transit_addr = $transit_low_balance_address;
                $transactionModel->m_id = $MSendTxidModel->m_id;
                $transactionModel->txid = $txid;
                $transactionModel->from_addr = $fromAddress;
                $transactionModel->to_addr = $transit_low_balance_address;
                $transactionModel->amount = -1 * $amount / self::ETHER_DIGITS;
                $transactionModel->currency = 'ethereum';
                $transactionModel->status = MSendTxid::TX_STATUS_COMPLETE;
                $transactionModel->category = MSendTxid::CATEGORY_SEND;
                $transactionModel->isShowToUser = MSendTxid::HIDE_FROM_USER;

                $transactionModel->save();


            } else {
                throw new ErrorException('sendFromMerchant error: Processing err 2');
            }
        }


        $MSendTxidModel->setStatus(MSendTxid::TX_STATUS_WATING_FOR_SEND);

        return $transit_low_balance_address;
    }


    public function getInfo()
    {
        $info['balance'] = 0;
        $info['accounts'] = [];
        $data = $this->ethereumClient->call('eth_accounts', []);
        $addresses = $data['result'];

        foreach ($addresses as $address) {
            $data = $this->ethereumClient->call('eth_getBalance', [$address, 'latest']);
            $info['accounts'][$address] = floatval(hexdec($data['result'])) / self::ETHER_DIGITS;
            $info['balance'] += $info['accounts'][$address];
        }

        return $info;
    }


    public function getTransactionsList($accountName, $count = 1000, $skip = 0)
    {
        $addresesArray = $this->showAddresses($accountName);

        $transactions = [];
        foreach ($addresesArray as $address) {

            $txArr = $this->getTransactionsListByAddress($accountName, $address);

            foreach ($txArr as $txx) {
                $transactions[] = $this->makeEtherScanTransactionPretty($txx, $address);
            }
        }

        return $transactions;


//        $trans = $this->altcoinClient->listtransactions((string)$account, (int)$count, (int)$skip, (boolean)$watchOnly);
//
//        if ($this->altcoinClient->error == "") {
//            return $trans;
//        } else {
//            throw new ErrorException('listtransactions error: ' . $this->altcoinClient->error);
//        }
    }

    public function getTransactionsListByAddress($account, $address)
    {
        $etherscan = new Etherscan(self::ETHERSCAN_API_KEY);
        $data = $etherscan->transactionList($address);

        $transactions = [];
        foreach ($data['result'] as $transaction) {
            $transactions[] = $this->makeEtherScanTransactionPretty($transaction, $address);
        }

        return $transactions;
    }

    public function getTransaction($txid)
    {
        $trans = $this->altcoinClient->gettransaction((string)$txid);

        if ($this->altcoinClient->error == "") {
            return $trans;
        } else {
            throw new ErrorException('gettransaction error: ' . $this->altcoinClient->error);
        }
    }

    /**
     * Транзакцию полученую с эфирскана делаем похожей на транзакцию битка
     *
     * @param $transaction
     * @param $address
     * @return mixed
     */
    public function makeEtherScanTransactionPretty($transaction, $address)
    {
        $transaction['fee'] = 0;

        if ($address == $transaction['to']) {
            $transaction['category'] = 'receive';
        } elseif ($address == $transaction['from']) {
            $transaction['category'] = 'send';
            $transaction['value'] = -1 * abs($transaction['value']);

            $tranFee = -1 * abs($transaction['gasPrice'] * $transaction['gas']) * pow(10, -18);
            $transaction['fee'] = $tranFee;
        }

        $transaction['address'] = $transaction['to'];
        $transaction['amount'] = $transaction['value'] * pow(10, -18);
        $transaction['txid'] = $transaction['hash'];
        $transaction['time'] = $transaction['timeStamp'];

        return $transaction;
    }

    public function walletsInfo()
    {
        // TODO: Implement walletsInfo() method.
    }

    /**
     * возвращает кошельки только с положительным балансом
     *
     * @param $account
     * @return array
     * @throws ErrorException
     */
    public function showAddressesWithPlusBalance($account, $only_int = false)
    {
        // получаем адреса с ненулевыми балансами
        $addressesArr = $this->showAddresses($account);

        $addressesAndBalancesArr = [];
        foreach ($addressesArr as $address) {
            if ($only_int) {
                $balance = $this->getAddressBalance($account, $address, 0, true);
            } else {
                $balance = $this->getAddressBalance($account, $address);
            }

            if ($balance > 0) {
                $addressesAndBalancesArr[$address] = $balance;
            }
        }

        return $addressesAndBalancesArr;
    }


    public function setOptimalFee($fromAddress, $toAddress, $value)
    {
        // todo
        $params = [
            'from' => $fromAddress,
            'to' => $toAddress,
            'value' => '0x' . dechex($value),
        ];
        $this->gas = $this->ethereumClient->call('eth_estimateGas', [$params])['result'];
        $this->gasPrice = $this->ethereumClient->call('eth_gasPrice', [])['result'];
    }

    public function getClient()
    {
        return $this->ethereumClient;
    }
}
