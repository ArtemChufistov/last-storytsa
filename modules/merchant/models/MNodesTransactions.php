<?php

namespace app\modules\merchant\models;

use Yii;
use yii\db\Expression;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "m_nodes_transactions".
 *
 * @property int $id
 * @property string $txid
 * @property int $confirmations
 * @property int $m_account_id
 * @property int $user_id
 * @property int $m_payment_task_id
 * @property int $is_processing_complete
 * @property string $currency
 * @property string $category
 * @property int $send_msg_status
 * @property double $amount
 * @property double $fee
 * @property string $from_addr
 * @property string $to_addr
 * @property string $created_at
 * @property string $blockchain_time
 * @property MAccount $mAccount
 */
class MNodesTransactions extends \yii\db\ActiveRecord
{
    const CATEGORY_SEND = 'send';
    const CATEGORY_RECEIVE = 'receive';

    const IS_IN_PROCESSING = 0;
    const IS_COMPLETE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_nodes_transactions';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['txid', 'confirmations', 'currency', 'category', 'amount'], 'required'],
            [['confirmations', 'send_msg_status'], 'integer'],
            [['amount', 'fee'], 'number'],
            [['created_at', 'blockchain_time', 'm_account_id', 'user_id', 'm_payment_task_id', 'is_processing_complete'], 'safe'],
            [['txid'], 'string', 'max' => 200],
            [['currency', 'from_addr', 'to_addr'], 'string', 'max' => 100],
            [['category'], 'string', 'max' => 20],
//            [['user_id', 'currency', 'category', 'from_addr', 'to_addr', 'txid'], 'unique', 'targetAttribute' => ['user_id', 'currency', 'category', 'from_addr', 'to_addr', 'txid']],
            [['user_id', 'currency', 'category', 'to_addr', 'txid'], 'unique', 'targetAttribute' => ['user_id', 'currency', 'category', 'to_addr', 'txid']],
        ];
    }

    public static function getCategoryArray()
    {
        return [
            self::CATEGORY_SEND => Yii::t('app', 'Отправлено'),
            self::CATEGORY_RECEIVE => Yii::t('app', 'Получено')
        ];
    }

    public function getCategoryTitle()
    {
        $data = seld::getCategoryArray();

        return $data[$this->category];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'txid' => Yii::t('app', 'Txid'),
            'confirmations' => Yii::t('app', 'Confirmations'),
            'currency' => Yii::t('app', 'Currency'),
            'category' => Yii::t('app', 'Category'),
            'send_msg_status' => Yii::t('app', 'Send Msg Status'),
            'amount' => Yii::t('app', 'Amount'),
            'fee' => Yii::t('app', 'Fee'),
            'from_addr' => Yii::t('app', 'From Addr'),
            'to_addr' => Yii::t('app', 'To Addr'),
            'created_at' => Yii::t('app', 'Created At'),
            'blockchain_time' => Yii::t('app', 'Blockchain Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMAccount()
    {
        return $this->hasOne(MAccount::className(), ['id' => 'm_account_id']);
    }
}
