<?php

namespace app\modules\merchant\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\merchant\models\MNodesTransactions;
use kartik\daterange\DateRangeBehavior;


/**
 * MNodesTransactionsSearch represents the model behind the search form of `app\modules\merchant\models\MNodesTransactions`.
 */
class MNodesTransactionsSearch extends MNodesTransactions
{
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;
    public $type;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
                'dateStartFormat' => 'Y-m-d H:i',
                'dateEndFormat' => 'Y-m-d H:i'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'confirmations', 'send_msg_status'], 'integer'],
            [['txid', 'currency', 'category', 'from_addr', 'to_addr', 'created_at', 'blockchain_time'], 'safe'],
            [['amount', 'fee'], 'number'],
            [['createTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MNodesTransactions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'send_msg_status' => $this->send_msg_status,
            'amount' => $this->amount,
            'category' => $this->category,
            'currency' => $this->currency,
            'fee' => $this->fee,
            'txid' => $this->txid,
            'from_addr' => $this->from_addr,
            'to_addr' => $this->to_addr,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['between', 'blockchain_time', $this->createTimeStart, $this->createTimeEnd]);
        $query->andFilterWhere(['>=','confirmations', $this->confirmations]);

        return $dataProvider;
    }
}
