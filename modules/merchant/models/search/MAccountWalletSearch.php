<?php

namespace app\modules\merchant\models\search;

use app\modules\merchant\models\MAccountWallet;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kartik\daterange\DateRangeBehavior;

/**
 * MAccountWalletSearch represents the model behind the search form of `app\admin\modules\altcoin\models\MAccountWallet`.
 */
class MAccountWalletSearch extends MAccountWallet
{
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
                'dateStartFormat' => 'Y-m-d H:i',
                'dateEndFormat' => 'Y-m-d H:i'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'm_account_id', 'created_at'], 'integer'],
            [['currency_wallet', 'currency_type'], 'safe'],
            [['balance'], 'number'],
            [['createTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MAccountWallet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'm_account_id' => $this->m_account_id,
            'balance' => $this->balance,
            'currency_type' => $this->currency_type
        ]);

//        echo $this->createTimeStart . " - " . $this->createTimeEnd;


        $query->andFilterWhere(['between', 'created_at', $this->createTimeStart, $this->createTimeEnd ]);

        $query->andFilterWhere(['like', 'currency_wallet', $this->currency_wallet]);

        return $dataProvider;
    }
}
