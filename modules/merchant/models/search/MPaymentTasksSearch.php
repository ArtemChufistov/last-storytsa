<?php

namespace app\modules\merchant\models\search;

use app\modules\merchant\models\MPaymentTasks;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kartik\daterange\DateRangeBehavior;


/**
 * This is the model class for table "m_send_txid".
 *
 * @property int $id
 * @property string $txid
 * @property int $m_id
 * @property string $created_at
 * @property string $currency
 * @property int $status
 * @property float $amount
 * @property string $from_addr
 * @property string $to_addr
 * @property string $category
 * @property int $isShowToUser
 * @property MAccount $mAccount
 */
class MPaymentTasksSearch extends MPaymentTasks
{

    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;
    public $type;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
                'dateStartFormat' => 'Y-m-d H:i',
                'dateEndFormat' => 'Y-m-d H:i'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'm_id', 'created_at', 'status', 'isShowToUser'], 'integer'],
            [['category', 'to_addr', 'from_addr', 'txid', 'amount', 'currency', 'confirmations', 'type'], 'safe'],
            [['createTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MSendTxid::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'txid' => $this->txid,
            'm_id' => $this->m_id,
            'sent_time' => $this->created_at,
            'status' => $this->status,
            'amount' => $this->amount,
            'category' => $this->category,
            'to_addr' => $this->to_addr,
            'from_addr' => $this->from_addr,
            'currency' => $this->currency,
            'isShowToUser' => $this->isShowToUser
        ]);

        $query->andFilterWhere(['between', 'blockchain_time', $this->createTimeStart, $this->createTimeEnd ]);
        $query->andFilterWhere(['>=','confirmations', $this->confirmations]);



        return $dataProvider;

    }
}
