<?php

namespace app\modules\merchant\models\search;

use app\modules\merchant\models\MAccountAllowIp;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MAccountAllowIpSearch represents the model behind the search form of `app\admin\modules\altcoin\models\MAccountAllowIp`.
 */
class MAccountAllowIpSearch extends MAccountAllowIp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'm_account_id', 'created_at', 'is_delete'], 'integer'],
            [['allow_ip'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MAccountAllowIp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'm_account_id' => $this->m_account_id,
            'created_at' => $this->created_at,
            'is_delete' => $this->is_delete,
        ]);

        $query->andFilterWhere(['like', 'allow_ip', $this->allow_ip]);

        return $dataProvider;
    }
}
