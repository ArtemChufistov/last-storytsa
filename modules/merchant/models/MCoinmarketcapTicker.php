<?php

namespace app\modules\merchant\models;

use Yii;

/**
 * This is the model class for table "m_coinmarketcap_ticker".
 *
 * @property string $id
 * @property string $name
 * @property string $symbol
 * @property string $rank
 * @property double $price_usd
 * @property double $price_btc
 * @property double $h24h_volume_usd
 * @property double $market_cap_usd
 * @property double $available_supply
 * @property double $total_supply
 * @property string $max_supply
 * @property double $percent_change_1h
 * @property double $percent_change_24h
 * @property double $percent_change_7d
 * @property int $last_updated
 */
class MCoinmarketcapTicker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_coinmarketcap_ticker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'symbol', 'price_usd', 'price_btc'], 'required'],
            [['price_usd', 'price_btc', 'h24h_volume_usd', 'market_cap_usd', 'available_supply', 'total_supply', 'percent_change_1h', 'percent_change_24h', 'percent_change_7d'], 'number'],
            [['last_updated'], 'integer'],
            [['id', 'name'], 'string', 'max' => 100],
            [['symbol', 'rank', 'max_supply'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'symbol' => 'Symbol',
            'rank' => 'Rank',
            'price_usd' => 'Price Usd',
            'price_btc' => 'Price Btc',
            'h24h_volume_usd' => 'H24h Volume Usd',
            'market_cap_usd' => 'Market Cap Usd',
            'available_supply' => 'Available Supply',
            'total_supply' => 'Total Supply',
            'max_supply' => 'Max Supply',
            'percent_change_1h' => 'Percent Change 1h',
            'percent_change_24h' => 'Percent Change 24h',
            'percent_change_7d' => 'Percent Change 7d',
            'last_updated' => 'Last Updated',
        ];
    }
}
