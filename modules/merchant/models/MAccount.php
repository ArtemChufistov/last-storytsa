<?php

namespace app\modules\merchant\models;

use lowbase\user\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use ErrorException;

/**
 * This is the model class for table "m_account".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $secret
 * @property string $created_at
 * @property string $updated_at
 * @property string $comment
 * @property string $url
 * @property int $is_delete
 * @property int $is_turn_on
 * @property object $image
 *
 * @property User $user
 * @property MAccountAllowIp[] $mAccountAllowIps
 * @property MAccountApiRequestLog[] $mAccountApiRequestLogs
 * @property MAccountWallet[] $mAccountWallets
 * @property [] $allActiveUserMerchants
 */
class MAccount extends \yii\db\ActiveRecord
{
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_account';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                // 'value' => 0,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => 'user_id'

            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'is_turn_on'], 'required'],
            [['user_id', 'is_delete'], 'integer'],
            [['created_at', 'updated_at', 'url', 'comment', 'image'], 'safe'],
            [['url'], 'url', 'defaultScheme' => ''],
            [['name'], 'string', 'max' => 100],
            [['secret'], 'string', 'min' => 6, 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['image'], 'file'/*, 'skipOnEmpty' => true*/, 'extensions' => 'png, jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Название магазина',
            'secret' => 'Секретный Ключ',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'is_delete' => 'Is Delete',
            'is_turn_on' => 'Включен',
            'comment' => 'Описание магазина',
            'url' => 'URL магазина',
            'image' => 'Файл картинки'
        ];
    }

    public function upload()
    {
        if ($this->validate() && isset($this->image)) {
            // Исправить на нормальный путь!!!
            $this->image->saveAs("/app/web/cmt/icons/m-logo/{$this->id}.png");
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getAllActiveUserMerchants()
    {

        $models = self::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['is_delete' => 0])->orderBy('id ASC')->all();
        $merchantsArray = ArrayHelper::map($models, 'id', 'id');

        return $merchantsArray;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMAccountAllowIps()
    {
        return $this->hasMany(MAccountAllowIp::className(), ['m_account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMAccountApiRequestLogs()
    {
        return $this->hasMany(MAccountApiRequestLog::className(), ['m_account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMAccountWallets()
    {
        return $this->hasMany(MAccountWallet::className(), ['m_account_id' => 'id']);
    }


    /**
     * Отправка пользователю информации по транзакции
     *
     * @param MNodesTransactions $m_node_transaction_model
     * @return array
     */
    public function sendToCustomerTxInfo($m_node_transaction_model = null)
    {
        $m_send_txid_log = new MSendTxidAffortsNum();

        // тестовая транзакция
        if (!isset($m_node_transaction_model)) {
            $url = $this->url;
            $m_node_transaction_model = MNodesTransactions::find()->orderBy('id ASC')->one();
            $m_send_txid_log->m_send_tx_id = 0;
        } else {
            $url = $m_node_transaction_model->mAccount->url;
        }


        if(!empty($url)) {

            $txInfo = Yii::$app->get($m_node_transaction_model->currency)->getTransaction($m_node_transaction_model->txid);
            $currency = $m_node_transaction_model->currency;

            $m_send_txid_log->m_send_tx_id = $m_node_transaction_model->id;


            $newTxInfo = [];
            foreach ($txInfo['details'] as $detail) {

                if ($detail['category'] == 'receive') {
                    $item['category'] = $detail['category'];
                    $item['address'] = $detail['address'];
                    $item['amount'] = $detail['amount'];

                    $newTxInfo[] = $item;
                }
            }


            $request = [
                "merchantId" => $this->id,
                "currency" => $currency,
                'txid' => $txInfo['txid'],
                'details' => $newTxInfo
            ];


            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setFormat(Client::FORMAT_URLENCODED)
//            ->setOptions([ 'timeout' => 1 ])
                ->setUrl($url)
                ->setData($request)
                ->send();


            if ($response->isOk) {

                if ($response->content == '*ok*') {

                    if (isset($m_node_transaction_model)) {
                        $m_node_transaction_model->send_msg_status = 1;
                        $m_node_transaction_model->save();
                    }

                    $m_send_txid_log->external_server_response = $response->content;
                    $response = "Успех, сервер клиент успешно получил транзакцию Ответ: *ok* . Транзакция будет считается полученой клиентом и отправлятся больше не будет.";
                } else {
                    $m_send_txid_log->external_server_response = 'Err';
                    $response = ['isError' => 'Что-то пошло не так, сервер получатель не вернул JSON ответ *ok* - подтверждение получения транзакции', 'answer' => trim($response->content)];
                }

            }


            $m_send_txid_log->save();


            return $answer = ['request' => $request, 'response' => $response];
        }

        throw new ErrorException( "Ошибка отправки уведомлений");
    }

}
