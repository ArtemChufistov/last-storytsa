<?php

namespace app\modules\merchant\models;

use Yii;
use yii\db\Expression;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "m_send_txid_afforts_num".
 *
 * @property int $id
 * @property int $m_send_tx_id
 * @property string $external_server_response
 * @property string $answer_time
 */
class MSendTxidAffortsNum extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['answer_time'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_send_txid_afforts_num';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_send_tx_id', 'external_server_response'], 'required'],
            [['m_send_tx_id'], 'integer'],
            [['answer_time'], 'safe'],
            [['external_server_response'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'm_send_tx_id' => 'M Send Tx ID',
            'external_server_response' => 'External Server Response',
            'answer_time' => 'Answer Time',
        ];
    }
}
