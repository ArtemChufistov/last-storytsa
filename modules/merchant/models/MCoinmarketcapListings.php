<?php

namespace app\modules\merchant\models;

use Yii;

/**
 * This is the model class for table "m_coinmarketcap_listings".
 *
 * @property string $id
 * @property string $name
 * @property string $symbol
 * @property string $website_slug
 */
class MCoinmarketcapListings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_coinmarketcap_listings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['name', 'symbol', 'website_slug'], 'string', 'max' => 100],
            [['website_slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'symbol' => 'Symbol',
            'website_slug' => 'Website Slug',
        ];
    }

    public static function coinInfo($currency){

       return self::find()->where(['website_slug' => $currency])->one();

    }
}