<?php

namespace app\modules\merchant\models\form;

use Yii;
use yii\base\Model;

/**
 * Enable Two-Factor Authentication form
 */
class TxCodeForm extends Model {

    /**
     * @var string The code entered by the user
     */
    public $code;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['code'], 'required'],
            ['code', 'filter', 'filter' => 'trim'],
            ['code', 'string', 'min' => 6],
        ];
    }

    /**
     * Наименование полей формы
     * @return array
     */
    public function attributeLabels() {
        return [
            'code' => Yii::t('app', 'Код Подтверждения транзакции')
        ];
    }
}