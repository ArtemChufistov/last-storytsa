<?php

namespace app\modules\merchant\models\form;

use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use Yii;
use yii\base\Model;
use yii\validators\Validator;


class NewAddressForm extends Model
{
    public $currency;

    public function rules()
    {
        return [
            [['currency'], 'required'],
            [['currency'], 'validate_currency'],
        ];
    }

    public function validate_currency($attribute, $params)
    {
        $currencyArr = MerchantHelper::avalableWalletsFromMerchantModuleArray();
        if (!isset($currencyArr[$this->$attribute])) {
            $this->addError('currency', Yii::t('app', 'Валюта не доступна'));
        }
    }

    public function attributeLabels()
    {
        return [
            'currency' => 'Валюта',
        ];
    }
}