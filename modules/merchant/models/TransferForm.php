<?php

namespace app\modules\merchant\models;

use Yii;
use yii\base\Model;
use yii\validators\Validator;


class TransferForm extends Model
{
    public $currency;
    public $amount;
    public $address;
    public $fee;


    public function rules()
    {
        return [
            [['address'], 'string', 'min' => 15],
            [['fee'], 'safe'],
            [['currency', 'address', 'amount'], 'required'],
            [['amount'], 'number'],
            [['amount'], 'amountBalance']
        ];
    }

    public function attributeLabels()
    {
        return [
            'currency' => 'Платежная система',
            'address' => 'На адрес',
            'amount' => 'Сумма',
            'fee' => 'Комиссия',
        ];
    }


    public function amountBalance($attribute, $params)
    {
        $balance = Yii::$app->get($this->currency)->getAddressesBalanceSum(Yii::$app->user->identity->id);

        if ($balance < $this->$attribute) {
            $this->addError($attribute, Yii::t('app', 'Максимальная сумма для перевода: {amount}', ['amount' => $balance]));
        }
    }
}