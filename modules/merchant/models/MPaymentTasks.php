<?php

namespace app\modules\merchant\models;

use Yii;
use yii\db\Expression;
use \yii\db\ActiveRecord;


/**
 * This is the model class for table "m_send_txid".
 *
 * @property int $id
 * @property int $id_parent
 * @property string $txid
 * @property int $m_id
 * @property int $user_id
 * @property int $is_processing_complete
 * @property string $created_at
 * @property string $currency
 * @property int $status
 * @property int $feePaySender
 * @property double $amount
 * @property double $fee_per_kByte
 * @property double $fee_per_transaction
 * @property int $fee_per_byte
 * @property double $fee
 * @property string $from_addr
 * @property string $to_addr
 * @property string $transit_addr
 * @property string $category
 * @property int $isShowToUser
 * @property int $request_type
 * @property int $send_msg_status
 * @property string $blockchain_time
 * @property int $confirmations
 * @property int $m_node_transaction_id
 * @property MAccount $mAccount
 */
class MPaymentTasks extends ActiveRecord
{
    const TX_STATUS_NEW = 0;
    const TX_STATUS_PROCESING = 10;
    const TX_STATUS_FOR_TRANSIT = 70;

    const TX_STATUS_FOR_TRANSIT_ONE_WALLET = 81;
    const TX_STATUS_FOR_TRANSIT_TWO_WALLET = 82;
    const TX_STATUS_FOR_TRANSIT_SEVERAL_WALLET = 83;

    const TX_STATUS_WATING_FOR_SEND = 88;
    const TX_STATUS_WATING_FOR_SEND_AFTER_SEND = 89;
    const TX_STATUS_WAS_SEND = 90;
    const TX_STATUS_COMPLETE = 100;
    const TX_STATUS_FAIL = 120;

    const TX_STATUS_CREATE_NEW_TX_FAIL = 125;

    const CATEGORY_SEND = 'send';
    const CATEGORY_RECEIVE = 'receive';

    const SHOW_TO_USER = 1;
    const HIDE_FROM_USER = 0;

    const REQUEST_TYPE_SYSTEM = 0;
    const REQUEST_TYPE_API = 50;
    const REQUEST_TYPE_WEB = 70;


    const IS_IN_PROCESSING = 0;
    const IS_COMPLETE = 1;


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_payment_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'currency', 'amount', 'category', 'm_node_transaction_id'], 'required'],
            [['m_id', 'status', 'confirmations', 'isShowToUser', 'send_msg_status', 'request_type', 'feePaySender', 'm_node_transaction_id', 'is_processing_complete'], 'integer'],
            [['created_at', 'from_addr', 'to_addr', 'transit_addr', 'fee', 'blockchain_time', 'id_parent', 'fee_per_kByte', 'fee_per_transaction', 'fee_per_byte'], 'safe'],
            [['txid'], 'string', 'max' => 200],
            [['txid', 'currency', 'amount', 'from_addr', 'to_addr', 'category'], 'unique', 'targetAttribute' => ['txid', 'currency', 'amount', 'from_addr', 'to_addr', 'category']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'txid' => Yii::t('app', 'Txid'),
            'm_id' => Yii::t('app', 'Мерчант ID'),
            'created_at' => Yii::t('app', 'Получена'),
            'status' => Yii::t('app', 'Статус'),
            'amount' => Yii::t('app', 'Кол-во'),
            'category' => Yii::t('app', 'Категория'),
            'to_addr' => Yii::t('app', 'На адр.'),
            'from_addr' => Yii::t('app', 'С адр.'),
            'confirmations' => Yii::t('app', 'Подтверждений'),
            'blockchain_time' => Yii::t('app', 'Время создания транзакции'),
            'currency' => Yii::t('app', 'Валюта'),
            'fee' => Yii::t('app', 'Комисия'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMAccount()
    {
        return $this->hasOne(MAccount::className(), ['id' => 'm_id']);
    }

    public function setStatus($status)
    {
        if ($this->status < $status) {
            $this->status = $status;
            $this->save();
        }
    }
}