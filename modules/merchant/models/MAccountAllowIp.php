<?php

namespace app\modules\merchant\models;

use Yii;

/**
 * This is the model class for table "m_account_allow_ip".
 *
 * @property int $id
 * @property int $m_account_id
 * @property string $allow_ip
 * @property int $created_at
 * @property int $is_delete
 *
 * @property MAccount $mAccount
 */
class MAccountAllowIp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_account_allow_ip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['m_account_id', 'allow_ip'], 'required'],
            [['m_account_id', 'created_at', 'is_delete'], 'integer'],
            [['allow_ip'], 'string', 'max' => 50],
            [['m_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => MAccount::className(), 'targetAttribute' => ['m_account_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'm_account_id' => 'M Account ID',
            'allow_ip' => 'Allow Ip',
            'created_at' => 'Created At',
            'is_delete' => 'Is Delete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMAccount()
    {
        return $this->hasOne(MAccount::className(), ['id' => 'm_account_id']);
    }
}
