<?php

namespace app\modules\merchant\models;

use Yii;
use yii\db\Expression;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "m_account_api_request_log".
 *
 * @property int $id
 * @property int $m_account_id
 * @property string $request
 * @property string $currency
 * @property string $command
 * @property int $created_at
 *
 * @property MAccount $mAccount
 */
class MAccountApiRequestLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_account_api_request_log';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request'], 'required'],
            [['m_account_id', 'created_at'], 'integer'],
            [['currency', 'command'], 'safe'],
            [['request'], 'string', 'max' => 255],
            [['m_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => MAccount::className(), 'targetAttribute' => ['m_account_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'm_account_id' => 'M Account ID',
            'request' => 'Request',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMAccount()
    {
        return $this->hasOne(MAccount::className(), ['id' => 'm_account_id']);
    }
}
