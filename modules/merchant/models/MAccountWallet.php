<?php

namespace app\modules\merchant\models;

use lowbase\user\models\User;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * This is the model class for table "m_account_wallet".
 *
 * @property int $id
 * @property int $m_account_id
 * @property int $user_id
 * @property string $currency_wallet
 * @property string $currency_type
 * @property double $balance
 * @property int $created_at
 *
 * @property MAccount $mAccount
 * @property LbUser $user
 */
class MAccountWallet extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_account_wallet';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['currency_wallet', 'currency_type', 'user_id'], 'required'],
            [['m_account_id', 'created_at'], 'integer'],
            [['balance'], 'number'],
            [['currency_wallet'], 'string', 'max' => 255],
            [['currency_type'], 'string', 'max' => 50],
            [['m_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => MAccount::className(), 'targetAttribute' => ['m_account_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'm_account_id' => 'Мерчант ID',
            'currency_wallet' => 'Кошелек',
            'currency_type' => 'Валюта',
            'balance' => 'Баланс',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMAccount()
    {
        return $this->hasOne(MAccount::className(), ['id' => 'm_account_id']);
    }
}
