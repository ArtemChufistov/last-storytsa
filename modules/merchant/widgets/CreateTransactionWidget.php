<?php

namespace app\modules\merchant\widgets;

use app\modules\mail\components\SendMailComponent;
use ErrorException;
use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use app\modules\merchant\models\form\TxCodeForm;
use app\modules\merchant\models\MPaymentTasks;
use yii;
use yii\base\Widget;
use yii\helpers\Url;
use app\modules\merchant\models\TransferForm;

class CreateTransactionWidget extends Widget
{
    public $view = '_create-transaction';

    public function init()
    {
        parent::init();
    }

    public function run()
    {

        $session = Yii::$app->session;
        $transferForm = new TransferForm();
        $txCodeForm = new TxCodeForm();



        if ($txCodeForm->load(Yii::$app->request->post()) && $txCodeForm->validate()) {

            try {

                $tx_inf = $session->get('tx_confirm_code');

                if ($txCodeForm->code == $tx_inf['code']) {

                    $session->remove('tx_confirm_code');

                    if(!MerchantHelper::createNewSendTransaction(Yii::$app->user->identity->id, null,
                        $tx_inf['currency'], $tx_inf['address'], $tx_inf['amount'],
                        0, MPaymentTasks::REQUEST_TYPE_WEB)){

                        throw new ErrorException('Ошибка создания транзакции повторите попытку ');
                    }

                    Yii::$app->session->setFlash('mTransferSuccess', Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/merchant/widgets/_transfer_success', [
                        'tx_inf' => $tx_inf
                    ]));

                    Yii::$app->controller->refresh();

                    return true;
                }else{
                    $txCodeForm->addError('code', 'Некорректный код');

                    return $this->render('@app/themes/' . Yii::$app->params['theme'] . '/merchant/widgets/_create_transaction_code', [
                        'currency' => $tx_inf['currency'],
                        'address' => $tx_inf['address'],
                        'amount' => $tx_inf['amount'],
                        'txCodeForm' => $txCodeForm
                    ]);
                }

            } catch (ErrorException $e) {
                Yii::$app->session->setFlash('mTransferError', $e->getMessage());
            }
        }


        if ($transferForm->load(Yii::$app->request->post()) && $transferForm->validate()) {

            try {
                $code = rand(100000, 999999);

                $tx_info = [
                    'currency' => $transferForm->currency,
                    'address' => $transferForm->address,
                    'amount' => $transferForm->amount,
                    'code' => $code
                ];

                $session->set('tx_confirm_code', $tx_info);
                SendMailComponent::confirmTransaction(Yii::$app->user->identity, Yii::$app->user->identity->email, $tx_info);

                return $this->render('@app/themes/' . Yii::$app->params['theme'] . '/merchant/widgets/_create_transaction_code', [
                    'currency' => $transferForm->currency,
                    'address' => $transferForm->address,
                    'amount' => $transferForm->amount,
                    'txCodeForm' => $txCodeForm
                ]);

            } catch (ErrorException $e) {
                Yii::$app->session->setFlash('mTransferError', $e->getMessage());
            }
        }

        return $this->render('@app/themes/' . Yii::$app->params['theme'] . '/merchant/widgets/' . $this->view, [
            'transferForm' => $transferForm
        ]);
    }
}