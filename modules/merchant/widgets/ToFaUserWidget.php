<?php

namespace app\modules\merchant\widgets;

use app\modules\merchant\components\twofa\models\TwoFaForm;
use yii;
use yii\base\Widget;
use yii\helpers\Url;


class ToFaUserWidget extends Widget
{
    public $view = '_to_fa_user';
    public $user_id;

    private $user;

    public function init()
    {
        $this->user = Yii::$app->user->identity;
        parent::init();
    }

    public function run()
    {
        $model = new TwoFaForm();
        $user = Yii::$app->user->identity;
        $model->setUser($user);

        if (!$user->hasTwoFaEnabled()) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->response->redirect(Url::to(['/office/user']));
            }
        } else {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user->disableTwoFa();
                Yii::$app->response->redirect(Url::to(['/office/user']));
            }
        }

        $model->code = '';

        return $this->render('@app/themes/' . Yii::$app->params['theme'] . '/merchant/widgets/' . $this->view, [
            'model' => $model
        ]);
    }
}