<?php

namespace app\modules\merchant\widgets;

use app\modules\merchant\components\merchant\helpers\MerchantHelper;
use app\modules\merchant\models\form\NewAddressForm;
use yii;
use yii\base\Widget;
use yii\helpers\Url;
use app\modules\merchant\models\TransferForm;

class GetCoinsWidget extends Widget {
    public $view = '_get-coins';

    public function init() {
        parent::init();
    }

    public function run() {

        $model = new NewAddressForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            try {
                $address = Yii::$app->get($model->currency)->generateAddress(null, Yii::$app->user->identity->id);
                Yii::$app->session->setFlash('mNewAddress', $address);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('mNewAddressError', Yii::t('app', 'Ошибка: {error}', [ 'error' => $e->getMessage()]));
            }
        }

        return $this->render('@app/themes/' . Yii::$app->params['theme'] . '/merchant/widgets/' . $this->view, [
            'model' => $model
        ]);
    }
}