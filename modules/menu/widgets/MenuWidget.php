<?php

namespace app\modules\menu\widgets;

use app\modules\menu\models\Menu;
use yii;
use yii\base\Widget;
use yii\helpers\Url;

class MenuWidget extends Widget {
	public $menuName;

	public $view;

	public $firstActive = false;

	public $requestUrlByRoute = false;

	public function init() {
		parent::init();
	}

	public function run() {
		$menu = Menu::find()->where(['name' => $this->menuName])->andWhere(['lang' => Yii::$app->language])->roots()->one();

		if (empty($menu)) {
			$menu = Menu::find()->where(['name' => $this->menuName])->roots()->one();
			if (empty($menu)) {
				throw new \Exception('menu empty ' . $this->menuName, 1);
			}
		}

		if ($this->requestUrlByRoute == true) {
			$requestUrl = Url::to([\Yii::$app->controller->id . '/' . Yii::$app->controller->action->id]);
		} else {
			$requestUrl = Yii::$app->request->url;
		}

		return $this->render('@app/themes/' . Yii::$app->params['theme'] . '/menu/widgets/' . $this->view, [
			'menu' => $menu,
			'firstActive' => $this->firstActive,
			'requestUrl' => $requestUrl,
		]);
	}
}