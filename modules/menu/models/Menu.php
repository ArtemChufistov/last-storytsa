<?php

namespace app\modules\menu\models;

use Yii;
use creocoder\nestedsets\NestedSetsBehavior;
/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $title
 * @property string $link
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $name
 * @property string $lang
 */
class Menu extends \yii\db\ActiveRecord
{
    public $makeRoot;
    public $parentId;
    public $attach;

    const  PREPAND = 'prependTo';
    const  APPEND = 'appendTo';
    const  INSERT_BEFORE = 'insertBefore';
    const  INSERT_AFTER = 'insertAfter';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    public static function attachTypes()
    {
        return [
            self::PREPAND => Yii::t('app', 'первый дочерний элемент'),
            self::APPEND => Yii::t('app', 'последний дочерний элемент'),
            self::INSERT_BEFORE => Yii::t('app', 'перед выбранным элементом'),
            self::INSERT_AFTER => Yii::t('app', 'после выбранного элемента'),
        ];
    }

    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                 'treeAttribute' => 'tree',
                 'leftAttribute' => 'lft',
                 'rightAttribute' => 'rgt',
                 'depthAttribute' => 'depth',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function attach()
    {
        if ($this->makeRoot == true){
            $this->makeRoot();
            return;
        }

        if (empty($this->parentId)){
            $parentMenuItem = $this->parents(1)->one();
        }else{
            $parentMenuItem = Menu::find()->where(['id' => $this->parentId])->one();
        }

        $this->tree = $parentMenuItem->tree;
        $this->name = $parentMenuItem->name;
        $this->lang = $parentMenuItem->lang;

        if (!in_array($this->attach, array_keys(self::attachTypes()))){
            $this->save();
            return;
        }

        $function = $this->attach;

        $this->$function($parentMenuItem);
    }

    public static function find()
    {
        return new MenuQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tree, title', 'link', 'lft', 'rgt', 'depth', 'name', 'lang', 'icon', 'parentId', 'attach', 'makeRoot'], 'safe'],
            [['lft', 'rgt', 'depth'], 'integer'],
            [['title', 'link', 'name', 'lang'], 'string', 'max' => 255],
        ];
    }

    public function getDropDownList()
    {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $list[0] = Yii::t('app', 'Выберите значение');

        foreach($dataProvider->getModels() as $modelItem){

            if ($modelItem->id == $this->id){
                continue;
            }
            $title = '';
            for ($i = 0; $i < $modelItem->depth; $i++){
                $title = $title . '--';
            }
            $title = $title . $modelItem->title . '           (' . $modelItem->lang . ')';
            $list[$modelItem->id] = $title;
        }

        return $list;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tree' => Yii::t('app', 'Дерево'),
            'title' => Yii::t('app', 'Заголовок'),
            'link' => Yii::t('app', 'Ссылка'),
            'lft' => Yii::t('app', 'Lft'),
            'rgt' => Yii::t('app', 'Rgt'),
            'depth' => Yii::t('app', 'Уровень'),
            'name' => Yii::t('app', 'Системное название меню'),
            'lang' => Yii::t('app', 'Язык'),
            'icon' => Yii::t('app', 'Иконка'),
            'parentId' => Yii::t('app', 'Родитель'),
            'attach' => Yii::t('app', 'Поставить как')
        ];
    }
}
