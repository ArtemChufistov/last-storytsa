<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\menu\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Список пунктов меню');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <div class="col-md-12">
      <div class="box box-success collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header">
          <p>
            <?= Html::a(Yii::t('app', 'Создать пункт меню'), ['create'], ['class' => 'btn btn-success']) ?>
          </p>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '<div class = "col-md-3">{summary}</div>{items}{pager}',
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    [
                        'attribute' => 'title',
                        'format' => 'raw',
                        'value' => function ($model) { return $this->render('@app/modules/menu/views/menu/columns/title.php', ['model' => $model]);},
                    ],
                    [
                        'attribute' => 'link',
                        'format' => 'raw',
                        'value' => function ($model) { return $this->render('@app/modules/menu/views/menu/columns/link.php', ['model' => $model]);},
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'value' => function ($model) { return $this->render('@app/modules/menu/views/menu/columns/name.php', ['model' => $model]);},
                    ],
                    [
                        'attribute' => 'icon',
                        'format' => 'raw',
                        'value' => function ($model) { return $this->render('@app/modules/menu/views/menu/columns/icon.php', ['model' => $model]);},
                    ],
                    'lang',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
      </div>
    </div>
</div>
