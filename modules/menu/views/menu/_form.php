<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use app\modules\menu\models\Menu;

/* @var $this yii\web\View */
/* @var $model app\modules\menu\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>
<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <div class="col-md-12">
                <?= $form->field($model, 'makeRoot')->checkBox(['label' => 'Сделать новое дерево']); ?>
            </div>

            <div class="col-md-6">
                <?php echo $form->field($model, 'parentId')->widget(Select2::classname(), [
                    'data' => $model->getDropDownList(),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],]);
                ?>
            </div>

            <div class="col-md-6">
                <?php echo $form->field($model, 'attach')->widget(Select2::classname(), [
                    'data' => array_merge([0 => 'Выберите значение'], Menu::attachTypes()),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],]);
                ?>
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
            </div>
            
            <div class="col-md-12">
                <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить пункт меню') : Yii::t('app', 'Обновить пункт меню'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
      </div>
    </div>
</div>

