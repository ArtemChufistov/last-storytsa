<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\menu\models\MenuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class = "row">
        <div class="col-md-3">
            <?= $form->field($model, 'id') ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'title') ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'link') ?>
        </div>

        <div class="col-md-3">
            <?php  echo $form->field($model, 'depth') ?>
        </div>
    </div>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'lang') ?>

    <div class = "row">
        <div class="col-md-6">
            <?= Html::submitButton(Yii::t('app', 'Поиск'), ['class' => 'btn btn-success']) ?>
            <?= Html::resetButton(Yii::t('app', 'Сбросить'), ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
