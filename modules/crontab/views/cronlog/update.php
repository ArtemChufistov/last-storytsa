<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\crontab\models\CronLog */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Cron Log',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cron Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cron-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
