<?php

namespace app\modules\crontab\commands;

use app\modules\crontab\models\CronLog;
use app\modules\crontab\models\Job;
use app\modules\finance\components\cron\CoinBaseSyncComponent;
use app\modules\finance\components\cron\CurrencyGraphComponent;
use app\modules\finance\components\cron\PaymentComponent;
use app\modules\finance\components\FchangeComponent;
use app\modules\finance\components\paysystem\CoinBaseComponent;
use app\modules\finance\components\paysystem\PoloniexComponent;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyCourseHistory;
use app\modules\finance\models\Payment;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\UserPayWalletBitcoin;
use app\modules\mail\components\SendMailComponent;
use app\modules\matrix\components\MarketingComponent;
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixQueue;
use app\modules\merchant\components\cron\Refresher;
use app\modules\merchant\components\cron\TxInfoSender;
use app\modules\profile\models\User;
use app\modules\profile\models\UserInfo;
use yii\httpclient\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Address;
use yii\console\Controller;
use yii\helpers\Console;
use app\modules\profile\promo\SuhbaMathcingBonusPromo;
use app\modules\invest\components\marketing\FamilyMarketing;
use app\modules\lang\components\AdditionalTranslate;
use app\modules\invest\components\UserInvestComponent;
use app\modules\invest\models\UserInvest;
use app\modules\finance\components\cron\CourseComponent;
use app\modules\finance\components\paysystem\AdvcashComponent;
use app\modules\invest\components\marketing\SuhbaMarketing;
use app\modules\profile\components\UserComponent;
use app\modules\profile\models\Promo;
use app\modules\finance\models\AdvcashTransaction;
use app\modules\invest\components\marketing\LkpoolMarketing;
use app\modules\finance\components\cron\SyncComponent;
use app\modules\mainpage\models\Preference;
use app\modules\shop\components\cron\RefreshComponent;
use app\modules\invest\components\marketing\IngamoMarketing;

// количество мест в матрице - SELECT count(user_id) as countUser, user_id FROM `matrix` GROUP BY user_id ORDER BY `countUser` DESC

class TestController extends \yii\console\Controller {
	public function actionIndex() {

        \app\modules\finance\components\cron\PaymentComponent::process();
	    exit;
        $url = 'https://top-exchange.com/user_api/get_balances';
        $login = 'ingamo';
        $apiPass = 'Fkbyf889Fktrcfylh89';

        $client = new Client();

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->setData([
                'login' => $login,
                'api_pass' => $apiPass
            ])->send();

        $data = $response->getData();

        if (!empty($data) && $data['is_ok'] == 'ok') {
            echo "\r\n";
            print_r($data);
            echo "\r\n";
        }

        exit;
        $userArray = User::find()->where(['<=', 'id', 26262])->all();
        foreach ($userArray as $user) {
            SendMailComponent::sendPromo($user, $user->email);
            echo 'Юзеру отправено приглашение: ' . $user->id . "\r\n";
            sleep(rand(0, 10));
        }
        exit;
        $userArray = User::find()->where(['>=', 'id', 29110])->all();

        foreach ($userArray as $user) {
            SendMailComponent::inviteForActivateLK($user->email);
            echo 'Юзеру отправено приглашение: ' . $user->id . "\r\n";
            sleep(rand(0, 10));
        }

        exit;
        $address = '14iY44ywgdF8wLDcpC66goNjFqoY4V3Z8t12';
        if (checkAddress($address)) {
            echo 'valid' . "\r\n";
        }else{
            echo 'not valid' . "\r\n";
        }

        exit;
        $userArray = User::find()->where(['>=', 'id', 25967])->all();
        foreach($userArray as $user) {
            SendMailComponent::inviteForActivateLK($user->email);
            sleep(rand(0, 30));
            echo 'sending mail to: ' . $user->email . "\r\n";
        }
    }
}

function checkAddress($address)
{
    $origbase58 = $address;
    $dec = "0";

    for ($i = 0; $i < strlen($address); $i++)
    {
        $dec = bcadd(bcmul($dec,"58",0),strpos("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz",substr($address,$i,1)),0);
    }

    $address = "";

    while (bccomp($dec,0) == 1)
    {
        $dv = bcdiv($dec,"16",0);
        $rem = (integer)bcmod($dec,"16");
        $dec = $dv;
        $address = $address.substr("0123456789ABCDEF",$rem,1);
    }

    $address = strrev($address);

    for ($i = 0; $i < strlen($origbase58) && substr($origbase58,$i,1) == "1"; $i++)
    {
        $address = "00".$address;
    }

    if (strlen($address)%2 != 0)
    {
        $address = "0".$address;
    }

    if (strlen($address) != 50)
    {
        return false;
    }

    if (hexdec(substr($address,0,2)) > 0)
    {
        return false;
    }

    return substr(strtoupper(hash("sha256",hash("sha256",pack("H*",substr($address,0,strlen($address)-8)),true))),0,8) == substr($address,strlen($address)-8);
}