<?php

namespace app\modules\crontab\commands;

use yii\console\Controller;
use yii\helpers\Console;
use app\modules\crontab\models\Job;
use app\modules\crontab\models\CronLog;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\UserInvest;
use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use app\modules\profile\models\UserInfo;
use app\modules\profile\models\User;
use app\modules\profile\models\Country;
use app\modules\profile\models\City;
use app\modules\profile\models\AuthAssignment;
use app\modules\payment\models\Payment;
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixGift;
use app\modules\mainpage\models\Preference;
use app\modules\mail\components\SendMailComponent;
use app\modules\finance\models\Transaction;

/*
Класс используется для занесения первоначальных структур в проект
*/

class LoadexcelController extends \yii\console\Controller
{
    public function actionIndex()
    {
        $this->ingamo();
    }

    public function ingamo()
    {

        //$sheetData = array_map('str_getcsv', file(dirname(__FILE__) . '/../excel/ingamo2/cryptoconsulting.csv'));
        //$sheetData = array_map('str_getcsv', file(dirname(__FILE__) . '/../excel/ingamo2/Azrbaidzhan_valetyn.csv'));
        //$sheetData = array_map('str_getcsv', file(dirname(__FILE__) . '/../excel/ingamo2/Vietnam_coreteam1_final.csv'));
        $sheetData = array_map('str_getcsv', file(dirname(__FILE__) . '/../excel/ingamo2/synteraleader.csv'));

        $dataType = 'syntera';

        foreach($sheetData as $num => $row){
            if ($num > 0) {
                $userInfo = explode(";", $row[0]);

                $user = $this->createUserIfExist($num, $userInfo);
                if (!empty($user)) {
                    $this->createTransactionIfExist($num, $userInfo, $user, $dataType);
                }
            }
        }
    }

    public function setUserPassword($row)
    {
        $user = User::find()->where(['login' => 'A' . $row['A']])->one();

        if (!empty($user)){
            $user->setPassword($row['M']);
            $user->save(false);
            echo 'Пользователю: ' . $user->login . ' назначен пароль: ' . $row['M'] . "\r\n";
        }else{
            echo 'Пользователь не найден: ' . $row['A'] . "\r\n";
        }
    }

    public function createUserIfExist($num, $userExcel)
    {
        if (empty($userExcel[0])) {
            self::showErrorRow($num+1, 'не удалось получить E-mail', $userExcel);
            return false;
        }

        if (empty($userExcel[1])) {
            self::showErrorRow($num+1, 'не удалось получить логин пригласителя', $userExcel);
            //$userExcel[1] = 'admin';
            return false;
        }

        $authAssignment = AuthAssignment::ITEM_NAME_VERIFIED_USER;
        $user = User::find()->where(['email' => $userExcel[0]])->one();
        $password = rand(1000000, 9999999);

        if (empty($user)){
            // зарегистрировать пользователя
            $user = new User;
            $user->email      = $userExcel[0];
            $user->created_at = date('Y-m-d H:i:s');
            $user->login      = $userExcel[2];
            $user->status     = User::STATUS_ACTIVE;
            $user->setPassword($password);

            $user->save(false);
            $user->updateAuthAssigment($authAssignment);

            $parentUser = User::find()->where(['login' => $userExcel[1]])->one();
            if (empty($parentUser)){
                self::showErrorRow($num+1, 'не найден пригласитель: ' . $userExcel[1], $userExcel);
                return false;
            }

            $user->parent_id = $parentUser->id;
            $user->save(false);
            $user->appendToParent();

            //echo 'Добавлен пользователь: ' . $user->login . ' пароль: ' . $password . ' поставлен под: ' . $parentUser->login . "\r\n";
        }

        return $user;
    }

    public function createTransactionIfExist($num, $userExcel, $user, $dataType)
    {
        $currencyFreez = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();
        $transaction = Transaction::find()->where(['to_user_id' => $user->id])->andwhere(['currency_id' => $currencyFreez->id])->one();

        if (empty($transaction) && !empty($userExcel[3])) {
            $transaction = new Transaction;
            $transaction->to_user_id = $user->id;
            $transaction->data = $dataType;
            $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
            $transaction->sum = $userExcel[3];
            $transaction->currency_id = $currencyFreez->id;
            $transaction->save();

            $toUser = $user->getBalances([$transaction->currency_id])[$transaction->currency_id];
            $toUser->recalculateValue();

            //echo 'Для пользователя ' . $user->login . ' добавлена транза на сумму:' .  $transaction->sum  . "\r\n";
        }
    }

    public static function showErrorRow($num, $message, $userExcel)
    {
        echo 'Косяк строка №: ' . $num . ' ' . $message . ' ' . implode(', ', $userExcel) . "\r\n";
    }

    public function mixUserArray($newUserArray, $oldUserArray)
    {
        foreach ($newUserArray as $login => $newUser) {
            if(!empty($oldUserArray[$login])){
                $newUserArray[$login]['email'] = $oldUserArray[$login]['email'];
                $newUserArray[$login]['skype'] = $oldUserArray[$login]['skype'];
                $newUserArray[$login]['address'] = $oldUserArray[$login]['address'];
                $newUserArray[$login]['password'] = $oldUserArray[$login]['password'];
                $newUserArray[$login]['status'] = $oldUserArray[$login]['status'];
                $newUserArray[$login]['birthday'] = $oldUserArray[$login]['birthday'];
            }else{
                $newUserArray[$login]['email'] = '';
                $newUserArray[$login]['skype'] = '';
                $newUserArray[$login]['address'] = '';
                $newUserArray[$login]['password'] = '';
                $newUserArray[$login]['status'] = '';
                $newUserArray[$login]['birthday'] = '';
            }
        }

        return $newUserArray;
    }

    /******************** STORYTSA ***********************/

    public function storytsa()
    {
        $objPHPExcel = \PHPExcel_IOFactory::load(dirname(__FILE__) . '/../excel/struct7.xls');
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

        $users = [];
        foreach($sheetData as $num => $row){

            if ($row['B'] == ''){
                continue;
            }
            $users[$num + 1] = [
                'login' => $row['B'],
                'parentLogin' => $row['C'],
                'level' => $row['D'],
            ];
        }

        $userArray = self::checkLogins($users);

        self::checkWallets($userArray);
exit;
        self::checkMatrix($userArray);

        echo 123;exit;
        //self::toMatrixBitExp($userArray);
        //self::activateLevels($userArray);

        //self::setLevel1Gifts($userArray);
        //self::setLevels($userArray);
    }

    public static function checkMatrix($userArray)
    {
        foreach($userArray as $user){
            $parentUser = User::find()->where(['login' => $user->parentLogin])->one();
            $parentMatrixXls = Matrix::find()->where(['user_id' => $parentUser->id])->one();

            if (empty($parentMatrixXls)){
                echo 'нет места в матрице для юзера: ' . $parentUser->login . "\r\n";
            }

            $userMatrix = Matrix::find()->where(['user_id' => $user->id])->one();

            if (empty($userMatrix)){
                echo 'Нет места в матрице для юзера: ' . $user->login . "\r\n";
            }

            $parentMatrix = $userMatrix->parent()->one();
            
            if ($parentMatrix->getUser()->one()->login != $parentMatrixXls->getUser()->one()->login){
                echo 'У пользователя: ' . $user->login . ' парент: ' . $parentMatrix->getUser()->one()->login . ' по экселю: ' . $parentMatrixXls->getUser()->one()->login . "\r\n";
            }
            echo $user->id . "\r\n";
        }
    }

    // активация уровней
    public static function activateLevels($userArray)
    {
        foreach($userArray as $user){
            $matrix = Matrix::find()->where(['user_id' => $user->id])->one();

            $matrixGift = MatrixGift::find()->where(['matrix_id' => $matrix->id])->one();

            if ($user->levelNum == 2){
                $matrixGift->buy_level_2 = 1;
            }elseif($user->levelNum == 3){
                $matrixGift->buy_level_2 = 1;
                $matrixGift->buy_level_3 = 1;
            }
            $matrixGift->save(false);
        }
    }

    // поставить в матрицу
    public static function toMatrixBitExp($userArray)
    {
        $currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();

        foreach($userArray as $user){
            $parentUser = User::find()->where(['login' => $user->parentLogin])->one();
            $parentMatrix = Matrix::find()->where(['user_id' => $parentUser->id])->one();

            if (empty($parentMatrix)){
                echo 'нет места в матрице для юзера: ' . $parentUser->login . "\r\n";
            }
            echo 'Пытаемся вставить место в матрицу' . "\r\n"; 

            $result = $parentMatrix->findPlace(2);

            $matrix = new Matrix;
            $matrix->sum = 0.03;
            $matrix->sort = $result['sort'];
            $matrix->status = Matrix::STATUS_PAY_OK;
            $matrix->user_id = $user->id;
            $matrix->parent_id = $result['id'];
            $matrix->currency_id = $currency->id;
            $matrix->date_add = date('Y-m-d H:i:s');

            $matrix->save(false);

            $matrixGift = new MatrixGift;
            $matrixGift->matrix_id = $matrix->id;
            $matrixGift->buy_level_1 = 1;
            $matrixGift->left_level_1 = 2;
            $matrixGift->save(false);

            echo 'Место успешно вставлено для логина: ' . $user->login . "\r\n";
        }
    }

    // подсчет оставшихся подарков для 1го уровня
    public static function setLevel1Gifts($userArray)
    {
    	$count = 2;
    	foreach($userArray as $user){
    		$matrix = Matrix::find()->where(['user_id' => $user->id])->one();

    		$children = $matrix->children()->all();

    		$matrixGift = $matrix->getMatrixGift()->one();
    		$matrixGift->left_level_1 = $count - count($children);

    		$matrixGift->save(false);
    	}
    }

    // растановка уровней
    public static function setLevels($userArray)
    {
        $paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_BITCOIN])->one();
        $currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();

    	foreach($userArray as $user){

    		if ($user->levelNum > 1){
	    		for ($i = 2; $i <= $user->levelNum ; $i++) { 
	                $marketing = new StorytsaMarketing;
	                $marketing->set(['user' => $user, 'paySystem' => $paySystem, 'currency' => $currency, 'level' => $i]);
	                $payment = $marketing->getPaymentBuyLevel();

	                $payment->date_add = $user->paymentDate;
	                $payment->status = Payment::PROCESS_SYSTEM;
	                $payment->save(false);

	                StorytsaMarketing::putMatrixLevels();

	                echo 'У пользователя: ' . $user->login . ' приобретен уровень: ' . $i . ' заявка номер: ' . $payment->id . "\r\n";
	    		}
	    		sleep(4);
    		}
    	}
    }


    // покупка мест в матрице
    public static function toMatrix($userArray)
    {
    	$countNoMtrix = 0;
    	foreach($userArray as $user){
    		$matrix = Matrix::find()->where(['user_id' => $user->id])->one();

    		if (empty($matrix)){

    			$payment = Payment::find()->where(['from_user_id' => $user->id, 'type' => Payment::TYPE_BUY_PLACE, 'status' => Payment::STATUS_WAIT])->one();

    			if (empty($payment)){
    				$payment = Payment::find()->where(['from_user_id' => $user->id, 'type' => Payment::TYPE_BUY_PLACE, 'status' => Payment::STATUS_WAIT_SYSTEM])->one();
    			}

    			if (empty($payment)){
    				$payment = Payment::find()->where(['from_user_id' => $user->id, 'type' => Payment::TYPE_BUY_PLACE])->one();
    				echo 'У пользователя трабла: ' . $user->login . ' заявка в статусе: ' .  $payment->status . "\r\n";exit;
    			}else{
    				$payment->status = Payment::PROCESS_SYSTEM;
    				$payment->date_add = $user->paymentDate;
    				$payment->save(false);
    			}

    			StorytsaMarketing::putMatrixPlaces();

    			$matrixPlace = Matrix::find()->where(['user_id' => $user->id])->one();

    			if (empty($matrixPlace)){
    				echo 'Фигня какаято у пользователя:' . $user->login . "\r\n"; exit; 
    			}
				
    			echo 'Пользователь: ' . $matrixPlace->getUser()->one()->login . ' встал под: ' . $matrixPlace->parent()->one()->getUser()->one()->login . "\r\n";

    			sleep(2);

    			$countNoMtrix++;
    		}
    	}

    	echo "\r\n";
    	echo 'Пользователей поставлено в матрицу: ' . $countNoMtrix;
    	echo "\r\n";

    	/*
    	foreach($userArray as $user){
    		$payment = Payment::find()->where(['from_user_id' => $user->id])->one();

    		echo 'У пользователя: ' . $user->login . ' заявка в статусе: ' . $payment->status . "\r\n";
    	}
    	*/
    }

    // проверка на созданность заявок в матрицу
    public static function checkPayments($userArray)
    {
    	$countNoPayment = 0;
    	foreach($userArray as $user){
    		$payment = Payment::find()->where(['from_user_id' => $user->id])->one();

    		if (empty($payment)){
    			echo 'У пользователя: ' . $user->login . ' нет заявки на матрицу' . "\r\n";
    			$countNoPayment++;
    		}
    	}

    	echo "\r\n";
    	echo 'Итого без заявок: ' . $countNoPayment;
    	echo "\r\n";
    }

    // проверка на заполненость реквизитов
    public static function checkWallets($userArray)
    {
    	$countWithoutWallets = 0;
    	foreach ($userArray as $user) {

			foreach($user->getAllPayWallet() as $wallet){
				if (empty($wallet->wallet)){
					echo 'У пользователя: ' . $user->login . ' нет ревизитов' . "\r\n";
					$countWithoutWallets++;
				}
			}
    	}

    	echo "\r\n";
    	echo 'Итого без ревизитов: ' . $countWithoutWallets;
    	echo "\r\n";
    }

    // првоерка логинов на существование и то что они активированы
    public static function checkLogins($users)
    {
    	$countNotVerify = 0;
    	$countNotFounded = 0;

    	$userArray = [];
    	foreach ($users as $num => $userItem) {
    		
    		$user = User::find()->where(['login' => $userItem['login']])->one();

            $parentUser = User::find()->where(['login' => $userItem['parentLogin']])->one();

            if (empty($parentUser)){
                echo 'Строка ' . ($num-1) . ' не найден пригласитель: ' . $userItem['parentLogin'] . "\r\n";
                $countNotFounded++;
                continue;
            }

    		if (empty($user)){
    			echo 'Строка ' . ($num-1) . ' не найден логин: ' . $userItem['login'] . "\r\n";
    			$countNotFounded++;
    			continue;
    		}

    		if ($user->getAuthAssignments()->one()->item_name == AuthAssignment::ITEM_NAME_NOT_VERIFIED_USER){
    			echo "Не верифицирован: " . $user->login . "\r\n";
    			$countNotVerify++;
    		}

    		if (!($userItem['level'] == '' || $userItem['level'] == ' ')){
    			$level = $userItem['level'];
    		}else{
    			$level = 1;
    		}

            $user->levelNum = $level;
            $user->parentLogin = $userItem['parentLogin'];

    		$userArray[] = $user;
    	}

    	echo "\r\n";
    	echo "Итого не верифицированных: " . $countNotVerify . "\r\n";
        echo "Итого не найдено: " . $countNotFounded . "\r\n";
    	echo "Итого пользователей: " . count($userArray) . "\r\n";
    	echo "\r\n";

    	return $userArray;
    }

    /******************** END OF STORYTSA ***********************/

}