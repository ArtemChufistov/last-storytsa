<?php

namespace app\modules\crontab\commands;

use yii\console\Controller;
use yii\helpers\Console;
use app\modules\crontab\models\Job;
use app\modules\mainpage\models\Preference;
use app\modules\crontab\models\CronLog;
 
class ConsoleController extends \yii\console\Controller
{
    public function actionIndex($cronNum)
    {
        echo 'Запущен в: ' . date('Y-m-d H:i:s') . "\r\n";

        $startTime = time();
        
        $cronRunPeriodInSec = Preference::find()->where(['key' => Preference::KEY_CRON_RUN_PERIOD])->one()->value;

        while( (time() - $startTime) < $cronRunPeriodInSec - 2){

            $jobArray = Job::find()->where(['active' => Job::ACTIVE_TRUE])->andWhere(['cron_num' => $cronNum])->all();

            foreach($jobArray as $job){

                if ((time() - $startTime) >= 50){
                    break;
                }

                if ((time() - $job->frequency) < strtotime($job->last_date)){
                    sleep(1);continue;
                }

                $job->last_date = date('Y-m-d H:i:s');
                $job->save(false);

                echo 'Выполнение в: ' . date('Y-m-d H:i:s') . ' задания: ' . $job->key . "\r\n";

                try{
                    $cronLog = new CronLog;

                    $cronLog->success = 0;
                    $cronLog->job_id = $job->id;
                    $cronLog->date_start = date('Y-m-d H:i:s');

                    $info = call_user_func($job->key);

                    $cronLog->success = 1;
                    $cronLog->addition_info = $info;
                }catch(exception $e){
                    echo $e->getMessage();
                    $cronLog->addition_info = $e->getMessage();
                }

                $cronLog->date_end = date('Y-m-d H:i:s');
                $cronLog->save();
            }
        }


        echo 'Завершен в: ' . date('Y-m-d H:i:s') . "\r\n";
    }
}