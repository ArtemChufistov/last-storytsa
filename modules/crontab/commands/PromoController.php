<?php

namespace app\modules\crontab\commands;

use app\modules\profile\components\PromoComponent;
use Yii;


// количество мест в матрице - SELECT count(user_id) as countUser, user_id FROM `matrix` GROUP BY user_id ORDER BY `countUser` DESC 

class PromoController extends \yii\console\Controller
{
    public function actionIndex()
    {
        PromoComponent::run();
    }
}