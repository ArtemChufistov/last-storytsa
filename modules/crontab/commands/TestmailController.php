<?php

namespace app\modules\crontab\commands;

use app\modules\crontab\models\CronLog;
use app\modules\crontab\models\Job;
use app\modules\mail\components\SendMailComponent;

class TestmailController extends \yii\console\Controller {
	public function actionIndex($host, $username, $password, $port, $encryption, $email) {
		SendMailComponent::testEmail($host, $username, $password, $port, $encryption, $email);
	}
}