<?php

namespace app\modules\crontab\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crontab\models\CronLog;

/**
 * CronLogSearch represents the model behind the search form of `app\modules\crontab\models\CronLog`.
 */
class CronLogSearch extends CronLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'job_id', 'success'], 'integer'],
            [['work', 'date_end', 'date_start', 'addition_info'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CronLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'job_id' => $this->job_id,
            'date_end' => $this->date_end,
            'date_start' => $this->date_start,
            'success' => $this->success,
        ]);

        $query->andFilterWhere(['like', 'work', $this->work])
            ->andFilterWhere(['like', 'addition_info', $this->addition_info]);

        return $dataProvider;
    }
}
