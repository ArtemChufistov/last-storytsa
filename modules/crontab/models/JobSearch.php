<?php

namespace app\modules\crontab\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crontab\models\Job;

/**
 * JobSearch represents the model behind the search form of `app\modules\crontab\models\Job`.
 */
class JobSearch extends Job
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['type', 'work', 'key', 'frequency', 'last_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Job::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'last_date' => $this->last_date,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'work', $this->work])
            ->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'frequency', $this->frequency]);

        return $dataProvider;
    }
}
