<?php

namespace app\modules\crontab\models;

use Yii;

/**
 * This is the model class for table "job".
 *
 * @property integer $id
 * @property string $type
 * @property string $work
 * @property string $key
 * @property string $frequency
 * @property string $last_date
 */
class Job extends \yii\db\ActiveRecord
{

    const KEY_SEND_MAIL = 'app\modules\mail\components\SendMailComponent::run';
    const KEY_CHECK_PAYMENT = 'app\modules\matrix\components\StorytsaMarketing::checkPayments';
    const KEY_PUT_MATRIX_PLACE = 'app\modules\matrix\components\StorytsaMarketing::putMatrixPlaces';

    const ACTIVE_TRUE = 1;
    const ACTIVE_FALSE = 0;

    const CRON_NUM_FIRST = 'first';
    const CRON_NUM_SECOND = 'second';
    const CRON_NUM_THREE = 'three';
    const CRON_NUM_FOUR = 'four';

    public static function getCronNumArray() {
        return [
            self::CRON_NUM_FIRST => Yii::t('app', 'Первая консоль'),
            self::CRON_NUM_SECOND => Yii::t('app', 'Вторая консоль'),
            self::CRON_NUM_THREE => Yii::t('app', 'Третья консоль'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'frequency'], 'required'],
            [['work'], 'string'],
            [['last_date', 'type', 'active'], 'safe'],
            [['type', 'key', 'frequency'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Тип'),
            'work' => Yii::t('app', 'Задача'),
            'key' => Yii::t('app', 'Ключ'),
            'frequency' => Yii::t('app', 'Частота запуска'),
            'last_date' => Yii::t('app', 'Последняя дата запуска'),
            'active' => Yii::t('app', 'Активность'),
            'cron_num' => Yii::t('app', 'Номер крона'),
        ];
    }
}
