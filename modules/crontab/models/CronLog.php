<?php

namespace app\modules\crontab\models;

use Yii;

/**
 * This is the model class for table "cron_log".
 *
 * @property integer $id
 * @property integer $job_id
 * @property string $work
 * @property string $date_end
 * @property string $date_start
 * @property integer $success
 * @property string $addition_info
 */
class CronLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cron_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'success'], 'integer'],
            [['work', 'addition_info'], 'string'],
            [['date_end', 'date_start'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'job_id' => Yii::t('app', 'Job ID'),
            'work' => Yii::t('app', 'Work'),
            'date_end' => Yii::t('app', 'Date End'),
            'date_start' => Yii::t('app', 'Date Start'),
            'success' => Yii::t('app', 'Success'),
            'addition_info' => Yii::t('app', 'Addition Info'),
        ];
    }
}
