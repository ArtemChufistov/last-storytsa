<?php
namespace app\modules\invest\components\marketing;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\profile\models\User;
use app\modules\finance\models\Currency;
use app\modules\mainpage\models\Preference;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\UserBalance;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;

class SuhbaMarketing{

	private $user;
	private $currency;
	private $paySystem;
	private $investPref;
	public $investType;
	public $userInvest;

	public $user_invest_date_add;
	public $user_to_invest_date_add;
	public $structure_currency_key;
	public $without_from_balance = false;
	public $without_user_invest = false;
	public $without_structure_balance = false;

	public $dateToAddMarketingSum = '2017-10-10 00:00:00'; // Дата с которой начать распределять маркетинговую сумму

	public $refUserArray;

	public static function getTransactionPeriod()
	{
		return 60*60*2;
	}

	public static function getPeriodArray()
	{
		$periodArray = [
            [ 'dateFrom' => '2017-10-10 00:00:00', 'dateTo' => '2017-12-15 23:59:59' ],
            [ 'dateFrom' => '2017-12-23 00:00:00', 'dateTo' => '2018-02-05 23:59:59' ],
            [ 'dateFrom' => '2018-02-06 00:00:00', 'dateTo' => '2018-02-28 23:59:59' ],
            [ 'dateFrom' => '2018-03-01 00:00:00', 'dateTo' => '2018-03-06 23:59:59' ],
            [ 'dateFrom' => '2018-03-07 00:00:00', 'dateTo' => '2018-03-31 23:59:59' ],
            [ 'dateFrom' => '2018-04-01 00:00:00', 'dateTo' => '2018-04-30 23:59:59' ],
            [ 'dateFrom' => '2018-05-01 00:00:00', 'dateTo' => '2018-05-31 23:59:59' ],
            [ 'dateFrom' => '2018-06-01 00:00:00', 'dateTo' => '2018-06-30 23:59:59' ],
            [ 'dateFrom' => '2018-07-01 00:00:00', 'dateTo' => '2018-07-31 23:59:59' ],
            [ 'dateFrom' => '2018-08-01 00:00:00', 'dateTo' => '2018-08-31 23:59:59' ],
            [ 'dateFrom' => '2018-09-01 00:00:00', 'dateTo' => '2018-09-30 23:59:59' ],
            [ 'dateFrom' => '2018-10-01 00:00:00', 'dateTo' => '2018-10-31 23:59:59' ],
            [ 'dateFrom' => '2018-11-01 00:00:00', 'dateTo' => '2018-11-30 23:59:59' ],
            [ 'dateFrom' => '2018-12-01 00:00:00', 'dateTo' => '2018-12-31 23:59:59' ],
        ];

        return $periodArray;
	}

	public function set($data)
	{
		foreach ($data as $item => $value) {
			$this->$item = $value;
		}

		if (empty($data['marketingData'])){
			throw new \Exception("Invest type id not set", 1);
		}

		$this->currency = Currency::find()->where(['key' => Currency::KEY_RUR])->one();

		$this->investType = InvestType::find()->where(['id' => $data['marketingData']])->one();

		if (empty($this->investType)){
			return;
		}

		$this->initInvestPref();

		return $this;
	}

	public function setUser($user)
	{
		$this->user = $user;
	}

	// обработка маркетинга по крону
	public function handle()
	{

	}

	// приобретение инвестиционного пакета участником
	public function handleInvest()
	{
		if ($this->without_from_balance == false){
			$this->fromBalance($this->investType->sum);
		}

		if ($this->without_user_invest == false){
			$this->createUserInvest();
		}

		if ($this->without_structure_balance == false){
			if ($this->without_user_invest == true){
				$this->findUserInvest();
			}
			$this->toStructureBalance();
		}

		return true;
	}

	// начисления по структуре на 21 уровней
	public function toStructureBalance()
	{
		if (empty($this->user)){
			return;
		}

		$amount = $this->investType['sum'];

		if (empty($this->structure_currency_key)){
			$this->structure_currency_key = Currency::KEY_FREEZ;
		}
		// пригласителю 1
		$parent1 = $this->findParentWithInvest($this->user);
		if (empty($parent1) || $this->user->id == $parent1->id){return;}
		if (!$this->toUserBalanceIfInvest($parent1, $amount, $this->investPref[InvestPref::KEY_PARENT1][0]->value, $this->structure_currency_key, 1)){ return; };

		// пригласителю 2
		$parent2 = $this->findParentWithInvest($parent1);
		if ($parent1->id == $parent2->id){return;}
		if (!$this->toUserBalanceIfInvest($parent2, $amount, $this->investPref[InvestPref::KEY_PARENT2][0]->value, $this->structure_currency_key, 2)){ return; };

		// пригласителю 3
		$parent3 = $this->findParentWithInvest($parent2);
		if ($parent2->id == $parent3->id){return;}
		if (!$this->toUserBalanceIfInvest($parent3, $amount, $this->investPref[InvestPref::KEY_PARENT3][0]->value, $this->structure_currency_key, 3)){ return; };

		// пригласителю 4
		$parent4 = $this->findParentWithInvest($parent3);
		if ($parent3->id == $parent4->id){return;}
		if (!$this->toUserBalanceIfInvest($parent4, $amount, $this->investPref[InvestPref::KEY_PARENT4][0]->value, $this->structure_currency_key, 4)){ return; };

		// пригласителю 5
		$parent5 = $this->findParentWithInvest($parent4);
		if ($parent4->id == $parent5->id){return;}
		if (!$this->toUserBalanceIfInvest($parent5, $amount, $this->investPref[InvestPref::KEY_PARENT5][0]->value, $this->structure_currency_key, 5)){ return; };

		// пригласителю 6
		$parent6 = $this->findParentWithInvest($parent5);
		if ($parent5->id == $parent6->id){return;}
		if (!$this->toUserBalanceIfInvest($parent6, $amount, $this->investPref[InvestPref::KEY_PARENT6][0]->value, $this->structure_currency_key, 6)){ return; };

		// пригласителю 7
		$parent7 = $this->findParentWithInvest($parent6);
		if ($parent6->id == $parent7->id){return;}
		if (!$this->toUserBalanceIfInvest($parent7, $amount, $this->investPref[InvestPref::KEY_PARENT7][0]->value, $this->structure_currency_key, 7)){ return; };

		// пригласителю 8
		$parent8 = $this->findParentWithInvest($parent7);
		if ($parent7->id == $parent8->id){return;}
		if (!$this->toUserBalanceIfInvest($parent8, $amount, $this->investPref[InvestPref::KEY_PARENT8][0]->value, $this->structure_currency_key, 8)){ return; };

		// пригласителю 9
		$parent9 = $this->findParentWithInvest($parent8);
		if ($parent8->id == $parent9->id){return;}
		if (!$this->toUserBalanceIfInvest($parent9, $amount, $this->investPref[InvestPref::KEY_PARENT9][0]->value, $this->structure_currency_key, 9)){ return; };

		// пригласителю 10
		$parent10 = $this->findParentWithInvest($parent9);
		if ($parent9->id == $parent10->id){return;}
		if (!$this->toUserBalanceIfInvest($parent10, $amount, $this->investPref[InvestPref::KEY_PARENT10][0]->value, $this->structure_currency_key, 10)){ return; };

		// пригласителю 11
		$parent11 = $this->findParentWithInvest($parent10);
		if ($parent10->id == $parent11->id){return;}
		if (!$this->toUserBalanceIfInvest($parent11, $amount, $this->investPref[InvestPref::KEY_PARENT11][0]->value, $this->structure_currency_key, 11)){ return; };

		// пригласителю 12
		$parent12 = $this->findParentWithInvest($parent11);
		if ($parent11->id == $parent12->id){return;}
		if (!$this->toUserBalanceIfInvest($parent12, $amount, $this->investPref[InvestPref::KEY_PARENT12][0]->value, $this->structure_currency_key, 12)){ return; };

		// пригласителю 13
		$parent13 = $this->findParentWithInvest($parent12);
		if ($parent12->id == $parent13->id){return;}
		if (!$this->toUserBalanceIfInvest($parent13, $amount, $this->investPref[InvestPref::KEY_PARENT13][0]->value, $this->structure_currency_key, 13)){ return; };

		// пригласителю 14
		$parent14 = $this->findParentWithInvest($parent13);
		if ($parent13->id == $parent14->id){return;}
		if (!$this->toUserBalanceIfInvest($parent14, $amount, $this->investPref[InvestPref::KEY_PARENT14][0]->value, $this->structure_currency_key, 14)){ return; };

		// пригласителю 15
		$parent15 = $this->findParentWithInvest($parent14);
		if ($parent14->id == $parent15->id){return;}
		if (!$this->toUserBalanceIfInvest($parent15, $amount, $this->investPref[InvestPref::KEY_PARENT15][0]->value, $this->structure_currency_key, 15)){ return; };

		// пригласителю 16
		$parent16 = $this->findParentWithInvest($parent15);
		if ($parent15->id == $parent16->id){return;}
		if (!$this->toUserBalanceIfInvest($parent16, $amount, $this->investPref[InvestPref::KEY_PARENT16][0]->value, $this->structure_currency_key, 16)){ return; };

		// пригласителю 17
		$parent17 = $this->findParentWithInvest($parent16);
		if ($parent16->id == $parent17->id){return;}
		if (!$this->toUserBalanceIfInvest($parent17, $amount, $this->investPref[InvestPref::KEY_PARENT17][0]->value, $this->structure_currency_key, 17)){ return; };

		// пригласителю 18
		$parent18 = $this->findParentWithInvest($parent17);
		if ($parent17->id == $parent18->id){return;}
		if (!$this->toUserBalanceIfInvest($parent18, $amount, $this->investPref[InvestPref::KEY_PARENT18][0]->value, $this->structure_currency_key, 18)){ return; };

		// пригласителю 19
		$parent19 = $this->findParentWithInvest($parent18);
		if ($parent18->id == $parent19->id){return;}
		if (!$this->toUserBalanceIfInvest($parent19, $amount, $this->investPref[InvestPref::KEY_PARENT19][0]->value, $this->structure_currency_key, 19)){ return; };

		// пригласителю 20
		$parent20 = $this->findParentWithInvest($parent19);
		if ($parent19->id == $parent20->id){return;}
		if (!$this->toUserBalanceIfInvest($parent20, $amount, $this->investPref[InvestPref::KEY_PARENT20][0]->value, $this->structure_currency_key, 20)){ return; };

		// пригласителю 21
		$parent21 = $this->findParentWithInvest($parent20);
		if ($parent20->id == $parent21->id){return;}
		if (!$this->toUserBalanceIfInvest($parent21, $amount, $this->investPref[InvestPref::KEY_PARENT21][0]->value, $this->structure_currency_key, 21)){ return; };
	}


	public function findUserInvest()
	{
		$this->userInvest = UserInvest::find()
			->where(['user_id' => $this->user->id])
			->andWhere(['date_add' => date('Y-m-d H:i:s', strtotime($this->user_invest_date_add))])
			->andWhere(['invest_type_id' => $this->investType->id])
			->one();

		if (empty($this->userInvest)){
			throw new Exception('Не найден пакет для пользователя: ' . $this->userInvest . ' дата: ' . $this->user_invest_date_add, 1);
		}
	}

	public function findParentWithInvest($user)
	{
		$cloneUser = clone $user;
		$investUserFinded = false;

		while ($investUserFinded == false) {
			$cloneUser = $cloneUser->parent()->one();
			
			if (empty($cloneUser)){
				//echo ' Не найден пригласитель для пользоватея: ' . $user->login . "\r\n";
				$cloneUser = $user;
				$investUserFinded = true;
				break;
			}

			$userInvest = UserInvest::find()->where(['user_id' => $cloneUser->id])->andWhere(['<=', 'date_add', $this->user_to_invest_date_add])->one();

			if (!empty($userInvest)){
				$investUserFinded = true;
				break;
			}

			//echo 'Пользователь не получит рефские: ' . $cloneUser->login . ' от пользователя: ' . $this->user->login . "\r\n" . "\r\n";;
		}

		return $cloneUser;
	}

	// Перевод средств на основной баланс с задержкой 24 часа
	public static function fromMonthToMainBalance()
	{
		$nowTime = time();

		$firstMonthday = date('Y-m-01');

		$currencyFreez = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();

		$userBalanceArray = UserBalance::find()->where(['currency_id' => $currencyFreez->id])->andWhere(['>','value', '0'])->all();

		foreach($userBalanceArray as $userBalance){

			$transaction = Transaction::find()
				->where(['from_user_id' => $userBalance->user_id])
				->andWhere(['currency_id' => $currencyFreez->id])
				->orderBy(['date_add' => SORT_DESC])
				->one();

			if (empty($transaction)){
				$transaction = Transaction::find()
					->where(['to_user_id' => $userBalance->user_id])
					->andWhere(['currency_id' => $currencyFreez->id])
					->orderBy(['date_add' => SORT_ASC])
					->one();

				if (empty($transaction)){
					echo 'Неправильный замороженный баланс у пользователя: ' . $userBalance->getUser()->one()->login . "\r\n";
					continue;
				}
			}

			if (strtotime($transaction->date_add) > strtotime($firstMonthday)){
				continue;
			}

			$userBalance->recalculateValue();

			// переводим на основной счёт
			if ($userBalance->value > 0){

				echo 'Перевод с замороженного баланса на основной для пользователя: ' . $userBalance->getUser()->one()->login . ' Сумма: ' . $userBalance->value . "\r\n";

				// списываем средства с 24го счёта
		        $transaction = new Transaction;
		        $transaction->sum = $userBalance->value;
		        $transaction->type = Transaction::TYPE_MINUS_REF_BONUS;
		        $transaction->from_user_id = $userBalance->user_id;
		        $transaction->currency_id = $currencyFreez->id;
		        $transaction->date_add = date('Y-m-d H:i:s');
        		$transaction->save(false);

        		$currencyRUR = Currency::find()->where(['key' => Currency::KEY_RUR])->one();

        		// зачисляем на основной счёт
		        $transaction = new Transaction;
		        $transaction->sum = $userBalance->value;
		        $transaction->type = Transaction::TYPE_PLUS_REF_BONUS;
		        $transaction->to_user_id = $userBalance->user_id;
		        $transaction->currency_id = $currencyRUR->id;
		        $transaction->date_add = date('Y-m-d H:i:s');
        		$transaction->save(false);

		        $balanceRUR = $transaction->getToUser()->one()->getBalances([$currencyRUR->id])[$currencyRUR->id];

		        $balanceRUR->recalculateValue();
        		$userBalance->recalculateValue();
			}
		}
	}

	public function toUserBalanceIfInvest($user, $amount, $percent, $currencyKey, $level)
	{

		if (strtotime($this->user_invest_date_add) < strtotime($this->dateToAddMarketingSum)){
			return false;
		}

		if (empty($user)){
			return true;
		}

		$sum = ($amount / 100) * $percent;

		$this->refUserArray[] = [
			'sum' => $sum, 
			'to_user_id' => $user->id, 
			'from_user_id' => $this->user->id, 
			'user_invest_id' => $this->userInvest->id,
			'currency_id' => $this->userInvest->currency_id, 
			'percent' => $percent,
			'level' => $level,
			'date' => $this->userInvest->date_add];

		return true;
	}

	// начисление дивидендов на инвестиционный пакет
	public function handlePercent($userInvest, $investType)
	{
		$this->investType = $investType;

		$this->initInvestPref();

		$sum = ($investType->sum / 100) * $this->investPref[InvestPref::KEY_STAT_PERCENT][0]->value;

		$userInvest->date_update = date("Y-m-d H:i:s", strtotime($userInvest->date_update) + $this->investPref[InvestPref::KEY_PAY_PERIOD][0]->value);
		$userInvest->paid_sum += $sum;
		$userInvest->closed_percent += $this->investPref[InvestPref::KEY_STAT_PERCENT][0]->value;
		$userInvest->save(false);
		
        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        $transaction->to_user_id = $userInvest->user_id;
        $transaction->currency_id = $this->investPref[InvestPref::KEY_CURRENT_CURRENCY][0]->value;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

        $balance = $userInvest->getUser()->one()->getBalances([$transaction->currency_id])[$transaction->currency_id];
        $balance->recalculateValue();
	
        //echo "\r\n" . 'Начислено участнику: ' . $userInvest->getUser()->one()->login . ' со вклада: ' . $userInvest->id. ' сумма: ' . $sum . "\r\n";
	}

	// приобрести инвест пакет для пользователя
	public function createUserInvest()
	{
		$this->userInvest = new UserInvest;
		$this->userInvest->user_id = $this->user->id;
		$this->userInvest->invest_type_id = $this->investType->id;
		$this->userInvest->currency_id = $this->currency->id;
		$this->userInvest->sum = $this->investType->sum;
		$this->userInvest->paid_sum = 0;
		$this->userInvest->closed_percent = 0;

		if (empty($this->user_invest_date_add)){
			$this->userInvest->date_add = date("Y-m-d H:i:s");
		}else{
			$this->userInvest->date_add = $this->user_invest_date_add;
		}

		$this->userInvest->date_update = $this->userInvest->date_add;
		$this->userInvest->save();

		$currencyVou = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();

		$countAkcii = $this->countAkicciInInvestForDate($this->userInvest->date_add, $this->investType->id);

        $transaction = new Transaction;
        $transaction->sum = $countAkcii;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_AKCII;
        $transaction->to_user_id = $this->userInvest->user_id;
        $transaction->currency_id = $currencyVou->id;
        $transaction->date_add = $this->userInvest->date_add;
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

        $balance = $transaction->getToUser()->one()->getBalances([$transaction->currency_id])[$transaction->currency_id];
        $balance->recalculateValue();

        //echo "\r\n" . 'Начислено участнику: ' . $this->userInvest->getUser()->one()->login . ' акций: ' . $transaction->sum . ' дата: ' . $transaction->date_add .  "\r\n";
	}

	// проверка возможности списания с баланса
	public function checkBalance($cost)
	{
		$this->user->recalclulateBalances();

		$balance = $this->user->getBalances()[$this->currency->id];

		if ($balance->value < $cost){
			return false;
		}else{
			return true;
		}
	}

	// списать средства с баланса участника
	public function fromBalance($cost)
	{
		$this->user->recalclulateBalances();

		$balance = $this->user->getBalances()[$this->currency->id];

		if ($balance->value < $cost){
			throw new \Exception(Yii::t('app', 'Не достаточно средств, пожалуйста пополните свой баланс на сумму <strong>{cost}</strong> {currency} в разделе <a href = "{link}">Финансы</a>', [
				'cost' => number_format($cost, 2, ',', ' '),
				'currency' => $this->currency->title,
				'link' => Url::to(['/profile/office/finance']),
			]));
			return;
		}

        $transaction = new Transaction;
        $transaction->from_user_id = $this->user->id;
        $transaction->type = Transaction::TYPE_BUY_INVEST;
        $transaction->sum = $cost;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

		$balance->recalculateValue();

		return true;
	}

	// начилисть средства на баланс пригласителя
	public function toParentBalance($percent, $level)
	{
		$descendants = $this->user->ancestors($level, true)->all();

		if (empty($descendants)){
			throw new \Exception("Parent not found for user", 1);
		}else{
			$parent = array_pop($descendants);
		}

		$cost = ($this->investType->sum / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $cost;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        $transaction->to_user_id = $parent->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

        $balance = $parent->getBalances()[$this->currency->id];

		$balance->recalculateValue();

		return $cost;
	}

	// начислить средства на баланс админа
	public function chargeBalanceFix($percent, $login)
	{
		$user = User::find()->where(['login' => $login])->one();

		$balance = $user->getBalances()[$this->currency->id];

		$sum = ($this->investType->sum / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_ADMIN_BONUS;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

		$balance->recalculateValue();

		return $sum;
	}

	// инициализация матричных настроек
	public function initInvestPref()
	{
		foreach(InvestPref::find()->where(['invest_type_id' => $this->investType->id])->all() as $investPref){
			$this->investPref[$investPref->key][] = $investPref;
		};

		return $this;
	}

	// количество акции в инвест паете в разные даты
	public function countAkicciInInvestForDate($date, $investTypeId)
	{
		$investPref = InvestPref::find()->where(['key' => InvestPref::KET_COUNT_AKCII])->andWhere(['invest_type_id' => $investTypeId])->one();

		return $investPref->value;

		//return $investType->
		/*
		$akciiArray = [
			[
				'fromDate' => '2017-07-11 00:00:00',
				'toDate' => '2017-08-07 23:59:59',
				'counAkcii' => [
					'МИНИМАЛЬНЫЙ' => 10000,
					'БАЗОВЫЙ' => 62500,
					'СТАНДАРТ' => 250000,
					'БИЗНЕС' => 750000,
					'VIP' => 1500000,
				]
			],[
				'fromDate' => '2017-08-08 00:00:00',
				'toDate' => '2017-09-05 23:59:59',
				'counAkcii' => [
					'МИНИМАЛЬНЫЙ' => 8000,
					'БАЗОВЫЙ' => 50000,
					'СТАНДАРТ' => 200000,
					'БИЗНЕС' => 600000,
					'VIP' => 1200000,
				]
			],[
				'fromDate' => '2017-09-06 00:00:00',
				'toDate' => '2017-10-09 23:59:59',
				'counAkcii' => [
					'МИНИМАЛЬНЫЙ' => 6000,
					'БАЗОВЫЙ' => 37500,
					'СТАНДАРТ' => 150000,
					'БИЗНЕС' => 450000,
					'VIP' => 900000,
				]
			],[
				'fromDate' => '2017-10-10 00:00:00',
				'toDate' => '2017-12-30 23:59:59',
				'counAkcii' => [
					'МИНИМАЛЬНЫЙ' => 4000,
					'БАЗОВЫЙ' => 25000,
					'СТАНДАРТ' => 100000,
					'БИЗНЕС' => 300000,
					'VIP' => 600000,
				]
			],[
				'fromDate' => '2018-01-01 00:00:00',
				'toDate' => '2018-12-30 23:59:59',
				'counAkcii' => [
					'МИНИМАЛЬНЫЙ' => 20000,
					'БАЗОВЫЙ' => 50000,
					'СТАНДАРТ' => 150000,
					'БИЗНЕС' => 600000,
					'VIP' => 1800000,
					'Супер VIP' => 1200000,
					'PREMIUN' => 2400000,
				]
			],[
				'fromDate' => '2018-01-01 00:00:00',
				'toDate' => '2018-12-30 23:59:59',
				'counAkcii' => [
					'МИНИМАЛЬНЫЙ' => 20000,
					'БАЗОВЫЙ' => 50000,
					'СТАНДАРТ' => 150000,
					'БИЗНЕС' => 600000,
					'VIP' => 1800000,
					'Супер VIP' => 1200000,
					'PREMIUN' => 2400000,
				]
			]
		];

		$investType = InvestType::find()->where(['id' => $investTypeId])->one();

		foreach($akciiArray as $akcii){
			if (strtotime($akcii['fromDate']) <= strtotime($date) && strtotime($date) <= strtotime($akcii['toDate'])){
				//echo 'Для даты:' . $date . ' куплено акций: ' . $akcii['counAkcii'][$investType->name] . "\r\n";
				return $akcii['counAkcii'][$investType->name];
			}
		}

		echo 'Пакет не найден для: ' . $investType->name . ' дата: ' . $date . "\r\n";
		exit;
		*/
	}

	public function buildUserTree()
	{
		$time = time();
		$result = [];

		$childrens[0]['id'] = $this->user->login;
		$childrens[0]['user_id'] = $this->user->id;
		$childrens[0]['parentId'] = 0;

		$descendants = $this->user->descendants()->all();

		$userInfArray = ArrayHelper::map(User::find()->all(), 'id', 'login');

		$users[$this->user->login] = $this->user;
		foreach ($descendants as $num => $descendant) {

			$childrens[$num + 1]['id'] = $descendant->login;
			$childrens[$num + 1]['user_id'] = $descendant->id;
			$childrens[$num + 1]['parentId'] = $userInfArray[$descendant->parent_id];
			$users[$descendant->login] = $descendant;
		}

		$userInvestArray = ArrayHelper::map(UserInvest::find()->all(), 'user_id', 'id');

		foreach ($childrens as $num => $children) {
			if (empty($userInvestArray[$children['user_id']])){
				$delParentId = $children['id'];
				$reParentId = $children['parentId'];
				unset($childrens[$num]);
				$childrens = self::reParentFor($childrens, $delParentId, $reParentId);
			}
		}

		if (empty($childrens)){
			return [];
		}

		foreach ($childrens as $num => &$children) {
			if (empty($users[$children['parentId']])){
				continue;
			}

			$children['text'] = Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/office/partial/_info-row-work', [
				'user' => $users[$children['id']], 
				'userInfo' => $users[$children['id']]->initUserInfo(), 
				'parent' => $users[$children['parentId']]
			]);
		}

		$userTree = $this->buildTree($childrens, 'parentId', 'nodes', 'id');



		if (!empty($userTree[0]['nodes'])) {
			return $userTree[0]['nodes'];
		} else {
			return [];
		}

		return $result;
	}

	private function buildTree($flat, $pidKey, $nodes, $idKey = null) {
		$grouped = array();
		foreach ($flat as $sub) {
			$grouped[$sub[$pidKey]][] = $sub;
		}

		$fnBuilder = function ($siblings) use (&$fnBuilder, $nodes, $grouped, $idKey) {
			foreach ($siblings as $k => $sibling) {
				$id = $sibling[$idKey];
				if (isset($grouped[$id])) {
					$sibling[$nodes] = $fnBuilder($grouped[$id], $nodes);
				}
				$siblings[$k] = $sibling;
			}

			return $siblings;
		};

		$tree = $fnBuilder($grouped[0]);

		return $tree;
	}

	public function reParentFor($childrens, $delParentId, $reParentId){

		foreach($childrens as &$child){
			if ($child['parentId'] === $delParentId){
				$child['parentId'] = $reParentId;
			}
		}

		return $childrens;
	}

}
?>