<?php
namespace app\modules\invest\components\marketing;

use Yii;
use yii\helpers\Url;
use app\modules\profile\models\User;
use app\modules\payment\models\Currency;
use app\modules\mainpage\models\Preference;
use app\modules\payment\models\Transaction;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;

class InvestMarketing{

	private $user;
	private $currency;
	private $paySystem;
	private $investType;
	private $investPref;
	private $userInvest;

	public function set($data)
	{
		foreach ($data as $item => $value) {
			$this->$item = $value;
		}

		if (empty($data[0])){
			throw new \Exception("Invest type id not set", 1);
		}

		$this->currency = Currency::find()->where(['key' => Currency::KEY_USD])->one();

		$this->investType = InvestType::find()->where(['id' => $data[0]])->one();

		$this->initInvestPref();

		return $this;
	}

	// обработка маркетинга по крону
	public function handle()
	{
        $investTypeArray = InvestType::find()->all();

        foreach ($investTypeArray as $investType) {
            $investPrefPayPeriod = InvestPref::find()->where(['key' => InvestPref::KEY_PAY_PERIOD])->andWhere(['invest_type_id' => $investType->id])->one();
            
            if (!empty($investPrefPayPeriod)){
                $userInvest = UserInvest::find()->where(['invest_type_id' => $investType->id])->andWhere(['<=', 'date_update', date("Y-m-d H:i:s", time() - $investPrefPayPeriod->value)])->one();
                if (!empty($userInvest)){
                    $marketing->handlePercent($userInvest, $investType, $investPrefPayPeriod->value);
                }
            }
        }
	}

	// приобретение инвестиционного пакета участником
	public function handleInvest()
	{
		$this->fromBalance($this->investType->sum);

		$this->createUserInvest();

		if (!empty($this->investPref[InvestPref::KEY_PARENT])){
			foreach($this->investPref[InvestPref::KEY_PARENT] as $investPrefParent){
				$this->toParentBalance($investPrefParent->value, $investPrefParent->value1);
			}
		}

		if (!empty($this->investPref[InvestPref::KEY_USER])){
			foreach($this->investPref[InvestPref::KEY_USER] as $investPrefParent){
				$this->chargeBalanceFix($investPrefParent->value, $investPrefParent->value1);
			}
		}

		return true;
	}

	// начисление дивидендов на инвестиционный пакет
	public function handlePercent($userInvest, $investType)
	{
		$this->investType = $investType;

		$this->initInvestPref();

		$sum = ($investType->sum / 100) * $this->investPref[InvestPref::KEY_STAT_PERCENT][0]->value;

		$userInvest->date_update = date("Y-m-d H:i:s", strtotime($userInvest->date_update) + $this->investPref[InvestPref::KEY_PAY_PERIOD][0]->value);
		$userInvest->paid_sum += $sum;
		$userInvest->closed_percent += $this->investPref[InvestPref::KEY_STAT_PERCENT][0]->value;
		$userInvest->save(false);
		
        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        $transaction->to_user_id = $userInvest->user_id;
        $transaction->currency_id = $this->investPref[InvestPref::KEY_CURRENT_CURRENCY][0]->value;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

        $balance = $userInvest->getUser()->one()->getBalances()[$transaction->currency_id];
        $balance->recalculateValue();
	
        echo "\r\n" . 'Начислено участнику: ' . $userInvest->getUser()->one()->login . ' со вклада: ' . $userInvest->id. ' сумма: ' . $sum . "\r\n";
	}

	// приобрести инвест пакет для пользователя
	public function createUserInvest()
	{
		$this->userInvest = new UserInvest;
		$this->userInvest->user_id = $this->user->id;
		$this->userInvest->invest_type_id = $this->investType->id;
		$this->userInvest->currency_id = $this->currency->id;
		$this->userInvest->sum = $this->investType->sum;
		$this->userInvest->paid_sum = 0;
		$this->userInvest->closed_percent = 0;
		$this->userInvest->date_add = date("Y-m-d H:i:s");
		$this->userInvest->date_update = $this->userInvest->date_add;
		$this->userInvest->save();
	}

	// проверка возможности списания с баланса
	public function checkBalance($cost)
	{
		$this->user->recalclulateBalances();

		$balance = $this->user->getBalances()[$this->currency->id];

		if ($balance->value < $cost){
			return false;
		}else{
			return true;
		}
	}

	// списать средства с баланса участника
	public function fromBalance($cost)
	{
		$this->user->recalclulateBalances();

		$balance = $this->user->getBalances()[$this->currency->id];

		if ($balance->value < $cost){
			throw new \Exception(Yii::t('app', 'Не достаточно средств, пожалуйста пополните свой баланс на сумму {cost} {currency} в разделе <a href = "{link}">Финансы</a>', [
				'cost' => $cost,
				'currency' => $this->currency->title,
				'link' => Url::to(['/profile/office/finance']),
			]));
			return;
		}

        $transaction = new Transaction;
        $transaction->from_user_id = $this->user->id;
        $transaction->type = Transaction::TYPE_BUY_INVEST;
        $transaction->sum = $cost;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

		$balance->recalculateValue();

		return true;
	}

	// начилисть средства на баланс пригласителя
	public function toParentBalance($percent, $level)
	{
		$descendants = $this->user->ancestors($level, true)->all();

		if (empty($descendants)){
			throw new \Exception("Parent not found for user", 1);
		}else{
			$parent = array_pop($descendants);
		}

		$cost = ($this->investType->sum / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $cost;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        $transaction->to_user_id = $parent->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

        $balance = $parent->getBalances()[$this->currency->id];

		$balance->recalculateValue();

		return $cost;
	}

	// начислить средства на баланс админа
	public function chargeBalanceFix($percent, $login)
	{
		$user = User::find()->where(['login' => $login])->one();

		$balance = $user->getBalances()[$this->currency->id];

		$sum = ($this->investType->sum / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_ADMIN_BONUS;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

		$balance->recalculateValue();

		return $sum;
	}

	// инициализация матричных настроек
	private function initInvestPref()
	{
		foreach(InvestPref::find()->where(['invest_type_id' => $this->investType->id])->all() as $investPref){
			$this->investPref[$investPref->key][] = $investPref;
		};

		return $this;
	}

}
?>