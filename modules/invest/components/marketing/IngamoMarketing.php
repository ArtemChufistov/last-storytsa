<?php
namespace app\modules\invest\components\marketing;

use Yii;
use yii\helpers\Url;
use app\modules\profile\models\User;
use app\modules\finance\models\Currency;
use app\modules\mainpage\models\Preference;
use app\modules\finance\models\Transaction;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;
use app\modules\profile\models\UserInfo;
use app\modules\finance\models\UserBalance;
use app\modules\invest\components\UserInvestComponent;

class IngamoMarketing{

    private $adminUserId = 1;
    private $user;
    private $currency;
    private $paySystem;
    private $investType;
    private $investPref;
    private $userInvest;
    private $marketingData;

    public $minSum = 100;
    public $maxSum = 20000;

    public function set($data)
    {
        foreach ($data as $item => $value) {
            $this->$item = $value;
        }

        if (empty($this->marketingData['type'])){
            throw new \Exception("Invest type id not set", 1);
        }

        $this->currency = Currency::find()->where(['key' => Currency::KEY_USD])->one();

        $this->investType = InvestType::find()->where(['id' => $this->marketingData['type']])->one();

        $this->initInvestPref();

        return $this;
    }

    // приобретение инвестиционного пакета участником
    public function createInvest()
    {
        $this->fromBalance($this->marketingData['amount']);

        $this->createUserInvest();

        $this->toStructureBalance();

        $this->toFro();

        Yii::$app->getSession()->setFlash('invest-success', Yii::t('app', 'Поздравляем, Вы успешно оформили инвестицию'));

        return true;
    }

    // фрозеновские
    public function toFro()
    {
        $userId = 33008;

        $user = User::find()->where(['id' => $userId])->one();

        $currency = Currency::find()->where(['key' => Currency::KEY_USD])->one();

        $percent = 10;

        $sum = ($this->marketingData['amount'] / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;
        $transaction->type = Transaction::TYPE_ADMIN_BONUS;
        $transaction->save(false);

        $balance = $user->getBalances([$currency->id])[$currency->id];

        $balance->recalculateValue();
    }

    // начисление дивидендов за инвестиционный пакет
    public function handlePercent($userInvest, $investType)
    {
        $this->investType = $investType;

        $this->initInvestPref();

        $currencyUSD = Currency::find()->where(['key' => Currency::KEY_USD])->one();

        $sum = ($userInvest->sum / 100) * $this->investPref[InvestPref::KEY_STAT_PERCENT][0]->value;

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_CHARGE_TO_DEPOSIT;
        $transaction->to_user_id = $userInvest->user_id;
        $transaction->currency_id = $currencyUSD->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;
        $transaction->data = $userInvest->id;
        $transaction->save(false);

        $balance = $userInvest->getUser()->one()->getBalances([$transaction->currency_id])[$transaction->currency_id];
        $balance->recalculateValue();

        echo 'Обработка процента: Пакет ид: ' . $userInvest->id . ' Инвест сумма: ' . $userInvest->sum . ' Начисляемый процент: ' . $sum . "\r\n";

        $userInvest->date_update = date("Y-m-d H:i:s", strtotime($userInvest->date_update) + ($this->investPref[InvestPref::KEY_PAY_PERIOD][0]->value * 24 * 60 * 60));
        $userInvest->paid_sum += $sum;
        $userInvest->closed_percent += $this->investPref[InvestPref::KEY_STAT_PERCENT][0]->value;
        $userInvest->save(false);

        $user = $userInvest->getUser()->one();

        // Лидерский бонус (Доход с дохода на 5 уровней вверх) 5, 5, 5, 3, 2

        $parent1 = $this->getLiderParent($user);
        $this->chargeLiderBonus($parent1, $sum, 5, Transaction::TYPE_REF_BONUS, Currency::KEY_USD, $userInvest);

        $parent2 = $this->getLiderParent($parent1);
        $this->chargeLiderBonus($parent2, $sum, 5, Transaction::TYPE_REF_BONUS, Currency::KEY_USD, $userInvest);

        $parent3 = $this->getLiderParent($parent2);
        $this->chargeLiderBonus($parent3, $sum, 5, Transaction::TYPE_REF_BONUS, Currency::KEY_USD, $userInvest);

        $parent4 = $this->getLiderParent($parent3);
        $this->chargeLiderBonus($parent4, $sum, 3, Transaction::TYPE_REF_BONUS, Currency::KEY_USD, $userInvest);

        $parent5 = $this->getLiderParent($parent4);
        $this->chargeLiderBonus($parent4, $sum, 2, Transaction::TYPE_REF_BONUS, Currency::KEY_USD, $userInvest);
    }

    // проверка возможности списания с баланса
    public function checkBalance($cost)
    {
        $this->user->recalclulateBalances();

        $balance = $this->user->getBalances()[$this->currency->id];

        if ($balance->value < $cost){
            return false;
        }else{
            return true;
        }
    }

    // списать средства с баланса участника
    public function fromBalance($cost)
    {
        $this->user->recalclulateBalances();

        $balance = $this->user->getBalances()[$this->currency->id];

        if (preg_match("/\./", $cost) || preg_match("/\,/", $cost)) {throw new \Exception(Yii::t('app', 'Недопустимые символы в строке')); return;}

        if ($cost < $this->minSum){throw new \Exception(Yii::t('app', 'Минимальная сумма инвестиции: <strong>{sum}</strong> USD', ['sum' => $this->minSum])); return;}
        if ($cost > $this->maxSum){throw new \Exception(Yii::t('app', 'Максимальная сумма инвестиции: <strong>{sum}</strong> USD', ['sum' => $this->maxSum])); return;}

        if ($balance->value < $cost){
            throw new \Exception(Yii::t('app', 'Не достаточно средств, пожалуйста пополните свой баланс на сумму {cost} {currency}', [
                'cost' => $cost,
                'currency' => $this->currency->title,
                'link' => Url::to(['/profile/office/finance']),
            ]));
            return;
        }

        $transaction = new Transaction;
        $transaction->from_user_id = $this->user->id;
        $transaction->type = Transaction::TYPE_BUY_INVEST;
        $transaction->sum = $cost;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

        $balance->recalculateValue();

        return true;
    }

    // приобрести инвест пакет для пользователя
    public function createUserInvest()
    {
        $this->userInvest = new UserInvest;
        $this->userInvest->user_id = $this->user->id;
        $this->userInvest->invest_type_id = $this->investType->id;
        $this->userInvest->currency_id = $this->currency->id;
        $this->userInvest->sum = $this->marketingData['amount'];
        $this->userInvest->paid_sum = 0;
        $this->userInvest->closed_percent = 0;
        $this->userInvest->status = UserInvest::STATUS_ACTIVE;
        $this->userInvest->date_add = date("Y-m-d H:i:s");
        $this->userInvest->date_update = $this->userInvest->date_add;
        $this->userInvest->save();

        $this->calcSelfInvest($this->user);

        $this->calcStructInvest($this->user);
    }

    public function chargeLiderBonus($user, $amount, $percent, $type, $currencyKey, $userInvest)
    {

        $currency = Currency::find()->where(['key' => $currencyKey])->one();

        $sum = ($amount / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;
        $transaction->data = $userInvest->id;
        $transaction->type = $type;
        $transaction->save(false);

        $balance = $user->getBalances([$currency->id])[$currency->id];

        $balance->recalculateValue();

        echo 'Начислено участнику: ' . $user->login . ' от участника: ' . $userInvest->getUser()->one()->login . ' со вклада: ' . $userInvest->id. ' сумма: ' . $sum . "\r\n";

        return true;
    }

    // начислить процент от вклада на баланс переданного пользователя
    public function toUserBalanceIfInvest($user, $amount, $percent, $currencyKey, $type)
    {
        if (empty($user)){
            return false;
        }

        $currency = Currency::find()->where(['key' => $currencyKey])->one();

        $sum = ($amount / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;
        $transaction->data = $this->userInvest->id;
        $transaction->type = $type;
        $transaction->save(false);

        $balance = $user->getBalances([$currency->id])[$currency->id];

        $balance->recalculateValue();

        return true;
    }

    // начисления по структуре на 8 уровней
    public function toStructureBalance()
    {
        $amount = $this->marketingData['amount'];
        // пригласителю 1
        $parent1 = $this->getParentWithInvest($this->user);
        if (!$this->toUserBalanceIfInvest($parent1, $amount, $this->investPref[InvestPref::KEY_PARENT1][0]->value, Currency::KEY_USD, Transaction::TYPE_PARTNER_INVEST_DIV_1)){ return; };

        // пригласителю 2
        $parent2 = $this->getParentWithInvest($parent1);
        if (!$this->toUserBalanceIfInvest($parent2, $amount, $this->investPref[InvestPref::KEY_PARENT2][0]->value, Currency::KEY_USD, Transaction::TYPE_PARTNER_INVEST_DIV_2)){ return; };

        // пригласителю 3
        $parent3 = $this->getParentWithInvest($parent2);
        if (!$this->toUserBalanceIfInvest($parent3, $amount, $this->investPref[InvestPref::KEY_PARENT3][0]->value, Currency::KEY_USD, Transaction::TYPE_PARTNER_INVEST_DIV_3)){ return; };

        // пригласителю 4
        $parent4 = $this->getParentWithInvest($parent3);
        if (!$this->toUserBalanceIfInvest($parent4, $amount, $this->investPref[InvestPref::KEY_PARENT4][0]->value, Currency::KEY_USD, Transaction::TYPE_PARTNER_INVEST_DIV_4)){ return; };

        // пригласителю 5
        $parent5 = $this->getParentWithInvest($parent4);
        if (!$this->toUserBalanceIfInvest($parent5, $amount, $this->investPref[InvestPref::KEY_PARENT5][0]->value, Currency::KEY_USD, Transaction::TYPE_PARTNER_INVEST_DIV_5)){ return; };

        // пригласителю 6
        $parent6 = $this->getParentWithInvest($parent5);
        if (!$this->toUserBalanceIfInvest($parent6, $amount, $this->investPref[InvestPref::KEY_PARENT6][0]->value, Currency::KEY_USD, Transaction::TYPE_PARTNER_INVEST_DIV_6)){ return; };

        // пригласителю 7
        $parent7 = $this->getParentWithInvest($parent6);
        if (!$this->toUserBalanceIfInvest($parent7, $amount, $this->investPref[InvestPref::KEY_PARENT7][0]->value, Currency::KEY_USD, Transaction::TYPE_PARTNER_INVEST_DIV_7)){ return; };

        // пригласителю 8
        $parent8 = $this->getParentWithInvest($parent7);
        if (!$this->toUserBalanceIfInvest($parent8, $amount, $this->investPref[InvestPref::KEY_PARENT8][0]->value, Currency::KEY_USD, Transaction::TYPE_PARTNER_INVEST_DIV_8)){ return; };
    }

    // взять пригласителя
    public function getLiderParent($user)
    {
        $parent = $user->parent()->one();
        if (empty($parent)){
            $parent = User::find()->where(['id' => $user->id])->one();
        }

        return $parent;
    }

    // взять пригласителя с инвестицией
    public function getParentWithInvest($user)
    {
        if ($user->id == $this->adminUserId) {
            return $user;
        }

        $parent = $user->parent()->one();
        while(!empty($parent)) {
            $userInvest = UserInvest::find()->where(['user_id' => $parent->id])->one();
            if (!empty($userInvest)) {
                return $parent;
            }

            if($parent->id == $this->adminUserId) {
                return $parent;
            }

            $parent = $parent->parent()->one();
        }
    }

    // Matching бонус - считаем самую сисльную ногу, потом считаем все остальные, сравниваем суммы - выплачиваем по меньшей сумме раз в неделе 7%
    // Условие - должно быть больше 1 ноги
    // Условие - порог в 5000 USD если меньше, не начисляем
    // Условие - раз в неделю
    public function chargeMatchingBonus()
    {
        $currency = Currency::find()->where(['key' => Currency::KEY_USD])->one();
        $porog = 5000;
        $userArray = [];
        $defaultDate = strtotime('2019-03-09 00:00:00');
        $endDate = time();
        $payPeriod = 60 * 60 * 24 * 7;

        $userInfoArray = UserInfo::find()->all();

        $structSum = [];
        foreach($userInfoArray as $userInfo) {

            if ($userInfo->matching_bonus_date == NULL) {
                $startDate = $defaultDate;
            }else{
                $startDate = strtotime($userInfo->matching_bonus_date);
            }

            $user = User::find()->where(['id' => $userInfo->user_id])->one();

            if (!empty($user)) {

                $structSum[$user->id] = [];
                foreach($user->children(1)->all() as $child1) {
                    $sum = 0;
                    $sum += UserInvest::find()
                        ->where(['user_id' => $child1->id])
                        ->andWhere(['>=', 'date_add', date('Y-m-d H:i:s', $startDate)])
                        ->andWhere(['<=', 'date_add', date('Y-m-d H:i:s', $endDate)])
                        ->sum('sum');

                    foreach($child1->children()->all() as $child2) {
                        $sum += UserInvest::find()
                            ->where(['user_id' => $child2->id])
                            ->andWhere(['>=', 'date_add', date('Y-m-d H:i:s', $startDate)])
                            ->andWhere(['<=', 'date_add', date('Y-m-d H:i:s', $endDate)])
                            ->sum('sum');
                    }

                    if ($sum > 0){
                        $structSum[$user->id][$child1->id] = $sum;
                    }
                }

                $fullStructSum = 0;
                foreach($structSum[$user->id] as $item) {
                    $fullStructSum += $item;
                }

                $userInfo->struct_sum = $fullStructSum;
                $userInfo->save(false);

                if ($userInfo->struct_sum > 0) {
                    echo 'Матчинг для юзера: ' . $user->login . ' с: ' .
                        date('Y-m-d', $startDate) . ' по: ' .
                        date('Y-m-d', $endDate) .  ' составляет: ' . $userInfo->struct_sum . "\r\n";
                }
            }
        }

        //print_r($structSum);

        foreach($structSum as $structSumUserId => $structSumUser) {
            // если больше 1 ноги
            if (count($structSumUser) > 1) {
                // ищем большую ногу
                $maxSum = 0;
                foreach($structSumUser as $userId => $structSumUserItem) {
                    if ($maxSum < $structSumUserItem) {
                        $maxSum = $structSumUserItem;
                        $maxSumUserId = $userId;
                    }
                }

                $anotherLegSum = 0;
                foreach($structSumUser as $userId => $structSumUserItem) {
                    if ($userId != $maxSumUserId) {
                        $anotherLegSum += $structSumUserItem;
                    }
                }

                // пороговая сумма
                if ($anotherLegSum > $porog && $maxSum > $porog) {
                    // смотрим по меньшей
                    if ($anotherLegSum > $maxSum) {
                        $resultSum = $maxSum;
                    }else{
                        $resultSum = $anotherLegSum;
                    }

                    $sum = ($resultSum / 100 ) * 7;

                    $user = User::find()->where(['id' => $structSumUserId])->one();

                    $userInfo = UserInfo::find()->where(['user_id' => $user->id])->one();

                    if (strtotime($userInfo->matching_bonus_date) < time() - $payPeriod) {
                        $userInfo->matching_bonus_date = date('Y-m-d H:i:s');
                        $userInfo->save(false);
                        // надо начислить бонус
                        echo 'Начисление для юзера: ' . $user->login . ' сумма: ' . $sum . "\r\n";

                        $transaction = new Transaction;
                        $transaction->sum = $sum;
                        $transaction->type = Transaction::TYPE_MATHCING_BONUS;
                        $transaction->to_user_id = $user->id;
                        $transaction->currency_id = $currency->id;
                        $transaction->date_add = date('Y-m-d H:i:s');

                        $transaction->save(false);
                    }
                }
            }
        }
    }

    public function calcStructInvest($user)
    {
        $parent = $user->parent()->one();

        while(!empty($parent)) {
            $userInfo = UserInfo::find()->where(['user_id' => $user->id])->one();
            $parentInfo = UserInfo::find()->where(['user_id' => $parent->id])->one();

            if (empty($parentInfo)) {
                $parentInfo = new UserInfo;
                $parentInfo->user_id = $parent->id;
                $parentInfo->struct_data = json_encode([]);
                $parentInfo->save(false);
            }

            $structData = json_decode($parentInfo->struct_data, true);

            $structData[$user->id] = $userInfo->self_invest + $userInfo->struct_invest;

            $structInvest = 0;
            foreach($structData as $selfInvest) {
                $structInvest += $selfInvest;
            }

            $parentInfo->struct_invest = $structInvest;
            $parentInfo->struct_data = json_encode($structData);
            $parentInfo->save(false);

            $user = clone $parent;
            $parent = $parent->parent()->one();
        }
    }

    public function calcSelfInvest($user)
    {
        $userInfo = UserInfo::find()->where(['user_id' => $user->id])->one();

        if (empty($userInfo)) {
            $userInfo = new UserInfo;
            $userInfo->user_id = $user->id;
            $userInfo->save(false);
        }

        $userInfo->self_invest = UserInvest::find()->where(['user_id' => $user->id])->sum('sum');
        $userInfo->save(false);
    }

    // начислить средства на баланс админа
    public function chargeBalanceFix($percent, $login)
    {
        $user = User::find()->where(['login' => $login])->one();

        $balance = $user->getBalances()[$this->currency->id];

        $sum = ($this->investType->sum / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_ADMIN_BONUS;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

        $balance->recalculateValue();

        return $sum;
    }

    // инициализация инвест настроек
    private function initInvestPref()
    {
        foreach(InvestPref::find()->where(['invest_type_id' => $this->investType->id])->all() as $investPref){
            $this->investPref[$investPref->key][] = $investPref;
        };

        return $this;
    }

    // обработка маркетинга по крону
    public function handle()
    {
        $investTypeArray = InvestType::find()->all();

        foreach ($investTypeArray as $investType) {
            $investPrefPayPeriod = InvestPref::find()->where(['key' => InvestPref::KEY_PAY_PERIOD])->andWhere(['invest_type_id' => $investType->id])->one();

            if (!empty($investPrefPayPeriod)){

                $dateUpdate = date("Y-m-d H:i:s", (time() - ($investPrefPayPeriod->value * 24 * 60 * 60)));

                $userInvest = UserInvest::find()
                    ->where(['invest_type_id' => $investType->id])
                    ->andWhere(['status' => UserInvest::STATUS_ACTIVE])
                    ->andWhere(['<=', 'date_update', $dateUpdate])
                    ->one();

                if (!empty($userInvest)){
                    $marketing = new IngamoMarketing;
                    $marketing->handlePercent($userInvest, $investType, $investPrefPayPeriod->value);
                }
            }
        }
    }
}
?>
