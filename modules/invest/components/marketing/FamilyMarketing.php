<?php
namespace app\modules\invest\components\marketing;

use Yii;
use yii\helpers\Url;
use app\modules\profile\models\User;
use app\modules\finance\models\Currency;
use app\modules\mainpage\models\Preference;
use app\modules\finance\models\Transaction;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;
use app\modules\finance\models\UserBalance;
use app\modules\invest\components\UserInvestComponent;

class FamilyMarketing{

	private $user;
	private $currency;
	private $paySystem;
	private $investType;
	private $investPref;
	private $userInvest;
	private $marketingData;

	public $minSum = 100;
	public $maxSum = 20000;

	public function set($data)
	{
		foreach ($data as $item => $value) {
			$this->$item = $value;
		}

		if (empty($this->marketingData['type'])){
			throw new \Exception("Invest type id not set", 1);
		}

		$this->currency = Currency::find()->where(['key' => Currency::KEY_USD])->one();

		$this->investType = InvestType::find()->where(['id' => $this->marketingData['type']])->one();

		$this->initInvestPref();

		return $this;
	}

	// приобретение инвестиционного пакета участником
	public function handleInvest()
	{
		$this->fromBalance($this->marketingData['amount']);

		$this->createUserInvest();

		$this->toStructureBalance();

		return true;
	}

	// начисление дивидендов на инвестиционный пакет
	public function handlePercent($userInvest, $investType)
	{
		$this->investType = $investType;

		$this->initInvestPref();

		$currencyFreez = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();
		$currencyDic = Currency::find()->where(['key' => Currency::KEY_DIO])->one();

		$sum = ($userInvest->sum / 100) * $this->investPref[InvestPref::KEY_STAT_PERCENT][0]->value;

		$mainSum = ($sum / 100) * 95;
		$cryptoSum = $sum - $mainSum;

		$transaction = new Transaction;
		$transaction->sum = $cryptoSum / $currencyDic->course_to_usd;
		$transaction->course = $currencyDic->course_to_usd;
		$transaction->type = Transaction::TYPE_CHARGE_TO_DEPOSIT;
		$transaction->to_user_id = $userInvest->user_id;
		$transaction->currency_id = $currencyDic->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;
        $transaction->data = $userInvest->id;
        $transaction->save(false);

        $balance = $userInvest->getUser()->one()->getBalances([$transaction->currency_id])[$transaction->currency_id];
        $balance->recalculateValue();

		echo 'Обработка процента: Пакет ид: ' . $userInvest->id . ' Инвест сумма: ' . $userInvest->sum . ' Начисляемый процент: ' . $mainSum . "\r\n";

		$userInvest->date_update = date("Y-m-d H:i:s", strtotime($userInvest->date_update) + $this->investPref[InvestPref::KEY_PAY_PERIOD][0]->value);
		$userInvest->paid_sum += $sum;
		$userInvest->closed_percent += $this->investPref[InvestPref::KEY_STAT_PERCENT][0]->value;
		$userInvest->save(false);
		
        $transaction = new Transaction;
        $transaction->sum = $mainSum;
        $transaction->type = Transaction::TYPE_INVEST_PERCENT;
        $transaction->to_user_id = $userInvest->user_id;
        $transaction->currency_id = $currencyFreez->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;
        $transaction->data = $userInvest->id;

        $transaction->save(false);

        $balance = $userInvest->getUser()->one()->getBalances([$transaction->currency_id])[$transaction->currency_id];
        $balance->recalculateValue();
	
        echo 'Начислено участнику: ' . $userInvest->getUser()->one()->login . ' со вклада: ' . $userInvest->id. ' сумма: ' . $mainSum . "\r\n";
	}

	// приобрести инвест пакет для пользователя
	public function createUserInvest()
	{
		$this->userInvest = new UserInvest;
		$this->userInvest->user_id = $this->user->id;
		$this->userInvest->invest_type_id = $this->investType->id;
		$this->userInvest->currency_id = $this->currency->id;
		$this->userInvest->sum = $this->marketingData['amount'];

		if (!empty($this->marketingData['returnType'])){
			$this->userInvest->returnType = UserInvest::RETURN_TYPE_RETURN14;
		}else{
			$this->userInvest->returnType = UserInvest::RETURN_TYPE_NORETURN;
		}

		$this->userInvest->paid_sum = 0;
		$this->userInvest->closed_percent = 0;
		$this->userInvest->status = UserInvest::STATUS_ACTIVE;
		$this->userInvest->date_add = date("Y-m-d H:i:s");
		$this->userInvest->date_update = $this->userInvest->date_add;
		$this->userInvest->save();

		UserInvestComponent::recalcUserInvest($this->user, InvestPref::KEY_SIMPLE_INVEST);
	}

	// проверка возможности списания с баланса
	public function checkBalance($cost)
	{
		$this->user->recalclulateBalances();

		$balance = $this->user->getBalances()[$this->currency->id];

		if ($balance->value < $cost){
			return false;
		}else{
			return true;
		}
	}

	// списать средства с баланса участника
	public function fromBalance($cost)
	{
		$this->user->recalclulateBalances();

		$balance = $this->user->getBalances()[$this->currency->id];

		if (preg_match("/\./", $cost) || preg_match("/\,/", $cost)) {throw new \Exception(Yii::t('app', 'Недопустимые символы в строке')); return;}

		if ($cost < $this->minSum){throw new \Exception(Yii::t('app', 'Минимальная сумма инвестиции: <strong>{sum}</strong> USD', ['sum' => $this->minSum])); return;}
		if ($cost > $this->maxSum){throw new \Exception(Yii::t('app', 'Максимальная сумма инвестиции: <strong>{sum}</strong> USD', ['sum' => $this->maxSum])); return;}

		if ($balance->value < $cost){
			throw new \Exception(Yii::t('app', 'Не достаточно средств, пожалуйста пополните свой баланс на сумму {cost} {currency} в разделе <a href = "{link}">Финансы</a>', [
				'cost' => $cost,
				'currency' => $this->currency->title,
				'link' => Url::to(['/profile/office/finance']),
			]));
			return;
		}

        $transaction = new Transaction;
        $transaction->from_user_id = $this->user->id;
        $transaction->type = Transaction::TYPE_BUY_INVEST;
        $transaction->sum = $cost;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

		$balance->recalculateValue();

		return true;
	}

	// начислить процент от вклада на баланс переданного пользователя
	public function toUserBalanceIfInvest($user, $amount, $percent, $currencyKey)
	{
		if (empty($user)){
			return false;
		}

		if ($currencyKey == Currency::KEY_DEPOZIT){

			$investPrefArray = InvestPref::find()->where(['key' => InvestPref::KEY_SIMPLE_INVEST])->all();

			$simpleInvestIds = [];
			foreach ($investPrefArray as $investPref) {
			    $simpleInvestIds[$investPref->invest_type_id] = $investPref->invest_type_id;
			}

			$userInvest = UserInvest::find()
				->where(['user_id' => $user->id])
				->andWhere(['invest_type_id' => $simpleInvestIds])
				->andWhere(['status' => UserInvest::STATUS_ACTIVE])
				->one();
			if (empty($userInvest)){
				return true;
			}
		}

		$currency = Currency::find()->where(['key' => $currencyKey])->one();

		$sum = ($amount / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $sum;

        if ($currencyKey == Currency::KEY_DEPOZIT){
        	$transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE_TO_DEPOSIT;
        }elseif($currencyKey == Currency::KEY_FREEZ24){
        	$transaction->type = Transaction::TYPE_REF_BONUS;
        }else{
        	$transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        }

        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;
        $transaction->data = $this->userInvest->id;

        $transaction->save(false);

        $balance = $user->getBalances([$currency->id])[$currency->id];

		$balance->recalculateValue();

		return true;
	}

	// начисления по структуре на 12 уровней
	public function toStructureBalance()
	{
		$amount = $this->marketingData['amount'];

		// пригласителю 1
		$parent1 = $this->user->parent()->one();

		if (empty($parent1)){
			return;
		}

		
		$investPrefArray = InvestPref::find()->where(['key' => InvestPref::KEY_SIMPLE_INVEST])->all();

		$simpleInvestIds = [];
		foreach ($investPrefArray as $investPref) {
		    $simpleInvestIds[$investPref->invest_type_id] = $investPref->invest_type_id;
		}

		$investTypeArray = UserInvest::find()->where(['user_id' => $parent1->id])
			->andWhere(['status' => UserInvest::STATUS_ACTIVE])
			->andWhere(['invest_type_id' => $simpleInvestIds])
			->all();

		$investParentSum = 0;
		foreach($investTypeArray as $investType){
			$investParentSum += $investType->sum;
		}

		if ($investParentSum > 10000){
			$percent = 10;
		}else{
			$percent = 8;
		}

		// пригласителю 1
		if (!$this->toUserBalanceIfInvest($parent1, $amount, $percent, Currency::KEY_FREEZ24)){ return; };

		// пригласителю 2
		$parent2 = $parent1->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent2, $amount, $this->investPref[InvestPref::KEY_PARENT2][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 3
		$parent3 = $parent2->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent3, $amount, $this->investPref[InvestPref::KEY_PARENT3][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 4
		$parent4 = $parent3->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent4, $amount, $this->investPref[InvestPref::KEY_PARENT4][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 5
		$parent5 = $parent4->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent5, $amount, $this->investPref[InvestPref::KEY_PARENT5][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 6
		$parent6 = $parent5->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent6, $amount, $this->investPref[InvestPref::KEY_PARENT6][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 7
		$parent7 = $parent6->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent7, $amount, $this->investPref[InvestPref::KEY_PARENT7][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 8
		$parent8 = $parent7->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent8, $amount, $this->investPref[InvestPref::KEY_PARENT8][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 9
		$parent9 = $parent8->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent9, $amount, $this->investPref[InvestPref::KEY_PARENT9][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 10
		$parent10 = $parent9->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent10, $amount, $this->investPref[InvestPref::KEY_PARENT10][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 11
		$parent11 = $parent10->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent11, $amount, $this->investPref[InvestPref::KEY_PARENT11][0]->value, Currency::KEY_DEPOZIT)){ return; };

		// пригласителю 12
		$parent12 = $parent11->parent()->one();
		if (!$this->toUserBalanceIfInvest($parent12, $amount, $this->investPref[InvestPref::KEY_PARENT12][0]->value, Currency::KEY_DEPOZIT)){ return; };
	}

	// закрыть пользовательский инвест. пакет
	public static function closeUserInvest()
	{
        $investTypeArray = InvestType::find()->all();

        foreach ($investTypeArray as $investType) {
            $investPeriod = InvestPref::find()->where(['key' => InvestPref::KEY_INVEST_PERIOD])->andWhere(['invest_type_id' => $investType->id])->one();
            
            if (!empty($investPeriod)){
                $userInvest = UserInvest::find()
                	->where(['invest_type_id' => $investType->id])
                	->andWhere(['status' => UserInvest::STATUS_ACTIVE])
                	->andWhere(['<=', 'date_add', date("Y-m-d H:i:s", time() - $investPeriod->value)])
                	->one();

                if (!empty($userInvest)){
                	// toDo начислить тело инвест пакета на ти дневыней замороженный счет
                    $userInvest->status = UserInvest::STATUS_CLOSED;
                    $userInvest->save(false);
                }
            }
        }
	}

	// закрыть инвестицию, превести средства на пользователський счёт
	public function cancelInvest($userInvest)
	{
		if ($userInvest->returnType != UserInvest::RETURN_TYPE_RETURN14){
			throw new Exception(Yii::t('app', 'Нельзя отменить невозвратный вклад'));
			return;
		}

		$sum = $userInvest->sum - ($userInvest->sum / 100) * 27;

		$user = User::find()->where(['id' => $userInvest->user_id])->one();

        $userInvest->status = UserInvest::STATUS_CANCEL;
        $userInvest->save(false);

		$currencyFreez = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_RETURN_FROM_INVEST;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $currencyFreez->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->data = $userInvest->id;

        $transaction->save(false);

		$balance = $user->getBalances([$transaction->currency_id])[$transaction->currency_id];

		$balance->recalculateValue();
	}

	// начислить средства на баланс админа
	public function chargeBalanceFix($percent, $login)
	{
		$user = User::find()->where(['login' => $login])->one();

		$balance = $user->getBalances()[$this->currency->id];

		$sum = ($this->investType->sum / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_ADMIN_BONUS;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

		$balance->recalculateValue();

		return $sum;
	}

	// инициализация инвест настроек
	private function initInvestPref()
	{
		foreach(InvestPref::find()->where(['invest_type_id' => $this->investType->id])->all() as $investPref){
			$this->investPref[$investPref->key][] = $investPref;
		};

		return $this;
	}

	// обработка маркетинга по крону
	public function handle()
	{
        $investTypeArray = InvestType::find()->all();

        foreach ($investTypeArray as $investType) {
            $investPrefPayPeriod = InvestPref::find()->where(['key' => InvestPref::KEY_PAY_PERIOD])->andWhere(['invest_type_id' => $investType->id])->one();
            
            if (!empty($investPrefPayPeriod)){
                $userInvest = UserInvest::find()
                	->where(['invest_type_id' => $investType->id])
                	->andWhere(['status' => UserInvest::STATUS_ACTIVE])
                	->andWhere(['<=', 'date_update', date("Y-m-d H:i:s", time() - $investPrefPayPeriod->value)])
                	->one();

                if (!empty($userInvest)){
                    $this->handlePercent($userInvest, $investType, $investPrefPayPeriod->value);
                }
            }
        }
	}

	// создание инвестиционного депозита по крону (минимальное накопление на депозитном кошельке 100 USD)
	public static function createDepositInvest()
	{
		$nowTime = time();

		$timePeriod = 60 * 60 * 24 * 10;

		$minDepositSum = 100;

		$currencyDeposit = Currency::find()->where(['key' => Currency::KEY_DEPOZIT])->one();

		$userBalanceArray = UserBalance::find()->where(['currency_id' => $currencyDeposit->id])->andWhere(['>=','value', $minDepositSum])->all();

		foreach($userBalanceArray as $userBalance){

			$userBalance->recalculateValue();

			if ($userBalance->value <= 0){
				echo 'Неправильный депозитный баланс у пользователя: ' . $userBalance->getUser()->one()->login . "\r\n";
				continue;
			}

			// берём последнее списание
			$transaction = Transaction::find()
				->where(['from_user_id' => $userBalance->user_id])
				->andWhere(['currency_id' => $currencyDeposit->id])
				->orderBy(['date_add' => SORT_DESC])
				->one();

			// если уже были списания
			if (!empty($transaction)){
				// если не прошло timePeriod времени просто ждём
				if (strtotime($transaction->date_add) > ($nowTime - $timePeriod)){
					continue;
				}

				$plusTransaction = Transaction::find()
					->where(['user_id' => $userBalance->user_id])
					->andWhere(['currency_id' => $currencyDeposit->id])
					->andWhere(['<=', 'date_add', date('Y-m-d H:i:s', ($nowTime - $timePeriod))])
					->andWhere(['>=', 'date_add', strtotime($transaction->date_add)])
					->one();

				if (empty($plusTransaction)){
					continue;
				}
			}else{
				$transaction = Transaction::find()
					->where(['to_user_id' => $userBalance->user_id])
					->andWhere(['currency_id' => $currencyDeposit->id])
					->orderBy(['date_add' => SORT_ASC])
					->one();

				if (strtotime($transaction->date_add) > ($nowTime - $timePeriod)){
					continue;
				}
			}

			// создаём депозитный вклад
			$investPrefDeposit = InvestPref::find()->where(['key' => InvestPref::KEY_DEPOSIT_INVEST])->one();

			echo 'Создание депозита в: ' . date('Y-m-d H:i:s', $nowTime) . ' для пользователя: ' . $userBalance->getUser()->one()->login . ' сумма:' . $userBalance->value . "\r\n";

			$userInvest = new UserInvest;
			$userInvest->user_id = $userBalance->user_id;
			$userInvest->invest_type_id = $investPrefDeposit->invest_type_id;
			$userInvest->currency_id = $currencyDeposit->id;
			$userInvest->sum = $userBalance->value;
			$userInvest->returnType = UserInvest::RETURN_TYPE_NORETURN;
			$userInvest->paid_sum = 0;
			$userInvest->closed_percent = 0;
			$userInvest->status = UserInvest::STATUS_ACTIVE;
			$userInvest->date_add = date("Y-m-d H:i:s");
			$userInvest->date_update = $userInvest->date_add;
			$userInvest->save(false);
			
			// списываем средства
	        $transaction = new Transaction;
	        $transaction->sum = $userBalance->value;
	        $transaction->type = Transaction::TYPE_CREATE_DEPOSIT_INVEST;
	        $transaction->from_user_id = $userBalance->user_id;
	        $transaction->currency_id = $currencyDeposit->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->invest_type_id = $investPrefDeposit->invest_type_id;

    		$transaction->save(false);

    		$userBalance->recalculateValue();
		}
	}

	// Перевод средств на основной баланс с задержкой 24 часа
	public static function from24ToMainBalance()
	{
		$nowTime = time();

		$timePeriod = 60 * 60 * 24;

		$currencyFreez24 = Currency::find()->where(['key' => Currency::KEY_FREEZ24])->one();

		$userBalanceArray = UserBalance::find()->where(['currency_id' => $currencyFreez24->id])->andWhere(['>','value', '0'])->all();

		foreach($userBalanceArray as $userBalance){

			$transaction = Transaction::find()
				->where(['from_user_id' => $userBalance->user_id])
				->andWhere(['currency_id' => $currencyFreez24->id])
				->orderBy(['date_add' => SORT_DESC])
				->one();

			if (empty($transaction)){
				$transaction = Transaction::find()
					->where(['to_user_id' => $userBalance->user_id])
					->andWhere(['currency_id' => $currencyFreez24->id])
					->orderBy(['date_add' => SORT_ASC])
					->one();

				if (empty($transaction)){
					echo 'Неправильный 24часовой баланс у пользователя: ' . $userBalance->getUser()->one()->login . "\r\n";
					continue;
				}
			}

			if (strtotime($transaction->date_add) > ($nowTime - $timePeriod)){
				continue;
			}

			$userBalance->recalculateValue();

			// переводим на основной счёт
			if ($userBalance->value > 0){

				echo 'Перевод с 24часового баланса на основной для пользователя: ' . $userBalance->getUser()->one()->login . ' Сумма: ' . $userBalance->value . "\r\n";

				// списываем средства с 24го счёта
		        $transaction = new Transaction;
		        $transaction->sum = $userBalance->value;
		        $transaction->type = Transaction::TYPE_MINUS_REF_BONUS;
		        $transaction->from_user_id = $userBalance->user_id;
		        $transaction->currency_id = $currencyFreez24->id;
		        $transaction->date_add = date('Y-m-d H:i:s');
        		$transaction->save(false);

        		$currencyUsd = Currency::find()->where(['key' => Currency::KEY_USD])->one();

        		// зачисляем на основной счёт
		        $transaction = new Transaction;
		        $transaction->sum = $userBalance->value;
		        $transaction->type = Transaction::TYPE_PLUS_REF_BONUS;
		        $transaction->to_user_id = $userBalance->user_id;
		        $transaction->currency_id = $currencyUsd->id;
		        $transaction->date_add = date('Y-m-d H:i:s');
        		$transaction->save(false);

		        $balanceUsd = $transaction->getToUser()->one()->getBalances([$currencyUsd->id])[$currencyUsd->id];

		        $balanceUsd->recalculateValue();
        		$userBalance->recalculateValue();
			}
		}
	}

	// Перевод средств на основной баланс с задержкой 10 дней
	public static function from10DaysToMainBalance()
	{
		$excludedUserInvestIds = [6, 7];
		$nowTime = time();

		$timePeriod = 60 * 60 * 24 * 10;

		$currencyFreez = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();
		$currencyFreez = Currency::find()->where(['key' => Currency::KEY_FREEZ])->one();

		$userInvestArray = UserInvest::find()->all();

		foreach($userInvestArray as $userInvest){

			$transaction = Transaction::find()
				->where(['from_user_id' => $userInvest->user_id])
				->andWhere(['type' => Transaction::TYPE_MINUS_INVEST_DOHOD])
				->andWhere(['data' => $userInvest->id])
				->andWhere(['>=', 'date_add', date('Y-m-d H:i:s', ($nowTime - $timePeriod))])
				->one();

			if (!empty($transaction)){
				continue;
			};
			
			$transaction = Transaction::find()
				->where(['to_user_id' => $userInvest->user_id])
				->andWhere(['type' => Transaction::TYPE_INVEST_PERCENT])
				->andWhere(['data' => $userInvest->id])
				->andWhere(['not in', 'data', $excludedUserInvestIds])
				->orderBy(['date_add' => SORT_ASC])
				->one();

			if (empty($transaction)){
				continue;
			}

			if (strtotime($transaction->date_add) > ($nowTime - $timePeriod)){
				continue;
			}

			$transactionPlusSum = Transaction::find()
				->select('SUM(sum) as sum')
				->where(['to_user_id' => $userInvest->user_id])
				->andWhere(['currency_id' => $currencyFreez->id])
				->andWhere(['type' => Transaction::TYPE_INVEST_PERCENT])
				->andWhere(['data' => $userInvest->id])
				->andWhere(['not in', 'data', $excludedUserInvestIds])
				->one();

			$transactionMinusSum = Transaction::find()
				->select('SUM(sum) as sum')
				->where(['from_user_id' => $userInvest->user_id])
				->andWhere(['currency_id' => $currencyFreez->id])
				->andWhere(['type' => Transaction::TYPE_MINUS_INVEST_DOHOD])
				->andWhere(['not in', 'data', $excludedUserInvestIds])
				->andWhere(['data' => $userInvest->id])
				->one();

			$sum = $transactionPlusSum->sum - $transactionMinusSum->sum;

			if ($sum <= 0){
				echo 'Проблема с балансом у пользователя: ' . $userInvest->getUser()->one()->login . ' баланс: ' . $sum . "\r\n";
				continue;
			}

			echo 'Перевод с 10днев счёта на основной для депозита: ' . $userInvest->id . ' Пользователь: ' . $userInvest->getUser()->one()->login . ' Сумма: ' . $sum . "\r\n";

			// списываем средства с 10го счёта
	        $transaction = new Transaction;
	        $transaction->sum = $sum;
	        $transaction->type = Transaction::TYPE_MINUS_INVEST_DOHOD;
	        $transaction->from_user_id = $userInvest->user_id;
	        $transaction->currency_id = $currencyFreez->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->data = $userInvest->id;
    		$transaction->save(false);

    		$currencyUsd = Currency::find()->where(['key' => Currency::KEY_USD])->one();

    		// зачисляем на основной счёт
	        $transaction = new Transaction;
	        $transaction->sum = $sum;
	        $transaction->type = Transaction::TYPE_PLUS_INVEST_DOHOD;
	        $transaction->to_user_id = $userInvest->user_id;
	        $transaction->currency_id = $currencyUsd->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->data = $userInvest->id;
    		$transaction->save(false);

	        $balanceUsd = $transaction->getToUser()->one()->getBalances([$currencyUsd->id])[$currencyUsd->id];
	        $balanceFreez = $transaction->getToUser()->one()->getBalances([$currencyFreez->id])[$currencyFreez->id];

	        $balanceUsd->recalculateValue();
    		$balanceFreez->recalculateValue();

		}
	}

	// toDo перевед средств за отменённые ордеры на основной баланс с 10ти дневного

	// toDo первеод средсты за Mathing bonus с 10ти дневного

}
?>