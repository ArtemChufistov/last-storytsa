<?php
namespace app\modules\invest\components\marketing;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\profile\models\User;
use app\modules\finance\models\Currency;
use app\modules\mainpage\models\Preference;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\UserBalance;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;
use app\modules\invest\models\MasterNode;

class LkpoolMarketing{

	private $user;
	private $currency;
	private $investPref;
	public $investType;
	public $userInvest;
	public $investNum;

	public $marketingData;

	public function set($data) {
		foreach ($data as $item => $value) {
			$this->$item = $value;
		}

		if (empty($this->marketingData['invest_type_id'])){
			throw new \Exception("Invest type id not set", 1);
		}

		$this->currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();

		$this->investType = InvestType::find()->where(['id' => $this->marketingData['invest_type_id']])->one();

		if (empty($this->investType)){
			throw new \Exception('Invest not found');
		}


		$this->initInvestPref();

		return $this;
	}

	// приобретение инвестиционного пакета участником
	public function createInvest() {

		if (empty($this->marketingData['closed_percent'])){
			throw new \Exception('Closed percent not found');
		}

		$closedPercent = intval($this->marketingData['closed_percent']);

		if ($closedPercent < 0){
			throw new \Exception('Closed percent more than 0');
		}

		if ($closedPercent == 0){
			throw new \Exception(Yii::t('app', 'Необходимо выбрать количество долей'));
		}

		$currentInvestData = self::getCurrentInvestData($this->investType->id);
		$this->investNum = $currentInvestData['invest_num'];
		$currentPercent = $currentInvestData['closed_percent'];

		if ($closedPercent > (100 - $currentPercent)){
			throw new \Exception(Yii::t('app', 'Укажите меньшее количество долей'));
		}

		$paidSum = ($this->investType->sum / 100) * $closedPercent;

		if (!$this->fromBalance($paidSum)){
			throw new \Exception(Yii::t('app', 'Не достаточно средств, пожалуйста пополните свой баланс на сумму <strong>{cost}</strong> {currency} в разделе <a href = "{link}">Финансы</a>', [
				'cost' => $paidSum,
				'currency' => $this->currency->title,
				'link' => Url::to(['/profile/office/in']),
			]));
		}

		$this->userInvest = new UserInvest;
		$this->userInvest->user_id = $this->user->id;
		$this->userInvest->invest_type_id = $this->investType->id;
		$this->userInvest->currency_id = $this->currency->id;
		$this->userInvest->sum = $this->investType->sum;
		$this->userInvest->paid_sum = $paidSum;
		$this->userInvest->closed_percent = $closedPercent;
		$this->userInvest->invest_num = $this->investNum;
		$this->userInvest->date_add = date("Y-m-d H:i:s");

		$this->userInvest->save(false);

		Yii::$app->getSession()->setFlash('message', Yii::t('app', 'Доля в мастер ноде приобретена<br/> <a style = "font-weight: bold;" href = "/office/viewinvest">Посмотреть свои мастер ноды</a>'));
	}

	public function createMasterNode() {

		$userInvestArray = UserInvest::find()->groupBy(['invest_type_id', 'invest_num'])->all();

		foreach ($userInvestArray as $userInvest) {
			$masterNode = MasterNode::find()
				->where(['invest_type_id' => $userInvest->invest_type_id])
				->andWhere(['invest_num' => $userInvest->invest_num])
				->one();

			if (empty($masterNode)){
				echo 'Создание ноды' . "\r\n";

				$masterNode = new MasterNode;
				$masterNode->invest_num = $userInvest->invest_num;
				$masterNode->invest_type_id = $userInvest->invest_type_id;
				$masterNode->status = MasterNode::STATUS_PROGRESS;
				//$masterNode->save(false);

				echo 'Начисление админских' . "\r\n";
				$this->chargeAdmins();

			}
		}

	}

	// списать средства с баланса участника
	public function fromBalance($cost) {
		$this->user->recalclulateBalances();

		$balance = $this->user->getBalances()[$this->currency->id];

		if ($balance->value < $cost){
			return false;
		}

        $transaction = new Transaction;
        $transaction->from_user_id = $this->user->id;
        $transaction->type = Transaction::TYPE_BUY_INVEST;
        $transaction->sum = $cost;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

		$balance->recalculateValue();

		return true;
	}

	// начилисть средства на баланс пригласителя
	public function toParentBalance($percent, $level) {
		$descendants = $this->user->ancestors($level, true)->all();

		if (empty($descendants)){
			throw new \Exception("Parent not found for user", 1);
		}else{
			$parent = array_pop($descendants);
		}

		$cost = ($this->investType->sum / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $cost;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        $transaction->to_user_id = $parent->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

        $balance = $parent->getBalances()[$this->currency->id];

		$balance->recalculateValue();

		return $cost;
	}

	// начислить средства на баланс админа
	public function chargeAdmins($percent, $login) {
		$user = User::find()->where(['login' => $login])->one();

		$balance = $user->getBalances()[$this->currency->id];

		$sum = ($this->investType->sum / 100) * $percent;

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_ADMIN_BONUS;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->invest_type_id = $this->investType->id;

        $transaction->save(false);

		$balance->recalculateValue();

		return $sum;
	}

	// инициализация инвест настроек
	public function initInvestPref() {
		foreach(InvestPref::find()->where(['invest_type_id' => $this->investType->id])->all() as $investPref){
			$this->investPref[$investPref->key][] = $investPref;
		};

		return $this;
	}

	// взять порядковый номер текущей ноды
	public static function getCurrentInvestNum($investTypeId) {
		$investType = InvestType::find()->where(['id' => $investTypeId])->one();

		$userInvest = UserInvest::find()->where(['invest_type_id' => $investType->id])->orderBy(['invest_num' => SORT_DESC])->one();

		if (empty($userInvest)){
			return 0;
		}else{
			return $userInvest->invest_num;
		}
	}

	// взять максимальное количество процентов для текущей ноды
	public static function getCurrentInvestData($investTypeId) {

		$data = ['closed_percent' => 0, 'invest_num' => 0];

		$investNum = self::getCurrentInvestNum($investTypeId);

		$closedPercent = UserInvest::find()
			->where(['invest_type_id' => $investTypeId])
			->andWhere(['invest_num' => $investNum])
			->sum('closed_percent');

		if (empty($closedPercent)){
			$data['closed_percent'] = 0;
		}else{
			if ($closedPercent == 100){
				$data['closed_percent'] = 0;
				$data['invest_num'] = $investNum + 1;
			}else{
				$data['closed_percent'] = $closedPercent;
				$data['invest_num'] = $investNum;
			}
		}

		return $data;
	}
}
?>