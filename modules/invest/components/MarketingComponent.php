<?php

namespace app\modules\invest\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\invest\models;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;
use app\modules\payment\models\Currency;
use app\modules\mainpage\models\Preference;
use app\modules\invest\components\marketing\Marketing;

class MarketingComponent
{
    public static function run($args = [])
    {

        $prefMarketing = Preference::find()->where(['key' => Preference::KEY_INVEST_MARKETING])->one();

        if (!empty($prefMarketing)){
            $marketing = new $prefMarketing->value;
        }else{
            $marketing = new Marketing;
        }

        $marketing->handle();
    }
}