<?php

namespace app\modules\invest\components;

use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;
use app\modules\profile\models\UserInfo;
use app\modules\profile\models\User;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * 
 */
class UserInvestComponent
{
	// пересчёт инвестиций структуры и личный инвестиций всех участников
	public static function recalcAllStructUserInvest()
	{
		self::recalcAllUserInvest();

		self::recalcAllStructInvest();
	}

	// Пересчёт личных инвестиций всех участников
	public static function recalcAllUserInvest()
	{
		$userArray = User::find()->all();

		foreach($userArray as $user){
			self::recalcUserInvest($user, InvestPref::KEY_SIMPLE_INVEST);
		}
	}

	// пересчёт всех оборотов структуры
	public function recalcAllStructInvest()
	{
		$userArray = User::find()->all();

		foreach($userArray as $user){
			$userIds = ArrayHelper::map($user->descendants()->all(), 'id', 'id');

			$sum = UserInfo::find()->select('SUM(self_invest) as self_invest')->where(['in', 'user_id', $userIds])->one();

			//echo 'Обороты структуры пользователь: ' . $user->login . ' сумма: ' . $sum->self_invest . "\r\n";

			$userData = $user->initUserInfo();
			$userData->struct_invest = $sum->self_invest;
			$userData->save(false);
		}
	}

	// Пересчёт личных инвестиций переданного участника
	public static function recalcUserInvest($user, $investPrefKey)
	{
		$investPrefArray = InvestPref::find()->where(['key' => $investPrefKey])->all();

		$investTypes = [];
		foreach($investPrefArray as $investPref){
			$invest = $investPref->getInvestType()->one();

			if (!empty($invest)){
				$investTypes[$invest->id] = $invest->id;
			}
		}

		$invest = UserInvest::find()->select('SUM(sum) as sum')->where(['user_id' => $user->id])->andWhere(['in', 'invest_type_id', $investTypes])->one();

		if (!empty($invest) && $invest->sum > 0){
			$sum = $invest->sum;
		}else{
			$sum = 0;
		}

		$userData = $user->initUserInfo();

		$userData->self_invest = $sum;
		$userData->save(false);

		return $sum;
	}
}