<?php

namespace app\modules\invest\components;

use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;
use app\modules\profile\models\UserInfo;
use app\modules\profile\models\User;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * 
 */
class ReportcsvComponent
{
	// пересчёт инвестиций структуры и личный инвестиций всех участников
	public static function suhbaWorkStruct()
	{

		$file = '/home/suhba.net/html/web/user_data.csv';

		unlink($file);

		$fp = fopen('/home/suhba.net/html/web/user_data.csv', 'w');

		$userData = [
			'Логин',
			'ФИО',
			'Спонсор',
			'Спонсор ФИО',
			'Баланс',
			'Сумма начисленных реферальных',
			'Cумма начисленного лидерского бонуса',
			'Количество расчитанных долей',
			'Личный оборот',
			'Общий оборот структуры',
			'Боковой оборот',
			'Количество акций',
			'Почта',
			'Телефон',
		];

		fputcsv($fp, $userData);

		$currencyRub = Currency::find()->where(['key' => Currency::KEY_RUR])->one();
		$currencyVou = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();

		$userArray = User::find()->all();
		foreach($userArray as $num => $user){

			$userInfo = $user->initUserInfo();

			$parent = $user->parent()->one();

			$referalSum = Transaction::find()
				->select('SUM(sum) as sum')
				->where(['to_user_id' => $user->id])
				->andWhere(['currency_id' => $currencyRub->id])
				->andWhere(['type' => Transaction::TYPE_PARTNER_BUY_PLACE_TO_DEPOSIT])
				->one();

			$balances = $user->getBalances([$currencyRub->id, $currencyVou->id]);

			$fulChildInvest = 0;
			$mightLegSum = 0;
			$children = [];

			foreach($user->children()->all() as $child){
			  $children[] = $child->id;
			  $childInfo = $child->initUserInfo();

			  if ($mightLegSum < $childInfo->struct_invest){
			    $mightLegSum = $childInfo->struct_invest;
			  }

			  $fulChildInvest += $childInfo->struct_invest;

			}

			$userData = [
				$user->login,
				$user->first_name,
				empty($parent) ? '' : $parent->login,
				empty($parent) ? '' : $parent->first_name,
				$balances[$currencyRub->id]->value,
				$referalSum->sum,
				'0',
				$userInfo->count_parts,
				$userInfo->self_invest,
				$userInfo->struct_invest,
				($userInfo->struct_invest - $mightLegSum),
				$balances[$currencyVou->id]->value,
				$user->email,
				$user->phone,
			];

			fputcsv($fp, $userData);
			echo 'Пользователь обработан: ' . $user->login . "\r\n";
		}

fclose($fp);

	}
}