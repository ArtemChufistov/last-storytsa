<?php

namespace app\modules\invest\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\invest\models\InvestPref;

/**
 * InvestPrefSearch represents the model behind the search form of `app\modules\invest\models\InvestPref`.
 */
class InvestPrefSearch extends InvestPref
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['key', 'value', 'invest_type_id', 'value1', 'value2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvestPref::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'invest_type_id' => $this->invest_type_id,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'value1', $this->value1])
            ->andFilterWhere(['like', 'value2', $this->value2]);

        return $dataProvider;
    }
}
