<?php

namespace app\modules\invest\models;

use Yii;

/**
 * This is the model class for table "invest_type".
 *
 * @property int $id
 * @property string $key
 * @property string $name
 * @property int $percent
 * @property string $sum
 */
class InvestType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['percent', 'text'], 'safe'],
            [['key', 'name', 'sum'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Ключ'),
            'name' => Yii::t('app', 'Название'),
            'percent' => Yii::t('app', 'Процент'),
            'sum' => Yii::t('app', 'Сумма'),
            'text' => Yii::t('app', 'Текст'),
        ];
    }
}
