<?php

namespace app\modules\invest\models\search;

use app\modules\invest\models\DetailMarketing;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class DetailMarketingSearch extends DetailMarketing {

	public $dateFrom;
	public $dateTo;

	/**
	 * @inheritdoc
	 */
    public function rules()
    {
        return [
            [['to_user_id', 'from_user_id', 'currency_id', 'user_invest_id', 'level'], 'integer'],
            [['sum', 'percent'], 'number'],
            [['dateFrom', 'dateTo'], 'safe'],
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = DetailMarketing::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['date' => SORT_DESC]],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'to_user_id' => $this->to_user_id,
		]);

		if (!empty($this->dateFrom)){
			$query->andWhere(['>=', 'date', $this->dateFrom]);
		}

		if (!empty($this->dateTo)){
			$query->andWhere(['<=', 'date', $this->dateTo]);
		}

		return $dataProvider;
	}

	public function sumForPeriod($params){
		$query = DetailMarketing::find();

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'to_user_id' => $this->to_user_id,
		]);

		if (!empty($this->dateFrom)){
			$query->andWhere(['>=', 'date', $this->dateFrom]);
		}

		if (!empty($this->dateTo)){
			$query->andWhere(['<=', 'date', $this->dateTo]);
		}

		$query->select('SUM(sum) as sum');

		return $query->one()->sum;
	}
}
