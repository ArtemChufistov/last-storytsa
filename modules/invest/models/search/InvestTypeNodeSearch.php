<?php

namespace app\modules\invest\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\InvestPref;

class InvestTypeNodeSearch extends InvestType {

	public $change;
	public $volume;
	public $marketcap;
	public $roi;
	public $nodes;
	public $required;
	public $mn_worth;
	public $coins_for_launch;

	/**
	 * @inheritdoc
	 */
    public function rules()
    {
        return [
            [['key', 'name', 'sum', 'change', 'volume', 'marketcap', 'roi', 'nodes', 'required', 'mn_worth', 'coins_for_launch'], 'safe'],
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = InvestTypeNodeSearch::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->select('invest_type.*,
			`invest_pref_change`.`value` AS `change`,
			`invest_pref_volume`.`value` AS `volume`,
			`invest_pref_marketcap`.`value` AS `marketcap`,
			`invest_pref_roi`.`value` AS `roi`,
			`invest_pref_nodes`.`value` AS `nodes`,
			`invest_pref_required`.`value` AS `required`,
			`invest_pref_mn_worth`.`value` AS `mn_worth`,
			`invest_pref_coins_for_launch`.`value` AS `coins_for_launch`,
		');

		$query->leftJoin('invest_pref AS invest_pref_change', '`invest_pref_change`.`invest_type_id` = `invest_type`.`id`');
		$query->leftJoin('invest_pref AS invest_pref_volume', '`invest_pref_volume`.`invest_type_id` = `invest_type`.`id`');
		$query->leftJoin('invest_pref AS invest_pref_marketcap', '`invest_pref_marketcap`.`invest_type_id` = `invest_type`.`id`');
		$query->leftJoin('invest_pref AS invest_pref_roi', '`invest_pref_roi`.`invest_type_id` = `invest_type`.`id`');
		$query->leftJoin('invest_pref AS invest_pref_nodes', '`invest_pref_nodes`.`invest_type_id` = `invest_type`.`id`');
		$query->leftJoin('invest_pref AS invest_pref_required', '`invest_pref_required`.`invest_type_id` = `invest_type`.`id`');
		$query->leftJoin('invest_pref AS invest_pref_mn_worth', '`invest_pref_mn_worth`.`invest_type_id` = `invest_type`.`id`');
		$query->leftJoin('invest_pref AS invest_pref_coins_for_launch', '`invest_pref_coins_for_launch`.`invest_type_id` = `invest_type`.`id`');
		
		$query->where(['invest_pref_change.key' => InvestPref::KEY_CHANGE])
			->andWhere(['invest_pref_volume.key' => InvestPref::KEY_VOLUME])
			->andWhere(['invest_pref_marketcap.key' => InvestPref::KEY_MARKETCAP])
			->andWhere(['invest_pref_roi.key' => InvestPref::KEY_ROI])
			->andWhere(['invest_pref_nodes.key' => InvestPref::KEY_NODES])
			->andWhere(['invest_pref_required.key' => InvestPref::KEY_REQUIRED])
			->andWhere(['invest_pref_mn_worth.key' => InvestPref::KEY_MN_WORTH])
			->andWhere(['invest_pref_coins_for_launch.key' => InvestPref::KEY_COINS_FOR_LAUNCH])
			->createCommand()->getRawSql();
		//  ->all();

		return $dataProvider;
	}
}
