<?php

namespace app\modules\invest\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\invest\models\UserInvest;

/**
 * Поиск среди пользователей
 * Class UserSearch
 * @package lowbase\user\models
 */
class InvestStatSearch extends UserInvest
{
    public $sumPay;
    public $perday;
    public $dateFrom;
    public $dateTo;

    public function rules() {
        return [
            [['dateFrom', 'dateTo'], 'safe'],
        ];
    }

    /**
     * Сценарии
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Наименования дополнительных полей
     * аттрибутов, присущих модели поиска
     * @return array
     */
    public function attributeLabels()
    {
        $label = parent::attributeLabels();
        $label['sumPay'] = Yii::t('app', 'sumPay');
        $label['perday'] = Yii::t('app', 'perday');
        $label['dateFrom'] = Yii::t('app', 'Дата от');
        $label['dateTo'] = Yii::t('app', 'Дата до');

        return $label;
    }

    public function initDateFromTo()
    {
        $time = time();
        if (empty($this->dateFrom)){
            $this->dateFrom = date('Y-m-d 00:00:00', $time - (60*60*24*30));
        }else{
            $this->dateFrom = date('Y-m-d 00:00:00', strtotime($this->dateFrom));
        }

        if (empty($this->dateTo)){
            $this->dateTo = date('Y-m-d 23:59:59', $time);
        }else{
            $this->dateTo = date('Y-m-d 23:59:59', strtotime($this->dateTo));
        }
    }

    /**
     * Создает DataProvider на основе переданных данных
     * @param $params - параметры
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $this->initDateFromTo();

        $query = parent::find();

        $query->select('sum(sum) as sumPay, DATE_FORMAT(date_add,"%Y-%m-%d") as `perday`');

        $query->where(['>=', 'date_add', $this->dateFrom]);

        $query->andWhere(['<=', 'date_add', $this->dateTo]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => ['date_add' => SORT_ASC],
            ),
        ]);

        //$this->load($params);
        $query->groupBy(['perday']);


        return $dataProvider;
    }
}
