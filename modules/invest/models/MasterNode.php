<?php

namespace app\modules\invest\models;

use app\modules\profile\models\User;
use app\modules\invest\models\InvestType;
use Yii;

/**
 * This is the model class for table "master_node".
 *
 */
class MasterNode extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_PROGRESS = 'progress';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_node';
    }

    public function getInvestType()
    {
        return $this->hasOne(InvestType::className(), ['id' => 'invest_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invest_type_id', 'invest_num'], 'integer'],
            [['status', 'data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'invest_type_id' => Yii::t('app', 'Инвест. пакет'),
            'invest_num' => Yii::t('app', 'Номер ноды'),
            'status' => Yii::t('app', 'Статус'),
            'data' => Yii::t('app', 'Данные'),
        ];
    }
}
