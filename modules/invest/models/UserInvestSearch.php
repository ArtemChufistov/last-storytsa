<?php

namespace app\modules\invest\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\invest\models\UserInvest;

/**
 * UserInvestSearch represents the model behind the search form of `app\modules\invest\models\UserInvest`.
 */
class UserInvestSearch extends UserInvest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'invest_type_id', 'currency_id', 'sum'], 'integer'],
            [['paid_sum', 'closed_percent'], 'number'],
            [['date_add', 'date_update'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserInvest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'invest_type_id' => $this->invest_type_id,
            'currency_id' => $this->currency_id,
            'sum' => $this->sum,
            'paid_sum' => $this->paid_sum,
            'closed_percent' => $this->closed_percent,
            'date_add' => $this->date_add,
            'date_update' => $this->date_update,
        ]);

        return $dataProvider;
    }
}
