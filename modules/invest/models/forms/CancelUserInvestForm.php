<?php

namespace app\modules\invest\models\forms;

use app\modules\invest\models\UserInvest;
use yii\base\Model;
use Yii;

class CancelUserInvestForm extends Model {

	public $user_id;
	public $user_invest_type_id;
	public $user_invest_type_datestamp;


	public function attributeLabels() {
		return [
			'user_id' => Yii::t('app', 'Пользователь'),
			'user_invest_type_id' => Yii::t('app', 'Инвестиция'),
		];
	}

	public function rules() {
		return [
			[['user_id', 'user_invest_type_datestamp'], 'required'],
			[['user_invest_type_datestamp'], 'dateStampValidate'],
			[['user_invest_type_id'], 'safe'],
		];
	}

	public function dateStampValidate(){

		$userInvest = UserInvest::find()
			->where(['user_id' => $this->user_id])
			->andWhere(['date_add' => date('Y-m-d H:i:s', $this->user_invest_type_datestamp)])
			->one();

		if (empty($userInvest)){
			$this->addError($attribute, Yii::t('app', 'Инвестиция не найдена'));
			return;
		}

		$this->user_invest_type_id = $userInvest->id;
	}

}
