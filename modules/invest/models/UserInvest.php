<?php

namespace app\modules\invest\models;

use app\modules\profile\models\User;
use app\modules\invest\models\InvestType;
use Yii;

/**
 * This is the model class for table "user_invest".
 *
 * @property int $id
 * @property int $user_id
 * @property int $invest_type_id
 * @property int $currency_id
 * @property int $sum
 * @property double $paid_sum
 * @property double $closed_percent
 * @property string $date_add
 * @property string $date_update
 */
class UserInvest extends \yii\db\ActiveRecord
{
    const RETURN_TYPE_RETURN14 = 'return14';
    const RETURN_TYPE_NORETURN = 'noreturn';

    const STATUS_ACTIVE = 'active';
    const STATUS_CLOSED = 'closed';
    const STATUS_CANCEL = 'cancel';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_invest';
    }

    public function getInvestType()
    {
        return $this->hasOne(InvestType::className(), ['id' => 'invest_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'invest_type_id', 'currency_id', 'sum'], 'integer'],
            [['paid_sum', 'closed_percent'], 'number'],
            [['date_add', 'date_update', 'returnType', 'status', 'invest_num'], 'safe'],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(User::className(), ['id' => 'currency_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'invest_type_id' => Yii::t('app', 'Инвест. пакет'),
            'currency_id' => Yii::t('app', 'Валюта'),
            'sum' => Yii::t('app', 'Сумма'),
            'paid_sum' => Yii::t('app', 'Выплаченая сумма'),
            'returnType' => Yii::t('app', 'Возвратный тип'),
            'status' => Yii::t('app', 'Статус'),
            'closed_percent' => Yii::t('app', 'Выплаченый процент'),
            'date_add' => Yii::t('app', 'Дата добавления'),
            'date_update' => Yii::t('app', 'Дата обновления'),
            'invest_num' => Yii::t('app', 'Номер инвест'),
        ];
    }
}
