<?php

namespace app\modules\invest\models;

use app\modules\finance\models\Currency;
use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "{{%detail_marketing}}".
 *
 * @property int $id
 * @property int $to_user_id
 * @property int $from_user_id
 * @property int $currency_id
 * @property double $sum
 * @property int $user_invest_id
 * @property string $date
 */
class DetailMarketing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%detail_marketing}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_user_id', 'from_user_id', 'currency_id', 'user_invest_id', 'level'], 'integer'],
            [['sum', 'percent'], 'number'],
            [['date'], 'safe'],
        ];
    }

    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getToUser() {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    public function getFromUser() {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'to_user_id' => Yii::t('app', 'To User ID'),
            'from_user_id' => Yii::t('app', 'From User ID'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'sum' => Yii::t('app', 'Sum'),
            'percent' => Yii::t('app', 'Percent'),
            'level' => Yii::t('app', 'Level'),
            'user_invest_id' => Yii::t('app', 'User Invest ID'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
