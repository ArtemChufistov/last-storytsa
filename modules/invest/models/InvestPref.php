<?php

namespace app\modules\invest\models;

use app\modules\invest\models\InvestType;
use Yii;

/**
 * This is the model class for table "invest_pref".
 *
 * @property int $id
 * @property string $key
 * @property string $value
 */
class InvestPref extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_pref';
    }

    const KEY_STAT_PERCENT = 'stat_percent'; // процент
    const KEY_PAY_PERIOD = 'pay_period'; // период начисления
    const KEY_INVEST_PERIOD = 'invest_period'; // период начисления
    const KEY_PARENT = 'parent'; // пригласителю N уровня
    const KEY_PARENT1 = 'parent1'; // пригласителю 1 уровня
    const KEY_PARENT2 = 'parent2'; // пригласителю 2 уровня
    const KEY_PARENT3 = 'parent3'; // пригласителю 3 уровня
    const KEY_PARENT4 = 'parent4'; // пригласителю 4 уровня
    const KEY_PARENT5 = 'parent5'; // пригласителю 5 уровня
    const KEY_PARENT6 = 'parent6'; // пригласителю 6 уровня
    const KEY_PARENT7 = 'parent7'; // пригласителю 7 уровня
    const KEY_PARENT8 = 'parent8'; // пригласителю 8 уровня
    const KEY_PARENT9 = 'parent9'; // пригласителю 9 уровня
    const KEY_PARENT10 = 'parent10'; // пригласителю 10 уровня
    const KEY_PARENT11 = 'parent11'; // пригласителю 11 уровня
    const KEY_PARENT12 = 'parent12'; // пригласителю 12 уровня
    const KEY_PARENT13 = 'parent13'; // пригласителю 13 уровня
    const KEY_PARENT14 = 'parent14'; // пригласителю 14 уровня
    const KEY_PARENT15 = 'parent15'; // пригласителю 15 уровня
    const KEY_PARENT16 = 'parent16'; // пригласителю 16 уровня
    const KEY_PARENT17 = 'parent17'; // пригласителю 17 уровня
    const KEY_PARENT18 = 'parent18'; // пригласителю 18 уровня
    const KEY_PARENT19 = 'parent19'; // пригласителю 19 уровня
    const KEY_PARENT20 = 'parent20'; // пригласителю 20 уровня
    const KEY_PARENT21 = 'parent21'; // пригласителю 21 уровня
    const KEY_USER = 'user'; // пользователю
    const KEY_CURRENT_CURRENCY = 'current_currency'; // пользователю
    const KEY_SIMPLE_INVEST = 'simple_invest'; // стандартный пакет
    const KEY_DEPOSIT_INVEST = 'deposit_invest'; // депозитный пакет
    const KEY_INVEST_CANCEL_AGREE = 'cancel_agree'; // можно отменить инвестицию
    const KEY_INVEST_CANCEL_PERCENT = 'cancel_percent'; // штраф за отмену инвестиции
    const KET_COUNT_AKCII = 'count_akcii'; // сколько акций начислить

    const KEY_CHANGE = 'change';
    const KEY_VOLUME = 'volume';
    const KEY_MARKETCAP = 'marketcap';
    const KEY_ROI = 'roi';
    const KEY_NODES = 'nodes';
    const KEY_REQUIRED = 'required';
    const KEY_MN_WORTH = 'mn_worth';
    const KEY_COINS_FOR_LAUNCH = 'coins_for_launch';


    public static function getKeys()
    {
        return [
            self::KEY_STAT_PERCENT => Yii::t('app', 'Процент'),
            self::KEY_PAY_PERIOD => Yii::t('app', 'Период начисления'),
            self::KEY_INVEST_PERIOD => Yii::t('app', 'Период действия'),
            self::KEY_PARENT => Yii::t('app', 'Пригласителю N уровня'),
            self::KEY_PARENT1 => Yii::t('app', 'Пригласителю 1 уровня'),
            self::KEY_PARENT2 => Yii::t('app', 'Пригласителю 2 уровня'),
            self::KEY_PARENT3 => Yii::t('app', 'Пригласителю 3 уровня'),
            self::KEY_PARENT4 => Yii::t('app', 'Пригласителю 4 уровня'),
            self::KEY_PARENT5 => Yii::t('app', 'Пригласителю 5 уровня'),
            self::KEY_PARENT6 => Yii::t('app', 'Пригласителю 6 уровня'),
            self::KEY_PARENT7 => Yii::t('app', 'Пригласителю 7 уровня'),
            self::KEY_PARENT8 => Yii::t('app', 'Пригласителю 8 уровня'),
            self::KEY_PARENT9 => Yii::t('app', 'Пригласителю 9 уровня'),
            self::KEY_PARENT10 => Yii::t('app', 'Пригласителю 10 уровня'),
            self::KEY_PARENT11 => Yii::t('app', 'Пригласителю 11 уровня'),
            self::KEY_PARENT12 => Yii::t('app', 'Пригласителю 12 уровня'),
            self::KEY_PARENT13 => Yii::t('app', 'Пригласителю 13 уровня'),
            self::KEY_PARENT14 => Yii::t('app', 'Пригласителю 14 уровня'),
            self::KEY_PARENT15 => Yii::t('app', 'Пригласителю 15 уровня'),
            self::KEY_PARENT16 => Yii::t('app', 'Пригласителю 16 уровня'),
            self::KEY_PARENT17 => Yii::t('app', 'Пригласителю 17 уровня'),
            self::KEY_PARENT18 => Yii::t('app', 'Пригласителю 18 уровня'),
            self::KEY_PARENT19 => Yii::t('app', 'Пригласителю 19 уровня'),
            self::KEY_PARENT20 => Yii::t('app', 'Пригласителю 20 уровня'),
            self::KEY_PARENT21 => Yii::t('app', 'Пригласителю 21 уровня'),
            self::KEY_USER => Yii::t('app', 'Пользователю'),
            self::KEY_CURRENT_CURRENCY => Yii::t('app', 'Основная валюта'),
            self::KEY_SIMPLE_INVEST => Yii::t('app', 'Стандартный пакет'),
            self::KEY_DEPOSIT_INVEST => Yii::t('app', 'Депозитный пакет'),
            self::KEY_INVEST_CANCEL_AGREE => Yii::t('app', 'Можно отменить инвестицию'),
            self::KEY_INVEST_CANCEL_PERCENT => Yii::t('app', 'Штраф за отмену процента'),
            self::KET_COUNT_AKCII => Yii::t('app', 'Сколько акций начислить'),
            self::KEY_CHANGE => Yii::t('app', 'Change'),
            self::KEY_VOLUME => Yii::t('app', 'Volume'),
            self::KEY_MARKETCAP => Yii::t('app', 'Marketcap'),
            self::KEY_ROI => Yii::t('app', 'Roi'),
            self::KEY_NODES => Yii::t('app', 'Nodes'),
            self::KEY_REQUIRED => Yii::t('app', 'Required'),
            self::KEY_MN_WORTH => Yii::t('app', 'Mn Worth'),
            self::KEY_COINS_FOR_LAUNCH => Yii::t('app', 'Coins for launch'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'invest_type_id'], 'required'],
            [['value', 'value1', 'value2'], 'string'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    public function getKeyTitle()
    {
        $data = self::getKeys();

        return $data[$this->key];
    }

    public function getInvestType()
    {
        return $this->hasOne(InvestType::className(), ['id' => 'invest_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Ключ'),
            'value' => Yii::t('app', 'Значение'),
            'value1' => Yii::t('app', 'Значение 1'),
            'value2' => Yii::t('app', 'Значение 2'),
            'invest_type_id' => Yii::t('app', 'Инвест пакет'),
        ];
    }
}
