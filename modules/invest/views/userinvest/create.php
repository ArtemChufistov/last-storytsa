<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\invest\models\UserInvest */

$this->title = Yii::t('app', 'Добавление инвест. пакет.');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Инвест. пакеты участников'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-invest-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
