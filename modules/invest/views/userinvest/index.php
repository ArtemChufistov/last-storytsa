<?php

use app\modules\invest\models\InvestType;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\invest\models\UserInvestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Инвест. пакеты участников');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-invest-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
              <?= Html::a(Yii::t('app', 'Добавить инвест. пакет участнику'), ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <?php Pjax::begin(); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        [
                          'attribute' => 'user_id',
                          'format' => 'raw',
                          'filter' => Select2::widget([
                            'name' => (new ReflectionClass($searchModel))->getShortName() . '[user_id]',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                              'placeholder' => Yii::t('app', 'Введите логин'),
                            ],
                            'pluginOptions' => [
                              'minimumInputLength' => 2,
                              'ajax' => [
                                'url' => Url::to(['/profile/backuser/searhbylogin']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                              ],
                              'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                              'templateResult' => new JsExpression('function(user) { return user.login; }'),
                              'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                            ],
                          ]),
                          'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_user_id', ['model' => $model]);},
                        ],[
                          'attribute' => 'invest_type_id',
                          'filter' => Select2::widget([
                            'name' => (new ReflectionClass($searchModel))->getShortName() . '[invest_type_id]',
                            'value' => $searchModel->invest_type_id,
                            'attribute' => 'invest_type_id',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'data' => ArrayHelper::map(InvestType::find()->all(), 'id', 'name'),
                            'options' => [
                              'placeholder' => Yii::t('app', 'Выберите'),
                              'style' => ['width' => '150px'],
                            ]]),
                          'format' => 'raw',
                          'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_invest-type-id', ['model' => $model]);},
                        ],[
                          'attribute' => 'currency_id',
                          'format' => 'raw',
                          'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_currency-id', ['model' => $model]);},
                        ],
                        'sum'
                        , [
                          'attribute' => 'date_add',
                          'format' => 'raw',
                          'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'date_add',
                            'options' => ['class' => 'form-control'],
                            'language' => 'ru',

                          ]),
                          'value' => function ($model) {return Yii::$app->controller->renderPartial('/grid/_date-add', ['model' => $model]);},
                        ],
                        // 'paid_sum',
                        // 'closed_percent',
                        // 'date_update',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
          </div>
        </div>
    </div>
</div>
