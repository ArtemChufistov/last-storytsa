<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\invest\models\UserInvest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-invest-form">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
            <div class="invest-type-form">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'user_id')->textInput() ?>

                <?= $form->field($model, 'invest_type_id')->textInput() ?>

                <?= $form->field($model, 'currency_id')->textInput() ?>

                <?= $form->field($model, 'sum')->textInput() ?>

                <?= $form->field($model, 'paid_sum')->textInput() ?>

                <?= $form->field($model, 'closed_percent')->textInput() ?>

                <?= $form->field($model, 'date_add')->textInput() ?>

                <?= $form->field($model, 'date_update')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
      </div>
    </div>
</div>
