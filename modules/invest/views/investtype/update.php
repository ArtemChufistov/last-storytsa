<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\invest\models\InvestType */

$this->title = Yii::t('app', 'Редактировать инвест. пакет: ', [
    'modelClass' => 'Invest Type',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Управление пакетами'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="invest-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
