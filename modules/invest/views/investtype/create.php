<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\invest\models\InvestType */

$this->title = Yii::t('app', 'Добавить инвестиционный пакет');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Управление пакетами'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invest-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
