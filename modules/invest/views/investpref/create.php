<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\invest\models\InvestPref */

$this->title = Yii::t('app', 'Добавление деталей пакета');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки пакетов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invest-pref-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
