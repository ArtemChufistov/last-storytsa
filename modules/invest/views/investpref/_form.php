<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\InvestPref;

/* @var $this yii\web\View */
/* @var $model app\modules\invest\models\InvestPref */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invest-pref-form">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
			<div class="invest-type-form">

			    <?php $form = ActiveForm::begin(); ?>

                <?php echo $form->field($model, 'key')->widget(Select2::classname(), [
				    'data' => InvestPref::getKeys(),
				    'options' => ['placeholder' => 'Выберите ключ...'],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
                ?>

			    <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>
			    <?= $form->field($model, 'value1')->textarea(['rows' => 6]) ?>
			    <?= $form->field($model, 'value2')->textarea(['rows' => 6]) ?>

			    <?php echo $form->field($model, 'invest_type_id')->widget(Select2::classname(), [
				    'data' => ArrayHelper::map(InvestType::find()->all(), 'id', 'name'),
				    'options' => ['placeholder' => 'Выберите пакет...'],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
                ?>

			    <div class="form-group">
			        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
			    </div>

			    <?php ActiveForm::end(); ?>

			</div>
		</div>
      </div>
    </div>
</div>
