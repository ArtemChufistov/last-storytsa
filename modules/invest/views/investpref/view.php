<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\invest\models\InvestPref */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки пакетов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invest-pref-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">

            <p>
                <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'key',
                        'format' => 'raw',
                        'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_invest-pref-key', ['model' => $model]);},
                    ],
                    'value:ntext',
                    [
                        'attribute' => 'invest_type_id',
                        'format' => 'raw',
                        'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_invest-type-id', ['model' => $model]);},
                    ],
                ],
            ]) ?>
        </div>
      </div>
    </div>
  </div>
</div>
