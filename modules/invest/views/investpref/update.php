<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\invest\models\InvestPref */

$this->title = Yii::t('app', 'Редактирование настроек пакета: ', [
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки пакетов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="invest-pref-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
