<?php

namespace app\modules\lang\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\lang\models\Message;
use yii\helpers\ArrayHelper;

/**
 * MessageSearch represents the model behind the search form of `app\modules\lang\models\Message`.
 */
class MessageSearch extends Message
{

    public $showempty;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'showempty'], 'integer'],
            [['language', 'translation', 'sourceMessage', 'showempty'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['id', 'language', 'translation', 'sourceMessage']]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'translation', $this->translation]);

        if ($this->showempty){
            $notInquery = ArrayHelper::map(Message::find()->where(['language' => $this->language])->all(), 'id', 'id');

            $query->andFilterWhere(['=', 'language', \Yii::$app->language]);
            $query->andFilterWhere(['NOT IN', 'id', [1,2]]);
            //echo $query->createCommand()->getRawSql();exit;
        }else{
            if ($this->language){
                $query->andFilterWhere(['like', 'language', $this->language]);
            }
        }

        return $dataProvider;
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Исходный текст'),
            'language' => Yii::t('app', 'Язык'),
            'translation' => Yii::t('app', 'Перевод'),
            'showempty' => Yii::t('app', 'Показать несуществующие')
        ];
    }
}
