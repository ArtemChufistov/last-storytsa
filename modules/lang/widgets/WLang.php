<?php
namespace app\modules\lang\widgets;

use app\modules\lang\models\Lang;

class WLang extends \yii\bootstrap\Widget
{
	public $view = '_lang-choser';

    public function init(){}

    public function run() {
		return \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/layouts/partial/' . $this->view, [
            'current' => Lang::getCurrent(),
            'langs' => Lang::find()->where('id != :current_id', [':current_id' => Lang::getCurrent()->id])->andWhere(['active' => Lang::ACTIVE_TRUE])->all(),
        ]);
    }
}