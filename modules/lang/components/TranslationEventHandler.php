<?php

namespace app\modules\lang\components;

use yii\i18n\MissingTranslationEvent;
use app\modules\lang\models\Lang;
use app\modules\lang\models\Message;
use app\modules\lang\models\SourceMessage;
use Yii;

class TranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
    	if (Yii::$app->sourceLanguage == Yii::$app->language){
    		$messageTest = Message::find()->where(['translation' => $event->message, 'language' => Yii::$app->language])->one();

    		if (empty($messageTest)){
    			$sourceMessage = SourceMessage::find()->where(['category' => $event->category, 'message' => $event->message])->one();

    			if (empty($sourceMessage)){
			    	$sourceMessage = new SourceMessage;
			    	$sourceMessage->category = $event->category;
			    	$sourceMessage->message = $event->message;
			    	$sourceMessage->save();
    			}

                $message = Message::find()->where(['id' => $sourceMessage->id])->andWhere(['language' => Yii::$app->language])->one();
                if (empty($message)){
                    $message = new Message;
                    $message->id = $sourceMessage->id;
                    $message->language = Yii::$app->language;
                    $message->translation = $event->message;
                    $message->save();
                }
    		}
    	}
    }
}