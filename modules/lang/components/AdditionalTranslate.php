<?php

namespace app\modules\lang\components;

use yii\i18n\MissingTranslationEvent;
use app\modules\lang\models\Lang;
use app\modules\lang\models\Message;
use app\modules\lang\models\SourceMessage;

class AdditionalTranslate
{
    // скопировать значения одного языка в другой
    public static function copyLanguageValues($fromLanguage, $toLanguage)
    {
    	$messageArray = Message::find()->where(['language' => $fromLanguage])->all();

    	foreach($messageArray as $message){
    		$sourseMessage = $message->getId0()->one();


    		$messageToLang = Message::find()->where(['language' => $toLanguage])->andWHere(['translation' => $sourseMessage->message])->one();

    		if (!empty($messageToLang)){
    			$messageToLang->translation = $message->translation;
    			$messageToLang->save(false);
    		}
    	}

    	echo 'Перевод выполнен' . "\r\n";
    }
}