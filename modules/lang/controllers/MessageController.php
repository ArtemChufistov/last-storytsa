<?php

namespace app\modules\lang\controllers;

use app\admin\controllers\AdminController;
use app\modules\lang\models\Message;
use app\modules\lang\models\MessageSearch;
use app\modules\lang\models\SourceMessage;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MessageController implements the CRUD actions for Message model.
 */
class MessageController extends AdminController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'view', 'create', 'update', 'delete'],
				'rules' => [
					[
						'actions' => ['index', 'view', 'create', 'update', 'delete'],
						'allow' => true,
						'roles' => ['administrator'],
					], [
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['langMessageIndex'],
					], [
						'actions' => ['view'],
						'allow' => true,
						'roles' => ['langMessageView'],
					], [
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['langMessageCreate'],
					], [
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['langMessageUpdate'],
					], [
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['langMessageDelete'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Message models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new MessageSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Message model.
	 * @param integer $id
	 * @param string $language
	 * @return mixed
	 */
	public function actionView($id, $language) {
		return $this->render('view', [
			'model' => $this->findModel($id, $language),
		]);
	}

	public function actionSearchsource() {
		$result = [];

		if (empty(Yii::$app->request->get('q'))) {
			return $result;
		}

		$sourceMessageArray = SourceMessage::find()->where(['like', 'message', Yii::$app->request->get('q')])->all();

		foreach ($sourceMessageArray as $sourceMessage) {
			$result[] = [
				'id' => $sourceMessage->id,
				'message' => $sourceMessage->message,
			];
		}

		echo json_encode(['results' => $result]);
	}

	public function actionAutotranslate() {
		$word = $_GET['word'];
		$source = \Yii::$app->sourceLanguage;
		$target = $_GET['target'];

		$result = \Yii::$app->translate->translate($source, $target, $word);

		echo json_encode(['results' => $result]);
	}

	/**
	 * Creates a new Message model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Message();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id, 'language' => $model->language]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Message model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @param string $language
	 * @return mixed
	 */
	public function actionUpdate($id, $language) {
		$model = $this->findModel($id, $language);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id, 'language' => $model->language]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Message model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @param string $language
	 * @return mixed
	 */
	public function actionDelete($id, $language) {
		$this->findModel($id, $language)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Message model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @param string $language
	 * @return Message the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id, $language) {
		if (($model = Message::findOne(['id' => $id, 'language' => $language])) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
