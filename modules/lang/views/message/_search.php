<?php

use app\modules\lang\models\Lang;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\lang\models\MessageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-search">

    <?php $form = ActiveForm::begin([
	'action' => ['index'],
	'method' => 'get',
	'options' => [
		'data-pjax' => 1,
	],
]);?>

    <?php echo $form->field($model, 'language')->widget(Select2::classname(), [
	'data' => array_merge([Yii::t('app', 'Не выбрано')], ArrayHelper::map(Lang::find()->all(), 'local', 'name')),
	'options' => ['placeholder' => 'Выберите язык...'],
	'pluginOptions' => [
		'allowClear' => true,
	],
]); ?>

    <?php echo $form->field($model, 'id')->widget(Select2::classname(), [
	'initValueText' => Yii::t('app', 'Текст'), //$model->getId0()->one()->message, // set the initial display text
	'options' => ['placeholder' => 'Введите'],
	'pluginOptions' => [
		'allowClear' => true,
		'minimumInputLength' => 2,
		'language' => [
			'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
		],
		'ajax' => [
			'url' => Url::to(['/lang/message/searchsource']),
			'dataType' => 'json',
			'data' => new JsExpression('function(params) { console.log(params);return {q:params.term}; }'),
		],
		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
		'templateResult' => new JsExpression('function(source) { return source.message; }'),
		'templateSelection' => new JsExpression('function (source) { return source.message || source.text;; }'),
	]]); ?>

    <?=$form->field($model, 'translation')?>

    <?=$form->field($model, 'showempty')->checkbox(['uncheck' => null, 'checked' => true]);?>

    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Поиск'), ['class' => 'btn btn-primary'])?>
        <?=Html::resetButton(Yii::t('app', 'Сбросить'), ['class' => 'btn btn-default'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
