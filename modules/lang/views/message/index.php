<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\lang\models\Lang;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\lang\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Переводы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                 <?= Html::a(Yii::t('app', 'Добавить перевод'), ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <?php Pjax::begin(); ?>

            <div class="box-body no-padding">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],[
                          'attribute' => 'id',
                          'label' => 'Id',
                        ],[
                          'attribute' => 'language',
                          'format' => 'raw',
                          'filter' => Select2::widget([
                              'name' => (new ReflectionClass($searchModel))->getShortName() . '[language]',
                              'value' => $searchModel->language,
                              'attribute' => 'language',
                              'theme' => Select2::THEME_BOOTSTRAP,
                              'data' => array_merge([Yii::t('app', 'Не выбрано')], ArrayHelper::map(Lang::find()->all(), 'local', 'name')),
                              'options' => [
                                  'placeholder' => Yii::t('app', 'Выберите'),
                                  'style' => ['width' => '150px'],
                              ],]
                            ),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_language', ['model' => $model]);},
                        ],[
                          'attribute' => 'id',
                          'format' => 'raw',
                          'filter' => Select2::widget([
                              'name' => (new ReflectionClass($searchModel))->getShortName() . '[id]',
                              'theme' => Select2::THEME_BOOTSTRAP,
                              'options' => [
                                  'placeholder' => Yii::t('app', 'Введите слово'),
                              ],
                              'pluginOptions' => [
                                  'minimumInputLength' => 2,
                                  'ajax' => [
                                      'url' => Url::to(['/lang/message/searchsource']),
                                      'dataType' => 'json',
                                      'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                  ],
                                  'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                  'templateResult' => new JsExpression('function(source) { return source.message; }'),
                                  'templateSelection' => new JsExpression('function (source) { return source.message || source.text; }'),
                              ]
                          ]),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_source-message', ['model' => $model]);},
                        ],
                        'translation:ntext',
                        ['class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open" title="View Details"></span>', $url, ['data-pjax' => 0, 'target' => "_blank"]);
                                },
                                        'update' => function ($url, $model) {

                                    return Html::a('<span class="glyphicon glyphicon-pencil" title="Update"></span>',$url, ['data-pjax' => 0, 'target' => "_blank"]);
                                },
                                        'delete' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-trash" title= "Delete"></span>', $url, ['data-pjax' => 0, 'target' => "_blank"]);
                                },
                                    ],
                                ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
      </div>
    </div>
</div>