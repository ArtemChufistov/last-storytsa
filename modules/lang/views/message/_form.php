<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\lang\models\Lang;

/* @var $this yii\web\View */
/* @var $model app\modules\lang\models\Message */
/* @var $form yii\widgets\ActiveForm */
?>
<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
			<div class="message-form">

			    <?php $form = ActiveForm::begin(); ?>

		        <?php echo $form->field($model, 'id')->widget(Select2::classname(), [
			            'initValueText' => $model->getId0()->one()->message, // set the initial display text
			            'options' => ['placeholder' => 'Введите'],
			            'pluginOptions' => [
			            'allowClear' => true,
			            'minimumInputLength' => 2,
			            'language' => [
			                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
			            ],
			            'ajax' => [
			                'url' => Url::to(['/lang/message/searchsource']),
			                'dataType' => 'json',
			                'data' => new JsExpression('function(params) { console.log(params);return {q:params.term}; }')
			            ],
			            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
			            'templateResult' => new JsExpression('function(source) { return source.message; }'),
			            'templateSelection' => new JsExpression('function (source) { return source.message || source.text;; }'),
			        ],
			        ]);
		        ?>

		        <?php echo $form->field($model, 'language')->widget(Select2::classname(), [
		            'data' => array_merge([Yii::t('app', 'Не выбрано')], ArrayHelper::map(Lang::find()->all(), 'local', 'name')),
		            'options' => ['placeholder' => 'Выберите язык...'],
		            'pluginOptions' => [
		                'allowClear' => true
		            ],
		        ]);?>

			    <?= $form->field($model, 'translation')->textarea(['rows' => 6]) ?>

			    <div class="form-group">
			        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
			        <?= Html::button(Yii::t('app', 'Перевести автоматически'), ['class' => 'btn btn-success autoTranslate']) ?>
			    </div>

			    <?php ActiveForm::end(); ?>

            </div>
        </div>
      </div>
    </div>
</div>

<?php
$this->registerJs("
$('.autoTranslate').on('click', function(){
	var message = $('#message-id').select2('data');
	var target = $('#message-language').select2('data');
	var resultMessage = '';

	if (message[0]['text'] != undefined){
		resultMessage = message[0]['text'];
		console.log('3de3d');
	}
	if (message[0]['message'] != undefined){
		resultMessage = message[0]['message'];
	}

	if (resultMessage != '' && target[0]['id'] != ''){
		$.get( '" . Url::to(['/lang/message/autotranslate']) . "?word=' + resultMessage + '&target=' + target[0]['id'], function( data ) {
			data = JSON.parse(data);

			if (data.results.code == '200'){
				$('#message-translation').val(data.results.text);
			}
		});
	}
})
");
?>