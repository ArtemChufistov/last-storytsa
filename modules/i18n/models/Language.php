<?php
namespace app\modules\i18n\models;

use app\components\db\ActiveRecord;
use app\modules\i18n\models\query\LanguageQuery;
use Yii;

/**
 * Class Language
 * @package app\modules\i18n\models
 * @property integer $id
 * @property string $name
 * @property string $icon
 * @property string $locale
 * @property integer $source_language
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Language extends ActiveRecord
{
    const DEFAULT_LOCALE = 'en-US';
    const DEFAULT_LANGUAGE_NAME = 'English';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%i18n_language}}';
    }

    /**
     * @return LanguageQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new LanguageQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'icon', 'locale'], 'required'],

            ['name', 'string', 'max' => 32],
            ['locale', 'string', 'max' => 8],

            [['source_language', 'active'], 'integer'],
            [['source_language', 'active'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'icon' => Yii::t('common', 'Флаг'),
            'locale' => Yii::t('common', 'Локаль'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return Language
     */
    public static function getDefault()
    {
        return self::find()->byLocale(self::DEFAULT_LOCALE)->one();
    }
}
