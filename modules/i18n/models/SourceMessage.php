<?php
namespace app\modules\i18n\models;

use app\components\db\ActiveRecord;
use Yii;

/**
 * Class SourceMessage
 * @package app\models
 * @property integer $id
 * @property string $category
 * @property string $message
 * @property Message[] $messages
 */
class SourceMessage extends ActiveRecord
{
    public $is_fully_translation = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%i18n_source_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'message'], 'required'],
            [['category', 'message'], 'filter', 'filter' => 'trim'],
            ['category', 'string', 'max' => 32],
            ['message', 'validateDuplicate'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'category' => Yii::t('common', 'Категория'),
            'message' => Yii::t('common', 'Фраза'),
            'is_fully_translation' => Yii::t('common', 'Полный перевод'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateDuplicate($attribute, $params)
    {
        $query = SourceMessage::find()
            ->where(['category' => $this->category])
            ->andWhere(['message' => $this->message]);

        if (!$this->isNewRecord) {
            $query->andWhere('id != ' . $this->id);
        }

        $exists = $query->exists();

        if ($exists) {
            $this->addError($attribute, Yii::t('common', 'Фраза уже существует.'));
        }
    }

    /**
     * @return $this
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['id' => 'id']);
    }
}
