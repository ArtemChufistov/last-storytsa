<?php
namespace app\modules\i18n\models\search;

use app\modules\i18n\models\Language;
use app\modules\i18n\models\Message;
use app\modules\i18n\models\SourceMessage;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class SourceMessageSearch
 * @package app\models\i18n
 */
class SourceMessageSearch extends SourceMessage
{
    public $isFullyTranslation = 'no';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'message'], 'safe'],
            ['isFullyTranslation', 'in', 'range' => ['', 'no', 'yes']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(['isFullyTranslation' => Yii::t('common', 'Полный перевод')], parent::attributeLabels());
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['category'] = SORT_ASC;
            $sort['id'] = SORT_ASC;
        }

        $activeLanguages = array_keys(Language::find()->collection());
        $countActiveTranslations = count($activeLanguages);
        $str = implode("', '", $activeLanguages);

        $query = SourceMessage::find()
            ->select(['*', "IF((SELECT COUNT(*) FROM " . Message::tableName() . " WHERE language IN ('" . $str . "') AND id = " . SourceMessage::tableName() . ".id) = $countActiveTranslations, 1, 0) is_fully_translation"])
            ->with('messages')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 20 : 0,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if ($params) {
            if (!$this->load($params) || !$this->validate()) {
                return $dataProvider;
            }
        }

        $query->andFilterWhere(['like', 'category', $this->category]);
        $query->andFilterWhere(['like', 'message', $this->message]);

        if ($this->isFullyTranslation == 'yes' || $this->isFullyTranslation == 'no') {
            $query->having('is_fully_translation = ' . ($this->isFullyTranslation == 'yes' ? 1 : 0));
        }

        return $dataProvider;
    }
}
