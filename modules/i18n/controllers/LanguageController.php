<?php
namespace app\modules\i18n\controllers;

use app\components\web\Controller;
use app\modules\i18n\models\Language;
use app\modules\i18n\models\search\LanguageSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class LanguageController
 * @package app\modules\i18n\controllers
 */
class LanguageController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return 123;exit;
        $searchModel = new LanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        /** @var \app\modules\i18n\models\Language $model */
        $model = Language::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Язык не найден.'));
        }

        $model->active = 1;

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Язык успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        /** @var \app\modules\i18n\models\Language $model */
        $model = Language::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Язык не найден.'));
        }

        if (!$model->source_language) {
            $model->active = 0;

            if ($model->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Язык успешно деактивирован.'), 'success');
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Нельзя деактивировать основной язык.'));
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param null $locale
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionChange($locale = null)
    {
        $language = Language::find()->active()->byLocale($locale)->one();

        if (!$language) {
            throw new HttpException(400, Yii::t('common', 'Не удалось изменить язык интерфейса.'));
        }

        Yii::$app->session->set('user.language.id', $language->id);

        return $this->redirect(Yii::$app->request->referrer);
    }
}
