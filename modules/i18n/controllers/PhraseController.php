<?php
namespace app\modules\i18n\controllers;

use app\components\web\Controller;
use app\modules\i18n\models\search\SourceMessageSearch;
use app\modules\i18n\models\SourceMessage;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class PhraseController
 * @package app\controllers\i18n
 */
class PhraseController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new SourceMessageSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        /** @var \app\modules\i18n\models\SourceMessage $model */

        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new SourceMessage();
            $model->category = 'common';
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Фраза успешно добавлена.') : Yii::t('common', 'Фраза успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Фраза успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return SourceMessage
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = SourceMessage::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Фраза не найдена.'));
        }

        return $model;
    }
}
