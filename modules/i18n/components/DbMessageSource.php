<?php
namespace app\modules\i18n\components;

/**
 * Class DbMessageSource
 * @package app\components\i18n
 */
class DbMessageSource extends \yii\i18n\DbMessageSource
{
    /**
     * @param string $category
     * @param string $message
     * @param string $language
     * @return bool|string
     */
    public function translate($category, $message, $language)
    {
        return parent::translate($category, $message, $language);
    }
}
