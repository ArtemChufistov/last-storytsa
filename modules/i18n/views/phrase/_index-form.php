<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\i18n\models\search\SourceMessageSearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'category')->textInput() ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($modelSearch, 'message')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'isFullyTranslation')->select2List([
            'no' => Yii::t('common', 'Нет'),
            'yes' => Yii::t('common', 'Да'),
        ], [
            'prompt' => '—',
            'length' => -1,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
