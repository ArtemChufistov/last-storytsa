<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\i18n\models\SourceMessage $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление фразы') : Yii::t('common', 'Редактирование фразы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Интернационализация'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список фраз'), 'url' => Url::toRoute('/i18n/phrase/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление фразы') : Yii::t('common', 'Редактирование фразы')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фраза'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>
