<?php
use app\helpers\DataProvider;
use app\widgets\Panel;
use app\components\grid\GridView;
use app\components\grid\DateColumn;
use app\components\grid\ActionColumn;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список языков');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Интернационализация'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список языков')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с языками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($data) {
            $class = (!$data->active) ? 'text-gray' : '';
            return ['class' => 'tr-vertical-align-middle' . ' ' . $class];
        },
        'columns' => [
            [
                'attribute' => 'id',
                'label' => '#',
                'headerOptions' => ['class' => 'width-100'],
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'locale',
                'headerOptions' => ['class' => 'width-100 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['deactivate', 'id' => $data->id]);
                        },
                        'can' => function ($data) {
                            return Yii::$app->user->can('i18n.language.deactivate') && $data->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['activate', 'id' => $data->id]);
                        },
                        'can' => function ($data) {
                            return Yii::$app->user->can('i18n.language.activate') && !$data->active;
                        }
                    ],
                ]
            ]
        ],
    ]),
]); ?>
