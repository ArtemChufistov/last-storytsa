<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\i18n\models\search\MessageSearch $modelSearch */
/** @var \app\modules\i18n\models\Language[] $languages */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'language')->select2List($languages, [
                'prompt' => '—',
                'length' => -1,
            ]) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'phrase')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'translation')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
