<?php

namespace app\modules\matrix\models;

use Yii;

/**
 * This is the model class for table "matrix_category_matrix".
 *
 * @property integer $id
 * @property integer $matrix_id
 * @property integer $matrix_category_id
 *
 * @property MatrixCategory $matrixCategory
 * @property Matrix $matrix
 */
class MatrixCategoryMatrix extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matrix_category_matrix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['matrix_id', 'matrix_category_id'], 'integer'],
            [['matrix_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => MatrixCategory::className(), 'targetAttribute' => ['matrix_category_id' => 'id']],
            [['matrix_id'], 'exist', 'skipOnError' => true, 'targetClass' => Matrix::className(), 'targetAttribute' => ['matrix_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'matrix_id' => Yii::t('app', 'Matrix ID'),
            'matrix_category_id' => Yii::t('app', 'Matrix Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatrixCategory()
    {
        return $this->hasOne(MatrixCategory::className(), ['id' => 'matrix_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatrix()
    {
        return $this->hasOne(Matrix::className(), ['id' => 'matrix_id']);
    }
}
