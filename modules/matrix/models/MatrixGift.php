<?php

namespace app\modules\matrix\models;

use app\modules\matrix\models\Matrix;
use Yii;

/**
 * This is the model class for table "matrix_gift".
 *
 * @property integer $id
 * @property integer $matrix_id
 * @property integer $left_level_1
 * @property integer $left_level_2
 * @property integer $left_level_3
 * @property integer $left_level_4
 * @property integer $left_level_5
 * @property integer $left_level_6
 */
class MatrixGift extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matrix_gift';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['matrix_id', 'left_level_1', 'left_level_2', 'left_level_3', 'left_level_4', 'left_level_5', 'left_level_6', 'buy_level_1', 'buy_level_2', 'buy_level_3', 'buy_level_4', 'buy_level_5', 'buy_level_6'], 'integer'],
        ];
    }

    public function getMatrix()
    {
        return $this->hasOne(Matrix::className(), ['id' => 'matrix_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'matrix_id' => Yii::t('app', 'Matrix ID'),
            'left_level_1' => Yii::t('app', '1ур. инфо'),
            'left_level_2' => Yii::t('app', '2ур. инфо'),
            'left_level_3' => Yii::t('app', '3ур. инфо'),
            'left_level_4' => Yii::t('app', '4ур. инфо'),
            'left_level_5' => Yii::t('app', '5ур. инфо'),
            'left_level_6' => Yii::t('app', '6ур. инфо'),
            'buy_level_1' => Yii::t('app', '1ур. данные'),
            'buy_level_2' => Yii::t('app', '2ур. данные'),
            'buy_level_3' => Yii::t('app', '3ур. данные'),
            'buy_level_4' => Yii::t('app', '4ур. данные'),
            'buy_level_5' => Yii::t('app', '5ур. данные'),
            'buy_level_6' => Yii::t('app', '6ур. данные'),
        ];
    }
}
