<?php

namespace app\modules\matrix\models;

use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixCategoryMatrix;
use Yii;

/**
 * This is the model class for table "matrix_category".
 *
 * @property integer $id
 * @property string $name
 *
 * @property MatrixCategoryMatrix[] $matrixCategoryMatrices
 */
class MatrixCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matrix_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
        ];
    }

    public function getMatrixes()
    {
        return $this->hasMany(Matrix::className(), ['id' => 'matrix_id'])
            ->viaTable(MatrixCategoryMatrix::tableName(), ['matrix_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatrixCategoryMatrices()
    {
        return $this->hasMany(MatrixCategoryMatrix::className(), ['matrix_category_id' => 'id']);
    }
}
