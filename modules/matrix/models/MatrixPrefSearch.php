<?php

namespace app\modules\matrix\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\matrix\models\MatrixPref;

/**
 * MatrixPrefSearch represents the model behind the search form of `app\modules\matrix\models\MatrixPref`.
 */
class MatrixPrefSearch extends MatrixPref
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'matrix_id'], 'integer'],
            [['key', 'value', 'value1', 'value2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MatrixPref::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'matrix_id' => $this->matrix_id,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'value1', $this->value1])
            ->andFilterWhere(['like', 'value2', $this->value2]);

        return $dataProvider;
    }
}
