<?php

namespace app\modules\matrix\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\matrix\models\Matrix;

/**
 * MatrixChangePlaceSearch represents the model behind the search form of `app\modules\matrix\models\Matrix`.
 */
class MatrixChangePlaceSearch extends Matrix
{
    public $parent_user_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'parent_user_id', 'parent_id', 'currency_id', 'status', 'sort', 'count_submit', 'root_id'], 'integer'],
            [['sum'], 'number'],
            [['date_add', 'wallet_from', 'wallet_to', 'transaction', 'slug', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Matrix::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'parent_user_id' => $this->parent_user_id,
            'parent_id' => $this->parent_id,
            'sum' => $this->sum,
            'currency_id' => $this->currency_id,
            'status' => $this->status,
            'sort' => $this->sort,
            'date_add' => $this->date_add,
            'count_submit' => $this->count_submit,
            'root_id' => $this->root_id,
        ]);

        $query->andFilterWhere(['like', 'wallet_from', $this->wallet_from])
            ->andFilterWhere(['like', 'wallet_to', $this->wallet_to])
            ->andFilterWhere(['like', 'transaction', $this->transaction])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
