<?php
namespace app\modules\matrix\models;

use yii\db\ActiveRecord;
use valentinek\behaviors\ClosureTableQuery;
use \yii\db\ActiveQuery;

class MatrixQuery extends ActiveQuery
{
    public function behaviors() {
        return [
            [
                'class' => ClosureTableQuery::className(),
                'tableName' => 'matrix_tree'
            ],
        ];
    }
}

?>