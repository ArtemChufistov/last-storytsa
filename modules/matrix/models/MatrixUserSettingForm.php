<?php

namespace app\modules\matrix\models;

use app\modules\profile\models\User;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models;
use Yii;

/**
 * This is the model class for table "{{%matrix_user_setting}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $matrix_id
 * @property integer $matrix_root_id
 * @property integer $active
 * @property string $date_add
 */
class MatrixUserSettingForm extends MatrixUserSetting
{
    public $user_login;
    public $matrix_slug;
    public $matrix_root_slug;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%matrix_user_setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_login', 'initUserId'],
            ['matrix_slug', 'initMatrixId'],
            ['matrix_root_slug', 'initMatrixRootId'],
            [['date_add', 'user_login', 'matrix_slug', 'matrix_root_slug', 'active', 'my_place', 'sort', 'child_place'], 'safe'],
        ];
    }

    public function initUserId($attribute, $params)
    {
        if (empty($this->user_login)){
            $this->addError($attribute, Yii::t('app', 'Необходимо указать логин пользователя'));
        }

        $user = User::find()->where(['login' => $this->user_login])->one();

        if (empty($user)){
            $this->addError($attribute, Yii::t('app', 'Нет такого логина'));
        }

        $this->user_id = $user->id;
    }

    public function initMatrixId($attribute, $params)
    {
        if (empty($this->matrix_slug)){
            $this->addError($attribute, Yii::t('app', 'Необходимо указать место'));
        }

        $matrix = Matrix::find()->where(['slug' => $this->matrix_slug])->one();

        if (empty($matrix)){
            $this->addError($attribute, Yii::t('app', 'Нет такого места'));
        }

        $this->matrix_id = $matrix->id;
    }

    public function initMatrixRootId($attribute, $params)
    {
        if (empty($this->matrix_root_slug)){
            $this->addError($attribute, Yii::t('app', 'Необходимо указать матрицу'));
        }

        $matrix = Matrix::find()->where(['slug' => $this->matrix_root_slug])->one();

        if (empty($matrix)){
            $this->addError($attribute, Yii::t('app', 'Нет такой матрицы'));
        }

        $this->matrix_root_id = $matrix->id;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_login' => Yii::t('app', 'Логин'),
            'matrix_slug' => Yii::t('app', 'Хэш места'),
            'matrix_root_slug' => Yii::t('app', 'Хэш матрицы'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'matrix_id' => Yii::t('app', 'Место'),
            'matrix_root_id' => Yii::t('app', 'Матрица'),
            'active' => Yii::t('app', 'Поставить место'),
            'sort' => Yii::t('app', 'Сортировка'),
            'date_add' => Yii::t('app', 'Дата добавления'),
        ];
    }
}
