<?php

namespace app\modules\matrix\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\matrix\models\MatrixUserSetting;

/**
 * MatrixUserSettingSearch represents the model behind the search form of `app\modules\matrix\models\MatrixUserSetting`.
 */
class MatrixUserSettingSearch extends MatrixUserSetting
{
    public $slug;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'matrix_id', 'matrix_root_id', 'active', 'sort'], 'integer'],
            [['date_add', 'slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MatrixUserSetting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->slug)){
            $query->innerJoinWith('matrix', 'matrix.id = matrix_user_setting.matrix_id')->andWhere(['matrix.slug' => $this->slug]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'matrix_id' => $this->matrix_id,
            'matrix_root_id' => $this->matrix_root_id,
            'active' => $this->active,
            'sort' => $this->sort,
            'date_add' => $this->date_add,
        ]);

        return $dataProvider;
    }
}
