<?php

namespace app\modules\matrix\models;

use app\modules\matrix\models\MatrixCategoryMatrix;
use app\modules\matrix\models\MatrixCategory;
use app\modules\matrix\models\MatrixGift;
use app\modules\finance\models\Currency;
use valentinek\behaviors\ClosureTable;
use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "matrix".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $parent_id
 * @property double $sum
 * @property integer $currency_id
 * @property integer $status
 * @property integer $sort
 */
class Matrix extends \yii\db\ActiveRecord
{
    const STATUS_PAY_WAIT = 1;
    const STATUS_PAY_OK = 2;
    const STATUS_PAY_CANCEL = 3;

    const SOLT = 'vcn78hgSDEWh83fvfsdHAS2fc3';

    public $place;
    public $parentPlace;
    public $nodes;
    public $depth;
    public $categoryArray;

    // для сохранения без передвижения
    public $withoutMove = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matrix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'parent_id', 'currency_id', 'status', 'sort'], 'integer'],
            [['walletFrom', 'walletTo', 'transaction', 'count_submit', 'name', 'categoryArray', 'root_id', 'countPlaces'], 'safe'], 
            [['sum'], 'number'],
        ];
    }

    public static function getStatusArray()
    {
        return [
            self::STATUS_PAY_WAIT => Yii::t('app', 'Ожидает оплаты'),
            self::STATUS_PAY_OK => Yii::t('app', 'Оплачено'),
            self::STATUS_PAY_CANCEL => Yii::t('app', 'Отменено'),
        ];
    }

    public function getStatusTitle()
    {
        $array = self::getStatusArray();

        return $array[$this->status];
    }

    public function getCategories()
    {
        return $this->hasMany(MatrixCategory::className(), ['id' => 'matrix_category_id'])
            ->viaTable(MatrixCategoryMatrix::tableName(), ['matrix_id' => 'id']);
    }

    // только вершины деревьев
    public function roots()
    {
        return $this->where(['is', 'parent_id', NULL]);
    }

    public function descendantsForLevel($level)
    {
        $levelDescendants = [];
        foreach($this->descendants()->all() as $descendant){

            if ($descendant->getLevelTo($this) == $level){
                $levelDescendants[] = $descendant;
            }
        }

        return $levelDescendants;
    }

    public function behaviors()
    {
        return [[
            'class' => ClosureTable::className(),
            'tableName' => 'matrix_tree'
        ]];
    }

    public static function find()
    {
        return new MatrixQuery(static::className());
    }

    public function getLevelTo($place)
    {
        $result = $this->find()->select('matrix_tree.depth')->innerJoin('matrix_tree', 'matrix_tree.parent = matrix.id')->where(['matrix_tree.child' => $place->id, 'matrix_tree.parent' => $this->id])->one();

        if (!empty($result)){
            return $result->depth;
        }

        $result = $this->find()->select('matrix_tree.depth')->innerJoin('matrix_tree', 'matrix_tree.parent = matrix.id')->where(['matrix_tree.child' => $this->id, 'matrix_tree.parent' => $place->id])->one();

        if (!empty($result)){
            return $result->depth;
        }

        return 0;
    }

    // обход дерева в ширину возвращает ид места к которому присоединиться и сортировку
    public function findPlace($countChildren)
    {
        $descendants = $this->descendants()->select('`matrix`.*, `depth`')->orderBy(['depth' => 'asc'])->all();
        
        $this->depth = 0;
        if (empty($descendants)){
            $descendants = [$this];
        }else{
            $descendants = array_merge([$this], $descendants);
        }

        foreach($descendants as $descendant){
            $simpleTree[$descendant->parent_id][$descendant->sort] = $descendant->id;
            $levelTree[$descendant->depth][$descendant->parent_id][$descendant->sort] = $descendant->id;
        }

        foreach($levelTree as &$level){
            foreach($level as &$treeLevel){
                ksort($treeLevel);
            }
        }

        $countLevelTree = count($levelTree);

        if ($countLevelTree == 1){
            return ['id' => $this->id, 'sort' => 0];
        }

        for($lev = 0 ; $lev < ($countLevelTree - 1); $lev++){
            $levelTree[$lev + 1] = self::sortNextLevelForCurrentLevel($levelTree[$lev], $levelTree[$lev + 1]);
        }
        

        foreach ($levelTree as $levelTreeItem) {
            foreach($levelTreeItem as $item){
                foreach($item as $subItem){
                    if (empty($simpleTree[$subItem])){
                        return ['id' => $subItem, 'sort' => 0];
                    }elseif(count($simpleTree[$subItem]) < $countChildren){
                        for($i = 0; $i <= count($simpleTree[$subItem]); $i++){
                            if (empty($simpleTree[$subItem][$i])){
                                return ['id' => $subItem, 'sort' => $i];
                            }
                        }
                    }
                }
            }
        }
    }

    // возвращает упорядоченый массив вида id -> parentid ($fields - доп поля) 
    public function fullSortedTreeAsParentArray($fields, $toLevel = 0)
    {
        if ($toLevel == 0){
            $descendants = $this->descendants()->select('`matrix`.*, `depth`')->orderBy(['depth' => SORT_ASC])->all();
        }else{
            $descendants = $this->descendants()->select('`matrix`.*, `depth`')->andWhere(['<=', 'depth', $toLevel])->orderBy(['depth' => SORT_ASC])->all();
        }
        
        $this->depth = 0;
        if (empty($descendants)){
            $descendants = [$this];
        }else{
            $descendants = array_merge([$this], $descendants);
        }

        foreach($descendants as $descendant){
            $simpleTree[$descendant->parent_id][$descendant->sort] = $descendant->id;
            $levelTree[$descendant->depth][$descendant->parent_id][$descendant->sort] = $descendant->id;
        }

        foreach($levelTree as &$level){
            foreach($level as &$treeLevel){
                ksort($treeLevel);
            }
        }

        $countLevelTree = count($levelTree);

        if ($countLevelTree == 1){
            return ['id' => $this->id, 'sort' => 0];
        }

        for($lev = 0 ; $lev < ($countLevelTree - 1); $lev++){
            $levelTree[$lev + 1] = self::sortNextLevelForCurrentLevel($levelTree[$lev], $levelTree[$lev + 1]);
        }

        $data = [];
        foreach ($levelTree as $levelTreeItem) {
            foreach($levelTreeItem as $itemId => $item){
                foreach($item as $subItem){
                    if ($subItem == $itemId){
                        $currentData = ['id' => $subItem, 'parentId' => 0];
                    }else{
                        $currentData = ['id' => $subItem, 'parentId' => $itemId];
                    }

                    $addFieldData = [];

                    if (!empty($fields)){
                        $matrixData = Matrix::find()->where(['id' => $item])->one();
                    
                        foreach($fields as $fieldTitle => $field){

                            if ($field == 'login'){
                                $user = $matrixData->getUser()->one();
                                if (!empty($user)){
                                    $addFieldData[$fieldTitle] = $user->login;
                                }else{
                                    $addFieldData[$fieldTitle] = Yii::t('app', 'No data');
                                }
                            }elseif ($field == 'place'){
                                $addFieldData[$fieldTitle] = Yii::t('app', '{name} место', ['name' => $matrixData->name]);
                            }else{
                                $addFieldData[$fieldTitle] = $matrixData->$field;
                            }
                        }
                    }

                    if (!empty($addFieldData)){
                        $data[] = array_merge($currentData, $addFieldData);
                    }else{
                        $data[] = $currentData;
                    }
                }
            }
        }

        return $data;
    }

    // сортировка следующего уровня по предыдущему
    private static function sortNextLevelForCurrentLevel($currentLevel, $nextLevel)
    {
        $newNextLevel = [];

        foreach($currentLevel as $numItem => $currentLevelItem){
            foreach ($currentLevelItem as $numSubItem => $currentLevelSubItem) {
                $newNextLevel[$currentLevelSubItem] = $currentLevelSubItem;
            }
        }

        foreach($newNextLevel as &$newNextLevelItem){
            if (!empty($nextLevel[$newNextLevelItem])){
                $newNextLevelItem = $nextLevel[$newNextLevelItem];
            }else{
                $newNextLevelItem = [];
            }
        }

        return $newNextLevel;
    }

    // взять ребёнка с сортировкой для потомка
    public function getChildForSort($sort)
    {
        foreach($this->children()->orderBy(['sort' => 'asc'])->all() as $child){
            if ($child->sort == $sort){
                return $child;
            }
        }
        return [];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getMatrixGift()
    {
        return $this->hasOne(MatrixGift::className(), ['matrix_id' => 'id']);
    }

    public function getRoot()
    {
        return $this->hasOne(Matrix::className(), ['id' => 'root_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // категории матриц - если есть
        if (!empty($this->categoryArray)){

            foreach($this->categoryArray as $categoryId){
                $matrixCategory = new MatrixCategoryMatrix;
                $matrixCategory->matrix_category_id = $categoryId;
                $matrixCategory->matrix_id = $this->id;

                $matrixCategory->save();
            }
        }

        // если не указан родитель - сохраняем как вершину
        if ($this->parent_id == ''){
           $this->parent_id = $this->id;
           $this->root_id = $this->id;
           $this->save(false); 
        }
        
        $parentMatrix = Matrix::find()->where(['id' => $this->parent_id])->one();

        $currentMatrix = Matrix::find()->where(['id' => $this->id])->one();

        if ($this->getLevelTo($parentMatrix) == 0 && $parentMatrix->id != $currentMatrix->id){
            $currentMatrix->appendTo($parentMatrix);
        }

        // количество мест
        if ($parentMatrix->id != $currentMatrix->id){
            $parentMatrix->withoutMove = true;
            $parentMatrix->countPlaces = $parentMatrix->descendants()->count();
            $parentMatrix->save(false);
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            if($this->isNewRecord){
                $this->slug = md5(time() . self::SOLT);
                $this->setAttribute('date_add', date('Y-m-d H:i:s'));
            }else{
                $parent = Matrix::find()->where(['id' => $this->parent_id])->one();

                if (!empty($parent) && $parent->id != $this->id && $this->withoutMove == false){
                    $this->moveTo($parent);
                }
            }
            return  true;
        }else{
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Участник'),
            'parent_id' => Yii::t('app', 'Родитель'),
            'sum' => Yii::t('app', 'Сумма'),
            'currency_id' => Yii::t('app', 'Валюта'),
            'status' => Yii::t('app', 'Статус'),
            'sort' => Yii::t('app', 'Сортировка'),
            'date_add' => Yii::t('app', 'Дата добавления'),
            'wallet_from' => Yii::t('app', 'Кошелек с которого перевод'),
            'wallet_to' => Yii::t('app', 'Кошелек для перевода'),
            'transaction' => Yii::t('app', 'Транзакция'),
            'count_submit' => Yii::t('app', 'Количество подтверждений'),
            'slug' => Yii::t('app', 'Хэш'),
            'name' => Yii::t('app', 'Название'),
            'root_id' => Yii::t('app', 'Вершина'),
            'countPlaces' => Yii::t('app', 'Количество мест'),
        ];
    }
}
