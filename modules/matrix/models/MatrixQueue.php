<?php

namespace app\modules\matrix\models;

use app\modules\payment\models\Currency;
use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "matrix_queue".
 *
 * @property integer $id
 * @property integer $matrix_id
 * @property integer $currency_id
 * @property integer $user_id
 * @property integer $matrix_root_id
 * @property double $sum
 * @property string $status
 * @property string $type
 * @property string $date_add
 * @property string $date_update
 */
class MatrixQueue extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_PROCESS = 'process';
    const STATUS_FINISH = 'finish';

    const TYPE_AUTO = 'auto';
    const TYPE_MANUAL = 'manual';

    public static function getStatusArray()
    {
        return [
            self::STATUS_NEW => Yii::t('app', 'Новый'),
            self::STATUS_PROCESS => Yii::t('app', 'В обработке'),
            self::STATUS_FINISH => Yii::t('app', 'Обработан')
        ];
    }

    public function getStatusTitle()
    {
        $data = self::getStatusArray();

        return $data[$this->status];
    }

    public static function getTypes()
    {
        return [
            self::TYPE_AUTO => Yii::t('app', 'Авто'),
            self::TYPE_MANUAL => Yii::t('app', 'Ручной')
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matrix_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['matrix_id', 'from_matrix_root_id', 'currency_id', 'user_id', 'matrix_root_id'], 'integer'],
            [['sum'], 'number'],
            [['status', 'type'], 'required'],
            [['date_add', 'date_update'], 'safe'],
            [['status', 'type'], 'string', 'max' => 255],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord){
            $this->date_add = date('Y-m-d H:i:s');
        }

        $this->date_update = date('Y-m-d H:i:s');

        if (parent::beforeSave($insert)) {
            return true;
        } else {
            return false;
        }
    }

    public function getMatrix()
    {
        return $this->hasOne(Matrix::className(), ['id' => 'matrix_id']);
    }

    public function getRootMatrix()
    {
        return $this->hasOne(Matrix::className(), ['id' => 'matrix_root_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'matrix_id' => Yii::t('app', 'Место матрицы'),
            'currency_id' => Yii::t('app', 'Валюта'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'matrix_root_id' => Yii::t('app', 'Матрица'),
            'from_matrix_root_id' => Yii::t('app', 'От матрицы'),
            'sum' => Yii::t('app', 'Сумма'),
            'status' => Yii::t('app', 'Статус'),
            'type' => Yii::t('app', 'Тип'),
            'date_add' => Yii::t('app', 'Дата добавления'),
            'date_update' => Yii::t('app', 'Дата обновления'),
        ];
    }
}
