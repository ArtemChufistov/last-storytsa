<?php

namespace app\modules\matrix\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use app\modules\profile\models\User;
use app\modules\matrix\models\MatrixGift;

/**
 * MatrixGiftSearch represents the model behind the search form of `app\modules\matrix\models\MatrixGift`.
 */
class MatrixGiftSearch extends MatrixGift
{
    public $matrix_user_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'matrix_id', 'left_level_1', 'left_level_2', 'left_level_3', 'left_level_4', 'left_level_5', 'left_level_6', 'buy_level_1', 'buy_level_2', 'buy_level_3', 'buy_level_4', 'buy_level_5', 'buy_level_6', 'matrix_user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MatrixGift::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'matrix_id' => $this->matrix_id,
            'left_level_1' => $this->left_level_1,
            'left_level_2' => $this->left_level_2,
            'left_level_3' => $this->left_level_3,
            'left_level_4' => $this->left_level_4,
            'left_level_5' => $this->left_level_5,
            'left_level_6' => $this->left_level_6,
            'buy_level_1' => $this->buy_level_1,
            'buy_level_2' => $this->buy_level_2,
            'buy_level_3' => $this->buy_level_3,
            'buy_level_4' => $this->buy_level_4,
            'buy_level_5' => $this->buy_level_5,
            'buy_level_6' => $this->buy_level_6,
        ]);

        if (!empty($this->matrix_user_id)){
            $user = User::find()->where(['id' => $this->matrix_user_id])->one();
            if (!empty($user)){
                $matrixIds = ArrayHelper::map(Matrix::find()->where(['user_id' => $user->id])->all(), 'id', 'id');
                $query->andFilterWhere(['in', 'matrix_id', $matrixIds]);
            }
        }

        return $dataProvider;
    }
}
