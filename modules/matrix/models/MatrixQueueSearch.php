<?php

namespace app\modules\matrix\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\matrix\models\MatrixQueue;

/**
 * MatrixQueueSearch represents the model behind the search form of `app\modules\matrix\models\MatrixQueue`.
 */
class MatrixQueueSearch extends MatrixQueue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'matrix_id', 'currency_id', 'user_id', 'matrix_root_id'], 'integer'],
            [['sum'], 'number'],
            [['status', 'type', 'date_add', 'date_update'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MatrixQueue::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'matrix_id' => $this->matrix_id,
            'currency_id' => $this->currency_id,
            'user_id' => $this->user_id,
            'matrix_root_id' => $this->matrix_root_id,
            'sum' => $this->sum,
            'date_add' => $this->date_add,
            'date_update' => $this->date_update,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
