<?php

namespace app\modules\matrix\models;

use Yii;
use app\modules\profile\models\User;

/**
 * This is the model class for table "{{%matrix_user_setting}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $matrix_id
 * @property integer $matrix_root_id
 * @property integer $active
 * @property string $date_add
 */
class MatrixUserSetting extends \yii\db\ActiveRecord
{
    public $slug;

    const ACTIVE_TRUE = 1;
    const ACTIVE_FALSE = 0;


    public function getActiveArray()
    {
        return [
            self::ACTIVE_TRUE => Yii::t('app', 'Активен'),
            self::ACTIVE_FALSE => Yii::t('app', 'Не активен'),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%matrix_user_setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'matrix_id', 'matrix_root_id', 'active'], 'integer'],
            [['date_add'], 'safe'],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getMatrixRoot()
    {
        return $this->hasOne(Matrix::className(), ['id' => 'matrix_root_id']);
    }

    public function getMatrix()
    {
        return $this->hasOne(Matrix::className(), ['id' => 'matrix_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'matrix_id' => Yii::t('app', 'Место'),
            'matrix_root_id' => Yii::t('app', 'Матрица'),
            'active' => Yii::t('app', 'Активно'),
            'sort' => Yii::t('app', 'Поставить следующее место'),
            'date_add' => Yii::t('app', 'Дата добавления'),
            'slug' => Yii::t('app', 'Хэш места'),
        ];
    }
}
