<?php

namespace app\modules\matrix\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\matrix\models\Matrix;

/**
 * Поиск среди пользователей
 * Class UserSearch
 * @package lowbase\user\models
 */
class MatrixStatSearch extends Matrix
{
    public $countPlaces;

    /**
     * Сценарии
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
                [['countPlaces', 'date_add', 'user_id'], 'safe'], 
            ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
                'countPlaces' => Yii::t('app', 'Кол-во купленых мест'),
            ]);
    }

    /**
     * Создает DataProvider на основе переданных данных
     * @param $params - параметры
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MatrixStatSearch::find()->select('*, count(user_id) as countPlaces');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // Сортировка по умолчанию
            'sort' => [
                'attributes' =>  ['countPlaces', 'user_id', 'date_add'],
                'defaultOrder' => ['date_add' => SORT_DESC],
            ],
        ]);

        $this->load($params);
        $query->groupBy(['user_id']);

        $query->andFilterWhere([
            'user_id' => $this->user_id,
        ]);

        if ($this->date_add != ''){
            $query->andFilterWhere(['between', 'date_add', date('Y-m-d 0:0:0', strtotime($this->date_add)), date('Y-m-d 23:59:59', strtotime($this->date_add))]);
        }

        return $dataProvider;
    }
}
