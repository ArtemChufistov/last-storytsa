<?php

namespace app\modules\matrix\models;

use app\modules\matrix\models\Matrix;
use Yii;

/**
 * This is the model class for table "matrix_pref".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property integer $matrix_id
 */
class MatrixPref extends \yii\db\ActiveRecord
{
    const KEY_COUNT_CHILDREN = 'place_count_children'; // количество детей у потомка
    const KEY_DEPTH = 'depth';  // глубина места
    const KEY_COST = 'place_cost'; // цена места
    const KEY_TAKE_FROM = 'take_from'; // откуда списываем
    const KEY_DESCENDANT1 = 'descendant1'; // действие при появлениии потомка 1
    const KEY_DESCENDANT2 = 'descendant2'; // действие при появлениии потомка 2
    const KEY_DESCENDANT3 = 'descendant3'; // действие при появлениии потомка 3
    const KEY_DESCENDANT4 = 'descendant4'; // действие при появлениии потомка 4
    const KEY_DESCENDANT5 = 'descendant5'; // действие при появлениии потомка 5
    const KEY_DESCENDANT6 = 'descendant6'; // действие при появлениии потомка 6
    const KEY_DESCENDANT7 = 'descendant7'; // действие при появлениии потомка 7
    const KEY_DESCENDANT8 = 'descendant8'; // действие при появлениии потомка 8
    const KEY_DESCENDANT9 = 'descendant9'; // действие при появлениии потомка 9
    const KEY_FIRST_PROGRAM = 'first_program'; // дополнительный ключ для первой старта программы
    const KEY_SECOND_PROGRAM = 'second_program'; // дополнительный ключ для первой старта программы
    const KEY_MAIN_CURRENCY = 'main_currency'; // основная валюта матрицы
    const KEY_CHARGE_TO_USER_1 = 'charge_to_user_1'; // кому начислить user_id
    const KEY_CHARGE_TO_USER_2 = 'charge_to_user_2'; // кому начислить user_id
    const KEY_CHARGE_TO_USER_3 = 'charge_to_user_3'; // кому начислить user_id
    const KEY_WITHOUT_PARENT_MECHANISM = 'without_parent_mechanism'; // Отключить механизм следования за пригласителем
    const KEY_CHARGE_INVITER = 'charge_parent'; // начислить пригласителю
    const KEY_CHARGE_INVITER_VOU = 'charge_parent_vou'; // начислить пригласителю
    const KEY_CHARGE_INVITER_VOU1 = 'charge_parent_vou1'; // начислить пригласителю 1
    const KEY_REFERAL_LEVEL_SUM = 'referal_level_sum'; // Сумма, распределяемая по рефералке
    const KEY_REFERAL_LEVEL_SUM_VOU = 'referal_level_sum_vou'; // Сумма, распределяемая по рефералке
    const KEY_FROM_MATRIX_1 = 'from_matrix_1'; // из матрицы 1
    const KEY_FROM_MATRIX_2 = 'from_matrix_2'; // из матрицы 2
    const KEY_FROM_MATRIX_3 = 'from_matrix_3'; // из матрицы 3
    const KEY_REINVEST_TO = 'reinvest_to';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matrix_pref';
    }

    public static function getKeys()
    {
        return [
            self::KEY_COUNT_CHILDREN => Yii::t('app', 'Кол-во детей'),
            self::KEY_DEPTH => Yii::t('app', 'Глубина'),
            self::KEY_COST => Yii::t('app', 'Цена места'),
            self::KEY_TAKE_FROM => Yii::t('app', 'Откуда списать'),
            self::KEY_DESCENDANT1 => Yii::t('app', 'Потомок 1'),
            self::KEY_DESCENDANT2 => Yii::t('app', 'Потомок 2'),
            self::KEY_DESCENDANT3 => Yii::t('app', 'Потомок 3'),
            self::KEY_DESCENDANT4 => Yii::t('app', 'Потомок 4'),
            self::KEY_DESCENDANT5 => Yii::t('app', 'Потомок 5'),
            self::KEY_DESCENDANT6 => Yii::t('app', 'Потомок 6'),
            self::KEY_DESCENDANT7 => Yii::t('app', 'Потомок 7'),
            self::KEY_DESCENDANT8 => Yii::t('app', 'Потомок 8'),
            self::KEY_DESCENDANT9 => Yii::t('app', 'Потомок 9'),
            self::KEY_FIRST_PROGRAM => Yii::t('app', 'Первая программа'),
            self::KEY_SECOND_PROGRAM => Yii::t('app', 'Вторая программа'),
            self::KEY_MAIN_CURRENCY => Yii::t('app', 'Основная валюта матрицы'),
            self::KEY_CHARGE_TO_USER_1 => Yii::t('app', 'Начислить пользователю 1'),
            self::KEY_CHARGE_TO_USER_2 => Yii::t('app', 'Начислить пользователю 2'),
            self::KEY_CHARGE_TO_USER_3 => Yii::t('app', 'Начислить пользователю 3'),
            self::KEY_WITHOUT_PARENT_MECHANISM => Yii::t('app', 'Отключить механизм следования за пригласителем'),
            self::KEY_CHARGE_INVITER => Yii::t('app', 'Начислить пригласителю'),
            self::KEY_CHARGE_INVITER_VOU => Yii::t('app', 'Начислить пригласителю ваучеры'),
            self::KEY_CHARGE_INVITER_VOU1 => Yii::t('app', 'Начислить пригласителю ваучеры 1'),
            self::KEY_FROM_MATRIX_1 => Yii::t('app', 'Из матрицы 1'),
            self::KEY_FROM_MATRIX_2 => Yii::t('app', 'Из матрицы 2'),
            self::KEY_FROM_MATRIX_3 => Yii::t('app', 'Из матрицы 3'),
            self::KEY_REFERAL_LEVEL_SUM => Yii::t('app', 'Сумма, распределяемая по рефералке'),
            self::KEY_REFERAL_LEVEL_SUM_VOU => Yii::t('app', 'Сумма, распределяемая по рефералке в ваучерах'),
            self::KEY_REINVEST_TO => Yii::t('app', 'Реинвест куда')
        ];
    }

    public function getKeyTitle()
    {
        $data = self::getKeys();

        return $data[$this->key];
    }

    public function getMatrix()
    {
        return $this->hasOne(Matrix::className(), ['id' => 'matrix_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['value', 'value1', 'value2'], 'string'],
            [['matrix_id'], 'integer'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    public static function getKeyDescription($key)
    {
        switch ($key) {
            case self::KEY_COUNT_CHILDREN:
                return Yii::t('app', '<strong>Значение - Целое число </strong></br> Количество возможных ног у одного места');
                break;

            case self::KEY_DEPTH:
                return Yii::t('app', '<strong>Значение - Целое число </strong></br> Глубина для начисления средств');
                break;

            case self::KEY_COST:
                return Yii::t('app', '<strong>Значение - Дробное число с плавающей точкой </strong></br> Цена места, валюта определяется из свойства "Основная валюта матрицы"');
                break;

            case self::KEY_TAKE_FROM:
                return Yii::t('app', '<strong>Значение - Функция обработчик, возможные значение (fromBalance)</strong></br> Функция списания средст с пользователя');
                break;

            case self::KEY_DESCENDANT1:
                return Yii::t('app', '<strong>Значение - Функция обработчик</strong></br><strong>Значение 1 - Целое число, ИД матрицы</strong></br> Функция управления поведением маркетинга');
                break;

            case self::KEY_DESCENDANT2:
                return Yii::t('app', '<strong>Значение - Функция обработчик</strong></br><strong>Значение 1 - Целое число, ИД матрицы</strong></br> Функция управления поведением маркетинга');
                break;

            case self::KEY_DESCENDANT3:
                return Yii::t('app', '<strong>Значение - Функция обработчик</strong></br><strong>Значение 1 - Целое число, ИД матрицы</strong></br> Функция управления поведением маркетинга');
                break;

            case self::KEY_DESCENDANT4:
                return Yii::t('app', '<strong>Значение - Функция обработчик</strong></br><strong>Значение 1 - Целое число, ИД матрицы</strong></br> Функция управления поведением маркетинга');
                break;

            case self::KEY_DESCENDANT5:
                return Yii::t('app', '<strong>Значение - Функция обработчик</strong></br><strong>Значение 1 - Целое число, ИД матрицы</strong></br> Функция управления поведением маркетинга');
                break;

            case self::KEY_DESCENDANT6:
                return Yii::t('app', '<strong>Значение - Функция обработчик</strong></br><strong>Значение 1 - Целое число, ИД матрицы</strong></br> Функция управления поведением маркетинга');
                break;

            case self::KEY_FIRST_PROGRAM:
                return Yii::t('app', '<strong>Флаг</strong></br> Определяет, какая матрица является входной');
                break;

            case self::KEY_SECOND_PROGRAM:
                return Yii::t('app', '<strong>Флаг</strong></br> Определяет, какая матрица является входной');
                break;

            case self::KEY_MAIN_CURRENCY:
                return Yii::t('app', '<strong>Значение - Целое число</strong></br> Ид валюты по которой вести расчет места');
                break;

            case self::KEY_CHARGE_TO_USER_1:
                return Yii::t('app', '<strong>Значение - Целое число, ИД пользователя</strong></br><strong>Значение 1 - Дробное число, сумма</strong></br><strong>Значение 2 - Функция обработчик</strong></br> Управления поведением маркетинга');
                break;

            case self::KEY_CHARGE_TO_USER_2:
                return Yii::t('app', '<strong>Значение - Целое число, ИД пользователя</strong></br><strong>Значение 1 - Дробное число, сумма</strong></br><strong>Значение 2 - Функция обработчик</strong></br> Управления поведением маркетинга');
                break;

            case self::KEY_CHARGE_TO_USER_3:
                return Yii::t('app', '<strong>Значение - Целое число, ИД пользователя</strong></br><strong>Значение 1 - Дробное число, сумма</strong></br><strong>Значение 2 - Функция обработчик</strong></br> Управления поведением маркетинга');
                break;

            default:
                return Yii::t('app', 'Нет описания');
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Ключ'),
            'value' => Yii::t('app', 'Значение'),
            'value1' => Yii::t('app', 'Значение 1'),
            'value2' => Yii::t('app', 'Значение 2'),
            'matrix_id' => Yii::t('app', 'Матрица'),
        ];
    }
}
