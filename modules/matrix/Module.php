<?php

namespace app\modules\matrix;

use yii\base\BootstrapInterface;

/**
 * matrix module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\matrix\controllers';

    public $controllerMap = [
        'evinizi' => 'app\modules\matrix\controllers\marketing\EviniziController',
        'bitmoda' => 'app\modules\matrix\controllers\marketing\BitmodaController',
        'bitinfo' => 'app\modules\matrix\controllers\marketing\BitinfoController',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            [
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'marketing/evinizi/<action>',
                'route' => 'matrix/evinizi/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'office/bitmoda/<action>',
                'route' => 'matrix/bitmoda/<action>',
            ],[
                'class' => 'yii\web\UrlRule', 
                'pattern' => 'office/bitinfo/<action>',
                'route' => 'matrix/bitinfo/<action>',
            ]
        ]);
    }
}
