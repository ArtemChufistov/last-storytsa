<?php

namespace app\modules\matrix\controllers;

use Yii;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixChangePlaceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\admin\controllers\AdminController;
use yii\filters\AccessControl;

/**
 * MatrixchangeplaceController implements the CRUD actions for Matrix model.
 */
class MatrixchangeplaceController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['index', 'searhbyslug'],
                        'allow' => true,
                        'roles' => ['matrixChangePlaceIndex'],
                    ],[
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['matrixChangePlaceView'],
                    ],[
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['matrixChangePlaceCreate'],
                    ],[
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['matrixChangePlaceUpdate'],
                    ],[
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['matrixChangePlaceDelete'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Matrix models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MatrixChangePlaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Matrix model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Matrix model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Matrix();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Matrix model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionSearhbyslug()
    {
        $result = [];

        if (empty(Yii::$app->request->get('q'))){
            return $result;
        }

        $userArray = Matrix::find()->where(['like', 'slug', Yii::$app->request->get('q')])->all();

        foreach($userArray as $user){
            $result[] =[
                'id' => $user->id,
                'slug' => $user->login,
            ];
        }

        echo json_encode(['results' => $result]);

    }

    public function actionSearhbymatrixid()
    {
        $result = [];

        if (empty(Yii::$app->request->get('q'))){
            return $result;
        }

        $matrixArray = Matrix::find()->where(['id' => Yii::$app->request->get('q')])->all();

        foreach($matrixArray as $matrix){
            $result[] =[
                'id' => $matrix->id,
                'matrix' => $matrix->id,
            ];
        }

        echo json_encode(['results' => $result]);

    }
    /**
     * Deletes an existing Matrix model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Matrix model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Matrix the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Matrix::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
