<?php

namespace app\modules\matrix\controllers\marketing;

use app\components\RefComponent;
use app\components\UserController;
use yii\web\Controller;


class BitcoinexpressController extends UserController {
	public $user;

	public function init() {
		$this->user = Yii::$app->user->identity;
		$this->view->params['user'] = Yii::$app->user;
		$this->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/office';
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'foreColor' => '3373751', //синий
			],
		];
	}
	// Маркетинг bitcoin-express
	public function actionBitcoinexpress($slug = '', $childSlug = '') {
		$paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_BITCOIN])->one();
		$currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
		$fromUserPayment = [];
		$toUserPayment = [];

		// если передано - отменить заявку
		if (!empty(Yii::$app->request->get('cancelOrder')) && !empty(Yii::$app->request->get('paymenthash'))) {
			$fromUserPayment = Payment::find()->where([
				'from_user_id' => $this->user->id,
				'hash' => Yii::$app->request->get('paymenthash'),
				'status' => [Payment::STATUS_WAIT]])->one();

			if (empty($fromUserPayment)) {
				Yii::$app->getSession()->setFlash('message', Yii::t('app', 'нет такой заявки'));
			} else {
				$fromUserPayment->status = Payment::STATUS_CANCEL;
				$fromUserPayment->save(false);
				return Yii::$app->getResponse()->redirect('/profile/office/bitcoinexpress?paymenthash=' . $payment->fromUserPayment);
			}
		}

		// если передавно - приоберсти место в матрице
		if (Yii::$app->request->post('marketing-matrix')) {
			try {
				$marketing = new BitcoinExpressMarketing;
				$marketing->set(['user' => $this->user, 'paySystem' => $paySystem, 'currency' => $currency]);
				$fromUserPayment = $marketing->getPaymentBuyPlace();

				return Yii::$app->getResponse()->redirect('/profile/office/bitcoinexpress?paymenthash=' . $fromUserPayment->hash);
			} catch (\Exception $e) {
				Yii::$app->getSession()->setFlash('message', $e->getMessage());
			}
		}

		// если передавно - приоберсти уровень в матрице
		if (!empty(Yii::$app->request->get('buy')) && !empty(Yii::$app->request->get('level'))) {
			try {
				$marketing = new BitcoinExpressMarketing;
				$marketing->set(['user' => $this->user, 'paySystem' => $paySystem, 'currency' => $currency, 'level' => Yii::$app->request->get('level')]);
				$fromUserPayment = $marketing->getPaymentBuyLevel();

				return Yii::$app->getResponse()->redirect('/profile/office/bitcoinexpress?paymenthash=' . $fromUserPayment->hash);
			} catch (\Exception $e) {
				Yii::$app->getSession()->setFlash('message', $e->getMessage());
			}
		}

		// отображение оплаты места с возможностью вставки хэша транзакции
		if (!empty(Yii::$app->request->get('paymenthash'))) {
			$fromUserPayment = Payment::find()->where(['from_user_id' => $this->user->id, 'hash' => Yii::$app->request->get('paymenthash')])->one();

			if (!empty($fromUserPayment)) {

				if ($fromUserPayment->load(Yii::$app->request->post())) {
					if ($fromUserPayment->validate()) {
						$fromUserPayment->status = Payment::STATUS_WAIT_SYSTEM;
						$fromUserPayment->save(true, ['transaction_hash', 'status']);
					}
				}
			} else {
				$toUserPayment = Payment::find()->where(['to_user_id' => $this->user->id, 'hash' => Yii::$app->request->get('paymenthash')])->one();

				// подтверждение получения платежа
				if (!empty(Yii::$app->request->post('gotPayment')) && $toUserPayment->status = Payment::STATUS_WAIT_SYSTEM) {
					$toUserPayment->status = Payment::PROCESS_SYSTEM;
					$toUserPayment->save(true, ['status']);
				}
			}
		}

		// какие кнопки при каких условиях показывать
		$marketing = new BitcoinExpressMarketing;
		$marketing->set(['user' => $this->user]);
		$buttons = $marketing->getButtons();

		$startPlace = '';
		$matrixPlace = '';
		if (!empty($childSlug)) {
			$startPlace = Matrix::find()->where(['slug' => $slug])->one();
			$currentPlace = Matrix::find()->where(['slug' => $childSlug])->one();
		} elseif (!empty($slug)) {
			$currentPlace = Matrix::find()->where(['slug' => $slug])->one();
			$startPlace = $currentPlace;
		} else {
			$currentPlace = Matrix::find()->where(['user_id' => $this->user->id])->one();
			$startPlace = $currentPlace;
		}

		if (!empty($startPlace)) {
			$currentLevel = $startPlace->getLevelTo($currentPlace);
		} else {
			$currentLevel = 0;
		}

		$fromUserPayments = Payment::find()->orderBy(['id' => SORT_DESC])->where(['from_user_id' => $this->user->id])->all();
		$toUserPayments = Payment::find()->orderBy(['id' => SORT_DESC])
			->where(['to_user_id' => $this->user->id])
			->andWhere(['<>', 'status', Payment::STATUS_CANCEL])
			->orderBy(['date_add' => SORT_DESC])->all();
		$parentUser = User::find()->where(['id' => $this->user->parent_id])->one();

		return $this->render('office/matrix', [
			'fromUserPayments' => $fromUserPayments,
			'fromUserPayment' => $fromUserPayment,
			'toUserPayments' => $toUserPayments,
			'toUserPayment' => $toUserPayment,
			'currentLevel' => $currentLevel,
			'currentPlace' => $currentPlace,
			'startPlace' => $startPlace,
			'parentUser' => $parentUser,
			'matrixTree' => $matrixTree,
			'paySystem' => $paySystem,
			'currency' => $currency,
			'buttons' => $buttons,
			'user' => $this->user,
		]);
	}
}