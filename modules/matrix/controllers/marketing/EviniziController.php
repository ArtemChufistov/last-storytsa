<?php

namespace app\modules\matrix\controllers\marketing;

use app\components\RefComponent;
use app\components\UserController;
use yii\web\Controller;


class EviniziController extends UserController {
	public $user;

	public function init() {
		$this->user = Yii::$app->user->identity;
		$this->view->params['user'] = Yii::$app->user;
		$this->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/office';
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'foreColor' => '3373751', //синий
			],
		];
	}

	// Ваучер
	public function actionVoucher() {
echo 123;exit;
		return $this->render('office/voucher', [
			'user' => $this->user,
		]);
	}
}