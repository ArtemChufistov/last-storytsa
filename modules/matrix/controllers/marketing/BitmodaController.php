<?php

namespace app\modules\matrix\controllers\marketing;

use app\components\RefComponent;
use app\components\UserController;
use app\modules\matrix\models\MatrixCategory;
use app\modules\profile\models\YoutubeVideo;
use app\modules\profile\models\UserShowedVideo;
use app\modules\mainpage\models\Preference;
use app\modules\matrix\components\BitmodaMarketing;
use app\modules\matrix\models\MatrixQueue;
use app\modules\profile\models\YoutubeStat;
use app\modules\finance\models\Currency;
use app\modules\finance\models\UserBalance;
use app\modules\finance\models\Transaction;
use yii\db\Expression;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use Yii;

class BitmodaController extends UserController {
	public $user;

	public function init() {
		$this->user = Yii::$app->user->identity;
		$this->view->params['user'] = Yii::$app->user;
		$this->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/office';
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'foreColor' => '3373751', //синий
			],
		];
	}

	// Маркетинг
	public function actionMarketing() {

		$currencyBtc = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
		$currencyVou = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();

		// покупка места в матрице
		if (!empty(Yii::$app->request->get('doJob'))) {
			
			try {

				$matrixCategory = 1;	
				$matrixCategory = MatrixCategory::find()->where(['id' => $matrixCategory])->one();
				$rootMatrixes = $matrixCategory->getMatrixes()->all();

				foreach ($rootMatrixes as $rootMatrixItem) {
					if (!empty($marketingData['rootId']) && $rootMatrixItem->id == $marketingData['rootId']) {
						$rootMatrixMarketing = $rootMatrixItem;
					}
				}

				if (empty($rootMatrixMarketing)) {
					$rootMatrixMarketing = $rootMatrixes[0];
				}
				$prefMarketing = Preference::find()->where(['key' => Preference::KEY_MATRIX_MARKETING])->one();

				if (!empty($prefMarketing)) {
					$marketingClassName = $prefMarketing->value;
				} else {
					$marketingClassName = 'Marketing';
				}
				$marketing = new $marketingClassName;
				$marketing->set(['user' => $this->user, 'rootMatrix' => $rootMatrixMarketing]);
				$queue = $marketing->addToQueue();

				if ($marketing->handleQueue($queue)) {
					$queue->status = MatrixQueue::STATUS_FINISH;
					$queue->save();
				}

				Yii::$app->getSession()->setFlash('message', Yii::t('app', 'место успешно приобретено'));
			} catch (\Exception $e) {
				echo $e->getMessage();exit;
				Yii::$app->getSession()->setFlash('message', $e->getMessage());
			}
		}

		/* видео */
		$currentDate = date('Y-m-d');
		$userShowedVideo = UserShowedVideo::find()->where(['user_id' => $this->user->id])->andWhere(['date' => $currentDate])->one();

		if (empty($userShowedVideo)){
			$userShowedVideo = new UserShowedVideo;
			$userShowedVideo->user_id = $this->user->id;
			$userShowedVideo->date = $currentDate;
			$userShowedVideo->save(false);
		}

		$userBalance = UserBalance::find()->where(['currency_id' => $currencyVou->id])->andWhere(['user_id' => $this->user->id])->one();
		$videoCost = Preference::find()->where(['key' => Preference::KEY_VIDEO_COST])->one();

		if ($userShowedVideo->count <= 5){
			if (!empty($userBalance) && $userBalance->value > $videoCost->value){
		      $token = $_SESSION['app\modules\profile\components\oauth\GoogleOAuth_google_token']->getParams()['access_token'];

		      try{
			      
			      $client = new \Google_Client();
			      $client->setAccessToken($token);

			      $service = new \Google_Service_YouTube($client);

			      $userChannels = $service->channels->listChannels(
			          "contentDetails",
			          array(
			              "mine" => "true",
			              "fields" => "items/id"
			          )
			      );

		      }catch(\Google_Service_Exception $e){
		      	return $this->redirect(['/logout']);
		      }

		      $userChannelId = $userChannels['items'][0]['id'];
		      $notFullPayVideo = YoutubeStat::find()->where(['showed' => 0])->andWhere(['user_id' => $userChannelId])->one();

		      if (empty($notFullPayVideo)){
		      	$notFullPayVideo = YoutubeStat::find()->where(['liked' => 0])->andWhere(['user_id' => $userChannelId])->one();
		      	if (empty($notFullPayVideo)){
		      		$fullPayVideoArray = ArrayHelper::map(YoutubeStat::find()
		      			->where(['showed' => 1])
		      			->andWhere(['liked' => 1])
		      			->andWhere(['user_id' => $userChannelId])->all(), 'video_id', 'video_id');

		      		$youtubeStatVideo = YoutubeVideo::find()->where(['not in', 'video_id', $fullPayVideoArray])->orderBy(new Expression('rand()'))->one();

		      		$videoId = $youtubeStatVideo->video_id;
		      	}else{
		      		$videoId = $notFullPayVideo->video_id;
		      	}
		      }else{
		      	$videoId = $notFullPayVideo->video_id;
		      }

		      $video = $service->videos->listVideos("snippet", ['id' => $videoId])->getItems()[0]->getSnippet();
		      $channelId = $video->getChannelId();

		      $timeZone = new \DateTimeZone('Europe/Moscow');
		      $dateTime = new \DateTime();
		      $dateTime->setTimezone($timeZone);
		      $dateTimeString = $dateTime->format('Y\-m\-d\ h:i:s');

		      if (!($youtubeStat = YoutubeStat::find()->where(['user_id' => $userChannelId, 'video_id' => $videoId])->one())) {
		        $youtubeStat = new YoutubeStat();
		        $youtubeStat->loadDefaultValues();
		        $youtubeStat->user_id = $userChannelId;
		        $youtubeStat->video_id = $videoId;
		        $youtubeStat->date_add = $dateTimeString;
		      }

		      if ($youtubeStat->validate() && isset(Yii::$app->request->post()['YoutubeStat']['play_button']) && !empty(Yii::$app->request->post()['YoutubeStat']['play_button']) && Yii::$app->request->post()['YoutubeStat']['play_button']) {
		        $youtubeStat->showed = 1;
		        $youtubeStat->date_showed = $dateTimeString;
		      } elseif ($youtubeStat->validate() && isset(Yii::$app->request->post()['YoutubeStat']['like_button']) && !empty(Yii::$app->request->post()['YoutubeStat']['like_button']) && Yii::$app->request->post()['YoutubeStat']['like_button']) {
		        $service->videos->rate($videoId, "like");
		        $youtubeStat->liked = 1;
		        $youtubeStat->date_liked = $dateTimeString;
		      } elseif ($youtubeStat->validate() && isset(Yii::$app->request->post()['YoutubeStat']['subscribe_button']) && !empty(Yii::$app->request->post()['YoutubeStat']['subscribe_button']) && Yii::$app->request->post()['YoutubeStat']['subscribe_button']) {
		        $resourceId = new Google_Service_YouTube_ResourceId();
		        $resourceId->setKind('youtube#channel');
		        $resourceId->setChannelId('UCfGCWB2fxKDGGBkBG_Dt2Ow');

		        $snippet = new Google_Service_YouTube_SubscriptionSnippet();
		        $snippet->setResourceId($resourceId);

		        $postBody = new Google_Service_YouTube_Subscription();
		        $postBody->setSnippet($snippet);

		        $response = $service->subscriptions->insert("snippet", $postBody, []);
		      }
		      $youtubeStat->save();

		      if ($youtubeStat->liked == 1 && $youtubeStat->showed == 1){
	      	    $transaction = new Transaction;
		        $transaction->sum = -$videoCost->value;
		        $transaction->type = Transaction::TYPE_FOR_VIDEO;
		        $transaction->to_user_id = $this->user->id;
		        $transaction->currency_id = $currencyVou->id;
		        $transaction->date_add = date('Y-m-d H:i:s');

		        $transaction->save(false);

	      	    $transaction = new Transaction;
		        $transaction->sum = $videoCost->value;
		        $transaction->type = Transaction::TYPE_FOR_VIDEO;
		        $transaction->to_user_id = $this->user->id;
		        $transaction->currency_id = $currencyBtc->id;
		        $transaction->date_add = date('Y-m-d H:i:s');

		        $transaction->save(false);

		        $this->user->withNoActive()->recalclulateBalances();
		        $userShowedVideo->count++;
		        $userShowedVideo->save(false);

		        $this->refresh();
		      }
			}
		}

		if (!empty($userChannelId)){
			$youTubeuserid = $userChannelId;
		}else{
			$youTubeuserid = 0;
		}

		if (empty($youtubeStat)){
			$youtubeStat = new YoutubeStat();
		}

		if (empty($videoId)){
			$videoId = false;
		}
		/* end of видео*/

		return $this->render('office/marketing/bitmoda', [
			'user' => $this->user,
			'youtubeStat' => $youtubeStat,
			'videoId' => $videoId,
			'youTubeuserid' => $youTubeuserid
		]);
	}

	// Видео раздел
	public function actionVideo() {

        $searchModel = new YoutubeVideo();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('office/video', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'user' => $this->user,
		]);
	}
}