<?php

namespace app\modules\matrix\controllers\marketing;

use app\components\RefComponent;
use app\components\UserController;
use app\modules\matrix\models\MatrixCategory;
use app\modules\profile\models\YoutubeVideo;
use app\modules\profile\models\UserShowedVideo;
use app\modules\mainpage\models\Preference;
use app\modules\matrix\components\BitmodaMarketing;
use app\modules\matrix\models\MatrixQueue;
use app\modules\profile\models\YoutubeStat;
use app\modules\finance\models\Currency;
use app\modules\finance\models\UserBalance;
use app\modules\finance\models\Transaction;
use yii\db\Expression;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use Yii;

class BitinfoController extends UserController {
	public $user;

	public function init() {
		$this->user = Yii::$app->user->identity;
		$this->view->params['user'] = Yii::$app->user;
		$this->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/office';
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'foreColor' => '3373751', //синий
			],
		];
	}

	// Маркетинг
	public function actionMarketing() {

		return $this->render('office/marketing/bitinfo', [
			'user' => $this->user,

		]);
	}
}