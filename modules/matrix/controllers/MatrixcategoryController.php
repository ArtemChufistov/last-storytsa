<?php

namespace app\modules\matrix\controllers;

use Yii;
use app\modules\matrix\models\MatrixCategory;
use app\modules\matrix\models\MatrixCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\admin\controllers\AdminController;
use yii\filters\AccessControl;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixSearch;
/**
 * MatrixcategoryController implements the CRUD actions for MatrixCategory model.
 */
class MatrixcategoryController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['matrixCategoryIndex'],
                    ],[
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['matrixCategoryView'],
                    ],[
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['matrixCategoryCreate'],
                    ],[
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['matrixCategoryUpdate'],
                    ],[
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['matrixCategoryDelete'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MatrixCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MatrixCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MatrixCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MatrixCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MatrixCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MatrixCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MatrixCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MatrixCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MatrixCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MatrixCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
