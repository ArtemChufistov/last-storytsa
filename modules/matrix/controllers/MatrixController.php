<?php

namespace app\modules\matrix\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\admin\controllers\AdminController;
use yii\filters\AccessControl;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixSearch;
/**
 * MatrixController implements the CRUD actions for Matrix model.
 */
class MatrixController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['index', 'fulltree'],
                        'allow' => true,
                        'roles' => ['MatrixIndex'],
                    ],[
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['MatrixView'],
                    ],[
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['matrixCreatePlaceIndex'],
                    ],[
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['MatrixUpdate'],
                    ],[
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['MatrixDelete'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new MatrixSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFulltree($id, $toLevel = 5)
    {
        $this->layout = 'fulltree-layout';

        $matrix = Matrix::find()->where(['id' => $id])->one();

        if (!empty(Yii::$app->request->post())) {
            $toLevel = Yii::$app->request->post('toLevel');
        }

        $fields = ['name' => 'login', 'title' => 'place'];
        $data = $matrix->fullSortedTreeAsParentArray($fields, $toLevel);

        $treeArray = array();
        $arrTmp = array();

        foreach ($data as $item) {
            $parentId = $item['parentId'];
            $id = $item['id'];

            if ($parentId  == 0)
            {
                $treeArray[$id] = $item;
                $arrTmp[$id] = &$treeArray[$id];
            }
            else 
            {
                if (!empty($arrTmp[$parentId])) 
                {
                    $arrTmp[$parentId]['children'][$id] = $item;
                    $arrTmp[$id] = &$arrTmp[$parentId]['children'][$id];
                }
            }
        }

        unset($arrTmp);

        $resultTreeArray = [];
        foreach ($treeArray as $treeArray) {
            $resultTreeArray = $treeArray;
        }

        $treeArray = $this->arrayValuesRecursive($resultTreeArray);

        $maxDepth = $matrix->descendants()->select('`depth`')->orderBy(['depth' => SORT_DESC])->one();

        if (!empty($maxDepth)){
            $maxDepth = $maxDepth->depth;
        }

        return $this->render('fulltree', [
            'treeArray' => $treeArray,
            'maxDepth' => $maxDepth,
            'toLevel' => $toLevel,
            'matrix' => $matrix

        ]);
    }

    /**
     * Displays a single Matrix model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Matrix model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Matrix();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Matrix model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Matrix model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Matrix model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Matrix the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Matrix::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function arrayValuesRecursive($arr)
    {
      foreach ($arr as $key => $value)
      {
        if (is_array($value))
        {
          $arr[$key] = $this->arrayValuesRecursive($value);
        }
      }

      if (isset($arr['children']))
      {
        $arr['children'] = array_values($arr['children']);
      }

      return $arr;
    }
}
