<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixGift */

$this->title = Yii::t('app', 'Добавить уровневые данные');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Уровневые данные'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-gift-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
