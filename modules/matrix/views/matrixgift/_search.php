<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixGiftSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="matrix-gift-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'matrix_id') ?>

    <?= $form->field($model, 'left_level_1') ?>

    <?= $form->field($model, 'left_level_2') ?>

    <?= $form->field($model, 'left_level_3') ?>

    <?php // echo $form->field($model, 'left_level_4') ?>

    <?php // echo $form->field($model, 'left_level_5') ?>

    <?php // echo $form->field($model, 'left_level_6') ?>

    <?php // echo $form->field($model, 'buy_level_1') ?>

    <?php // echo $form->field($model, 'buy_level_2') ?>

    <?php // echo $form->field($model, 'buy_level_3') ?>

    <?php // echo $form->field($model, 'buy_level_4') ?>

    <?php // echo $form->field($model, 'buy_level_5') ?>

    <?php // echo $form->field($model, 'buy_level_6') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
