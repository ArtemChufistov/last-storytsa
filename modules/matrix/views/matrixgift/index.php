<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\matrix\models\MatrixGiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Уровневые данные');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-gift-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
              <?= Html::a(Yii::t('app', 'Добавить уровневые данные'), ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <?php Pjax::begin(); ?>
                <div class="box-body no-padding">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            'matrix_id',
                            [
                              'attribute' => 'matrix_user_id',
                              'format' => 'raw',
                              'label' => Yii::t('app', 'Участник'),
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[matrix_user_id]',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Введите логин'),
                                  ],
                                  'pluginOptions' => [
                                      'minimumInputLength' => 2,
                                      'ajax' => [
                                          'url' => Url::to(['/profile/backuser/searhbylogin']),
                                          'dataType' => 'json',
                                          'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                      ],
                                      'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                      'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                      'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                                  ]
                              ]),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_matrix_user-id', ['model' => $model]);},
                            ],
                            [
                                'attribute' => 'left_level_1',
                                'format' => 'raw',
                                'label' => Yii::t('app', '1 ур инфо')
                            ],[
                                'attribute' => 'left_level_2',
                                'format' => 'raw',
                                'label' => Yii::t('app', '2 ур инфо')
                            ],[
                                'attribute' => 'left_level_3',
                                'format' => 'raw',
                                'label' => Yii::t('app', '3 ур инфо')
                            ],[
                                'attribute' => 'left_level_3',
                                'format' => 'raw',
                                'label' => Yii::t('app', '3 ур инфо')
                            ],[
                                'attribute' => 'left_level_4',
                                'format' => 'raw',
                                'label' => Yii::t('app', '4 ур инфо')
                            ],[
                                'attribute' => 'left_level_4',
                                'format' => 'raw',
                                'label' => Yii::t('app', '4 ур инфо')
                            ],[
                                'attribute' => 'left_level_5',
                                'format' => 'raw',
                                'label' => Yii::t('app', '5 ур инфо')
                            ],[
                                'attribute' => 'left_level_6',
                                'format' => 'raw',
                                'label' => Yii::t('app', '6 ур инфо')
                            ],[
                                'attribute' => 'buy_level_1',
                                'format' => 'raw',
                                'label' => Yii::t('app', '1 ур данные')
                            ],[
                                'attribute' => 'buy_level_2',
                                'format' => 'raw',
                                'label' => Yii::t('app', '2 ур данные')
                            ],[
                                'attribute' => 'buy_level_3',
                                'format' => 'raw',
                                'label' => Yii::t('app', '3 ур данные')
                            ],[
                                'attribute' => 'buy_level_4',
                                'format' => 'raw',
                                'label' => Yii::t('app', '4 ур данные')
                            ],[
                                'attribute' => 'buy_level_5',
                                'format' => 'raw',
                                'label' => Yii::t('app', '5 ур данные')
                            ],[
                                'attribute' => 'buy_level_6',
                                'format' => 'raw',
                                'label' => Yii::t('app', '6 ур данные')
                            ],
                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                </div>
            <?php Pjax::end(); ?>
          </div>
        </div>
    </div>
</div>

