<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixGift */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Уровневые данные'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-gift-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="box-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'matrix_id',
                    [
                      'attribute' => 'matrix_user_id',
                      'format' => 'raw',
                      'label' => Yii::t('app', 'Участник'),
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_matrix_user-id', ['model' => $model]);},
                    ],
                    [
                        'attribute' => 'left_level_1',
                        'format' => 'raw',
                        'label' => Yii::t('app', '1 ур инфо')
                    ],[
                        'attribute' => 'left_level_2',
                        'format' => 'raw',
                        'label' => Yii::t('app', '2 ур инфо')
                    ],[
                        'attribute' => 'left_level_3',
                        'format' => 'raw',
                        'label' => Yii::t('app', '3 ур инфо')
                    ],[
                        'attribute' => 'left_level_3',
                        'format' => 'raw',
                        'label' => Yii::t('app', '3 ур инфо')
                    ],[
                        'attribute' => 'left_level_4',
                        'format' => 'raw',
                        'label' => Yii::t('app', '4 ур инфо')
                    ],[
                        'attribute' => 'left_level_4',
                        'format' => 'raw',
                        'label' => Yii::t('app', '4 ур инфо')
                    ],[
                        'attribute' => 'left_level_5',
                        'format' => 'raw',
                        'label' => Yii::t('app', '5 ур инфо')
                    ],[
                        'attribute' => 'left_level_6',
                        'format' => 'raw',
                        'label' => Yii::t('app', '6 ур инфо')
                    ],[
                        'attribute' => 'buy_level_1',
                        'format' => 'raw',
                        'label' => Yii::t('app', '1 ур данные')
                    ],[
                        'attribute' => 'buy_level_2',
                        'format' => 'raw',
                        'label' => Yii::t('app', '2 ур данные')
                    ],[
                        'attribute' => 'buy_level_3',
                        'format' => 'raw',
                        'label' => Yii::t('app', '3 ур данные')
                    ],[
                        'attribute' => 'buy_level_4',
                        'format' => 'raw',
                        'label' => Yii::t('app', '4 ур данные')
                    ],[
                        'attribute' => 'buy_level_5',
                        'format' => 'raw',
                        'label' => Yii::t('app', '5 ур данные')
                    ],[
                        'attribute' => 'buy_level_6',
                        'format' => 'raw',
                        'label' => Yii::t('app', '6 ур данные')
                    ],
                ],
            ]) ?>

        </div>
      </div>
    </div>
  </div>
</div>
