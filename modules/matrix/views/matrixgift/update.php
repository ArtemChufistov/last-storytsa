<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixGift */

$this->title = Yii::t('app', 'Редактирование уровневых данных: ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Уровневые данные'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="matrix-gift-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
