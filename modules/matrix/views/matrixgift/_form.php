<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixGift */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
            <div class="matrix-gift-form">

                <?php $form = ActiveForm::begin(); ?>

                <?php if (!empty($model)): ?>
                    <?php $matrix = $model->getMatrix()->one();?>

                    <?php if (!empty($matrix)): ?>
                        <div class="form-group">
                            <?= Html::label(Yii::t('app', 'Логин участника'), Yii::t('app', 'Логин участника'), ['class' => 'control-label']) ?>
                            <?= Html::input('text', '', $matrix->getUser()->one()->login, ['class' => 'form-control', 'readonly' => true]) ?>
                        </div>
                    <?php endif;?>
                <?php endif;?>

                <?= $form->field($model, 'matrix_id')->textInput() ?>

                <?= $form->field($model, 'left_level_1')->textInput() ?>

                <?= $form->field($model, 'left_level_2')->textInput() ?>

                <?= $form->field($model, 'left_level_3')->textInput() ?>

                <?= $form->field($model, 'left_level_4')->textInput() ?>

                <?= $form->field($model, 'left_level_5')->textInput() ?>

                <?= $form->field($model, 'left_level_6')->textInput() ?>

                <?= $form->field($model, 'buy_level_1')->textInput() ?>

                <?= $form->field($model, 'buy_level_2')->textInput() ?>

                <?= $form->field($model, 'buy_level_3')->textInput() ?>

                <?= $form->field($model, 'buy_level_4')->textInput() ?>

                <?= $form->field($model, 'buy_level_5')->textInput() ?>

                <?= $form->field($model, 'buy_level_6')->textInput() ?>
                
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
      </div>
    </div>
</div>
