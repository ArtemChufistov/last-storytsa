<?php

use lowbase\user\UserAsset;
use app\assets\AdminAsset;
use app\assets\OfficeEndAppAsset;
use yii\helpers\Html;

$adminAsset = AdminAsset::register($this);
$userAsset = UserAsset::register($this);
$officeEndAppAsset = OfficeEndAppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <link rel="stylesheet" href="https://cdn.rawgit.com/FortAwesome/Font-Awesome/master/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/jquery.orgchart.css">
        <link rel="stylesheet" href="/css/style.css">

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
        <script type="text/javascript" src="/js/jquery.orgchart.js"></script>
    </head>
  <body>
    <?php $this->beginBody() ?>
        <?php echo $content;?>
    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>