<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixPref */

$this->title = Yii::t('app', 'Редактировать матричное свойство: ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Матричные свойства'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="matrix-pref-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
