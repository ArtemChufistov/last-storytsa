<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixPref;
/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixPref */
/* @var $form yii\widgets\ActiveForm */
?>
<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
			<div class="matrix-pref-form">

			    <?php $form = ActiveForm::begin(); ?>


                <?php echo $form->field($model, 'key')->widget(Select2::classname(), [
				    'data' => MatrixPref::getKeys(),
				    'options' => ['placeholder' => 'Выберите ключ...'],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
                ?>

			    <?= $form->field($model, 'value')->textarea(['rows' => 3]) ?>

			    <?= $form->field($model, 'value1')->textarea(['rows' => 3]) ?>
			    
			    <?= $form->field($model, 'value2')->textarea(['rows' => 3]) ?>

			    <?php echo $form->field($model, 'matrix_id')->widget(Select2::classname(), [
				    'data' => ArrayHelper::map(Matrix::find()->roots()->all(), 'id', 'name'),
				    'options' => ['placeholder' => 'Выберите матрицу...'],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]);
                ?>

			    <div class="form-group">
			        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			    </div>

			    <?php ActiveForm::end(); ?>

			</div>
      </div>
    </div>
</div>

