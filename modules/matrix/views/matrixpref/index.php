<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\matrix\models\MatrixPref;
use app\modules\matrix\models\Matrix;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\matrix\models\MatrixPrefSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Матричные свойства');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-pref-index">

    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="box box-success">
            <div class="box-header">
              <?= Html::a(Yii::t('app', 'Добавить свойство'), ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <?php Pjax::begin(); ?>

                <div class="box-body no-padding">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            [
                            'attribute' => 'key',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'name' => (new ReflectionClass($searchModel))->getShortName() . '[key]',
                                'value' => $searchModel->key,
                                'attribute' => 'key',
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'data' => array_merge([Yii::t('app', 'Не выбрано')], MatrixPref::getKeys()),
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Выберите'),
                                    'style' => ['width' => '150px'],
                                ],]
                              ),
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_matrix-pref-key', ['model' => $model]);},
                            ],
                            'value:ntext',
                            'value1:ntext',
                            'value2:ntext',
                            [
                            'attribute' => 'matrix_id',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'name' => (new ReflectionClass($searchModel))->getShortName() . '[matrix_id]',
                                'value' => $searchModel->key,
                                'attribute' => 'key',
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'data' => ArrayHelper::map(Matrix::find()->roots()->all(), 'id', 'name'),
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Выберите'),
                                    'style' => ['width' => '150px'],
                                ],]
                              ),
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_pref-matrix-id', ['model' => $model]);},
                            ],

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            <?php Pjax::end(); ?>
          </div>
        </div>

        <div class="col-md-4">
            <?php foreach(MatrixPref::getKeys() as $key => $keyTitle):?>
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $keyTitle;?></h3>
                        <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" type="button" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                        </button>
                        </div>
                    </div>
                    <div class="box-body" style="display: block;"><?php echo MatrixPref::getKeyDescription($key);?></div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
