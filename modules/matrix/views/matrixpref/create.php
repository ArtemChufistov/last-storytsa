<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixPref */

$this->title = Yii::t('app', 'Добавить матричное свойство');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Матричные свойства'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-pref-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
