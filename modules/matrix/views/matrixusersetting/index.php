<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixUserSetting;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\matrix\models\MatrixUserSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Matrix User Settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="matrix-user-setting-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">

                <p>
                    <?= Html::a(Yii::t('app', 'Добавить галку'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        [
                              'attribute' => 'user_id',
                              'format' => 'raw',
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[user_id]',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Введите логин'),
                                  ],
                                  'pluginOptions' => [
                                      'minimumInputLength' => 2,
                                      'ajax' => [
                                          'url' => Url::to(['/profile/backuser/searhbylogin']),
                                          'dataType' => 'json',
                                          'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                      ],
                                      'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                      'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                      'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                                  ]
                              ]),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
                        ],
                        [
                              'attribute' => 'slug',
                              'format' => 'raw',
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_slug', ['model' => $model]);},
                        ],
                        [
                          'attribute' => 'matrix_root_id',
                          'format' => 'raw',
                          'filter' => Select2::widget([
                              'name' => (new ReflectionClass($searchModel))->getShortName() . '[matrix_root_id]',
                              'value' => $searchModel->matrix_root_id,
                              'attribute' => 'matrix_root_id',
                              'theme' => Select2::THEME_BOOTSTRAP,
                              'data' => array_merge([Yii::t('app', 'Не выбрано')], ArrayHelper::map(Matrix::find()->where('parent_id = id')->all(), 'id', 'name')),
                              'options' => [
                                  'placeholder' => Yii::t('app', 'Выберите'),
                                  'style' => ['width' => '150px'],
                              ],]
                            ),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_matrix_root', ['model' => $model]);},
                        ],
                        [
                          'attribute' => 'active',
                          'format' => 'raw',
                          'filter' => Select2::widget([
                              'name' => (new ReflectionClass($searchModel))->getShortName() . '[active]',
                              'value' => $searchModel->active,
                              'attribute' => 'active',
                              'theme' => Select2::THEME_BOOTSTRAP,
                              'data' => array_merge([Yii::t('app', 'Не выбрано')],MatrixUserSetting::getActiveArray()),
                              'options' => [
                                  'placeholder' => Yii::t('app', 'Выберите'),
                                  'style' => ['width' => '150px'],
                              ],]
                            ),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_active', ['model' => $model]);},
                        ],
                        'sort',
                        [
                            'attribute' => 'date_add',
                            'format' => 'raw',
                            'filter' => DatePicker::widget([
                                'model'=>$searchModel,
                                'attribute'=>'date_add',
                                'options' => ['class' => 'form-control'],
                                'language' => 'ru',
  
                            ]),
                            'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_date-add', ['model' => $model]);},
                          ],

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>

            </div>
          </div>
        </div>
    </div>
</div>