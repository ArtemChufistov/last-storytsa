<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixUserSetting */

$this->title = Yii::t('app', 'Create Matrix User Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Matrix User Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-user-setting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
