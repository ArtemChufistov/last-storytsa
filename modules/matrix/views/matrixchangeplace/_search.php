<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixChangePlaceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="matrix-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'parent_id') ?>

    <?= $form->field($model, 'sum') ?>

    <?= $form->field($model, 'currency_id') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'sort') ?>

    <?php // echo $form->field($model, 'date_add') ?>

    <?php // echo $form->field($model, 'wallet_from') ?>

    <?php // echo $form->field($model, 'wallet_to') ?>

    <?php // echo $form->field($model, 'transaction') ?>

    <?php // echo $form->field($model, 'count_submit') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'root_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
