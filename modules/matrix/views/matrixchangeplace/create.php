<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\Matrix */

$this->title = Yii::t('app', 'Добавление места');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Смена мест'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
