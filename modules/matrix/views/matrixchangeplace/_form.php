<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\payment\models\Currency;
/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\Matrix */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
            <div class="matrix-form">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'id')->textInput() ?>

                <?php echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
                    'initValueText' => $model->parent_id,
                    'value' => $model->parent_id,
                    'options' => ['placeholder' => Yii::t('app', 'Введите хэш места')],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['/matrix/matrixchangeplace/searhbymatrixid']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(matr) { return matr.id; }'),
                        'templateSelection' => new JsExpression('function (matr) { return matr.matrix || matr.text; }'),
                    ],
                ]);?>

                <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'initValueText' => $model->getUser()->one()->login,
                    'value' => $model->getUser()->one()->id,
                    'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'minimumInputLength' => 2,
                        'ajax' => [
                            'url' => Url::to(['/profile/backuser/searhbylogin']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.login; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                    ],
                ]);?>

                <?= $form->field($model, 'sum')->textInput() ?>

                <?php echo $form->field($model, 'currency_id')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Выберите валюту...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>

                <?= $form->field($model, 'status')->textInput() ?>

                <?= $form->field($model, 'sort')->textInput() ?>

                <?= $form->field($model, 'transaction')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'count_submit')->textInput() ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'root_id')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Сохранить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
      </div>
    </div>
</div>
