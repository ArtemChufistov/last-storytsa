<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixCategory */

$this->title = Yii::t('app', 'Редактирование категории матриц: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Категории матриц'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="matrix-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
