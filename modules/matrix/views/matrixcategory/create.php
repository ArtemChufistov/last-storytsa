<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixCategory */

$this->title = Yii::t('app', 'Добавление категории матриц');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Категории матриц'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
