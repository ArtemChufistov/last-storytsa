<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use app\modules\matrix\models\Matrix;
use app\modules\payment\models\Currency;
use app\modules\matrix\models\MatrixQueue;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixQueue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
            <div class="matrix-queue-form">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'matrix_id')->textInput() ?>

                <?php echo $form->field($model, 'currency_id')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Выберите валюту...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>

                <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'initValueText' => $model->getUser()->one()->login,
                    'value' => $model->getUser()->one()->id,
                    'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'minimumInputLength' => 2,
                        'ajax' => [
                            'url' => Url::to(['/profile/backuser/searhbylogin']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.login; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                    ],
                ]);?>

                <?php echo $form->field($model, 'matrix_root_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Matrix::find()->roots()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Выберите матрицу...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'sum')->textInput() ?>

                <?php echo $form->field($model, 'status')->widget(Select2::classname(), [
                    'data' => MatrixQueue::getStatusArray(),
                    'options' => ['placeholder' => 'Выберите статус...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>

                <?php echo $form->field($model, 'type')->widget(Select2::classname(), [
                    'data' => MatrixQueue::getTypes(),
                    'options' => ['placeholder' => 'Выберите тип...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>

                <?= $form->field($model, 'date_add')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => $model->getAttributeLabel('date_add')],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>

                <?= $form->field($model, 'date_update')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
      </div>
    </div>
</div>
