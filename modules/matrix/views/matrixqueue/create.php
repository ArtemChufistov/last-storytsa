<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixQueue */

$this->title = Yii::t('app', 'Добавление в очередь');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Матричная очередь'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-queue-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
