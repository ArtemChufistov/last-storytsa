<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\MatrixQueue */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Матричная очередь'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-queue-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">

            <p>
                <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'matrix_id',
                    [
                    'attribute' => 'currency_id',
                    'format' => 'raw',
                    'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_currency_id', ['model' => $model]);},
                    ],[
                      'attribute' => 'user_id',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
                    ],[
                    'attribute' => 'matrix_root_id',
                    'format' => 'raw',
                    'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_matrix-root-id', ['model' => $model]);},
                    ],[
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_matrix-queu-status', ['model' => $model]);},
                    ],
                ],
            ]) ?>

        </div>
      </div>
    </div>
  </div>
</div>
