<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\finance\models\Currency;
use app\modules\matrix\models\MatrixCategory;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\Matrix */
/* @var $form yii\widgets\ActiveForm */

?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
            <div class="matrix-form">

                <?php $form = ActiveForm::begin(); ?>

                <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'initValueText' => !empty($model->getUser()->one()) ? $model->getUser()->one()->login : '',
                    'value' => !empty($model->getUser()->one()) ? $model->getUser()->one()->login : '',
                    'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'minimumInputLength' => 2,
                        'ajax' => [
                            'url' => Url::to(['/profile/backuser/searhbylogin']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.login; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                    ],
                ]);?>

                <?= $form->field($model, 'sum')->textInput(['maxlength' => true]) ?>

                <?php echo $form->field($model, 'currency_id')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Выберите валюту...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>
                
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?php  $model->categoryArray = $model->getCategories()->all();
                    echo $form->field($model, 'categoryArray')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(MatrixCategory::find()->all(), 'id', 'name'),
                        'options' => ['placeholder' => Yii::t('app', 'Выберите категорию'), 'multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [',', ' '],
                            'maximumInputLength' => 10
                        ],
                    ])->label(Yii::t('app', 'Категории'));
                ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
      </div>
    </div>
</div>
