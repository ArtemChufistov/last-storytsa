<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\Matrix */

$this->title = Yii::t('app', 'Редактирование матрицы: ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Матрицы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="matrix-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
