<?php
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

$levels = [];
for($i = 1 ; $i < $maxDepth ; $i++){$levels[] = $i;};
?>
<div id="chart-container">
  <div id = "control">
    <?php $form = ActiveForm::begin(); ?>

      <div class="form-group">
        <label class="control-label"><?php echo Yii::t('app', 'Показывать до уровня');?></label>
        <?php echo Select2::widget([
              'name' => 'toLevel',
              'value' => $toLevel,
              'data' => $levels,
          ]);?>
      </div>
      <div class="form-group">
          <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
      </div>

    <?php ActiveForm::end(); ?>
  </div>
</div>

<script type="text/javascript">
$(function() {

  var datascource = <?php echo json_encode($treeArray, JSON_NUMERIC_CHECK);?>;

  $('#chart-container').orgchart({
    'data' : datascource,
    'nodeContent': 'title',
    'pan': true,
    'zoom': true
  });

});
</script>

<style type="text/css">
  .select2-selection{
    min-height: 33px;
  }
</style>