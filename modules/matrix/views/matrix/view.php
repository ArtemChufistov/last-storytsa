<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\Matrix */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Матрицы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">

            <p>
                <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                      'attribute' => 'user_id',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
                    ],
                    'parent_id',
                    'sum',
                    [
                      'attribute' => 'currency_id',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_currency_id', ['model' => $model]);},
                    ],
                    'name',
                    'status',
                    'sort',
                    'date_add',
                    'wallet_from',
                    'wallet_to',
                    'transaction',
                    'count_submit',
                    'slug',
                ],
            ]) ?>

        </div>
      </div>
    </div>
  </div>
</div>