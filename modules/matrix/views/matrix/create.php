<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\matrix\models\Matrix */

$this->title = Yii::t('app', 'Добавление матрицы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Матрицы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matrix-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>