<?php
namespace app\modules\matrix\components;

use app\modules\payment\models\forms\PaymentInOutForm;
use app\modules\payment\components\CoinBaseComponent;
use app\modules\payment\models\UserPayWalletBitcoin;
use app\modules\matrix\components\Marketing;
use app\modules\payment\models\PaySystem;
use app\modules\payment\models\Payment;
use app\modules\payment\models\Currency;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixGift;
use linslin\yii2\curl;

class VulcanMarketing extends Marketing
{
    public function process()
    {

    }

	// создать заявки на ввод/вывод
	public function createInOutPayment()
	{
        $currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
        $paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_COINBASE])->one();

        $paymentForm = new PaymentInOutForm;
        $paymentForm->type = $this->paymentType;
        $paymentForm->currency_id = $currency->id;
        $paymentForm->date_add = date('Y-m-d H:i:s');
        $paymentForm->pay_system_id = $paySystem->id;
        $paymentForm->status = Payment::STATUS_WAIT_SYSTEM;

        if ($paymentForm->type == Payment::TYPE_IN_OFFICE){
            $paymentForm->to_user_id = $this->user->id;
        }else{
            $paymentForm->from_user_id = $this->user->id;
        }

        if ($paymentForm->load($this->paymentData)){

            $paymentForm->currency_id = $currency->id;

        	if ($paymentForm->type == Payment::TYPE_IN_OFFICE){
        		$paymentForm->wallet = $this->user->getPayWallet($paySystem->id, $currency->id)->in_wallet;
        	}

            if ($paymentForm->validate()){
                $paymentForm->realSum = $paymentForm->sum;
                $paymentForm->save();
            }
        }

        return $paymentForm;
	}

}
?>