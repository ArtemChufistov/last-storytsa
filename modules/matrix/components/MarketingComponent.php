<?php

namespace app\modules\matrix\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\profile\models\User;
use app\modules\finance\models\Currency;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixQueue;
use app\modules\mainpage\models\Preference;

class MarketingComponent
{
    public function run($args = [])
    {
        $marketing = Preference::find()->where(['key' => Preference::KEY_MATRIX_MARKETING])->one()->value;
        $countFromQueue = Preference::find()->where(['key' => Preference::KEY_COUNT_FROM_MATRIX_QUEUE])->one();

        if(!empty($countFromQueue)){
            $limit = $countFromQueue->value;
        }else{
            $limit = 1;
        }

        $matrixQueueArray = MatrixQueue::find()->where(['status' => MatrixQueue::STATUS_NEW, 'type' => MatrixQueue::TYPE_AUTO])->limit($limit)->orderBy(['id' => SORT_DESC])->all();

        foreach($matrixQueueArray as $matrixQueue){

            $user = User::find()->where(['id' => $matrixQueue->user_id])->one();
            $currency = Currency::find()->where(['id' => $matrixQueue->currency_id])->one();
            $rootMatrix = Matrix::find()->where(['id' => $matrixQueue->matrix_root_id])->one();

            $marketinClass = new $marketing;
            $marketinClass->set(['user' => $user, 'currency' => $currency, 'rootMatrix' => $rootMatrix]);

            if ($marketinClass->handleQueue($matrixQueue)){
                $matrixQueue->status = MatrixQueue::STATUS_FINISH;
                $matrixQueue->save();
            }
        }
    }
}