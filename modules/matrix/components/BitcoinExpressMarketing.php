<?php
namespace app\modules\matrix\components;

use app\modules\mail\components\SendMailComponent;
use app\modules\payment\models\UserPayWalletBitcoin;
use app\modules\payment\models\PaySystem;
use app\modules\payment\models\Payment;
use app\modules\payment\models\Currency;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixGift;
use linslin\yii2\curl;

class BitcoinExpressMarketing
{
	private $user;
	private $currency;
	private $paySystem;
	private $matrix;
	private $level;

	const COUNT_CHILDREN = 2; // 2 потомка по маркетингу
	const PLACE_COST_BTC = 0.03;
	const SOLT = 'vcn78hgSDEWh8asde3fvfsdHAS2fc3';

	const BUTTON_BUY_PLACE = 'buy_place';
	const BUTTON_BUY_LEVEL = 'buy_level';

	public function set($data)
	{
		foreach ($data as $item => $value) {
			$this->$item = $value;
		}

		$this->allowLevels = self::getLevelCostArray();

		$this->matrix = new Matrix;
	}

	public static function getLevelCostArray()
	{
		return [1 => 0.03 , 2 => 0.05 , 3 => 0.15];
	}

	public static function getCostForLevel($level)
	{
		$array = self::getLevelCostArray();

		return $array[$level];
	}

	// поиск в структуре пригласителя для покупки уровня
	public function getPaymentBuyLevel()
	{
        if (empty($this->user->getPayWallet($this->paySystem->id, $this->currency->id))){
            throw new \Exception(\Yii::t('app', 'Для того чтобы принять участие необходимо указать свой кошелек BitCoin в разделе </br> <a href="/profile/office/walletdetail">Платёжные реквизиты.</a>'), 1);
        }

        if (empty($this->allowLevels[$this->level])){
            throw new \Exception(\Yii::t('app', 'Нет такого уровня'), 1);
        }

        $buyLevelNum = 'buy_level_' . $this->level;

        if ($this->level == 1){
        	$buyLevelNumM1 = 'buy_level_' . $this->level;
        }else{
        	$buyLevelNumM1 = 'buy_level_' . ($this->level - 1);
        }
        $userMatrix = Matrix::find()->where(['user_id' => $this->user->id])->one();

        if (empty($userMatrix)){
            throw new \Exception(\Yii::t('app', 'Сначало нужно купить место в матрице'), 1);
        }

        // если уже есть заявка для покупки этого уровня - отправляем на неё
        $statusArray = [Payment::STATUS_WAIT, Payment::STATUS_WAIT_SYSTEM, Payment::PROCESS_SYSTEM];
        foreach($statusArray as $statusKey => $statusValue){
        	$currentPayment = Payment::find()
        		->where(['type' => Payment::TYPE_BUY_LEVEL, 'additional_info' => $this->level, 'status' => $statusValue, 'from_user_id' => $this->user->id])->one();
        	if (!empty($currentPayment)){
        		return $currentPayment;
        	}
        }

		$userMatrixGift = $userMatrix->getMatrixGift()->one();

		if (empty($userMatrixGift)){
			return;
		}elseif ((!empty($userMatrixGift) && (($userMatrixGift->$buyLevelNum > 0 && $userMatrixGift->$buyLevelNum == $userMatrixGift->$buyLevelNumM1 ) || $userMatrixGift->$buyLevelNumM1 == 0)) || empty($userMatrixGift)){
			if ($userMatrixGift->$buyLevelNum != $userMatrixGift->$buyLevelNumM1){
	            throw new \Exception(\Yii::t('app', 'Необходима покупка уровня {level}',[
	            	'level' => ($this->level - 1),
	            ]), 1);
			}
		}

        $matrixPlace = $this->user->getMatrixPlaces()->one();

        $see = false;

        $buyLevelsForMatrix = [];
        foreach(array_reverse($matrixPlace->ancestors()->all()) as $num => $matrix){
        	if (($num + 1)%$this->level == 0){
        		$matrixGift = $matrix->getMatrixGift()->one();
        		if (!empty($matrixGift)){
        			if ($matrixGift->$buyLevelNum >= ($userMatrixGift->$buyLevelNum + 1)){
						$placeFinded = true;
						break;
        			}else{
        				$buyLevelsForMatrix[] = $matrix->id;
        			}
        		}
        	}
        }

		$toUser = $matrix->getUser()->one();
		$payWallet = $toUser->getPayWallet($this->paySystem->id, $this->currency->id);

		$payment = new Payment;

		$payment->sum = $this->allowLevels[$this->level];
		$payment->type = Payment::TYPE_BUY_LEVEL;
		$payment->status = Payment::STATUS_WAIT;
		$payment->to_user_id = $toUser->id;
		$payment->currency_id = $this->currency->id;
		$payment->from_user_id = $this->user->id;
		$payment->pay_system_id = $this->paySystem->id;
		$payment->wallet = $payWallet->wallet;
		$payment->date_add = date('Y-m-d H:i:s');
		$payment->additional_info = $this->level;
		$payment->matrix_id = $userMatrix->id;
		$payment->comment = json_encode($buyLevelsForMatrix);

		$payment->save(false);

		$payment->hash = md5($payment->id);

		$payment->save(false);

		return $payment;
	}

	// поиск в структуре пригласителя для покупки места
	public function getPaymentBuyPlace()
	{
		$wallet = $this->user->getPayWallet($this->paySystem->id, $this->currency->id);

        if (empty($wallet) || $wallet->wallet == ''){
            throw new \Exception(\Yii::t('app', 'Для того чтобы принять участие необходимо указать свой кошелек BitCoin в разделе </br> <a href="/profile/office/walletdetail">Платёжные реквизиты.</a>'), 1);
        }
        
        $oldPayment = Payment::find()->where([
        	'from_user_id' => $this->user->id,
        	'type' => Payment::TYPE_BUY_PLACE,
        	'status' => [Payment::STATUS_WAIT, Payment::STATUS_WAIT_SYSTEM, Payment::PROCESS_SYSTEM, Payment::STATUS_OK]])->one();

        if (!empty($oldPayment)){
        	return $oldPayment;
        }

		// обходим всех потомков
		$placeFinded = false;
		foreach(array_reverse($this->user->ancestors()->all()) as $parentUser){

			$matrixPlace = Matrix::find()->where(['user_id' => $parentUser->id])->one();

			if (empty($matrixPlace)){
				continue;
			}else{
				$matrixGift = MatrixGift::find()->where(['matrix_id' => $matrixPlace->id])->one();
				$placeFinded = true;
				break;
			}
		}

		if ($placeFinded == false){
			throw new \Exception(\Yii::t('app', 'Нет свободных мест'), 1);return;
		}
		
		$result = $matrixPlace->findPlace(self::COUNT_CHILDREN);

		$descendant = Matrix::find()->where(['id' => $result['id']])->one();

		// проверка заявок в ожидании и очередь
		$testPayments = Payment::find()
			->where(['in', 'status', [Payment::STATUS_WAIT, Payment::STATUS_WAIT_SYSTEM]])
			->andWhere(['type' => Payment::TYPE_BUY_PLACE])
			->andWhere(['to_user_id' => $descendant->user_id])->all();

		if (self::COUNT_CHILDREN - (count($testPayments) + count($descendant->children()->all())) <= 0  ){
            throw new \Exception(\Yii::t('app', 'Если вы видите это сообщение, то это значит, что в настоящий момент заканчивается формирование одной из линий Вашего пригласителя. Просим Вас повторить данное действие через 1 час.'), 1);
            return;
		}

		$toUser = $descendant->getUser()->one();
		$payWallet = $toUser->getPayWallet($this->paySystem->id, $this->currency->id);

		$payment = new Payment;

		$payment->sum = self::PLACE_COST_BTC;
		$payment->type = Payment::TYPE_BUY_PLACE;
		$payment->status = Payment::STATUS_WAIT;
		$payment->to_user_id = $toUser->id;
		$payment->currency_id = $this->currency->id;
		$payment->from_user_id = $this->user->id;
		$payment->pay_system_id = $this->paySystem->id;
		$payment->wallet = $payWallet->wallet;
		$payment->date_add = date('Y-m-d H:i:s');

		$payment->save(false);

		$payment->hash = md5($payment->id);

		$payment->save(false);

		return $payment;
	}

	// сверка платежей с блокчейном
	public static function checkPayments()
	{
		$payments = Payment::find()->where(['status' => Payment::STATUS_WAIT_SYSTEM])->andWhere(['not', ['transaction_hash' => '']])->all();

		foreach($payments as $payment){
			$curl = new curl\Curl();

			$url = UserPayWalletBitcoin::BLOCKHAIN_URL . $payment->transaction_hash;
	        $response = $curl->get($url);

			if (!empty($response)){
				$responseData = json_decode($response, true);
				if (!empty($responseData)){

					if (empty($responseData['block_height'])){
						continue;
					}
					$out = '';
					if (!empty($responseData['out'][0]) && $responseData['out'][0]['addr'] == $payment->wallet){
						$out = $responseData['out'][0];
					}
					if (!empty($responseData['out'][1]) && $responseData['out'][1]['addr'] == $payment->wallet){
						$out = $responseData['out'][1];
					}

					if (!empty($out)){
						if ($out['addr'] == $payment->wallet && $out['value'] / 100000000 >= $payment->sum){
							$payment->status = Payment::PROCESS_SYSTEM;
							$payment->save(false);
						}
					}
				}
			}
		}
	}

	// проверка заявки на корректность с сетью биткоин
	public function checkPaymentById($id)
	{
		$payment = Payment::find()->where(['id' => $id])->one();

		if (empty($payment)){
			echo 'Заявка не найдена' . "\r\n";exit;
		}

		$curl = new curl\Curl();

		$url = UserPayWalletBitcoin::BLOCKHAIN_URL . $payment->transaction_hash;
        $response = $curl->get($url);

		if (!empty($response)){
			$responseData = json_decode($response, true);
			if (!empty($responseData)){

				if (empty($responseData['block_height'])){
					continue;
				}
				$out = '';
				if (!empty($responseData['out'][0]) && $responseData['out'][0]['addr'] == $payment->wallet){
					$out = $responseData['out'][0];
				}
				if (!empty($responseData['out'][1]) && $responseData['out'][1]['addr'] == $payment->wallet){
					$out = $responseData['out'][1];
				}

				if (!empty($out)){
					if ($out['addr'] == $payment->wallet && $out['value'] / 100000000 >= $payment->sum){
						echo 'Заявка корректная, деньги должны быть перечислены' . "\r\n";
					}else{
						$blockchainValue = $out['value'] / 100000000;
						echo 'Сумма не совпадает сумма с блокчейна: ' . $blockchainValue . ' сумма заявки: ' . $payment->sum . "\r\n";
						echo $out['addr'] . ' - кошелек с блокчейна' . "\r\n";
						echo $payment->wallet . ' - кошелек пользователя' . "\r\n";
					}
				}else{
					echo 'Кошелек получателя не найден' . "\r\n";
				}
				//print_r($responseData);
			}else{
				echo 'Ответ от блокчейна некорректен' . "\r\n";
			}
		}else{
			echo 'Ответ от блокчейна некорректен' . "\r\n";
		}
	}

	// поставить место в матрицу
	public static function putMatrixPlaces()
	{
        $currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
        $paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_BITCOIN])->one();

		$payments = Payment::find()->where(['status' => Payment::PROCESS_SYSTEM, 'type' => Payment::TYPE_BUY_PLACE])->limit(1)->all();

		foreach($payments as $payment){
			$parentUser = $payment->getToUser()->one();

/*
			if (!empty($payment->getFromUser()->one())){
				if (!empty($payment->getFromUser()->one()->parent()->one())){
					if (!empty($payment->getFromUser()->one()->parent()->one()->getMatrixPlaces()->one())){
						$matrixPlace = $payment->getFromUser()->one()->parent()->one()->getMatrixPlaces()->one();
					}
				}
			}
			if (empty($matrixPlace)){
*/
			$matrixPlace = $parentUser->getMatrixPlaces()->one();
//			}

			$result = $matrixPlace->findPlace(self::COUNT_CHILDREN);

			echo 'Пытаемся вставить место в матрицу по заявке: ' . $payment->id . "\r\n"; 
			$db = \Yii::$app->db;
			$transaction = $db->beginTransaction();
			try {
		        $matrix = new Matrix;
		        $matrix->sum = $payment->sum;
		        $matrix->sort = $result['sort'];
		        $matrix->status = Matrix::STATUS_PAY_OK;
		        $matrix->user_id = $payment->from_user_id;
		        $matrix->parent_id = $result['id'];
		        $matrix->currency_id = $currency->id;
		        $matrix->date_add = date('Y-m-d H:i:s');

		        $matrix->save(false);

		        $matrixGift = new MatrixGift;
		        $matrixGift->matrix_id = $matrix->id;
		        $matrixGift->buy_level_1 = 1;
		        $matrixGift->left_level_1 = 2;
		        $matrixGift->save(false);

                $payment->matrix_id = $matrix->id;
				$payment->status = Payment::STATUS_OK;
				$payment->save(false);

				print_r($payment->attributes);

				$transaction->commit();
				echo 'Коммит прошёл успешно в' . date('Y-m-d H:i:s') . "\r\n";
			} catch (Exception $e) {
				$transaction->rollback();
				echo $e->getMessage();
				return $e->getMessage();
			}
		}
	}

	// поставить уровень для места в матрице
	public static function putMatrixLevels()
	{
        $currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
        $paySystem = PaySystem::find()->where(['key' => PaySystem::KEY_BITCOIN])->one();

		$payments = Payment::find()->where(['status' => Payment::PROCESS_SYSTEM, 'type' => Payment::TYPE_BUY_LEVEL])->limit(1)->all();

		foreach($payments as $payment){

			$fromMatrixGift = $payment->getToUser()->one()->getMatrixPlaces()->one()->getMatrixGift()->one();
			if (empty($fromMatrixGift)){
				continue;
			}

			$matrixPlace = $payment->getMatrix()->one();

			if (empty($matrixPlace)){
				continue;
			}

			$toMatrixGift = $matrixPlace->getMatrixGift()->one();

			if (empty($toMatrixGift)){
				$toMatrixGift = new MatrixGift;
				$toMatrixGift->matrix_id = $matrixPlace->id;
			}

			$buyLevelNum = 'buy_level_' . $payment->additional_info;

			self::getCostForLevel($payment->additional_info);

			$db = \Yii::$app->db;
			$transaction = $db->beginTransaction();
			echo 'Пытаемся начислить уровень по заявке: ' . $payment->id . "\r\n"; 
			try {

				$toMatrixGift->$buyLevelNum++;
				$toMatrixGift->save(false);

				if (!empty($payment->comment)){
					$payLevels = json_decode($payment->comment);
					foreach ($payLevels as $payLevelMatrixId) {
						$payMatrix = Matrix::find()->where(['id' => $payLevelMatrixId])->one();

						$lostMatrixGift = $payMatrix->getMatrixGift()->one();
						if (!empty($lostMatrixGift)){
							$lostMatrixGift->$buyLevelNum++;
							$lostMatrixGift->save(false);
						}
					}
				}

				$payment->status = Payment::STATUS_OK;
				$payment->save(false);

				print_r($payment->attributes);

				$transaction->commit();
				echo 'Транзакция покупки уровня прошла успешно в ' . date('Y-m-d H:i:s') . "\r\n";

			} catch (Exception $e) {
				$transaction->rollback();
				echo $e->getMessage();
				return $e->getMessage();
			}
		}
	}

    public function getButtons()
    {
    	$this->matrix = Matrix::find()->where(['user_id' => $this->user->id])->one();

    	if (empty($this->matrix)){
    		return [self::BUTTON_BUY_PLACE => true];
    	}else{
    		return [self::BUTTON_BUY_LEVEL => true];
    	}

    }

    // тестовая функция - нельзя юзать
    public static function putPlays($user_id, $parent_id)
    {
        $matrix = new Matrix;
        $matrix->user_id = $user_id;
        $matrix->parent_id = $parent_id;
        $matrix->sum = 0.05;
        $matrix->currency_id = 1;
        $matrix->status = 1;
        $matrix->date_add = date('Y-m-d H:i:s');
        $matrix->slug = md5(time());
        $matrix->save(false);
    }
}
?>
