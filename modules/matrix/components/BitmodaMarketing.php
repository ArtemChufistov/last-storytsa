<?php
namespace app\modules\matrix\components;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\profile\models\User;
use app\modules\matrix\models\Matrix;
use app\modules\finance\models\Currency;
use app\modules\matrix\models\MatrixPref;
use app\modules\matrix\models\MatrixQueue;
use app\modules\mainpage\models\Preference;
use app\modules\finance\models\Transaction;
use app\modules\matrix\models\MatrixUserSetting;
use app\modules\profile\components\YoutubeStatComponent;

class BitmodaMarketing{

	private $user;
	private $matrix;
	private $currency;
	private $paySystem;
	private $matrixPref;
	private $matrixQueue;
	private $rootMatrix;
	private $adminSum;

	public function set($data)
	{
		foreach ($data as $item => $value) {
			$this->$item = $value;
		}

		$this->currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();

		$this->matrix = new Matrix;

		$this->adminSum = 0;

		return $this;
	}

	// первое добавление в очередь на матрицу
	public function addToQueue()
	{
		if (empty($this->currency)){
			throw new \Exception(Yii::t('app', 'Валюта не передана'));
		}

		if (empty($this->user)){
			throw new \Exception(Yii::t('app', 'Пользователь не назначен'));
		}

		if (empty($this->rootMatrix)){
			throw new \Exception(Yii::t('app', 'Матрица не назначена'));
		}

		$matrix = Matrix::find()->where(['user_id' => $this->user->id])->andWhere(['root_id' => $this->rootMatrix->id])->one();

		if (!empty($matrix)){
			throw new \Exception(Yii::t('app', 'Нельзя иметь несколько мест в одной матрице'));
		}

		if ($this->initMatrixPref()->addElevemtToQueue() == true){
			return $this->matrixQueue;
		}else{
			throw new Exception("Не удалось добавить элемент в очередь");
			
		}
	}

	// покупка места в матрице
	public function handleQueue($matrixQueue)
	{
		$adminUser = User::find()->where(['login' => 'admin'])->one();

		if ($this->user->id == $adminUser->id){
			return true;
		}

		if (empty($this->currency)){
			throw new \Exception(Yii::t('app', 'Валюта не передана'));
		}

		if (empty($this->user)){
			throw new \Exception(Yii::t('app', 'Пользователь не назначен'));
		}

		if (empty($this->rootMatrix)){
			throw new \Exception(Yii::t('app', 'Матрица не назначена'));
		}

		$this->initMatrixPref();
		$parentUser = $this->user->parent()->one();

		// если пригласитель админ - ставим под вершину
		if ($parentUser->id == $adminUser->id){
			$result['sort'] = count($this->rootMatrix->children()->all());
			$result['id'] = $this->rootMatrix->id;

			// начисляем пригласителю
			$this->chargeUser($this->matrixPref[MatrixPref::KEY_CHARGE_INVITER]->value, $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value, $adminUser->id);

			// начисляем пригласителю на просмотры видео
			$this->chargeUser($this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value1, $adminUser->id);

			// 0.03 админу
			$this->chargeBalanceFix($this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2]->value1);

		}else{
			$parentUserMatrix = Matrix::find()
				->where(['user_id' => $parentUser->id])
				->andWhere(['root_id' => $this->rootMatrix->id])
				->one();

			// если у пригласителя не куплено место - ставим под вершину
			if (empty($parentUserMatrix)){

				$result['sort'] = count($this->rootMatrix->children()->all());
				$result['id'] = $this->rootMatrix->id;
				// начисляем пригласителю
				$this->chargeUser($this->matrixPref[MatrixPref::KEY_CHARGE_INVITER]->value, $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value, $parentUser->id);

				// начисляем пригласителю на просмотры видео
				//$this->chargeUser($this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value1, $adminUser->id);

				// 0.03 админу
				$this->chargeBalanceFix($this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2]->value1);

			}else{
				// если у пригласителя заняты оба места - ставим под вершину
				if (count($parentUserMatrix->children()->all()) == 2){

					$result['sort'] = count($this->rootMatrix->children()->all());
					$result['id'] = $this->rootMatrix->id;

					// начисляем пригласителю
					$this->chargeUser($this->matrixPref[MatrixPref::KEY_CHARGE_INVITER]->value, $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value, $parentUser->id);

					// начисляем пригласителю на просмотры видео
					//$this->chargeUser($this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value1, $parentUser->id);

					// 0.03 админу
					$this->chargeBalanceFix($this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2]->value1);
				}else{
					$result['sort'] = count($parentUserMatrix->children()->all());
					$result['id'] = $parentUserMatrix->id;

					// если место первое - ничего не делаем так как все начисления уже сделаны
					if ($result['sort'] == 0){
					// если место второе - распределяем средства на 5 уровней вверх
					}elseif ($result['sort'] == 1) {
						// начисляем пригласителю вложенную сумму
						$this->chargeUser($this->matrixPref[MatrixPref::KEY_COST]->value, $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value, $parentUserMatrix->user_id);

						// начисляем пригласителю на просмотры видео
						$this->chargeUser($this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU1]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU1]->value1, $parentUserMatrix->user_id);

						$sumForPartners = $this->matrixPref[MatrixPref::KEY_COST]->value;
						$sumForPartners = $sumForPartners - $this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU1]->value;

						// рефералка на 5 уровней вверх
						$referalSum = $this->matrixPref[MatrixPref::KEY_REFERAL_LEVEL_SUM]->value;
						$referalSumVou = $this->matrixPref[MatrixPref::KEY_REFERAL_LEVEL_SUM_VOU]->value;

						$parentUserMatrix2 = $parentUserMatrix->parent()->one();

						if (!empty($parentUserMatrix2)){

							//$this->chargeUser($referalSum, $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value, $parentUserMatrix2->user_id);
							$this->chargeUser($referalSumVou, $this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value1, $parentUserMatrix2->user_id);
							$parentUserMatrix3 = $parentUserMatrix2->parent()->one();
							$sumForPartners = $sumForPartners - ($referalSum + $referalSumVou);
							if (!empty($parentUserMatrix3)){

								//$this->chargeUser($referalSum, $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value, $parentUserMatrix3->user_id);
								$this->chargeUser($referalSumVou, $this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value1, $parentUserMatrix3->user_id);
								$parentUserMatrix4 = $parentUserMatrix3->parent()->one();
								$sumForPartners = $sumForPartners - ($referalSum + $referalSumVou);
								if (!empty($parentUserMatrix4)){

									//$this->chargeUser($referalSum, $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value, $parentUserMatrix4->user_id);
									$this->chargeUser($referalSumVou, $this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value1, $parentUserMatrix4->user_id);
									$parentUserMatrix5 = $parentUserMatrix4->parent()->one();
									$sumForPartners = $sumForPartners - ($referalSum + $referalSumVou);
									if (!empty($parentUserMatrix5)){

										//$this->chargeUser($referalSum, $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value, $parentUserMatrix5->user_id);
										$this->chargeUser($referalSumVou, $this->matrixPref[MatrixPref::KEY_CHARGE_INVITER_VOU]->value1, $parentUserMatrix5->user_id);
										$sumForPartners = $sumForPartners - ($referalSum + $referalSumVou);

										$countDesc = count($parentUserMatrix5->descendants(5)->all());
										if ($countDesc >= 62){
											$this->reinvestMatrix($this->matrixPref[MatrixPref::KEY_REINVEST_TO]->value1);
										}
									}
								}
							}
						}

						if ($sumForPartners > 0){
							$this->chargeUser($sumForPartners, $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value, $adminUser->id);
						}
					}
				}
			}
		}

        $this->matrix = new Matrix;
        $this->matrix->sum = $this->matrixPref[MatrixPref::KEY_COST]->value;
        $this->matrix->sort = $result['sort'];
        $this->matrix->status = Matrix::STATUS_PAY_OK;
        $this->matrix->user_id = $this->user->id;
        $this->matrix->parent_id = $result['id'];
        $this->matrix->currency_id = $this->currency->id;
        $this->matrix->date_add = date('Y-m-d H:i:s');
        $this->matrix->root_id = $this->rootMatrix->id;
        $this->matrix->name = 1;

        $this->matrix->save(false);

		$this->matrixQueue = $matrixQueue;
        $this->matrixQueue->matrix_id = $this->matrix->id;
        $this->matrixQueue->save(false);

        // распределния для потомков

        return true;
	}

	// начислить X долларов пригласителю
	public function chargeInviter($sum, $currencyId)
	{
		$user = $this->matrix->getUser()->one();
		$currency = Currency::find()->where(['id' => $currencyId])->one();

		if (empty($user)){
			return;
		};

		$parentUser = $user->parent()->one();

		if (empty($parentUser)){
			return;
		};

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        $transaction->to_user_id = $parentUser->id;
        $transaction->currency_id = $currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');

        $transaction->save(false);

        $parentUser->withNoActive()->recalclulateBalances();
	}

	public function chargeUser($sum, $currencyId, $userId)
	{
		$user = User::find()->where(['id' => $userId])->one();
		$currency = Currency::find()->where(['id' => $currencyId])->one();

		if (empty($user)){
			return;
		};

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        $transaction->to_user_id = $user->id;
        $transaction->currency_id = $currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');

        $transaction->save(false);

        $user->withNoActive()->recalclulateBalances();
	}

	// поставить потомка в очередь на матрицу
	public function reinvestMatrix($rootMatrixId, $value2 = '')
	{
		$ancestorMatrix = $this->matrix->ancestors($this->matrixPref[MatrixPref::KEY_DEPTH]->value)->one();

        $user = $ancestorMatrix->getUser()->one();
        $currency = Currency::find()->where(['id' => $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value])->one();
		$rootMatrix = Matrix::find()->where(['id' => $rootMatrixId])->one();

        $marketing = Preference::find()->where(['key' => Preference::KEY_MATRIX_MARKETING])->one()->value;

        $marketingClass = new $marketing;
        $marketingClass->set(['user' => $user, 'currency' => $currency, 'rootMatrix' => $rootMatrix]);

        if ($marketingClass->initMatrixPref()->addElevemtToQueue(MatrixQueue::TYPE_AUTO) == true){

	        $transaction = new Transaction;
	        $transaction->to_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $this->matrixQueue->id;

	        $transaction->save(false);

	        $transaction = new Transaction;
	        $transaction->from_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_REINVEST_MATRIX_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $marketingClass->matrixQueue->id;

	        $transaction->save(false);

        	return $this->matrixQueue;
        }else{
        	throw new Exception("Не удалось добавить элемент в очередь по неизвестным причинам");
        }
	}

	// проверка возможности списания с баланса
	public function checkBalance($cost)
	{
		$this->user->withNoActive()->recalclulateBalances();

		$balance = $this->user->getBalances()[$this->currency->id];

		if ($balance->value < $cost){
			return false;
		}else{
			return true;
		}
	}

	// списать средства с баланса участника
	public function fromBalance($cost)
	{
		$this->user->withNoActive()->recalclulateBalances();

		$balance = $this->user->getBalances()[$this->currency->id];

		if ($balance->value < $cost){
			throw new \Exception(Yii::t('app', 'Не достаточно средств, пожалуйста пополните свой баланс на сумму {cost} {currency} в разделе <a href = "{link}">Финансы</a>', [
				'cost' => $cost,
				'currency' => $this->currency->title,
				'link' => Url::to(['/profile/office/finance']),
			]));
			return;
		}

        $transaction = new Transaction;
        $transaction->from_user_id = $this->user->id;
        $transaction->type = Transaction::TYPE_BUY_MATRIX_PLACE;
        $transaction->sum = $cost;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->matrix_queue_id = $this->matrixQueue->id;
        $transaction->save(false);

		$this->user->withNoActive()->recalclulateBalances();

		return true;
	}

	// начилисть средства на баланс участника
	public function toBalance($cost, $value2 = '')
	{
		$ancestorMatrix = $this->matrix->ancestors($this->matrixPref[MatrixPref::KEY_DEPTH]->value)->one();

		// получается, что некому начислять
		if (empty($ancestorMatrix)){
			return $cost;
		}

		$chargeUser = $ancestorMatrix->getUser()->one();

		$balance = $chargeUser->getBalances()[$this->currency->id];

        $transaction = new Transaction;
        $transaction->sum = $cost;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        $transaction->to_user_id = $chargeUser->id;
        //$transaction->from_user_id = $this->user->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->matrix_queue_id = $this->matrixQueue->id;

        $transaction->save(false);

		$chargeUser->withNoActive()->recalclulateBalances();

		return $cost;
	}

	// начилисть средства на баланс участника и админа
	public function toBalanceAndAdmin($cost, $value2 = '')
	{
		$ancestorMatrix = $this->matrix->ancestors($this->matrixPref[MatrixPref::KEY_DEPTH]->value)->one();

		// получается, что некому начислять
		if (empty($ancestorMatrix)){
			return $cost;
		}

		$chargeUser = $ancestorMatrix->getUser()->one();

		$balance = $chargeUser->getBalances()[$this->currency->id];

        $transaction = new Transaction;
        $transaction->sum = $cost;
        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
        $transaction->to_user_id = $chargeUser->id;
        //$transaction->from_user_id = $this->user->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->matrix_queue_id = $this->matrixQueue->id;

        $transaction->save(false);

		$balance->recalculateValue();


		if ($value2 != ''){
			$chargeUser = User::find()->where(['id' => 1])->one();
			$balance = $chargeUser->getBalances()[$this->currency->id];

	        $transaction = new Transaction;
	        $transaction->sum = $value2;
	        $transaction->type = Transaction::TYPE_ADMIN_BONUS;
	        $transaction->to_user_id = $chargeUser->id;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $this->matrixQueue->id;

	        $transaction->save(false);

			$balance->recalculateValue();
		}

		return $cost;
	}

	// начислить средства на баланс админа
	public function chargeBalanceFix($userId, $sum, $type = '')
	{
		if ($type == ''){
			$type = Transaction::TYPE_ADMIN_BONUS;
		}

		$user = User::find()->where(['id' => $userId])->one();

		$balance = $user->getBalances()[$this->currency->id];

        $transaction = new Transaction;
        $transaction->sum = $sum;
        $transaction->type = $type;
        $transaction->to_user_id = $user->id;
        //$transaction->from_user_id = $this->user->id;
        $transaction->currency_id = $this->currency->id;
        $transaction->date_add = date('Y-m-d H:i:s');
        $transaction->matrix_queue_id = $this->matrixQueue->id;

        $transaction->save(false);

		$balance->recalculateValue();

		return $sum;
	}

	// создать 2 места в очереди в n-ой матрице
	public function doubleReinvest($rootMatrixId, $sum = '')
	{
		$user       = $this->matrix->ancestors($this->matrixPref[MatrixPref::KEY_DEPTH]->value)->one()->getUser()->one();
        $currency   = Currency::find()->where(['id' => $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value])->one();
        $marketing  = Preference::find()->where(['key' => Preference::KEY_MATRIX_MARKETING])->one()->value;
		$rootMatrix = Matrix::find()->where(['id' => $rootMatrixId])->one();

        $marketingClass = new $marketing;
        $marketingClass->set(['user' => $user, 'currency' => $currency, 'rootMatrix' => $rootMatrix])->initMatrixPref();

        if ($marketingClass->addElevemtToQueue(MatrixQueue::TYPE_AUTO, false) != true){
        	throw new Exception("Не удалось добавить элемент в очередь по неизвестным причинам");
        }else{

	        $transaction = new Transaction;
	        $transaction->to_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $this->matrixQueue->id;

	        $transaction->save(false);

	        $transaction = new Transaction;
	        $transaction->from_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_REINVEST_MATRIX_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $marketingClass->matrixQueue->id;

	        $transaction->save(false);
        }
        
        if ($marketingClass->addElevemtToQueue(MatrixQueue::TYPE_AUTO, false) != true){
        	throw new Exception("Не удалось добавить элемент в очередь по неизвестным причинам");
        }else{

	        $transaction = new Transaction;
	        $transaction->to_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $this->matrixQueue->id;

	        $transaction->save(false);

	        $transaction = new Transaction;
	        $transaction->from_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_REINVEST_MATRIX_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $marketingClass->matrixQueue->id;

	        $transaction->save(false);
        }
        
        return $marketingClass->matrixQueue;
	}

	// создать место в очереди в матрицу, а оставшиеся на руки
	public function toReinvestToBalance($rootMatrixId, $sum)
	{
		$user       = $this->matrix->ancestors($this->matrixPref[MatrixPref::KEY_DEPTH]->value)->one()->getUser()->one();
        $currency   = Currency::find()->where(['id' => $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value])->one();
        $marketing  = Preference::find()->where(['key' => Preference::KEY_MATRIX_MARKETING])->one()->value;
		$rootMatrix = Matrix::find()->where(['id' => $rootMatrixId])->one();

        $marketingClass = new $marketing;
        $marketingClass->set(['user' => $user, 'currency' => $currency, 'rootMatrix' => $rootMatrix])->initMatrixPref();

        $sum = $sum - $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;

        if ($marketingClass->addElevemtToQueue(MatrixQueue::TYPE_AUTO, false) != true){
        	throw new Exception("Не удалось добавить элемент в очередь по неизвестным причинам");
        }else{

	        $transaction = new Transaction;
	        $transaction->to_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $this->matrixQueue->id;

	        $transaction->save(false);

	        $transaction = new Transaction;
	        $transaction->from_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_REINVEST_MATRIX_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $marketingClass->matrixQueue->id;

	        $transaction->save(false);
        }

        $marketingClass->chargeBalanceFix($user->id, $sum, Transaction::TYPE_PARTNER_BUY_PLACE);
	}

	// переход в 4уцю матрицу для маркетинга Evinizi
	public function toReinvestToBalanceVoucherCheck($rootMatrixId, $sum)
	{
		$user       = $this->matrix->ancestors($this->matrixPref[MatrixPref::KEY_DEPTH]->value)->one()->getUser()->one();
        $currency   = Currency::find()->where(['id' => $this->matrixPref[MatrixPref::KEY_MAIN_CURRENCY]->value])->one();
        $marketing  = Preference::find()->where(['key' => Preference::KEY_MATRIX_MARKETING])->one()->value;
		$rootMatrix = Matrix::find()->where(['id' => $rootMatrixId])->one();

        $marketingClass = new $marketing;
        $marketingClass->set(['user' => $user, 'currency' => $currency, 'rootMatrix' => $rootMatrix])->initMatrixPref();

        if ($marketingClass->addElevemtToQueue(MatrixQueue::TYPE_AUTO, false) != true){
        	throw new Exception("Не удалось добавить элемент в очередь по неизвестным причинам");
        }else{

	        $transaction = new Transaction;
	        $transaction->to_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $this->matrixQueue->id;

	        $transaction->save(false);

	        $transaction = new Transaction;
	        $transaction->from_user_id = $user->id;
	        $transaction->type = Transaction::TYPE_REINVEST_MATRIX_PLACE;
	        $transaction->sum = $marketingClass->matrixPref[MatrixPref::KEY_COST]->value;
	        $transaction->currency_id = $this->currency->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $marketingClass->matrixQueue->id;

	        $transaction->save(false);
        }

        $matrixChecker = Matrix::find()->where(['user_id' => $user->id])->andWhere(['root_id' => $rootMatrixId])->one();

        if (empty($matrixChecker)){
        	$sum = 10000;
        }else{
        	$sum = 11200;

        	$currencyVoucher = Currency::find()->where(['key' => Currency::KEY_VOUCHER])->one();
			$balance = $user->getBalances()[$currencyVoucher->id];

	        $transaction = new Transaction;
	        $transaction->sum = 3;
	        $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE;
	        $transaction->to_user_id = $user->id;
	        $transaction->currency_id = $currencyVoucher->id;
	        $transaction->date_add = date('Y-m-d H:i:s');
	        $transaction->matrix_queue_id = $this->matrixQueue->id;

	        $transaction->save(false);

			$balance->recalculateValue();
        }

        $marketingClass->chargeBalanceFix($user->id, $sum, Transaction::TYPE_PARTNER_BUY_PLACE);
	}

	// добавление в очередь
	public function addElevemtToQueue($type = '', $takeFrom = true)
	{
		if ($type == ''){
			$type = MatrixQueue::TYPE_MANUAL;
		}
		// проверка возможности списания с баланса если нужно
		if ($takeFrom == true){
			if (!empty($this->matrixPref[MatrixPref::KEY_TAKE_FROM])){
				$takeFromFunction = $this->matrixPref[MatrixPref::KEY_TAKE_FROM]->value;

				if ($this->$takeFromFunction($this->matrixPref[MatrixPref::KEY_COST]->value) == false){
					throw new \Exception(Yii::t('app', 'Не достаточно средств, пожалуйста пополните свой баланс на сумму {cost} {currency} в разделе <a href = "{link}">Финансы</a>', [
						'cost' => $cost,
						'currency' => $this->currency->title,
						'link' => Url::to(['/profile/office/finance']),
					]));
					return false;
				};
			}
		}

		$this->matrixQueue = new MatrixQueue;
		$this->matrixQueue->matrix_root_id = $this->rootMatrix->id;
		$this->matrixQueue->currency_id = $this->currency->id;
		$this->matrixQueue->user_id = $this->user->id;
		$this->matrixQueue->status = MatrixQueue::STATUS_NEW;
		$this->matrixQueue->type = $type;
		$this->matrixQueue->sum = $this->matrixPref[MatrixPref::KEY_COST]->value;

		$this->matrixQueue->save();
		// списание с баланса если нужно
		if ($takeFrom == true){
			if (!empty($this->matrixPref[MatrixPref::KEY_TAKE_FROM])){
				$takeFromFunction = $this->matrixPref[MatrixPref::KEY_TAKE_FROM]->value1;
				$this->$takeFromFunction($this->matrixPref[MatrixPref::KEY_COST]->value);
			}
		}

		$this->chargeAdminBalanceIfNeed();

		return true;
	}

	// админские начисления
	public function chargeAdminBalanceIfNeed()
	{
		if (!empty($this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_1])){
			$chargeFunction = $this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_1]->value2;
			$this->adminSum += $this->$chargeFunction($this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_1]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_1]->value1);
		}

		if (!empty($this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2])){
			/*
			$chargeFunction = $this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2]->value2;
			$this->adminSum += $this->$chargeFunction($this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_2]->value1);
			*/
		}

		if (!empty($this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_3])){
			$chargeFunction = $this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_3]->value2;
			$this->adminSum += $this->$chargeFunction($this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_3]->value, $this->matrixPref[MatrixPref::KEY_CHARGE_TO_USER_3]->value1);
		}
	}

	// инициализация матричных настроек
	private function initMatrixPref()
	{
		foreach(MatrixPref::find()->where(['matrix_id' => $this->rootMatrix->id])->all() as $matrixPref){
			$this->matrixPref[$matrixPref->key] = $matrixPref;
		};

		return $this;
	}

	// взять настройки матрицы для юзера (галки, куда ставить место)
	private static function getMatrixUserSetting($userId, $rootMatrixId)
	{
		$matrixUserSetting = MatrixUserSetting::find()->where(['user_id' => $userId, 'matrix_root_id' => $rootMatrixId, 'active' => MatrixUserSetting::ACTIVE_TRUE])->one();
		if (!empty($matrixUserSetting)){
			$matrix = Matrix::find()->where(['root_id' => $matrixUserSetting->matrix_root_id, 'id' => $matrixUserSetting->matrix_id])->one();

			if (empty($matrix)){
				return false;
			}

			foreach($matrix->children()->all() as $child){
				if ($child->sort == $matrixUserSetting->sort){
					$matrixUserSetting->active = MatrixUserSetting::ACTIVE_FALSE;
					$matrixUserSetting->save(false);
					return false;
				}
			}
			$matrixUserSetting->active = MatrixUserSetting::ACTIVE_FALSE;
			$matrixUserSetting->save(false);
			return $matrixUserSetting;
		}else{
			return false;
		}
	}

	// взять место потомка в матрице
	private function getDescendantPlace()
	{
		$placeFinded = false;

		$ancestors = $this->user->ancestors()->all();

		if (empty($ancestors)){
			$matrixPlace = Matrix::find()->where(['user_id' => $this->user->id, 'root_id' => $this->rootMatrix->id])->one();
			return $matrixPlace;
		}

		foreach(array_reverse($ancestors) as $parentUser){

			$matrixPlace = Matrix::find()->where(['user_id' => $parentUser->id, 'root_id' => $this->rootMatrix->id])->one();

			if (empty($matrixPlace)){
				continue;
			}else{
				$placeFinded = true;
				break;
			}
		}

		if ($placeFinded == false){
			throw new \Exception(Yii::t('app', 'Нет мест в матрице'));
		}

		return $matrixPlace;
	}

    // узнать каким потомком является
	private function getCountDescendant()
	{
		$ancestor = $this->matrix->ancestors($this->matrixPref[MatrixPref::KEY_DEPTH]->value)->one();

		$countInLvl = 0;
        foreach ($ancestor->descendants($this->matrixPref[MatrixPref::KEY_DEPTH]->value)->all() as $descendant) {

            if($descendant->getLevelTo($ancestor) == $this->matrixPref[MatrixPref::KEY_DEPTH]->value){
                $countInLvl++;
            }
        }

		return $countInLvl;
	}

}
?>