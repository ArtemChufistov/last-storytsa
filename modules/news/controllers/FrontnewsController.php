<?php

namespace app\modules\news\controllers;

use Yii;
use app\modules\news\models\News;
use app\modules\news\models\NewsSearch;
use app\components\FrontendController;

class FrontnewsController extends FrontendController
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor' => '3373751' //синий
            ],
        ];
    }

	public function actionIndex()
	{
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	    return $this->render('news/news', [
	        'searchModel' => $searchModel,
	        'dataProvider' => $dataProvider,
	    ]);
	}

	public function actionShow($slug = 'index')
	{
	    $model = News::find()->where(['slug' => $slug])->one();

	    return $this->render('news/show', [
	        'model' => $model,
	    ]);
	}
}