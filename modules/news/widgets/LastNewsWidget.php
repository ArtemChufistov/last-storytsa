<?php
namespace app\modules\news\widgets;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Widget;
use app\modules\news\models\News;
use app\components\LayoutComponent;

class LastNewsWidget extends Widget{

	public $count = 3;

	public $view;

	public function init(){
	}

	public function run(){

		$lastNews = News::find()->orderBy(['lang' => \Yii::$app->language, 'id' => 'desc'])->limit($this->count)->all();

		return \Yii::$app->controller->renderPartial(LayoutComponent::themePath('/news/widgets/' . $this->view), [
			'lastNews' => $lastNews,
		]);
	}
}
?>