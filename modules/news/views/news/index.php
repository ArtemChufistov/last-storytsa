<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\news\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Список новостей');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <div class="col-md-12">
      <div class="box box-success collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
      </div>
    </div>


    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header">
          <p>
            <?= Html::a(Yii::t('app', 'Добавить новость'), ['create'], ['class' => 'btn btn-success']) ?>
          </p>
        </div>

        <div class="box-body no-padding">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'date',
                    'slug',
                    'title',
                    'smallDescription:ntext',
                    'lang',
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
      </div>
    </div>
    <?php Pjax::end(); ?>
</div>
