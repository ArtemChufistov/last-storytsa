<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

        <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

        <div class="col-md-4">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'date')
                ->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => $model->getAttributeLabel('date')],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>
        </div>

        <div class="col-md-2">
            <?= $form->field($model, 'lang')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-12">

            <?= $form->field($model, 'smallDescription')->widget(CKEditor::className(), [
                'editorOptions' => ElFinder::ckeditorOptions('elfinder', []),
            ]); ?>
        </div>

        <div class="col-md-12">

            <?php if ($model->image) {
                    echo "<img src='/".$model->image."' class='thumbnail'>";
                    echo "<p>" . Html::a(Yii::t('user', 'Удалить изображение'), ['rmv', 'id' => $model->id]) . "</p>";
                }
            ?>
            <?= $form->field($model, 'photo')->fileInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel('photo')
            ]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'fullDescription')->widget(CKEditor::className(), [
                'editorOptions' => ElFinder::ckeditorOptions('elfinder', []),
            ]); ?>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        </div>
      </div>
    </div>
</div>
