<?php

namespace app\modules\news\models;

use yii\imagine\Image;
use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $lang
 * @property string $slug
 * @property string $date
 * @property string $title
 * @property string $smallDescription
 * @property string $fullDescription
 */
class News extends \yii\db\ActiveRecord
{
    // Расширение сохраняемого файла изобрежния
    const EXT = '.jpg';

    public $photo;  // изображение
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'slug', 'title', 'smallDescription', 'fullDescription'], 'required'],
            [['date', 'photo'], 'safe'],
            [['smallDescription', 'fullDescription'], 'string'],
            [['lang', 'slug', 'title'], 'string', 'max' => 255],
            [['photo'], 'image',
                'minHeight' => 100,
                'skipOnEmpty' => true
            ],  // Изображение не менее 100 пикселей в высоту
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lang' => Yii::t('app', 'Язык'),
            'slug' => Yii::t('app', 'Ссылка'),
            'date' => Yii::t('app', 'Дата'),
            'title' => Yii::t('app', 'Заголовок'),
            'image' => Yii::t('app', 'Изображение'),
            'photo' => Yii::t('app', 'Изображение'),
            'smallDescription' => Yii::t('app', 'Краткое описание'),
            'fullDescription' => Yii::t('app', 'Полное описание'),
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveImage();
    }

    /**
     * Сохранение изображения
     */
    public function saveImage()
    {
        if ($this->photo) {
            $this->removeImage();   // Сначала удаляем старое изображение
            $module = Yii::$app->controller->module;
            $path = $module->imagePath; // Путь для сохранения аватаров
            $name = time(); // Название файла
            $this->image = $path. '/' . $name . $this::EXT;   // Путь файла и название
            if (!file_exists($path)) {
                mkdir($path, 0777, true);   // Создаем директорию при отсутствии
            }
            if (is_object($this->photo)) {
                // Загружено через FileUploadInterface
                Image::autorotate($this->photo->tempName)->save($this->image);
            } else {
                // Загружено по ссылке с удаленного сервера
                file_put_contents($this->image, $this->photo);
            }
            $this::getDb()
                ->createCommand()
                ->update($this->tableName(), ['image' => $this->image], ['id' => $this->id])
                ->execute();
        }
    }

    /**
     * Удаляем изображение при его наличии
     */
    public function removeImage()
    {
        if ($this->image) {
            // Если файл существует
            if (file_exists($this->image)) {
                unlink($this->image);
            }
            // Не регистрация пользователя
            if (!$this->isNewRecord) {
                $this::getDb()
                    ->createCommand()
                    ->update($this::tableName(), ['image' => null], ['id' => $this->id])
                    ->execute();
            }
        }
    }
}
