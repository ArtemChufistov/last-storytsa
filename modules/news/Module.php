<?php

namespace app\modules\news;

/**
 * news module definition class
 */
class Module extends \yii\base\Module
{
    public $imagePath = 'attach/news/images';
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\news\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
