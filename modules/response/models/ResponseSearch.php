<?php

namespace app\modules\response\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\response\models\Response;

/**
 * ResponseSearch represents the model behind the search form of `app\modules\response\models\Response`.
 */
class ResponseSearch extends Response
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['name', 'text', 'img1', 'img2', 'img3', 'video1', 'video2', 'video3', 'date', 'public'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Response::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'public' => $this->public
        ]);

        if ($this->date != ''){
            $query->andFilterWhere(['between', 'date', date('Y-m-d 0:0:0', strtotime($this->date)), date('Y-m-d 23:59:59', strtotime($this->date))]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'img1', $this->img1])
            ->andFilterWhere(['like', 'img2', $this->img2])
            ->andFilterWhere(['like', 'img3', $this->img3])
            ->andFilterWhere(['like', 'video1', $this->video1])
            ->andFilterWhere(['like', 'video2', $this->video2])
            ->andFilterWhere(['like', 'video3', $this->video3]);

        return $dataProvider;
    }
}
