<?php

namespace app\modules\response\models;

use Yii;
use yii\imagine\Image;
use app\modules\profile\models\User;

/**
 * This is the model class for table "response".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $text
 * @property string $img1
 * @property string $img2
 * @property string $img3
 * @property string $video1
 * @property string $video2
 * @property string $video3
 * @property string $date
 */
class Response extends \yii\db\ActiveRecord
{
    const COUNT_PUBLIC_RESP = 5; // сколько отзывов можно пубилковать пользователю

    const EXT = '.jpg';
    /**
     * @inheritdoc
     */

    const PUBLIC_TRUE = 1;
    const PUBLIC_FALSE = 0;

    public $photo1;  // изображение
    public $photo2;  // изображение
    public $photo3;  // изображение

    public static function getPublicArray()
    {
        return [
            self::PUBLIC_TRUE => Yii::t('app', 'Опубликован'),
            self::PUBLIC_FALSE => Yii::t('app', 'Неопубликован'),
        ];
    }

    public function getPublicTitle()
    {
        $array = self::getPublicArray();

        if (!empty($array[$this->public])){
            return $array[$this->public];
        }

        return '';
    }

    public static function tableName()
    {
        return 'response';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'name'], 'required'],
            [['user_id'], 'integer'],
            [['text'], 'string'],
            [['date', 'public'], 'safe'],
            [['img1', 'img2', 'img3'], 'string', 'max' => 255],
            [['text'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            [['photo1', 'photo2', 'photo3'], 'image',
                'minHeight' => 100,
                'skipOnEmpty' => true
            ],  // Изображение не менее 100 пикселей в высоту
            [['name', 'img1', 'img2', 'img3', 'video1', 'video2', 'video3'], 'string', 'max' => 255],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->date = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveImg1();
        $this->saveImg2();
        $this->saveImg3();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function saveImg1()
    {

        if ($this->photo1) {
            $this->removeImg1();   // Сначала удаляем старое изображение
            $module = Yii::$app->controller->module;
            $path = $module->photoPath1; // Путь для сохранения аватаров
            $name = time(); // Название файла
            $this->img1 = $path. '/' . $name . $this::EXT;   // Путь файла и название
            if (!file_exists($path)) {
                mkdir($path, 0777, true);   // Создаем директорию при отсутствии
            }
            if (is_object($this->photo1)) {
                // Загружено через FileUploadInterface
                Image::thumbnail($this->photo1->tempName, 200, 200)->save($this->img1);   // Сохраняем изображение в формате 200x200 пикселей
            } else {
                // Загружено по ссылке с удаленного сервера
                file_put_contents($this->img1, $this->photo1);
            }
            $this::getDb()
                ->createCommand()
                ->update($this->tableName(), ['img1' => $this->img1], ['id' => $this->id])
                ->execute();
        }
    }

    public function saveImg2()
    {
        if ($this->photo2) {
            $this->removeImg2();   // Сначала удаляем старое изображение
            $module = Yii::$app->controller->module;
            $path = $module->photoPath2; // Путь для сохранения аватаров
            $name = time(); // Название файла
            $this->img2 = $path. '/' . $name . $this::EXT;   // Путь файла и название
            if (!file_exists($path)) {
                mkdir($path, 0777, true);   // Создаем директорию при отсутствии
            }
            if (is_object($this->photo2)) {
                // Загружено через FileUploadInterface
                Image::thumbnail($this->photo2->tempName, 200, 200)->save($this->img2);   // Сохраняем изображение в формате 200x200 пикселей
            } else {
                // Загружено по ссылке с удаленного сервера
                file_put_contents($this->img2, $this->photo2);
            }
            $this::getDb()
                ->createCommand()
                ->update($this->tableName(), ['img2' => $this->img2], ['id' => $this->id])
                ->execute();
        }
    }

    public function saveImg3()
    {
        if ($this->photo3) {
            $this->removeImg3();   // Сначала удаляем старое изображение
            $module = Yii::$app->controller->module;
            $path = $module->photoPath3; // Путь для сохранения аватаров
            $name = time(); // Название файла
            $this->img3 = $path. '/' . $name . $this::EXT;   // Путь файла и название
            if (!file_exists($path)) {
                mkdir($path, 0777, true);   // Создаем директорию при отсутствии
            }
            if (is_object($this->photo3)) {
                // Загружено через FileUploadInterface
                Image::thumbnail($this->photo3->tempName, 200, 200)->save($this->img3);   // Сохраняем изображение в формате 200x200 пикселей
            } else {
                // Загружено по ссылке с удаленного сервера
                file_put_contents($this->img3, $this->photo3);
            }
            $this::getDb()
                ->createCommand()
                ->update($this->tableName(), ['img3' => $this->img3], ['id' => $this->id])
                ->execute();
        }
    }

    /**
     * Удаляем изображение при его наличии
     */
    public function removeImg1()
    {
        if ($this->img1) {
            // Если файл существует
            if (file_exists($this->img1)) {
                unlink($this->img1);
            }
            // Не регистрация пользователя
            if (!$this->isNewRecord) {
                $this::getDb()
                    ->createCommand()
                    ->update($this::tableName(), ['image' => null], ['id' => $this->id])
                    ->execute();
            }
        }
    }

    /**
     * Удаляем изображение при его наличии
     */
    public function removeImg2()
    {
        if ($this->img2) {
            // Если файл существует
            if (file_exists($this->img2)) {
                unlink($this->img2);
            }
            // Не регистрация пользователя
            if (!$this->isNewRecord) {
                $this::getDb()
                    ->createCommand()
                    ->update($this::tableName(), ['image' => null], ['id' => $this->id])
                    ->execute();
            }
        }
    }

    /**
     * Удаляем изображение при его наличии
     */
    public function removeImg3()
    {
        if ($this->img3) {
            // Если файл существует
            if (file_exists($this->img3)) {
                unlink($this->img3);
            }
            // Не регистрация пользователя
            if (!$this->isNewRecord) {
                $this::getDb()
                    ->createCommand()
                    ->update($this::tableName(), ['image' => null], ['id' => $this->id])
                    ->execute();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Участник'),
            'name' => Yii::t('app', 'Имя'),
            'public' => Yii::t('app', 'Опубликован'),
            'text' => Yii::t('app', 'Текст отзыва'),
            'img1' => Yii::t('app', 'Изображение № 1'),
            'img2' => Yii::t('app', 'Изображение № 2'),
            'img3' => Yii::t('app', 'Изображение № 3'),
            'photo1' => Yii::t('app', 'Изображение № 1'),
            'photo2' => Yii::t('app', 'Изображение № 2'),
            'photo3' => Yii::t('app', 'Изображение № 3'),
            'video1' => Yii::t('app', 'Код видео отзыва № 1'),
            'video2' => Yii::t('app', 'Код видео отзыва № 2'),
            'video3' => Yii::t('app', 'Код видео отзыва № 3'),
            'date' => Yii::t('app', 'Дата добавления'),
        ];
    }
}
