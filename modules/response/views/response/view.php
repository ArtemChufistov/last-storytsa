<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Отзывы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="response-view">

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                      'attribute' => 'user_id',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
                    ],
                    'name',
                    'text:ntext',
                    [
                      'attribute' => 'img1',
                      'format' => 'raw',
                      'filter' => false,
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_img1', ['model' => $model]);},
                    ],[
                      'attribute' => 'img2',
                      'format' => 'raw',
                      'filter' => false,
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_img2', ['model' => $model]);},
                    ],[
                      'attribute' => 'img3',
                      'format' => 'raw',
                      'filter' => false,
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_img3', ['model' => $model]);},
                    ],
                    'video1',
                    'video2',
                    'video3',
                    'date',
                ],
            ]) ?>
        </div>
      </div>
    </div>
  </div>

</div>
