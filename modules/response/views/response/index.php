<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use app\modules\response\models\Response;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\response\models\ResponseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отзывы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
                <?php Pjax::begin(); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        [
                          'attribute' => 'public',
                          'format' => 'raw',
                          'filter' => Select2::widget([
                              'model' => $searchModel,
                              'attribute' => 'public',
                              'theme' => Select2::THEME_BOOTSTRAP,
                              'data' => Response::getPublicArray(),
                              'options' => [
                                  'placeholder' => Yii::t('app', 'Выберите'),
                                  'style' => ['width' => '150px'],
                              ],]
                            ),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_public', ['model' => $model]);},
                        ],
                        [
                          'attribute' => 'user_id',
                          'format' => 'raw',
                          'filter' => Select2::widget([
                              'name' => (new ReflectionClass($searchModel))->getShortName() . '[user_id]',
                              'theme' => Select2::THEME_BOOTSTRAP,
                              'options' => [
                                  'placeholder' => Yii::t('app', 'Введите логин'),
                                  'style' => ['width' => '150px']
                              ],
                              'pluginOptions' => [
                                  //'minimumInputLength' => 2,
                                  'ajax' => [
                                      'url' => Url::to(['/profile/backuser/searhbylogin']),
                                      'dataType' => 'json',
                                      'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                  ],
                                  'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                  'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                  'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                              ]
                          ]),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
                        ],
                        'name',
                        [
                          'attribute' => 'text',
                          'format' => 'raw',
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_text', ['model' => $model]);},
                        ],[
                          'attribute' => 'img1',
                          'format' => 'raw',
                          'filter' => false,
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_img1', ['model' => $model]);},
                        ],
                        [
                          'attribute' => 'date',
                          'format' => 'raw',
                          'filter' => true,
                          'filter' => DatePicker::widget([
                              'model'=>$searchModel,
                              'attribute'=>'date',
                              'options' => ['class' => 'form-control', 'style' => ['width' => '150px']],
                              'language' => 'ru',

                          ]),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_date', ['model' => $model]);},
                        ],
                        // 'img2',
                        // 'img3',
                        // 'video1',
                        // 'video2',
                        // 'video3',
                        // 'date',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
          </div>
        </div>
    </div>
</div>

