<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\widgets\FileInput;
use kartik\widgets\DatePicker;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use app\modules\response\models\Response;
/* @var $this yii\web\View */
/* @var $model app\modules\response\models\Response */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

            <div class="response-form">

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'initValueText' => $model->getUser()->one()->login,
                    'value' => $model->getUser()->one()->id,
                    'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'minimumInputLength' => 2,
                        'ajax' => [
                            'url' => Url::to(['/profile/backuser/searhbylogin']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.login; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                    ],
                ]);?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', []),
                ]); ?>

                <?php echo FileInput::widget([
                    'name' => (new ReflectionClass($model))->getShortName() . '[img1]',
                    'options'=>[
                        'multiple'=>false
                    ],
                    'pluginOptions' => [
                        'initialPreview'=>[
                            'http://' . Yii::$app->params['frontSiteUrl'] . '/' . $model->img1,
                        ],
                        'initialPreviewAsData'=>true,
                        'overwriteInitial'=>false,
                        'maxFileSize'=>2800
                    ]
                ]);
                ?>

                <?php echo FileInput::widget([
                    'name' => (new ReflectionClass($model))->getShortName() . '[img2]',
                    'options'=>[
                        'multiple'=>false
                    ],
                    'pluginOptions' => [
                        'initialPreview'=>[
                            'http://' . Yii::$app->params['frontSiteUrl'] . '/' . $model->img2,
                        ],
                        'initialPreviewAsData'=>true,
                        'overwriteInitial'=>false,
                        'maxFileSize'=>2800
                    ]
                ]);
                ?>

                <?php echo FileInput::widget([
                    'name' => (new ReflectionClass($model))->getShortName() . '[img3]',
                    'options'=>[
                        'multiple'=>false
                    ],
                    'pluginOptions' => [
                        'initialPreview'=>[
                            'http://' . Yii::$app->params['frontSiteUrl'] . '/' . $model->img3,
                        ],
                        'initialPreviewAsData'=>true,
                        'overwriteInitial'=>false,
                        'maxFileSize'=>2800
                    ]
                ]);
                ?>

                <?= $form->field($model, 'video1')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'video2')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'video3')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'date')
                ->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => $model->getAttributeLabel('date')],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>

                <?php echo $form->field($model, 'public')->widget(Select2::classname(), [
                    'data' => Response::getPublicArray(),
                    'options' => ['placeholder' => Yii::t('app', 'Выберите')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        
        </div>
      </div>
    </div>
</div>