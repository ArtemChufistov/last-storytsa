<?php

namespace app\modules\response;

/**
 * response module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\response\controllers';

    public $photoPath1 = 'attach/response/images1';
    public $photoPath2 = 'attach/response/images2';
    public $photoPath3 = 'attach/response/images3';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
