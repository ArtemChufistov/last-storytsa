<?php

namespace app\modules\stat\controllers;

use app\admin\controllers\AdminController;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\search\PaymentStatSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * NewsController implements the CRUD actions for News model.
 */
class PaymentController extends AdminController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'view', 'create', 'update', 'delete'],
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['administrator'],
					], [
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['statPaymentIndex'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	public function actionIndex() {
		$paySystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();

		$searchModel = new PaymentStatSearch();

		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$statArray = [];
		$perdayArray = [];
		foreach (array_reverse($dataProvider->getModels()) as $item) {
			$perdayArray[strtotime($item->perday)] = date('d/m/Y', strtotime($item->perday));
			if ($item->to_pay_system_id == $paySystemLk->id) {
				$statArray[Yii::t('app', 'Пополнения в статусе "{status}"', ['status' => $item->getStatusTitle()])][strtotime($item->perday)] = floatval($item->toSumPay);
			} elseif ($item->from_pay_system_id == $paySystemLk->id) {
				$statArray[Yii::t('app', 'Снятие в статусе "{status}"', ['status' => $item->getStatusTitle()])][strtotime($item->perday)] = floatval($item->sumPay);
			}
		}

		$num = 0;
		$resultStatArray = [];
		foreach ($statArray as $statName => $stat) {
			foreach ($perdayArray as $perdayTitle => $perdayItem) {
				if (empty($stat[$perdayTitle])) {
					$stat[$perdayTitle] = 0;
				}
			}
			ksort($stat);
			$resultStatArray[$num]['name'] = $statName;
			$resultStatArray[$num]['data'] = array_values($stat);
			$num++;
		}

		return $this->render('index', [
			'perdayArray' => $perdayArray,
			'searchModel' => $searchModel,
			'statArray' => $resultStatArray,
		]);
	}
}