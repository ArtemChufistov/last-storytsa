<?php

namespace app\modules\stat\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\admin\controllers\AdminController;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\search\UserBalanceStatSearch;

/**
 * NewsController implements the CRUD actions for News model.
 */
class UserbalanceController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['statUserBalanceIndex'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserBalanceStatSearch();
        $searchModel->initParams(Yii::$app->request->queryParams);

        $perdayArray = [];
        $balanceArray = [];

        for($currentDate = strtotime(date('Y-m-d 23:59:59', strtotime($searchModel->dateFrom))); $currentDate <= strtotime($searchModel->dateTo); $currentDate += 60*60*24){
            $balanceArray[] = $searchModel->sumByDate(date('Y-m-d H:i:s', $currentDate));
            $perdayArray[] = date('d/m/Y', $currentDate); 

        }

        return $this->render('index', [
            'balanceArray' => $balanceArray,
            'searchModel' => $searchModel,
            'perdayArray' => $perdayArray,
        ]);
    }
}