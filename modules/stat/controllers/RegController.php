<?php

namespace app\modules\stat\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\admin\controllers\AdminController;
use app\modules\profile\models\UserStatSearch;

/**
 * NewsController implements the CRUD actions for News model.
 */
class RegController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['statRegIndex'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserStatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $perdayArray = [];
        $countUserArray = [];
        foreach($dataProvider->getModels() as $item){
            $perdayArray[] = date('d/m/Y', strtotime($item->perday)); 
            $countUserArray[] = intval($item->countUser); 
        }

        return $this->render('index', [
            'perdayArray' => $perdayArray,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'countUserArray' => $countUserArray,
        ]);
    }
}