<?php

namespace app\modules\stat\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\admin\controllers\AdminController;
use yii\filters\AccessControl;
use app\modules\invest\models\InvestStatSearch;

/**
 * NewsController implements the CRUD actions for News model.
 */
class InvestController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['statInvestIndex'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new InvestStatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $perdayArray = [];
        $sumInvestArray = [];
        foreach($dataProvider->getModels() as $item){
            $perdayArray[] = date('d/m/Y', strtotime($item->perday)); 
            $sumInvestArray[] = floatval($item->sumPay); 
        }

        return $this->render('index', [
            'sumInvestArray' => $sumInvestArray,
            'perdayArray' => $perdayArray,
            'searchModel' => $searchModel,
        ]);
    }
}