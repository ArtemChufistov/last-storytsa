<?php

namespace app\modules\stat\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\admin\controllers\AdminController;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\search\TransactionStatSearch;

/**
 * NewsController implements the CRUD actions for News model.
 */
class TransactionController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['statUserBalanceIndex'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TransactionStatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $statArray = [];
        $perdayArray = [];
        foreach (array_reverse($dataProvider->getModels()) as $item) {
            $perdayArray[strtotime($item->perday)] = date('d/m/Y', strtotime($item->perday));
            $statArray[$item->getTypetitle()][$item->perday] = floatval($item->sum);
        }

        $num = 0;
        $resultStatArray = [];
        foreach ($statArray as $statName => $stat) {
            ksort($stat);
            $resultStatArray[$num]['name'] = $statName;
            $resultStatArray[$num]['data'] = array_values($stat);
            $num++;
        }

        $transactionTypes = ArrayHelper::map(Transaction::find()->where(['currency_id' => $searchModel->currency_id])->select('type')->distinct()->all(), 'type', 'type');
        $fullTransactionType = Transaction::getTypeArray();

        foreach($transactionTypes as $typeKey => &$typeTitle){
            $typeTitle = $fullTransactionType[$typeKey];
        }

        return $this->render('index', [
            'transactionTypes' => $transactionTypes,
            'perdayArray' => $perdayArray,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statArray' => $resultStatArray,
        ]);
    }
}