<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\payment\models\CurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Места в матрице');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-index">

    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                <h1><?= Html::encode($this->title) ?></h1>
                <p>
                    <?= Html::a(Yii::t('app', 'Добавить валюту'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>
            <?php Pjax::begin(); ?>

                <div class="box-body no-padding">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                              'attribute' => 'countPlaces',
                              'format' => 'raw',
                            ],
                            [
                              'attribute' => 'date_add',
                              'format' => 'raw',
                              'filter' => DatePicker::widget([
                                  'model'=>$searchModel,
                                  'attribute'=>'date_add',
                                  'options' => ['class' => 'form-control'],
                                  'language' => 'ru',
    
                              ]),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_date-add', ['model' => $model]);},
                            ],
                            [
                              'attribute' => 'user_id',
                              'format' => 'raw',
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[user_id]',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Введите логин'),
                                  ],
                                  'pluginOptions' => [
                                      'minimumInputLength' => 2,
                                      'ajax' => [
                                          'url' => Url::to(['/profile/backuser/searhbylogin']),
                                          'dataType' => 'json',
                                          'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                      ],
                                      'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                      'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                      'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                                  ]
                              ]),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
                            ]
                        ],
                    ]); ?>
                </div>
            <?php Pjax::end(); ?>
          </div>
        </div>
    </div>
</div>

