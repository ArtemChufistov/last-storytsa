<?php

use yii\web\JsExpression;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use app\assets\OfficeEndAppAsset;
use miloschuman\highcharts\Highcharts;
use app\modules\profile\models\User;
use yii\widgets\ActiveForm;
use app\modules\contact\models\ContactMessage;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\menu\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Статистика регистраций');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">
  <div class = "row">

      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header">
          </div>
          <div class="box-body no-padding innerRowPadding5">
            <?php $form = ActiveForm::begin(['action' => [''], 'method' => 'get']);?>
            <div class = "row">
                <div class="col-md-3">
                  <?= $form->field($searchModel, 'dateFrom')->widget(DatePicker::classname(), [
                      'options' => ['placeholder' => Yii::t('app', 'Дата от')],
                      'pluginOptions' => [
                          'format' => 'yyyy-mm-dd',
                          'autoclose'=>true
                      ]
                  ]);?>
                </div>
                <div class="col-md-3">
                  <?= $form->field($searchModel, 'dateTo')->widget(DatePicker::classname(), [
                      'options' => ['placeholder' => Yii::t('app', 'Дата до')],
                      'pluginOptions' => [
                          'format' => 'yyyy-mm-dd',
                          'autoclose'=>true
                      ]
                  ]);?>
                </div>
            </div>
            <div class = "row">
                <div class="col-md-12">
                  <div class="form-group">
                    <?=Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-primary'])?>

                    <a href = "<?php echo Url::to(['reg/index']);?>" class = "btn btn-success"><?php echo Yii::t('app', 'Сбросить фильтр');?></a>
                  </div>
                </div>
            </div>
            <?php ActiveForm::end();?>
          </div>
        </div>
      </div>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header">

        </div>
          <div class="box-body no-padding">
            
          <?php echo Highcharts::widget([
               'options' => [
                  'title' => [
                    'text' => Yii::t('app', 'Статистика регистраций по дням')
                  ],
                  'chart' => [
                      'type' => 'area'
                  ],
                  'xAxis' => [
                     'categories' => $perdayArray
                  ],
                  'yAxis' => [
                     'title' => Yii::t('app', 'Количество')
                  ],
                  'series' => [
                     ['name' => Yii::t('app', 'Количество'), 'data' => $countUserArray],
                  ],
                  'credits' => [
                    'enabled' => false
                  ]
               ]
            ]);?>

          </div>
      </div>
    </div>
   <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo Yii::t('app', 'Сводная статистика');?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered">
            <tr>
              <td><?php echo Yii::t('app', 'Итого за выбранный период:');?></td>
              <?php $count = 0;?>
              <?php foreach($countUserArray as $countUser):?>
                <?php $count += $countUser;?>
              <?php endforeach;?>
              <td><strong><?php echo $count;?></strong> <?php echo Yii::t('app', 'Пользователей');?></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  .ct-label{
    font-size: 18px;
    font-weight: bold;
  }
  .innerRowPadding5 .row{
    margin-left: 5px;
    margin-right: 5px;
  }
</style>