<?php

use app\modules\finance\models\search\PaymentStatSearch;
use app\modules\finance\models\Currency;
use kartik\select2\Select2;
use miloschuman\highcharts\Highcharts;
use okeanos\chartist\Chartist;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

$this->title = Yii::t('app', 'Статистика оплат');
$this->params['breadcrumbs'][] = $this->title;

foreach($searchModel->officeTypeArray as $officeType => $num){
  $searchModel->officeTypeArray[$officeType] = $officeType;
}

foreach($searchModel->statusArray as $statusType => $num){
  $searchModel->statusArray[$statusType] = $statusType;
}

?>
<div class="menu-index">

  <div class = "row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header">
          </div>
          <div class="box-body no-padding innerRowPadding5">
            <?php $form = ActiveForm::begin(['action' => [''], 'method' => 'get']);?>
            <div class = "row">
                <div class="col-md-2">
                  <?php echo $form->field($searchModel, 'currency')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
                    'options' => ['placeholder' => 'Выберите валюту...'],
                    'pluginOptions' => [
                      'allowClear' => true,
                    ],
                  ]); ?>
                </div>
                <div class="col-md-4">
                  <?php echo $form->field($searchModel, 'officeTypeArray')->widget(Select2::classname(), [
                      'data' => PaymentStatSearch::getOfficeTypeArray(),
                      'options' => ['placeholder' => Yii::t('app', 'Выберите тип платежа'), 'multiple' => true],
                      'pluginOptions' => [
                          'tags' => true,
                          'tokenSeparators' => [',', ' '],
                          'maximumInputLength' => 10
                      ],
                  ])
                  ?>
                </div>
              </div>
              <div class = "row">
                <div class="col-md-4">
                  <?php echo $form->field($searchModel, 'statusArray')->widget(Select2::classname(), [
                      'data' => PaymentStatSearch::getStatusArray(),
                      'options' => ['placeholder' => Yii::t('app', 'Выберите статусы платежа'), 'multiple' => true],
                      'pluginOptions' => [
                          'tags' => true,
                          'tokenSeparators' => [',', ' '],
                          'maximumInputLength' => 10
                      ],
                  ])
                  ?>
                </div>
            </div>
            <div class = "row">
                <div class="col-md-3">
                  <?= $form->field($searchModel, 'dateFrom')->widget(DatePicker::classname(), [
                      'options' => ['placeholder' => Yii::t('app', 'Дата от')],
                      'pluginOptions' => [
                          'format' => 'yyyy-mm-dd',
                          'autoclose'=>true
                      ]
                  ]);?>
                </div>
                <div class="col-md-3">
                  <?= $form->field($searchModel, 'dateTo')->widget(DatePicker::classname(), [
                      'options' => ['placeholder' => Yii::t('app', 'Дата до')],
                      'pluginOptions' => [
                          'format' => 'yyyy-mm-dd',
                          'autoclose'=>true
                      ]
                  ]);?>
                </div>
            </div>
            <div class = "row">
                <div class="col-md-12">
                  <div class="form-group">
                    <?=Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-primary'])?>
                    <a href = "<?php echo Url::to(['payment/index']);?>" class = "btn btn-success"><?php echo Yii::t('app', 'Сбросить фильтр');?></a>
                  </div>
                </div>
            </div>
            <?php ActiveForm::end();?>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header">

          </div>
            <div class="box-body no-padding">

            <?php echo Highcharts::widget([
            	'options' => [
            		'title' => [
            			'text' => Yii::t('app', 'Оплаты в {currency}', ['currency' => $searchModel->getCurrency()->one()->key]),
            		],
            		'chart' => [
            			'type' => 'column',
            		],
            		'xAxis' => [
            			'categories' => array_values($perdayArray),
            		],
            		'yAxis' => [
            			'title' => Yii::t('app', 'Количество'),
            		],
            		'series' => $statArray,
            		'credits' => [
            			'enabled' => false,
            		],
            	],
            ]); ?>
            </div>
        </div>
      </div>

     <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo Yii::t('app', 'Сводная статистика за выбранный период');?></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered">
              <?php foreach($statArray as $statItem):?>
              <tr>
                <td><?php echo $statItem['name'];?></td>
                <td>
                  <?php $sum = 0; ?>
                  <?php foreach($statItem['data'] as $statData):?>
                    <?php $sum += $statData;?>
                  <?php endforeach;?>
                  <?php echo number_format($sum, 2, '.', ' '); ?> <?php echo $searchModel->getCurrency()->one()->key;?>                 
                </td>
              </tr>
              <?php endforeach;?>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>


<style type="text/css">
  .ct-label{
    font-size: 18px;
    font-weight: bold;
  }
  .innerRowPadding5 .row{
    margin-left: 5px;
    margin-right: 5px;
  }
</style>