<?php

use yii\web\JsExpression;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use app\assets\OfficeEndAppAsset;
use miloschuman\highcharts\Highcharts;
use app\modules\profile\models\User;
use app\modules\finance\models\Currency;
use app\modules\finance\models\Transaction;
use app\modules\contact\models\ContactMessage;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\menu\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Статистика транзакций');
$this->params['breadcrumbs'][] = $this->title;

foreach($searchModel->officeTypeArray as $officeType => $num){
  $searchModel->officeTypeArray[$officeType] = $officeType;
}

?>
<div class="menu-index">
  <div class = "row">

      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header">
          </div>
          <div class="box-body no-padding innerRowPadding5">
            <?php $form = ActiveForm::begin(['action' => [''], 'method' => 'get']);?>
            <div class = "row">
              <div class="col-md-2">
                <?php echo $form->field($searchModel, 'currency_id')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(Currency::find()->all(), 'id', 'title'),
                  'options' => ['placeholder' => 'Выберите валюту...'],
                  'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'allowClear' => true,
                  ],
                ]); ?>
              </div>
              <div class="col-md-3">
                <?= $form->field($searchModel, 'dateFrom')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => Yii::t('app', 'Дата от')],
                    'pluginOptions' => [
                        'autoclose'=>true
                    ]
                ]);?>
              </div>
              <div class="col-md-3">
                <?= $form->field($searchModel, 'dateTo')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => Yii::t('app', 'Дата до')],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'autoclose'=>true
                    ]
                ]);?>
              </div>
            </div>
            <div class = "row">
              <div class="col-md-4">
                <?php echo $form->field($searchModel, 'officeTypeArray')->widget(Select2::classname(), [
                    'data' => $transactionTypes,
                    'options' => ['placeholder' => Yii::t('app', 'Выберите типы внутренних транзакций'), 'multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'tokenSeparators' => [',', ' '],
                        'maximumInputLength' => 10
                    ],
                ])
                ?>
              </div>
            </div>
            <div class = "row">
              <div class="col-md-12">
                <div class="form-group">
                  <?= Html::submitButton(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-primary'])?>

                  <a href = "<?php echo Url::to(['transaction/index']);?>" class = "btn btn-success"><?php echo Yii::t('app', 'Сбросить фильтр');?></a>
                </div>
              </div>
            </div>
            <?php ActiveForm::end();?>
          </div>
        </div>
      </div>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header">

        </div>
          <div class="box-body no-padding">

            <?php echo Highcharts::widget([
              'options' => [
                'title' => [
                  'text' => Yii::t('app', 'Транзакции валюты "{currency}"', ['currency' => $searchModel->getCurrency()->one()->title]),
                ],
                'chart' => [
                  'type' => 'column',
                ],
                'xAxis' => [
                  'categories' => array_values($perdayArray),
                ],
                'yAxis' => [
                  'title' => Yii::t('app', 'Количество'),
                ],
                'series' => $statArray,
                'credits' => [
                  'enabled' => false,
                ],
              ],
            ]); ?>

          </div>
      </div>
    </div>
   <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo Yii::t('app', 'Сводная статистика');?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered">
              <?php foreach($statArray as $statItem):?>
              <tr>
                <td><?php echo $statItem['name'];?></td>
                <td>
                  <?php $sum = 0; ?>
                  <?php foreach($statItem['data'] as $statData):?>
                    <?php $sum += $statData;?>
                  <?php endforeach;?>
                  <?php echo number_format($sum, 2, '.', ' '); ?> <?php echo $searchModel->getCurrency()->one()->title;?>                 
                </td>
              </tr>
              <?php endforeach;?>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  .ct-label{
    font-size: 18px;
    font-weight: bold;
  }
  .innerRowPadding5 .row{
    margin-left: 5px;
    margin-right: 5px;
  }
</style>