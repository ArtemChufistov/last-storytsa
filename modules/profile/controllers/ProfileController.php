<?php

namespace app\modules\profile\controllers;

use app\components\RefComponent;
use app\components\UserController;
use app\modules\merchant\components\twofa\models\TwoFaForm;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\models\forms\LoginForm;
use app\modules\profile\models\forms\PasswordResetForm;
use app\modules\profile\models\forms\SignupNoLoginForm;
use app\modules\profile\models\forms\SignupForm;
use app\modules\profile\models\ResetPassword;
use app\modules\mainpage\models\Preference;
use app\modules\profile\models\User;
use yii\web\BadRequestHttpException;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;

class ProfileController extends UserController
{
    public $ref;

    public function init()
    {
        $this->ref = RefComponent::init();
        $this->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/profile';
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => null,
                'foreColor' => '3373751', //синий
            ],
        ];
    }

    public function goHome()
    {
        return Yii::$app->getResponse()->redirect(Url::to(['/profile/office/dashboard']));
    }

    /**
     * Разделение ролей
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['captcha', 'login', 'signup', 'logout', 'confirm', 'online', 'show', 'index', 'view', 'reset'],
                'rules' => [
                    [
                        'actions' => ['login', 'signup', 'confirm', 'show', 'reset', 'submitemail'],
                        'allow' => true,
                        //'roles' => ['?'],
                    ],
                    [
                        'actions' => ['login', 'signup', 'show', 'logout', 'profile', 'online'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['captcha'],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * Регистрация
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        // Уже авторизированных отправляем на домашнюю страницу
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $prefSignUp = Preference::find()->where(['key' => Preference::KEY_SINGUP_NO_LOGIN])->one();

        if (empty($prefSignUp)) {
            $model = new SignupForm();
        } else {
            $model = new SignupNoLoginForm();
        }

        $model->ref = $this->ref;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                if (!empty($prefSignUp)) {
                    $user = User::find()->where(['id' => $model->id])->one();
                    $user->login = 'A' . $user->id;
                    $user->save(false);
                }
                Yii::$app->user->login(User::findIdentity($model->id));
                return $this->goHome();
            }
        }

        return $this->render('profile/signup', [
            'model' => $model,
        ]);
    }

    /**
     * Авторизация
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if (!empty($user)) {
                if (!$user->hasTwoFaEnabled()) {
                    $model->login();
                    return $this->redirect(['/office/dashboard']);
                }

                Yii::$app->user->createLoginVerificationSession($user); //Allow the user to verify the login
                return $this->redirect(['/login-verification']);
            }
        }

        $model->password = '';

        return $this->render('profile/login', [
            'model' => $model,
        ]);
    }

    public function actionLoginVerification()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = Yii::$app->user->getIdentityFromLoginVerificationSession();
        if ($user === null) {
            Yii::$app->session->destroy();
            return $this->goHome();
        }

        $model = new TwoFaForm();
        $model->setScenario(TwoFaForm::SCENARIO_LOGIN);
        $model->setUser($user);

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/office/dashboard']);
        }
        return $this->render('profile/login-verification', [
            'model' => $model,
        ]);
    }


    //Восстановление пароля
    public function actionResetpasswd()
    {
        $forget = new PasswordResetForm();
        if ($forget->load(Yii::$app->request->post()) && $forget->validate()) {
            if ($forget->sendEmail()) {
                // Отправлено подтверждение по Email
                Yii::$app->getSession()->setFlash('reset-success', Yii::t('user', 'Ссылка с активацией нового пароля отправлена на Email.'));
            }
        }

        return $this->render('profile/resetpasswd', [
            'forget' => $forget,
        ]);

    }

    public function actionSubmitemail($login, $hash)
    {

        $this->user = User::find()->where(['login' => $login])->one();

        if (empty($this->user) || $this->user->email_confirm_token == '' || $this->user->email_confirm_token != $hash) {
            Yii::$app->getResponse()->redirect(['/profile/office/dashboard']);
            return;
        } else {

            $this->user->email = $this->user->changing_email;
            $this->user->changing_email = '';
            $this->user->email_confirm_token = '';

            $this->user->updateAuthAssigment(AuthAssignment::ITEM_NAME_VERIFIED_USER);
            $this->user->save(false);

            return $this->render('office/submitemail', [
                'user' => $this->user,
            ]);
        }
    }

    /**
     * Сброс пароля через электронную почту
     * @param $token - токен сброса пароля, высылаемый почтой
     * @param $password - новый пароль
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionReset($token, $password)
    {
        try {
            $model = new ResetPassword($token, $password);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user_id = $model->resetPassword()) {

            // Авторизируемся при успешном сбросе пароля
            Yii::$app->user->login(User::findIdentity($user_id));
            return $this->goHome();
        }
        return $this->redirect(['/']);
    }

    /**
     * Деавторизация
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return parent::goHome();
    }

    public function actionSetref($ref)
    {
        RefComponent::init($ref);
    }
}
