<?php

namespace app\modules\profile\controllers;

use Yii;
use yii\helpers\Url;
use app\components\RefComponent;
use app\components\UserController;
use app\modules\profile\models\forms\LoginForm;
use app\modules\profile\models\forms\PasswordResetForm;
use app\modules\profile\models\forms\SignupForm;
use app\modules\profile\models\ResetPassword;
use app\modules\profile\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\admin\controllers\AdminController;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class BackuserController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['searhbylogin'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['searhbylogin'],
                        'allow' => true,
                        'roles' => ['searhbyloginIndex'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSearhbylogin()
    {
        $result = [];

        if (empty(Yii::$app->request->get('q'))){
            return $result;
        }

        $userArray = User::find()->where(['like', 'login', Yii::$app->request->get('q')])->all();

        foreach($userArray as $user){
            $result[] =[
                'id' => $user->id,
                'login' => $user->login,
            ];
        }

        echo json_encode(['results' => $result]);

    }
}
