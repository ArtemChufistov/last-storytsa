<?php

namespace app\modules\profile\controllers;

use Yii;
use app\modules\profile\models\Occupation;
use app\modules\profile\models\OccupationSearch;
use app\admin\controllers\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * OccupationController implements the CRUD actions for Occupation model.
 */
class OccupationController extends AdminController
{
    /**
     * @inheritdoc
     */
     public function behaviors()
     {
       return [
         'access' => [
           'class' => AccessControl::className(),
           'only' => ['index'],
           'rules' => [
             [
               'actions' => ['index'],
               'allow' => true,
               'roles' => ['administrator'],
             ], [
               'actions' => ['index'],
               'allow' => true,
               'roles' => ['occupationIndex'],
             ], [
               'actions' => ['create'],
               'allow' => true,
               'roles' => ['occupationCreate'],
             ], [
               'actions' => ['view'],
               'allow' => true,
               'roles' => ['occupationView'],
             ], [
               'actions' => ['update'],
               'allow' => true,
               'roles' => ['occupationUpdate'],
             ], [
               'actions' => ['delete'],
               'allow' => true,
               'roles' => ['occupationDelete'],
             ],
           ],
         ],
         'verbs' => [
           'class' => VerbFilter::className(),
           'actions' => [
             'delete' => ['POST'],
           ],
         ],
       ];
     }

    /**
     * Lists all Occupation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OccupationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Occupation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Occupation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Occupation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Occupation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Occupation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Occupation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Occupation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Occupation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
