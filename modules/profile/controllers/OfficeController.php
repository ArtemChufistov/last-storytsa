<?php

namespace app\modules\profile\controllers;

use app\components\UserController;
use app\modules\event\models\EventSearch;
use app\modules\finance\components\FinanceComponent;
use app\modules\finance\components\PaymentComponent;
use app\modules\finance\components\payment\FormComponent;
use app\modules\finance\models\CoinbaseTransaction;
use app\modules\finance\models\Currency;
use app\modules\finance\models\CurrencyCourse;
use app\modules\finance\models\forms\UsertransactionForm;
use app\modules\finance\models\Payment;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\search\PaymentFinanceSearch;
use app\modules\finance\models\search\TransactionFinanceSearch;
use app\modules\invest\models\search\DetailMarketingSearch;
use app\modules\finance\models\Transaction;
use app\modules\invest\models\UserInvest;
use app\modules\invest\models\InvestType;
use app\modules\invest\models\forms\CancelUserInvestForm;
use app\modules\mail\components\SendMailComponent;
use app\modules\mainpage\models\Preference;
use app\modules\matrix\components\BitcoinExpressMarketing;
use app\modules\matrix\components\StorytsaMarketing;
use app\modules\matrix\models\Matrix;
use app\modules\matrix\models\MatrixCategory;
use app\modules\matrix\models\MatrixQueue;
use app\modules\matrix\models\MatrixUserSetting;
use app\modules\matrix\models\MatrixUserSettingForm;
use app\modules\merchant\models\MAccount;
use app\modules\merchant\models\MAccountWallet;
use app\modules\merchant\models\MNodesTransactions;
use app\modules\merchant\models\MSendTxid;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\models\forms\ProfileForm;
use app\modules\profile\models\User;
use app\modules\response\models\Response;
use app\modules\support\models\Ticket;
use app\modules\support\models\TicketMessage;
use app\modules\support\models\TicketStatus;
use dosamigos\qrcode\formats\Bitcoin;
use dosamigos\qrcode\QrCode;
use lowbase\document\models\Document;
use lowbase\document\models\Like;
use lowbase\document\models\Visit;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\modules\news\models\NewsSearch;
use yii\web\ForbiddenHttpException;
use Yii;
use yii\helpers\ArrayHelper;

class OfficeController extends UserController {

	public $user;


	public function init() {
		$this->user = Yii::$app->user->identity;
		$this->view->params['user'] = Yii::$app->user;

		if (!empty($this->user)){
	        $userInfo = $this->user->initUserInfo();
	        $userInfo->lang = Yii::$app->language;
	        $userInfo->online_date = date('Y-m-d H:i:s');
	        $userInfo->save(false);
		}

		$this->layout = Yii::$app->getView()->theme->pathMap['@app/views'] . '/layouts/office';
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'foreColor' => '3373751', //синий
			],
		];
	}

	public function behaviors() {

		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['captcha', 'user', 'walletdetail', 'struct', 'matrix', 'invest', 'onuserinvest', 'finance', 'in', 'out', 'dashboard', 'promo', 'response', 'support', 'remove', 'showuser', 'cryptodeposit'],
				'rules' => [[
					'actions' => ['dashboard', 'user', 'remove', 'showuser', 'deleteresponse'],
					'allow' => true,
					'roles' => ['profileOffice'],
				], [
					'actions' => ['walletdetail'],
					'allow' => true,
					'roles' => ['walletdetailOffice'],
				], [
					'actions' => ['struct'],
					'allow' => true,
					'roles' => ['structOffice'],
				], [
					'actions' => ['matrix'],
					'allow' => true,
					'roles' => ['matrixOffice'],
				], [
					'actions' => ['finance', 'in', 'out', 'invest', 'onuserinvest', 'cryptodeposit'],
					'allow' => true,
					'roles' => ['financeOffice'],
				], [
					'actions' => ['promo'],
					'allow' => true,
					'roles' => ['promoOffice'],
				], [
					'actions' => ['response'],
					'allow' => true,
					'roles' => ['responseOffice'],
				], [
					'actions' => ['support'],
					'allow' => true,
					'roles' => ['supportOffice'],
				],
				],
				'denyCallback' => function () {
					if (Yii::$app->user->getIsGuest()){
						return Yii::$app->response->redirect(['login']);
					}else{
			        	return Yii::$app->response->redirect(['accessdenied']);
					}
			    },
			],
		];
	}

	// Профиль пользователя (личный кабинет)
	public function actionUser() {
		$oldUser = $this->user;
		$this->user = ProfileForm::findOne(Yii::$app->user->id);

		if ($this->user === null) {
			throw new NotFoundHttpException(Yii::t('app', 'Запрошенная страница не найдена.'));
		}

		if ($this->user->birthday) {
			$date = new \DateTime($this->user->birthday);
			$this->user->birthday = $date->format('d.m.Y');
		}

		if ($this->user->load(Yii::$app->request->post())) {

			$this->user->email = $oldUser->email;

		    if (isset($this->user->send_mail_confirm_token_submit)) {

		      $seconds = time() - strtotime($this->user->last_date_email_confirm_token);

		      if ($seconds < User::LAST_DATE_EMAIL_CONFIRM_TOKEN_EXPIRE) {
		        $this->user->addError('email', Yii::t('app', 'Повторная отправка письма возможна через {seconds} секунд', [
		          'seconds' => User::LAST_DATE_EMAIL_CONFIRM_TOKEN_EXPIRE - (int) $seconds,
		        ]));
		      } else {
		        $oldUser->generateEmailConfirmToken();
		        $oldUser->save(false);
		        SendMailComponent::unConfirmEmailCode($oldUser, $oldUser->email);
		        $this->user->addError('email_confirm_token', Yii::t('app', 'На указаную Вами почту отправлен код подтверждения, пожалуйста укажите его.'));
		      }
		    }

		    if (isset($this->user->confirm_token_submit)) {
		      if ($this->user->email_confirm_token != $oldUser->email_confirm_token) {
		        $this->user->addError('email_confirm_token', Yii::t('app', 'Неправильный код подтверждения'));
		      } else {
		        $this->user->email_confirm_token = '';
		        $this->user->last_date_email_confirm_token = '';
		        $this->user->updateAuthAssigment(AuthAssignment::ITEM_NAME_NOT_VERIFIED_USER);
		        $this->user->save(false);
		        $this->refresh();
		      }
		    }

		    if (isset($this->user->change_password_submit)) {
		    }

			if (empty($this->user->getErrors())) {
				// Получаем изображение, если оно есть
				$this->user->photo = UploadedFile::getInstance($this->user, 'photo');
				if ($this->user->save()) {
					Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Данные профиля обновлены.'));
					$this->refresh();
				}
			}
		}

		return $this->render('office/user', [
			'user' => $this->user,
			'oldUser' => $oldUser,
		]);
	}


    // сводная статистика
    public function actionDashboard() {

        return $this->render('office/dashboard', [
            'user' => $this->user,
        ]);
    }

	/**
	 * Пользовательское открытое отображение
	 * профиля пользователя
	 *
	 * @param $id - ID пользователя
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionShow($id) {
		$model = $this->findModel($id);

		if (method_exists($this->module, 'getCustomView')) {
			return $this->render($this->module->getCustomView('show', '@vendor/lowbase/yii2-user/views/user/show'), [
				'model' => $model,
			]);
		} else {
			return $this->render('@vendor/lowbase/yii2-user/views/user/show', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Фиксирование пользователя Онлайн
	 * Используется onlineWidget посредством
	 * Ajax-запросов
	 */
	public function actionOnline() {
		User::afterLogin((Yii::$app->request->post('id')));
	}

	// Удаление собственной аватарки
	public function actionRemove() {
		$model = ProfileForm::findOne(Yii::$app->user->id);
		if ($model !== null) {
			$model->removeImage(); // Удаление изображения
			Yii::$app->getSession()->setFlash('success', Yii::t('user', 'Изображение удалено.'));
		}

		return Yii::$app->getResponse()->redirect(['/profile/office/user']);
	}

	// Платёжные реквизиты
	public function actionWalletdetail() {
		$userWallets = [];

		$paySystemLk = PaySystem::find()->where(['key' => PaySystem::KEY_LK])->one();

		$currencyCourseArray = CurrencyCourse::find()->where(['from_pay_system_id' => $paySystemLk->id])->all();

		foreach ($currencyCourseArray as $currencyCourse) {
			$currency = $currencyCourse->getToCurrency()->one();
			$paySystem = $currencyCourse->getToPaySystem()->one();
			$classname = 'app\modules\finance\models\UserPayWallet' . $paySystem->key;
			$userWallet = $classname::find()->where(['user_id' => $this->user->id, 'pay_system_id' => $paySystem->id, 'currency_id' => $currency->id])->one();
			if (empty($userWallet)) {
				$userWallet = new $classname;
				$userWallet->user_id = $this->user->id;
				$userWallet->pay_system_id = $paySystem->id;
				$userWallet->currency_id = $currency->id;
			}
			$userWallets[] = $userWallet;

			if ($userWallet->load(Yii::$app->request->post())) {
				if ($userWallet->validate()) {
					$userWallet->save();
				}
			}
		}

		return $this->render('office/wallet-detail', [
			'user' => $this->user,
			'userWallets' => $userWallets,
		]);
	}

	// Структура
	public function actionStruct() {
		ini_set("memory_limit", "2000M");
		return $this->render('office/struct', [
			'user' => $this->user,
		]);
	}

	public function actionWorkstruct(){
		ini_set("memory_limit", "2000M");
		$prefMarketing = Preference::find()->where(['key' => Preference::KEY_INVEST_MARKETING])->one();

		if (!empty($prefMarketing)) {
			$marketingClassName = $prefMarketing->value;
		} else {
			$marketingClassName = 'Marketing';
		}
		$marketing = new $marketingClassName;
		$marketing->setUser($this->user);

		$userTree = $marketing->buildUserTree();

		return $this->render('office/work-struct', [
			'userTree' => $userTree,
			'user' => $this->user
		]);
	}

	// Статистика
	public function actionStat() {
		$eventModel = new EventSearch();
		$eventModel->user_id = $this->user->id;
		$eventProvider = $eventModel->search(Yii::$app->request->queryParams);

		return $this->render('office/stat', [
			'eventProvider' => $eventProvider,
			'eventModel' => $eventModel,
			'user' => $this->user,
		]);
	}

	// Структура - информация о пользователе (AJAX)
	public function actionShowuser() {
		if (Yii::$app->request->validateCsrfToken()) {
			$user = User::find()->where(['login' => Yii::$app->request->post('login')])->one();

			return $this->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/office/partial/_show-user', [
				'user' => $user,
			]);
		}

		echo Yii::$app->request->post('_csrf') . "\r\n" . Yii::$app->request->getCsrfToken();
	}

	// программа покупки места в матрице
	public function actionProgram($matrixCategory = 1, $matrix = '', $placeSlug = '', $parentPlaceSlug = '') {
		$matrixCategory = MatrixCategory::find()->where(['id' => $matrixCategory])->one();
		$rootMatrixes = $matrixCategory->getMatrixes()->all();

		$currency = Currency::find()->where(['key' => Currency::KEY_USD])->one();

		$matrixUserSetting = new MatrixUserSettingForm;

		// если - поставить галку
		if ($matrixUserSetting->load(Yii::$app->request->post()) && $matrixUserSetting->validate()) {

			$settingArray = MatrixUserSetting::find()->where([
				'user_id' => $matrixUserSetting->user_id, 'matrix_root_id' => $matrixUserSetting->matrix_root_id])->all();

			foreach ($settingArray as $setting) {
				$setting->active = MatrixUserSetting::ACTIVE_FALSE;
				$setting->save(false);
			}

			$setting = MatrixUserSetting::find()->where([
				'user_id' => $matrixUserSetting->user_id,
				'matrix_root_id' => $matrixUserSetting->matrix_root_id, 'matrix_id' => $matrixUserSetting->matrix_id, 'sort' => $matrixUserSetting->sort])->one();

			if (!empty($setting)) {
				$setting->active = $matrixUserSetting->active;
				$setting->save(false);
			} else {
				$matrixUserSetting->date_add = date('Y-m-d H:i:s');
				$matrixUserSetting->save();
			}
		}

		// если - приоберсти место в матрице
		if (Yii::$app->request->post('marketing-matrix')) {

			$marketingData = Yii::$app->request->post('marketing-matrix');
			try {

				foreach ($rootMatrixes as $rootMatrixItem) {
					if (!empty($marketingData['rootId']) && $rootMatrixItem->id == $marketingData['rootId']) {
						$rootMatrixMarketing = $rootMatrixItem;
					}
				}

				if (empty($rootMatrixMarketing)) {
					$rootMatrixMarketing = $rootMatrixes[0];
				}

				$prefMarketing = Preference::find()->where(['key' => Preference::KEY_MATRIX_MARKETING])->one();

				if (!empty($prefMarketing)) {
					$marketingClassName = $prefMarketing->value;
				} else {
					$marketingClassName = 'Marketing';
				}
				$marketing = new $marketingClassName;
				$marketing->set(['user' => $this->user, 'currency' => $currency, 'rootMatrix' => $rootMatrixMarketing]);

				$queue = $marketing->addToQueue();

				if ($marketing->handleQueue($queue)) {
					$queue->status = MatrixQueue::STATUS_FINISH;
					$queue->save();
				}

				Yii::$app->getSession()->setFlash('message', Yii::t('app', 'место успешно приобретено'));
			} catch (\Exception $e) {
				Yii::$app->getSession()->setFlash('message', $e->getMessage());
			}
		}

		if ($matrix == '') {
			$rootMatrix = $rootMatrixes[0];
		} else {
			$rootMatrix = Matrix::find()->where(['slug' => $matrix])->one();
		}

		if (empty($placeSlug)) {
			$currentPlace = Matrix::find()->where(['user_id' => $this->user->id, 'root_id' => $rootMatrix->id])->one();
		} else {
			$currentPlace = Matrix::find()->where(['slug' => $placeSlug])->one();
		}

		if (empty($parentPlaceSlug)) {
			$parentPlace = $currentPlace;
		} else {
			$parentPlace = Matrix::find()->where(['slug' => $parentPlaceSlug])->one();
		}

		if (!empty($currentPlace) && !empty($parentPlace)) {
			$currentLevel = $currentPlace->getLevelTo($parentPlace);
		} else {
			$currentLevel = 0;
		}

		$parentUser = User::find()->where(['id' => $this->user->parent_id])->one();

		return $this->render('office/program', [
			'countRootMatrix' => count($rootMatrixes),
			'matrixCategory' => $matrixCategory,
			'rootMatrixes' => $rootMatrixes,
			'currentPlace' => $currentPlace,
			'currentLevel' => $currentLevel,
			'parentPlace' => $parentPlace,
			'rootMatrix' => $rootMatrix,
			'parentUser' => $parentUser,
			'user' => $this->user,
		]);
	}

	// инвестиционные пакеты (покупка)
	public function actionInvest() {
		// если - приоберсти инвест. пакет
		$marketingData = Yii::$app->request->post('marketingData');

		if (!empty($marketingData)) {

			$prefMarketing = Preference::find()->where(['key' => Preference::KEY_INVEST_MARKETING])->one();
			if (!empty($prefMarketing)) {
				$marketingClassName = $prefMarketing->value;
			} else {
				$marketingClassName = 'Marketing';
			}

			try {
				$marketing = new $marketingClassName;
				$marketing->set(['user' => $this->user, 'marketingData' => $marketingData]);
				$marketing->createInvest();

				return Yii::$app->response->redirect(['office/invest']);
			} catch (\Exception $e) {
				Yii::$app->getSession()->setFlash('message', $e->getMessage());
			}
		}

		$userInvestArray = UserInvest::find()->where(['user_id' => $this->user->id])->all();

		return $this->render('office/invest', [
			'userInvestArray' => $userInvestArray,
			'user' => $this->user,
		]);
	}

	public function actionInvestdetail($investTypeId) {
		$investType = InvestType::find()->where(['id' => $investTypeId])->one();

		return $this->render('office/invest-detail', [
			'investType' => $investType,
			'user' => $this->user,
		]);
	}

	// инвестиционные пакеты (пользовательские) с возможностью отмены
	public function actionViewinvest() {

		$cancelUserInvestForm = new CancelUserInvestForm;

		$cancelUserInvestForm->user_id = $this->user->id;

		if ($cancelUserInvestForm->load(Yii::$app->request->post()) && $cancelUserInvestForm->validate()) {

				$prefMarketing = Preference::find()->where(['key' => Preference::KEY_INVEST_MARKETING])->one();

				if (!empty($prefMarketing)) {
					$marketingClassName = $prefMarketing->value;
				} else {
					$marketingClassName = 'Marketing';
				}

				$userInvest = UserInvest::find()->where(['id' => $cancelUserInvestForm->user_invest_type_id])->one();

				$marketing = $marketingClassName::cancelInvest($userInvest);

		}

		$userInvestArray = UserInvest::find()->where(['user_id' => $this->user->id])->orderBy(['date_add' => SORT_DESC])->all();

		return $this->render('office/view-invest', [
			'cancelUserInvestForm' => $cancelUserInvestForm,
			'userInvestArray' => $userInvestArray,
			'user' => $this->user,
		]);
	}

	// QR code
	public function actionQrcode($address, $amount = '') {
		if ($amount == '') {
			$bitcoin = new Bitcoin(['address' => $address]);
		} else {
			$bitcoin = new Bitcoin(['address' => $address, 'amount' => $amount]);
		}

		return QrCode::png($bitcoin->getText());
	}

	// детальная информация по маркетингу
	public function actionDetailmarketing() {
		$detailMarketing = new DetailMarketingSearch;

		$detailMarketing->to_user_id = $this->user->id;
		$detailProvider = $detailMarketing->search(Yii::$app->request->queryParams);
		$detailProvider->pagination->pageSize = 10;

		return $this->render('office/detail-marketing', [
			'user' => $this->user,
			'detailMarketing' => $detailMarketing,
			'detailProvider' => $detailProvider,
		]);
	}

	// Финансы
	public function actionFinance($paymentType = '') {

		// внутренний перевод
		$usertransactionForm = new UsertransactionForm;
		if ($usertransactionForm->load(Yii::$app->request->post())) {

			$usertransactionForm->from_login = $this->user->login;
			if ($usertransactionForm->validate()) {
				Transaction::makeInnerPay($usertransactionForm->from_user_id, $usertransactionForm->to_user_id, $usertransactionForm->sum, $usertransactionForm->currency_id);
				Yii::$app->getSession()->setFlash('innerTransactionSuccess', Yii::t('app', 'Внутренний перевод участнику {login} на сумму {sum} {currency} выполнен',[
					'login' => $usertransactionForm->getToUser()->one()->login,
					'sum' => $usertransactionForm->sum,
					'currency' => $usertransactionForm->getCurrency()->one()->title,
					]));
				return Yii::$app->response->redirect(['office/finance']);
			}
		}

		// список заявок на пополнение/снятие
		$payment = new PaymentFinanceSearch;

		$payment->from_user_id = $this->user->id;
		$paymentProvider = $payment->search(Yii::$app->request->queryParams);
		$paymentProvider->pagination->pageSize = 10;

		// список транзакций
		$transaction = new TransactionFinanceSearch;

		$transaction->from_user_id = $this->user->id;
		$transactionProvider = $transaction->search(Yii::$app->request->queryParams);
		$transactionProvider->pagination->pageSize = 10;

		$additionPaymentArray = Payment::find()->where(['<>', 'additional_info', ''])->andWhere(['from_user_id' => $this->user->id])->all();

		foreach ($this->user->getBalances() as $balance) {
			$userCurrencyArray[] = $balance->getCurrency()->one();
		}

		return $this->render('office/finance', [
			'additionPaymentArray' => $additionPaymentArray,
			'usertransactionForm' => $usertransactionForm,
			'transactionProvider' => $transactionProvider,
			'userCurrencyArray' => $userCurrencyArray,
			'paymentProvider' => $paymentProvider,
			'transaction' => $transaction,
			'payment' => $payment,
			'user' => $this->user,
		]);
	}

	// Пополнить ЛК
	public function actionIn() {
		// создание заявки на пополнение
		$paymentComp = new FormComponent;

		$paymentComp->set(['user' => $this->user, 'data' => Yii::$app->request->get()]);
		$paymentForm = $paymentComp->inPayment();

		// валюты для пополнения
		$currencyArray = FinanceComponent::getInLkCurrencies();

		// список заявок на пополнение/снятие
		$payment = new PaymentFinanceSearch;

		$payment->type = Payment::TYPE_IN_OFFICE;
		$payment->from_user_id = $this->user->id;
		$paymentProvider = $payment->search(Yii::$app->request->queryParams);
		$paymentProvider->pagination->pageSize = 10;

		// список транзакций
		$transaction = new TransactionFinanceSearch;
		$transaction->from_user_id = $this->user->id;
		$transactionProvider = $transaction->search(Yii::$app->request->queryParams);
		$transactionProvider->pagination->pageSize = 10;

		return $this->render('office/in', [
			'transactionProvider' => $transactionProvider,
			'paymentProvider' => $paymentProvider,
			'currencyArray' => $currencyArray,
			'transaction' => $transaction,
			'paymentForm' => $paymentForm,
			'payment' => $payment,
			'user' => $this->user,
		]);
	}

	// Снять с ЛК
	public function actionOut() {
		// создание заявки на снятие
		$paymentComp = new FormComponent;

		$paymentComp->set(['user' => $this->user, 'data' => Yii::$app->request->get()]);
		$paymentForm = $paymentComp->outPayment();

		// валюты для снятия
		$currencyArray = FinanceComponent::getOutLkCurrencies();

		// список заявок на пополнение/снятие
		$payment = new PaymentFinanceSearch;

		$payment->type = Payment::TYPE_OUT_OFFICE;
		$payment->from_user_id = $this->user->id;
		$paymentProvider = $payment->search(Yii::$app->request->queryParams);
		$paymentProvider->pagination->pageSize = 10;

		// список транзакций
		$transaction = new TransactionFinanceSearch;

		$transaction->from_user_id = $this->user->id;
		$transactionProvider = $transaction->search(Yii::$app->request->queryParams);
		$transactionProvider->pagination->pageSize = 10;

		return $this->render('office/out', [
			'transactionProvider' => $transactionProvider,
			'paymentProvider' => $paymentProvider,
			'currencyArray' => $currencyArray,
			'transaction' => $transaction,
			'paymentForm' => $paymentForm,
			'payment' => $payment,
			'user' => $this->user,
		]);
	}

	// Отзывы
	public function actionResponse() {
		$responseArray = $this->user->getResponses()->all();

		$response = new Response;

		if (!empty(Yii::$app->request->post()) && $response->load(Yii::$app->request->post())) {
			$response->photo1 = UploadedFile::getInstance($response, 'photo1');
			$response->photo2 = UploadedFile::getInstance($response, 'photo2');
			$response->photo3 = UploadedFile::getInstance($response, 'photo3');
			if (count($responseArray) < Response::COUNT_PUBLIC_RESP) {
				$response->user_id = $this->user->id;
				$response->save();

				$responseArray = $this->user->getResponses()->all();
				Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Администрация благодарит Вас за оставленный отзыв'));
			} else {
				Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Нельзя публиковать больше {count} отзывов', ['count' => Response::COUNT_PUBLIC_RESP]));
			}
		}

		return $this->render('office/response', [
			'user' => $this->user,
			'response' => $response,
			'responseArray' => $responseArray,
		]);
	}

	// Удалить отзывы
	public function actionDeleteresponse($id) {
		$response = Response::find()->where(['id' => $id])->one();

		if (empty($response) || $response->user_id != $this->user->id) {
			return;
		}

		$response->delete();
	}

	// Тех поддержка
	public function actionSupport($ticketId = 0) {
		$ticket = new Ticket;

		if (!empty(Yii::$app->request->post()) && $ticket->load(Yii::$app->request->post())) {
			if ($ticket->validate()) {
				$ticket->user_id = $this->user->id;
				$ticket->status_id = TicketStatus::find()->where(['name' => TicketStatus::NAME_NEW])->one()->id;
				$ticket->save();
			}
		}

		$ticketMessage = new TicketMessage;

		if ($ticketId != 0) {
			$currentTicket = Ticket::find()->where(['id' => $ticketId])->one();
			$currentTicket->setMessageIsRead($this->user->id);
			$ticketMessage->user_id = $this->user->id;
			$ticketMessage->ticket_id = $currentTicket->id;
			if ($ticketMessage->load(Yii::$app->request->post())) {
				$ticketMessage->save();
			}

			$ticketMessages = TicketMessage::find()->where(['ticket_id' => $ticketId])->orderBy('dateAdd')->all();

		} else {
			$currentTicket = [];
			$ticketMessages = [];
		}

		$ticketArray = $this->user->gettickets()->orderBy(['id' => SORT_DESC])->all();

		return $this->render('office/support', [
			'ticketMessages' => $ticketMessages,
			'ticketMessage' => $ticketMessage,
			'currentTicket' => $currentTicket,
			'ticketArray' => $ticketArray,
			'ticket' => $ticket,
			'user' => $this->user,
		]);
	}

	public function actionMerchant() {




		return $this->render('office/merchant', [
			]);
	}

	public function actionCoindetail($merchantId) {
		return $this->render('office/coindetail', [
				'merchantId' => $merchantId
			]);
	}

	// Детали платежа AJAX
	public function actionPaymentinfo($paymenthash) {
		$payment = Payment::find()->where(['hash' => $paymenthash])->one();
		if (empty($payment)) {
			return;
		}

		if (!in_array($this->user->id, [$payment->from_user_id, $payment->to_user_id])) {
			return;
		}

		$cbSystem = PaySystem::find()->where(['key' => PaySystem::KEY_COINBASE])->one();

		if (!empty($cbSystem) && ($payment->from_pay_system_id == $cbSystem->id || $payment->to_pay_system_id == $cbSystem->id)) {

			$coinbaseTransaction = CoinbaseTransaction::find()->where(['payment_id' => $payment->id])->one();

			return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/partial/payment/_coinbase-transaction',
					['payment' => $payment, 'user' => $this->user, 'coinbaseTransaction' => $coinbaseTransaction]);
		}

		$cmSystem = PaySystem::find()->where(['key' => PaySystem::KEY_CM])->one();

		if (!empty($cmSystem) && ($payment->from_pay_system_id == $cmSystem->id || $payment->to_pay_system_id == $cmSystem->id)) {

			$cmTransaction = CoinbaseTransaction::find()->where(['payment_id' => $payment->id])->one();

			return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/partial/payment/_cm-transaction',
					['payment' => $payment, 'user' => $this->user, 'cmTransaction' => $cmTransaction]);
		}
	}

	// страница с информацией по заявке
	public function actionShowpayment($paymenthash) {
		$payment = Payment::find()->where(['hash' => $paymenthash])->one();
		if (empty($payment)) {
			throw new NotFoundHttpException(Yii::t('document', 'Запрашиваемая страница не найдена.'));
		}

		if (!in_array($this->user->id, [$payment->from_user_id, $payment->to_user_id])) {
			throw new NotFoundHttpException(Yii::t('document', 'Запрашиваемая страница не найдена.'));
		}

		return $this->render('office/show-payment', [
			'payment' => $payment,
			'user' => $this->user,
		]);
	}

	// Время до закрытия платежа AJAX
	public function actionToclosepaymentdate($paymenthash) {
		$payment = Payment::find()->where(['hash' => $paymenthash])->one();
		if (empty($payment)) {
			return;
		}

		if (!in_array($this->user->id, [$payment->from_user_id, $payment->to_user_id])) {
			return;
		}

		if ($payment->status == Payment::STATUS_CANCEL){
			return;
		}

		$pendingPaymentTime = Preference::find()->where(['key' => Preference::KEY_PENDING_PAYMENT_TIME])->one();

		$time = (strtotime($payment->date_add) + $pendingPaymentTime->value) - time();

		$hours = $time / (60 * 60);

		echo date('H:i:s',$time);
	}


	// Криптодепозиторий
	public function actionCryptodeposit() {
		$currency = Currency::find()->where(['key' => Currency::KEY_DIO])->one();
		$transaction = new TransactionFinanceSearch;
		$transaction->currency_id = $currency->id;
		$transaction->from_user_id = $this->user->id;
		$transactionProvider = $transaction->search(Yii::$app->request->queryParams);
		$transactionProvider->pagination->pageSize = 10;

		return $this->render('office/cryptodeposit', [
            'user' => $this->user,
            'transaction' => $transaction,
            'transactionProvider' => $transactionProvider
		]);
	}

	// Новости
	public function actionNews() {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('office/news', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
		]);
	}

	// FAQ
	public function actionFaq() {
		return $this->render('office/faq');
	}

	// Рекламные материалы
	public function actionPromo() {
		return $this->render('office/promo', [
			'user' => $this->user,
		]);
	}

	// Документы
	public function actionDoc() {
		return $this->render('office/doc', [
			'user' => $this->user,
		]);
	}

	// просмотр документа
	public function actionDocument($alias = 'index') {
		// Отображаем только опубликованные документы
		$model = Document::find()->where(['alias' => $alias, 'status' => Document::STATUS_ACTIVE])->one();
		if ($model == null) {
			throw new NotFoundHttpException(Yii::t('document', 'Запрашиваемая страница не найдена.'));
		}
		Visit::check($model->id); // Фиксируем просмотр
		$views = Visit::getAll($model->id); // Считаем просмотры
		$likes = Like::getAll($model->id); // Считаем лайки
		// Если задан шаблон отображения, то отображаем согласно нему, иначе стандартное отображение статьи
		$template = (isset($model->template) && $model->template->path) ? $model->template->path : '/office/document';
		return $this->render($template, [
			'model' => $model,
			'views' => ($views) ? $views[0]->count : 0,
			'likes' => ($likes) ? $likes[0]->count : 0,
		]);
	}

	// плашка информации пользователя по логину
	public function actionUserinfo($login) {
		$user = User::find()->where(['login' => $login])->one();

		return $this->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/_user-info', [
			'user' => $user
		]);
	}

	// поиск по логину
    public function actionSearhbylogin() {
        $result = [];

        if (empty(Yii::$app->request->get('q'))){
            return $result;
        }

        $userArray = User::find()
        	->where(['like', 'login', Yii::$app->request->get('q')])
        	->orWhere(['like', 'first_name', Yii::$app->request->get('q')])
        	->orWhere(['like', 'last_name', Yii::$app->request->get('q')])
        	->all();

		return $this->renderPartial('@app/themes/' . Yii::$app->params['theme'] . '/office/partial/_search-by-login', [
			'userArray' => $userArray
		]);
    }

	public function actionTestip() {
		echo 'Ваш IP: ' . $_SERVER['REMOTE_ADDR'];
	}

	// тест метод
	public function actionTest() {
		return $this->render('office/test');
	}
}