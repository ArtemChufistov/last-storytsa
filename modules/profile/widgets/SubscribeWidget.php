<?php
namespace app\modules\profile\widgets;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Widget;
use app\modules\profile\models\forms\ChangeSubmitEmailForm;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\models\Subscriber;

class SubscribeWidget extends Widget{
	
	public $user;

	public $view = 'subscribe';

	public function init(){
	}
	
	public function run(){

		$subscriber = new Subscriber;

		if ($subscriber->load(\Yii::$app->request->post())) {
			if ($subscriber->validate()){
				$subscriber->save();
				\Yii::$app->getSession()->setFlash('succsessSubscribe', 'Вы успешно подписались на новости проекта!');
			}
		}

		return \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/partial/' . $this->view, [
			'subscriber' => $subscriber
		]);
	}
}
?>