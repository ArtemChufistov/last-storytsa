<?php
namespace app\modules\profile\widgets;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Widget;
use app\modules\profile\models\forms\InviteFriendsByEmailForm;
use app\modules\profile\models\forms\ChangeSubmitEmailForm;
use app\modules\mail\components\SendMailComponent;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\models\Subscriber;
use Yii;

class InviteFriendsByEmailWidget extends Widget{
	
	public $user;

	public $view = '/office/widgets/_invite-friend-by-email';

	public function init(){
	}
	
	public function run(){

		$inviteForm = new InviteFriendsByEmailForm;

		$inviteForm->user = $this->user;

		if ($inviteForm->load(\Yii::$app->request->post()) && $inviteForm->validate()) {
			foreach($inviteForm->mailArray as $email){
				// отправить приглашение
	            SendMailComponent::sendInviteByEMail($this->user, $email);
			}

			Yii::$app->getSession()->setFlash('successInviteByEmail', Yii::t('app', 'Приглашения отправлены'));
		}

		return Yii::$app->controller->renderPartial('@app/themes/' . Yii::$app->params['theme'] . $this->view, [
			'inviteForm' => $inviteForm
		]);
	}
}
?>