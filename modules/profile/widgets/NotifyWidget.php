<?php
namespace app\modules\profile\widgets;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Widget;
use app\modules\profile\models\forms\ChangeSubmitEmailForm;
use app\modules\profile\models\AuthAssignment;

class NotifyWidget extends Widget{
	
	public $user;

	public function init(){
	}
	
	public function run(){

		return \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/widgets/_notify', [
			'changeEmailForm' => $changeEmailForm, 
			'user' => $this->user
		]);
	}
}
?>