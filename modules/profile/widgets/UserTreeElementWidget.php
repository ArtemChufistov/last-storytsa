<?php
namespace app\modules\profile\widgets;

use app\modules\profile\models\forms\ChangeSubmitEmailForm;
use app\modules\matrix\models\MatrixUserSettingForm;
use app\modules\matrix\models\MatrixUserSetting;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\models\Subscriber;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Widget;

class UserTreeElementWidget extends Widget{
	
	public $ancestorPlace; // место потомка
	public $matrixCategory; // категория матриц
	public $currentPlace; // текущее место
	public $currentUser; // пользователь ЛК
	public $parentPlace; // верхнее место (от которого идет просмотр)
	public $rootMatrix; // выбранная матрица
	public $startPlace;
	public $sort;  // сортировка

	public function init(){
	}
	
	public function run(){

		if (!empty($this->currentPlace)){
			$user = $this->currentPlace->getUser()->one();
		}else{
			$user = '';
		}

		$matrixUserSetting = MatrixUserSettingForm::find()->where([
			'matrix_root_id' => $this->rootMatrix->id,
			'matrix_id' => empty($this->ancestorPlace) ? $this->rootMatrix->id : $this->ancestorPlace->id, 
			'user_id' => $this->currentUser->id,
			'active' => MatrixUserSetting::ACTIVE_TRUE,
			'sort' => $this->sort,
			])->one();

		if (empty($matrixUserSetting)){
			$matrixUserSetting = new MatrixUserSettingForm;
			$matrixUserSetting->matrix_root_id = $this->rootMatrix->id;
			$matrixUserSetting->matrix_id = empty($this->ancestorPlace) ? $this->rootMatrix->id : $this->ancestorPlace->id;
			$matrixUserSetting->user_id = $this->currentUser->id;
			$matrixUserSetting->sort = $this->sort;
		}

		$matrixUserSetting->user_login = $this->currentUser->login;
		$matrixUserSetting->matrix_slug = empty($this->ancestorPlace) ? $this->rootMatrix->slug : $this->ancestorPlace->slug;
		$matrixUserSetting->matrix_root_slug = $this->rootMatrix->slug;

		return \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/widgets/_user-tree-element', [
			'matrixUserSetting' => $matrixUserSetting,
			'matrixCategory' => $this->matrixCategory,
			'ancestorPlace' => $this->ancestorPlace,
			'currentPlace' => $this->currentPlace,
			'currentUser' => $this->currentUser,
			'parentPlace' => $this->parentPlace,
			'rootMatrix' => $this->rootMatrix,
			'startPlace' => $this->startPlace,
			'user' => $user,
		]);
	}
}
?>