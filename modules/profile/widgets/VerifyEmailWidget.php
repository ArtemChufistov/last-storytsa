<?php
namespace app\modules\profile\widgets;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Widget;
use app\modules\profile\models\forms\ChangeSubmitEmailForm;
use app\modules\profile\models\AuthAssignment;

class VerifyEmailWidget extends Widget{
	
	public $user;

	public function init(){
	}
	
	public function run(){

		$changeEmailForm = new ChangeSubmitEmailForm;

		if ($changeEmailForm->load(\Yii::$app->request->post())){
			$changeEmailForm->user = $this->user;
			if ($changeEmailForm->validate()) {
				header("Refresh:3");
				return \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/widgets/_verify-success-modal');
			}
		}

		if ($this->user->getAuthAssignments()->one()->item_name == AuthAssignment::ITEM_NAME_NOT_VERIFIED_USER){
			return \Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] .'/office/widgets/_verify-user-modal', [
				'changeEmailForm' => $changeEmailForm, 
				'user' => $this->user
			]);
		}
	}
}
?>