<?php
namespace app\modules\profile\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\authclient\ClientInterface;
use yii\base\InvalidConfigException;
use app\modules\profile\components\AuthChoice;


class AuthChoiceBitmoda extends AuthChoice
{
    public function clientLink($client, $text = null, array $htmlOptions = [])
    {
        echo Html::beginTag('span', ['class' => $this->clientCssClass]);

        if (!array_key_exists('class', $htmlOptions)) {
            $htmlOptions['class'] = 'hero-btn default';
        }

        $viewOptions = $client->getViewOptions();
        if (empty($viewOptions['widget'])) {
            if ($this->popupMode) {
                if (isset($viewOptions['popupWidth'])) {
                    $htmlOptions['data-popup-width'] = $viewOptions['popupWidth'];
                }
                if (isset($viewOptions['popupHeight'])) {
                    $htmlOptions['data-popup-height'] = $viewOptions['popupHeight'];
                }
            }
            echo Html::a(Yii::t('app', 'ВХОД G+'), $this->createClientUrl($client), $htmlOptions);
        } else {
            $widgetConfig = $viewOptions['widget'];
            if (!isset($widgetConfig['class'])) {
                throw new InvalidConfigException('Widget config "class" parameter is missing');
            }
            /* @var $widgetClass Widget */
            $widgetClass = $widgetConfig['class'];
            if (!(is_subclass_of($widgetClass, AuthChoiceItem::className()))) {
                throw new InvalidConfigException('Item widget class must be subclass of "' . AuthChoiceItem::className() . '"');
            }
            unset($widgetConfig['class']);
            $widgetConfig['client'] = $client;
            $widgetConfig['authChoice'] = $this;
            echo $widgetClass::widget($widgetConfig);
        }
        echo Html::endTag('span');
    }

    /**
     * Меняем ul на div
     * @throws InvalidConfigException
     */
    protected function renderMainContent()
    {
        foreach ($this->getClients() as $externalService) {
            $this->clientLink($externalService);
        }
    }
  
}