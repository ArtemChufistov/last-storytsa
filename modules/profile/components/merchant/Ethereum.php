<?php

namespace app\modules\profile\components\merchant;

use app\modules\profile\components\merchant\clients\RpcClient;
use yii\base\Component;
use yii\httpclient\Client;

class Ethereum extends Component
{
    const ETHER_DIGITS = 1000000000000000000;

    /**
     * @var RpcClient
     */
    private $ethereumClient;

    public $host;

    public $port;

    public $gas = 21000;

    public $gasPrice = 21000000000;


    public function init()
    {
        parent::init();
        $this->ethereumClient = new RpcClient(new Client(['baseUrl' => 'http://'.$this->host.':'.$this->port]));
    }

    public function getClient()
    {

//    This call was removed in version 0.16.0. Use the appropriate fields from:
//    - getblockchaininfo: blocks, difficulty, chain
//    - getnetworkinfo: version, protocolversion, timeoffset, connections, proxy, relayfee, warnings
//    - getwalletinfo: balance, keypoololdest, keypoolsize, paytxfee, unlocked_until, walletversion
//
//    bitcoin-cli has the option -getinfo to collect and format these in the old format.


        return $this->ethereumClient;


    }

    public function generateAddress($password = '')
    {
        $data = $this->ethereumClient->call('personal_newAccount', [$password]);

        return $data['result'];
    }

    public function send(/*$fromAddress, */$toAddress, $amount, $feePaySender = true)
    {
        $fromAddress = '0x8057daaee5b7fd00be4c9a93cd41c17601dc09e0';

        $value = $amount * self::ETHER_DIGITS;
        $tranFee = $this->gasPrice * $this->gas;

        if (!$feePaySender) {
            $value -= $tranFee;
        }

        $data = $this->ethereumClient->call('personal_unlockAccount', [$fromAddress, '', 0]);
        if (isset($data['error'])) {
            throw new \Exception('personal_unlockAccount error raised: '.$data['error']['message']);
        }

        $params = [
            'from' => $fromAddress,
            'to' => $toAddress,
       //     'gas' => '0x'.dechex($this->gas),
//            'gasPrice' => '0x'.dechex($this->gasPrice),
            'value' => '0x'.dechex($value),
        ];




//        $data = $this->ethereumClient->call('eth_sendTransaction', [$params]);
        $data = $this->ethereumClient->call('eth_estimateGas', [$params]);
        if (isset($data['error'])) {
            throw new \Exception('eth_sendTransaction error raised: '.$data['error']['message']);
        }

        echo "<pre>";
        print_r(hexdec($data['result']));
        print_r($params);
        die;

        // return txid
        return $data['result'];
    }

    public function getInfo()
    {
        $info['balance'] = 0;
        $info['accounts'] = [];
        $data = $this->ethereumClient->call('eth_accounts', []);
        $addresses = $data['result'];

        foreach ($addresses as $address) {
            $data = $this->ethereumClient->call('eth_getBalance', [$address, 'latest']);
            $info['accounts'][$address] = floatval(hexdec($data['result'])) / self::ETHER_DIGITS;
            $info['balance'] += $info['accounts'][$address];
        }

        return $info;
    }

    public function showAddresses()
    {
        $data = $this->ethereumClient->call('eth_accounts', []);
        $addresses = $data['result'];

        return $addresses;
    }

}
