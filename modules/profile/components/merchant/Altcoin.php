<?php

namespace app\modules\profile\components\merchant;

use app\modules\profile\components\merchant\clients\EasyBitcoin;
use ErrorException;
use yii\base\Component;

class Altcoin extends Component
{
    /**
     * @var EasyBitcoin
     */
    protected $altcoinClient;

    public $username;

    public $password;

    public $host;

    public $port;

    public function init()
    {
        parent::init();
        $this->altcoinClient = new EasyBitcoin($this->username, $this->password, $this->host, $this->port);
    }

    public function getClient()
    {

//    This call was removed in version 0.16.0. Use the appropriate fields from:
//    - getblockchaininfo: blocks, difficulty, chain
//    - getnetworkinfo: version, protocolversion, timeoffset, connections, proxy, relayfee, warnings
//    - getwalletinfo: balance, keypoololdest, keypoolsize, paytxfee, unlocked_until, walletversion
//
//    bitcoin-cli has the option -getinfo to collect and format these in the old format.


        return $this->altcoinClient;
    }

    /**
     * Создать новый адрес
     *
     * @param string $accountName
     * @return mixed
     * @throws ErrorException
     */
    public function generateAddress($accountName = '')
    {
        if (!$accountName) {
            $accountName = $this->username;
        }

        $address = $this->altcoinClient->getnewaddress((string)$accountName);
        if ($this->altcoinClient->error == "") {
            return $address;
        } else {
            throw new ErrorException('getnewaddress error: ' . $this->altcoinClient->error);
        }
    }

    /**
     * Функция перевода для $this->username
     *
     * @param $toAddress
     * @param $amount
     * @param string $comment
     * @param string $commentTo
     * @param bool $subtractFeeFromAmount
     * @return string
     * @throws ErrorException
     */
    public function send($toAddress, $amount, $comment = '', $commentTo = '', $subtractFeeFromAmount = false)
    {
      //  $txid = $this->altcoinClient->sendtoaddress($toAddress, $amount, $comment, $commentTo, $subtractFeeFromAmount);

        $txid = 'Ошибка';

        if ($this->altcoinClient->error == "") {
            return $txid;
        } else {
            throw new ErrorException('sendtoaddress error: ' . $this->altcoinClient->error);
        }
    }

    /**
     * Функция перевода с учетом аккаунта отправителя
     *
     * @param $fromAccount
     * @param $toAddress
     * @param $amount - сумма
     * @param int $confirmations_num - число подтвержденй
     * @param string $comment - для внутреннего использования
     * @param string $commentTo - коммент получателю
     * @return string - txid
     * @throws ErrorException если нет ответа
     */
    public function sendFrom($fromAccount, $toAddress, $amount, $confirmations_num = 6, $comment = '', $commentTo = '')
    {
        $txid = $this->altcoinClient->sendfrom($fromAccount, $toAddress, $amount, $confirmations_num, $comment, $commentTo);

        if ($this->altcoinClient->error == "") {
            return $txid;
        } else {
            throw new ErrorException('sendfrom error: ' . $this->altcoinClient->error);
        }
    }

    public function getInfo()
    {

//    This call was removed in version 0.16.0. Use the appropriate fields from:
//    - getblockchaininfo: blocks, difficulty, chain
//    - getnetworkinfo: version, protocolversion, timeoffset, connections, proxy, relayfee, warnings
//    - getwalletinfo: balance, keypoololdest, keypoolsize, paytxfee, unlocked_until, walletversion
//
//    bitcoin-cli has the option -getinfo to collect and format these in the old format.


        $info = $this->altcoinClient->getwalletinfo();

        if ($this->altcoinClient->error == "") {
            return $info;
        } else {
            throw new ErrorException('getinfo error: ' . $this->altcoinClient->error);
        }
    }

    /**
     * Баланс аккаунта по всем кошелькам
     *
     * @param $account
     * @param int $confirmations_num
     * @return mixed
     * @throws ErrorException
     */
    public function getAccountBalance($account, $confirmations_num = 1)
    {
        $info = $this->altcoinClient->getbalance((string)$account, (int)$confirmations_num, true);

        if ($this->altcoinClient->error == "") {
            return $info;
        } else {
            throw new ErrorException('getbalance error: ' . $this->altcoinClient->error);
        }
    }


    public function getAccountAddressBalance($address, $confirmations_num = 1)
    {
        $info = $this->altcoinClient->getreceivedbyaddress((string)$address, (int)$confirmations_num);

        if ($this->altcoinClient->error == "") {
            return $info;
        } else {
            throw new ErrorException('getreceivedbyaddress error: ' . $this->altcoinClient->error);
        }
    }

    /**
     * Список транзакций
     *
     * @param $account
     * @param int $skip
     * @param int $count
     * @param bool $watchOnly
     * @return mixed
     * @throws ErrorException
     */
    public function getTransactionsList($account, $count = 1000,  $skip = 0, $watchOnly = true)
    {
        $trans = $this->altcoinClient->listtransactions((string)$account, (int)$count, (int)$skip, (boolean)$watchOnly);

        if ($this->altcoinClient->error == "") {
            return $trans;
        } else {
            throw new ErrorException('listtransactions error: ' . $this->altcoinClient->error);
        }
    }

    public function getTransactionsListByAddress($account, $address)
    {
        $transactions = $this->getTransactionsList($account);

        $clearByAddress = [];
        foreach ($transactions as $transaction) {
            if ($transaction['address'] == $address) {
                $clearByAddress[] = $transaction;
            }
        }

        return $clearByAddress;
    }

    /**
     * Подробная информация по транзакции
     *
     * @param $txid
     * @return mixed
     * @throws ErrorException
     */
    public function getTransaction($txid)
    {
         $trans = $this->altcoinClient->gettransaction((string)$txid);

        if ($this->altcoinClient->error == "") {
            return $trans;
        } else {
            throw new ErrorException('gettransaction error: ' . $this->altcoinClient->error);
        }
    }

    /**
     * Список адресов
     *
     * @param $account string
     * @return mixed
     * @throws ErrorException
     */
    public function showAddresses($account)
    {
        //todo решить момент
        if (!$account) {
            $account = $this->username;
        }

        $datas = $this->altcoinClient->getaddressesbyaccount((string)$account);

        if ($this->altcoinClient->error == "") {
            return $datas;
        } else {
            throw new ErrorException('getaddressesbyaccount error: ' . $this->altcoinClient->error);
        }
    }

}
