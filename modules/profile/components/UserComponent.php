<?php

namespace app\modules\profile\components;

use app\modules\profile\models\User;

class UserComponent {
	public static function syncParents()
	{
		foreach(User::find()->all() as $user){
			$parentUser = $user->parent()->one();

			if (!empty($parentUser)){
				$user->parent_id = $parentUser->id;
				$user->save(false);
			}
		}
	}
}