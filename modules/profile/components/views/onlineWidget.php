<?php

use yii\helpers\Url;

$this->registerJs(
    'setInterval(function(){
        $.ajax({
            url: "' . Url::to(['user/user/online']) . '",
            type:"POST",
            data:{id: "'.\Yii::$app->user->id.'"},
            });
    },'.$time.');'
);
