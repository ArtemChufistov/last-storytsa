<?php

namespace app\modules\profile\components\oauth;

use app\modules\profile\models\User;
use app\modules\profile\models\UserOauthKey;

/**
 * Авторизация через Facebook
 * Class Facebook
 * @package lowbase\user\components\oauth
 */
class Facebook extends \yii\authclient\clients\Facebook
{
    /**
     * Размеры Popap-окна
     * @return array
     */
    public function getViewOptions()
    {
        return [
            'popupWidth' => 900,
            'popupHeight' => 600
        ];
    }

    /**
     * Преобразование пола
     * @return array
     */
    public function normalizeSex()
    {
        return [
            'male' => User::SEX_MALE,
            'female' => User::SEX_FEMALE
        ];
    }

    /**
     * Получение аттрибутов
     * @return array
     * @throws \yii\base\Exception
     */
    protected function initUserAttributes()
    {
        $attributes = $this->api('me', 'GET', [
            'fields' => implode(',', [
                'id',
                'email',
                'first_name',
                'last_name',
                'picture.height(200).width(200)',
                'gender'
            ]),
        ]);

        $return_attributes = [
            'User' => [
                'email' => $attributes['email'],
                'first_name' => $attributes['first_name'],
                'last_name' => $attributes['last_name'],
                'photo' => $attributes['picture']['data']['url'],
                'sex' => $this->normalizeSex()[$attributes['gender']]
            ],
            'provider_user_id' => $attributes['id'],
            'provider_id' => UserOauthKey::getAvailableClients()['facebook'],
            'page' => $attributes['id'],
        ];

        return $return_attributes;
    }
}
