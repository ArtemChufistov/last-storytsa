<?php

namespace app\modules\profile\components\oauth;

use app\modules\profile\models\User;
use app\modules\profile\models\UserOauthKey;

/**
 * Авторизация с помощью Twitter
 * Class Twitter
 * @package lowbase\user\components\oauth\
 */
class Twitter extends \yii\authclient\clients\Twitter
{
    /**
     * Размеры Popap-окна
     * @return array
     */
    public function getViewOptions()
    {
        return [
            'popupWidth' => 900,
            'popupHeight' => 500
        ];
    }

    /**
     * Получение аттрибутов
     * @return array
     * @throws \yii\base\Exception
     */
    protected function initUserAttributes()
    {
        $attributes = $this->api('account/verify_credentials.json', 'GET');

        $return_attributes = [
            'User' => [
                'email' => null,
                'first_name' => $attributes['name'],
                'photo' => '',
                'sex' => User::SEX_MALE,
            ],
            'provider_user_id' => $attributes['id'],
            'provider_id' => UserOauthKey::getAvailableClients()['twitter'],
            'page' => $attributes['screen_name'],
        ];

        return $return_attributes;
    }
}
