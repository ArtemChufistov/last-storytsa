<?php

namespace app\modules\profile\components;

use Yii;
use yii\helpers\Html;
use app\modules\profile\models\Promo;

class PromoComponent
{
    public function run()
    {
        $promoArray = Promo::find()->all();

        foreach ($promoArray as $promo) {
                $handler = $promo->getHandler();
                $handler::handle($promo);
        }
    }
}