<?php

namespace app\modules\profile\components;

use app\modules\profile\models\forms\LoginForm;
use yii\base\Widget;

/**
 * Виджет авторизации (входа на сайт)
 * 
 * Class LoginWidget
 * @package app\modules\user\components
 */
class LoginWidget extends Widget
{
    public $oauth = true;   // Авторизация с помощью соц. сетей

    public function run()
    {
        $model = new LoginForm();
        return $this->render('loginWidget', [
            'model' => $model,
            'oauth' => $this->oauth
        ]);
    }
}
