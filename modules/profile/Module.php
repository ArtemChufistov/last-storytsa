<?php

namespace app\modules\profile;

use yii\base\BootstrapInterface;

/**
 * profile module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\profile\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }

    //Массив с собственными отображениями
    public $customViews = [];
    //Массив с собственными шаблонами писем
    public $customMailViews = [];
    //Action для капчи
    public $captchaAction = '/user/default/captcha';

    public $userPhotoPath = 'attach/user/images';

    public $photoPath1 = 'attach/response/images1';
    public $photoPath2 = 'attach/response/images2';
    public $photoPath3 = 'attach/response/images3';

    /**
     * Собственные отображения
     * Допустимые параметры:
     *
     * signup - регистрация
     * login - авторизация
     * profile - профиль
     * repass - восстановление пароля
     * show - просмотр пользователя
     *
     * @param $customView - отображение
     * @param $default - отображение по умолчанию
     * @return mixed
     */
    public function getCustomView($customView, $default)
    {
        if (isset($this->customViews[$customView])) {
            return $this->customViews[$customView];
        } else {
            return $default;
        }
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([

            [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/investdetail/<investTypeId>',
                'route' => 'profile/office/investdetail',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/<action>',
                'route' => 'profile/office/<action>'
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/coindetail/<merchantId>',
                'route' => 'profile/office/coindetail'
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'submitemail/<login>/<hash>',
                'route' => 'profile/profile/submitemail',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/deleteresponse/<id>',
                'route' => 'profile/office/deleteresponse',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/support/<ticketId>',
                'route' => 'profile/office/support',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/matrix/<slug>',
                'route' => 'profile/office/matrix',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/matrix/<slug>',
                'route' => 'profile/office/matrix',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/showpayment/<paymenthash>',
                'route' => 'profile/office/showpayment',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/paymentinfo/<paymenthash>',
                'route' => 'profile/office/paymentinfo',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/toclosepaymentdate/<paymenthash>',
                'route' => 'profile/office/toclosepaymentdate',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/matrix/<slug>/<childSlug>',
                'route' => 'profile/office/matrix',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/<action>',
                'route' => 'profile/office/<action>'
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/coindetail/<merchantId>',
                'route' => 'profile/office/coindetail'
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'submitemail/<login>/<hash>',
                'route' => 'profile/profile/submitemail',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/deleteresponse/<id>',
                'route' => 'profile/office/deleteresponse',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/support/<ticketId>',
                'route' => 'profile/office/support',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/matrix/<slug>',
                'route' => 'profile/office/matrix',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/matrix/<slug>',
                'route' => 'profile/office/matrix',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/showpayment/<paymenthash>',
                'route' => 'profile/office/showpayment',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/paymentinfo/<paymenthash>',
                'route' => 'profile/office/paymentinfo',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/toclosepaymentdate/<paymenthash>',
                'route' => 'profile/office/toclosepaymentdate',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/matrix/<slug>/<childSlug>',
                'route' => 'profile/office/matrix',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/qrcode/<address>/<amount>',
                'route' => 'profile/office/qrcode',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'profile/captcha',
                'route' => 'profile/profile/captcha',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'office/userinfo/<login>',
                'route' => 'profile/office/userinfo',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => '<action:(login|login-verification|logout|signup|confirm|reset|profile|remove|online|resetpasswd)>',
                'route' => 'profile/profile/<action>',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'auth/<authclient:[\w\-]+>',
                'route' => 'profile/auth/index',
            ]
        ], false);
    }
}
