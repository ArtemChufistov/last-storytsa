<?php

namespace app\modules\profile\promo;

use Yii;
use yii\helpers\Html;
use app\modules\profile\models\Promo;
use app\modules\profile\models\UserPromo;
use app\modules\profile\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Currency;
use app\modules\invest\models\UserInvest;
use app\modules\matrix\models\Matrix;
use app\modules\mainpage\models\Preference;


/* 
Suhba - админские 2%
*/
class SuhbaAdmin
{
    public function handle($promo)
    {
		$startTime = '2017-12-23 00:00:00';

    	$userLogins = [
    		'Frozzen' => 2,
    	];

    	$currency = Currency::find()->where(['key' => Currency::KEY_RUR])->one();

    	$prefLastInvestId = Preference::find()->where(['key' => Preference::KEY_ADMIN_LAST_INVEST_ID])->one();

		$userInvestArray = UserInvest::find()->where(['>=', 'date_add', $startTime])->andWhere(['>', 'id', $prefLastInvestId->value])->all();

    	foreach ($userInvestArray as $userInvest){
    		foreach($userLogins as $userLogin => $userPercent){

    			$userSum = ($userInvest->sum / 100) * $userPercent;
    			$currentPayDate = $userInvest->date_add;

    			$user = User::find()->where(['login' => $userLogin])->one();

    			echo 'Админские логину: ' . $userLogin . ' сумма: ' . $userSum . "\r\n";

				$transaction = new Transaction;
				$transaction->sum = $userSum;
				$transaction->type = Transaction::TYPE_ADMIN_BONUS;
				$transaction->to_user_id = $user->id;
				$transaction->currency_id = $currency->id;
		        $transaction->date_add = $currentPayDate;
		        $transaction->save(false);

		        $balance = $user->getBalances([$currency->id])[$currency->id];

				$balance->recalculateValue();
    		}

    		$prefLastInvestId->value = $userInvest->id;
    		$prefLastInvestId->save(false);
    	}
    }
}