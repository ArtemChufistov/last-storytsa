<?php

namespace app\modules\profile\promo;

use Yii;
use yii\helpers\Html;
use app\modules\profile\models\UserInfo;
use app\modules\profile\models\Promo;
use app\modules\profile\models\UserPromo;
use app\modules\profile\models\User;
use app\modules\invest\models\InvestPref;
use app\modules\invest\models\UserInvest;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Currency;
use app\modules\matrix\models\Matrix;
use yii\helpers\ArrayHelper;
use app\modules\invest\components\marketing\SuhbaMarketing;

/* 
Suhba — доделать Начисление бонуса.
Механизм расчета.Относительно каждого участника система делает расчет его группового оборота и оборота в отдельных ветках. 
Система сверяет  соответствие условиям – оборот общий, оборот большой ноги вычитается из общего оборота.
Если он менее 50% (или сумма малых веток более 50%), то система начисляет соответствующее оборотам количество долей.Система рассчитывает общее количество долей, приходящихся на всех участников, выполнивших условие маркетинга и делит 5% месячного оборота на общее количество долей.   Далее, каждый выполнивший участник получает начисление равное произведению количество его долей на сумму, эквивалентную одной доли. Бонус начисляется ежемесячно.
Из струткрного оборота вычитаем оборот больше ветки - полученое значение центральный столбей - количество долей

 Более 10 000 000
1 000 000 - 1
2 000 000 - 2
3 000 000 - 3
4 000 000 - 4
5 000 000 - 5
6 000 000 - 6
7 000 000 - 7
8 000 000 - 8
9 000 000 - 9
10 000 000 - 10
*/
class SuhbaMathcingBonusPromo
{

	public static function getCountPartsForSum($struct_invest, $big_leg_invest, $userDataItem, $dateFrom, $dateTo)
	{
		$parts = [
			0 => 0,
			1 => 2000000,
			2 => 4000000,
			3 => 6000000,
			4 => 8000000,
			5 => 10000000,
			6 => 12000000,
			7 => 14000000,
			8 => 16000000,
			9 => 18000000,
			10 => 20000000,
		];

		if ($userDataItem['self_invest'] < 50000){
			return 0;
		}

		$other_leg_invest = $struct_invest - $big_leg_invest;

		$countParts = 0;
		foreach ($parts as $numPart => $part) {
			if ($other_leg_invest > $part && $big_leg_invest > $part){
				$countParts =  $numPart;
			}
		}

		if ($userDataItem['login'] == 'A112'){
			echo $userDataItem['login'] . ' долей:' . $countParts .  ' структурный: ' . $struct_invest . ' большая нога: ' . $big_leg_invest . "\r\n";
		}


		return $countParts;

	}


    public function handle($promo = '')
    {
    	return;
    	$time = strtotime('2018-01-04 13:59:59');
        //$time = time();

    	$percent = 5; // процент, отчисляемый на матчинг бонус

        $createTransactionPeriod = SuhbaMarketing::getTransactionPeriod();

        $periodArray = SuhbaMarketing::getPeriodArray();

    	$currencyRUR = Currency::find()->where(['key' => Currency::KEY_RUR])->one();

        foreach($periodArray as $periodItem){
            if ($time >= strtotime($periodItem['dateFrom']) && $time <= strtotime($periodItem['dateTo'])){
                $dateFrom = $periodItem['dateFrom'];
                $dateTo = $periodItem['dateTo'];
                break;
            }
        }

        if (empty($dateFrom) || empty($dateTo)){
            return;
        }

    	// считаем всего инвестиций
    	$countInvest = UserInvest::find()
    		->select('SUM(sum) AS sum')
    		->where(['>=', 'date_add', $dateFrom])
    		->andWhere(['<', 'date_add', $dateTo])
    		->one();

    	if (empty($countInvest->sum)){
    		$countInvest->sum = 0;
    	}

    	// сумма - отдаваемая на промо доли
    	$percentSum = (($countInvest->sum ) / 100 ) * $percent;

	    // считаем структурный оборот, большей ноги, личные инвестиции, доли, за период
	    $userData = self::calcAllStructUserInvest($dateFrom, $dateTo);

	    // количество долей
	    $countParts = self::calcSumCountParts($userData);

	    if ($countParts == 0){
	    	echo 'От: ' . $dateFrom . ' до: ' . $dateTo . ' 0 долей' . "\r\n";
	    	return;
	    }

	    // цена доли
	    $costPart = $percentSum / $countParts;

		echo 'От: ' . $dateFrom . ' до: ' . $dateTo . ' инвестировано на сумму: ' . 
			number_format($countInvest->sum, 2, '.', ' ') . ' на промо бонус: ' . number_format($percentSum, 2, '.', ' ') .
			' долей: ' . $countParts . ' цена доли: ' . $costPart . "\r\n";

		Yii::$app->db->createCommand()->update('user_info', ['count_parts' => 0])->execute();

        // Если настало время начислений
        if ($time >= (strtotime($dateTo) - $createTransactionPeriod)){
		    foreach($userData as $userItem){
		    	if ($userItem['count_parts'] > 0){
		    		$sum = $userItem['count_parts'] * $costPart;

		    		$transaction = Transaction::find()
		    			->select('SUM(sum) as sum')
		    			->where(['to_user_id' => $userItem['id']])
		    			->andWhere(['type' => Transaction::TYPE_PROMO1])
		    			->andWhere(['date_add' => date('Y-m-d H:i:s', strtotime($dateTo))])
		    			->one();

		    		$realSum = $sum;
		    		if (!empty($transaction) && $transaction->sum > 0 && $transaction->sum < $sum){
		    			$realSum = $sum - $transaction->sum;
		    			echo 'Для логина: ' . $userItem['login'] . ' недоначислено: ' . $realSum . ' за дату: ' . date('Y-m-d H:i:s', strtotime($dateTo)) . "\r\n";
		    		}elseif (!empty($transaction) && $transaction->sum == $sum){
		    			continue;
		    		}

		    		echo 'Для логина: ' . $userItem['login'] . ' начисляем: ' . $userItem['count_parts'] . ' долей, по цене: ' . $costPart . "\r\n";

		    		$transaction = new Transaction;
		    		$transaction->sum = $realSum;
		    		$transaction->currency_id = $currencyRUR->id;
		    		$transaction->to_user_id = $userItem['id'];
		    		$transaction->type = Transaction::TYPE_PROMO1;
		    		$transaction->data = $userItem['count_parts'];
		    		$transaction->date_add =  date('Y-m-d H:i:s', strtotime($dateTo));

		    		$transaction->save(false);
		    	}
		    }
		}else{
			foreach($userData as $userItem){
		    	if ($userItem['count_parts'] > 0){
			    	$user = User::find()->where(['id' => $userItem['id']])->one();
			    	$userInfo = $user->initUserInfo();

			    	$userInfo->count_parts = $userItem['count_parts'];
			    	$userInfo->save(false);
		    	}
		    }
		}
	}

    public function calcUserCountParts()
    {
    	return;
    	/*
    	$payDay           = '01'; // день выплат
    	$percent          = 5; // процент, отчисляемый на матчинг бонус
    	$startProjectDate = date('2017-10-' . $payDay . ' 00:00:00'); // дата старта проекта
    	$nowDate          = date('Y-m-' . $payDay . ' 00:00:00'); // дата выплаты в текущем месяце

    	$currentDate = $startProjectDate;
    	$currencyRUR = Currency::find()->where(['key' => Currency::KEY_RUR])->one();

    	while(strtotime($currentDate) < strtotime($nowDate)){

    		$dateFrom = date('Y-m-d H:i:s', strtotime($currentDate) + 1);
    		$currentDate = date('Y-m-d H:i:s', strtotime($currentDate . ' +1 month'));
    		$dateTo = $currentDate;

	    	// считаем всего инвестиций
	    	$countInvest = UserInvest::find()
	    		->select('SUM(sum) AS sum')
	    		->where(['>=', 'date_add', $dateFrom])
	    		->where(['<', 'date_add', $dateTo])
	    		->one();

	    	if (empty($countInvest->sum)){
	    		$countInvest->sum = 0;
	    	}

	    	// сумма - отдаваемая на промо доли
	    	$percentSum = (($countInvest->sum ) / 100 ) * $percent;

		    // считаем структурный оборот, большей ноги, личные инвестиции, доли, за месяц
		    $userData = self::calcAllStructUserInvest($dateFrom, $dateTo);

		    foreach($userData as $item){
		    	$user = User::find()->where(['id' => $item['id']])->one();
		    	$userInfo = $user->initUserInfo();

		    	$userInfo->count_parts = $item['count_parts'];
		    	$userInfo->save(false);
		    }
		}
		*/
	}


	// пересчёт инвестиций структуры и личных инвестиций всех участников
	public static function calcAllStructUserInvest($dateFrom, $dateTo)
	{
		$userData = self::calcAllUserInvest($dateFrom, $dateTo);

		$userData = self::calcAllStructInvest($userData);

		$userData = self::calcBigLegInvest($userData);

		$userData = self::calcCountParts($userData, $dateFrom, $dateTo);

		return $userData;
	}

	// Пересчёт личных инвестиций всех участников
	public static function calcAllUserInvest($dateFrom, $dateTo)
	{
		$userArray = User::find()->all();

		$userData = [];

		foreach($userArray as $user){

			$self_invest = self::calcUserInvest($user, InvestPref::KEY_SIMPLE_INVEST, $dateFrom, $dateTo);
			
			$userData[$user->id] = [
				'id' => $user->id,
				'login' => $user->login,
				'self_invest' => $self_invest,
				'struct_invest' => 0,
				'big_leg_invest' => 0,
				'count_parts' => 0,
				'legs' => []
			];
		}

		return $userData;
	}

	// пересчёт всех оборотов структуры
	public function calcAllStructInvest($userData)
	{
		foreach($userData as &$userDataItem){
			$user = User::find()->where(['id' => $userDataItem['id']])->one();
			$userIds = ArrayHelper::map($user->descendants()->all(), 'id', 'id');

			foreach($userIds as $userId){
				if (!empty($userData[$userId]['self_invest'])){
					$userDataItem['struct_invest'] += $userData[$userId]['self_invest'];
				}
			}
		}

		return $userData;
	}

	// Пересчёт личных инвестиций переданного участника
	public static function calcUserInvest($user, $investPrefKey, $dateFrom, $dateTo)
	{
		$investPrefArray = InvestPref::find()->where(['key' => $investPrefKey])->all();

		$investTypes = [];
		foreach($investPrefArray as $investPref){
			$invest = $investPref->getInvestType()->one();

			if (!empty($invest)){
				$investTypes[$invest->id] = $invest->id;
			}
		}

		$invest = UserInvest::find()
			->select('SUM(sum) as sum')
			->where(['user_id' => $user->id])
			->andWhere(['in', 'invest_type_id', $investTypes])
    		->andWhere(['>=', 'date_add', $dateFrom])
    		->andWhere(['<', 'date_add', $dateTo])
			->one();

		if (!empty($invest) && $invest->sum > 0){
			$sum = $invest->sum;
		}else{
			$sum = 0;
		}

		return $sum;
	}

	// Расчёт инвестиций для большей ноги участника
	public function calcBigLegInvest($userData)
	{
		foreach($userData as &$userDataItem){
			$user = User::find()->where(['id' => $userDataItem['id']])->one();
			$childrenIds = ArrayHelper::map($user->children()->all(), 'id', 'id');

			foreach($childrenIds as $childId){
				$userDataItem['legs'][$childId] = $userData[$childId]['struct_invest'];
			}

			$big_leg_invest = 0;
			foreach($userDataItem['legs'] as $leg){
				if ($leg > $big_leg_invest){
					$big_leg_invest = $leg;
				}
			}

			$userDataItem['big_leg_invest'] = $big_leg_invest;
		}

		return $userData;
	}

	// Считаем количество долей
	public function calcCountParts($userData, $dateFrom, $dateTo)
	{
		foreach($userData as &$userDataItem){
			$userDataItem['count_parts'] = self::getCountPartsForSum($userDataItem['struct_invest'], $userDataItem['big_leg_invest'], $userDataItem, $dateFrom, $dateTo);
		}

		return $userData;
	}

	// Считаем общее колиечство долей
	public function calcSumCountParts($userData)
	{
		$sumParts = 0;
		foreach($userData as $userDataItem){
			$sumParts += $userDataItem['count_parts'];
		}

		return $sumParts;
	}

}