<?php

namespace app\modules\profile\promo;

use Yii;
use yii\helpers\Html;
use app\modules\profile\models\Promo;
use app\modules\profile\models\UserPromo;
use app\modules\profile\models\User;
use app\modules\finance\models\Transaction;
use app\modules\finance\models\Currency;
use app\modules\invest\models\UserInvest;
use app\modules\matrix\models\Matrix;
use app\modules\mainpage\models\Preference;
use app\modules\invest\models\DetailMarketing;
use app\modules\invest\components\marketing\SuhbaMarketing;


/* 
Suhba - структурное распределение на 21 уровень
*/
class SuhbaStruct
{
    public function handle($promo)
    {
        $time = time();
        //$time = strtotime("2018-03-06 22:59:59");
        $createTransactionPeriod = SuhbaMarketing::getTransactionPeriod();
        $periodArray = SuhbaMarketing::getPeriodArray();

        foreach($periodArray as $periodItem){
            if ($time >= strtotime($periodItem['dateFrom']) && $time <= strtotime($periodItem['dateTo'])){
                $dateFrom = $periodItem['dateFrom'];
                $dateTo = $periodItem['dateTo'];
                break;
            }
        }

        if (empty($dateFrom) || empty($dateTo)){
            return;
        }

        $userInvestArray = UserInvest::find()
            ->andWhere(['>=', 'date_add', $dateFrom])
            ->andWhere(['<=', 'date_add', $dateTo])
            ->all();

        echo 'Расчёт структурных дата: ' . date('Y-m-d H:i:s', $time) . "\r\n";
        $userArray = [];
        foreach ($userInvestArray as $userInvest){
            $marketing = new SuhbaMarketing;
            
            $marketing->userInvest = $userInvest;
            $marketing->investType = $userInvest->getInvestType()->one();
            $marketing->user_invest_date_add = $userInvest->date_add;
            $marketing->user_to_invest_date_add = $periodItem['dateTo'];

            $marketing->initInvestPref();
            $marketing->setUser($userInvest->getUser()->one());
            $marketing->toStructureBalance();

            if(!empty($marketing->refUserArray)){
                foreach($marketing->refUserArray as $refUser){

                    if (empty($userArray[$refUser['to_user_id']]['sum'])){
                        $userArray[$refUser['to_user_id']]['sum'] = 0;
                    }
                    $userArray[$refUser['to_user_id']]['sum'] += $refUser['sum'];
                    $userArray[$refUser['to_user_id']]['info'][] = $refUser;
                };
            };
        }

        $calcSec = time();

        Yii::$app->db->createCommand()->update('user_info', ['struct_sum' => 0])->execute();

        foreach($userArray as $userId => $structUserInfo){

            $user = User::find()->where(['id' => $userId])->one();
            $userInfo = $user->initUserInfo();
            $userInfo->struct_sum = $structUserInfo['sum'];
            $userInfo->struct_data = json_encode($structUserInfo['info']);
            $userInfo->save(false);

            foreach($structUserInfo['info'] as $info){
                $detailMarketing = DetailMarketing::find()
                    ->where(['to_user_id' => $info['to_user_id']])
                    ->andWhere(['from_user_id' => $info['from_user_id']])
                    ->andWhere(['currency_id' => $info['currency_id']])
                    ->andWhere(['user_invest_id' => $info['user_invest_id']])
                    ->one();

                if (empty($detailMarketing)){
                    $detailMarketing = new DetailMarketing;
                    $detailMarketing->to_user_id = $info['to_user_id'];
                    $detailMarketing->from_user_id = $info['from_user_id'];
                    $detailMarketing->currency_id = $info['currency_id'];
                    $detailMarketing->user_invest_id = $info['user_invest_id'];
                }

                if ($detailMarketing->sum != $info['sum']){
                    $detailMarketing->percent = $info['percent'];
                    $detailMarketing->level = $info['level'];
                    $detailMarketing->date = $info['date'];
                    $detailMarketing->sum = $info['sum'];
                    $detailMarketing->save(false);
                }

                //echo 'Пересчёт для: ' . $detailMarketing->getToUser()->one()->login . ' дата: ' . $info['date'] . ' окончен' ."\r\n";
            }
        }
        echo 'Расчёт структурных завершён время выполнения: ' . (time() - $calcSec) . "\r\n";

        // Если настало время начислений
        if ($time >= (strtotime($dateTo) - $createTransactionPeriod)){
            $currency = Currency::find()->where(['key' => Currency::KEY_RUR])->one();
            foreach($userArray as $userId => $structUserInfo){

                $testTransaction = Transaction::find()
                    ->where(['to_user_id' => $userId])
                    ->andWhere(['type' => Transaction::TYPE_PARTNER_BUY_PLACE_TO_DEPOSIT])
                    ->andWhere(['>=', 'date_add', $dateFrom])
                    ->andWhere(['<=', 'date_add', $dateTo])
                    ->one();

                if (empty($testTransaction)){

                    $transaction = new Transaction;
                    $transaction->sum = $structUserInfo['sum'];
                    $transaction->type = Transaction::TYPE_PARTNER_BUY_PLACE_TO_DEPOSIT;
                    $transaction->to_user_id = $userId;
                    $transaction->currency_id = $currency->id;
                    $transaction->date_add = date('Y-m-d H:i:s', $time);
                    $transaction->save(false);

                    $balance = $transaction->getToUser()->one()->getBalances([$transaction->currency_id])[$transaction->currency_id];
                    $balance->recalculateValue();
                    echo 'юзеру: ' . $transaction->getToUser()->one()->login . ' начислено: ' . $transaction->sum . "\r\n";
                }
            }
        }
    }
}