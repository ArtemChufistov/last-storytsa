<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\contact\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <?php echo $form->field($model, 'id')->widget(Select2::classname(), [
                'initValueText' => $model->login,
                'value' => $model->id,
                'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                'theme' => Select2::THEME_BOOTSTRAP,
                'pluginOptions' => [
                    'minimumInputLength' => 2,
                    'ajax' => [
                        'url' => Url::to(['/profile/backuser/searhbylogin']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(user) { return user.login; }'),
                    'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                ],
            ])->label(Yii::t('app', 'Логин')); ?>

            <?php echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
                'initValueText' => $model->parent()->one()->login,
                'value' => $model->parent_id,
                'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                'theme' => Select2::THEME_BOOTSTRAP,
                'pluginOptions' => [
                    'minimumInputLength' => 2,
                    'ajax' => [
                        'url' => Url::to(['/profile/backuser/searhbylogin']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(user) { return user.login; }'),
                    'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                ],
            ]);?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
</div>