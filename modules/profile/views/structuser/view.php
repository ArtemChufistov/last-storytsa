<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\profile\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователь'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriber-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <p>
                <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div>
        <div class="box-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                      'attribute' => 'id',
                      'format' => 'raw',
                      'label' => Yii::t('app', 'Пользователь'),
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user_login', ['model' => $model]);},
                    ],[
                      'attribute' => 'parent_id',
                      'format' => 'raw',
                      'label' => Yii::t('app', 'Пригласитель'),
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user_parent_login', ['model' => $model]);},
                    ],
                ],
            ]) ?>

        </div>
      </div>
    </div>
  </div>
</div>
