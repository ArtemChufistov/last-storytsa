<?php
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\profile\models\SubscriberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Управление структурами');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriber-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">

            </div>
            <?php Pjax::begin(); ?>

                <div class="box-body no-padding">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                          'attribute' => 'id',
                          'format' => 'raw',
                          'label' => Yii::t('app', 'Пользователь'),
                          'filter' => Select2::widget([
                              'name' => (new ReflectionClass($searchModel))->getShortName() . '[id]',
                              'theme' => Select2::THEME_BOOTSTRAP,
                              'options' => [
                                  'placeholder' => Yii::t('app', 'Введите логин'),
                              ],
                              'pluginOptions' => [
                                  'minimumInputLength' => 2,
                                  'ajax' => [
                                      'url' => Url::to(['/profile/backuser/searhbylogin']),
                                      'dataType' => 'json',
                                      'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                  ],
                                  'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                  'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                  'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                              ]
                          ]),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user_login', ['model' => $model]);},
                        ],[
                          'attribute' => 'parent_id',
                          'format' => 'raw',
                          'label' => Yii::t('app', 'Пригласитель'),
                          'filter' => Select2::widget([
                              'name' => (new ReflectionClass($searchModel))->getShortName() . '[parent_id]',
                              'theme' => Select2::THEME_BOOTSTRAP,
                              'options' => [
                                  'placeholder' => Yii::t('app', 'Введите логин'),
                              ],
                              'pluginOptions' => [
                                  'minimumInputLength' => 2,
                                  'ajax' => [
                                      'url' => Url::to(['/profile/backuser/searhbylogin']),
                                      'dataType' => 'json',
                                      'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                  ],
                                  'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                  'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                  'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                              ]
                          ]),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user_parent_login', ['model' => $model]);},
                        ],[
                          'attribute' => 'parent_id',
                          'format' => 'raw',
                          'label' => Yii::t('app', 'Цепь пригласителей'),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user_parent_chain', ['model' => $model]);},
                        ],[
                          'attribute' => 'parent_id',
                          'format' => 'raw',
                          'label' => Yii::t('app', 'Лично приглашённых'),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user_childs', ['model' => $model]);},
                        ],[
                          'attribute' => 'parent_id',
                          'format' => 'raw',
                          'label' => Yii::t('app', 'Всего в структуре'),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user_descendants', ['model' => $model]);},
                        ],
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
      </div>
    </div>
</div>
