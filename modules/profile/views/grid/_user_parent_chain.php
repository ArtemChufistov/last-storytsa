<?php foreach(array_reverse($model->ancestors()->all()) as $ancestor):?>
	<i class="fa fa-arrow-right" aria-hidden="true"></i> <small class="label label-success"><?php echo $ancestor->login; ?></small>
<?php endforeach;?>