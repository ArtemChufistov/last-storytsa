<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\profile\models\Occupation */

$this->title = Yii::t('app', 'Create Occupation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Occupations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="occupation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
