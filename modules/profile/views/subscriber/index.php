<?php
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\profile\models\SubscriberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Подписчики');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriber-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                 <?= Html::a(Yii::t('app', 'Добавить подписчика'), ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <?php Pjax::begin(); ?>

                <div class="box-body no-padding">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        [
                          'attribute' => 'user_id',
                          'format' => 'raw',
                          'filter' => Select2::widget([
                              'name' => (new ReflectionClass($searchModel))->getShortName() . '[user_id]',
                              'theme' => Select2::THEME_BOOTSTRAP,
                              'options' => [
                                  'placeholder' => Yii::t('app', 'Введите логин'),
                              ],
                              'pluginOptions' => [
                                  'minimumInputLength' => 2,
                                  'ajax' => [
                                      'url' => Url::to(['/profile/backuser/searhbylogin']),
                                      'dataType' => 'json',
                                      'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                  ],
                                  'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                  'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                  'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                              ]
                          ]),
                          'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/user_id', ['model' => $model]);},
                        ],
                        'mail',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
      </div>
    </div>
</div>
