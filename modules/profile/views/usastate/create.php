<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\profile\models\UsaState */

$this->title = Yii::t('app', 'Create Usa State');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usa States'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usastate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
