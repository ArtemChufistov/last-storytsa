<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\profile\models\YoutubeVideo */

$this->title = Yii::t('app', 'Create Youtube Video');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Youtube Videos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="youtube-video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
