<?php

namespace app\modules\profile\models;

use Yii;
use app\modules\profile\models\User;

/**
 * This is the model class for table "{{%user_info}}".
 *
 * @property int $id
 * @property int $user_id
 * @property double $self_invest
 * @property double $struct_invest
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['lastDateSendMailInvite', 'lang', 'online_date', 'sub_id', 'count_parts', 'struct_sum', 'struct_data', 'matching_bonus_date'], 'safe'],
            [['self_invest', 'struct_invest'], 'number'],
        ];
    }

    public static function infoRow($user)
    {
        $userInfo = $user->initUserInfo();
        $data['title'] = 'text';
        $data['value'] = Yii::$app->controller->renderPartial('@app/themes/' . \Yii::$app->params['theme'] . '/office/partial/_info-row', ['user' => $user, 'userInfo' => $userInfo]);

        return $data;
    }

    // toDo -удалить?
    public static function getStructInvest()
    {
        $data['title'] = 'struct_invest';
        $data['value'] = [ 2, 3];

        return $data;
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lang' => Yii::t('app', 'Language'),
            'user_id' => Yii::t('app', 'User ID'),
            'sub_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'count_parts' => Yii::t('app', 'Count parts'),
            'self_invest' => Yii::t('app', 'Self Invest'),
            'struct_invest' => Yii::t('app', 'Struct Invest'),
            'online_date' => Yii::t('app', 'Last online date'),
            'lastDateSendMailInvite' => Yii::t('app', 'Last Date send mail invite'),
            'struct_sum' => Yii::t('app', 'Сумма структуры за период'),
            'struct_data' => Yii::t('app', 'Информация по структрным начислениям'),
            'matching_bonus_date' => Yii::t('app', 'Дата выплаты матчинг бонуса'),
        ];
    }
}
