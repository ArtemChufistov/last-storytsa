<?php

namespace app\modules\profile\models;

use Yii;

/**
 * This is the model class for table "promo".
 *
 * @property int $id
 * @property string $key
 * @property string $name
 */
class Promo extends \yii\db\ActiveRecord
{

    const KEY_EVINIZI_PROMO_1 = 'eviniziPromo1';
    const KEY_EVINIZI_PROMO_2 = 'eviniziPromo2';
    const KEY_EVINIZI_PROMO_3 = 'eviniziPromo3';
    const KEY_SUHBA_MATHCING_BONUS_PROMO = 'suhbaMathcingBonusPromo';
    const KEY_SUHBA_ADMIN_BONUS = 'suhbaAdminBonus';
    const KEY_SUHBA_STRUCT_BONUS = 'suhbaStructBonus';


    public static function getKeyHandlers()
    {
        return [
            self::KEY_EVINIZI_PROMO_1 => 'app\modules\profile\components\EviniziPromo1',
            self::KEY_EVINIZI_PROMO_2 => 'app\modules\profile\components\EviniziPromo2',
            self::KEY_EVINIZI_PROMO_3 => 'app\modules\profile\components\EviniziPromo3',
            self::KEY_SUHBA_MATHCING_BONUS_PROMO => 'app\modules\profile\promo\SuhbaMathcingBonusPromo',
            self::KEY_SUHBA_ADMIN_BONUS => 'app\modules\profile\promo\SuhbaAdmin',
            self::KEY_SUHBA_STRUCT_BONUS => 'app\modules\profile\promo\SuhbaStruct',
        ];
    }

    public function getHandler()
    {
        $data = self::getKeyHandlers();

        return $data[$this->key];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}