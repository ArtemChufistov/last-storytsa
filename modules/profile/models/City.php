<?php

namespace app\modules\profile\models;

use Yii;

/**
 * This is the model class for table "lb_city".
 *
 * @property int $id
 * @property int $country_id
 * @property string $city
 * @property string $state
 * @property string $region
 * @property int $biggest_city
 *
 * @property LbCountry $country
 * @property LbUser[] $lbUsers
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lb_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'city', 'region'], 'required'],
            [['country_id', 'biggest_city'], 'integer'],
            [['city', 'state', 'region'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => LbCountry::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'region' => Yii::t('app', 'Region'),
            'biggest_city' => Yii::t('app', 'Biggest City'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(LbCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLbUsers()
    {
        return $this->hasMany(LbUser::className(), ['city_id' => 'id']);
    }
}
