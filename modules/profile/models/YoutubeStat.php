<?php

namespace app\modules\profile\models;

use Yii;

/**
 * This is the model class for table "{{%youtube_stat}}".
 *
 * @property int $id
 * @property string $user_id
 * @property string $video_id
 * @property int $showed
 * @property int $liked
 * @property int $comented
 * @property int $subscribed
 * @property string $date_add
 * @property string $date_showed
 * @property string $date_liked
 * @property string $date_comented
 * @property string $date_subscribed
 */
class YoutubeStat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%youtube_stat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['showed', 'liked', 'comented', 'subscribed'], 'integer'],
            [['date_add', 'date_showed', 'date_liked', 'date_comented', 'date_subscribed'], 'safe'],
            [['user_id', 'video_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'video_id' => Yii::t('app', 'Video ID'),
            'showed' => Yii::t('app', 'Showed'),
            'liked' => Yii::t('app', 'Liked'),
            'comented' => Yii::t('app', 'Comented'),
            'subscribed' => Yii::t('app', 'Subscribed'),
            'date_add' => Yii::t('app', 'Date Add'),
            'date_showed' => Yii::t('app', 'Date Showed'),
            'date_liked' => Yii::t('app', 'Date Liked'),
            'date_comented' => Yii::t('app', 'Date Comented'),
            'date_subscribed' => Yii::t('app', 'Date Subscribed'),
        ];
    }
}
