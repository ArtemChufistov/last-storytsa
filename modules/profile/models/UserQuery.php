<?php
namespace app\modules\profile\models;

use yii\db\ActiveRecord;
use valentinek\behaviors\ClosureTableQuery;
use \yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{
    public function behaviors() {
        return [
            [
                'class' => ClosureTableQuery::className(),
                'tableName' => 'user_tree'
            ],
        ];
    }
}

?>