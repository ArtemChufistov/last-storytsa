<?php

namespace app\modules\profile\models\forms;

use app\modules\mail\components\SendMailComponent;
use app\modules\profile\models\User;
use yii\base\Model;

/**
 * Форма восстановления пароля
 * Class PasswordResetForm
 * @package lowbase\user\models\forms
 */
class PasswordResetForm extends Model
{
    public $email;      // Электронная почта
    public $password;   // Пароль
    public $captcha;    // Капча

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return [
            [['email', 'password', 'captcha'], 'required'], // Обязательны для заполнения
            [['password'], 'string', 'min' => 4],   // Пароль минимум 4 символа
            ['email', 'email'], // Электронная почта
            ['email', 'exist',
                'targetClass' => '\lowbase\user\models\User',
                'message' => \Yii::t('app', 'Пользователь с таким Email не зарегистрирован.')
            ],  // Значение электронной почты должно присутствовать в базе данных
            ['captcha', 'captcha', 'captchaAction' => 'profile/profile/captcha'], // Проверка капчи
        ];
    }

    /**
     * Наименования полей формы
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'password' => \Yii::t('user', 'Новый пароль'),
            'email' => \Yii::t('user', 'Email'),
            'captcha' => \Yii::t('user', 'Защитный код'),
        ];
    }

    /**
     * Отправка сообщения с подтверждением
     * нового пароля
     * @return bool
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findByEmail($this->email);

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }
            if ($user->save()) {
                SendMailComponent::passwordReset($user, $this->password);
                return true;
            }
        }

        return false;
    }
}