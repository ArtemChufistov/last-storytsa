<?php

namespace app\modules\profile\models\forms;

use app\modules\mainpage\models\Preference;
use app\modules\profile\models\User;
use Yii;

/**
 * Форма регистрации на сайте
 * Class SignupForm
 * @package lowbase\user\models\forms
 */
class SignupForm extends User {
	public $ref; // Логин
	public $password; // Пароль
	public $captcha; // Капча
	public $licenseAgree; // Лицензионное соглашение
	public $passwordRepeat; // Повтор пароля

	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules() {
		return [
			[['login', 'password', 'email', 'licenseAgree'], 'required'], // Обязательные поля
			['login', 'match', 'pattern' => '/^[a-zA-Z](.[a-zA-Z0-9_-]*)$/', 'message' => Yii::t('app', 'Поле "Логин" может содержать только латинские буквы')],
			['login', 'unique', 'targetClass' => self::className(),
				'message' => Yii::t('app', 'Данный Логин уже зарегистрирован.')], // Логин должен быть уникальным
			['ref', 'refValidate', 'skipOnEmpty' => false],
			['ref', 'match', 'pattern' => '/^[a-zA-Z](.[a-zA-Z0-9_-]*)$/', 'message' => Yii::t('app', 'Поле "Пригласитель" может содержать только латинские буквы')],
			['passwordRepeat', 'passwordRepeatValidate'],
			['licenseAgree', 'licenseAgreeVlidate'],
			['email', 'email'], // Электронная почта
			['email', 'emailUnique'], // Электронная почта
			[['password'], 'string', 'min' => 4], // Пароль минимум 4 символа
			[['login'], 'string', 'min' => 5],
			[['sex', 'country_id', 'city_id', 'status'], 'integer'], // Целочисленные значения
			[['birthday', 'login_at', 'ref'], 'safe'], // Безопасные аттрибуты
			[['login', 'email', 'password', 'captcha', 'ref'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
			[['email', 'phone'], 'string', 'max' => 30], // Строка (максимум 100 символов)
			[['login', 'last_name', 'phone'], 'string', 'max' => 15], // Строка (максимум 100 символов)
			[['auth_key'], 'string', 'max' => 32], // Строка (максимум 32 символа)
			[['ip'], 'string', 'max' => 20], // Строка (максимуму 20 символов)
			[['password_hash', 'password_reset_token', 'email_confirm_token', 'image', 'address'], 'string', 'max' => 255], // Строка (максимум 255 символов)
			['status', 'in', 'range' => array_keys(self::getStatusArray())], // Статус должен быть из списка статусов
			['status', 'default', 'value' => self::STATUS_WAIT], // Статус после регистрации "Ожидает подтверждения"
			['sex', 'in', 'range' => array_keys(self::getSexArray())], // Пол должен быть из гендерного списка
			[['login', 'last_name', 'email', 'phone', 'address'], 'filter', 'filter' => 'trim'], // Обрезаем строки по краям
			[['last_name', 'password_reset_token', 'email_confirm_token',
				'image', 'sex', 'phone', 'country_id', 'city_id', 'address',
				'auth_key', 'password_hash', 'email', 'ip', 'login_at'], 'default', 'value' => null], // По умолчанию значение = null
		];
	}

	// валидация рефки
	public function refValidate($attribute, $params) {

		if ($this->$attribute == '') {
			$defaultLogin = Preference::find()->where(['key' => Preference::KEY_USER_REG_DEFAULT_PARENT])->one();
			if (empty($defaultLogin)) {
				$this->addError($attribute, Yii::t('app', 'Необходимо указать логин пригласителя'));
			} else {
				$defaultParent = User::find()->where(['id' => $defaultLogin->value])->one();
				if (empty($defaultParent)) {
					$this->addError($attribute, Yii::t('app', 'Необходимо указать логин пригласителя'));
				}
				$this->parent_id = $defaultParent->id;
			}
			return;
		}

		$parentUser = $this->find()->where(['login' => $this->$attribute])->one();

		if (empty($parentUser)) {
			$this->addError($attribute, Yii::t('app', 'Пригласитель не найден, пожалуйста уточните его Логин'));
		} else {
			$this->parent_id = $parentUser->id;
		}
	}

    public function emailUnique($attribute, $params) {
            $prefEmailUnique = Preference::find()->where(['key' => Preference::KEY_USER_EMAIL_UNIQUE])->one();

            if (empty($prefEmailUnique)){
                    return;
            }else{
                    $user = User::find()->where(['email' => $this->$attribute])->one();
                    if (!empty($user)){
                            $this->addError($attribute, Yii::t('app', 'такой E-mail уже зарегистрирован'));
                    }
            }
    }

	public function passwordRepeatValidate($attribute, $params) {
		if ($this->$attribute != $this->password) {
			$this->addError($attribute, Yii::t('app', 'Пароли не совпадают'));
		}
	}

	public function licenseAgreeVlidate($attribute, $params) {
		if ($this->$attribute == 0) {
			$this->addError($attribute, Yii::t('app', 'Необходимо принять условия лицензионного соглашения'));
		}
	}

	public function attributeLabels() {
		$labels = parent::attributeLabels();
		$labels['password'] = Yii::t('app', 'Пароль');
		$labels['passwordRepeat'] = Yii::t('app', 'Повтор пароля');
		$labels['licenseAgree'] = Yii::t('app', 'Лицензионное соглашение');
		$labels['captcha'] = Yii::t('app', 'Защитный код');
		$labels['ref'] = Yii::t('app', 'Ваш пригласитель');
		$labels['login'] = Yii::t('app', 'Логин');

		return $labels;
	}

	/**
	 * Генерация ключа авторизации, токена подтверждения регистрации
	 * и хеширование пароля перед сохранением
	 * @param bool $insert
	 * @return bool
	 */
	public function beforeSave($insert) {
		if (empty($this->ref)) {
			$user = User::find()->orderBy(['id' => SORT_ASC])->one();
			$this->ref = $user->login;
			$this->leaf = 1;
			$this->parent_id = $user->id;
		}

		if (parent::beforeSave($insert)) {

			$this->setPassword($this->password);
			$this->status = parent::STATUS_ACTIVE;
			//$this->generateAuthKey();
			//$this->generateEmailConfirmToken();
			return true;
		}
		return false;
	}

	/**
	 * Отправка email об успешной регистрации
	 * Вставка в дерево пользователя
	 */
	public function afterSave($insert, $changedAttributes) {
		$this->setDefaultRegisterInfo();
	}
}
