<?php

namespace app\modules\profile\models\forms;

use app\modules\mail\components\SendMailComponent;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\models\User;
use lowbase\user\models\UserOauthKey;
use yii\db\ActiveRecord;
use Yii;

class ChangeSubmitEmailForm extends \yii\base\Model
{
    public $user;

    public $changing_email;
    public $email_confirm_token;

    public $send_mail_submit;
    public $send_confirm_token_submit;

    public $email_is_send;

    /**
     * Правила валдиации
     * @return array
     */
    public function rules()
    {
        return [
            [['changing_email'], 'required'],   // Обязательно для заполнения
            ['changing_email', 'email'], // Электронная почта
            [['send_mail_submit', 'email_is_send'], 'safe'],
            [['changing_email'], 'string', 'max' => 100], // Строковые значения (максимум 100 символов)
            [['changing_email'], 'filter', 'filter' => 'trim'],   // Обрезание строк по краям
            ['email_confirm_token', 'confirm_token_validate', 'skipOnEmpty' => false],
        ];
    }

    /**
     * Наименования дополнительных
     * полей формы
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'changing_email' => Yii::t('app', 'E-mail'),
            'email_confirm_token' => Yii::t('app', 'Код подтверждения')
        ];
    }

    /**
     * Валидация электронной почты
     *
     * Указывается обязательно при отсутствии
     * ключей авторизации соц. сетей
     */
    public function emailValidate()
    {
        if (!$this->email && !UserOauthKey::isOAuth($this->id)) {
            $this->addError('changing_email', Yii::t('app', 'Необходимо указать Email.'));
        }
    }

    public function confirm_token_validate($attribute, $params)
    {
        if (!empty($this->getErrors())){
            return;
        }

        if ($this->user->email_confirm_token == '' || $this->user->changing_email != $this->changing_email || $this->send_mail_submit == true){

            $seconds = time() - strtotime($this->user->last_date_email_confirm_token);

            if ($seconds < User::LAST_DATE_EMAIL_CONFIRM_TOKEN_EXPIRE){
                $this->addError('changing_email', Yii::t('app', 'Повторная отправка письма возможна через {seconds} секунд',[
                    'seconds' => User::LAST_DATE_EMAIL_CONFIRM_TOKEN_EXPIRE - (int)$seconds]));
                return;
            }

            $this->user->generateEmailConfirmToken();
            $this->user->changing_email = $this->changing_email;
            $this->user->save(false);

            // отправить почту
            SendMailComponent::confirmEmailCode($this->user, $this->user->changing_email);

            $this->addError('email_is_send', Yii::t('app', 'На указаную Вами почту отправлено письмо, пожалуйста перейдите по ссылке из письма.'));
            return;
        }else{
            if ($this->user->email_confirm_token != $this->$attribute){
                $this->addError('email_confirm_token', Yii::t('app', 'Необходимо указать правильный код подтверждения.'));
            }else{
                $this->user->email = $this->user->changing_email;
                $this->user->changing_email = '';
                $this->user->email_confirm_token = '';
                $this->user->updateAuthAssigment(AuthAssignment::ITEM_NAME_VERIFIED_USER);
                $this->user->save(false);
            }
        }
    }
}
