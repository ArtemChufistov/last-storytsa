<?php

namespace app\modules\profile\models\forms;

use app\modules\mainpage\models\Preference;
use app\modules\profile\models\User;
use Yii;

/**
 * Форма регистрации на сайте с помощью соцсетей
 * Class SocialRegisterForm
 * @package lowbase\user\models\forms
 */
class SocialRegisterForm extends User {
	public $ref; // Логин
	public $password; // Пароль
	public $captcha; // Капча
	public $passwordRepeat; // Повтор пароля

	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules() {
        return [
            [['login', 'email'], 'required'], // Имя обязательно для заполнения
            [['sex', 'country_id', 'city_id', 'status'], 'integer'], // Только целочисленные значения
            [['birthday', 'login_at', 'parent_id', 'last_date_email_confirm_token', 'changing_email', 'skype', 'pay_password_hash', 'last_date_pay_password'], 'safe'], // Безопасные аттрибуты (любые значения) - преобразуются автоматически
            [['login', 'first_name', 'last_name', 'email', 'changing_email', 'skype', 'phone'], 'string', 'max' => 100], // Строки до 100 символов
            [['auth_key'], 'string', 'max' => 32], // Строка до 32 символов
            [['ip'], 'string', 'max' => 20], // Строковое значение (максимум 20 симоволов)
            [['password_hash', 'password_reset_token', 'pay_password_hash', 'email_confirm_token', 'image', 'address'], 'string', 'max' => 255], // Строки до 255 символов
            ['status', 'in', 'range' => array_keys(self::getStatusArray())], // Статус возможен только из списка статусов
            ['status', 'default', 'value' => self::STATUS_ACTIVE], // По умолчанию статус "Активен"
            ['sex', 'in', 'range' => array_keys(self::getSexArray())], // Пол только из гендерного списка
            ['email', 'email'], // Формат электронной почты
            [['login', 'first_name', 'last_name', 'email', 'skype', 'phone', 'address'], 'filter', 'filter' => 'trim'], // Обрезаем строки по краям
            [['first_name', 'last_name', 'password_reset_token', 'email_confirm_token',
                'image', 'sex', 'changing_email', 'pay_password_hash', 'skype', 'phone', 'country_id', 'city_id', 'address',
                'auth_key', 'password_hash', 'email', 'ip', 'login_at'], 'default', 'value' => null], // По умолчанию Null
            [['photo'], 'image',
                'minHeight' => 100,
                'skipOnEmpty' => true,
            ], // Изображение не менее 100 пикселей в высоту
        ];
    }

	public function passwordRepeatValidate($attribute, $params) {
		if ($this->$attribute != $this->password) {
			$this->addError($attribute, Yii::t('app', 'Пароли не совпадают'));
		}
	}
    
	public function attributeLabels() {
		$labels = parent::attributeLabels();
		$labels['password'] = Yii::t('app', 'Пароль');
		$labels['passwordRepeat'] = Yii::t('app', 'Повтор пароля');
		$labels['licenseAgree'] = Yii::t('app', 'Лицензионное соглашение');
		$labels['captcha'] = Yii::t('app', 'Защитный код');
		$labels['ref'] = Yii::t('app', 'Ваш пригласитель');
		$labels['login'] = Yii::t('app', 'Логин');

		return $labels;
	}
  
    /**
	 * Гендерный список
	 * @return array
	 */
	public static function getSexArray() {
		return [
			self::SEX_MALE => Yii::t('app', 'Мужской'),
			self::SEX_FEMALE => Yii::t('app', 'Женский'),
		];
	}
}