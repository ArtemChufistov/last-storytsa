<?php

namespace app\modules\profile\models\forms;

use app\modules\profile\models\User;
use lowbase\user\models\UserOauthKey;
use borales\extensions\phoneInput\PhoneInputValidator;
use Yii;

class ProfileForm extends User
{
    public $password;   // Новый пароль
    public $repeat_password; // подтверждение нового пароля
    public $send_mail_confirm_token_submit;
    public $confirm_token_submit;
    public $change_password_submit;
    public $old_password;

    // Код аутентификации
    public $authenticator_code;
    public $send_two_fa_confirm_submit;

    /**
     * Правила валдиации
     * @return array
     */
    public function rules()
    {
        return [
            //[['first_name'], 'required'],   // Обязательно для заполнения
            [['password', 'repeat_password'], 'passwordValidate', 'skipOnEmpty' => false], // Валидация пароля методом модели
            [['email'], 'emailValidate', 'skipOnEmpty' => false],   // Валидация электронной почты методом модели
            ['repeat_password', 'compare', 'compareAttribute'=>'password', 'message'=> Yii::t('app', 'Новый пароль не совпадает с подтверждением')],
            //[['password'], 'validatePassword'],
            ['email', 'email'], // Электронная почта
            [['password'], 'string', 'min' => 4],   // Пароль минимум 4 символа
            [['authenticator_code'], 'string', 'min' => 6, 'max' => 6],   // Пароль минимум 4 символа
            [['sex', 'country_id', 'city_id', 'status', 'authenticator_code'], 'integer'],    // Целочисленные значения
            [['birthday', 'login_at', 'confirm_token_submit', 'change_password_submit', 'send_mail_confirm_token_submit'
                , 'old_password', 'skype', 'nationality', 'passport', 'send_two_fa_confirm_submit', 'totp_secret'], 'safe'], // Безопасные аттрибуты
            [['first_name', 'last_name', 'email', 'phone'], 'string', 'max' => 100], // Строковые значения (максимум 100 символов)
            [['auth_key'], 'string', 'max' => 32],  // Строковое значение (максимум 32 символа)
            [['phone'], PhoneInputValidator::className()],
            [['ip'], 'string', 'max' => 20],    // Строковое значение (максимум 20 симоволов)
            [['password_hash', 'password_reset_token', 'email_confirm_token', 'image', 'address'], 'string', 'max' => 255], // Строка (максимум 255 символов)
            ['status', 'in', 'range' => array_keys(self::getStatusArray())], // Статус должен быть из списка статусов
            ['sex', 'in', 'range' => array_keys(self::getSexArray())], // Пол должен быть из гендерного списка
            [['first_name', 'last_name', 'email', 'phone', 'address'], 'filter', 'filter' => 'trim'],   // Обрезание строк по краям
            [['last_name', 'password_reset_token', 'email_confirm_token',
                'image', 'sex', 'phone', 'country_id', 'city_id', 'address',
                'auth_key', 'password_hash', 'email', 'ip', 'login_at'], 'default', 'value' => null],   // По умолчанию = null
        ];
    }

    /**
     * Наименования дополнительных
     * полей формы
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['password'] = Yii::t('user', 'Новый пароль');
        $labels['repeat_password'] = Yii::t('user', 'Подтверждение пароля');
        $labels['send_mail_confirm_token_submit'] = Yii::t('app', 'Отправить токен на E-mail ');
        $labels['confirm_token_submit'] = Yii::t('app', 'Подтвердить E-mail ');
        $labels['change_password_submit'] = Yii::t('app', 'Сменить пароль');
        $labels['send_two_fa_confirm_submit'] = Yii::t('app', 'Двухфакторная аутентификация');
        $labels['old_password'] = Yii::t('app', 'Старый пароль');
        $labels['passport'] = Yii::t('app', 'Номер удостоверения личности');
        $labels['nationality'] = Yii::t('app', 'Гражданство');
        $labels['authenticator_code'] = Yii::t('app', 'Код Аутентификации');
        return $labels;
    }

    /**
     * Генерация пароля и ключа авторизации,
     * преобразование дня рождения в необходимый
     * формат перед сохранением
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Если указан новый пароль
            if ($this->password) {
                $this->setPassword($this->password);
                $this->generateAuthKey();
            }
            // Преобразование дня рождения
            if ($this->birthday) {
                $date = new \DateTime($this->birthday);
                $this->birthday = $date->format('Y-m-d');
            }
            return true;
        }
        return false;
    }

    /**
     * Валидация пароля
     *
     * Указывается обязательно при отсутствии
     * ключей авторизации соц. сетей
     */
    public function passwordValidate()
    {
        if ($this->password_hash === null && !$this->password && !UserOauthKey::isOAuth($this->id)) {
            $this->addError('password', Yii::t('user', 'Необходимо указать пароль.'));
        }

        if (!empty($this->password) && empty($this->old_password)){
            $this->addError('old_password', Yii::t('user', 'Необходимо указать пароль.'));
        }

        if (!empty($this->old_password)){
            if (!$this->validatePassword($this->old_password)) {
                $this->addError('old_password', Yii::t('app', 'Неправильно введен старый пароль.'));
            }

            if (!empty($this->password) || !empty($this->repeat_password)){
                $this->passwordRepeatValidate();
            }
        }
    }

    public function passwordRepeatValidate()
    {
        if ($this->password != $this->repeat_password){
            $this->addError('password', Yii::t('user', 'Новый пароль не совпадает с подтверждением'));
        }
    }

    /**
     * Валидация электронной почты
     *
     * Указывается обязательно при отсутствии
     * ключей авторизации соц. сетей
     */
    public function emailValidate()
    {
        if (!$this->email && !UserOauthKey::isOAuth($this->id)) {
            $this->addError('email', Yii::t('user', 'Необходимо указать Email.'));
        }
    }
}
