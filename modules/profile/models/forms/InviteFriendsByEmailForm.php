<?php

namespace app\modules\profile\models\forms;

use app\modules\mail\components\SendMailComponent;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\models\User;
use lowbase\user\models\UserOauthKey;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;
use Yii;

class InviteFriendsByEmailForm extends \yii\base\Model
{
    public $user;
    public $separate_emails;
    public $mailArray;

    const MAX_EMAILS = 10;

    const SEND_MAIL_PERIOD = 86400;

    /**
     * Правила валдиации
     * @return array
     */
    public function rules()
    {
        return [
            [['separate_emails'], 'required'],   // Обязательно для заполнения
            ['separate_emails', 'separateValidate', 'skipOnEmpty' => false]
        ];
    }

    public function separateValidate()
    {
        $emailList = HtmlPurifier::process($this->separate_emails);
        $emailArray = explode(',', $this->separate_emails);
        
        foreach($emailArray as &$email){

            $email = trim($email);

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
              $this->addError('separate_emails', Yii::t('app', 'Некорректный e-mail список'));
              return false;
            }
        }

        if (count($email) > self::MAX_EMAILS){
          $this->addError('separate_emails', Yii::t('app', 'Максимально можно отправить {max_emails} приглашений', ['max_emails' => self::MAX_EMAIL]));
          return false;
        }


        $userInfo = $this->user->initUserInfo();

        if (strtotime($userInfo->lastDateSendMailInvite) > (time() - self::SEND_MAIL_PERIOD)){
          $this->addError('separate_emails', Yii::t('app', 'Следущая отправка приглашений возможна после {date}', ['date' => date('H:i d-m-Y', (strtotime($userInfo->lastDateSendMailInvite) + self::SEND_MAIL_PERIOD))]));
          return false;
        }

        $userInfo->lastDateSendMailInvite = date('Y-m-d H:i:s');
        $userInfo->save(false);

        $this->mailArray = $emailArray;
    }

    /**
     * Наименования дополнительных
     * полей формы
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'separate_emails' => Yii::t('app', 'Список E-mail через запятую')
        ];
    }
}
