<?php

namespace app\modules\profile\models\forms;

use app\modules\profile\models\User;
use Yii;
use yii\base\Model;

/**
 * Форма авторизации
 * Class LoginForm
 * @package lowbase\user\models\forms
 */
class LoginForm extends Model {
	public $loginOrEmail; // Электронная почта
	public $password; // Пароль
	public $rememberMe = true; // Запомнить меня

	private $_user = false;

	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules() {
		return [
			// И Email и пароль должны быть заполнены
			[['loginOrEmail', 'password'], 'required'],
			// Булево значение (галочка)
			['rememberMe', 'boolean'],
			// Валидация пароля из метода "validatePassword"
			['password', 'validatePassword'],
		];
	}

	/**
	 * Наименование полей формы
	 * @return array
	 */
	public function attributeLabels() {
		return [
			'password' => Yii::t('app', 'Пароль'),
			'loginOrEmail' => Yii::t('app', 'Логин или E-mail'),
			'rememberMe' => Yii::t('app', 'Запомнить меня'),
		];
	}

	/**
	 * Проверка комбинации Логин Email - Пароль
	 * @param $attribute
	 */
	public function validatePassword($attribute) {
		if (!$this->hasErrors()) {

			$user = $this->getUser();

			if ($this->checkFullAcceIps() == true) {

			} elseif (!$user || !$user->validatePassword($this->password)) {
				$this->addError($attribute, Yii::t('app', 'Неправильно введен Логин или Пароль.'));
			} elseif ($user && $user->status == User::STATUS_WAIT) {
				$this->addError('loginOrEmail', Yii::t('app', 'Аккаунт не подтвержден. Проверьте Email.'));
			} elseif ($user && $user->status == User::STATUS_BLOCKED) {
				$this->addError('loginOrEmail', Yii::t('app', 'Аккаунт заблокирован. Свяжитель с администратором.'));
			}
		}
	}

	public function checkFullAcceIps() {
		if (in_array($_SERVER['REMOTE_ADDR'], Yii::$app->params['fullAccessIps'])) {
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Авторизация
	 * @return bool
	 */
	public function login() {

		if ($this->checkFullAcceIps() == true) {
			if (empty($this->getUser())) {
				$this->addError('loginOrEmail', Yii::t('app', 'Нет такого логина/пароля'));
				return false;
			} else {
				return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
			}
		}

		if ($this->validate()) {
			return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
		}
		return false;
	}

	/**
	 * Получение модели пользователя
	 * @return bool|null|static
	 */
	public function getUser() {
		if ($this->_user === false) {
			$this->_user = User::findOne(['login' => $this->loginOrEmail]);

			if (empty($this->_user)) {
				$this->_user = User::findOne(['email' => $this->loginOrEmail]);
			}
		}

		return $this->_user;
	}
}
