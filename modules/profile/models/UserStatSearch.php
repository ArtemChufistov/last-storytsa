<?php

namespace app\modules\profile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Поиск среди пользователей
 * Class UserSearch
 * @package lowbase\user\models
 */
class UserStatSearch extends User
{

    public $countUser;
    public $perday;
    public $dateFrom;
    public $dateTo;

    /**
     * Сценарии
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function rules() {
        return [
            [['dateFrom', 'dateTo'], 'safe'],
        ];
    }

    /**
     * Наименования дополнительных полей
     * аттрибутов, присущих модели поиска
     * @return array
     */
    public function attributeLabels()
    {
        $label = parent::attributeLabels();
        $label['count'] = Yii::t('app', 'count');
        $label['dateFrom'] = Yii::t('app', 'Дата от');
        $label['dateTo'] = Yii::t('app', 'Дата до');

        return $label;
    }

    public function initDateFromTo()
    {
        $time = time();
        if (empty($this->dateFrom)){
            $this->dateFrom = date('Y-m-d 00:00:00', $time - (60*60*24*30));
        }else{
            $this->dateFrom = date('Y-m-d 00:00:00', strtotime($this->dateFrom));
        }

        if (empty($this->dateTo)){
            $this->dateTo = date('Y-m-d 23:59:59', $time);
        }else{
            $this->dateTo = date('Y-m-d 23:59:59', strtotime($this->dateTo));
        }
    }

    /**
     * Создает DataProvider на основе переданных данных
     * @param $params - параметры
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $this->initDateFromTo();

        $query = UserStatSearch::find();

        $query->select('count(*) as countUser, DATE_FORMAT(created_at,"%Y-%m-%d") as `perday`');

        $query->where(['>=', 'created_at', $this->dateFrom]);

        $query->andWhere(['<=', 'created_at', $this->dateTo]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => ['created_at' => SORT_ASC],
            ),
        ]);

        $query->groupBy('perday');

        return $dataProvider;
    }
}
