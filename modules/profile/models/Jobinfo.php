<?php

namespace app\modules\profile\models;

use Yii;

/**
 * This is the model class for table "{{%lb_jobinfo}}".
 *
 * @property int $id
 * @property int $company_name
 * @property int $state_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property int $occupation_id
 * @property string $attach_file
 */
class Jobinfo extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%lb_jobinfo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_name', 'state_id', 'name', 'phone', 'email', 'website', 'occupation_id'], 'required'],
            [['state_id', 'occupation_id'], 'integer'],
            [['company_name', 'name', 'phone', 'email', 'website', 'attach_file'], 'string', 'max' => 100],
            [['attach_file'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_name' => Yii::t('app', 'Company Name'),
            'state_id' => Yii::t('app', 'Location'),
            'name' => Yii::t('app', 'Your Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'E-mail'),
            'website' => Yii::t('app', 'Website'),
            'occupation_id' => Yii::t('app', 'Occupation'),
            'attach_file' => Yii::t('app', 'Attach File'),
        ];
    }
}
