
<?php

namespace app\modules\profile\models;

use Yii;

/**
 * This is the model class for table "user_promo".
 *
 * @property int $id
 * @property int $promo_id
 * @property int $user_id
 * @property double $sum
 * @property double $accured_sum
 * @property string $params
 */
class UserPromo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_promo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promo_id', 'user_id'], 'integer'],
            [['sum', 'accured_sum'], 'number'],
            [['params'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'promo_id' => Yii::t('app', 'Promo ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'sum' => Yii::t('app', 'Sum'),
            'accured_sum' => Yii::t('app', 'Accured Sum'),
            'params' => Yii::t('app', 'Params'),
        ];
    }
}