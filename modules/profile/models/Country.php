<?php

namespace app\modules\profile\models;

use Yii;

/**
 * This is the model class for table "lb_country".
 *
 * @property int $id
 * @property string $name
 * @property string $currency_code
 * @property string $currency
 *
 * @property LbCity[] $lbCities
 * @property LbUser[] $lbUsers
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lb_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'currency_code'], 'required'],
            [['name', 'currency'], 'string', 'max' => 255],
            [['currency_code'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'currency_code' => Yii::t('app', 'Currency Code'),
            'currency' => Yii::t('app', 'Currency'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLbCities()
    {
        return $this->hasMany(LbCity::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLbUsers()
    {
        return $this->hasMany(LbUser::className(), ['country_id' => 'id']);
    }
}
