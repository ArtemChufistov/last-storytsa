<?php

namespace app\modules\profile\models;

use Yii;

/**
 * This is the model class for table "user_showed_video".
 *
 * @property int $id
 * @property int $user_id
 * @property int $count
 * @property string $date
 */
class UserShowedVideo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_showed_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'count'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'count' => Yii::t('app', 'Count'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
