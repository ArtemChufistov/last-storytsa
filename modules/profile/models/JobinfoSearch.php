<?php

namespace app\modules\profile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\profile\models\Jobinfo;

/**
 * JobinfoSearch represents the model behind the search form of `app\modules\profile\models\Jobinfo`.
 */
class JobinfoSearch extends Jobinfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_name', 'state_id', 'name', 'phone', 'email', 'website', 'occupation_id', 'attach_file'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Jobinfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_name' => $this->company_name,
            'state_id' => $this->state_id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'website' => $this->website,
            'occupation_id' => $this->occupation_id,
            'attach_file' => $this->attach_file,
        ]);

        return $dataProvider;
    }
}
