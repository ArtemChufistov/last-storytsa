<?php

namespace app\modules\profile\models;

use app\modules\event\models\Event;
use app\modules\finance\components\paysystem\PaySystemComponent;
use app\modules\finance\models\Currency;
use app\modules\finance\models\PaySystem;
use app\modules\finance\models\Transaction;
use app\modules\mail\components\SendMailComponent;
use app\modules\matrix\models\Matrix;
use app\modules\merchant\components\twofa\behaviors\TwoFaBehavior;
use app\modules\profile\models\AuthAssignment;
use app\modules\profile\models\UserOauthKey;
use app\modules\profile\models\UserInfo;
use app\modules\response\models\Response;
use app\modules\support\models\Ticket;
use lowbase\user\models\City;
use lowbase\user\models\Country;
use valentinek\behaviors\ClosureTable;
use app\modules\mainpage\models\Preference;
use app\modules\finance\behaviors\UserCurrencyBehavior;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\imagine\Image;
use yii\web\IdentityInterface;

/**
 * Класс пользователей
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email_confirm_token
 * @property string $email
 * @property string $image
 * @property integer $sex
 * @property string $birthday
 * @property string $phone
 * @property integer $country_id
 * @property integer $city_id
 * @property string $address
 * @property integer $status
 * @property string $ip
 * @property string $created_at
 * @property string $updated_at
 * @property string $login_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthItem[] $itemNames
 * @property UserOauthKey[] $userOauthKeys
 */
class User extends ActiveRecord implements IdentityInterface {
	// Статусы пользователя
	const STATUS_BLOCKED = 0; // заблокирован
	const STATUS_ACTIVE = 1; // активен
	const STATUS_WAIT = 2; // ожидает подтверждения

	// Гендерные статусы
	const SEX_MALE = 1; // мужчина
	const SEX_FEMALE = 2; // женщина
	const SEX_OTHER = 3;

	const TOFA_ON = 1;
	const TOFA_OFF = 0;

	// Время действия токенов
	const EXPIRE = 3600;

	// Расширение сохраняемого файла изобрежния
	// Определение Mime не предусмотрено. Файлы
	// изобрежния в соц. сетях часто без расширения в
	// названиях
	const EXT = '.jpg';

	const LAST_DATE_EMAIL_CONFIRM_TOKEN_EXPIRE = 180;

	public $photo; // аватар (само изображение)
	public $send_mail_confirm_token_submit;

	// права пользователя
	public $rights;
	public $paymentDate;
	public $levelNum;

	/**
	 * Название таблицы
	 * @return string
	 */
	public static function tableName() {
		return 'lb_user';
	}

	/**
	 * Автозаполнение полей создание и редактирование
	 * профиля
	 * @return array
	 */
	public function behaviors() {
		return [
		    [
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'created_at',
			'updatedAtAttribute' => 'updated_at',
			'value' => date('Y-m-d H:i:s'),
		],[
			'class' => ClosureTable::className(),
			'tableName' => 'user_tree',
		 ],[
			'class' => UserCurrencyBehavior::className(),
			'user' => $this
		 ],
            'two_fa' => ['class' => TwoFaBehavior::className()]
		];
	}

	/**
	 * Статусы пользователя
	 * @return array
	 */
	public static function getStatusArray() {
		return [
			self::STATUS_BLOCKED => Yii::t('app', 'Заблокирован'),
			self::STATUS_ACTIVE => Yii::t('app', 'Активен'),
			self::STATUS_WAIT => Yii::t('app', 'Не активен'),
		];
	}

	/**
	 * Гендерный список
	 * @return array
	 */
	public static function getSexArray() {
		return [
			self::SEX_MALE => Yii::t('app', 'Мужской'),
			self::SEX_FEMALE => Yii::t('app', 'Женский'),
			self::SEX_OTHER => Yii::t('app', 'Другой'),
		];
	}

	/**
	 * Правила валидации модели
	 * @return array
	 */
	public function rules() {
		return [
			[['login', 'email'], 'required'], // Имя обязательно для заполнения 
            [['sex', 'country_id', 'city_id', 'status'], 'integer'], // Только целочисленные значения
            [['birthday', 'login_at', 'parent_id', 'last_date_email_confirm_token', 'changing_email', 'skype', 'pay_password_hash', 'last_date_pay_password', 'leaf', 'nationality', 'passport', 'totp_secret'], 'safe'], // Безопасные аттрибуты (любые значения) - преобразуются автоматически
            [['login', 'last_name', 'email', 'phone'], 'string', 'max' => 100], // Строки до 100 символов 
            [['auth_key'], 'string', 'max' => 32], // Строка до 32 символов 
            [['ip'], 'string', 'max' => 20], // Строковое значение (максимум 20 симоволов) 
            [['password_hash', 'password_reset_token', 'pay_password_hash', 'email_confirm_token', 'image', 'address'], 'string', 'max' => 255], // Строки до 255 символов
            ['status', 'in', 'range' => array_keys(self::getStatusArray())], // Статус возможен только из списка статусов 
            ['status', 'default', 'value' => self::STATUS_ACTIVE], // По умолчанию статус "Активен" 
            ['sex', 'in', 'range' => array_keys(self::getSexArray())], // Пол только из гендерного списка 
            ['email', 'email'], // Формат электронной почты 
            [['login', 'first_name', 'last_name', 'email', 'skype', 'phone', 'address'], 'filter', 'filter' => 'trim'], // Обрезаем строки по краям 
            [['first_name', 'last_name', 'password_reset_token', 'email_confirm_token', 
              'image', 'sex', 'changing_email', 'pay_password_hash', 'skype', 'phone', 'country_id', 'city_id', 'address', 
              'auth_key', 'password_hash', 'email', 'ip', 'login_at'], 'default', 'value' => null], // По умолчанию Null 
            [['photo'], 'image', 
              'minHeight' => 100,
				'skipOnEmpty' => true,
			], // Изображение не менее 100 пикселей в высоту
		];
	}

	/**
	 * Наименования полей модели
	 * @return array
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'parent_id' => Yii::t('app', 'Пригласитель'),
			'login' => Yii::t('app', 'Логин'),
			'rights' => Yii::t('app', 'Права'),
			'skype' => Yii::t('app', 'Skype'),
			'first_name' => Yii::t('app', 'Имя'),
			'last_name' => Yii::t('app', 'Фамилия'),
			'auth_key' => Yii::t('app', 'Ключ авторизации'),
			'password_hash' => Yii::t('app', 'Хеш пароля'),
			'password_reset_token' => Yii::t('app', 'Токен восстановления пароля'),
			'email_confirm_token' => Yii::t('app', 'Код подтверждения'),
			'changing_email' => Yii::t('app', 'Новый E-mail'),
			'email' => Yii::t('app', 'Текущий E-mail'),
			'image' => Yii::t('app', 'Фото'),
			'photo' => Yii::t('app', 'Фото'),
			'sex' => Yii::t('app', 'Пол'),
			'birthday' => Yii::t('app', 'Дата рождения'),
			'phone' => Yii::t('app', 'Телефон'),
			'country_id' => Yii::t('app', 'Страна'),
			'city_id' => Yii::t('app', 'Город'),
			'address' => Yii::t('app', 'Адрес'),
			'status' => Yii::t('app', 'Статус'),
			'ip' => Yii::t('app', 'IP'),
			'created_at' => Yii::t('app', 'Создан'),
			'updated_at' => Yii::t('app', 'Обновлен'),
			'login_at' => Yii::t('app', 'Авторизован'),
			'leaf' => Yii::t('app', 'Листок'),
			'nationality' => Yii::t('app', 'Гражданство'),
			'passport' => Yii::t('app', '№ Паспорта'),
			'totp_secret' => Yii::t('app', 'Ключ безопасности'),
		];
	}

	/**
	 * Сязи пользователь => роль
	 * @return \yii\db\ActiveQuery
	 */
	public function getAuthAssignments() {
		return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
	}

	/**
	 * Роли и допуски (разрешения)
	 * @return \yii\db\ActiveQuery
	 */
	public function getItemNames() {
		return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('lb_auth_assignment', ['user_id' => 'id']);
	}

	/**
	 * Ключи авторизации соц. сетей и страницы соц. сетей
	 * @return \yii\db\ActiveQuery
	 */
	public function getKeys() {
		return $this->hasMany(UserOauthKey::className(), ['user_id' => 'id']);
	}

	public function getResponses() {
		return $this->hasMany(Response::className(), ['user_id' => 'id']);
	}

	public function getTickets() {
		return $this->hasMany(Ticket::className(), ['user_id' => 'id']);
	}

	public function getMatrixPlaces() {
		return $this->hasMany(Matrix::className(), ['user_id' => 'id']);
	}

	public function getEvents() {
		return $this->hasMany(Event::className(), ['user_id' => 'id']);
	}

	public function getUserInfo() {
		return $this->hasMany(UserInfo::className(), ['user_id' => 'id']);
	}

	public function initUserInfo()
	{
		$userInfo = $this->getUserInfo()->one();

		if (empty($userInfo)){
			$userInfo = new UserInfo;
			$userInfo->user_id = $this->id;
			$userInfo->save(false);
		}

		return $userInfo;
	}

	/**
	 * Страна
	 * @return \yii\db\ActiveQuery
	 */
	public function getCountry() {
		return $this->hasOne(Country::className(), ['id' => 'country_id']);
	}

	/**
	 * Город
	 * @return \yii\db\ActiveQuery
	 */
	public function getCity() {
		return $this->hasOne(City::className(), ['id' => 'city_id']);
	}

	/**
	 * Поиск пользователя по Id
	 * @param int|string $id - ID
	 * @return null|static
	 */
	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	/**
	 * Поиск пользователя по Email
	 * @param $email - электронная почта
	 * @return null|static
	 */
	public static function findByEmail($email) {
		return static::findOne(['email' => $email]);
	}

	/**
	 * Ключ авторизации
	 * @return string
	 */
	public function getAuthKey() {
		return $this->auth_key;
	}

	/**
	 * ID пользователя
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Проверка ключа авторизации
	 * @param string $authKey - ключ авторизации
	 * @return bool
	 */
	public function validateAuthKey($authKey) {
		return $this->authKey === $authKey;
	}

	/**
	 * Поиск по токену доступа (не поддерживается)
	 * @param mixed $token - токен
	 * @param null $type - тип
	 * @throws NotSupportedException - Исключение "Не подерживается"
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		throw new NotSupportedException(Yii::t('app', 'Поиск по токену не поддерживается.'));
	}

	/**
	 * Проверка правильности пароля
	 * @param $password - пароль
	 * @return bool
	 */
	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Генераия Хеша пароля
	 * @param $password - пароль
	 */
	public function setPassword($password) {
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Поиск по токену восстановления паролья
	 * Работает и для неактивированных пользователей
	 * @param $token - токен восстановления пароля
	 * @return null|static
	 */
	public static function findByPasswordResetToken($token) {
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}
		return static::findOne([
			'password_reset_token' => $token,
		]);
	}

	/**
	 * Генерация случайного авторизационного ключа
	 * для пользователя
	 */
	public function generateAuthKey() {
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Проверка токена восстановления пароля
	 * согласно его давности, заданной константой EXPIRE
	 * @param $token - токен восстановления пароля
	 * @return bool
	 */
	public static function isPasswordResetTokenValid($token) {
		if (empty($token)) {
			return false;
		}
		$parts = explode('_', $token);
		$timestamp = (int) end($parts);
		return $timestamp + self::EXPIRE >= time();
	}

	/**
	 * Генерация случайного токена
	 * восстановления пароля
	 */
	public function generatePasswordResetToken() {
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Очищение токена восстановления пароля
	 */
	public function removePasswordResetToken() {
		$this->password_reset_token = null;
	}

	/**
	 * Проверка токена подтверждения Email
	 * @param $email_confirm_token - токен подтверждения электронной почты
	 * @return null|static
	 */
	public static function findByEmailConfirmToken($email_confirm_token) {
		return static::findOne(['email_confirm_token' => $email_confirm_token, 'status' => self::STATUS_WAIT]);
	}

	/**
	 * Генерация случайного токена
	 * подтверждения электронной почты
	 */ 
	public function generateEmailConfirmToken() {
		$this->email_confirm_token = rand(100000, 999999);
		$this->last_date_email_confirm_token = date('Y-m-d H:i:s');
	}

	/**
	 * Очищение токена подтверждения почты
	 */
	public function removeEmailConfirmToken() {
		$this->email_confirm_token = null;
	}

	/**
	 * @param bool $insert
	 * @param array $changedAttributes
	 * Сохраняем изображения после сохранения
	 * данных пользователя
	 */
	public function afterSave($insert, $changedAttributes) {
		parent::afterSave($insert, $changedAttributes);
		$this->saveImage();
	}

	/**
	 * Действия, выполняющиеся после авторизации.
	 * Сохранение IP адреса и даты авторизации.
	 *
	 * Для активации текущего обновления необходимо
	 * повесить текущую функцию на событие 'on afterLogin'
	 * компонента user в конфигурационном файле.
	 * @param $id - ID пользователя
	 */
	public static function afterLogin($id) {
		self::getDb()->createCommand()->update(self::tableName(), [
			'ip' => $_SERVER["REMOTE_ADDR"],
			'login_at' => date('Y-m-d H:i:s'),
		], ['id' => $id])->execute();
	}

	/**
	 * Сохранение изображения (аватара)
	 * пользвоателя
	 */
	public function saveImage() {
		if ($this->photo) {
			$this->removeImage(); // Сначала удаляем старое изображение
			$module = Yii::$app->controller->module;
			$path = $module->userPhotoPath; // Путь для сохранения аватаров
			$name = time(); // Название файла
			$this->image = $path . '/' . $name . $this::EXT; // Путь файла и название



			if (!file_exists($path)) {
				mkdir($path, 0777, true); // Создаем директорию при отсутствии
			}
			if (is_object($this->photo)) {
				// Загружено через FileUploadInterface

				Image::thumbnail($this->photo->tempName, 200, 200)->save($this->image); // Сохраняем изображение в формате 200x200 пикселей
			} else {
				// Загружено по ссылке с удаленного сервера
				file_put_contents($this->image, $this->photo);
			}
			$this::getDb()
				->createCommand()
				->update($this->tableName(), ['image' => $this->image], ['id' => $this->id])
				->execute();
		}
	}

	/**
	 * Удаляем изображение при его наличии
	 */
	public function removeImage() {
		if ($this->image) {
			// Если файл существует
			if (file_exists($this->image)) {
				unlink($this->image);
			}
			// Не регистрация пользователя
			if (!$this->isNewRecord) {
				$this::getDb()
					->createCommand()
					->update($this::tableName(), ['image' => null], ['id' => $this->id])
					->execute();
			}
		}
	}

	// Отправить платёжный пароль
	public function sendPayPassword() {
		$password = rand(100000, 999999);

		$this->pay_password_hash = Yii::$app->security->generatePasswordHash($password);
		$this->last_date_pay_password = date('Y-m-d H:i:s');

		SendMailComponent::payPassword($this, $password);

		$this->save();
	}

	/**
	 * Список всех пользователей
	 * @param bool $show_id - показывать ID пользователя
	 * @return array - [id => Имя Фамилия (ID)]
	 */
	public static function getAll($show_id = false) {
		$users = [];
		$model = self::find()->all();
		if ($model) {
			foreach ($model as $m) {
				$name = ($m->last_name) ? $m->first_name . " " . $m->last_name : $m->first_name;
				if ($show_id) {
					$name .= " (" . $m->id . ")";
				}
				$users[$m->id] = $name;
			}
		}

		return $users;
	}

	public function getPayWallet($paySystemId, $currencyId) {
		$currency = Currency::find()->where(['id' => $currencyId])->one();
		$paySystem = PaySystem::find()->where(['id' => $paySystemId])->one();
		$classname = PaySystemComponent::getUserWalletClassName($paySystem);
		$userWallet = $classname::find()->where(['user_id' => $this->id, 'pay_system_id' => $paySystem->id, 'currency_id' => $currency->id])->one();

		if (empty($userWallet)) {
			$userWallet = new $classname;
			$userWallet->user_id = $this->id;
			$userWallet->pay_system_id = $paySystem->id;
			$userWallet->currency_id = $currency->id;
			$userWallet->save(false);
		} elseif (isset($userWallet->in_wallet) && $userWallet->in_wallet == '') {
			$userWallet->save(false);
		}

		return $userWallet;
	}

	public function updateAuthAssigment($authRule) {
		foreach ($this->getAuthAssignments()->all() as $authAssignmentItem) {
			$authAssignmentItem->delete();
		};

		$authAssignment = new AuthAssignment;
		$authAssignment->user_id = $this->id;
		$authAssignment->item_name = $authRule;
		$authAssignment->created_at = time();
		$authAssignment->updated_at = time();

		$authAssignment->save();
	}

	public function appendToParent() {
		$parentUser = $this->find()->where(['id' => $this->parent_id])->one();

		$currentUser = $this->find()->where(['id' => $this->id])->one();

		$currentUser->appendTo($parentUser);
	}

	public function getImage() {
		if ($this->image) {
			return '/' . $this->image;
		} else {
			return '/images/male.png';
		}
	}

	public static function find() {
		return new UserQuery(static::className());
	}

	// действия для только что зарегистрированного пользователя
	public function setDefaultRegisterInfo() {
		//SendMailComponent::successRegister($this);

		$defaultRole = Preference::find()->where(['key' => Preference::KEY_REG_USER_DEFAULT_RULE])->one();

		if (empty($defaultRole)){
			$this->updateAuthAssigment(AuthAssignment::ITEM_NAME_NOT_VERIFIED_USER);
		}else{
			$this->updateAuthAssigment($defaultRole->value);
		}

		$this->appendToParent();

		$parent = User::find()->where(['id' => $this->parent_id])->one();
		if (!empty($parent)){
			if ($parent->leaf == 1){
				$parent->leaf = 0;
				$parent->save(false);
			}
		}

		foreach (array_reverse($this->ancestors()->all()) as $num => $ancestor) {
			$event = new Event;
			$event->type = Event::TYPE_REG;
			$event->user_id = $ancestor->id;
			$event->to_user_id = $this->id;
			$event->date_add = date('Y-m-d H:i:s');
			$event->addition_info = $num + 1;
			$event->save();
		}

		SendMailComponent::successPartnerRegister($this);

		return true;
	}

	public function buildUserTree($userAttributes, $id = 'id', $parentId = 'parentId', $nodes = 'nodes', $depth = 0) {
		$childrens[0][$id] = $this->login;
		$childrens[0][$parentId] = 0;

		if ($depth > 0){
			$descendants = $this->descendants($depth)->all();
		}else{
			$descendants = $this->descendants()->all();
		}

		foreach ($descendants as $num => $descendant) {
			$childrens[$num + 1][$id] = $descendant->login;
			$parent = $descendant->parent()->one();
			if (!empty($parent)){
				$childrens[$num + 1][$parentId] = $parent->login;
			}else{
				echo 'У логина: ' . $descendant->login . ' нет пригласителя';exit;
			}

			foreach ($userAttributes as $attribute) {
				if (!empty($attribute['function'])){
					$function = $attribute['function'];
					$result = UserInfo::$function($descendant);
					$childrens[$num + 1][$result['title']] = $result['value'];
				}else{
					$title = $attribute['title'];
					$value = $attribute['value'];
					$openTag = empty($attribute['openTag']) ? '' : $attribute['openTag'];
					$closeTag = empty($attribute['closeTag']) ? '' : $attribute['closeTag'];
					$childrens[$num + 1][$title] = $openTag . $descendant->$value .  $closeTag;
				}
			}
		}

		$userTree = $this->buildTree($childrens, $parentId, $nodes, $id);

		if (!empty($userTree[0][$nodes])) {
			return $userTree[0][$nodes];
		} else {
			return [];
		}
	}

	private function buildTree($flat, $pidKey, $nodes, $idKey = null) {
		$grouped = array();
		foreach ($flat as $sub) {
			$grouped[$sub[$pidKey]][] = $sub;
		}

		$fnBuilder = function ($siblings) use (&$fnBuilder, $nodes, $grouped, $idKey) {
			foreach ($siblings as $k => $sibling) {
				$id = $sibling[$idKey];
				if (isset($grouped[$id])) {
					$sibling[$nodes] = $fnBuilder($grouped[$id], $nodes);
				}
				$siblings[$k] = $sibling;
			}

			return $siblings;
		};

		$tree = $fnBuilder($grouped[0]);

		return $tree;
	}
}
