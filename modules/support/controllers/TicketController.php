<?php

namespace app\modules\support\controllers;

use Yii;
use yii\helpers\Url;
use app\modules\support\models\Ticket;
use app\modules\support\models\TicketSearch;
use app\modules\support\models\TicketMessage;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\admin\controllers\AdminController;
use yii\filters\AccessControl;

/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TicketController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],[
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['ticketIndex'],
                    ],[
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['ticketView'],
                    ],[
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['ticketCreate'],
                    ],[
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['ticketUpdate'],
                    ],[
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['ticketDelete'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ticket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'user' => $this->user,
        ]);
    }

    /**
     * Displays a single Ticket model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $ticketMessageId = 0)
    {

        if ($ticketMessageId != 0){
            $ticketMessage = TicketMessage::find()->where(['id' => $ticketMessageId])->one();

        }else{
            $ticketMessage = new TicketMessage;
            $ticketMessage->user_id = Yii::$app->user->id;
            $ticketMessage->ticket_id = $id;
        }

        $model = $this->findModel($id);
        $model->setMessageIsRead($this->user->id);

        if ($ticketMessage->load(Yii::$app->request->post()) && $ticketMessage->save()) {
            return Yii::$app->getResponse()->redirect(Url::to(['/support/ticket/view', 'id' => $model->id]));
        }else{

        }

        $ticketMessageArray = TicketMessage::find()->where(['ticket_id' => $id])->orderBy(['dateAdd' => SORT_DESC])->all();
        return $this->render('view', [
            'model' => $model,
            'user' => $this->user,
            'ticketMessage' => $ticketMessage,
            'ticketMessageArray' => $ticketMessageArray,
        ]);
    }

    /**
     * Creates a new Ticket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ticket();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Ticket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ticket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ticket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ticket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
