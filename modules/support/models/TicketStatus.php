<?php

namespace app\modules\support\models;

use Yii;

/**
 * This is the model class for table "ticket_status".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 */
class TicketStatus extends \yii\db\ActiveRecord
{
    const NAME_NEW = 'новый';
    const NAME_IN_WORK = 'в работе';
    const NAME_OK = 'выполнен';
    const NAME_CANCEL = 'отменен';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_status';
    }

    public function titleName()
    {
        return Yii::t('app', '{name}', ['name' => $this->name]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'required'],
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'color' => Yii::t('app', 'Цвет'),
        ];
    }
}
