<?php

namespace app\modules\support\models;

use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "ticket_message".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property string $dateAdd
 */
class TicketMessage extends \yii\db\ActiveRecord
{
    const READED_YES = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'text'], 'required'],
            [['user_id', 'ticket_id'], 'integer'],
            [['text'], 'string'],
            [['dateAdd', 'readed'], 'safe'],
        ];
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord){
            if ($this->dateAdd == ''){
                $this->dateAdd = date('Y-m-d H:i:s');
            }
        }else{
            
        }
        parent::beforeSave($insert);
        return true;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'text' => Yii::t('app', 'Текст сообщения'),
            'dateAdd' => Yii::t('app', 'Дата добавления'),
            'readed' => Yii::t('app', 'Прочитан'),
        ];
    }
}
