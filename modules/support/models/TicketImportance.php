<?php

namespace app\modules\support\models;

use Yii;

/**
 * This is the model class for table "ticket_importance".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 */
class TicketImportance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_importance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'required'],
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'color' => Yii::t('app', 'Цвет'),
        ];
    }
}
