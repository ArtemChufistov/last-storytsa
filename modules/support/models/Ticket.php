<?php

namespace app\modules\support\models;

use app\modules\support\models\TicketDepartment;
use app\modules\support\models\TicketStatus;
use app\modules\support\models\TicketMessage;
use app\modules\profile\models\User;
use Yii;

/**
 * This is the model class for table "ticket".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $doing_user_id
 * @property string $text
 * @property integer $status_id
 * @property string $department
 * @property integer $importance_id
 * @property string $dateAdd
 */
class Ticket extends \yii\db\ActiveRecord
{
    public $obsUser;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'department'], 'required'],
            [['user_id', 'doing_user_id', 'status_id', 'importance_id'], 'integer'],
            [['text'], 'string'],
            [['dateAdd'], 'safe'],
            [['department'], 'string', 'max' => 255],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->dateAdd = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    public function getNotReadMessages($userId = 0)
    {
        if ($userId == 0){
            $userId = $this->user_id;
            return TicketMessage::find()->where(['ticket_id' => $this->id])->andWhere(['user_id' => $userId])->andWhere(['is', 'readed', NULL])->all();
        }else{
            return TicketMessage::find()->where(['ticket_id' => $this->id])->andWhere(['!=', 'user_id', $userId])->andWhere(['is', 'readed', NULL])->all();
        }

    }

    public function setMessageIsRead($userId)
    {
        $notReadedMessages = $this->getNotReadMessages($userId);

        foreach ($notReadedMessages as $notReadedMessage) {
            $notReadedMessage->readed = TicketMessage::READED_YES;
            $notReadedMessage->save(false);
        }
    }

    public function getMessages()
    {
        return $this->hasMany(TicketMessage::className(), ['ticket_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDoingUser()
    {
        return $this->hasOne(User::className(), ['id' => 'doing_user_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(TicketStatus::className(), ['id' => 'status_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(TicketDepartment::className(), ['id' => 'department']);
    }

    public function getImportance()
    {
        return $this->hasOne(TicketImportance::className(), ['id' => 'importance_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Создавший участник'),
            'doing_user_id' => Yii::t('app', 'Исполнитель'),
            'text' => Yii::t('app', 'Текст'),
            'status_id' => Yii::t('app', 'Статус'),
            'department' => Yii::t('app', 'Отдел'),
            'importance_id' => Yii::t('app', 'Приоритет'),
            'dateAdd' => Yii::t('app', 'Дата добавления'),
        ];
    }
}
