<?php

namespace app\modules\support\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\support\models\Ticket;

/**
 * TicketSearch represents the model behind the search form of `app\modules\support\models\Ticket`.
 */
class TicketSearch extends Ticket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'doing_user_id', 'status_id', 'importance_id'], 'integer'],
            [['text', 'department', 'dateAdd'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ticket::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'doing_user_id' => $this->doing_user_id,
            'status_id' => $this->status_id,
            'importance_id' => $this->importance_id,
        ]);

        if ($this->dateAdd != ''){
            $query->andFilterWhere(['between', 'dateAdd', date('Y-m-d 0:0:0', strtotime($this->dateAdd)), date('Y-m-d 23:59:59', strtotime($this->dateAdd))]);
        }

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'department', $this->department]);

        return $dataProvider;
    }
}
