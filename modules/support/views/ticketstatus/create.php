<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\support\models\TicketStatus */

$this->title = Yii::t('app', 'Добавление статуса тикета');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы тикетов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
