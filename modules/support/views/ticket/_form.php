<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use app\modules\support\models\TicketStatus;
use app\modules\support\models\TicketDepartment;
use app\modules\support\models\TicketImportance;

/* @var $this yii\web\View */
/* @var $model app\modules\support\models\Ticket */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-body">
            <div class="ticket-form">

                <?php $form = ActiveForm::begin(); ?>

                <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'initValueText' => $model->getUser()->one()->login,
                    'value' => $model->getUser()->one()->id,
                    'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'minimumInputLength' => 2,
                        'ajax' => [
                            'url' => Url::to(['/profile/backuser/searhbylogin']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.login; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                    ],
                ]);?>

                <?php echo $form->field($model, 'doing_user_id')->widget(Select2::classname(), [
                    'initValueText' => empty($model->getDoingUser()->one()) ? '' : $model->getDoingUser()->one()->login,
                    'value' => $model->getUser()->one()->id,
                    'options' => ['placeholder' => Yii::t('app', 'Введите логин')],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'minimumInputLength' => 2,
                        'ajax' => [
                            'url' => Url::to(['/profile/backuser/searhbylogin']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.login; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                    ],
                ]);?>

                <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', []),
                ]); ?>

                <?php echo $form->field($model, 'status_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TicketStatus::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => Yii::t('app', 'Выберите')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>

                <?php echo $form->field($model, 'department')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TicketDepartment::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => Yii::t('app', 'Выберите')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>

                <?php echo $form->field($model, 'importance_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TicketImportance::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => Yii::t('app', 'Выберите')],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>

                <?= $form->field($model, 'dateAdd')
                ->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => $model->getAttributeLabel('dateAdd')],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
      </div>
    </div>
</div>