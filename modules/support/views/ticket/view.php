<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\modules\support\models\Ticket */

$this->title = Yii::t('app', 'Тикет №') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Тех поддержка'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-view">
  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                      'attribute' => 'user_id',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
                    ],[
                      'attribute' => 'doing_user_id',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_doing-user-id', ['model' => $model]);},
                    ],[
                      'attribute' => 'text',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_full-text', ['model' => $model]);},
                    ],[
                      'attribute' => 'status_id',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_status-id', ['model' => $model]);},
                    ],[
                      'attribute' => 'dateAdd',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_date-add', ['model' => $model]);},
                    ],[
                      'attribute' => 'department',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_department-id', ['model' => $model]);},
                    ],[
                      'attribute' => 'importance_id',
                      'format' => 'raw',
                      'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_importance-id', ['model' => $model]);},
                    ],
                ],
            ]) ?>
        </div>
      </div>
    </div>
  </div>

  <div class = "row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
            <strong><?php echo Yii::t('app', 'Ответить'); ?></strong>
        </div>
        <div class="box-body">
          <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($ticketMessage, 'text')->widget(CKEditor::className(), [
                'editorOptions' => ElFinder::ckeditorOptions('elfinder', []),
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary']) ?>
            </div>

          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
  </div>

  <?php foreach($ticketMessageArray as $ticketMessageItem):?>
    <?php if ($ticketMessage->id != $ticketMessageItem->id):?>
      <div class = "row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
                <strong><?php echo $ticketMessageItem->getUser()->one()->login; ?></strong>
                <?php if ($ticketMessageItem->user_id == $user->id):?>
                  <small class="text-muted pull-right" style = "margin-left:15px;">
                    <i class="fa fa-pencil"></i>
                    <a href = "<?php echo Url::to(['/support/ticket/view', 'id' => $model->id, 'ticketMessageId' => $ticketMessageItem->id]);?>">Редактировать</a>
                  </small>
                <?php endif;?>
                <small class="text-muted pull-right">
                  <i class="fa fa-clock-o"></i>
                  <?php echo date('H:i d/m/Y', strtotime($ticketMessageItem->dateAdd));?>
                </small>
            </div>
            <div class="box-body">
              <pre><?php echo $ticketMessageItem->text;?></pre>
            </div>
          </div>
        </div>
      </div>
    <?php endif;?>
  <?php endforeach;?>
</div>