<?php

use yii\web\JsExpression;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use app\modules\support\models\TicketStatus;
use app\modules\support\models\TicketDepartment;
use app\modules\support\models\TicketImportance;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\menu\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Тикеты');
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="menu-index">
    <div class = "row">
        <div class="col-md-12">
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo Yii::t('app', 'Поиск');?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
                 <?= Html::a(Yii::t('app', 'Создать тикет'), ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <?php Pjax::begin(); ?>

                <div class="box-body no-padding">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',
                            [
                              'attribute' => 'id',
                              'format' => 'raw',
                              'label' => Yii::t('app', 'Новых (Всего)'),
                              'value' => function ($model, $user) { return Yii::$app->controller->renderPartial('/grid/_count-new-message', ['model' => $model, 'user' => $user]);},
                            ],[
                              'class' => 'yii\grid\ActionColumn'
                            ],[
                              'attribute' => 'user_id',
                              'format' => 'raw',
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[user_id]',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Введите логин'),
                                  ],
                                  'pluginOptions' => [
                                      'minimumInputLength' => 2,
                                      'ajax' => [
                                          'url' => Url::to(['/profile/backuser/searhbylogin']),
                                          'dataType' => 'json',
                                          'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                      ],
                                      'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                      'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                      'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                                  ]
                              ]),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_user-id', ['model' => $model]);},
                            ],
                            [
                              'attribute' => 'doing_user_id',
                              'format' => 'raw',
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[doing_user_id]',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Введите логин'),
                                  ],
                                  'pluginOptions' => [
                                      'minimumInputLength' => 2,
                                      'ajax' => [
                                          'url' => Url::to(['/profile/backuser/searhbylogin']),
                                          'dataType' => 'json',
                                          'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                      ],
                                      'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                      'templateResult' => new JsExpression('function(user) { return user.login; }'),
                                      'templateSelection' => new JsExpression('function (user) { return user.login || user.text; }'),
                                  ]
                              ]),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_doing-user-id', ['model' => $model]);},
                            ],
                            [
                              'attribute' => 'text',
                              'format' => 'raw',
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_text', ['model' => $model]);},
                            ],
                            [
                              'attribute' => 'status_id',
                              'format' => 'raw',
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[status_id]',
                                  'value' => $searchModel->status_id,
                                  'attribute' => 'status_id',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'data' => ArrayHelper::map(TicketStatus::find()->all(), 'id', 'name'),
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Выберите'),
                                      'style' => ['width' => '150px'],
                                  ],]
                                ),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_status-id', ['model' => $model]);},
                            ],[
                              'attribute' => 'department',
                              'format' => 'raw',
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[department]',
                                  'value' => $searchModel->status_id,
                                  'attribute' => 'status_id',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'data' => ArrayHelper::map(TicketDepartment::find()->all(), 'id', 'name'),
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Выберите'),
                                      'style' => ['width' => '150px'],
                                  ],]
                                ),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_department-id', ['model' => $model]);},
                            ],[
                              'attribute' => 'importance_id',
                              'format' => 'raw',
                              'filter' => Select2::widget([
                                  'name' => (new ReflectionClass($searchModel))->getShortName() . '[importance_id]',
                                  'value' => $searchModel->status_id,
                                  'attribute' => 'status_id',
                                  'theme' => Select2::THEME_BOOTSTRAP,
                                  'data' => ArrayHelper::map(TicketImportance::find()->all(), 'id', 'name'),
                                  'options' => [
                                      'placeholder' => Yii::t('app', 'Выберите'),
                                      'style' => ['width' => '150px'],
                                  ],]
                                ),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_importance-id', ['model' => $model]);},
                            ],[
                              'attribute' => 'dateAdd',
                              'format' => 'raw',
                              'filter' => DatePicker::widget([
                                  'model'=>$searchModel,
                                  'attribute'=>'dateAdd',
                                  'options' => ['class' => 'form-control'],
                                  'language' => 'ru',
    
                              ]),
                              'value' => function ($model) { return Yii::$app->controller->renderPartial('/grid/_date-add', ['model' => $model]);},
                            ],
                        ],
                    ]); ?>
                </div>
            <?php Pjax::end(); ?>
          </div>
        </div>
    </div>
</div>
