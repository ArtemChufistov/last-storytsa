<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\support\models\TicketImportance */

$this->title = Yii::t('app', 'Добавление приоритета тикета');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Приоритеты тикетов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-importance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
